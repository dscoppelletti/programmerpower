Programmer Power
================

> This project is discontinued.

Programmer Power is my software development project, a hobby, a concrete
goal that provides me the chance for gratifying my passion for computer
programming.

Programmer Power is just another development framework, a set of Java
libraries and components for building general purpose applications.<BR>
There are also some end-applications in order to illustrate how you can
use the components; I will try to add more of these applications, but
the most of my interest is in base components, software engineering, and
programming patterns.<BR>
Concerning me, you can say that base components are not mere means of
building user applications; the end-applications are mostly chances for
designing reusable standard components or studying new tools instead.

Since nineties, computer programming has been a main interest of mine,
and I have always wished to develop a project like Programmer Power as a
long-term goal.<BR>
This project has always been a *very long-term* goal and it is so still,
and not only because I can not spend on it as much time and money as I
would like.<BR>
During the years, I have followed the evolution of program languages
(from Pascal to C++, to C# .NET, to Java now); I have done a lot of
refactoring; I have taken some breaks and then I have begun again from
the start, and I have never reached an *end-product* stage.<BR>
I think that many other programming lovers have run a similar way.<BR>
Maybe, only now since I have decided to publish some results of my work,
I should be able to put Programmer Power on a sound basis and to manage
it just like a *real* project.

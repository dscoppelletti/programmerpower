/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Opzioni di un modulo di login.
 * 
 * @since 1.1.0
 */
public final class LoginModuleOptions implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Opzione {@code try_first_pass}: Se impostata con {@code true}, il modulo
     * di login tenta l&rsquo;autenticazione con la prima password accettata da
     * uno dei moduli di login precedenti e, solo se l&rsquo;autenticazione
     * fallisce, richiede una password in proprio.
     * 
     * @see #tryFirstPwd
     */
    public static final String OPT_TRYFIRSTPWD = "try_first_pass";
    
    /**
     * Opzione {@code use_first_pass}: Se impostata con {@code true}, il modulo
     * di login tenta l&rsquo;autenticazione con la prima password accettata da
     * uno dei moduli di login precedenti e, se l&rsquo;autenticazione fallisce,
     * lo segnala senza richiedere una password in proprio.
     * 
     * @see #useFirstPwd
     */
    public static final String OPT_USEFIRSTPWD = "use_first_pass";
    
    /**
     * Opzione {@code try_mapped_pass}: Se impostata con {@code true}, il modulo
     * di login determina la password con la quale tentare
     * l&rsquo;autenticazione a partire dalla prima password accettata da uno
     * dei moduli di login precedenti e, solo se l&rsquo;autenticazione
     * fallisce, richiede una password in proprio.
     * 
     * @see #tryMappedPwd
     */
    public static final String OPT_TRYMAPPEDPWD = "try_mapped_pass";
        
    /**
     * Opzione {@code use_mapped_pass}: Se impostata con {@code true}, il modulo
     * di login determina la password con la quale tentare
     * l&rsquo;autenticazione a partire dalla prima password accettata da uno
     * dei moduli di login precedenti e, se l&rsquo;autenticazione fallisce, lo
     * segnala senza richiedere una password in proprio.
     * 
     * @see #useMappedPwd
     */
    public static final String OPT_USEMAPPEDPWD = "use_mapped_pass";
    
    /**
     * Opzione {@code moduleBanner}: Se impostata con {@code true}, il modulo di 
     * login espone la propria descrizione attraverso il gestore della
     * comunicazione via callback.
     * 
     * @see #isBannerEnabled
     */
    public static final String OPT_MODULEBANNER = "moduleBanner";
    
    /**
     * Opzione {@code debug}: Se impostata con {@code true}, il modulo di login 
     * espone delle informazioni di debug.
     * 
     * @see #isDebugEnabled
     */
    public static final String OPT_DEBUG = "debug";
    
    /**
     * Stato {@code javax.security.auth.login.name}: Nome dell&rsquo;utente
     * accettato per primo da uno dei moduli di login.
     */
    public static final String STATE_NAME = "javax.security.auth.login.name";
    
    /**
     * Stato {@code javax.security.auth.login.password}: Password accettata per
     * prima da uno dei moduli di login.
     */
    public static final String STATE_PWD = "javax.security.auth.login.password";
      
    /**
     * @serial Indica se il modulo di login deve tentare l&rsquo;autenticazione
     *         con la prima password accettata da uno dei moduli di login
     *         precedenti e, solo se l&rsquo;autenticazione fallisce, deve
     *         richiedere una password in proprio.
     */
    private final boolean myTryFirstPwd;
       
    /**
     * @serial Indica se il modulo di login deve tentare l&rsquo;autenticazione
     *         con la prima password accettata da uno dei moduli di login
     *         precedenti e, se l&rsquo;autenticazione fallisce, deve segnalarlo
     *         senza richiedere una password in proprio.
     */    
    private final boolean myUseFirstPwd;
    
    /**
     * @serial Indica se il modulo di login deve determinare la password con la
     *         quale tentare l&rsquo;autenticazione a partire dalla prima
     *         password accettata da uno dei moduli di login precedenti e, solo
     *         se l&rsquo;autenticazione fallisce, deve richiedere una password
     *         in proprio.
     */    
    private final boolean myTryMappedPwd;
    
    /**
     * @serial Indica se il modulo di login deve determinare la password con la
     *         quale tentare l&rsquo;autenticazione a partire dalla prima
     *         password accettata da uno dei moduli di login precedenti e, se
     *         l&rsquo;autenticazione fallisce, deve segnalarlo senza richiedere
     *         una password in proprio.
     */    
    private final boolean myUseMappedPwd;
    
    /**
     * @serial Indica se il modulo di login deve esporre la propria descrizione
     *         attraverso il gestore della comunicazione via callback.
     */    
    private final boolean myBanner;
    
    /**
     * @serial Indica se il modulo di login deve esporre delle informazioni di
     *         debug.
     */    
    private final boolean myDebug;
    
    /**
     * Costruttore.
     * 
     * @param options Collezione. Pu&ograve; essere {@code null}.
     */
    public LoginModuleOptions(Map<String, ?> options) {
        myTryFirstPwd = CollectionTools.getBoolean(
                LoginModuleOptions.OPT_TRYFIRSTPWD, options, false);
        myUseFirstPwd = CollectionTools.getBoolean(
                LoginModuleOptions.OPT_USEFIRSTPWD, options, false);        
        myTryMappedPwd = CollectionTools.getBoolean(
                LoginModuleOptions.OPT_TRYMAPPEDPWD, options, false);
        myUseMappedPwd = CollectionTools.getBoolean(
                LoginModuleOptions.OPT_USEMAPPEDPWD, options, false);
        myBanner = CollectionTools.getBoolean(
                LoginModuleOptions.OPT_MODULEBANNER, options, false);
        myDebug = CollectionTools.getBoolean(
                LoginModuleOptions.OPT_DEBUG, options, false);        
    }

    /**
     * Indica se il modulo di login deve tentare l&rsquo;autenticazione con la
     * prima password accettata da uno dei moduli di login precedenti e, solo se
     * l&rsquo;autenticazione fallisce, deve richiedere una password in proprio.
     * 
     * @return Indicatore.
     */
    public boolean tryFirstPwd() {
        return myTryFirstPwd;
    }
    
    /**
     * Indica se il modulo di login deve tentare l&rsquo;autenticazione con la
     * prima password accettata da uno dei moduli di login precedenti e, se
     * l&rsquo;autenticazione fallisce, deve segnalarlo senza richiedere una
     * password in proprio.
     * 
     * @return Indicatore.
     */    
    public boolean useFirstPwd() {
        return myUseFirstPwd;
    }

    /**
     * Indica se il modulo di login deve determinare la password con la quale
     * tentare l&rsquo;autenticazione a partire dalla prima password accettata
     * da uno dei moduli di login precedenti e, solo se l&rsquo;autenticazione
     * fallisce, deve richiedere una password in proprio.
     * 
     * @return Indicatore.
     */    
    public boolean tryMappedPwd() {
        return myTryMappedPwd;
    }

    /**
     * Indica se il modulo di login deve determinare la password con la quale
     * tentare l&rsquo;autenticazione a partire dalla prima password accettata
     * da uno dei moduli di login precedenti e, se l&rsquo;autenticazione
     * fallisce, deve segnalarlo senza richiedere una password in proprio.
     * 
     * @return Indicatore.
     */    
    public boolean useMappedPwd() {
        return myUseMappedPwd;
    }    

    /**
     * Indica se il modulo di login deve esporre la propria descrizione
     * attraverso il gestore della comunicazione via callback.
     * 
     * @return Indicatore.
     */    
    public boolean isBannerEnabled() {
        return myBanner;
    }

    /**
     * Indica se il modulo di login deve esporre delle informazioni di debug.
     * 
     * @return Indicatore.
     */    
    public boolean isDebugEnabled() {
        return myDebug;
    }    
}

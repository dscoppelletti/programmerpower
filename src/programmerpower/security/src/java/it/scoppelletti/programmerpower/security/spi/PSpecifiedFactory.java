/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory della sorgente di codifica dell&rsquo;input {@code P}
 * per l&rsquo;algoritmo
 * <ACRONYM TITLE="Optimal Asymmetric Encryption Padding">OAEP</ACRONYM> che
 * prevede un valore {@code P} esplicito.
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code pspec.p}</TD>
 *      <TD>Valore per la codifica dell&rsquo;input espresso come sequenza di
 *      byte in formato esadecimale.</TD>
 * </TR>  
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.spi.OAEPParameterSpecFactory
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;1: RSA
 *        Cryptography Standard</A>
 * @see   <A HREF="http://www.ietf.org/rfc/rfc3447.txt" TARGET="_top">RFC
 *        3447: Public-Key Cryptography Standards (PKCS) #1: RSA
 *        Cryptography</A>  
 * @since 1.0.0
 */
public final class PSpecifiedFactory implements PSourceFactory {

    /**
     * Propriet&agrave; {@code p}: Valore {@code P}.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_P = "p";
    
    /**
     * Costruttore.
     */
    public PSpecifiedFactory() {        
    }
    
    public PSource newInstance(Properties props, String prefix) {
        String name, value;
        byte[] p;
        PSource pSrc;
        
        name = StringTools.concat(prefix, PSpecifiedFactory.PROP_P);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        try {
            p = Hex.decodeHex(value.toCharArray());
        } catch (DecoderException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        pSrc = new PSource.PSpecified(p);
        
        return pSrc;
    }
}

/*
 * Copyright (C) 2011-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.data.DataTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code UserManager}.
 * 
 * @since 1.0.0
 */
@Final
@Reserved
@Transactional
public class DefaultUserManager implements UserManager, UserDetailsService {

    @javax.annotation.Resource(name = PasswordDigester.BEAN_NAME)
    private PasswordDigester myPwdDigester;
    
    @PersistenceContext(unitName = DataTools.PERSISTENCE_UNIT)    
    private EntityManager myEntityMgr;
    
    /**
     * Costruttore.
     */
    public DefaultUserManager()  {        
    }
    
    @Transactional(propagation = Propagation.SUPPORTS)
    public User newUser(String code) {
        User user;
    
        if (StringUtils.isBlank(code)) {
            throw new ArgumentNullException("code");
        }
        
        user = new User();
        user.setCode(code.toLowerCase());
        
        return user;
    }
    
    @Transactional(readOnly = true)
    @PreAuthorize("hasRole('it.scoppelletti.admin') or " +
            "#userId == principal.id")                
    public User loadUser(int userId) {        
        return myEntityMgr.find(User.class, userId);
    }
    
    @Transactional(readOnly = true)
    @PreAuthorize("hasRole('it.scoppelletti.admin') or " +
            "#code == authentication.name")            
    public User loadUser(String code) {
        if (StringUtils.isBlank(code)) {
            throw new ArgumentNullException("code");
        }        
        
        try {
            return loadUserByCode(code);
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    @Transactional(readOnly = true)
    public User loadLoggedUser() {
        String code;
        Authentication auth;        
        SecurityContext secCtx = SecurityContextHolder.getContext();
        
        auth = secCtx.getAuthentication(); 
        if (auth == null || !auth.isAuthenticated() ||
                auth instanceof AnonymousAuthenticationToken) {
            return null;
        }
        
        code = auth.getName();
        try {
            return loadUserByCode(code);
        } catch (NoResultException ex) {
            return null;
        }        
    }
                
    /**
     * Legge un utente.
     * 
     * @param  userName Codice per l&rsquo;autenticazione dell&rsquo;utente.
     * @return          Oggetto. Non pu&ograve; essere {@code null}.
     * @throws org.springframework.security.core.userdetails.UsernameNotFoundException
     */    
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String userName) throws
            UsernameNotFoundException {
        SecurityResources res = new SecurityResources();
        
        if (StringUtils.isBlank(userName)) {
            throw new ArgumentNullException("userName");
        }
        
        try {
            return loadUserByCode(userName);
        } catch (NoResultException ex) {
            throw new UsernameNotFoundException(res.getUserNotFoundException(
                    userName), ex);
        }       
    }
    
    /**
     * Legge un utente
     * 
     * @param  code Codice dell&rsquo;utente.
     * @return      Oggetto.
     * @throws      javax.persistence.NoResultException
     */
    private User loadUserByCode(String code) {
        CriteriaQuery<User> query;
        Root<User> root;
        CriteriaBuilder builder = myEntityMgr.getCriteriaBuilder();
        
        query = builder.createQuery(User.class);
        root = query.from(User.class);
        query.where(builder.equal(root.<String>get(User.FIELD_CODE), code));

        return myEntityMgr.createQuery(query).getSingleResult();        
    }
    
    @PreAuthorize("hasRole('it.scoppelletti.admin')")
    public void saveUser(User obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myEntityMgr.persist(obj);
    }
            
    @PreAuthorize("hasRole('it.scoppelletti.admin') or " +
            "hasRole('it.scoppelletti.user') and " +
            "#obj.code == authentication.name")          
    public User updateUser(User obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        return myEntityMgr.merge(obj);
    }
    
    @PreAuthorize("hasRole('it.scoppelletti.admin') and " +
            "#obj.code != authentication.name")
    public void deleteUser(User obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myEntityMgr.remove(obj);
    }
    
    @Transactional(readOnly = true)
    @PreAuthorize("hasRole('it.scoppelletti.admin')")
    public List<User> listUsers() { 
        CriteriaQuery<User> query;
        Root<User> root;
        CriteriaBuilder builder = myEntityMgr.getCriteriaBuilder();        
                       
        query = builder.createQuery(User.class);
        root = query.from(User.class);
        query.orderBy(builder.asc(root.<String>get(User.FIELD_CODE)));
        
        return myEntityMgr.createQuery(query).getResultList();
    }
}

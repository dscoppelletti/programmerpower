/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.util.*;
import javax.security.auth.*;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;
import javax.security.auth.spi.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.context.*;

/**
 * Modulo di login che delega l&rsquo;esecuzione dei metodi
 * dell&rsquo;interfaccia {@code LoginModule} ad un bean che implementa la
 * stessa interfaccia.
 * 
 * @since 1.1.0
 */
public final class LoginModuleWrapper implements LoginModule {
    
    /**
     * Opzione 
     * {@code it.scoppelletti.programmerpower.security.login.LoginModuleWrapper.beanName}:
     * Nome del bean che implementa il modulo di login attraverso
     * l&rsquo;interfaccia {@code LoginModule}.
     */
    public static final String OPT_BEANNAME =
            "it.scoppelletti.programmerpower.security.login." +
            "LoginModuleWrapper.beanName";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            LoginModuleWrapper.class);

    private LoginModule myBean;
    
    /**
     * Costruttore.
     */
    public LoginModuleWrapper() {        
    }
    
    /**
     * Inizializzazione.
     * 
     * @param subject         Soggetto da autenticare.
     * @param callbackHandler Gestore della comunicazione via callback.
     * @param sharedState     Stato condiviso da tutti i moduli di login.
     * @param options         Opzioni del modulo di login.
     */
    public void initialize(Subject subject, CallbackHandler callbackHandler,
            Map<String, ?> sharedState, Map<String, ?> options) {
        myBean = getBean(options);
        if (myBean != null) {
            myBean.initialize(subject, callbackHandler, sharedState, options);
        }
    }

    /**
     * Restituisce il bean che implementa il modulo di login.
     * 
     * @param  options Opzioni del modulo di login.
     * @return         Oggetto.
     */
    private LoginModule getBean(Map<String, ?> options) {
        Object obj;
        String beanName;
        ApplicationContext applCtx;
        LoginModule bean;
        
        obj = options.get(JaasAuthenticationProvider.OPT_APPLICATIONCONTEXT);
        if (obj == null) {
            myLogger.warn("Option {} not set.",
                    JaasAuthenticationProvider.OPT_APPLICATIONCONTEXT);
            return null;
        }
        
        beanName = (String) options.get(LoginModuleWrapper.OPT_BEANNAME);
        if (StringUtils.isBlank(beanName)) {
            myLogger.warn("Option {} not set.",
                    LoginModuleWrapper.OPT_BEANNAME);
            return null;            
        }
        
        try {
            applCtx = (ApplicationContext) obj;
            bean = applCtx.getBean(beanName, LoginModule.class);
        } catch (Exception ex) {
            myLogger.error(String.format("Failed to get bean %1$s.", beanName),
                    ex);
            return null;
        }

        if (!applCtx.isPrototype(beanName)) {
            myLogger.warn("Bean {} is not prototype.", beanName);
            return null;
        }
        
        return bean;
    }
    
    /**
     * Esegue l&rsquo;autenticazione.
     * 
     * @return Indica se l&rsquo;autenticazione ha avuto successo {@code true}
     *         oppure il modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         L&rsquo;autenticazione &egrave; fallita.         
     */
    public boolean login() throws LoginException {
        return (myBean == null) ? false : myBean.login();
    }
    
    /**
     * Conferma l&rsquo;autenticazione.
     * 
     * @return Indica se la conferma ha avuto successo {@code true} oppure il
     *         modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         La conferma &egrave; fallita. 
     */
    public boolean commit() throws LoginException {
        return (myBean == null) ? false : myBean.commit();
    }
    
    /**
     * Annulla l&rsquo;autenticazione.
     * 
     * @return Indica se l&rsquo;annullamento ha avuto successo {@code true}
     *         oppure il modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         L&rsquo;annullamento &egrave; fallito.  
     */
    public boolean abort() throws LoginException {
        return (myBean == null) ? false : myBean.abort();
    }

    /**
     * Esegue il logout.
     * 
     * @return Indica se il logout ha avuto successo {@code true} oppure il
     *         modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         Il logout &egrave; fallito.  
     */
    public boolean logout() throws LoginException {
        boolean ret;
        
        if (myBean == null) {
            return false;
        }
        
        ret = myBean.logout();
        myBean = null;
        
        return ret;
    }
}

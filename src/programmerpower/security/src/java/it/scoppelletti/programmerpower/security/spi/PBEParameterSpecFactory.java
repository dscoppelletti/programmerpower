/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory dell&rsquo;insieme dei parametri per un algoritmo
 * <ACRONYM TITLE="Password Based Encryption">PBE</ACRONYM>.
 *  
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code salt}</TD>
 *      <TD>Sequenza di byte {@code salt} in formato esadecimale.</TD>
 * </TR>
 * <TR>
 *      <TD>{@code iterations}</TD>
 *      <TD>Numero di iterazioni.</TD>
 * </TR>
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getCipher
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;5:
 *        Password-Based Cryptography Standard</A>          
 * @see   <A HREF="http://www.ietf.org/rfc/rfc2898.txt" TARGET="_top">RFC
 *        2898 / PKCS #5: Password-Based Cryptography Specification</A>        
 * @since 1.0.0
 */
public final class PBEParameterSpecFactory implements 
        AlgorithmParameterSpecFactory {

    /**
     * Propriet&agrave; {@code salt}: Sequenza di byte {@code salt}.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_SALT = "salt";
    
    /** 
     * Propriet&agrave; {@code iterations}: Numero di iterazioni.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_ITERATIONCOUNT = "iterations";    
    
    /**
     * Costruttore.
     */
    public PBEParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        int count;
        String name, value;
        byte[] salt;
        PBEParameterSpec param;
        
        name = StringTools.concat(prefix, PBEParameterSpecFactory.PROP_SALT);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        try {
            salt = Hex.decodeHex(value.toCharArray());
        } catch (DecoderException ex) {
            throw SecurityTools.toSecurityException(ex);
        }            
        
        name = StringTools.concat(prefix,
                PBEParameterSpecFactory.PROP_ITERATIONCOUNT);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }        
        count = Integer.parseInt(value);
        
        param = new PBEParameterSpec(salt, count);
        
        return param;
    }
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.KeyStore;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.security.CryptoTools;
import it.scoppelletti.programmerpower.security.SecureString;
import it.scoppelletti.programmerpower.security.SecurityTools;
import it.scoppelletti.programmerpower.types.StringTools;

/**
 * Classe di factory dei parametri per proteggere l&rsquo;accesso ad un registro
 * delle chiavi in base ad una password.
 *
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>
 * <TR>
 *      <TD>{@code pwd}</TD>
 *      <TD>Password.</TD>
 * </TR> 
 * <TR>
 *      <TD>{@code alg}</TD>
 *      <TD>Codice dell&rsquo;algoritmo di cifratura.</TD>
 * </TR>
 * <TR>
 *      <TD>{@code param.factory}</TD>
 *      <TD>Nome della classe di factory dei parametri specifici
 *      dell&rsquo;algoritmo; la classe deve implementare
 *      l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory} e
 *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
 * </TR>
 * <TR>
 *      <TD>{@code param.factory.prefix}</TD>
 *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
 *      interrogate dal provider {@code param.factory}.</TD>          
 * </TR>    
 * </TBODY>
 * </TABLE></P>
 *      
 * @see   it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory
 * @see   <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
 *        Cryptography Architecture, Standard Algorithm Name Documentation for
 *        JDK 8</A>  
 * @since 1.1.0
 */
public final class PasswordProtectionFactory implements
    ProtectionParameterFactory {

    /**
     * Propriet&agrave; {@code pwd}: Password.
     */
    public static final String PROP_PASSWORD = "pwd";
    
    /**
     * Costruttore.
     */
    public PasswordProtectionFactory() {
    }
    
    public KeyStore.ProtectionParameter newInstance(Properties props,
            String prefix) {
        String alg, name;
        SecureString pwd = null;
        AlgorithmParameterSpec cipherParams;
        KeyStore.ProtectionParameter pwdParams;
        char[] v;
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }

        name = StringTools.concat(prefix,
                PasswordProtectionFactory.PROP_PASSWORD);
        pwd = new SecureString(props.getProperty(name));
        v = (pwd.isEmpty()) ? null : pwd.getValue();
        
        try {            
            name = StringTools.concat(prefix, CryptoTools.PROP_CIPHERALGORITHM);
            alg = props.getProperty(name);        
                   
            if (StringUtils.isBlank(alg)) {
                pwdParams = new KeyStore.PasswordProtection(v);
            } else {
                cipherParams = CryptoTools.getAlgorithmParameterSpec(props,
                        prefix);
                pwdParams = new KeyStore.PasswordProtection(v, alg,
                        cipherParams);
            }
        } finally {
            pwd = SecurityTools.destroy(pwd);
        }
        
        return pwdParams;
    }
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory dell&rsquo;insieme dei parametri dell&rsquo;algoritmo
 * <ACRONYM TITLE="Rivest Cipher 5">RC5</ACRONYM>.
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>
 * <TR>
 *      <TD>{@code version}</TD>
 *      <TD>Numero di versione.</TD>
 * </TR> 
 * <TR>
 *      <TD>{@code rounds}</TD>
 *      <TD>Numero di arrotondamenti.</TD>
 * </TR> 
 * <TR>
 *      <TD>{@code word.size}</TD>
 *      <TD>Dimensione della parola (numero di bit).</TD>
 * </TR>      
 * <TR>
 *      <TD>{@code iv}</TD>
 *      <TD>Eventuale vettore di inizializzazione in formato esadecimale per la
 *      modalit&agrave; di feedback; il vettore deve essere costituito da un
 *      numero di byte doppio rispetto alla dimensione della parola:<BR>
 *      sizeof({@code iv}) = 2 * {@code word.size} / 8</TD>
 * </TR>
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getCipher 
 * @see   <A HREF="http://www.ietf.org/rfc/rfc2040.txt" TARGET="_top">RFC 2040:
 *        The RC5, RC5-CBC, RC5-CBC-Pad, and RC5-CTS Algorithms</A>                  
 * @since 1.0.0
 */
public final class RC5ParameterSpecFactory implements 
        AlgorithmParameterSpecFactory {

    /**
     * Codice dell&rsquo;algoritmo di crittografia RC5. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String ALGORITHM = "RC5";
    
    /**
     * Propriet&agrave; {@code versions}: Numero di versions.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_VERSION = "version";
    
    /**
     * Propriet&agrave; {@code rounds}: Numero di arrotondamenti.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_ROUNDS = "rounds";
    
    /**
     * Propriet&agrave; {@code word.size}: Dimensione della parola in numero di
     * bit.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_WORDSIZE = "word.size";
    
    /**
     * Propriet&agrave; {@code iv}: Eventuale vettore di inizializzazione.
     */
    public static final String PROP_IV = "iv";
    
    /**
     * Costruttore.
     */
    public RC5ParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        int rounds, version, wordSize;
        String name, value;
        byte[] iv;
        AlgorithmParameterSpec param;
    
        name = StringTools.concat(prefix, RC5ParameterSpecFactory.PROP_VERSION);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        version = Integer.parseInt(value);
        
        name = StringTools.concat(prefix, RC5ParameterSpecFactory.PROP_ROUNDS);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        rounds = Integer.parseInt(value);
        
        name = StringTools.concat(prefix,
                RC5ParameterSpecFactory.PROP_WORDSIZE);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        wordSize = Integer.parseInt(value);
        
        name = StringTools.concat(prefix, RC5ParameterSpecFactory.PROP_IV);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            iv = null;
        } else {        
            try {
                iv = Hex.decodeHex(value.toCharArray());
            } catch (DecoderException ex) {
                throw SecurityTools.toSecurityException(ex);
            }
        }
        
        if (iv != null) {
            param = new RC5ParameterSpec(version, rounds, wordSize, iv);
        } else {
            param = new RC5ParameterSpec(version, rounds, wordSize);
        }
        
        return param;
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.security.*;
import java.util.*;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;
import org.apache.commons.collections4.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.security.authentication.*;
import org.springframework.security.authentication.jaas.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.core.session.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;

/**
 * Gestore dell&rsquo;autenticazione <ACRONYM
 * TITLE="Java Authentication and Authorization Service">JAAS</ACRONYM>.
 * 
 * @see it.scoppelletti.programmerpower.security.login.JaasAuthenticationRequest
 * @since 1.1.0
 */
@Final
public class JaasAuthenticationProvider implements AuthenticationProvider,
        ApplicationListener<SessionDestroyedEvent> {
    
    /**
     * Opzione
     * {@code it.scoppelletti.programmerpower.security.login.JaasAuthenticationProvider.applCtx}:
     * Contesto dell&rsquo;applicazione.
     */
    public static final String OPT_APPLICATIONCONTEXT =
            "it.scoppelletti.programmerpower.security.login." +
            "JaasAuthenticationProvider.applCtx";

    private static final Logger myLogger = LoggerFactory.getLogger(
            JaasAuthenticationProvider.class);    
    private CallbackHandler myCallbackHandler;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     */
    public JaasAuthenticationProvider() {         
    }
    
    /**
     * Imposta il gestore della comunicazione via callback.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setCallbackHandler(CallbackHandler obj) {
        myCallbackHandler = obj;
    }
    
    /**
     * Esegue l&rsquo;autenticazione.
     * 
     * @param  authRequest Richiesta di autenticazione.
     * @return             Token autenticato.
     * @throws org.springframework.security.core.AuthenticationException
     *         Autenticazione fallita.           
     */
    public Authentication authenticate(Authentication authRequest)
            throws AuthenticationException {
        Object authPrincipal, authCredentials;
        LoginContext loginCtx;
        JaasConfiguration cfg;
        AppConfiguration appCfg;
        PrincipalCollector collector;        
        JaasAuthenticationToken authToken;        
        LoginExceptionResolver exResolver;
        List<GrantedAuthority> authorityList;
        
        if (myCallbackHandler == null) {
            throw new PropertyNotSetException(toString(), "callbackHandler");
        }
                
        exResolver = new DefaultLoginExceptionResolver();
        try {
            appCfg = (AppConfiguration) authRequest.getCredentials();
            cfg = new JaasConfiguration(myApplCtx);
            cfg.setAppConfiguration(appCfg);
            loginCtx = new LoginContext(appCfg.getName(), null,
                    myCallbackHandler, cfg);            
            loginCtx.login();
            
            collector = new PrincipalCollector();
            for (Principal principal : loginCtx.getSubject().getPrincipals()) {
                collector.addPrincipal(principal);               
            }
            
            if (collector.getUserDetails() == null) {
                authPrincipal = authRequest.getPrincipal();
                if (CollectionUtils.isEmpty(
                        loginCtx.getSubject().getPublicCredentials())) {
                    authCredentials = authRequest.getCredentials(); 
                } else {
                    authCredentials =
                            loginCtx.getSubject().getPublicCredentials();                    
                }                
            } else {
                authPrincipal = collector.getUserDetails();
                authCredentials = collector.getUserDetails().getPassword();
            }
             
            authorityList = new ArrayList<>(collector.getAuthorities());
            Collections.sort(authorityList,
                    GrantedAuthorityComparator.getInstance());
            
            authToken = new JaasAuthenticationToken(authPrincipal,
                    authCredentials, authorityList, loginCtx);
        } catch (LoginException ex) {
            throw exResolver.resolveException(ex);
        }
        
        return authToken;
    }

    /**
     * Verifica se il gestore dell&rsquo;autenticazione supporta una classe di
     * richieste di autenticazione.
     * 
     * @param  auth Classe di richieste d autenticazione.
     * @return      Esito della verifica.
     * @see it.scoppelletti.programmerpower.security.login.JaasAuthenticationRequest
     */
    public boolean supports(Class<?> auth) {
        return JaasAuthenticationRequest.class.isAssignableFrom(auth);
    }

    /**
     * Gestisce l&rsquo;evento di terminazione di una sessione autenticata.
     * 
     * @param event Evento.
     */
    @Reserved
    public void onApplicationEvent(SessionDestroyedEvent event) {
        Authentication auth;
        JaasAuthenticationToken authToken;
        LoginContext loginCtx;
        List<SecurityContext> ctxList;
        
        if (event == null) {
            throw new ArgumentNullException("event");
        }
        
        myLogger.trace("Handling SessionDestroyedEvent.");
        ctxList = event.getSecurityContexts();
        if (ctxList == null) {            
            return;
        }
        
        for (SecurityContext secCtx : ctxList) {
            auth = secCtx.getAuthentication();
            if (!(auth instanceof JaasAuthenticationToken)) {
                continue;
            }
                        
            authToken = (JaasAuthenticationToken) auth;            
            loginCtx = authToken.getLoginContext();
            if (loginCtx == null) {
                myLogger.warn("Found JaasAuthenticationToken {} with no " +
                        "LoginContext set.", auth.getPrincipal());
                continue;
            }
            
            myLogger.trace("Logging out {}.", auth.getPrincipal());
            try {
                loginCtx.logout();
            } catch (Exception ex) {
                myLogger.error("Method logout failed.", ex);
            }
        }
    }   
}

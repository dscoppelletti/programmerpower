/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.KeyRep;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.types.EnumTools;
import it.scoppelletti.programmerpower.types.StringTools;

/**
 * Classe di factory di una chiave per un algoritmo di crittografia asimmetrico.
 *  
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code key.alg}</TD>
 *      <TD>Codice dell&rsquo;algoritmo di crittografia.</TD>
 * </TR>  
 * <TR>
 *      <TD>{@code key.type}</TD>
 *      <TD>Codice del tipo di chiave di crittografia secondo
 *      l&rsquo;enumerato {@code KeyRep.Type}.</TD>
 * </TR> 
 * <TR>
 *      <TD>{@code data}</TD>
 *      <TD>Sequenza di byte che rappresenta il valore della chiave codificata
 *      nel formato Base64 come definito da RFC 2045.</TD>
 * </TR> 
 * </TBODY>
 * </TABLE></P>
 *   
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKey
 * @see   <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
 *        Cryptography Architecture, Standard Algorithm Name Documentation for
 *        JDK 8</A>  
 * @see   <A HREF="http://www.ietf.org/rfc/rfc2045.txt" TARGET="_top">RFC
 *        2045: Multipurpose Internet Mail Extensions (MIME) Part One: Format of
 *        Internet Message Bodies</A>
 * @since 1.0.0
 */
public final class EncodedKeyFactory extends AbstractCryptoKeyFactory {

    /**
     * Propriet&agrave; {@code data}: Valore della chiave.
     * 
     * @it.scoppelletti.tag.required
     * @see <A HREF="http://www.ietf.org/rfc/rfc2045.txt" TARGET="_top">RFC
     *      2045: Multipurpose Internet Mail Extensions (MIME) Part One: Format
     *      of Internet Message Bodies</A>  
     */
    public static final String PROP_DATA = "data";
    
    /**
     * Costruttore.
     */
    public EncodedKeyFactory() {       
    }
    
    protected String getAlgorithm(Properties props, String prefix) {
        String name, alg;
        
        name = StringTools.concat(prefix, EncodedKeyFactory.PROP_ALGORITHM);
        alg = props.getProperty(name);
        if (StringUtils.isBlank(alg)) {
            throw new ArgumentNullException(name);
        }        
        
        return alg;
    }
    
    protected KeyRep.Type getKeyType(Properties props, String prefix) {
        String name, value;
        KeyRep.Type keyType;
        
        name = StringTools.concat(prefix, EncodedKeyFactory.PROP_KEYTYPE);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }        
        
        keyType = (KeyRep.Type) EnumTools.valueOf(KeyRep.Type.class, value);
        
        return keyType;
    }
    
    protected KeySpec getKeySpec(String alg, KeyRep.Type keyType,
            Properties props, String prefix) {
        String name, value;
        byte[] data;        
        KeySpec keySpec;
        
        name = StringTools.concat(prefix, EncodedKeyFactory.PROP_DATA);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        data = Base64.getDecoder().decode(value);
        
        switch (keyType) {
        case PUBLIC:
            keySpec = new X509EncodedKeySpec(data);            
            break;
            
        case PRIVATE:
            keySpec = new PKCS8EncodedKeySpec(data);
            break;
            
        default:
            throw new EnumConstantNotPresentException(
                    KeyRep.Type.class, keyType.toString());            
        }
                
        return keySpec;
    }
}

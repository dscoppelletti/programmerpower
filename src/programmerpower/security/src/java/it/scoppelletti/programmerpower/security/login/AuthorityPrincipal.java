/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.io.*;
import java.security.*;
import org.springframework.security.core.*;

/**
 * Elemento di autenticazione costituito da un&rsquo;autorit&agrave;.
 * 
 * @since 1.1.0
 */
public interface AuthorityPrincipal extends Principal, Serializable {

    /**
     * Restituisce l&rsquo;autorit&agrave;.
     * 
     * @return Oggetto.
     */
    GrantedAuthority getAuthority();    
}

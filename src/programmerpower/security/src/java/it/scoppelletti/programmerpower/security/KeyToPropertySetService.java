/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.security.*;
import java.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.spi.*;

/**
 * Servizio per la rappresentazione di una chiave di crittografia con una
 * collezione di parametri che consente di ricostruire la stessa chiave.
 */
final class KeyToPropertySetService extends ServiceLoaderWrapper<
    KeyToPropertySetProvider, Properties> {    
    private final Key myKey;
    
    /**
     * Costruttore.
     * 
     * @param key Chiave.
     */
    KeyToPropertySetService(Key key) {
        super(KeyToPropertySetProvider.class);
        
        if (key == null) {
            throw new ArgumentNullException("key");            
        }
        
        myKey = key;
    }
    
    @Override
    protected Properties query(KeyToPropertySetProvider provider) {
        return provider.toProperties(myKey);
    }    
}

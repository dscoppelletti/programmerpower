/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.util.*;
import javax.security.auth.login.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Configurazione di un contesto di login.
 *  
 * @see it.scoppelletti.programmerpower.security.login.JaasAuthenticationProvider
 * @since 1.1.0
 */
@Final
public class AppConfiguration implements BeanNameAware {   
    private String myName;    
    private final List<AppConfigurationEntry> myEntryList;
    
    /**
     * Costruttore.
     * 
     * @param entries Elementi della configurazione.
     */
    public AppConfiguration(List<AppConfigurationEntry> entries) {
        if (entries == null) {
            throw new ArgumentNullException("entries");
        }
        
        myEntryList = Collections.unmodifiableList(entries);
    }
    
    /**
     * Restituisce il nome del contesto di login.
     * 
     * @return Valore.
     * @see    #setBeanName
     */
    public String getName() {
        if (StringUtils.isBlank(myName)) {
            throw new PropertyNotSetException(toString(), "name");
        }
        
        return myName;
    }
    
    /**
     * Imposta il nome del bean.
     * 
     * @param value Valore.
     * @see         #getName
     */
    @Reserved
    public void setBeanName(String value) {
        myName = value;
    }
    
    /**
     * Restituisce gli elementi della configurazione.
     * 
     * @return Collezione.
     */
    public List<AppConfigurationEntry> getEntries() {
        return myEntryList;
    }
}

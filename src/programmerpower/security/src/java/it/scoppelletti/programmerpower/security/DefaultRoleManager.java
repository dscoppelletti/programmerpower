/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.data.DataTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code RoleManager}.
 * 
 * @since 1.0.0
 */
@Final
@Reserved
@Transactional
public class DefaultRoleManager implements RoleManager {
    
    @PersistenceContext(unitName = DataTools.PERSISTENCE_UNIT)    
    private EntityManager myEntityMgr;
    
    /**
     * Costruttore.
     */
    public DefaultRoleManager() {                
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public Role newRole(String code) {
        Role role;
    
        if (StringUtils.isBlank(code)) {
            throw new ArgumentNullException("code");
        }
        
        role = new Role();
        role.setCode(code.toLowerCase());
        
        return role;
    }
    
    @Transactional(readOnly = true)
    public Role loadRole(int roleId) {
        return myEntityMgr.find(Role.class, roleId);
    }
     
    @Transactional(readOnly = true)
    public Role loadRole(String code) {
        CriteriaQuery<Role> query;
        Root<Role> root;
        CriteriaBuilder builder = myEntityMgr.getCriteriaBuilder();
        
        if (StringUtils.isBlank(code)) {
            throw new ArgumentNullException("code");
        }
        
        query = builder.createQuery(Role.class);
        root = query.from(Role.class);
        query.where(builder.equal(root.<String>get(Role.FIELD_CODE), code));

        try {
            return myEntityMgr.createQuery(query).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    @PreAuthorize("hasRole('it.scoppelletti.admin')")
    public void saveRole(Role obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myEntityMgr.persist(obj);
    }
    
    @PreAuthorize("hasRole('it.scoppelletti.admin')")
    public Role updateRole(Role obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        return myEntityMgr.merge(obj);
    }
        
    @PreAuthorize("hasRole('it.scoppelletti.admin') and #obj.system == false")
    public void deleteRole(Role obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myEntityMgr.remove(obj);
    }
        
    @Transactional(readOnly = true)
    public List<Role> listRoles() {
        CriteriaQuery<Role> query;
        Root<Role> root;
        CriteriaBuilder builder = myEntityMgr.getCriteriaBuilder();
              
        query = builder.createQuery(Role.class);
        root = query.from(Role.class);
        query.orderBy(builder.asc(root.<String>get(Role.FIELD_CODE)));
        
        return myEntityMgr.createQuery(query).getResultList();
    }
}

/*
 * Copyright (C) 2011-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import it.scoppelletti.programmerpower.data.OptimisticLockSupport;
import it.scoppelletti.programmerpower.data.TableGeneration;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;
import it.scoppelletti.programmerpower.types.StringTools;

/**
 * Utente.
 * 
 * @see   it.scoppelletti.programmerpower.security.UserManager
 * @since 1.0.0
 */
@Final
@Entity
@Table(name = "sec_user")
@TableGenerator(name = "sec_user.id", table = TableGeneration.TABLE,
    pkColumnName = TableGeneration.PKCOLUMNNAME, pkColumnValue = "sec_user.id",
    valueColumnName = TableGeneration.VALUECOLUMNNAME,
    initialValue = TableGeneration.INITIALVALUE,
    allocationSize = TableGeneration.ALLOCATIONSIZE)   
public class User implements Serializable, OptimisticLockSupport, UserDetails {
    private static final long serialVersionUID = 2L;
    
    /**
     * Campo {@code code}.
     */
    static final String FIELD_CODE = "code";
    
    /**
     * @serial Id&#46; dell&rsquo;entit&agrave;.
     */
    private Integer myId;
    
    /**
     * @serial Codice.
     */
    private String myCode;
    
    /**
     * @serial Nome.
     */
    private String myName;
    
    /**
     * @serial Password.
     */
    private String myPwd;
    
    /**
     * @serial Indicatore di utente abilitato.
     */
    private boolean myEnabled;
    
    /**
     * @serial Ruoli.
     */
    private Set<Role> myGrantedRoles = new HashSet<Role>();
    
    /**
     * @serial Autorit&agrave;.
     */
    private List<GrantedAuthority> myAuthorities;
    
    /**
     * @serial Numero di versione.
     */
    private int myVersion;
     
    /**
     * Costruttore.
     */
    @Reserved
    public User() {        
    }

    /**
     * Restituisce l&rsquo;id&#46; dell&rsquo;entit&agrave;.
     * 
     * @return Valore.
     */
    @Id 
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "sec_user.id")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return myId;
    }

    /**
     * Imposta l&rsquo;id&#46. dell&rsquo;entit&agrave;.
     * 
     * @param value Valore.
     */
    @SuppressWarnings("unused")
    private void setId(Integer value) {
        myId = value;
    }
    
    /**
     * Restituisce il codice dell&rsquo;utente.
     * 
     * @return Valore.
     */
    @NaturalId
    @Column(name = "code", nullable = false, length = 16)
    public String getCode() {
        return myCode;
    }

    /**
     * Imposta il codice dell&rsquo;utente.
     * 
     * @param value Valore.
     */
    void setCode(String value) {
        myCode = value;
    }

    /**
     * Restituisce il codice per l&rsquo;autenticazione.
     * 
     * @return Valore.
     * @see    #getCode
     */
    @Transient
    public String getUsername() {
        return myCode;
    }
        
    /**
     * Restituisce il nome.
     * 
     * @return Valore.
     * @see    #setName
     */
    @Column(name = "name", length = 255)
    public String getName() {
        return myName;
    }
    
    /**
     * Imposta il nome.
     * 
     * @param value Valore.
     * @see         #getName
     */
    public void setName(String value) {
        myName = value;
    }
    
    /**
     * Restituisce la password.
     * 
     * @return Valore.
     * @see    #setPassword
     */  
    @Column(name = "pwd", nullable = false, length = 64)    
    public String getPassword() {
        return myPwd;
    }
    
    /**
     * Imposta la password.
     * 
     * @param value Valore.
     * @see         #getPassword
     */
    public void setPassword(String value) {
        myPwd = value;
    }
        
    /**
     * Indica se l&rsquo;utente &egrave; abilitato.
     * 
     * @return Indicatore.
     * @see    #setEnabled
     */
    @Column(name = "enabled", nullable = false)
    @Type(type = "org.hibernate.type.TrueFalseType")
    public boolean isEnabled() {
        return myEnabled;
    }
    
    /**
     * Imposta l&rsquo;indicatore di utente abilitato.
     * 
     * @param value Valore.
     * @see         #isEnabled
     */
    public void setEnabled(boolean value) {
        myEnabled = value;
    }
    
    /**
     * Indica se l&rsquo;account non &egrave; scaduto.
     * 
     * @return {@code true}
     */
    @Transient
    public boolean isAccountNonExpired() {
        return true;
    }
    
    /**
     * Indica se l&rsquo;account non &egrave; bloccato.
     * 
     * @return {@code true}
     */
    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }
    
    /**
     * Indica se le credenziali dell&rsquo;utente non sono scadute.
     * 
     * @return {@code true}
     */
    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }
    
    /**
     * Restituisce i ruoli.
     * 
     * @return Collezione
     * @see    #getAuthorities
     * @since  1.1.0
     */
    @ManyToMany
    @JoinTable(name = "sec_user_role",
        joinColumns = @JoinColumn(name = "user_id", nullable = false),
        foreignKey = @ForeignKey(name = "FK_sec_user_role"),
        inverseJoinColumns = @JoinColumn(name = "role_id", nullable = false),
        inverseForeignKey = @ForeignKey(name = "FK_sec_role_user"))
    public Set<Role> getGrantedRoles() {
        // - Hibernate 4.1.7
        // Se imposto FetchType.EAGER, il metodo setAuthorities inoltra
        // un'eccezione NullPointerException proprio al primo accesso alla
        // collezione myGrantedRoles, e sembra addirittura entrare in un loop
        // infinito!
        return myGrantedRoles;
    }
    
    /**
     * Imposta i ruoli.
     * 
     * @param obj Collezione.
     */
    @SuppressWarnings("unused")
    private void setGrantedRoles(Set<Role> obj) {
        myGrantedRoles = obj;
    }
        
    /**
     * Restituisce le autorit&agrave;.
     * 
     * @return Collezione.
     * @see    #getGrantedRoles
     */
    @Transient
    public Collection<GrantedAuthority> getAuthorities() {                 
        return myAuthorities;
    }

    /**
     * Imposta le autorit&agrave;.
     */
    @PostLoad
    @PrePersist
    @PreUpdate
    @Transient
    private void setAuthorities() {
        List<GrantedAuthority> authorityList;
          
        authorityList = new ArrayList<>();
        for (Role role : myGrantedRoles) {
            authorityList.add(new SimpleGrantedAuthority(StringUtils.lowerCase(
                    role.getCode())));
        }
        
        Collections.sort(authorityList,
                GrantedAuthorityComparator.getInstance());
        myAuthorities = Collections.unmodifiableList(authorityList);
    }
    
    @Version
    @Column(name = "version", nullable = false)
    public int getVersion() {
        return myVersion;
    }
    
    /**
     * Imposta il numero di versione.
     * 
     * @param value Valore.
     */
    @SuppressWarnings("unused")
    private void setVersion(int value) {
        myVersion = value;
    }
        
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("code", myCode).toString();
    }   
       
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        User op;
        
        if (!(obj instanceof User)) {
            return false;
        }
        
        op = (User) obj;
        
        if (!StringTools.equals(myCode, op.getCode(), true)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        if (myCode != null) {
            value = 37 * myCode.toLowerCase().hashCode();
        }
        
        return value;
    }    
}

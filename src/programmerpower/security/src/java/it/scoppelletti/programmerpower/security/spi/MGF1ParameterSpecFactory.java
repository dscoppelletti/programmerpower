/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import java.security.spec.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory dell&rsquo;insieme dei parametri dell&rsquo;algoritmo
 * <ACRONYM TITLE="Mask Generation Function 1">MGF1</ACRONYM>.
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code md}</TD>
 *      <TD>Codice dell&rsquo;algoritmo di codifica.</TD>
 * </TR>    
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.spi.OAEPParameterSpecFactory
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;1: RSA
 *        Cryptography Standard</A>
 * @see   <A HREF="http://www.ietf.org/rfc/rfc3447.txt" TARGET="_top">RFC
 *        3447: Public-Key Cryptography Standards (PKCS) #1: RSA
 *        Cryptography</A>           
 * @since 1.0.0
 */
public final class MGF1ParameterSpecFactory implements
        AlgorithmParameterSpecFactory {

    /**
     * Propriet&agrave; {@code md}: Nome dell&rsquo;algoritmo di codifica.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_MD = "md";
        
    /**
     * Costruttore.
     */
    public MGF1ParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        String md, name;
        AlgorithmParameterSpec param;
        
        name = StringTools.concat(prefix, MGF1ParameterSpecFactory.PROP_MD);
        md = props.getProperty(name);
        if (StringUtils.isBlank(md)) {
            throw new ArgumentNullException(name);
        }
        
        param = new MGF1ParameterSpec(md);
        
        return param;
    }       
}

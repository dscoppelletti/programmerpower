/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory dell&rsquo;insieme dei parametri dell&rsquo;algoritmo
 * <ACRONYM TITLE="Optimal Asymmetric Encryption Padding">OAEP</ACRONYM>.
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code md}</TD>
 *      <TD>Codice dell&rsquo;algoritmo di codifica.</TD>
 * </TR>  
 * <TR>
 *      <TD>{@code mgf}</TD>
 *      <TD>Codice dell&rsquo;algoritmo per la funzione di generazione di 
 *      maschera.</TD>
 * </TR>  
 * <TR>
 *     <TD>{@code mgf.param.factory}</TD>
 *     <TD>Nome della classe di factory dei parametri specifici
 *     dell&rsquo;algoritmo per la funzione di generazione della maschera; la
 *     classe deve implementare l&rsquo;interfaccia
 *     {@code AlgorithmParameterSpecFactory} e pu&ograve; prevedere altre
 *     propriet&agrave;.<BR>
 *     Se la propriet&agrave; non &egrave; impostata, sono utilizzati i
 *     parametri di default definiti dallo specifico provider
 *     <ACRONYM TITLE="Java Cryptography Architecture">JCA</ACRONYM>.</TD>          
 * </TR>
 * <TR>
 *      <TD>{@code mgf.param.factory.prefix}</TD>
 *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
 *      interrogate dal provider {@code mgf.param.factory}.</TD>          
 * </TR>  
 * <TR>
 *     <TD>{@code p.source.factory}</TD>
 *     <TD>Nome della classe di factory della sorgente di codifica
 *     dell&rsquo;input {@code P}; la classe deve implementare
 *     l&rsquo;interfaccia {@code PSourceFactory} e pu&ograve; prevedere altre
 *     propriet&agrave;.
 *     Se la propriet&agrave; non &egrave; impostata, &egrave; utilizzato un
 *     valore {@code P} rappresentato da un vettore di byte vuoto.</TD>          
 * </TR>
 * <TR>
 *      <TD>{@code p.source.factory.prefix}</TD>
 *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
 *      interrogate dal provider {@code p.source.factory}.</TD>          
 * </TR>  
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getCipher
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;1: RSA
 *        Cryptography Standard</A>
 * @see   <A HREF="http://www.ietf.org/rfc/rfc3447.txt" TARGET="_top">RFC
 *        3447: Public-Key Cryptography Standards (PKCS) #1: RSA
 *        Cryptography</A>           
 * @since 1.0.0
 */
public final class OAEPParameterSpecFactory implements 
        AlgorithmParameterSpecFactory {

    /**
     * Propriet&agrave; {@code md}: Nome dell&rsquo;algoritmo di codifica.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_MD = "md";
    
    /**
     * Propriet&agrave; {@code mgf}: Nome dell&rsquo;algoritmo per la funzione
     * di generazione di maschera.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_MGF = "mgf";
    
    /**
     * Propriet&agrave; {@code mgf.param.factory}: Eventuale nome di una classe
     * che implementa l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory}
     * per i parametri della funzione di generazione di maschera.
     */
    public static final String PROP_MGFPARAM_FACTORY = "mgf.param.factory";

    /**
     * Propriet&agrave; {@code mgf.param.factory.prefix}: Eventuale prefisso da
     * applicare al nome delle propriet&agrave; interrogate dal provider
     * {@code AlgorithmParameterSpecFactory} per i parametri della funzione di
     * generazione di maschera.
     */    
    public static final String PROP_MGFPARAMFACTORY_PREFIX =
        "mgf.param.factory.prefix";
    
    /**
     * Propriet&agrave; {@code p.source.factory}: Eventuale nome di una classe 
     * che implementa l&rsquo;interfaccia {@code PSourceFactory} per la sorgente
     * di codifica dell&rsquo;input {@code P}.
     */
    public static final String PROP_PSOURCEFACTORY = "p.source.factory";
    
    /**
     * Propriet&agrave; {@code p.source.factory.prefix}: Eventuale prefisso da
     * applicare al nome delle propriet&agrave; interrogate dal provider
     * {@code PSourceFactory} per la sorgente di codifica dell&rsquo;input
     * {@code P}.
     */    
    public static final String PROP_PSOURCEFACTORY_PREFIX =
        "p.source.factory.prefix";
    
    /**
     * Costruttore.
     */
    public OAEPParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        String md, mgf, name;
        AlgorithmParameterSpec mgfParam, param;
        PSource pSrc;
        
        name = StringTools.concat(prefix, OAEPParameterSpecFactory.PROP_MD);
        md = props.getProperty(name);
        if (StringUtils.isBlank(md)) {
            throw new ArgumentNullException(name);
        }
        
        name = StringTools.concat(prefix, OAEPParameterSpecFactory.PROP_MGF);
        mgf = props.getProperty(name);
        if (StringUtils.isBlank(mgf)) {
            throw new ArgumentNullException(name);
        }
        
        mgfParam = getMGFParameterSpec(props, prefix);
        pSrc = getPSource(props, prefix);        
        param = new OAEPParameterSpec(md, mgf, mgfParam, pSrc);
        
        return param;
    }
    
    /**
     * Restituisce un insieme dei parametri specifici per una funzione di
     * generazione di maschera.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code mgf.param.factory}</TD>
     *      <TD>Eventuale nome della classe di factory dei parametri specifici
     *      dell&rsquo;algoritmo; la classe deve implementare
     *      l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
     * </TR>
     * </TBODY>
     * </TABLE></P>
     *  
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     */
    private AlgorithmParameterSpec getMGFParameterSpec(Properties props,
            String prefix) {
        String name, value;
        Class<?> factoryClass;
        AlgorithmParameterSpec param;
        AlgorithmParameterSpecFactory factory;
                
        name = StringTools.concat(prefix,
                OAEPParameterSpecFactory.PROP_MGFPARAM_FACTORY);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            return null;
        }
        
        try {
            factoryClass = Class.forName(value);
            factory = (AlgorithmParameterSpecFactory)
                factoryClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        name = StringTools.concat(prefix,
                OAEPParameterSpecFactory.PROP_MGFPARAMFACTORY_PREFIX);
        param = factory.newInstance(props, props.getProperty(name));
        if (param == null) {
            throw new ReturnNullException(factory.getClass().getName(), 
                    "newInstance");
        }
        
        return param;         
    }
    
    /**
     * Restituisce un insieme dei parametri specifici per la sorgente di
     * codifica dell&rsquo;input {@code P}.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *     <TD>{@code p.source.factory}</TD>
     *     <TD>Eventuale nome della classe di factory della sorgente di codifica
     *     dell&rsquo;input {@code P}; la classe deve implementare
     *     l&rsquo;interfaccia {@code PSourceFactory} e pu&ograve; prevedere
     *     altre propriet&agrave;.</TD>          
     * </TR>
     * </TBODY>
     * </TABLE></P>
     *  
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     */
    private PSource getPSource(Properties props, String prefix) {
        String name, value;
        Class<?> factoryClass;
        PSource pSrc;
        PSourceFactory factory;
                
        name = StringTools.concat(prefix,
                OAEPParameterSpecFactory.PROP_PSOURCEFACTORY);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            return PSource.PSpecified.DEFAULT;
        }
        
        try {
            factoryClass = Class.forName(value);
            factory = (PSourceFactory) factoryClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        name = StringTools.concat(prefix,
                OAEPParameterSpecFactory.PROP_PSOURCEFACTORY_PREFIX);        
        pSrc = factory.newInstance(props, props.getProperty(name));
        if (pSrc == null) {
            throw new ReturnNullException(factory.getClass().getName(), 
                    "newInstance");
        }
        
        return pSrc;         
    }      
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.nio.*;
import java.nio.charset.*;
import java.util.*;
import javax.security.auth.DestroyFailedException;
import javax.security.auth.Destroyable;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Stringa <I>sicura</I>.
 * 
 * <P>La classe {@code SecureString} implementa una stringa con un vettore di
 * caratteri che pu&ograve; essere esplicitamente sovrascritto con valori
 * {@code 0} (metodo #clear) in modo da non mantenere in memoria la stringa
 * precedentemente contenuta fino all&rsquo;azione del
 * <ACRONYM TITLE="Garbage Collector">GC</ACRONYM>; la classe
 * {@code SecureString} &egrave; quindi adatta a conservare temporaneamente
 * stringhe <I>sensibili</I> (ad esempio, password di accesso) che, se mantenute
 * come normali stringhe Java, potrebbero essere pi&ugrave; facilmente
 * intercettate ispezionando la memoria del processo.</P>
 * 
 * @since 1.0.0
 */
public final class SecureString implements ValueType, Destroyable {
    private static final long serialVersionUID = 1;
    
    /**
     * @serial Vettore di caratteri. 
     */
    private char[] myValue;
    
    private transient byte[] myBytes;
    
    /**
     * Costruttore.
     */
    public SecureString() {
        init();        
    }
    
    /**
     * Costruttore.
     * 
     * @param value Vettore. L&rsquo;oggetto {@code SecureString} mantiene il
     *              valore in un proprio vettore di copia rimanendo cos&igrave;
     *              scollegato dal vettore originale.
     */
    public SecureString(char[] value) {
        init();        
        setValue(value);
    }
    
    /**
     * Costruttore.
     * 
     * @param value Valore.
     */
    public SecureString(String value) {
        init();        
        setValue(value);
    }
    
    /**
     * Costruttore.
     * 
     * @param value Valore.
     */
    public SecureString(byte[] value) {
        init();        
        setValue(value, 0, value.length);
    }
    
    /**
     * Costruttore.
     * 
     * @param value  Valore.
     * @param offset Indice del primo elemento del vettore {@code value} da
     *               considerare.
     * @param len    Numero di elementi del vettore {@code value} da
     *               considerare.
     */
    public SecureString(byte[] value, int offset, int len) {
        init();        
        setValue(value, offset, len);
    }
    
    /**
     * Inizializzazione.
     */
    private void init() {
        myValue = new char[0];
        myBytes = null;
    }
               
    /** 
     * Restituisce il valore.
     * 
     * <P>Il metodo {@code getValue} restituisce direttamente il vettore di
     * caratteri interno dell&rsquo;istanza {@code SecureString} che ne conserva
     * il valore, e tale vettore potrebbe quindi essere utilizzato per
     * modificare il valore stesso, bench&eacute; sia sconsigliato.<BR>
     * Nella pratica comune, un oggetto {@code SecureString} &egrave; utilizzato
     * per mantenere temporaneamente una stringa riservata (ad esempio, una
     * password di accesso) ed il metodo {@code getValue} &egrave; utilizzato
     * come accessore a sola lettura di questo valore per passarlo come
     * parametro ad un&rsquo;API di gestione della sicurezza che mantiene la
     * stringa in un proprio vettore di copia rimanendo cos&igrave; scollegata
     * dal vettore originale.</P>
     * 
     * @return Vettore. 
     * @see    #setValue(char[])
     * @see    #setValue(String)
     * @see    #setValue(byte[])
     * @see    #setValue(byte[], int, int)
     * @see    #clear
     */
    public final char[] getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     *  
     * @param value Vettore. L&rsquo;oggetto {@code SecureString} mantiene il
     *              valore in un proprio vettore di copia rimanendo cos&igrave;
     *              scollegato dal vettore originale.
     * @see         #getValue
     */
    public void setValue(char[] value) {
        int n;
        
        clear();
        
        if (value == null) {
            return;
        }
                
        n = value.length;
        if (n == 0) {
            return;
        }
        
        myValue = new char[n];
        System.arraycopy(value, 0, myValue, 0, n);
    }
        
    /**
     * Imposta il valore.
     *  
     * @param value Valore.
     * @see         #getValue
     */
    public void setValue(String value) {
        clear();
        
        if (!StringUtils.isBlank(value)) {
            myValue = value.toCharArray();
        }
    }
    
    /**
     * Imposta il valore.
     *  
     * @param value Valore.
     * @see         #getValue
     */    
    public void setValue(byte[] value) {
        setValue(value, 0, value.length);
    }
    
    /**
     * Imposta il valore.
     *  
     * @param value  Valore.
     * @param offset Indice del primo elemento del vettore {@code value} da
     *               considerare.
     * @param len    Numero di elementi del vettore {@code value} da
     *               considerare.
     * @see          #getValue
     */    
    public void setValue(byte[] value, int offset, int len) {
        char[] charCleaner, v;
        ByteBuffer byteBuf;
        CharBuffer charBuf;
        
        clear();
        
        if (value == null) {
            return;
        }       
        
        byteBuf = ByteBuffer.wrap(value, offset, len);
        charBuf = Charset.defaultCharset().decode(byteBuf);

        v = new char[charBuf.limit()];
        charBuf.get(v);
        
        charBuf.clear();
        charCleaner = new char[charBuf.capacity()];        
        Arrays.fill(charCleaner, (char) 0);
        charBuf.put(charCleaner);
        
        myValue = v;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * <P>Si ponga attenzione al fatto che il metodo {@code toString} 
     * restituisce una normale stringa Java che potrebbe favorire
     * l&rsquo;intercettazione del valore.</P>
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return new String(myValue);
    }   
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        int i, n;
        char[] v;
        SecureString op;
        
        if (!(obj instanceof SecureString)) {
            return false;
        }
                
        op = (SecureString) obj;
        v = op.getValue();
        
        n = myValue.length;
        if (n != v.length) {
            return false;
        }
        
        for (i = 0; i < n; i++) {
            if (myValue[i] != v[i]) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        for (Character c : myValue) {
            value = 37 * value + c.hashCode();     
        }
                
        return value;
    }    
    
    /**
     * Verifica se il valore &egrave; vuoto.
     * 
     * @return Esito della verifica.
     * @see    #isDestroyed
     */
    public boolean isEmpty() {
        return (myValue.length == 0);
    }
    
    /**
     * Azzera il valore.
     * 
     * @see #getValue
     * @see #setValue(char[])
     * @see #setValue(String)
     * @see #setValue(byte[])
     * @see #setValue(byte[], int, int)
     * @see #destroy
     */
    public void clear() {
        Arrays.fill(myValue, (char) 0);
        if (myBytes != null) {
            Arrays.fill(myBytes, (byte) 0);
        }
        
        myValue = new char[0];
        myBytes = null;
    }
    
    /**
     * Verifica se il valore &egrave; vuoto.
     * 
     * <P>Questa implementazione del metodo {@code isDestroyed} equivale al
     * metodo {@code isEmpty}.</P>
     * 
     * @return Esito della verifica.
     * @see    #isEmpty
     * @since  1.1.0
     */    
    @Override
    public boolean isDestroyed() {
        return isEmpty();
    }
    
    /**
     * Azzera il valore.
     * 
     * <P>Questa implementazione del metodo {@code isDestroyed} equivale al
     * metodo {@code clear}.</P>
     * 
     * @see   #clear
     * @since 1.1.0
     */
    public void destroy() throws DestroyFailedException {
        clear();
    }
    
    /**
     * Restituisce il valore codificato come vettore di byte.
     * 
     * <P>Il metodo {@code getBytes} restituisce direttamente il vettore interno
     * dell&rsquo;istanza {@code SecureString} nel quale viene codificata
     * una-tantum la stringa, e tale vettore potrebbe quindi essere utilizzato
     * per modificare il dato restituito dalle successive esecuzioni del metodo
     * {@code getBytes}, bench&eacute; sia sconsigliato.<BR>
     * Nella pratica comune, un oggetto {@code SecureString} &egrave; utilizzato
     * per mantenere temporaneamente una stringa riservata (ad esempio, una
     * password di accesso) ed il metodo {@code getBytes} &egrave; utilizzato
     * come accessore a sola lettura di questo valore per passarlo come
     * parametro ad un&rsquo;API di gestione della sicurezza che mantiene la
     * stringa in un proprio vettore di copia rimanendo cos&igrave; scollegata
     * dal vettore originale.<BR>
     * Il metodo {@code clear} provvede a sovrascrivere con valori {@code 0}
     * anche il vettore interno utilizzato per la codifica della stringa.</P> 
     *  
     * @return Vettore.
     * @see    #setValue(char[])
     * @see    #setValue(String)
     * @see    #setValue(byte[])
     * @see    #setValue(byte[], int, int)
     * @see    #clear  
     */
    public final byte[] getBytes() {
        byte[] byteCleaner;
        ByteBuffer byteBuf;
        CharBuffer charBuf;
        
        if (myBytes != null) {
            return myBytes;
        }
                        
        charBuf = CharBuffer.wrap(myValue);        
        byteBuf = Charset.defaultCharset().encode(charBuf);
        
        myBytes = new byte[byteBuf.limit()];
        byteBuf.get(myBytes);
        
        byteBuf.clear();
        byteCleaner = new byte[byteBuf.capacity()];
        Arrays.fill(byteCleaner, (byte) 0);
        byteBuf.put(byteCleaner);
        
        return myBytes;
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.io.*;
import java.security.*;
import java.util.*;
import javax.security.auth.*;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;
import javax.security.auth.spi.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di base di un modulo di login per l&rsquo;autenticazione attraverso
 * utente e password.
 * 
 * @since 1.1.0
 */
public abstract class AbstractUserLoginModule implements LoginModule {
    private static final Logger myLogger = LoggerFactory.getLogger(
            AbstractUserLoginModule.class);            
    private Subject mySubject;
    private Principal myPrincipal;
    private CallbackHandler myCallbackHandler;
    private LoginModuleOptions myOptions;
    private Map<String, ?> mySharedState;
                
    /**
     * Costruttore.
     */
    protected AbstractUserLoginModule() {        
    }
    
    /**
     * Inizializzazione.
     * 
     * @param subject         Soggetto da autenticare.
     * @param callbackHandler Gestore della comunicazione via callback.
     * @param sharedState     Stato condiviso da tutti i moduli di login.
     * @param options         Opzioni del modulo di login.
     */
    @Final
    public void initialize(Subject subject, CallbackHandler callbackHandler,
            Map<String, ?> sharedState, Map<String, ?> options) {
        mySubject = subject;
        myPrincipal = null;
        myCallbackHandler = callbackHandler;
        myOptions = new LoginModuleOptions(options);        
        mySharedState = sharedState;          
    }

    /**
     * Esegue l&rsquo;autenticazione.
     * 
     * @return Indica se l&rsquo;autenticazione ha avuto successo {@code true}
     *         oppure il modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         L&rsquo;autenticazione &egrave; fallita.         
     */   
    @Final
    public boolean login() throws LoginException {        
        String name, prevName;
        boolean firstPwd;
        SecureString pwd = null;
        NameCallback nameCallback;
        PasswordCallback pwdCallback = null;
        List<Callback> cbList;
        Callback[] cbArray;
        Map<String, Object> sharedState = popSharedState();
        SecurityResources res = new SecurityResources();
        
        if (myCallbackHandler == null) {
            throw new PropertyNotSetException(toString(), "callbackHandler");
        }
        if (mySubject == null) {
            throw new PropertyNotSetException(toString(), "subject");
        }
        if (myOptions == null) {
            myOptions = new LoginModuleOptions(null);
        }
        
        try {            
            cbList = new ArrayList<Callback>();
            if (myOptions.isBannerEnabled()) {
                cbList.add(new TextOutputCallback(
                    TextOutputCallback.INFORMATION, getClass().getName()));
            }
        
            prevName = CollectionTools.getString(LoginModuleOptions.STATE_NAME,
                sharedState, null);
            if (StringUtils.isBlank(prevName)) {
                nameCallback = new NameCallback(res.getNamePrompt());
            } else {
                nameCallback = new NameCallback(res.getNamePrompt(), prevName);
            }
      
            pwdCallback = new PasswordCallback(res.getPwdPrompt(), false);
            firstPwd = false;
            
            if (myOptions.tryFirstPwd() || myOptions.useFirstPwd()) {
                pwd = SecurityTools.getSecureString(
                        LoginModuleOptions.STATE_PWD, sharedState);
                
                if (!StringUtils.isBlank(prevName) &&
                        !ValueTools.isNullOrEmpty(pwd)) {
                    firstPwd = true;
                    nameCallback.setName(prevName);
                    pwdCallback.setPassword(pwd.getValue());                    
                }
            }
            
            if (!firstPwd) {
                cbList.add(nameCallback);
                cbList.add(pwdCallback);
            }       
       
            if (cbList.isEmpty()) {
                name = null;
            } else {
                cbArray = new Callback[cbList.size()];
                myCallbackHandler.handle(cbList.toArray(cbArray));        
                name = nameCallback.getName();
                pwd = new SecureString(pwdCallback.getPassword());
                myPrincipal = authenticate(name, pwd);                                
                pwdCallback.clearPassword();
            }
            
            if (myPrincipal == null && myOptions.tryFirstPwd() && firstPwd) {
                // E' fallito il tentativo di autenticazione con le prime
                // credenziali accettate da un modulo di login precedente:
                // Chiedo delle nuove credenziali per ritentare.                
                cbList.clear();
                
                if (!StringUtils.isBlank(name)) {
                    nameCallback = new NameCallback(res.getNamePrompt(), name);
                }
                cbList.add(nameCallback);                                
                cbList.add(pwdCallback);
                
                cbArray = new Callback[cbList.size()];
                myCallbackHandler.handle(cbList.toArray(cbArray));
                name = nameCallback.getName();
                pwd = new SecureString(pwdCallback.getPassword());
                myPrincipal = authenticate(name, pwd);                
            }                       
            
            if (myPrincipal != null && sharedState != null &&
                    !sharedState.containsKey(LoginModuleOptions.STATE_NAME) &&
                    !sharedState.containsKey(LoginModuleOptions.STATE_PWD)) {
                // L'autenticazione ha avuto successo:
                // Salvo le credenziali a favore dei successivi moduli di login.
                sharedState.put(LoginModuleOptions.STATE_NAME, name);
                sharedState.put(LoginModuleOptions.STATE_PWD, pwd.toString());                                   
            }            
        } catch (IOException|UnsupportedCallbackException|RuntimeException ex) {
            myLogger.error("Method login failed.", ex);
            myPrincipal = null;
            throw new LoginException(ApplicationException.toString(ex));
        } finally {
            if (pwd != null) {
                pwd.clear();
                pwd = null;
            }
            if (pwdCallback != null) {
                pwdCallback.clearPassword();
                pwdCallback = null;
            }
        }

        if (myPrincipal == null) {
            throw new FailedLoginException(res.getFailedLoginException());
        }
                
        return true;
    }
    
    /**
     * Esegue l&rsquo;autenticazione.
     * 
     * @param  name Nome dell&rsquo;utente.
     * @param  pwd  Password.
     * @return      Elemento di autenticazione dell&rsquo;utente. Se
     *              l&rsquo;autenticazione fallisce, restituisce {@code null}.
     */
    protected abstract Principal authenticate(String name, SecureString pwd);
    
    /**
     * Conferma l&rsquo;autenticazione.
     * 
     * @return Indica se la conferma ha avuto successo {@code true} oppure il
     *         modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         La conferma &egrave; fallita. 
     */   
    @Final
    public boolean commit() throws LoginException {
        popSharedState();
        
        if (mySubject == null || myPrincipal == null ||                
                !mySubject.getPrincipals().add(myPrincipal)) {
            return false;
        }
        
        return true;
    }

    /**
     * Annulla l&rsquo;autenticazione.
     * 
     * @return Indica se l&rsquo;annullamento ha avuto successo {@code true}
     *         oppure il modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         L&rsquo;annullamento &egrave; fallito.  
     */   
    @Final
    public boolean abort() throws LoginException {
        popSharedState();
        
        if (myPrincipal == null) {
            return false;
        }
        
        myPrincipal = null;
        return true;
    }

    /**
     * Esegue il logout.
     * 
     * @return Indica se il logout ha avuto successo {@code true} oppure il
     *         modulo di login deve essere ignorato {@code false}.
     * @throws javax.security.auth.Login.LoginException
     *         Il logout &egrave; fallito.  
     */   
    @Final
    public boolean logout() throws LoginException {
        boolean ret;
       
        popSharedState();
        ret = (mySubject != null && myPrincipal != null &&
                mySubject.getPrincipals().remove(myPrincipal));                
        myPrincipal = null;
        
        return ret;
    }

    /**
     * Restituisce lo stato condiviso da tutti i moduli di login e ne azzera
     * il riferimento come campo di istanza.
     * 
     * <P>Poich&eacute; lo stato condiviso da tutti i moduli di login potrebbe
     * anche contenere delle password in chiaro o altri dati sensibili, tutti
     * gli entry-point di questa classe provvedono ad azzerane il riferimento
     * per cercare di evitare che le istanze siano responsabili per questi
     * dati.</P>
     * 
     * @return Collezione.
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> popSharedState() {
        Map<String, Object> map;
        
        map = (Map<String, Object>) mySharedState;
        mySharedState = null;
        
        return map;
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.io.*;
import java.util.*;
import org.springframework.security.core.*;
import it.scoppelletti.programmerpower.*;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code AuthorityPrincipal}.
 * 
 * @since 1.1.0
 */
public final class DefaultAuthorityPrincipal implements AuthorityPrincipal {
    private static final long serialVersionUID = 1L;
    
    /**
     * @serial Autorit&agrave;.
     */
    private final GrantedAuthority myAuthority;
    
    /**
     * Costruttore.
     * 
     * @param authority Autorit&agrave;.
     */
    public DefaultAuthorityPrincipal(GrantedAuthority authority) {
        if (authority == null) {
            throw new ArgumentNullException("role");
        }
        
        myAuthority = authority;
    }
    
    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        return new DefaultAuthorityPrincipal(myAuthority);
    }
    
    /**
     * Rappresenta l&rsquo;autorit&agrave; con una stringa.
     * 
     * @return Stringa.
     */
    public String getName() {
        return myAuthority.getAuthority();
    }
    
    public GrantedAuthority getAuthority() {
        return myAuthority;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return myAuthority.toString();
    }   
       
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        DefaultAuthorityPrincipal op;
        
        if (!(obj instanceof DefaultAuthorityPrincipal)) {
            return false;
        }
        
        op = (DefaultAuthorityPrincipal) obj;
        
        if (!Objects.equals(myAuthority, op.getAuthority())) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * myAuthority.hashCode();
        
        return value;
    }    
}

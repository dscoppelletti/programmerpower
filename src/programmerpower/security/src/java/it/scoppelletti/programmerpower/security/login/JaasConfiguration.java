/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.security.*;
import java.util.*;
import javax.security.auth.login.*;
import org.slf4j.*;
import org.springframework.context.*;
import it.scoppelletti.programmerpower.*;

/**
 * Configurazione <ACRONYM
 * TITLE="Java Authentication and Authorization Service">JAAS</ACRONYM>.
 */
final class JaasConfiguration extends Configuration {
    private static final Logger myLogger = LoggerFactory.getLogger(
            JaasConfiguration.class);
    private final ApplicationContext myApplCtx;
    private AppConfigurationEntry[] myEntries;
           
    /**
     * Costruttore.
     * 
     * @param applCtx Contesto dell&rsquo;applicazione;
     */
    JaasConfiguration(ApplicationContext applCtx) {
        if (applCtx == null) {
            throw new ArgumentNullException("applCtx");
        }
        
        myApplCtx = applCtx;
    }
    
    /**
     * Restituisce il tipo della configurazione.
     * 
     * <P>Questa versione del metodo {@code getType} restituisce il nome
     * completo della classe che implementa la configurazione.</P>
     * 
     * @return Valore.
     */
    public String getType() {
        return getClass().getCanonicalName();
    }
    
    /**
     * Restituisce il provider della configurazione.
     * 
     * @return Oggetto ({@code null}).
     */
    @Override
    public Provider getProvider() {
        return null;
    }
    
    /**
     * Restituisce i parametri della configurazione.
     * 
     * @return Oggetto ({@code null}).
     */    
    @Override
    public Configuration.Parameters getParameters() {
        return null;
    }
    
    /**
     * Imposta la configurazione del contesto di login.
     * 
     * @param obj Oggetto.
     */    
    void setAppConfiguration(AppConfiguration obj) {
        int idx;
        Object value;
        AppConfigurationEntry entry;
        AppConfigurationEntry[] entries;
        Map<String, Object> options;
        
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        idx = 0;
        entries = new AppConfigurationEntry[obj.getEntries().size()];
        for (AppConfigurationEntry item : obj.getEntries()) {
            options = new HashMap<String, Object>(item.getOptions());
            
            value = options.put(
                    JaasAuthenticationProvider.OPT_APPLICATIONCONTEXT,
                    myApplCtx); 
            if (value != null) {
                myLogger.warn("Ovverriding option {} (original value: {})." ,
                        JaasAuthenticationProvider.OPT_APPLICATIONCONTEXT,
                        value);
            }
                            
            entry = new AppConfigurationEntry(item.getLoginModuleName(),
                    item.getControlFlag(), options);
            entries[idx++] = entry;
        }        
        
        myEntries = entries;
    }
    
    /**
     * Restituisce la configurazione di un contesto di login.
     * 
     * @param  name Nome del contesto di login.
     * @return      Vettore. Se il contesto di login non &egrave; configurato,
     *              restituisce {@code null}.
     */
    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
        return myEntries;
    }     
    
    /**
     * Rilegge la configurazione.
     */
    @Override
    public void refresh() {
        // NOP
    }   
}

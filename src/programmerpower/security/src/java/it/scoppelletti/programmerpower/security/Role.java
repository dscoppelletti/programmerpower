/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;
import it.scoppelletti.programmerpower.data.OptimisticLockSupport;
import it.scoppelletti.programmerpower.data.TableGeneration;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;
import it.scoppelletti.programmerpower.resources.ResourceTools;
import it.scoppelletti.programmerpower.types.StringTools;

/**
 * Ruolo.
 * 
 * @see   it.scoppelletti.programmerpower.security.RoleManager
 * @since 1.1.0
 */
@Final
@Entity
@Table(name = "sec_role")
@TableGenerator(name = "sec_role.id", table = TableGeneration.TABLE,
    pkColumnName = TableGeneration.PKCOLUMNNAME, pkColumnValue = "sec_role.id",
    valueColumnName = TableGeneration.VALUECOLUMNNAME,
    initialValue = TableGeneration.INITIALVALUE,
    allocationSize = TableGeneration.ALLOCATIONSIZE)   
public class Role implements Serializable, OptimisticLockSupport {
    private static final long serialVersionUID = 1L;
    
    /**
     * Campo {@code code}.
     */
    static final String FIELD_CODE = "code";
    
    /**
     * @serial Id&#46; dell&rsquo;entit&agrave;.
     */
    private Integer myId;
    
    /**
     * @serial Codice.
     */
    private String myCode;

    /**
     * @serial Indicatore di ruolo di sistema.
     */
    private boolean mySystem;
    
    /**
     * @serial Descrizione.
     */
    private String myDesc;
    
    /**
     * @serial Descrizioni localizzate.
     */
    private Map<String, String> myDescMap = new HashMap<String, String>();
    
    /**
     * @serial Numero di versione.
     */
    private int myVersion;
    
    /**
     * Costruttore.
     */
    @Reserved
    public Role() {        
    }
    
    /**
     * Restituisce l&rsquo;id&#46; dell&rsquo;entit&agrave;.
     * 
     * @return Valore.
     */
    @Id 
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "sec_role.id")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return myId;
    }

    /**
     * Imposta l&rsquo;id&#46. dell&rsquo;entit&agrave;.
     * 
     * @param value Valore.
     */
    @SuppressWarnings("unused")
    private void setId(Integer value) {
        myId = value;
    }
    
    /**
     * Restituisce il codice del ruolo.
     * 
     * @return Valore.
     */
    @NaturalId
    @Column(name = "code", nullable = false, length = 255)
    public String getCode() {
        return myCode;
    }

    /**
     * Imposta il codice del ruolo.
     * 
     * @param value Valore.
     */
    void setCode(String value) {
        myCode = value;
    }    
    
    /**
     * Indica se il ruolo &egrave; di sistema.
     * 
     * @return Indicatore.
     * @see    #setSystem
     */
    @Column(name = "system", nullable = false, updatable = false)
    @Type(type = "org.hibernate.type.TrueFalseType")    
    public boolean isSystem() {
        return mySystem;
    }
    
    /**
     * Imposta l&rsquo;indicatore di ruolo di sistema.
     * 
     * @param value Valore.
     * @see         #isSystem
     */
    public void setSystem(boolean value) {
        mySystem = value;
    }
    
    /**
     * Restituisce la descrizione.
     * 
     * @return Valore.
     * @see    #setDescription
     * @see    #getLocalizedDescription
     */
    @Column(name = "description", nullable = false, length = 255)
    public String getDescription() {
        return myDesc;
    }
    
    /**
     * Imposta la descrizione.
     * 
     * @param value Valore.
     * @see         #getDescription
     */
    public void setDescription(String value) {
        myDesc = value;
    }
    
    /**
     * Restituisce le descrizioni localizzate.
     * 
     * @return Collezione.
     * @see    #getLocalizedDescription
     */
    @ElementCollection   
    @JoinTable(name = "sec_role_desc",
        joinColumns = @JoinColumn(name = "role_id"),
        foreignKey = @ForeignKey(name = "FK_sec_role_desc"))
    @MapKeyColumn(name = "locale", nullable = false, length = 10)
    @Column(name = "description", nullable = false, length = 255)    
    public Map<String, String> getLocalizedDescriptions() {
        return myDescMap;
    }
    
    /**
     * Imposta le descrizioni localizzate.
     * 
     * @param obj Collezione.
     */
    @SuppressWarnings("unused")
    private void setLocalizedDescriptions(Map<String, String> obj) {
        myDescMap = obj;
    }

    /**
     * Restituisce la descrizione nella localizzazione corrente.
     * 
     * @return Valore.
     * @see    #getDescription
     * @see    #getLocalizedDescriptions
     */
    @Transient
    public String getLocalizedDescription() {
        return ResourceTools.getLocalizedString(myDescMap, null,
                StringUtils.isBlank(myDesc) ? myCode : myDesc);        
    }
    
    @Version
    @Column(name = "version", nullable = false)
    public int getVersion() {
        return myVersion;
    }
    
    /**
     * Imposta il numero di versione.
     * 
     * @param value Valore.
     */
    @SuppressWarnings("unused")
    private void setVersion(int value) {
        myVersion = value;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("code", myCode).toString();        
    }   
       
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        Role op;
        
        if (!(obj instanceof Role)) {
            return false;
        }
        
        op = (Role) obj;
        
        if (!StringTools.equals(myCode, op.getCode(), true)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        if (myCode != null) {
            value = 37 * myCode.toLowerCase().hashCode();
        }
        
        return value;
    }    
}

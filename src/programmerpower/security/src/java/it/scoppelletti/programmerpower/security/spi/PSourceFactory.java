/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import javax.crypto.spec.*;

/**
 * Interfaccia di factory della sorgente di codifica dell&rsquo;input {@code P}
 * per l&rsquo;algoritmo
 * <ACRONYM TITLE="Optimal Asymmetric Encryption Padding">OAEP</ACRONYM>.
 * 
 * @see   it.scoppelletti.programmerpower.security.spi.OAEPParameterSpecFactory
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;1: RSA
 *        Cryptography Standard</A>
 * @see   <A HREF="http://www.ietf.org/rfc/rfc3447.txt" TARGET="_top">RFC
 *        3447: Public-Key Cryptography Standards (PKCS) #1: RSA
 *        Cryptography</A> 
 * @since 1.0.0
 */
public interface PSourceFactory {
 
    /**
     * Istanzia un oggetto.
     * 
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}. 
     * @return        Oggetto.
     */   
    PSource newInstance(Properties props, String prefix);
}

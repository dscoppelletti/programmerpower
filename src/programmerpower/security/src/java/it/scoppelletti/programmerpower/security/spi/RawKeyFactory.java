/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.Key;
import java.util.Base64;
import java.util.Properties;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.types.StringTools;

/**
 * Classe di factory di una chiave per un algoritmo di crittografia simmetrico.
 *  
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code key.alg}</TD>
 *      <TD>Codice dell&rsquo;algoritmo di crittografia.</TD>
 * </TR>   
 * <TR>
 *      <TD>{@code data}</TD>
 *      <TD>Sequenza di byte che rappresenta il valore della chiave codificata
 *      nel formato Base64 come definito da RFC 2045.</TD>
 * </TR> 
 * </TBODY>
 * </TABLE></P>
 *   
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKey
 * @see   <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
 *        Cryptography Architecture, Standard Algorithm Name Documentation for
 *        JDK 8</A>    
 * @see   <A HREF="http://www.ietf.org/rfc/rfc2045.txt" TARGET="_top">RFC
 *        2045: Multipurpose Internet Mail Extensions (MIME) Part One: Format of
 *        Internet Message Bodies</A>
 * @since 1.0.0
 */
public final class RawKeyFactory implements CryptoKeyFactory {
        
    /** 
     * Propriet&agrave; {@code data}: Valore della chiave.
     * 
     * @it.scoppelletti.tag.required
     * @see <A HREF="http://www.ietf.org/rfc/rfc2045.txt" TARGET="_top">RFC
     *      2045: Multipurpose Internet Mail Extensions (MIME) Part One: Format
     *      of Internet Message Bodies</A>  
     */
    public static final String PROP_DATA = "data";
    
    /**
     * Costruttore.
     */
    public RawKeyFactory() {       
    }
    
    public Key newInstance(Properties props, String prefix) {
        String alg, name, value;
        byte[] data;        
        SecretKeySpec keySpec;
        
        name = StringTools.concat(prefix, RawKeyFactory.PROP_ALGORITHM);
        alg = props.getProperty(name);
        if (StringUtils.isBlank(alg)) {
            throw new ArgumentNullException(name);
        }        
        
        name = StringTools.concat(prefix, RawKeyFactory.PROP_DATA);        
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        data = Base64.getDecoder().decode(value);
        keySpec = new SecretKeySpec(data, alg);
                       
        return keySpec;
    }
}

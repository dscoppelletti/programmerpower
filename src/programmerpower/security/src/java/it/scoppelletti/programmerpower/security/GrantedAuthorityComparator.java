/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.util.*;
import org.apache.commons.lang3.*;
import org.springframework.security.core.*;
import it.scoppelletti.programmerpower.*;

/**
 * Confronto tra due autorit&agrave;.
 * 
 * <P>Si ricorda che la documentazione dell&rsquo;interfaccia
 * {@code UserDetails} di Spring Security stabilisce che il metodo
 * {@code getAuthorities} dell&rsquo;interfaccia deve restituire la collezione
 * delle autorit&agrave; ordinate per <I>chiave naturale</I>.</P>
 * 
 * @since 1.1.0
 */
public final class GrantedAuthorityComparator implements
    Comparator<GrantedAuthority> {

    private static final GrantedAuthorityComparator myInstance =
            new GrantedAuthorityComparator();
    
    /**
     * Costruttore privato per classe singleton.
     */
    private GrantedAuthorityComparator() {        
    }
    
    /**
     * Restituisce l&rsquo;istanza dell&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    public static GrantedAuthorityComparator getInstance() {
        return myInstance;
    }
    
    /**
     * Esegue il confronto tra due oggetti.
     * 
     * @param  obj1 Primo oggetto.
     * @param  obj2 Secondo oggetto.
     * @return      Risultato del confronto.
     */
    public int compare(GrantedAuthority obj1, GrantedAuthority obj2) {
        String role1, role2;
        
        if (obj1 == null) {
            throw new ArgumentNullException("Argument obj1 is null.");
        }
        if (obj2 == null) {
            throw new ArgumentNullException("Argument obj2 is null.");
        }
        
        role1 = obj1.getAuthority();
        role2 = obj2.getAuthority();

        // L'implementazione del confronto per ordinare le autorita' della
        // classe User di Spring Security 3.1.3 stabilisce che le autorita'
        // "personalizzate" (per le quali non e' definita una rappresentazione
        // come stringa sufficientemente precisa per il controllo degli accessi)
        // compaiano prima in una collezione ordinata.
        if (StringUtils.isBlank(role1)) {
            if (StringUtils.isBlank(role2)) {
                return 0;
            } else {
                return -1;
            }
        } else if (StringUtils.isBlank(role2)) {
            return 1;
        }
                
        return role1.compareTo(role2);
    }
}

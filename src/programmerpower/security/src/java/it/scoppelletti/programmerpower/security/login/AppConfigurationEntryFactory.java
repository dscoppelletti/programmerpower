/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.util.*;
import javax.security.auth.login.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Classe di factory della configurazione di un modulo di login.
 */
@Final
public class AppConfigurationEntryFactory {
    private String myLoginModuleName;
    private AppConfigurationEntry.LoginModuleControlFlag myControlFlag;
    private Map<String, ?> myOptions;
    
    /**
     * Costruttore.
     */
    public AppConfigurationEntryFactory() {
    }
    
    /**
     * Imposta il nome del modulo di login.
     * 
     * @param value Valore.
     */
    @Required
    public void setLoginModuleName(String value) {
        myLoginModuleName = value;
    }
    
    /**
     * Imposta la modalit&agrave; di controllo del modulo di login.
     * 
     * @param value Valore.
     */
    @Required
    public void setControlFlag(
            AppConfigurationEntry.LoginModuleControlFlag value) {
        myControlFlag = value;
    }
    
    /**
     * Imposta le opzioni del modulo di login.
     * 
     * @param map Collezione.
     */
    @Required
    public void setOptions(Map<String, ?> map) {
        myOptions = map;
    }
    
    /**
     * Istanzia la configurazione di un modulo di login.
     * 
     * @return Oggetto.
     */
    public AppConfigurationEntry newInstance() {
        AppConfigurationEntry entry;
        
        if (StringUtils.isBlank(myLoginModuleName)) {
            throw new PropertyNotSetException(toString(), "loginModuleName");
        }
        if (myControlFlag == null) {
            throw new PropertyNotSetException(toString(), "controlFlag");
        }
        if (myOptions == null) {
            throw new PropertyNotSetException(toString(), "options");
        }
        
        entry = new AppConfigurationEntry(myLoginModuleName, myControlFlag,
                myOptions);
        return entry;
    }
}

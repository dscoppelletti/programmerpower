/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;

/**
 * Implementazione di base dell&rsquo;interfaccia {@code CryptoKeyFactory}. 
 *  
 * @since 1.0.0
 */
public abstract class AbstractCryptoKeyFactory implements CryptoKeyFactory {

    /**
     * Propriet&agrave; {@code key.type}: Codice del tipo di chiave di
     * crittografia (enumerato {@code KeyRep.Type}).
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_KEYTYPE = "key.type";    
    
    /**
     * Costruttore.
     */
    protected AbstractCryptoKeyFactory() {
    }
     
    public final Key newInstance(Properties props, String prefix) {
        String alg;
        KeyRep.Type keyType;
        Key key;
        KeyFactory asymmetricKeyFactory = null;
        SecretKeyFactory secretKeyFactory = null;
        KeySpec keySpec;
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }
    
        alg = getAlgorithm(props, prefix);
        if (StringUtils.isBlank(alg)) {
            throw new ReturnNullException(getClass().getName(), "getAlgorithm");
        }
    
        keyType = getKeyType(props, prefix);
        if (keyType == null) {
            throw new ReturnNullException(getClass().getName(), "getKeyType");
        }
        
        try {
            if (keyType == KeyRep.Type.SECRET) {
                secretKeyFactory = SecretKeyFactory.getInstance(alg);
            } else {
                asymmetricKeyFactory = KeyFactory.getInstance(alg);
            }
        } catch (NoSuchAlgorithmException ex) {
            throw SecurityTools.toSecurityException(ex);
        }

        keySpec = getKeySpec(alg, keyType, props, prefix);
        if (keySpec == null) {
            throw new ReturnNullException(getClass().getName(), "getKeySpec");
        }
        
        try {
            switch (keyType) {
                case PUBLIC:
                    key = asymmetricKeyFactory.generatePublic(keySpec);
                    break;
                    
                case PRIVATE:
                    key = asymmetricKeyFactory.generatePrivate(keySpec);
                    break;
                    
                case SECRET:
                    key = secretKeyFactory.generateSecret(keySpec);
                    break;
                    
                default:
                    throw new EnumConstantNotPresentException(
                            KeyRep.Type.class, keyType.toString());
            }
        } catch (InvalidKeySpecException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        return key;
    }   

    /**
     * Restituisce il codice dell&rsquo;algoritmo di crittografia.
     *   
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}. 
     * @return        Valore. Non pu&ograve; essere {@code null}.  
     * @see    <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
     *         Cryptography Architecture, Standard Algorithm Name Documentation
     *         for JDK 8</A>       
     */
    protected abstract String getAlgorithm(Properties props, String prefix);
    
    /**
     * Restituisce il tipo di chiave.
     * 
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Valore. Non pu&ograve; essere {@code null}.
     */
    protected abstract KeyRep.Type getKeyType(Properties props, String prefix);
    
    /**
     * Restituisce l&rsquo;insieme dei parametri di calcolo della chiave.
     * 
     * @param  alg     Codice dell&rsquo;algoritmo.
     * @param  keyType Tipo di chiave.
     * @param  props   Propriet&agrave;.
     * @param  prefix  Prefisso da applicare al nome delle propriet&agrave; da
     *                 interrogare. Pu&ograve; essere {@code null}. 
     * @return         Oggetto. Non pu&ograve; essere {@code null}.
     */
    protected abstract KeySpec getKeySpec(String alg, KeyRep.Type keyType,
            Properties props, String prefix);   
}

/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import javax.crypto.*;

/**
 * Operazioni di cifratura.
 * 
 * @since 1.0.0
 */
public enum CipherOperation {
    
    /**
     * Cifratura.
     */
    ENCRYPT(Cipher.ENCRYPT_MODE),
    
    /**
     * Decifratura.
     */
    DECRYPT(Cipher.DECRYPT_MODE),
    
    /**
     * <DFN>Incartamento</DFN> di una chiave.
     */
    WRAP(Cipher.WRAP_MODE),
    
    /**
     * Estrazione di una chiave incartata.
     */
    UNWRAP(Cipher.UNWRAP_MODE);
    
    private final int myMode;
    
    /**
     * Costruttore.
     * 
     * @param opmode Modalit&agrave;.
     */
    private CipherOperation(int opmode) {
        myMode = opmode;
    }
    
    /**
     * Restituisce il corrispondente codice JCA.
     * 
     * @return Valore.
     */
    public int toJCA() {
        return myMode;
    }
}

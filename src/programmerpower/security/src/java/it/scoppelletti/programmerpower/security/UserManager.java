/*
 * Copyright (C) 2011-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.util.List;

/**
 * Gestore degli utenti.
 *  
 * @see   it.scoppelletti.programmerpower.security.User
 * @since 1.0.0
 */
public interface UserManager {    
    
    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String BEAN_NAME =
        "it-scoppelletti-programmerpower-security-userManager";
    
    /**
     * Istanzia un utente.
     * 
     * @param  code Codice dell&rsquo;utente.
     * @return      Oggetto.
     */
    User newUser(String code);
    
    /**
     * Legge un utente.
     * 
     * @param  userId Id&#46; dell&rsquo;utente.
     * @return        Oggetto. Se l&rsquo;utente non esiste, restituisce 
     *                {@code null}.
     */
    User loadUser(int userId);
        
    /**
     * Legge un utente.
     * 
     * @param  code Codice dell&rsquo;utente.
     * @return      Oggetto. Se l&rsquo;utente non esiste, restituisce 
     *              {@code null}.
     * @since       1.1.0
     */
    User loadUser(String code);
    
    /**
     * Legge l&rsquo;utente collegato.
     * 
     * @return Oggetto. Se nessun utente &egrave; collegato restituisce
     *         {@code null}.
     */
    User loadLoggedUser();
    
    /**
     * Crea un utente.
     * 
     * @param obj Oggetto.
     */
    void saveUser(User obj);
       
    /**
     * Aggiorna un utente.
     * 
     * @param  obj Oggetto.
     * @return     Oggetto aggiornato.
     */
    User updateUser(User obj);
    
    /**
     * Cancella un utente.
     * 
     * @param obj Oggetto.
     */
    void deleteUser(User obj);
    
    /**
     * Elenco degli utenti.
     * 
     * @return Collezione.
     */
    List<User> listUsers();
}

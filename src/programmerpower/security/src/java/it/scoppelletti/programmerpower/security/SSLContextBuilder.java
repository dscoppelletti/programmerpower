/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.commons.lang3.StringUtils;

/**
 * Costruttore di un contesto
 * <ACRONYM TITLE="Secure Socket Layer">SSL</ACRONYM>.
 * 
 * @since 1.1.0
 */
public final class SSLContextBuilder {
    
    /**
     * Codice del protocollo SSL di default. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String DEF_PROTOCOL = "TLSv1";
    
    private String myProtocol;
    private KeyManager[] myKeyManagers;
    private TrustManager[] myTrustManagers;
    
    /**
     * Costruttore.
     */
    public SSLContextBuilder() {
    }
    
    /**
     * Imposta il codice del protocollo.
     * 
     * @param                       value Valore.
     * @return                            Oggetto per metodi concatenati.
     * @it.scoppelletti.tag.default       {@link #DEF_PROTOCOL}
     * @see <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
     *      Cryptography Architecture, Standard Algorithm Name Documentation for
     *      JDK 8</A>  
     */
    public SSLContextBuilder setProtocol(String value) {
        myProtocol = value;
        return this;
    }
    
    /**
     * Imposta i gestori delle credenziali dal presentare per
     * l&rsquo;autenticazione.
     * 
     * @param                       obj Vettore.
     * @return                          Oggetto per metodi concatenati.
     * @it.scoppelletti.tag.default     Implementazione di JRE.
     * @see it.scoppelletti.programmerpower.security.SecurityTools#getKeyManagers
     */
    public SSLContextBuilder setKeyManagers(KeyManager[] obj) {
        myKeyManagers = obj;
        return this;
    }
    
    /**
     * Imposta i gestori per l&rsquo;accettazione delle credenziali presentate.
     * 
     * @param                       obj Vettore.
     * @return                          Oggetto per metodi concatenati.
     * @it.scoppelletti.tag.default     Implementazione di JRE.
     * @see it.scoppelletti.programmerpower.security.SecurityTools#getTrustManagers
     */
    public SSLContextBuilder setTrustManagers(TrustManager[] obj) {
        myTrustManagers = obj;
        return this;
    }
    
    /**
     * Istanzia il contesto SSL.
     * 
     * @return Oggetto.
     */
    public SSLContext build() {
        String protocol;
        SSLContext ctx;
        
        protocol = StringUtils.isBlank(myProtocol) ?
            SSLContextBuilder.DEF_PROTOCOL : myProtocol;
        
        try {
            ctx = SSLContext.getInstance(protocol);
        } catch (NoSuchAlgorithmException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        try {
            ctx.init(myKeyManagers, myTrustManagers, null);
        } catch (KeyManagementException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        return ctx;
    }
}

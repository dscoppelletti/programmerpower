/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory di una chiave per l&rsquo;algoritmo
 * <ACRONYM TITLE="Digital Encryption Standard">DES</ACRONYM>.
 *  
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code key}</TD>
 *      <TD>Chiave di 8 byte in formato esadecimale.</TD>
 * </TR>  
 * </TBODY>
 * </TABLE></P>
 *   
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKey
 * @see   <A HREF="http://csrc.nist.gov/publications/PubsFIPSArch.html"
 *        TARGET="_top"><ACRONYM
 *        TITLE="Federal Information Processing Standards">FIPS</ACRONYM> 46-3:
 *        Data Encryption Standard (DES)</A>
 * @since 1.0.0
 */
public final class DESKeyFactory extends AbstractCryptoKeyFactory {

    /**
     * Codice dell&rsquo;algoritmo di crittografia DES. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String ALGORITHM = "DES";
    
    /**
     * Propriet&agrave; {@code key}: Valore della chiave.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_KEY = "key";
    
    /**
     * Costruttore.
     */
    public DESKeyFactory() {       
    }
    
    protected String getAlgorithm(Properties props, String prefix) {
        return DESKeyFactory.ALGORITHM;
    }
    
    protected KeyRep.Type getKeyType(Properties props, String prefix) {
        return KeyRep.Type.SECRET;
    }
    
    protected KeySpec getKeySpec(String alg, KeyRep.Type keyType,
            Properties props, String prefix) {
        String name, value;
        byte[] key;
        KeySpec keySpec;
        
        name = StringTools.concat(prefix, DESKeyFactory.PROP_KEY);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        try {
            key = Hex.decodeHex(value.toCharArray());
        } catch (DecoderException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
                
        try {
            keySpec = new DESKeySpec(key);
        } catch (InvalidKeyException ex) {
            throw SecurityTools.toSecurityException(ex);                
        }
        
        return keySpec;
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.security.*;
import java.util.*;
import org.slf4j.*;
import org.springframework.security.authentication.jaas.*;
import org.springframework.security.core.*;
import org.springframework.security.core.userdetails.*;
import it.scoppelletti.programmerpower.*;

/**
 * Collettore di elementi di autenticazione.
 */
final class PrincipalCollector {
    private static final Logger myLogger = LoggerFactory.getLogger(
            PrincipalCollector.class);
    
    private UserDetails myUser;
    private Set<GrantedAuthority> myAuthoritySet;
    
    /**
     * Costruttore.
     */
    PrincipalCollector() {     
        myAuthoritySet = new HashSet<>();
    }
    
    /**
     * Restituisce l&rsquo;eventuale utente rilevato.
     * 
     * @return Oggetto.
     */
    UserDetails getUserDetails() {
        return myUser;
    }
    
    /**
     * Restituisce le autorit&agrave; collezionate.
     * 
     * @return Collezione.
     */
    Collection<GrantedAuthority> getAuthorities() {                 
        return myAuthoritySet;
    }    
    
    /**
     * Colleziona un elemento di autenticazione.
     * 
     * @param principal Oggetto.
     */
    void addPrincipal(Principal principal) {
        if (principal == null) {
            throw new ArgumentNullException("principal");
        }
        
        if (getUserDetails(principal)) {
            return;            
        } 
        
        if (getAuthority(principal)) {
            return;
        }
                   
        myAuthoritySet.add(new JaasGrantedAuthority(principal.getName(),
                principal));
    }
    
    /**
     * Rileva l&rsquo;utente rappresentato da un elemento di autenticazione.
     * 
     * @param  principal Elemento di autenticazione.
     * @return           Indica se l&rsquo;elemento di autenticazione &egrave;
     *                   stato gestito.
     */
    private boolean getUserDetails(Principal principal) {
        UserDetails user;
        UserPrincipal userPrincipal;
        
        if (!(principal instanceof UserPrincipal)) {
            return false;            
        }
        
        userPrincipal = (UserPrincipal) principal; 
        user = userPrincipal.getUserDetails();
        
        if (user == null) {
            myLogger.warn("Found UserPrincipal with no UserDetails set.");
            return false;
        }
        
        myLogger.trace("Found UserPrincipal {}.", principal);
        if (myUser == null) {            
            myUser = user;
        } else {
            myLogger.warn("Found more than once UserPrincipal: " +
                    "using the first and ignoring the others; anyway " +
                    "collecting the authorities.");
        }
                
        myAuthoritySet.addAll(user.getAuthorities());
        
        return true;
    }
    
    /**
     * Rileva l&rsquo;autorit&agrave; rappresentata da un elemento di
     * autenticazione.
     * 
     * @param  principal Elemento di autenticazione.
     * @return           Indica se l&rsquo;elemento di autenticazione &egrave;
     *                   stato gestito.
     */    
    private boolean getAuthority(Principal principal) {
        GrantedAuthority authority;
        AuthorityPrincipal authorityPrincipal;
        
        if (!(principal instanceof AuthorityPrincipal)) {
            return false;
        }
        
        authorityPrincipal = (AuthorityPrincipal) principal;
        authority = authorityPrincipal.getAuthority();
        if (authority == null) {
            myLogger.warn("Found AuthorityPrincipal with no Authority set.");
            return false;            
        }

        myLogger.trace("Found AuthorityPrincipal {}.", principal);
        myAuthoritySet.add(authority);
        return true;
    }
}

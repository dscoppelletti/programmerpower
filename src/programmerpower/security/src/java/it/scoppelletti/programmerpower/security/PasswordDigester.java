/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

/**
 * Servizio di codifica delle password.
 * 
 * @since 1.1.0
 */
public interface PasswordDigester {

    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String BEAN_NAME =
        "it-scoppelletti-programmerpower-security-passwordDigester";
    
    /**
     * Codifica una password.
     * 
     * @param  value Valore.
     * @return       Valore codificato.
     */
    String digestPassword(SecureString value);     
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.security.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.security.core.userdetails.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Modulo di login per l&rsquo;autenticazione attraverso un&rsquo;anagrafica
 * degli utenti.
 * 
 * @since 1.1.0
 */
@Final
public class UserLoginModule extends AbstractUserLoginModule {
    private static final Logger myLogger = LoggerFactory.getLogger(
            UserLoginModule.class);          
    
    @javax.annotation.Resource(name = UserManager.BEAN_NAME)
    private UserDetailsService myUserService;
    
    @javax.annotation.Resource(name = PasswordDigester.BEAN_NAME)
    private PasswordDigester myPwdDigester;
    
    /**
     * Costruttore.
     */
    public UserLoginModule() {        
    }
        
    @Override
    protected Principal authenticate(String name, SecureString pwd) {
        String pwdCode;
        Principal principal;
        UserDetails user;
    
        if (StringUtils.isBlank(name)) {
            myLogger.warn("Argument name not set.");
            return null;
        }               
        if (ValueTools.isNullOrEmpty(pwd)) {
            myLogger.warn("Argument pwd not set.");
            return null;
        }
        
        try {
            user = myUserService.loadUserByUsername(name);  
            pwdCode = myPwdDigester.digestPassword(pwd);
          
            if (!StringTools.equals(user.getPassword(), pwdCode, false)) {
                return null;
            }               

            principal = new DefaultUserPrincipal(user);
        } catch (Exception ex) {
            myLogger.error("Method authenticate failed.", ex);
            return null;
        }              
        
        return principal;
    }    
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.KeyStore;
import java.util.Properties;

/**
 * Interfaccia di factory dei parametri per proteggere l&rsquo;accesso ad un
 * registro delle chiavi.
 * 
 * @since 1.1.0
 */
public interface ProtectionParameterFactory {

    /**
     * Istanzia un oggetto.
     * 
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     */
    KeyStore.ProtectionParameter newInstance(Properties props, String prefix); 
}

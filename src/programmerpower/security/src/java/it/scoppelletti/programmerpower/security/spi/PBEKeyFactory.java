/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory di una chiave per un algoritmo 
 * <ACRONYM TITLE="Password Based Encryption">PBE</ACRONYM>.
 * 
 * <P>Molti algoritmi di crittografia utilizzano chiavi binarie perch&eacute; le
 * persone normalmente faticano a ricordare lunghe sequenze di numeri binari
 * (anche se rappresentate in esadecimale).<BR>
 * Le password di caratteri sono molto pi&ugrave; facili da ricordare
 * perch&egrave; sono normalmente costituite da una piccola variet&agrave; di
 * caratteri. I protocolli PBE sono stati definiti per generare chiavi binarie
 * pi&ugrave; <I>sicure</I> da password di caratteri.<BR>
 * Per rallentare il lavoro di chi tenta violare le password attraverso
 * dizionari di parole comuni gi&agrave; precalcolati (<DFN>dictionary
 * attack</DFN>), la maggior parte delle implementazioni PBE utilizzano un
 * numero casuale {@code salt} che appunto incrementa la casualit&agrave; delle
 * chiavi.</P>
 * 
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code key.alg}</TD>
 *      <TD>Codice dell&rsquo;algoritmo di crittografia.</TD>
 * </TR>   
 * <TR>
 *      <TD>{@code pwd}</TD>
 *      <TD>Password dalla quale derivare la chiave.</TD>
 * <TR>
 * <TR>
 *      <TD>{@code salt}</TD>
 *      <TD>Sequenza di byte {@code salt} in formato esadecimale.</TD>
 * </TR>
 * <TR>
 *      <TD>{@code iterations}</TD>
 *      <TD>Numero di iterazioni.</TD>
 * </TR>
 * <TR>
 *      <TD>{@code keylen}</TD>
 *      <TD>Lunghezza della chiave da derivare per un cifratore a dimensione
 *      variabile della chiave.</TD>
 * </TR>
 * <TR>
 * <TR>
 *     <TD COLSPAN="2">Le propriet&agrave; {@code salt} e {@code iterations}
 *     devono essere entrambe impostate oppure entrambe non impostate; solo se
 *     sono entrambe impostate e solo per i cifratori a dimensione variabile
 *     della chiave, pu&ograve; essere impostata anche la propriet&agrave;
 *     {@code keylen}.</TD>  
 * </TR>
 * </TBODY>
 * </TABLE></P>
 *   
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKey
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;5:
 *        Password-Based Cryptography Standard</A>          
 * @see   <A HREF="http://www.ietf.org/rfc/rfc2898.txt" TARGET="_top">RFC
 *        2898 / PKCS #5: Password-Based Cryptography Specification</A>                     
 * @since 1.0.0
 */
public final class PBEKeyFactory extends AbstractCryptoKeyFactory {

    /**
     * Propriet&agrave; {@code pwd}: Password dalla quale derivare la chiave.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_PASSWORD = "pwd";
        
    /**
     * Propriet&agrave; {@code salt}: Eventuale sequenza di byte {@code salt}.
     */
    public static final String PROP_SALT = "salt";
    
    /**
     * Propriet&agrave; {@code iterations}: Eventuale numero di iterazioni.
     */
    public static final String PROP_ITERATIONCOUNT = "iterations";
    
    /**
     * Propriet&agrave; {@code keylen}: Eventuale lunghezza della chiave da
     * derivare.  
     */
    public static final String PROP_KEYLEN = "keylen";
    
    /**
     * Costruttore.
     */
    public PBEKeyFactory() {       
    }
    
    protected String getAlgorithm(Properties props, String prefix) {
        String name, alg;
        
        name = StringTools.concat(prefix, PBEKeyFactory.PROP_ALGORITHM);
        alg = props.getProperty(name);
        if (StringUtils.isBlank(alg)) {
            throw new ArgumentNullException(name);
        }        
        
        return alg;
    }
    
    protected KeyRep.Type getKeyType(Properties props, String prefix) {
        return KeyRep.Type.SECRET;
    }
    
    protected KeySpec getKeySpec(String alg, KeyRep.Type keyType,
            Properties props, String prefix) {        
        int count, keyLen;
        boolean varKeySize;
        String countName, saltName, name, value;
        byte[] salt;
        KeySpec keySpec;
        SecureString pwd = null;
        
        saltName = StringTools.concat(prefix, PBEKeyFactory.PROP_SALT);
        value = props.getProperty(saltName);
        if (!StringUtils.isBlank(value)) {
            try {
                salt = Hex.decodeHex(value.toCharArray());
            } catch (DecoderException ex) {
                throw SecurityTools.toSecurityException(ex);
            }            
        } else {
            salt = null;
        }
        
        countName = StringTools.concat(prefix, PBEKeyFactory.PROP_ITERATIONCOUNT);
        value = props.getProperty(countName);
        if (!StringUtils.isBlank(value)) {
            if (salt == null) {
                throw new ArgumentNullException(saltName);
            }
            
            count = Integer.parseInt(value);
        } else if (salt != null) {
            throw new ArgumentNullException(countName);
        } else {
            count = 0;
        }
        
        name = StringTools.concat(prefix, PBEKeyFactory.PROP_KEYLEN);
        value = props.getProperty(name);
        varKeySize = !StringUtils.isBlank(value); 
        if (varKeySize) {
            if (salt == null) {
                throw new ArgumentNullException(saltName);
            }
            
            keyLen = Integer.parseInt(value);
        } else {
            keyLen = 0;
        }
        
        name = StringTools.concat(prefix, PBEKeyFactory.PROP_PASSWORD);
        pwd = new SecureString(props.getProperty(name));
        if (pwd.isEmpty()) {
            throw new ArgumentNullException(name);
        }
        
        try {
            if (salt == null) {
                keySpec = new PBEKeySpec(pwd.getValue());
            } else if (varKeySize) {
                keySpec = new PBEKeySpec(pwd.getValue(), salt, count,
                    keyLen);
            } else {
                keySpec = new PBEKeySpec(pwd.getValue(), salt, count);            
            }
        } finally {
            if (pwd != null) {
                pwd.clear();
                pwd = null;
            }
        }
        
        return keySpec;
    }
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyRep;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;
import java.util.Properties;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.ReturnNullException;
import it.scoppelletti.programmerpower.reflect.InvalidCastException;
import it.scoppelletti.programmerpower.reflect.ReflectionException;
import it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory;
import it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory;
import it.scoppelletti.programmerpower.security.spi.EncodedKeyFactory;
import it.scoppelletti.programmerpower.security.spi.RawKeyFactory;
import it.scoppelletti.programmerpower.types.EnumTools;
import it.scoppelletti.programmerpower.types.StringTools;

/**
 * Funzioni di utilit&agrave; per la crittografia.
 * 
 * <P>La classe {@code CryptoTools} consente di istanziare i principali oggetti
 * <ACRONYM TITLE="Java Cryptography Architecture">JCA</ACRONYM> in base ai
 * parametri impostati in una collezione di propriet&agrave; {@code Properties}
 * e utilizzando un modello 
 * <ACRONYM TITLE="Service Provider Interface">SPI</ACRONYM>; questo approccio
 * consente di mantenere il codice sorgente delle procedure il pi&ugrave;
 * possibile indipendente dagli specifici algoritmi di crittografia
 * adottati.</P>
 * 
 * @since 1.1.0
 */
public final class CryptoTools {
    
    /**
     * Propriet&agrave; {@code alg}: Codice dell&rsquo;algoritmo di cifratura.
     *
     * @it.scoppelletti.tag.required
     * @see #getCipher
     * @see it.scoppelletti.programmerpower.security.spi.PasswordProtectionFactory
     */
    public static final String PROP_CIPHERALGORITHM = "alg";
    
    /**
     * Propriet&agrave; {@code mode}: Codice della modalit&agrave; di un
     * algoritmo di cifratura.
     * 
     * @it.scoppelletti.tag.required
     * @see #getCipher
     */    
    public static final String PROP_CIPHERMODE = "mode";
    
    /**
     * Propriet&agrave; {@code padding}: Codice della modalit&agrave; di
     * riempimento di un algoritmo di cifratura.
     * 
     * @it.scoppelletti.tag.required
     * @see #getCipher
     */    
    public static final String PROP_CIPHERPADDING = "padding";
    
    /**
     * Propriet&agrave; {@code op}: Codice dell&rsquo;operazione di cifratura.
     * 
     * @it.scoppelletti.tag.required
     * @see #getCipher
     * @see it.scoppelletti.programmerpower.security.CipherOperation
     */    
    public static final String PROP_CIPHEROPERATION = "op";

    /**
     * Propriet&agrave; {@code alg}: Codice dell&rsquo;algoritmo di
     * crittografia.
     * 
     * @it.scoppelletti.tag.required
     * @see #getKeyGenerator
     * @see #getKeyPairGenerator
     */
    public static final String PROP_KEYALGORITHM = "alg";
    
    /**
     * Propriet&agrave; {@code key.size}: Dimensione della chiave in numero di
     * bit.
     * 
     * @it.scoppelletti.tag.required
     * @see #getKeyGenerator
     * @see #getKeyPairGenerator 
     */
    public static final String PROP_KEYSIZE = "key.size";
        
    /**
     * Propriet&agrave; {@code key.factory}: Nome della classe che implementa
     * l&rsquo;interfaccia {@code CryptoKeyFactory}.
     * 
     * @it.scoppelletti.tag.required
     * @see #getCipher
     * @see #getKey
     * @see it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory
     */    
    public static final String PROP_KEYFACTORY = "key.factory";
    
    /**
     * Propriet&agrave; {@code key.factory.prefix}: Eventuale prefisso da
     * applicare al nome delle propriet&agrave; interrogate dal provider
     * {@code CryptoKeyFactory}.
     * 
     * @see #getCipher
     * @see #getKey
     * @see it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory
     */    
    public static final String PROP_KEYFACTORY_PREFIX = "key.factory.prefix";
    
    /**
     * Propriet&agrave; {@code param.factory}: Nome della classe che implementa
     * l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory}.
     * 
     * @it.scoppelletti.tag.required
     * @see #getCipher
     * @see #getKeyGenerator
     * @see #getKeyPairGenerator 
     * @see #getAlgorithmParameterSpec
     * @see it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory
     * @see it.scoppelletti.programmerpower.security.spi.PasswordProtectionFactory
     */    
    public static final String PROP_PARAMFACTORY = "param.factory";
    
    /**
     * Propriet&agrave; {@code param.factory.prefix}: Eventuale prefisso da
     * applicare al nome delle propriet&agrave; interrogate dal provider
     * {@code AlgorithmParameterSpecFactory}.
     * 
     * @see #getCipher
     * @see #getKeyGenerator
     * @see #getKeyPairGenerator
     * @see #getAlgorithmParameterSpec
     * @see it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory
     * @see it.scoppelletti.programmerpower.security.spi.PasswordProtectionFactory
     */    
    public static final String PROP_PARAMFACTORY_PREFIX =
        "param.factory.prefix";
    
    /**
     * Costruttore privato per classe statica.
     */
    private CryptoTools() {        
    }  
    
    /**
     * Restituisce un cifratore.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code alg}</TD>
     *      <TD>Codice dell&rsquo;algoritmo di cifratura.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code mode}</TD>
     *      <TD>Codice dalla modalit&agrave; dell&rsquo;algoritmo di
     *      cifratura.</TD>
     * </TR> 
     * <TR>
     *      <TD>{@code padding}</TD>
     *      <TD>Codice dalla modalit&agrave; di riempimento dell&rsquo;algoritmo
     *      di cifratura.</TD>
     * </TR> 
     * <TR>
     *      <TD>{@code op}</TD>
     *      <TD>Codice dell&rsquo;operazione di cifratura.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code key.factory}</TD>
     *      <TD>Nome della classe di factory della chiave; la classe deve
     *      implementare l&rsquo;interfaccia {@code CryptoKeyFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code key.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code key.factory}.</TD>          
     * </TR> 
     * <TR>
     *      <TD>{@code param.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri specifici
     *      dell&rsquo;algoritmo; la classe deve implementare
     *      l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.<BR>
     *      Se la propriet&agrave; non &egrave; impostata, sono utilizzati i
     *      parametri di default definiti dallo specifico provider JCA.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code param.factory}.</TD>          
     * </TR>    
     * <TR>
     *     <TD COLSPAN="2">Le propriet&agrave; {@code mode} e {@code padding}
     *     devono essere entrambe impostate oppure entrambe non impostate.</TD>  
     * </TR> 
     * </TBODY>
     * </TABLE></P>
     *  
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     * @see    it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory
     * @see    it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory
     * @see    <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
     *         Cryptography Architecture, Standard Algorithm Name Documentation
     *         for JDK 8</A>                    
     */
    public static Cipher getCipher(Properties props, String prefix) {
        String alg, name, mode, modeName, padding, paddingName, value;
        Key key;
        Cipher cipher;
        AlgorithmParameterSpec params;
        CipherOperation op;
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_CIPHERALGORITHM);
        alg = props.getProperty(name);
        if (StringUtils.isBlank(alg)) {
            throw new ArgumentNullException(name);
        }
    
        modeName = StringTools.concat(prefix, CryptoTools.PROP_CIPHERMODE);
        mode = props.getProperty(modeName);
        
        paddingName = StringTools.concat(prefix,
                CryptoTools.PROP_CIPHERPADDING);
        padding = props.getProperty(paddingName);
        
        if (StringUtils.isBlank(mode) && !StringUtils.isBlank(padding)) {
            throw new ArgumentNullException(modeName);
        }
        if (!StringUtils.isBlank(mode) && StringUtils.isBlank(padding)) {
            throw new ArgumentNullException(paddingName);
        }
        if (!StringUtils.isBlank(mode)) {
            alg = alg.concat("/");
            alg = alg.concat(mode);
            alg = alg.concat("/");
            alg = alg.concat(padding);
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_CIPHEROPERATION);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }                
        
        op = EnumTools.valueOf(CipherOperation.class, value);
        
        key = CryptoTools.getKey(props, prefix); 
        params = CryptoTools.getAlgorithmParameterSpec(props, prefix);
        
        try {
            cipher = Cipher.getInstance(alg);
            if (params != null) {
                cipher.init(op.toJCA(), key, params);
            } else {
                cipher.init(op.toJCA(), key);
            }
        } catch (GeneralSecurityException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        return cipher;
    }
    
    /**
     * Restituisce una chiave di crittografia.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code key.factory}</TD>
     *      <TD>Nome della classe di factory della chiave; la classe deve
     *      implementare l&rsquo;interfaccia {@code CryptoKeyFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
     * </TR> 
     * <TR>
     *      <TD>{@code key.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code key.factory}.</TD>          
     * </TR> 
     * </TBODY>
     * </TABLE></P>
     *  
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     * @see    #toProperties
     * @see    it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory
     */
    public static Key getKey(Properties props, String prefix) {
        String name, value;
        Class<?> factoryClass;
        CryptoKeyFactory factory;
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_KEYFACTORY);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        try {
            factoryClass = Class.forName(value);
            factory = (CryptoKeyFactory) factoryClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_KEYFACTORY_PREFIX);
        
        return factory.newInstance(props, props.getProperty(name));
    }
   
    /**
     * Restituisce i parametri per la ricostruzione di una chiave di
     * crittografia.
     * 
     * <P>Il metodo {@code toProperties} rileva i provider del servizio
     * {@code KeyToPropertySetProvider} disponibili e delega il calcolo dei
     * parametri al primo di questi che supporta la tipologia della chiave di
     * crittografia {@code key}; se nessuno dei provider supporta la specifica
     * tipologia di chiavi, restituisce i parametri che rappresentano la chiave
     * codificata come sequenza di byte nel formato Base64 come definito da RFC
     * 2045.</P>
     * 
     * @param  key     Chiave.
     * @param  encoded Indica se restituire i parametri che rappresentano la
     *                 chiave codificata come sequenza di byte senza rilevare
     *                 gli eventuali provider {@code KeyToPropertySetProvider}
     *                 disponibili.
     * @return         Propriet&agrave;. 
     * @see #getKey
     * @see it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory
     * @see it.scoppelletti.programmerpower.security.spi.EncodedKeyFactory
     * @see it.scoppelletti.programmerpower.security.spi.KeyToPropertySetProvider
     * @see <A HREF="http://www.ietf.org/rfc/rfc2045.txt" TARGET="_top">RFC
     *      2045: Multipurpose Internet Mail Extensions (MIME) Part One: Format
     *      of Internet Message Bodies</A> 
     */
    public static Properties toProperties(Key key, boolean encoded) {
        byte[] data;
        KeyRep.Type keyType;
        Properties props;
        KeyToPropertySetService svc;
        SecurityResources res = new SecurityResources();
        
        if (key == null) {
            throw new ArgumentNullException("key");
        }
        
        if (!encoded) {
            svc = new KeyToPropertySetService(key);
            props = svc.query();
            if (props != null) {
                return props;
            }
        }
        
        if (key instanceof PublicKey) {
            keyType = KeyRep.Type.PUBLIC;
        } else if (key instanceof PrivateKey) {
            keyType = KeyRep.Type.PRIVATE;
        } else if (key instanceof SecretKey) {
            keyType = KeyRep.Type.SECRET;
        } else {
            throw new InvalidCastException(key.getClass().getName(),
                    KeyRep.Type.class.getName());
        }
        
        data = key.getEncoded();
        if (data == null) {
            throw new SecurityException(
                    res.getEncodedFormatNotSupportedException());
        }
        
        props = new Properties();
        if (keyType == KeyRep.Type.SECRET) {
            props.setProperty(CryptoTools.PROP_KEYFACTORY, 
                    RawKeyFactory.class.getName());
        } else {
            props.setProperty(CryptoTools.PROP_KEYFACTORY, 
                    EncodedKeyFactory.class.getName());
            props.setProperty(EncodedKeyFactory.PROP_KEYTYPE, 
                    keyType.name());                      
        }
        
        props.setProperty(RawKeyFactory.PROP_ALGORITHM, key.getAlgorithm()); 
        props.setProperty(RawKeyFactory.PROP_DATA,
                Base64.getEncoder().encodeToString(data));
        
        return props;
    }
    
    /**
     * Restituisce un generatore di chiavi simmetriche.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code alg}</TD>
     *      <TD>Codice dell&rsquo;algoritmo di crittografia.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code param.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri specifici
     *      dell&rsquo;algoritmo; la classe deve implementare
     *      l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code param.factory}.</TD>          
     * </TR> 
     * <TR>
     *      <TD>{@code key.size}</TD>
     *      <TD>Dimensione della chiave (numero di bit).</TD>          
     * </TR> 
     * <TR>
     *     <TD COLSPAN="2">Le propriet&agrave; {@code param.factory} e
     *     {@code key.size} non possono essere entrambe impostate; se nessuna
     *     delle due propriet&agrave; &egrave; impostata, il generatore
     *     sar&agrave; inizializzato con i parametri di default definiti dallo
     *     specifico provider JCA.</TD>
     * </TR>
     * </TBODY>
     * </TABLE></P>
     *    
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     * @see    it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory
     * @see    <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
     *         Cryptography Architecture, Standard Algorithm Name Documentation
     *         for JDK 8</A>   
     */
    public static KeyGenerator getKeyGenerator(Properties props,
            String prefix) {
        int keySize;
        String alg, name;
        KeyGenerator gen;
        AlgorithmParameterSpec params;
        SecurityResources res = new SecurityResources();
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_KEYALGORITHM);
        alg = props.getProperty(name);
        if (StringUtils.isBlank(alg)) {
            throw new ArgumentNullException(name);
        }
        
        params = CryptoTools.getAlgorithmParameterSpec(props, prefix);
        keySize = CryptoTools.getKeySize(props, prefix);
        if (params != null && keySize >= 0) {
            throw new IllegalArgumentException(
                    res.getArgumentIncompatibilityException(StringTools.concat(
                    prefix, CryptoTools.PROP_PARAMFACTORY), StringTools.concat(
                    prefix, CryptoTools.PROP_KEYSIZE)));
        }
        
        try {
            gen = KeyGenerator.getInstance(alg);
            if (params != null) {
                gen.init(params);
            }
            if (keySize >= 0) {
                gen.init(keySize);
            }
        } catch (GeneralSecurityException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        return gen;        
    }
    
    /**
     * Restituisce un generatore di coppie di chiavi (pubblica, privata).
     *      
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code alg}</TD>
     *      <TD>Codice dell&rsquo;algoritmo di crittografia.</TD>            
     * </TR>
     * <TR>
     *      <TD>{@code param.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri specifici
     *      dell&rsquo;algoritmo; la classe deve implementare
     *      l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code param.factory}.</TD>          
     * </TR> 
     * <TR>
     *      <TD>{@code key.size}</TD>
     *      <TD>Dimensione della chiave (numero di bit).</TD>          
     * </TR> 
     * <TR>
     *     <TD COLSPAN="2">Le propriet&agrave; {@code param.factory} e
     *     {@code key.size} non possono essere entrambe impostate; se nessuna
     *     delle due propriet&agrave; &egrave; impostata, il generatore
     *     sar&agrave; inizializzato con i parametri di default definiti dallo
     *     specifico provider JCA.</TD>
     * </TR>
     * </TBODY>
     * </TABLE></P>       
     *  
     * @param  props  Propriet&agrave;.
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto.
     * @see    it.scoppelletti.programmerpower.security.spi.AlgorithmParameterSpecFactory
     * @see    <A HREF="${it.scoppelletti.token.javaseUrl}/technotes/guides/security/StandardNames.html">Java
     *         Cryptography Architecture, Standard Algorithm Name Documentation
     *         for JDK 8</A>   
     */
    public static KeyPairGenerator getKeyPairGenerator(Properties props,
            String prefix) {
        int keySize;
        String alg, name;
        KeyPairGenerator gen;
        AlgorithmParameterSpec params;
        SecurityResources res = new SecurityResources();
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_KEYALGORITHM);
        alg = props.getProperty(name);
        if (StringUtils.isBlank(alg)) {
            throw new ArgumentNullException(name);
        }
        
        params = CryptoTools.getAlgorithmParameterSpec(props, prefix);
        keySize = CryptoTools.getKeySize(props, prefix);
        if (params != null && keySize >= 0) {
            throw new IllegalArgumentException(
                    res.getArgumentIncompatibilityException(StringTools.concat(
                    prefix, CryptoTools.PROP_PARAMFACTORY), StringTools.concat(
                    prefix, CryptoTools.PROP_KEYSIZE)));
        }
        
        try {
            gen = KeyPairGenerator.getInstance(alg);
            if (params != null) {
                gen.initialize(params);
            }
            if (keySize >= 0) {
                gen.initialize(keySize);
            }
        } catch (GeneralSecurityException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        return gen;
    }      
    
    /**
     * Restituisce i parametri specifici di un algoritmo di crittografia.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code param.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri specifici
     *      dell&rsquo;algoritmo; la classe deve implementare
     *      l&rsquo;interfaccia {@code AlgorithmParameterSpecFactory} e
     *      pu&ograve; prevedere altre propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code param.factory}.</TD>          
     * </TR> 
     * </TBODY>
     * </TABLE></P>
     *  
     * @param  props  Propriet&agrave;
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Oggetto. Se la propriet&agrave; {@code param.factory} non
     *                &egrave; impostata, restituisce {@code null}. 
     */
    public static AlgorithmParameterSpec getAlgorithmParameterSpec(
            Properties props, String prefix) {
        String name, value;
        Class<?> factoryClass;
        AlgorithmParameterSpec param;
        AlgorithmParameterSpecFactory factory;
        
        name = StringTools.concat(prefix, CryptoTools.PROP_PARAMFACTORY);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            return null;
        }
        
        try {
            factoryClass = Class.forName(value);
            factory = (AlgorithmParameterSpecFactory)
                    factoryClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        name = StringTools.concat(prefix, CryptoTools.PROP_PARAMFACTORY_PREFIX);
        param = factory.newInstance(props, props.getProperty(name));
        if (param == null) {
            throw new ReturnNullException(factory.getClass().getName(), 
                    "newInstance");
        }
        
        return param;
    }
    
    /**
     * Restituisce la dimensione di una chiave.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code key.size}</TD>
     *      <TD>Dimensione della chiave (numero di bit).</TD>          
     * </TR>
     * </TBODY>
     * </TABLE></P>
     *  
     * @param  props  Propriet&agrave;
     * @param  prefix Prefisso da applicare al nome delle propriet&agrave; da
     *                interrogare. Pu&ograve; essere {@code null}.
     * @return        Valore.
     */
    private static int getKeySize(Properties props, String prefix) {
        String name, value;
        int keySize;
        
        name = StringTools.concat(prefix, CryptoTools.PROP_KEYSIZE);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            return -1;
        }
        
        keySize = Integer.parseInt(value);
        if (keySize < 0) {
            // Converto i valori negativi nel valore 0 comunque non valido per
            // distinguere il caso di proprieta' non impostata (valore negativo)
            // dal caso di proprieta' impostata con un valore non valido
            keySize = 0;
        }
        
        return keySize;
    }       
}

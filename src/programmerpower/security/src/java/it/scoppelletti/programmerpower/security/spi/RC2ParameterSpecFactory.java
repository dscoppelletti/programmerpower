/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory dell&rsquo;insieme dei parametri dell&rsquo;algoritmo
 * <ACRONYM TITLE="Ron's Code 2">RC2</ACRONYM>.
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>
 * <TR>
 *      <TD>{@code key.size}</TD>
 *      <TD>Dimensione della chiave effettiva (numero di bit).</TD>
 * </TR>      
 * <TR>
 *      <TD>{@code iv}</TD>
 *      <TD>Eventuale vettore di inizializzazione di 8 byte in formato
 *      esadecimale per la modalit&agrave; di feedback.</TD>
 * </TR>  
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getCipher
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKeyGenerator
 * @see   <A HREF="http://www.ietf.org/rfc/rfc2268.txt" TARGET="_top">RFC 2268:
 *        A Description of the RC2(r) Encryption Algorithm</A>                  
 * @since 1.0.0
 */
public final class RC2ParameterSpecFactory implements 
        AlgorithmParameterSpecFactory {

    /**
     * Codice dell&rsquo;algoritmo di crittografia RC2. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String ALGORITHM = "RC2";
    
    /**
     * Propriet&agrave; {@code key.size}: Dimensione effettiva della chiave in
     * numero di bit.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_KEYSIZE = "key.size";
    
    /** 
     * Propriet&agrave; {@code iv}: Eventuale vettore di inizializzazione.
     */
    public static final String PROP_IV = "iv";
    
    /**
     * Costruttore.
     */
    public RC2ParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        int keySize;
        String name, value;
        byte[] iv;
        AlgorithmParameterSpec param;
    
        name = StringTools.concat(prefix, RC2ParameterSpecFactory.PROP_KEYSIZE);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        keySize = Integer.parseInt(value);
        
        name = StringTools.concat(prefix, RC2ParameterSpecFactory.PROP_IV);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            iv = null;
        } else {        
            try {
                iv = Hex.decodeHex(value.toCharArray());
            } catch (DecoderException ex) {
                throw SecurityTools.toSecurityException(ex);
            }
        }
        
        if (iv != null) {
            param = new RC2ParameterSpec(keySize, iv);
        } else {
            param = new RC2ParameterSpec(keySize);
        }
        
        return param;
    }
}

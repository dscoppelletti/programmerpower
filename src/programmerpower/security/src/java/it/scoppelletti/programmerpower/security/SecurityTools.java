/*
 * Copyright (C) 2010-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.io.File;
import java.security.*;
import java.util.*;
import javax.net.ssl.*;
import javax.security.auth.DestroyFailedException;
import javax.security.auth.Destroyable;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.config.ConfigContext;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.spi.*;

/**
 * Funzioni di utilit&agrave; sulla sicurezza.
 * 
 * @since 1.1.0
 */
public final class SecurityTools {           
        
    /**
     * Nome del bean del gestore dell&rsquo;autenticazione. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_AUTHENTICATIONMANAGER =
            "it-scoppelletti-authenticationManager";
    
    /**
     * Propriet&agrave; {@code alg}: Codice del protocollo di gestione delle
     * credenziali.
     * 
     * @it.scoppelletti.tag.default Protocollo di default di JVM
     * @see                         #getKeyManagers
     * @see                         #getTrustManagers
     */
    public static final String PROP_KEYALGORITHM = "alg";
    
    /**
     * Propriet&agrave; {@code keystore.file}: Nome del file del registro delle
     * chiavi.
     * 
     * @it.scoppelletti.tag.required
     * @see                          #getKeyManagers
     * @see                          #getTrustManagers
     */
    public static final String PROP_KEYSTOREFILE = "keystore.file";
    
    /**
     * Propriet&agrave; {@code keystore.type}: Codice del formato del registro
     * delle chiavi.
     * 
     * @it.scoppelletti.tag.default Formato di default di JVM.
     * @see                         #getKeyManagers
     * @see                         #getTrustManagers
     */
    public static final String PROP_KEYSTORETYPE = "keystore.type";
    
    /**
     * Propriet&agrave; {@code param.factory}: Nome della classe che implementa
     * l&rsquo;interfaccia {@code ProtectionParameterFactory}.
     * 
     * @it.scoppelletti.tag.required
     * @see #getKeyManagers
     * @see #getTrustManagers
     * @see it.scoppelletti.programmerpower.security.spi.ProtectionParameterFactory
     */    
    public static final String PROP_PROTECTIONFACTORY = "protection.factory";
    
    /**
     * Propriet&agrave; {@code param.factory.prefix}: Eventuale prefisso da
     * applicare al nome delle propriet&agrave; interrogate dal provider
     * {@code ProtectionParameterFactory}.
     * 
     * @see #getKeyManagers
     * @see #getTrustManagers
     * @see it.scoppelletti.programmerpower.security.spi.ProtectionParameterFactory
     */    
    public static final String PROP_PROTECTIONFACTORY_PREFIX =
        "protection.factory.prefix";
    
    /**
     * Propriet&agrave; {@code key.pwd}: Password per recuperare le chiavi dal
     * registro delle chiavi.
     * 
     * @it.scoppelletti.tag.required
     * @see                          #getKeyManagers
     */
    public static final String PROP_PASSWORD = "key.pwd";
    
    /**
     * Oggetto di factory del contesto di configurazione dei gestori delle
     * credenziali da presentare per l&rsquo;autenticazione.
     * 
     * @see #getKeyManagers  
     */
    public static final ConfigContext.Factory KEYMANAGER_CONFIG_FACTORY =
            new ConfigContext.Factory(
            "it.scoppelletti.programmerpower.security.KeyManager",
            ConfigContext.MODE_USER);
    
    /**
     * Oggetto di factory del contesto di configurazione dei gestori per
     * l&rsquo;accettazione delle credenziali presentate.
     * 
     * @see #getTrustManagers  
     */
    public static final ConfigContext.Factory TRUSTMANAGER_CONFIG_FACTORY =
            new ConfigContext.Factory(
            "it.scoppelletti.programmerpower.security.TrustManager",
            ConfigContext.MODE_USER);
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            SecurityTools.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private SecurityTools() {        
    }
    
    /**
     * Restituisce uno o pi&ugrave; gestori delle credenziali da presentare per
     * l&rsquo;autenticazione.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code alg}</TD>
     *      <TD>Codice del protocollo di gestione delle credenziali.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code keystoreFile}</TD>
     *      <TD>Nome del file del registro delle chiavi.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code keystoreType}</TD>
     *      <TD>Codice del formato del registro delle chiavi.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code protection.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri per proteggere
     *      l&rsquo;accesso al registro delle chiavi; la classe deve
     *      implementare l&rsquo;interfaccia
     *      {@code ProtectionParameterSpecFactory} e pu&ograve; prevedere altre
     *      propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code protection.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code protection.factory}.</TD>          
     * </TR> 
     * <TR>
     *      <TD>{@code pwd}</TD>
     *      <TD>Password per recuperare le chiavi dal registro delle
     *      chiavi.</TD>
     * </TR>
     * </TBODY>
     * </TABLE></P>
     * 
     * @param  profile Nome del profilo di configurazione.
     * @return         Vettore. Se il profilo di configurazione non esiste,
     *                 restituisce {@code null}.
     * @see            #KEYMANAGER_CONFIG_FACTORY
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
     *      TARGET="_top">Sistema di configurazione</A>    
     */
    public static KeyManager[] getKeyManagers(String profile) {
        String alg;
        KeyStore ks;
        Properties props;
        ConfigContext cfgCtx;
        SecureString pwd = null;
        KeyManagerFactory factory;
        
        if (StringUtils.isBlank(profile)) {
            throw new ArgumentNullException("profile");
        }
        
        cfgCtx = SecurityTools.KEYMANAGER_CONFIG_FACTORY.newConfigContext();
        props = cfgCtx.getProperties(profile);
        if (props == null) {
            return null;
        }
        
        alg = props.getProperty(SecurityTools.PROP_KEYALGORITHM);
        if (StringUtils.isBlank(alg)) {
            alg = KeyManagerFactory.getDefaultAlgorithm();
        }

        pwd = new SecureString(props.getProperty(SecurityTools.PROP_PASSWORD));
        if (pwd.isEmpty()) {
            throw new ArgumentNullException(SecurityTools.PROP_PASSWORD);
        }
        
        try {    
            factory = KeyManagerFactory.getInstance(alg);
            ks = SecurityTools.getKeyStore(props);
            factory.init(ks, pwd.getValue());
        } catch (KeyStoreException|NoSuchAlgorithmException|
                UnrecoverableKeyException ex) {
            throw SecurityTools.toSecurityException(ex);
        } finally {
            pwd = SecurityTools.destroy(pwd);
        }
        
        return factory.getKeyManagers();
    }
    
    /**
     * Restituisce uno o pi&ugrave; gestori per l&rsquo;accettazione delle
     * credenziali presentate.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code alg}</TD>
     *      <TD>Codice del protocollo di gestione delle credenziali.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code keystoreFile}</TD>
     *      <TD>Nome del file del registro delle chiavi.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code keystoreType}</TD>
     *      <TD>Codice del formato del registro delle chiavi.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code protection.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri per proteggere
     *      l&rsquo;accesso al registro delle chiavi; la classe deve
     *      implementare l&rsquo;interfaccia
     *      {@code ProtectionParameterSpecFactory} e pu&ograve; prevedere altre
     *      propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code protection.factory}.</TD>          
     * </TR> 
     * </TBODY>
     * </TABLE></P>
     * 
     * @param  profile Nome del profilo di configurazione.
     * @return         Vettore. Se il profilo di configurazione non esiste,
     *                 restituisce {@code null}.
     * @see            #TRUSTMANAGER_CONFIG_FACTORY
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
     *      TARGET="_top">Sistema di configurazione</A>    
     */
    public static TrustManager[] getTrustManagers(String profile) {
        String alg;
        KeyStore ks;
        Properties props;
        ConfigContext cfgCtx;
        TrustManagerFactory factory;        

        if (StringUtils.isBlank(profile)) {
            throw new ArgumentNullException("profile");
        }
        
        cfgCtx = SecurityTools.TRUSTMANAGER_CONFIG_FACTORY.newConfigContext();
        props = cfgCtx.getProperties(profile);
        if (props == null) {
            return null;
        }
        
        alg = props.getProperty(SecurityTools.PROP_KEYALGORITHM);
        if (StringUtils.isBlank(alg)) {
            alg = TrustManagerFactory.getDefaultAlgorithm();
        }

        try {    
            factory = TrustManagerFactory.getInstance(alg);
            ks = SecurityTools.getKeyStore(props);
            factory.init(ks);
        } catch (KeyStoreException|NoSuchAlgorithmException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        return factory.getTrustManagers();
    }
    
    /**
     * Restituisce un registro delle chiavi.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code keystoreType}</TD>
     *      <TD>Codice del formato del registro delle chiavi.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code keystoreFile}</TD>
     *      <TD>Nome del file del registro delle chiavi.</TD>
     * </TR>
     * <TR>
     *      <TD>{@code protection.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri per proteggere
     *      l&rsquo;accesso al registro delle chiavi; la classe deve
     *      implementare l&rsquo;interfaccia
     *      {@code ProtectionParameterSpecFactory} e pu&ograve; prevedere altre
     *      propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code protection.factory}.</TD>          
     * </TR> 
     * </TBODY>
     * </TABLE></P>
     * 
     * @param  props Propriet&agrave;
     * @return       Oggetto.
     */
    private static KeyStore getKeyStore(Properties props) {
        String ksType, path;
        KeyStore ks;
        KeyStore.Builder builder;
        KeyStore.ProtectionParameter param = null;
        
        path = props.getProperty(SecurityTools.PROP_KEYSTOREFILE);
        if (StringUtils.isBlank(path)) {
            throw new ArgumentNullException(SecurityTools.PROP_KEYSTOREFILE);
        }
        
        ksType = props.getProperty(SecurityTools.PROP_KEYSTORETYPE);
        if (StringUtils.isBlank(ksType)) {
            ksType = KeyStore.getDefaultType();
        }
        
        param = SecurityTools.getProtectionParameter(props);
        if (param == null) {
            throw new ArgumentNullException(
                    SecurityTools.PROP_PROTECTIONFACTORY);
        }
        
        try {          
            builder = KeyStore.Builder.newInstance(ksType, null, new File(path),
                    param);            
            ks = builder.getKeyStore();
        } catch (KeyStoreException ex) {
            throw new SecurityException(ex);
        } finally {
            if (param instanceof Destroyable) {
                SecurityTools.destroy((Destroyable) param);
                param = null;
            }
        }
        
        return ks;
    }
    
    /**
     * Restituisce i parametri per proteggere l&rsquo;accesso ad un registro
     * delle chiavi.
     * 
     * <H4>1. Propriet&agrave;</H4>
     * 
     * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
     * <THEAD>
     * <TR>
     *     <TH>Propriet&agrave;</TH>
     *     <TH>Descrizione</TH>     
     * </TR>
     * </THEAD>
     * <TBODY>
     * <TR>
     *      <TD>{@code protection.factory}</TD>
     *      <TD>Nome della classe di factory dei parametri per proteggere
     *      l&rsquo;accesso al registro delle chiavi; la classe deve
     *      implementare l&rsquo;interfaccia
     *      {@code ProtectionParameterSpecFactory} e pu&ograve; prevedere altre
     *      propriet&agrave;.</TD>          
     * </TR>
     * <TR>
     *      <TD>{@code param.factory.prefix}</TD>
     *      <TD>Eventuale prefisso da applicare al nome delle propriet&agrave;
     *      interrogate dal provider {@code protection.factory}.</TD>          
     * </TR> 
     * </TBODY>
     * </TABLE></P>
     * 
     * @param  props Propriet&agrave;
     * @return       Oggetto. Se la propriet&agrave;
     *               {@code protection.factory} non &egrave; impostata,
     *               restituisce {@code null}.
     */
    private static KeyStore.ProtectionParameter getProtectionParameter(
            Properties props) {
        String value;
        Class<?> factoryClass;
        KeyStore.ProtectionParameter param;
        ProtectionParameterFactory factory;
        
        value = props.getProperty(SecurityTools.PROP_PROTECTIONFACTORY);
        if (StringUtils.isBlank(value)) {
            return null;
        }
        
        try {
            factoryClass = Class.forName(value);
            factory = (ProtectionParameterFactory) factoryClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        param = factory.newInstance(props, props.getProperty(
                SecurityTools.PROP_PROTECTIONFACTORY_PREFIX));
        if (param == null) {
            throw new ReturnNullException(factory.getClass().getName(), 
                    "newInstance");
        }
        
        return param;        
    }
    
    /**
     * Converte un&rsquo;eccezione relativa alla gestione della sicurezza.
     * 
     * @param  ex Eccezione originale.
     * @return    Eccezione convertita.
     */
    public static SecurityException toSecurityException(Throwable ex) {
        return new SecurityException(ApplicationException.toString(ex), ex);
    }
    
    /**
     * Rimuove i dati di un oggetto rendendolo inutilizzabile.
     * 
     * <P>Il metodo {@code destroy} rimuove i dati di un oggetto ignorando
     * l&rsquo;eventuale eccezione inoltrata (tale eccezione viene emessa come
     * segnalazione di log).<BR>
     * <P>Il valore di ritorno {@code null} pu&ograve; essere utilizzato per
     * azzerare la stessa variabile {@code obj} in modo che tale variabile
     * faccia anche da indicatore di oggetto attivo ({@code obj != null}) o con
     * dati gi&agrave; rimossi ({@code obj == null}).</P>
     *  
     * @param  obj Oggetto. Se &egrave; {@code null}, si assume che i dati siano
     *             gi&agrave; stati rimossi.
     * @return     {@code null}.
     */    
    public static <T extends Destroyable> T destroy(T obj) {
        if (obj != null) {
            if (obj.isDestroyed()) {
                return null;
            }
            
            try {
                obj.destroy();
            } catch (DestroyFailedException ex) {
                myLogger.error("Failed to destroy object.", ex);
            }
        }
        
        return null;
    }
    
    /**
     * Legge un valore da una collezione.
     * 
     * @param  key Chiave.
     * @param  map Collezione. Pu&ograve; essere {@code null}.
     * @return     Valore.
     */    
    public static SecureString getSecureString(String key, Map<String, ?> map) {
        Object obj;
        SecureString value;
        
        if (StringUtils.isBlank(key)) {
            throw new ArgumentNullException("name");
        }
        if (map == null) {
            return null;
        }
     
        obj = map.get(key);
        if (obj == null) {
            return null;
        }        
        
        if (obj instanceof SecureString) {
            value = (SecureString) obj; 
            return new SecureString(value.getValue());
        }        
        if (obj instanceof char[]) {
            return new SecureString((char[]) obj);
        }
        
        return new SecureString(obj.toString());
    }        
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.math.*;
import java.security.*;
import java.security.spec.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory di una chiave privata per l&rsquo;algoritmo
 * <ACRONYM TITLE="Rivest, Shamir, & Adleman">RSA</ACRONYM>.
 *  
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code modulus}</TD>
 *      <TD>Modulo.</TD>
 * </TR> 
 * <TR>
 *      <TD>{@code privateexponent}</TD>
 *      <TD>Esponente privato.</TD>
 * </TR> 
 * </TBODY>
 * </TABLE></P>
 *   
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKey
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;1: RSA
 *        Cryptography Standard</A>     
 * @since 1.0.0
 */
public final class RSAPrivateKeyFactory extends AbstractCryptoKeyFactory {

    /**
     * Propriet&agrave; {@code modulus}: Valore del modulo.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_MODULUS = "modulus";
    
    /**
     * Propriet&agrave; {@code privateexponent}: Valore dell&rsquo;esponente
     * privato.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_PRIVATEEXPONENT = "privateexponent";
    
    /**
     * Costruttore.
     */
    public RSAPrivateKeyFactory() {       
    }
    
    protected String getAlgorithm(Properties props, String prefix) {
        return RSAKeyGenParameterSpecFactory.ALGORITHM;
    }
    
    protected KeyRep.Type getKeyType(Properties props, String prefix) {
        return KeyRep.Type.PRIVATE;
    }
    
    protected KeySpec getKeySpec(String alg, KeyRep.Type keyType,
            Properties props, String prefix) {
        String name, value;
        BigInteger mod, privateExp;
        
        name = StringTools.concat(prefix, RSAPrivateKeyFactory.PROP_MODULUS);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }        
        mod = new BigInteger(value);
        
        name = StringTools.concat(prefix,
                RSAPrivateKeyFactory.PROP_PRIVATEEXPONENT);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        privateExp = new BigInteger(value);
                
        return new RSAPrivateKeySpec(mod, privateExp);
    }
}

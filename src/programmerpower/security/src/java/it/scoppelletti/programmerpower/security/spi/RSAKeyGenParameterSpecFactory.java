/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.math.*;
import java.security.spec.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory dell&rsquo;insieme dei parametri dell&rsquo;algoritmo
 * <ACRONYM TITLE="Rivest, Shamir & Adleman">RSA</ACRONYM> per la generazione
 * di una coppia di chiavi (pubblica, privata).
 * 
 * <H4>1. Propriet&agrave;</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>
 *     <TH>Default</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>
 * <TR>
 *      <TD>{@code key.size}</TD>
 *      <TD>Dimensione del modulo (numero di bit).</TD>
 *      <TD>1024</TD>
 * </TR>
 * <TR>
 *      <TD>{@code publicexponent}</TD>
 *      <TD>Esponente pubblico.</TD>
 *      <TD>65537</TD>
 * </TR>      
 * </TBODY>
 * </TABLE></P>
 *  
 * @see it.scoppelletti.programmerpower.security.CryptoTools#getKeyPairGenerator
 * @see <A HREF="http://www.rsa.com/rsalabs" TARGET="_top">PKCS &#35;1: RSA
 *      Cryptography Standard</A>  
 * @since 1.0.0 
 */
public final class RSAKeyGenParameterSpecFactory implements 
        AlgorithmParameterSpecFactory {
    private static final int DEFAULT_KEYSIZE = 1024;
    
    /**
     * Codice dell&rsquo;algoritmo di crittografia RSA. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String ALGORITHM = "RSA";
    
    /**
     * Propriet&agrave; {@code key.size}: Eventuale dimensione del modulo in
     * numero di bit.  
     */
    public static final String PROP_KEYSIZE = "key.size";
        
    /**
     * Propriet&agrave; {@code publicexponent}: Eventuale valore
     * dell&rsquo;esponente pubblico.
     */
    public static final String PROP_PUBLICEXPONENT = "publicexponent";
    
    /**
     * Costruttore.
     */
    public RSAKeyGenParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        int keySize;        
        String name, value;
        BigInteger publicExp;
        
        if (props == null) {
            throw new ArgumentNullException("props");
        }
        
        name = StringTools.concat(prefix,
                RSAKeyGenParameterSpecFactory.PROP_KEYSIZE);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            keySize = RSAKeyGenParameterSpecFactory.DEFAULT_KEYSIZE;
        } else {            
            keySize = Integer.parseInt(value);
        }
        
        name = StringTools.concat(prefix,
                RSAKeyGenParameterSpecFactory.PROP_PUBLICEXPONENT);        
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            publicExp = RSAKeyGenParameterSpec.F4;
        } else {
            publicExp = new BigInteger(value);
        }
        
        return new RSAKeyGenParameterSpec(keySize, publicExp);
    }
}

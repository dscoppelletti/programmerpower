/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.scoppelletti.programmerpower.security;

import java.util.*;
import org.apache.commons.collections4.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.security.access.hierarchicalroles.*;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Implementazione di default della gerarchia dei ruoli.
 * 
 * @since 1.1.0
 */
@Final
@Reserved
public class DefaultRoleHierarchy implements RoleHierarchy {
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultRoleHierarchy.class);
    
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private RoleManager myRoleMgr;
    
    /**
     * Costruttore.
     */
    public DefaultRoleHierarchy() {
    }
    
    /**
     * Completa una collezione di autorit&agrave; assegnate esplicitamente con
     * tutte le corrispondenti autorit&agrave; assegnate implicitamente.
     * 
     * @param  authorities Collezione delle autorit&agrave; assegnate
     *                     esplicitamente.
     * @return             Collezione delle autorit&agrave; assegnate sia
     *                     esplicitamente che implicitamente.
     */
    public Collection<? extends GrantedAuthority>
        getReachableGrantedAuthorities(Collection<? extends GrantedAuthority>
        authorities) {
        String code;
        List<Role> roleList;
        List<GrantedAuthority> authorityList;
        Set<GrantedAuthority> authoritySet;
                
        if (CollectionUtils.isEmpty(authorities)) {
            myLogger.trace("No role granted: granting role {}.",
                    RoleManager.ROLE_USER);
            return Collections.<GrantedAuthority>singleton(
                    RoleManager.AUTHORITY_USER);
        }
        
        authoritySet = new HashSet<GrantedAuthority>();        
        if (authorities.contains(RoleManager.AUTHORITY_ADMIN)) {
            myLogger.debug("Role {} granted: granting all roles.",
                    RoleManager.ROLE_ADMIN);
            
            roleList = myRoleMgr.listRoles();
            if (CollectionUtils.isEmpty(roleList)) {
                myLogger.warn("No role defined.");
                authoritySet.addAll(authorities);
            } else {                        
                for (Role role : roleList) {
                    code = StringUtils.lowerCase(role.getCode());
                    authoritySet.add(new SimpleGrantedAuthority(code));
                }
         
                // - Database Schema Updater
                // All'inizializzazione di un nuovo database o comunque
                // all'aggiornamento dalle prime versioni del database, il 
                // ruolo di amministratore potrebbe non essere definito.
                if (authoritySet.add(RoleManager.AUTHORITY_ADMIN)) {
                    myLogger.warn("Role {} not defined: granting it anyway.",
                            RoleManager.ROLE_ADMIN);                    
                }
            }
        } else {
            authoritySet.addAll(authorities);
        }
        
        if (authoritySet.add(RoleManager.AUTHORITY_USER)) {
            myLogger.trace("Role {} not granted: granting it.",
                    RoleManager.ROLE_USER);
        }
        
        authorityList = new ArrayList<>(authoritySet);
        Collections.sort(authorityList,
                GrantedAuthorityComparator.getInstance());
        return Collections.unmodifiableList(authorityList);
    }           
}

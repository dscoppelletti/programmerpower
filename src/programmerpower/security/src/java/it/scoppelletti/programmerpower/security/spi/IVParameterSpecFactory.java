/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.util.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di factory del vettore di inizializzazione {@code IV} per una
 * modalit&agrave; di feedback. 
 *
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>     
 * <TR>
 *      <TD>{@code iv}</TD>
 *      <TD>Vettore di inizializzazione in formato esadecimale.</TD>
 * </TR>  
 * </TBODY>
 * </TABLE></P>
 * 
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getCipher
 * @since 1.0.0
 */
public final class IVParameterSpecFactory implements 
        AlgorithmParameterSpecFactory {

    /**
     * Propriet&agrave; {@code iv}: Vettore di inizializzazione.
     * 
     * @it.scoppelletti.tag.required
     */
    public static final String PROP_IV = "iv";
    
    /**
     * Costruttore.
     */
    public IVParameterSpecFactory() {        
    }
    
    public AlgorithmParameterSpec newInstance(Properties props, String prefix) {
        String name, value;
        byte[] iv;
        AlgorithmParameterSpec param;
        
        name = StringTools.concat(prefix, IVParameterSpecFactory.PROP_IV);
        value = props.getProperty(name);
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException(name);
        }
        
        try {
            iv = Hex.decodeHex(value.toCharArray());
        } catch (DecoderException ex) {
            throw SecurityTools.toSecurityException(ex);
        }
        
        param = new IvParameterSpec(iv);
        
        return param;
    }
}

/*
 * Copyright (C) 2010-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse per la gestione della sicurezza
 * 
 * @since 1.0.0
 */
public final class SecurityResources extends ResourceWrapper {
    
    /**
     * Costruttore.
     */
    public SecurityResources() {
    }
    
    /**
     * Eccezione per login fallito.
     * 
     * @return Testo.
     */
    public String getFailedLoginException() {
        return getString("FailedLoginException");
    }  
    
    /**
     * Eccezione per utente non esistente.
     * 
     * @param  code Codice dell&rsquo;utente.
     * @return      Testo.
     * @since       1.1.0
     */
    public String getUserNotFoundException(String code) {
        return format("UserNotFoundException", code);
    }

    /**
     * Eccezione per ruolo non esistente.
     * 
     * @param  code Codice del ruolo.
     * @return      Testo.
     * @since       1.1.0
     */
    public String getRoleNotFoundException(String code) {
        return format("RoleNotFoundException", code);
    }
    
    /**
     * Richiesta del nome dell&rsquo;utente.
     * 
     * @return Testo.
     * @since  1.1.0
     */
    public String getNamePrompt() {
        return getString("NamePrompt");
    }  
    
    /**
     * Richiesta della password.
     * 
     * @return Testo.
     * @since  1.1.0
     */
    public String getPwdPrompt() {
        return getString("PwdPrompt");
    }  
    
    /**
     * Eccezione per coppia di parametri incompatibili.
     * 
     * @param  paramName1 Nome del primo parametro.
     * @param  paramName2 Nome del secondo parametro.
     * @return            Testo.
     */
    String getArgumentIncompatibilityException(String paramName1,
            String paramName2) {        
        return format("ArgumentIncompatibiltyException", paramName1,
                paramName2);
    }
    
    /**
     * Eccezione per formato codificato non supportato.
     * 
     * @return Testo.
     */
    String getEncodedFormatNotSupportedException() {
       return getString("EncodedFormatNotSupportedException"); 
    }
}

/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.spi;

import java.security.*;
import java.util.*;

/**
 * Interfaccia del servizio per la rappresentazione di una chiave di
 * crittografia con una collezione di parametri che consente di ricostruire la
 * stessa chiave.
 * 
 * <P>I provider del servizio {@code KeyToPropertySetProvider} possono essere
 * distribuiti secondo le specifiche
 * <ACRONYM TITLE="Service Provider Interface">SPI</ACRONYM> dei file 
 * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM>: in particolare nella cartella
 * {@code META-INF/services} di ogni modulo JAR pu&ograve; essere inserito un
 * file di testo 
 * {@code it.scoppelletti.programmerpower.security.spi.KeyToPropertySetProvider}
 * nel quale ogni linea riporta il nome di una delle classi che implementano
 * l&rsquo;interfaccia {@code KeyToPropertySetProvider} e distribuite con il
 * modulo JAR.</P>
 * 
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#getKey
 * @see   it.scoppelletti.programmerpower.security.CryptoTools#toProperties
 * @see   it.scoppelletti.programmerpower.security.spi.CryptoKeyFactory
 * @since 1.0.0
 */
public interface KeyToPropertySetProvider {

    /**
     * Restituisce i parametri per la ricostruzione di una chiave di
     * crittografia.
     * 
     * @param  key Chiave.
     * @return     Propriet&agrave;. Se il provider non supporta la tipologia
     *             della chiave di crittografia {@code key}, restituisce 
     *             {@code null}.
     */    
    Properties toProperties(Key key);
}

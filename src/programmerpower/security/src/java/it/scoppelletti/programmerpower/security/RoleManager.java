/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security;

import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Gestore dei ruoli.
 * 
 * @see   it.scoppelletti.programmerpower.security.Role
 * @since 1.0.0
 */
public interface RoleManager {

    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String BEAN_NAME =
            "it-scoppelletti-programmerpower-security-roleManager";
    
    /**
     * Ruolo {@code it.scoppelletti.admin}: Amministratore.
     * 
     *  @since 1.1.0
     */
    public static final String ROLE_ADMIN = "it.scoppelletti.admin";
    
    /**
     * Ruolo {@code it.scoppelletti.user}: Utente.
     * 
     *  @since 1.1.0
     */
    public static final String ROLE_USER = "it.scoppelletti.user";
    
    /**
     * Autorit&agrave; di amministratore.
     * 
     *  @since 1.1.0
     */
    public static final GrantedAuthority AUTHORITY_ADMIN =
            new SimpleGrantedAuthority(RoleManager.ROLE_ADMIN);
    
    /**
     * Autorit&agrave; di utente.
     * 
     * @since 1.1.0
     */
    public static final GrantedAuthority AUTHORITY_USER =
            new SimpleGrantedAuthority(RoleManager.ROLE_USER);
    
    /**
     * Istanzia un ruolo.
     * 
     * @param  code Codice del ruolo.
     * @return      Oggetto.
     * @since       1.1.0
     */
    Role newRole(String code);
    
    /**
     * Legge un ruolo.
     * 
     * @param  roleId Id&#46; del ruolo.
     * @return        Oggetto. Se il ruolo non esiste, restituisce {@code null}.
     * @since         1.1.0
     */
    Role loadRole(int roleId);
       
    /**
     * Legge un ruolo.
     * 
     * @param  code Codice del ruolo.
     * @return      Oggetto. Se il ruolo non esiste, restituisce {@code null}.
     * @since       1.1.0
     */    
    Role loadRole(String code);
    
    /**
     * Crea un ruolo.
     * 
     * @param obj Oggetto.
     * @since     1.1.0
     */
    void saveRole(Role obj);
       
    /**
     * Aggiorna un ruolo.
     * 
     * @param  obj Oggetto.
     * @return     Oggetto aggiornato.
     * @since      1.1.0
     */
    Role updateRole(Role obj);
    
    /**
     * Cancella un ruolo.
     * 
     * @param obj Oggetto.
     * @since     1.1.0
     */
    void deleteRole(Role obj);
        
    /**
     * Elenco dei ruoli.
     * 
     * @return Collezione.
     * @since  1.1.0
     */
    List<Role> listRoles(); 
}

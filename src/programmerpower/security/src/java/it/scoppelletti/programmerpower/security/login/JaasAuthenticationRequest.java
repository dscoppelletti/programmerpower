/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.security.login;

import java.io.*;
import java.util.*;
import org.springframework.security.core.*;
import it.scoppelletti.programmerpower.*;

/**
 * Richiesta di autenticazione <ACRONYM
 * TITLE="Java Authentication and Authorization Service">JAAS</ACRONYM>.
 * 
 * @see it.scoppelletti.programmerpower.security.login.JaasAuthenticationProvider
 * @since 1.1.0
 */
public final class JaasAuthenticationRequest implements Authentication {
    private static final long serialVersionUID = 1L;
    
    private final transient AppConfiguration myAppCfg;
    
    /**
     * Costruttore.
     * 
     * @param appCfg Configurazione del contesto di login.
     */
    public JaasAuthenticationRequest(AppConfiguration appCfg) {
        if (appCfg == null) {
            throw new ArgumentNullException("appCfg");
        }
        
        myAppCfg = appCfg;
    }
    
    /**
     * Serializza l&rsquo;oggetto.
     * 
     * @param      out Flusso di scrittura.
     * @serialData     Questa classe non &egrave; serializzabile.    
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        throw new NotSerializableException(getClass().getName());
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param in Flusso di lettura.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        throw new NotSerializableException(getClass().getName());
    }  
    
    /**
     * Restituisce il nome del soggetto da autenticare.
     * 
     * <P>Questa implementazione del metodo {@code getPrincipal} restituisce il
     * nome completo della classe {@code JaasAuthenticationRequest}.</P>
     * 
     * @return Valore.
     */
    public String getName() {
        return getClass().getName();
    }

    /**
     * Restituisce il soggetto da autenticare.
     * 
     * <P>Questa implementazione del metodo {@code getPrincipal} restituisce il
     * nome completo della classe {@code JaasAuthenticationRequest}.</P>
     * 
     * @return Oggetto.
     */    
    public Object getPrincipal() {
        return getClass().getName();
    }
    
    /**
     * Restituisce le credenziali di autenticazione.
     * 
     * <P>Questa implementazione del metodo {@code getCredentials} restituice
     * la configurazione del contesto di login.</P>
     * 
     * @return Oggetto.
     */
    public Object getCredentials() {
        return myAppCfg;
    }
 
    /**
     * Restituisce gli eventuali ulteriori dati della richiesta.
     * 
     * @return Oggetto ({@code null}).
     */
    public Object getDetails() {
        return null;
    }

    /**
     * Restituisce le autorit&agrave; assegnate all&rsquo;elemento di
     * autenticazione.
     * 
     * @return Collezione vuota.
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.<GrantedAuthority>emptyList();
    }
    
    /**
     * Indica se l&rsquo;autenticazione &egrave; avvenuta.
     * 
     * @return Indicatore {@code false}.
     */
    public boolean isAuthenticated() {
        return false;
    }

    /**
     * Imposta l&rsquo;indicatore di autenticazione avvenuta.
     * 
     * @param value Valore.
     */
    public void setAuthenticated(boolean value) throws
            IllegalArgumentException {
        if (value) {
            throw new ArgumentOutOfRangeException("value", value,
                    Boolean.FALSE);
        }
    }
}

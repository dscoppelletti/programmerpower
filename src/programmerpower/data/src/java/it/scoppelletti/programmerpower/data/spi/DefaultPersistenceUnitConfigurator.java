/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data.spi;

import java.util.*;
import org.springframework.orm.jpa.persistenceunit.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Implementazione di default dell&rsquo;interfaccia
 * {@code PersistenceUnitConfigurator}.
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;beans ... &gt;
 *     ...
 *     &lt;bean prototype=&quot;true&quot;
 * class=&quot;it.scoppelletti.programmerpower.data.spi.DefaultPersistenceUnitConfigurator&quot;&gt;
 *         &lt;property name=&quot;classes&quot;&gt;
 *             &lt;list&gt;
 *                 &lt;value&gt;it.scoppelletti.billiard.Calendar&lt;/value&gt;             
 *                 &lt;value&gt;it.scoppelletti.billiard.Player&lt;/value&gt;
 *                 &lt;value&gt;...&lt;/value&gt;
 *             &lt;/list&gt;
 *         &lt;/property&gt;         
 *     &lt;/bean&gt;           
 *     ...        
 * &lt;/beans&gt;
 * </PRE></BLOCKQUOTE>
 *
 * @see it.scoppelletti.programmerpower.data.PersistenceUnitConfigurationService
 * @since 1.0.0
 */
@Final
public class DefaultPersistenceUnitConfigurator implements 
    PersistenceUnitConfigurator {
    private List<String> myClasses;
    
    /**
     * Costruttore.
     */
    public DefaultPersistenceUnitConfigurator() {        
    }
    
    /**
     * Imposta il nome delle classi da aggiungere a quelle gestite
     * dall&rsquo;unit&agrave; di persistenza.
     * 
     * @param list Collezione. Pu&ograve; essere {@code null}.
     */
    public void setClasses(List<String> list) {
        myClasses = list;
    }
    
    public void configure(MutablePersistenceUnitInfo info, String config) {
        if (myClasses != null) {
            for (String className : myClasses) {
                info.addManagedClassName(className);
            }
        }
    }
}

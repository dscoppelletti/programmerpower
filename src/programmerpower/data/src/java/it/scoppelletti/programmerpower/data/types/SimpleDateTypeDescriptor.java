/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data.types;

import java.text.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.hibernate.*;
import org.hibernate.type.descriptor.*;
import org.hibernate.type.descriptor.java.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Descrittore della classe {@code SimpleDate}.
 */
final class SimpleDateTypeDescriptor extends
        AbstractTypeDescriptor<SimpleDate> {
    private static final long serialVersionUID = 1L;
    private static final DateFormat myFormat = new SimpleDateFormat(
            "yyyy-MM-dd");
    private static final SimpleDateTypeDescriptor myInstance =
            new SimpleDateTypeDescriptor();
    
    /**
     * Costruttore privato per classe singleton.
     */
    private SimpleDateTypeDescriptor() {
        super(SimpleDate.class);        
    }
    
    /**
     * Restituisce l&rsquo;istanza.
     * 
     * @return Oggetto.
     */
    static SimpleDateTypeDescriptor getInstance() {
        return myInstance;
    }
    
    /**
     * Rappresenta un valore con una stringa.
     * 
     * @param  value Valore.
     * @return       Stringa.
     */
    @Override
    public String toString(SimpleDate value) {
        return String.valueOf(value);
    }

    /**
     * Converte una stringa in un valore.
     * 
     * @param  s Stringa.
     * @return   Valore.
     */
    @Override
    public SimpleDate fromString(String s) {
        Calendar cal;
        Date value;
        
        if (StringUtils.isBlank(s)) {
            return SimpleDate.NIL;
        }
        
        try {
            value = myFormat.parse(s);
        } catch (ParseException ex) {
            throw new HibernateException(String.format(
                    "Failed to parse string \"%1$s\".", s), ex);
        }
        
        cal = Calendar.getInstance();
        cal.setTime(value);        
        return SimpleDate.toSimpleDate(cal);
    }

    /**
     * Conversione tra tipi.
     * 
     * @param  value   Valore da convertire.
     * @param  options Opzioni.
     * @return         Valore convertito.
     */
    @Override
    public <T> SimpleDate wrap(T value, WrapperOptions options) {
        Calendar cal;
        
        if (value == null) {
            return SimpleDate.NIL;            
        }

        if (value instanceof SimpleDate) {
            return (SimpleDate) value;
        }        
        if (value instanceof SimpleTimestamp) {
            return ((SimpleTimestamp) value).getDate();
        }
        
        if (value instanceof Calendar) {
            cal = (Calendar) value;
        } else if (value instanceof java.util.Date) {
            cal = Calendar.getInstance();
            cal.setTime((java.util.Date) value);
        } else {
            throw unknownWrap(value.getClass());
        }
        
        return SimpleDate.toSimpleDate(cal);
    }
    
    /**
     * Conversione tra tipi.
     * 
     * @param  value   Valore da convertire.
     * @param  type    Classe nella quale convertire il valore.
     * @param  options Opzioni.
     * @return         Valore convertito.
     */        
    @Override
    @SuppressWarnings("unchecked")
    public <T> T unwrap(SimpleDate value, Class<T> type,
            WrapperOptions options) {
        if (ValueTools.isNullOrEmpty(value)) {
            return null;
        }
        
        if (SimpleDate.class.isAssignableFrom(type)) {
            return (T) value;
        }        
        if (SimpleTimestamp.class.isAssignableFrom(type)) {
            return (T) new SimpleTimestamp(value);
        }        
        if (Calendar.class.isAssignableFrom(type)) {
            return (T) value.toCalendar();
        }
        if (java.sql.Date.class.isAssignableFrom(type)) {
            return (T) new java.sql.Date(value.toCalendar().getTimeInMillis());
        }
        if (java.sql.Time.class.isAssignableFrom(type)) {
            return (T) new java.sql.Time(value.toCalendar().getTimeInMillis());
        }
        if (java.sql.Timestamp.class.isAssignableFrom(type)) {
            return (T) new java.sql.Timestamp(
                    value.toCalendar().getTimeInMillis());
        }
        if (java.util.Date.class.isAssignableFrom(type)) {
            return (T) new java.util.Date(value.toCalendar().getTimeInMillis());
        }
        
        throw unknownUnwrap(type);
    }
}

/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data;

import it.scoppelletti.programmerpower.config.ConfigContext;

/**
 * Funzioni di utilit&agrave; per lo strato di persistenza.
 * 
 * @since 1.1.0
 */
public final class DataTools {

    /**
     * Profilo da aggiungere all&rsquo;ambiente del contesto Spring per attivare
     * l&rsquo;utilizzo dello strato di persistenza. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String PROFILE = "it-scoppelletti-data";
    
    /**
     * Nome dell&rsquo;unit&agrave; di persistenza. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String PERSISTENCE_UNIT = "it-scoppelletti-ds";
    
    /**
     * Nome del bean dell&rsquo;unit&agrave; di persistenza. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_ENTITYMANAGERFACTORY =
        "it-scoppelletti-entityManagerFactory";
    
    /**
     * Oggetto di factory del contesto di configurazione dell&rsquo;unit&agrave;
     * di persistenza.
     * 
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
     *      TARGET="_top">Sistema di configurazione</A>      
     */
    public static final ConfigContext.Factory CONFIG_FACTORY =
            new ConfigContext.Factory("it.scoppelletti.programmerpower.data",
            ConfigContext.MODE_SHARED | ConfigContext.MODE_USER);
                   
    /**
     * Costruttore privato per classe statica.
     */
    private DataTools() {        
    }             
}

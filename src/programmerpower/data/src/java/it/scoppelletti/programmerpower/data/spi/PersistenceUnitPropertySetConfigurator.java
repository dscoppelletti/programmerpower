/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data.spi;

import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.orm.jpa.persistenceunit.*;
import it.scoppelletti.programmerpower.config.*;
import it.scoppelletti.programmerpower.data.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Contributo alla configurazione di un&rsquo;unit&agrave; di persistenza.
 * 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
 *        TARGET="_top">Sistema di configurazione</A>    
 * @since 1.0.0
 */
@Final
public class PersistenceUnitPropertySetConfigurator implements
    PersistenceUnitConfigurator {
    private static final Logger myLogger = LoggerFactory.getLogger(
            PersistenceUnitPropertySetConfigurator.class);
    
    /**
     * Costruttore.
     */
    public PersistenceUnitPropertySetConfigurator() {        
    }
    
    /**
     * Applica il contributo alla configurazione.
     *
     * <P>Questa implementazione del metodo {@code configure} imposta
     * sull&rsquo;unit&agrave; di persistenza {@code info} le propriet&agrave;
     * impostate sul profilo di configurazione {@code config} (sovrascrivendo
     * le propriet&agrave; eventualmente gi&agrave; impostate).</P>
     * 
     * @param info   Unit&agrave; di persistenza.
     * @param config Nome del profilo di configurazione.
     */
    public void configure(MutablePersistenceUnitInfo info, String config) {
        Properties cfgProps, jpaProps;
        ConfigContext cfgCtx;
        
        if (StringUtils.isBlank(config)) {
            return;
        }        
        
        myLogger.debug("Persistence Unit Configuration: {}.", config);                
        cfgCtx = DataTools.CONFIG_FACTORY.newConfigContext();
        cfgProps = cfgCtx.getProperties(config);
        if (cfgProps == null) {
            return;
        }
        
        jpaProps = info.getProperties();
        jpaProps.putAll(cfgProps);
        info.setProperties(jpaProps);        
    }
}

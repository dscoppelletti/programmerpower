/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data;

/**
 * Funzioni di utilit&agrave; per la generazione degli identificatori delle
 * entit&agrave; basata su tabella.
 *
 * @since 1.0.0
 */
public final class TableGeneration {

    /**
     * Nome della tabella. Il valore della costante &egrave; Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String TABLE = "data_idgen";
    
    /**
     * Nome della chiave primaria della tabella. Il valore della costante
     * &egrave; Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String PKCOLUMNNAME = "key";
    
    /**
     * Nome della colonna della tabella sulla quale &egrave; registrato il 
     * prossimo valore di identificatore estratto. Il valore della costante
     * &egrave; Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String VALUECOLUMNNAME = "next_id";
    
    /**
     * Valore iniziale degli identificatori.  Il valore della costante
     * &egrave; Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final int INITIALVALUE = 1;
    
    /**
     * Incremento tra un valore di un identificatore e il successivo.  Il valore
     * della costante &egrave; Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final int ALLOCATIONSIZE = 1;    
    
    /**
     * Costruttore privato per classe statica.
     */
    private TableGeneration() {        
    }
}

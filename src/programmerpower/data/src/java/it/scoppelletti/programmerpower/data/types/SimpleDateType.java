/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data.types;

import org.hibernate.type.*;
import org.hibernate.type.descriptor.sql.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Data.
 * 
 * @see   it.scoppelletti.programmerpower.types.SimpleDate
 * @since 1.0.0
 */  
public final class SimpleDateType extends
    AbstractSingleColumnStandardBasicType<SimpleDate> {    
    private static final long serialVersionUID = 1L;

    /**
     * Nome del tipo Hibernate. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NAME = "simpledate";
    
    /**
     * Costruttore.
     */
    public SimpleDateType() {
        super(DateTypeDescriptor.INSTANCE,
                SimpleDateTypeDescriptor.getInstance());
    }

    /**
     * Restituisce il nome del tipo Hibernate.
     * 
     * @return Valore.
     */
    @Override
    public String getName() {
        return SimpleDateType.NAME;
    }
    
    /**
     * Indica se registrare il tipo Hibernate collegandolo anche alla classe
     * Java.
     * 
     * @return Indicatore ({@code true}).
     */
    @Override
    protected boolean registerUnderJavaType() {
        return true;
    }    
}

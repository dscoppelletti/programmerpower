/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data.spi;

import org.springframework.orm.jpa.persistenceunit.*;

/**
 * Contributo alla configurazione di un&rsquo;unit&agrave; di persistenza.
 *  
 * @see it.scoppelletti.programmerpower.data.PersistenceUnitConfigurationService
 * @since 1.0.0
 */
public interface PersistenceUnitConfigurator {
    
    /**
     * Applica il contributo alla configurazione.
     * 
     * @param info   Unit&agrave; di persistenza.
     * @param config Nome del profilo di configurazione.
     */
    void configure(MutablePersistenceUnitInfo info, String config);
}

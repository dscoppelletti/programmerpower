/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.data;

import java.util.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.orm.jpa.persistenceunit.*;
import it.scoppelletti.programmerpower.data.spi.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Servizio di configurazione di un&rsquo;unit&agrave; di persistenza.
 * 
 * <P>Il servizio {@code PersistenceUnitConfigurationService} applicato ad
 * un&rsquo;unit&agrave; di persistenza rileva i bean configurati nel contesto
 * Spring che implementano l&rsquo;interfaccia
 * {@code PersistenceUnitConfigurator} e ne esegue il metodo
 * {@link it.scoppelletti.programmerpower.data.spi.PersistenceUnitConfigurator#configure}
 * che contribuisce appunto alla configurazione dell&rsquo;unit&agrave; di
 * persistenza</P>.
 * 
 * @see   it.scoppelletti.programmerpower.data.spi.PersistenceUnitConfigurator
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
 *        TARGET="_top">Sistema di configurazione</A>   
 * @since 1.0.0
 */
@Final
public class PersistenceUnitConfigurationService implements
        PersistenceUnitPostProcessor {
    private static final Logger myLogger = LoggerFactory.getLogger(
            PersistenceUnitConfigurationService.class);
    private String myConfig;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     */
    public PersistenceUnitConfigurationService() {        
    }
    
    /**
     * Imposta il nome del profilo di configurazione.
     * 
     * @param value Valore.
     */
    public void setConfig(String value) {
        myConfig = value;
    }
    
    /**
     * Esegue l&rsquo;operazione.
     * 
     * @param info Unit&agrave; di persistenza.
     */
    public void postProcessPersistenceUnitInfo(
            MutablePersistenceUnitInfo info) {
        Map<String, PersistenceUnitConfigurator> configurators;
        
        configurators = myApplCtx.getBeansOfType(
                PersistenceUnitConfigurator.class, true, true);
        for (Map.Entry<String, PersistenceUnitConfigurator> entry :
            configurators.entrySet()) {
            myLogger.trace("Calling method configure of " +
                    "PersistenceUnitConfigurator {}.", entry.getKey());
            try {
                entry.getValue().configure(info, myConfig);
            } catch (Exception ex) {
                myLogger.error(entry.getKey(), ex);
            }
        }        
    }       
}

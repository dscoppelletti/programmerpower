/*
 * Copyright (C) 2009 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.xml;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse XML.
 */
final class XmlResources extends ResourceWrapper {

    /**
     * Costruttore.
     */
    XmlResources() {
    }

    /**
     * Eccezione per file non trovato.
     * 
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Testo.
     */
    String getBadSchemaException(String publicId, String systemId) {
        return format("BadSchemaException", publicId, systemId);
    }    
    
    /**
     * Eccezione per raccolta del testo nel contenuto degli elementi XML
     * gi&agrave; attiva.
     * 
     * @return Testo.
     */
    String getContentCollectionAlreadyActive() {
        return getString("ContentCollectionAlreadyActive");
    }
}

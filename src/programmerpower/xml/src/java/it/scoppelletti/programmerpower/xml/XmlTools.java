/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.xml;

import java.math.*;
import javax.xml.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Funzioni di utilit&agrave;
 * <ACRONYM TITLE="eXtensible Markup Language">XML</ACRONYM>.
 * 
 * @since 1.2.0
 */
public final class XmlTools {
    private static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    
    /**
     * Valore XML {@code NaN}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NaN = "NaN";
    
    /**
     * Valore XML di infinito positivo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String POSITIVE_INFINITY = "INF";
    
    /**
     * Valore XML di infinito negativo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NEGATIVE_INFINITY = "-INF";
    
    /**
     * Costruttore privato per classe statica.
     */
    private XmlTools() {        
    }
    
    /**
     * Converte una stringa XML {@code float} nel corrispondente valore Java.
     * 
     * <P>Il tipo {@code float} negli schemi XML prevede delle stringhe
     * differenti rispetto al tipo Java {@code Float} per rappresentare i valori
     * di infinito positivo, negativo e {@code NaN}, quindi il metodo
     * {@code parseFloat} del tipo Java {@code Float} non &egrave; adatto a
     * convertire queste stringhe.</P>
     * 
     * @param  s Stringa da convertire.
     * @return   Valore. Se la stringa {@code s} &egrave; {@code null} oppure
     *           &egrave; vuota, restituisce {@code Float.NaN}.
     * @throws   java.lang.NumberFormatException
     */
    public static float parseFloat(String s) {
        if (s == null) {
            return Float.NaN;
        }
        
        s = s.trim();
        if (s.equalsIgnoreCase(XmlTools.POSITIVE_INFINITY)) {
            return Float.POSITIVE_INFINITY;
        } else if (s.equalsIgnoreCase(XmlTools.NEGATIVE_INFINITY)) {
            return Float.NEGATIVE_INFINITY;
        } else if (s.equalsIgnoreCase(XmlTools.NaN) || s.isEmpty()) {
            return Float.NaN;
        } 
        
        return parseFloat(s);        
    }
    
    /**
     * Converte una stringa XML {@code double} nel corrispondente valore Java.
     * 
     * <P>Il tipo {@code double} negli schemi XML prevede delle stringhe
     * differenti rispetto al tipo Java {@code Double} per rappresentare i
     * valori di infinito positivo, negativo e {@code NaN}, quindi il metodo
     * {@code parseDouble} del tipo Java {@code Double} non &egrave; adatto a
     * convertire queste stringhe.</P>
     * 
     * @param  s Stringa da convertire.
     * @return   Valore. Se la stringa {@code s} &egrave; {@code null} oppure
     *           &egrave; vuota, restituisce {@code Double.NaN}.
     * @throws   java.lang.NumberFormatException
     */
    public static double parseDouble(String s) {
        if (s == null) {
            return Double.NaN;
        }
        
        s = s.trim();
        if (s.equalsIgnoreCase(XmlTools.POSITIVE_INFINITY)) {
            return Double.POSITIVE_INFINITY;
        } else if (s.equalsIgnoreCase(XmlTools.NEGATIVE_INFINITY)) {
            return Double.NEGATIVE_INFINITY;
        } else if (s.equalsIgnoreCase(XmlTools.NaN) || s.isEmpty()) {
            return Double.NaN;
        } 
        
        return parseDouble(s);        
    }
        
    /**
     * Converte una stringa XML {@code boolean} nel corrispondente valore Java.
     *  
     * <P>Il tipo {@code boolean} negli schemi XML ammette non solo la coppia di
     * costanti {@code "true"} e {@code "false"} ma anche la coppia di costanti
     * {@code "1"} e {@code "0"}, quindi il metodo
     * {@code parseBoolean} del tipo Java {@code Boolean} non &egrave; adatto a
     * convertire queste stringhe perch&eacute; restituisce {@code true} se e
     * solo se la stringa da convertire &egrave; {@code "true"}.<BR>
     * Questa versione del metodo {@code parseBoolean} restituisce {@code true}
     * se e solo se la stringa da convertire &egrave; {@code "true"} oppure
     * {@code "1"}; il metodo ignora i caratteri {@code blank}
     * all&rsquo;inizio o al fondo della stringa e non distingue tra caratteri
     * maiuscoli e minuscoli.</P>  
     * 
     * @param  s Stringa da convertire.
     * @return   Valore. Se la stringa {@code s} &egrave; {@code null},
     *           restituisce {@code false}.
     */
    public static boolean parseBoolean(String s) {
        if (s == null) {
            return false;
        }
        
        s = s.trim().toLowerCase();
        if (s.equals("true") || s.equals("1")) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Converte una stringa XML nel corrispondente valore che pu&ograve; essere
     * assegnato ad un variabile di un certo tipo.
     * 
     * <P>Il metodo {@code parseObject} supporta la conversione delle stringhe
     * nei seguenti tipi di valore:</P>
     * 
     * <UL>
     * <LI>{@code String}
     * <LI>{@code Integer}
     * <LI>{@code Long}
     * <LI>{@link #parseFloat Float}
     * <LI>{@link #parseDouble Double}
     * <LI>{@code BigDecimal}
     * <LI>{@link #parseBoolean Boolean}
     * </UL>
     * 
     * @param  s      Stringa da convertire.
     * @param  type   Tipo della variabile alla quale si intende assegnare il
     *                valore.
     * @return Valore.
     * @throws java.lang.NumberFormatException
     * @throws it.scoppelletti.programmerpower.reflect.InvalidCastException
     *         Tipo di valore nel quale convertire la stringa non supportato.
     */
    public static Object parseObject(String s, Class<?> type) {
        Object value;
        
        if (type == null) {
            throw new ArgumentNullException("type");
        }
        
        if (String.class.isAssignableFrom(type)) {
            value = s;
        } else if (Integer.TYPE.isAssignableFrom(type)) {
            if (s != null) {
                s = s.trim();
            }
            
            value = new Integer(s.trim());
        } else if (Long.TYPE.isAssignableFrom(type)) {
            if (s != null) {
                s = s.trim();
            }
            
            value = new Long(s.trim());
        } else if (Float.TYPE.isAssignableFrom(type)) {
            value = new Float(XmlTools.parseFloat(s));
        } else if (Double.TYPE.isAssignableFrom(type)) {
            value = new Double(XmlTools.parseDouble(s));            
        } else if (BigDecimal.class.isAssignableFrom(type)) {
            if (s != null) {
                s = s.trim();
            }
            
            value = new BigDecimal(s);
        } else if (Boolean.TYPE.isAssignableFrom(type)) {
            value = new Boolean(XmlTools.parseBoolean(s));
        } else {
            throw new InvalidCastException(String.class.getName(),
                    type.getName());
        }
        
        return value;
    }
    
    /**
     * Istanzia un parser SAX.
     * 
     * <P>Il metodo {@code newSAXParser} configura il parser SAX restituito
     * con le seguenti caratteristiche:</P>
     * 
     * <OL>
     * <LI>Supporto degli spazi dei nomi.
     * <LI>Validazione rispetto allo schema XML riferito dall&rsquo;attributo
     * {@code schemaLocation} (o {@code noNamespaceSchemaLocation})
     * dell&rsquo;elemento radice del documento. 
     * </OL>
     * 
     * @return Oggetto.
     */
    public static SAXParser newSAXParser() {
        SAXParserFactory saxFactory;
        SAXParser saxParser;
        
        saxFactory = SAXParserFactory.newInstance();
        saxFactory.setNamespaceAware(true);
        saxFactory.setValidating(true);
        
        try {
            saxParser = saxFactory.newSAXParser();            
            saxParser.setProperty(XmlTools.JAXP_SCHEMA_LANGUAGE,
                    XMLConstants.W3C_XML_SCHEMA_NS_URI);            
        } catch (ParserConfigurationException ex) {
            throw new XmlException(ex);
        } catch (SAXException ex) {
            throw new XmlException(ex);
        }        
                
        return saxParser;        
    }

    /**
     * Rappresenta il valore corrente di un localizzatore SAX con una stringa.
     * 
     * @param  locator Localizzatore. Pu&ograve; essere {@code null}.
     * @return         Stringa. Se il localizzatore {@code locator} &egrave;
     *                 {@code null}, restituisce la stringa {@code "null"}.
     */
    public static String toString(Locator locator) {
        String s;
        
        if (locator == null) {
            return "null";
        }
        
        s = String.format(
                "[publicId=%1$s, systemId=%2$s, line=%3$d, column=%4$d]",
                locator.getPublicId(), locator.getSystemId(),
                locator.getLineNumber(), locator.getColumnNumber());
        
        return s;
    }
}

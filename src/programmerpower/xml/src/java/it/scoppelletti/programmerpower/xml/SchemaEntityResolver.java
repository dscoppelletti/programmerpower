/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.xml;

import java.io.*;
import org.apache.commons.lang3.*;
import org.xml.sax.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Risoluzione degli schemi
 * <ACRONYM TITLE="eXtensible Markup Language">XML</ACRONYM>.
 * 
 * <P>Le applicazioni Programmer Power normalmente validano i documenti XML
 * attraverso degli schemi XML, e tali schemi sono distributi come risorse
 * incluse in moduli <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> o comunque da
 * ricercare attraverso il class-path.</P>
 * 
 * <P>In generale, ogni funzione che tratta dei documenti XML li deve validare
 * attraverso degli schemi specifici prestabiliti e quindi non sembrerebbe
 * necessario che ogni documento riporti il riferimento al proprio schema di
 * validazione. Gli schemi di validazione (come qualsiasi altro componente
 * software) sono tuttavia soggetti a modifiche correttive o evolutive da una
 * versione all&rsquo;altra delle applicazioni, ma &egrave; auspicabile che le
 * applicazioni continuino a poter trattare anche i documenti XML conformi alle
 * versioni precedenti degli schemi di validazione.<BR>
 * Per quanto detto, i nomi degli schemi XML utilizzati dalle applicazioni
 * Programmer Power indicano anche il numero di versione dello schema e i
 * documenti XML trattati devono riportare il riferimento allo schema
 * nell&rsquo;attributo {@code schemaLocation} (o
 * {@code noNamespaceSchemaLocation}) dell&rsquo;elemento radice in modo che
 * l&rsquo;applicazione possa riconoscere anche il numero di versione dello
 * schema applicato.<BR>
 * Specificare il riferimento allo schema di validazione nei documenti XML  
 * pu&ograve; anche tornare comodo con quegli editor XML e quegli
 * <ACRONYM TITLE="Integrated Development Environment">IDE</ACRONYM> che
 * prevedono la possibilit&agrave; di rimappare i riferimenti agli schemi di
 * validazione a file <ACRONYM TITLE="XML Schema Definition">XSD</ACRONYM>
 * installati in locale per attivare varie funzionalit&agrave; di validazione e
 * auto-completamento del codice XML.</P>
 * 
 * <P>La classe {@code SchemaEntityResolver} implementa l&rsquo;interfaccia
 * {@code EntityResolver} per risolvere gli 
 * <ACRONYM TITLE="Uniform Resource Identifier">URI</ACRONYM> degli schemi di
 * validazione XML come risorse rilevate attraverso il class-path, ma solo se
 * corrispondono ad una delle versioni previste dello specifico schema 
 * richiesto dall&rsquo;applicazione.</P>
 * 
 * @since 1.0.0
 */
public class SchemaEntityResolver implements EntityResolver {
    
    /**
     * Estensione dei file di schema XML. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String SCHEMA_EXT = ".xsd";
    
    /**
     * Prefisso per il riconoscimento degli URI degli schemi XML utilizzati
     * dalle applicazioni Programmer Power. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #SchemaEntityResolver(String, int, int)
     */
    public static final String URI_PREFIX = "http://www.scoppelletti.it/res/";
    
    /**
     * Prefisso per la ricerca delle risorse corrispondenti agli schemi XML
     * utilizzati dalle applicazioni Programmer Power. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #SchemaEntityResolver(String, int, int)
     */
    public static final String RESOURCE_PREFIX = "it/scoppelletti/";
    
    private final String myPath;
    private final int myVerMin;
    private final int myVerMax;    
    private final String myUriPrefix;
    private final String myResPrefix;
    
    /**
     * Costruttore. 
     * 
     * <H4>1. Risconoscimento degli schemi</H4>
     * 
     * <OL>
     * <LI>L&rsquo;URI deve concidere al percorso {@code path} seguito dal
     * numero di versione (compreso tra gli estremi {@code versionMin} e
     * {@code versionMax}) e dall&rsquo;estensione {@code .xsd}.<BR>
     * <LI>Normalmente gli URI includono anche la notazione del protocollo e del
     * dominio che li rendono non utilizzabili come nomi di risorse da ricercare
     * attraverso il class-path; per questo motivo, &egrave; possibile
     * specificare separatamente il percorso {@code path} della risorsa ed un
     * prefisso {@code uriPrefix} che sar&agrave; utilizzato per riconoscere gli
     * URI corretti, ma che sar&agrave; soppresso dallo stesso URI per ricercare
     * la corrispondente risorsa.
     * <LI>Nel costruttore &egrave; infine possibile specificare un prefisso
     * {@code resourcePrefix} da applicare al percorso (o da sostituire a quello
     * soppresso, se specificato) per completare il nome della risorsa da
     * ricercare
     * </OL>
     *  
     * <P>Ad esempio, specificando il percorso {@code runtime/module}, un
     * intervallo di versioni tra 1 e 5, il prefisso per il riconoscimento degli
     * URI {@code http://www.scoppelletti.it/res/} e il prefisso per la ricerca
     * della risorsa {@code it/scoppelletti/}, un esempio di URI riconosciuto
     * come corretto potrebbe essere 
     * {@code http://www.scoppelletti.it/res/runtime/module2.xsd} e la
     * corrispondente risorsa ricercata sarebbe
     * {@code it/scoppelletti/runtime/module2.xsd}.</P>
     *    
     * @param path           Percorso dello schema.
     * @param versionMin     Numero di versione minimo.
     * @param versionMax     Numero di versione massimo.
     * @param uriPrefix      Prefisso per il riconoscimento degli URI.
     *                       Pu&ograve; essere {@code null}.
     * @param resourcePrefix Prefisso per la ricerca della risorsa. Pu&ograve;
     *                       essere {@code null}.
     */
    public SchemaEntityResolver(String path, int versionMin, int versionMax,
            String uriPrefix, String resourcePrefix) {
        if (StringUtils.isBlank(uriPrefix)) {
            throw new ArgumentNullException("uriPrefix");
        }
        if (StringUtils.isBlank(path)) {
            throw new ArgumentNullException("path");
        }
        if (versionMax < 1) {
            throw new ArgumentOutOfRangeException("versionMax", versionMax,
                    String.format("[1, 2, ...]", versionMax));
        }        
        if (versionMin < 1 || versionMin > versionMax) {
            throw new ArgumentOutOfRangeException("versionMin", versionMin,
                    String.format("[1, {0}]", versionMax));
        }
                
        myPath = path;
        myVerMin = versionMin;
        myVerMax = versionMax;
        myUriPrefix = uriPrefix;        
        myResPrefix = resourcePrefix;
    }
    
    /**
     * Costruttore.
     * 
     * <P>Questo sovraccarico del costruttore imposta il prefisso per il
     * riconoscimento degli URI {@code http://www.scoppelletti.it/res/} e il
     * prefisso per la ricerca della risorsa {@code it/scoppelletti/}.</P>
     * 
     * @param path       Percorso dello schema.
     * @param versionMin Numero di versione minimo.
     * @param versionMax Numero di versione massimo.
     * @see              #SchemaEntityResolver(String, int, int, String, String)
     */
    public SchemaEntityResolver(String path, int versionMin, int versionMax) {
        this(path, versionMin, versionMax, SchemaEntityResolver.URI_PREFIX,
                SchemaEntityResolver.RESOURCE_PREFIX);
    }
        
    /**
     * Risolve un&rsquo;entit&agrave;.
     *
     * <P>L&rsquo;URI di uno schema inserito nell&rsquo;attributo
     * {@code schemaLocation} (o {@code noNamespaceSchemaLocation}) di un
     * documento XML viene risolto dall&rsquo;interfaccia {@code EntityResolver}
     * come identificatore di sistema {@code systemId}.</P>
     * 
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Flusso di lettura dell&rsquo;entit&agrave;.
     */
    public InputSource resolveEntity(String publicId, String systemId) throws
            SAXException, IOException {
        String name;
        InputStream in;
        ReflectionResources reflectRes = new ReflectionResources();
        XmlResources xmlRes = new XmlResources();
        
        name = toResourceName(systemId); 
        if (name == null) {        
            throw new SAXException(xmlRes.getBadSchemaException(publicId,
                    systemId));
        }

        in = ReflectionTools.getResourceAsStream(name);
        if (in == null) {
            throw new SAXException(reflectRes.getResourceNotFoundException(
                    name));
        }
        
        return new InputSource(in);
    }
 
    /**
     * Converte un&rsquo;identificatore di sistema nel nome della risorsa.
     * 
     * @param  systemId Identificatore di sistema.
     * @return          Nome della risorsa. Se l&rsquo;identificatore di sistema
     *                  non &egrave; conforme, restituisce {@code null}.
     */
    private String toResourceName(String systemId) {
        int ver;
        String name, uriPrefix, value;
        
        if (systemId == null) {
            return null;
        }
        
        if (myUriPrefix != null) {
            uriPrefix = myUriPrefix.concat(myPath);
        } else {
            uriPrefix = myPath;
        }
        if (!systemId.startsWith(uriPrefix) ||
            !systemId.endsWith(SchemaEntityResolver.SCHEMA_EXT)) {
            return null;
        }
        
        value = systemId.substring(uriPrefix.length(), systemId.length() -
                SchemaEntityResolver.SCHEMA_EXT.length());
        try {
            ver = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            return null;
        }
        if (ver < myVerMin || ver > myVerMax) {
            return null;
        }

        if (myResPrefix != null) {
            name = myResPrefix.concat(myPath);
        } else {
            name = myPath;
        }
        name = name.concat(Integer.toString(ver));
        name = name.concat(SchemaEntityResolver.SCHEMA_EXT);
        
        return name;
    }
}

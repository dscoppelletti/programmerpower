/*
 * Copyright (C) 2009-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.xml;

import java.io.*;
import org.slf4j.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import it.scoppelletti.programmerpower.*;

/**
 * Gestore di default degli eventi del parser
 * <ACRONYM TITLE="Simple API for XML">SAX</ACRONYM>.
 * 
 * @since 1.0.0
 */
public abstract class XmlDefaultHandler extends DefaultHandler {
    private static final Logger myLogger = LoggerFactory.getLogger(
            XmlDefaultHandler.class);
    private Locator myLocator = null;
    private EntityResolver myEntityResolver = null;
    private ErrorHandler myErrorHandler = null;    
    private StringBuilder myContentBuffer = null;
   
    /**
     * Costruttore.
     */
    protected XmlDefaultHandler() {
    }
    
    /**
     * Restituisce il localizzatore degli eventi nel documento.
     * 
     * <P>Le classi derivate possono accedere al localizzatore degli eventi per
     * determinare appunto la posizione nel documento XML per la quale &egrave;
     * stato inoltrato l&rsquo;evento dal parser SAX; questo tipo di
     * informazione pu&ograve; essere utile, ad esempio, per integrare le
     * segnalazioni di errore.<BR>
     * Il localizzatore, se reso disponibile dalla specifica implementazione del
     * parser SAX, &egrave; accessibile dai gestori degli eventi relativi al
     * contenuto del documento XML rilevati dopo l&rsquo;evento
     * {@code startDocument} e prima dell&rsquo;evento {@code endDocument}.</P>
     * 
     * @return Oggetto. Pu&ograve; essere {@code null}.
     * @see    #setDocumentLocator
     */
    protected final Locator getDocumentLocator() {
        return myLocator;
    }
        
    /**
     * Imposta il localizzatore degli eventi nel documento.
     * 
     * @param                       locator Oggetto. Pu&ograve; essere
     *                                      {@code null}.
     * @it.scoppelletti.tag.default         Localizzatore eventualmente fornito
     *                                      dal parser.
     * @see                                 #getDocumentLocator
     */
    @Override
    public final void setDocumentLocator(Locator locator) {
        myLocator = locator;
    }
    
    /**
     * Restituisce il gestore della risoluzione delle entit&agrave;.
     * 
     * @return Gestore. Pu&ograve; essere {@code null}.
     * @see    #setEntityResolver
     */
    public final EntityResolver getEntityResolver() {
        return myEntityResolver;
    }
    
    /**
     * Imposta il gestore della risoluzione delle entit&agrave;.
     * 
     * <P>Se non viene impostato un gestore della risoluzione (o viene impostato
     * {@code null}), viene utilizzato un gestore predefinito.</P>
     *  
     * @param handler Gestore. Pu&ograve; essere {@code null}.
     * @see           #getEntityResolver
     * @see           #resolveEntity
     */
    public final void setEntityResolver(EntityResolver handler) {
        myEntityResolver = handler;
    }
    
    /**
     * Restituisce il gestore degli errori SAX.
     * 
     * @return Gestore. Pu&ograve; essere {@code null}.
     * @see    #setErrorHandler
     */
    public final ErrorHandler getErrorHandler() {
        return myErrorHandler;
    }
    
    /**
     * Imposta il gestore degli errori SAX.
     * 
     * <P>Se non viene impostato un gestore degli errori (o viene impostato
     * {@code null}), viene utilizzato un gestore predefinito.</P>
     *  
     * @param handler Gestore. Pu&ograve; essere {@code null}.
     * @see           #getErrorHandler
     * @see           #fatalError
     * @see           #error
     * @see           #warning
     */
    public final void setErrorHandler(ErrorHandler handler) {
        myErrorHandler = handler;
    }
    
    /**
     * Caratteri nel contenuto di un elemento.
     * 
     * <P>Questa implementazione del metodo {@code characters} colleziona i
     * caratteri rilevati se tale raccolta &egrave; attiva.</P>
     * 
     * @param ch     Buffer del testo.
     * @param start  Indice del primo carattere del testo nel vettore
     *               {@code ch}.
     * @param length Numero di caratteri del testo.
     * @see          #collectContent
     * @see          #ignorableWhitespace
     */
    @Override
    public void characters(char[] ch, int start, int length) throws
            SAXException {
        if (myContentBuffer != null) {
            myContentBuffer.append(ch, start, length);
        }
    }
    
    /**
     * Caratteri di spaziatura nel contenuto di un elemento.
     * 
     * <P>Alcuni parser possono utilizzare l&rsquo;evento
     * {@code ignorableWhitespace} anzich&eacute; l&rsquo;evento 
     * {@code characters} per rilevare i caratteri di spaziatura presenti nel
     * testo nel contenuto degli elementi; per questo motivo, questa
     * implementazione del metodo {@code ignorableWhitespace} delega la
     * gestione dei caratteri di spaziatura allo stesso metodo
     * {@code characters}.</P>
     *  
     * @param ch     Buffer del testo.
     * @param start  Indice del primo carattere del testo nel vettore
     *               {@code ch}.
     * @param length Numero di caratteri del testo.
     * @see          #characters
     */
    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws
            SAXException {
        characters(ch, start, length);        
    }
    
    /**
     * Attiva la raccolta del testo nel contenuto degli elementi.
     * 
     * <P>Le classi derivate normalmente attivano la raccolta del testo nel
     * contenuto degli elementi che lo prevedono eseguendo il metodo
     * {@code collectContent} nel metodo {@code startElement} che rileva
     * l&rsquo;apertura degli elementi interessati.</P>
     *  
     * @throws java.lang.IllegalStateException
     *         La raccolta del testo &egrave; gi&agrave; stata attivata.
     * @see    #getCollectedContent
     */
    protected final void collectContent() {
        XmlResources res = new XmlResources();
        
        if (myContentBuffer != null) {
            throw new IllegalStateException(
                    res.getContentCollectionAlreadyActive());            
        }
        
        myContentBuffer = new StringBuilder();
    }
    
    /**
     * Restituisce il testo raccolto dal contenuto degli elementi e disattiva
     * la raccolta.
     * 
     * <P>Le classi derivate normalmente gestiscono il testo raccolto dal
     * contenuto degli elementi che lo prevedono eseguendo il metodo
     * {@code getCollectedContent} nel metodo {@code endElement} che rileva la
     * chiusura degli elementi interessati.</P>
     * 
     * @return Testo. Se la raccolta del testo non &egrave; stata attivata,
     *         restituisce {@code null}.
     * @see    #collectContent
     */
    protected final String getCollectedContent() {
        String text;
        
        if (myContentBuffer == null) {
            return null;
        }
        
        text = myContentBuffer.toString();
        myContentBuffer = null;
        
        return text;
    }
    
    /**
     * Risolve un&rsquo;entit&agrave;.
     *
     * <P>Questa implementazione del metodo {@code resolveEntity} applica
     * l&rsquo;eventuale gestore della risoluzione impostata; se il gestore
     * non &egrave; impostato, restituisce {@code null}.</P>
     *  
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Flusso di lettura dell&rsquo;entit&agrave;. Pu&ograve;
     *                  essere {@code null} per attivare la rilevazione della
     *                  risorsa dall&rsquo;identificatore di sistema
     *                  {@code systemId}.
     * @see             #setEntityResolver
     */
    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws
            SAXException, IOException {
        if (myEntityResolver != null) {
            return myEntityResolver.resolveEntity(publicId, systemId);
        }
        
        return null;
    }
    
    /**
     * Errore fatale.
     *
     * <P>Questa implementazione del metodo {@code fatalError} utilizza
     * l&rsquo;eventuale gestore degli errori SAX impostato; in ogni caso, SAX
     * provvede comunque a inoltrare l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     * @see      #setErrorHandler
     */
    @Override
    public void fatalError(SAXParseException ex) throws SAXException {
        if (myErrorHandler != null) {
            myErrorHandler.fatalError(ex);
        }
    }
    
    /**
     * Errore non fatale.
     *
     * <P>Questa implementazione del metodo {@code error} utilizza 
     * l&rsquo;eventuale gestore degli errori SAX impostato; se il gestore non
     * &egrave; impostato, inoltra l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     * @see      #setErrorHandler
     */
    @Override
    public void error(SAXParseException ex) throws SAXException {
        if (myErrorHandler != null) {
            myErrorHandler.error(ex);
        } else {
            throw ex;
        }
    }
    
    /**
     * Avviso.
     *
     * <P>Questa implementazione del metodo {@code warning} utilizza 
     * l&rsquo;eventuale gestore degli errori SAX impostato; se il gestore non
     * &egrave; impostato, emette l&rsquo;eccezione come segnalazione su
     * log.</P>
     *
     * @param ex Eccezione.
     * @see      #setErrorHandler
     */
    @Override
    public void warning(SAXParseException ex) throws SAXException {
        if (myErrorHandler != null) {
            myErrorHandler.warning(ex);
        } else {
            myLogger.warn(ApplicationException.toString(ex), ex);
        }
    }    
}

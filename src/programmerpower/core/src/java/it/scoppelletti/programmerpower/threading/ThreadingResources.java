/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.threading;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse per la gestione dei thread.
 */
final class ThreadingResources extends ResourceWrapper {
    
    /**
     * Costruttore.
     */
    ThreadingResources() {
    }

    /**
     * Eccezione per oggetto utilizzato al di fuori del proprio contesto del
     * thread.
     * 
     * @param  objectId Identificatore dell&rsquo;oggetto.
     * @return Testo.
     */
    String getObjectOutOfThreadContextException(String objectId) {
        return format("ObjectOutOfThreadContextException", objectId);
    }
}

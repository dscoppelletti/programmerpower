/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

import java.io.*;
import java.net.*;

/**
 * Rilevamento
 * dell&rsquo;<ACRONYM TITLE="Uniform Resource Locator">URL</ACRONYM> di una
 * risorsa.
 */
final class URLFinder extends AbstractFinder<URL> {
    
    /**
     * Costruttore.
     * 
     * @param prop        Nome della propriet&agrave; di sistema sulla quale
     *                    pu&ograve; essere impostato un nome della risorsa da
     *                    ricercare alternativo a quello di default
     *                    {@code defaultName}.
     * @param defaultName Nome della risorsa di default.
     */
    URLFinder(String prop, String defaultName) {
        super(prop, defaultName);
    }
    
    @Override
    URL findFileImpl(String name) throws IOException {            
        File file;
        
        file = new File(name);
        if (!file.isFile()) {
            return null;
        }
        
        myLogger.debug("File {} found.", name);             
        
        return file.toURI().toURL();
    }
    
    @Override
    URL findResourceImpl(ClassLoader loader, String name) {
        return loader.getResource(name);
    }        
}        


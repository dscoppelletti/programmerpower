/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

/**
 * Eccezione per conversione di classe non ammessa.
 * 
 * @since 1.0.0
 */
public class InvalidCastException extends ClassCastException {
    private static final long serialVersionUID = 1;
    
    /**
     * @serial Nome della classe da convertire.
     */
    private final String myCastingClassName;
    
    /** 
     * @serial Nome della classe di destinazione.
     */
    private final String myTargetClassName;
    
    /**
     * Costruttore.
     *
     * @param castingClassName Nome della classe da convertire.
     * @param targetClassName  Nome della classe di destinazione.
     */
    public InvalidCastException(String castingClassName,
            String targetClassName) {
        myCastingClassName = castingClassName;
        myTargetClassName = targetClassName;
    }
    
    /**
     * Restituisce il nome della classe da convertire.
     * 
     * @return Valore.
     */
    public final String getCastingClassName() {
        return myCastingClassName;
    }
    
    /**
     * Restituisce il nome della classe di destinazione.
     * 
     * @return Valore.
     */
    public final String getTargetClassName() {
        return myTargetClassName;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        ReflectionResources res = new ReflectionResources();
        
        return res.getInvalidCastException(myCastingClassName, myTargetClassName);
    }    
}

/*
 * Copyright (C) 2010-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Funzioni di utilit&agrave; sugli enumerati.
 *
 * @since 2.1.0
 */
public final class EnumTools {

    /**
     * Suffisso da applicare al nome completo del tipo di enumerato per comporre
     * la base del nome del contenitore di risorse {@code ResourceBundle} che
     * definisce le etichette localizzate corrispondenti ai valori
     * dell&rsquo;enumerato. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see   #getLabel
     */
    public static final String LABEL_SUFFIX = "-labels";
    
    /**
     * Costruttore privato per classe statica.
     */
    private EnumTools() {
    }

    /**
     * Restituisce il valore di un enumerato corrispondente al nome di una
     * costante.
     * 
     * <P>Il metodo {@code valueOf}, contrariamente al corrispondente metodo
     * statico della classe Java {@code Enum}, ignora gli eventuali caratteri
     * {@code blank} iniziali e finali nel nome della costante e non distingue
     * tra caratteri maiuscoli e minuscoli.</P>
     * 
     * @param  <T>      Tipo di enumerato.
     * @param  enumType Classe dell&rsquo;enumerato.
     * @param  name     Nome della costante.
     * @return          Valore dell&rsquo;enumerato.
     * @throws java.lang.EnumConstantNotPresentException
     *         L&rsquo;enumerato non include la costante ricercata.
     */
    public static <T extends Enum<T>> T valueOf(Class<T> enumType,
            String name) {
        T[] values; 
        
        if (enumType == null) {
            throw new ArgumentNullException("enumType");
        }
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name");
        }
                
        values = enumType.getEnumConstants();
        if (values == null) {
            throw new InvalidCastException(enumType.getName(),
                    Enum.class.getName());
        }
        
        name = name.trim();
        for (T value : values) {
            if (name.compareToIgnoreCase(value.name()) == 0) {
                return value;
            }
        }
        
        throw new EnumConstantNotPresentException(enumType, name);
    }        
    
    /**
     * Restituisce l&rsquo;etichetta localizzata corrispondente ad un valore di
     * un enumerato.
     *
     * <P>Per ogni tipo enumerato &egrave; possibile utilizzare un insieme di
     * file di propriet&agrave; per associare ad ogni valore
     * dell&rsquo;enumerato un&rsquo;etichetta localizzata nelle diverse
     * lingue.<BR>
     * La base del nome del contenitore di risorse {@code ResourceBundle}
     * &egrave; composta aggiungendo al nome completo del tipo di enumerato il
     * suffisso {@code -labels}. Ad esempio, per l&rsquo;enumerato
     * {@code it.scoppelletti.programmerpower.ui.MessageType}, la base del nome
     * delle risorse &egrave;
     * {@code it.scoppelletti.programmerpower.ui.MessageType-labels}.<BR>
     * Se non viene rilevata nessuna risorsa associata al valore
     * dell&rsquo;enumerato, si assume che l&rsquo;etichetta coincida con il
     * nome del valore stesso.</P>
     *
     * @param  value  Valore.
     * @param  locale Localizzazione.
     * @return        Etichetta.
     */
    public static String getLabel(Enum<?> value, Locale locale) {
        String baseName, className, label, name;
        ResourceBundle res;
        
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        if (locale == null) {
            throw new ArgumentNullException("locale");
        }
                    
        className = value.getClass().getCanonicalName();
        if (StringUtils.isBlank(className)) {
            throw new ReturnNullException(value.getClass().getName(),
                    "getCanonicalClassName");
        }
        
        name = value.name();
        baseName = className.concat(EnumTools.LABEL_SUFFIX);                      
        try {
            res = ResourceBundle.getBundle(baseName, locale);
            label = res.getString(name);
        } catch (MissingResourceException ex) {
            label = name;
        }
        
        return label;
    }                 
}

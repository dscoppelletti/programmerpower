/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.ui;

import java.io.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Evento di esposizione di un messaggio.
 * 
 * @it.scoppelletti.tag.threadsafe
 * @since 1.0.0
 */
public final class MessageEvent implements Serializable {
    private static final long serialVersionUID = 2;
    
    /**
     * @serial Tipo di messaggio.
     */
    private final MessageType myMessageType;
    
    /**
     * @serial Messaggio. Pu&ograve; essere {@code null}.
     */
    private final String myMessage;
    
    /**
     * @serial Eccezione. Pu&ograve; essere {@code null}.
     */
    private final Throwable myException;
     
    /**
     * Costruttore.
     *
     * @param messageType Tipo di messaggio.
     * @param message     Messaggio. Pu&ograve; essere {@code null}.
     * @param ex          Eccezione. Pu&ograve; essere {@code null}.
     * @since             2.0.0                    
     */
    public MessageEvent(MessageType messageType, String message, Throwable ex) {
        if (messageType == null) {
            throw new ArgumentNullException("messageType");
        }
                
        myMessageType = messageType;
        myMessage = message;
        myException = ex;
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param      in Flusso di lettura.
     * @serialData    Formato di default.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {        
        in.defaultReadObject();        
        
        try {
            if (myMessageType == null) {
                throw new PropertyNotSetException(getClass().getName(),
                        "messageType");
            }
        } catch (Exception ex) {
            throw new DeserializeException(ex);
        }        
    }      
    
    /**
     * Restituisce il tipo di messaggio.
     *
     * @return Valore.
     */
    public MessageType getMessageType() {
        return myMessageType;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Valore. Pu&ograve; essere {@code null}.
     */
    public String getMessage() {
        return myMessage;
    }
    
    /**
     * Restituisce l&rsquo;eccezione.
     *
     * @return Oggetto. Pu&ograve; essere {@code null}.
     */
    public Throwable getThrowable() {
        return myException;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("MessageEvent(%1$s, %2$s, %3$s)", myMessageType,
                myMessage, myException); 
    }     
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

/**
 * Collezione di attributi.
 *
 * @since     2.0.0
 */
public interface AttributeMap {

    /**
     * Restituisce il valore di un attributo.
     * 
     * @param  name Nome dell&rsquo;attributo.
     * @return      Valore. Se l&rsquo;attributo non &egrave; impostato,
     *              restitutisce {@code null}.
     * @see         #setAttribute
     */
    Object getAttribute(String name);
    
    /**
     * Restituisce il valore di un attributo.
     * 
     * @param  name        Nome dell&rsquo;attributo.
     * @param  initializer Inizializzatore on-demand dell&rsquo;attributo.
     * @return             Valore.
     * @see                #getAttribute(String)
     * @see                #setAttribute
     */
    Object getAttribute(String name, RunnableWithResult<?> initializer);
    
    /**
     * Imposta il valore di un attributo.
     * 
     * @param name  Nome dell&rsquo;attributo.
     * @param value Valore.
     * @see         #getAttribute(String)
     * @see         #getAttribute(String, RunnableWithResult)
     * @see         #removeAttribute
     */
    void setAttribute(String name, Object value);
    
    /**
     * Rimuove un attributo.
     * 
     * @param name Nome dell&rsquo;attributo.
     * @see        #setAttribute
     */    
    void removeAttribute(String name);     
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import org.slf4j.*;
import it.scoppelletti.programmerpower.*;

/**
 * Funzioni di utilit&agrave; sugli oggetti.
 * 
 * @since 2.1.0
 */
public final class ObjectTools {
    private static final Logger myLogger = LoggerFactory.getLogger(
            ObjectTools.class);   
    
    /**
     * Costruttore privato per classe statica.
     */
    private ObjectTools() {        
    }
    
    /**
     * Rilascia le risorse per un oggetto.
     * 
     * <P>Il metodo {@code dispose} rilascia le risorse per un oggetto ignorando
     * l&rsquo;eventuale eccezione inoltrata (tale eccezione viene emessa come
     * segnalazione di log).<BR>
     * <P>Il valore di ritorno {@code null} pu&ograve; essere utilizzato per
     * azzerare la stessa variabile {@code obj} in modo che tale variabile
     * faccia anche da indicatore di oggetto attivo ({@code obj != null}) o con
     * risorse gi&grave; rilasciate ({@code obj == null}).</P>
     *  
     * @param  <T> Classe dell&rsquo;oggetto.
     * @param  obj Oggetto. Se &egrave; {@code null}, si assume che le risorse
     *             per l&rsquo;oggetto siano gi&agrave; state rilasciate.
     * @return     {@code null}.
     */
    public static <T extends Disposable> T dispose(T obj) {
        if (obj != null) {
            try {
                obj.dispose();
            } catch (RuntimeException ex) {
                myLogger.error("Failed to dispose object.", ex);
            }                          
        }
        
        return null;
    }
}

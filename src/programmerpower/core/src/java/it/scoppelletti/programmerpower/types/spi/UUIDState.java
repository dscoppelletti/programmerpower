/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types.spi;

import java.net.*;
import java.security.*;
import java.util.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Classe di base per l&rsquo;implementazione di uno stato utilizzabile da una
 * classe derivata dalla classe {@code DefaultUUIDProvider} per la generazione
 * di <ACRONYM TITLE="Universally Unique IDentifier">UUID</ACRONYM> della
 * versione 1 (basati sul tempo) secondo l&rsquo;algoritmo descritto nella
 * sezione 4.2 delle specifiche
 * <ACRONYM TITLE="Request for Comments">RFC</ACRONYM> 4122.</P>
 * 
 * @see   it.scoppelletti.programmerpower.types.spi.DefaultUUIDProvider
 * @see   <A HREF="http://www.ietf.org/rfc/rfc4122.txt" TARGET="_top">RFC
 *        4122: A Universally Unique IDentifier (UUID) URN Namespace</A>    
 * @since 2.1.0
 */
public abstract class UUIDState {
  
    /**
     * Nome della propriet&agrave; di sistema sulla quale &egrave; possibile
     * indicare se deve essere disattivata la verifica dell&rsquo;eventuale  
     * variazione dell&rsquo;indirizzo MAC della scheda di rete tra 
     * l&rsquo;estrazione di un UUID e l&rsquo;altro; se la propriet&agrave;
     * non &egrave; impostata (oppure &egrave; impostata con un qualsiasi valore
     * diverso da {@code "true"}), la verifica &egrave; attivata. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @it.scoppelletti.tag.default {@code false}
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/types/spi/DefaultUUIDProvider.html#idMAC">Indirizzo
     *      MAC della scheda di rete</A>
     */
    public static final String PROP_NOMACCHANGINGTEST =
        "it.scoppelletti.programmerpower.types.UUIDGenerator.noMACChangingTest";

    /** 
     * Numero di frazioni del timestamp in unit&agrave; da 100 nanosecondi
     * incluse in un millisecondo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */    
    public static final short TIMESTAMPFRAC_COUNT = 10000;
    
    /**
     * Dimensione (in byte) di un indirizzo MAC IEEE 802. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>. 
     */
    private static final int MACADDRESS_SIZE = 6;
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultUUIDProvider.class);            
    private long myTimestampMillis;
    private short myTimestampFrac;
    private short myClockSeq;
    private long myNodeID;
    private boolean myFirstMACChangedTest = true;
    private boolean myMACChangingTest;
    
    /**
     * Costruttore.
     */
    protected UUIDState() {
        String value;
        
        value = JVMTools.getProperty(UUIDState.PROP_NOMACCHANGINGTEST);
        myMACChangingTest = !Boolean.parseBoolean(value);
        
        if (!myMACChangingTest) {
            myLogger.warn("MAC address changing test disabled.");
        }                              
    }
        
    /**
     * Inizializzazione.
     */
    private void init() {
        byte[] nodeID;
        myTimestampMillis = System.currentTimeMillis();
        myTimestampFrac = 0;
        myClockSeq = newClockSequence();
        
        nodeID = getMACAddress();
        if (nodeID != null) {
            setNodeID(makeNodeID(nodeID));
        } else {
            myLogger.warn("Cannot get MAC address.");
            newNodeID();            
            if (myMACChangingTest) { 
                // E' inutile controllare se l'indirizzo MAC cambia
                myMACChangingTest = false;
                myLogger.warn("MAC address changing test disabled.");
            }                                   
        }
    }  
   
    /**
     * Restituisce il timestamp in millisecondi.
     * 
     * @return Valore.
     * @see    #setTimestampMillis
     * @see    #save
     */
    protected final long getTimestampMillis() {
        return myTimestampMillis;
    }
    
    /**
     * Imposta il timestamp in millisecondi.
     * 
     * @param value Valore.
     * @see         #getTimestampMillis
     * @see         #load
     */
    protected final void setTimestampMillis(long value) {
        if (value < 0) {
            throw new ArgumentOutOfRangeException("value", value, "[0, ...[");            
        }
        
        myTimestampMillis = value;
    }
    
    /**
     * Restituisce la frazione del timestamp in unit&agrave; da 100 nanosecondi.
     * 
     * @return Valore.
     * @see    #setTimestampFrac
     * @see    #save
     */
    protected final short getTimestampFrac() {
        return myTimestampFrac;
    }
    
    /**
     * Imposta la frazione del timestamp in unit&agrave; da 100 nanosecondi.
     * 
     * @param value Valore.
     * @see         #getTimestampFrac
     * @see         #load
     */
    protected final void setTimestampFrac(short value) {
        if (value < 0 || value >= UUIDState.TIMESTAMPFRAC_COUNT) {
            throw new ArgumentOutOfRangeException("value", value,
                    String.format("[0, %1$d[", UUIDState.TIMESTAMPFRAC_COUNT));
        }
        
        myTimestampFrac = value;
    }
    
    /**
     * Restituisce la sequenza di clock.
     * 
     * @return Valore.
     * @see    #setClockSequence
     * @see    #save
     */
    protected final short getClockSequence() {
        return myClockSeq;
    }
    
    /**
     * Imposta la sequenza di clock.
     * 
     * <P>Della sequenza di clock sono utilizzati i 14 bit meno
     * significativi.</P>
     * 
     * @param value Valore.
     * @see         #getClockSequence
     * @see         #load
     */
    protected final void setClockSequence(short value) {
        myClockSeq = value;
    }
    
    /**
     * Estrae una nuova sequenza di clock.
     * 
     * @return Valore.
     */
    private short newClockSequence() {
        short value;
        byte[] buf = new byte[2];
        
        RNGTools.getCSRNG().nextBytes(buf);
        value = (short) (((buf[0] & 0x3F) << 8) | buf[1] & 0xFF); // 14 bit
        
        return value;
    }  
    
    /**
     * Restituisce l&rsquo;id&#46; del nodo.
     * 
     * @return Valore.
     * @see    #setNodeID
     * @see    #save
     */
    protected final long getNodeID() {
        return myNodeID;
    }
    
    /**
     * Imposta l&rsquo;id&#46; del nodo.
     * 
     * <P>Dell&rsquo;id&#46; del nodo sono utilizzati i 48 bit meno
     * significativi.</P>
     * 
     * @param value Valore.
     * @see         #getNodeID
     * @see         #load
     */
    protected final void setNodeID(long value) {
        myNodeID = value;
    }
    
    /**
     * Estrae un nuovo id&#46; del nodo.
     * 
     * <H4>1. RFC 4122, sezione 4.5</H4>
     * 
     * <P>Bisogna utilizzare un numero casuale di 47 bit da usare come bit meno
     * significativi dei 48 previsti per il node ID (quindi il bit pi&ugrave;
     * significativo del node ID rimane 0); inoltre il bit unicast/multicast (il
     * bit meno significativo del byte pi&ugrave; significativo) deve essere
     * impostato per evitare che il numero estratto possa coincidere con un vero
     * indirizzo MAC.</P>
     * 
     * @see #getNodeID
     */
    private long newNodeID() {        
        byte[] buf = new byte[UUIDState.MACADDRESS_SIZE];
        
        RNGTools.getCSRNG().nextBytes(buf);
        
        buf[0] &= 0x7F;
        buf[0] |= 0x01;
        
        return makeNodeID(buf);
    }
    
    /**
     * Converte un vettore di byte nell&rsquo;id&#46; di un nodo.
     * 
     * @param  buf Vettore.
     * @return     Valore.
     */
    private long makeNodeID(byte[] buf) {
        long value;
        
        value = (long) (((buf[0] & 0xFF) << 40) | ((buf[1] & 0xFF) << 32) |
                ((buf[2] & 0xFF) << 24) | ((buf[3] & 0xFF) << 16) |
                ((buf[4] & 0xFF) << 8) | (buf[5] & 0xFF)); // 48 bit
        
        return value;        
    }

    /**
     * Calcola un UUID.
     * 
     * @param  timestamp     Timestamp.
     * @param  clockSequence Sequenza di clock.
     * @param  nodeID        Id&#46; del nodo.
     * @return               Valore.
     */    
    static UUID calcUUID(long timestamp, short clockSequence, long nodeID) {
        long msb, lsb;

        msb = timestamp << 32; // timestamp low
        msb |= (timestamp & 0xFFFF00000000L) >> 16; // timestamp mid
        msb |= 0x1000; // Versione 0001 = time-based
        msb |= (timestamp >> 48) & 0x0FFF; // timestamp high
         
        lsb = (long) 0x8000000000000000L; // Variante 10x = RFC 4122
        lsb |= (long) ((clockSequence & 0x3FFF) << 48); // sequenza di clock
        lsb |= (long) ((nodeID & 0x0000FFFFFFFFFFFFL) << 16); // Id. del nodo

        return new UUID(msb, lsb);
    }
    
    /**
     * Estrae il prossimo UUID.
     */
    final void nextUUID() {
        boolean newClockSeq;
        long currentTimestampMillis = System.currentTimeMillis();
        
        if (load()) {
            newClockSeq = testMACChanged();

            if (currentTimestampMillis < myTimestampMillis) {
                // L'orologio di sistema e' stato portato indietro dopo
                // l'ultimo UUID estratto:
                // Allineo lo stato all'orologio di sistema ma incremento la
                // sequenza di clock (se non l'ho appena riestratta).
                myLogger.warn("System time has been set backward."); 
                if (!newClockSeq) {
                    myClockSeq++;
                }
                myTimestampMillis = currentTimestampMillis;
                myTimestampFrac = 0;
            }            
        } else {
            init();            
        }
        myFirstMACChangedTest = false;
        
        if (currentTimestampMillis > myTimestampMillis) {
            // L'orologio di sistema e' avanzato dopo l'ultimo UUID estratto:
            // L'UUID si basa sull'orologio di sistema.
            myTimestampMillis = currentTimestampMillis;
            myTimestampFrac = 0;            
        } else { // if (currentTimestampMillis == myTimestampMillis)
            // L'orologio di sistema non e' avanzato dopo l'ultimo UUID
            // estratto:
            // L'UUID si basa sulla prossima frazione di timestamp.
            myTimestampFrac++;
            if (myTimestampFrac == UUIDState.TIMESTAMPFRAC_COUNT) {
                // Ho esaurito le frazioni di timestamp disponibili:
                // Dovro' aspettare che l'orologio di sistema avanzi.
                myTimestampMillis++;
                myTimestampFrac = 0;                                
            }
        }
    }
    
    /**
     * Registra lo stato. 
     * 
     * @param delayMax Massimo ritardo (millisecondi) che ci si aspetta di
     *                 rilevare sull&rsquo;orologio di sistema rispetto al
     *                 timestamp dell&rsquo;ultimo UUID estratto.
     */
    final void save(long delayMax) {
        long delay;
        boolean interruptPending;
        
        // Aspetto che l'orologio di sistema raggiunga il timestamp
        // corrispondente all'ultimo UUID estratto.
        // Durante questo ciclo ignoro le eventuali richieste di interruzione
        // ricevute per poi rispondere alla fine.
        interruptPending = Thread.interrupted();
        delay = myTimestampMillis - System.currentTimeMillis();
        while (delay > 0) {
            // Il ritardo massimo e' inizialmente quello specificato; poi lo
            // riduco man mano a quello rilevato ad ogni iterazione:
            // Se il ritardo rilevato e' maggiore, significa che l'orologio di
            // sistema e' stato portato indietro nel frattempo.
            if (delay > delayMax) {
                myLogger.warn("System time has been set backward.");
                break;
            }           
            delayMax = delay;
            
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                myLogger.warn("Sleep interrupted.", ex);
                interruptPending = true;
            }
            
            delay = myTimestampMillis - System.currentTimeMillis();
        }
                     
        if (interruptPending) {
            Thread.currentThread().interrupt();
        }
        
        save();
    }

    /**
     * Blocca le risorse.
     * 
     * <P>Le classi derivate devono implementare il metodo {@code lock} per
     * bloccare l&rsquo;accesso alle risorse condivise da pi&ugrave; processi
     * durante l&rsquo;estrazione di uno o pi&ugrave; UUID.</P>
     * 
     * @see #release
     */
    protected abstract void lock();
        
    /**
     * Rilascia le risorse.
     * 
     * <P>Le classi derivate devono implementare il metodo {@code release} per
     * rilasciare le risorse bloccate durante l&rsquo;estrazione di uno o
     * pi&ugrave; UUID.</P>
     * 
     * @see #lock
     */
    protected abstract void release();        
        
    /**
     * Legge lo stato.
     * 
     * <P>Le classi derivate devono implementare il metodo {@code load} per
     * leggere le propriet&grave; dello stato.</P>
     * 
     * @return Indica se la lettura dello stato ha avuto successo. Se la lettura
     *         dello stato fallisce, la classe {@code UUIDState} provvede ad
     *         inizializzare le propriet&agrave; dello stato con i valori
     *         opportuni.
     * @see    #save
     * @see    #setTimestampMillis
     * @see    #setTimestampFrac
     * @see    #setClockSequence
     * @see    #setNodeID
     */    
    protected abstract boolean load();
    
    /**
     * Registra lo stato.
     * 
     * <P>Le classi derivate devono implementare il metodo {@code save} per
     * registrare le propriet&grave; dello stato.</P>
     * 
     * @see #load
     * @see #getTimestampMillis
     * @see #getTimestampFrac
     * @see #getClockSequence
     * @see #getNodeID 
     */
    protected abstract void save();        
    
    /**
     * Restituisce l&rsquo;indirizzo MAC della scheda di rete.
     *
     * @return Vettore. Se l&rsquo;indirizzo non &egrave; determinabile,
     *         restituisce {@code null}.
     */
    private byte[] getMACAddress() {
        byte[] addr;
        
        addr = AccessController.doPrivileged(new PrivilegedAction<byte[]>() {
            public byte[] run() {
                return getMACAddressImpl();
            }
        });
        
        return addr;        
    }
    
    /**
     * Restituisce l&rsquo;indirizzo MAC della scheda di rete.
     *
     * @return Vettore. Se l&rsquo;indirizzo non &egrave; determinabile,
     *         restituisce {@code null}.
     */
    private byte[] getMACAddressImpl() {
        byte[] addr;
        NetworkInterface netInterface;
        Enumeration<NetworkInterface> netInterfaces;
        
        try {
            netInterfaces = NetworkInterface.getNetworkInterfaces();
            if (netInterfaces == null) {
                return null;
            }
            
            while (netInterfaces.hasMoreElements()) {
                netInterface = netInterfaces.nextElement();
                addr = netInterface.getHardwareAddress();
                if (addr != null && addr.length == UUIDState.MACADDRESS_SIZE) {
                    return addr;
                }                
            }
        } catch (SocketException ex) {
            myLogger.error("Failed to get MAC address.", ex);
        }
        
        return null;
    }
    
    /**
     * Verifica se l&rsquo; indirizzo MAC della scheda di rete &egrave; diverso
     * dall&rsquo;ultimo utilizzato.
     * 
     * @return Esito della verifica.
     */
    private boolean testMACChanged() {
        byte[] addr;
        long currentMACAddress;
        
        if (!myMACChangingTest && !myFirstMACChangedTest) {
            return false;
        }

        addr = getMACAddress();
        if (addr == null) {
            myLogger.warn("Cannot get MAC address."); 
            return false;
        }
        
        currentMACAddress = makeNodeID(addr);
        if (currentMACAddress == myNodeID) {
            return false;
        }

        myLogger.warn("MAC address changed.");
        myNodeID = currentMACAddress;
        myClockSeq = newClockSequence();
        
        return true;
    }    
}

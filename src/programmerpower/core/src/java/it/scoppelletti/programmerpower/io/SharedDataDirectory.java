/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.io.spi.SharedDataDirectoryProvider;
import it.scoppelletti.programmerpower.reflect.ServiceLoaderWrapper;

/**
 * Direttorio dei dati condiviso.
 * 
 * <P>Le applicazioni Programmer Power possono leggere alcuni dati da un
 * direttorio <DFN>condiviso</DFN>, ovvero sul quale tutti gli utenti hanno i
 * permessi di sola lettura; l&rsquo;amministratore del sistema dovrebbe invece
 * avere anche i permessi in scrittura per poter compilare manualmente i file
 * condivisi.</P>
 * 
 * <BLOCKQUOTE CLASS="note">
 * Poich&eacute; i file inseriti nel direttorio dei dati condiviso sono
 * leggibili da tutti gli utenti, non dovrebbero contenere dati sensibili.
 * </BLOCKQUOTE>
 * 
 * <P>Programmer Power utilizza un modello
 * <ACRONYM TITLE="Service Provider Interface">SPI</ACRONYM> per rilevare il
 * direttorio dei dati condiviso a seconda della piattaforma sulla quale il
 * programma &egrave; in esecuzione:</P>
 * 
 * <DL>
 * <DT>Piattaforma UNIX/Linux:</DT>
 * <DD>{@code /usr/local/etc/it-scoppelletti-programmerpower}</DD>
 * <DT>Piattaforma Microsoft Windows:</DT>
 * <DD>{@code C:\ProgramData\it-scoppelletti-programmerpower}</DD>
 * <DT>Piattaforma <ACRONYM TITLE="Macintosh">Mac</ACRONYM>
 * <ACRONYM TITLE="Operative System">OS</ACRONYM>:</DT>
 * <DD>{@code /Library/it-scoppelletti-programmerpower}</DD>
 * </DL>
 * 
 * @see   it.scoppelletti.programmerpower.io.spi.SharedDataDirectoryProvider
 * @since 2.0.0
 */
public final class SharedDataDirectory extends
    ServiceLoaderWrapper<SharedDataDirectoryProvider, File> {    
    private static final Logger myLogger = LoggerFactory.getLogger(
            SharedDataDirectory.class);    

    /**
     * Costruttore privato per classe statica.
     */
    private SharedDataDirectory() {    
        super(SharedDataDirectoryProvider.class);
    }
    
    /**
     * Restituisce il direttorio.
     * 
     * @return  Oggetto.
     * @throws  it.scoppelletti.programmerpower.io.FileNotExistException
     *          Il direttorio non esiste.
     * @see     #tryGet
     * @since   2.1.0
     */
    public static File get() {
        File dir;
        SharedDataDirectory service;
        
        service = new SharedDataDirectory();
        dir = service.query();        
        if (dir == null) {
            throw new FileNotExistException(
                    SharedDataDirectory.class.getName());
        }
        
        return dir;
    }  
    
    /**
     * Restituisce il direttorio.
     * 
     * @return Oggetto. Se il direttorio non esiste, restituisce {@code null}.
     * @see    #get
     * @since  2.1.0
     */
    public static File tryGet() {
        File dir;
        SharedDataDirectory service;
        
        service = new SharedDataDirectory();
        dir = service.query(); 
        if (dir == null) {
            myLogger.warn("Shared data directory not found.");
            return null;
        }
        
        return dir;
    }      
    
    protected File query(SharedDataDirectoryProvider provider) {
        File dir;
        
        dir = provider.getDirectory();
        if (dir == null) {
            return null;
        }
        
        try {            
            if (!dir.isDirectory()) {
                myLogger.warn("Directory {} not found.", dir);
                return null;
            }
            
            if (!dir.canRead()) {
                myLogger.warn("Directory {} not readable.", dir);
                return null;            
            }            
        } catch (SecurityException ex) {
            myLogger.warn(String.format("Failed to access directory %1$s.",
                    dir), ex);
            return null;
        }        
        
        return dir;
    }           
}


/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.resources;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.threading.*;

/**
 * Funzioni di utilit&agrave; sulle risorse.
 * 
 * @since 2.1.0
 */
public final class ResourceTools {
    private static final Logger myLogger = LoggerFactory.getLogger(
            ResourceTools.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private ResourceTools() {        
    }
    
    /**
     * Restituisce la localizzazione per il thread corrente.
     *  
     * @return Localizzazione. Se il contesto del thread non &egrave; stato
     *         impostato, restituisce la localizzazione di default di JVM.
     * @see    it.scoppelletti.programmerpower.threading.ThreadContext
     * @see    it.scoppelletti.programmerpower.threading.ThreadContext#getLocale
     */
    public static Locale getLocale() {
        ThreadContext threadCtx = ThreadContext.tryGetCurrentThreadContext();
        
        return (threadCtx != null) ? threadCtx.getLocale() :
            Locale.getDefault();        
    }
    
    /**
     * Restituisce la localizzazione corrispondente ad un codice.
     * 
     * <P>La classe {@code Locale} di JDK pubblica i sovraccarichi dei
     * costruttori per le varie combinazioni dei codici (lingua, paese,
     * variante), ma non la possibilit&agrave; di ottenere la localizzazione
     * corrispondente ad un codice nel formato del parametro {@code name}.</P>
     *  
     * @param  name Codice nel formato
     *              <VAR>language</VAR><CODE>_</CODE><VAR>country</VAR> (ad
     *              esempio, {@code it_IT}) o <VAR>language</VAR> (ad esempio,
     *              {@code it}).
     * @return      Oggetto. Se non esiste alcuna localizzazione corrispondente
     *              al codice {@code name} tra quelle disponibili in JVM,
     *              restituisce la localizzazione di default di JVM.
     */
    public static Locale getLocale(String name) {        
        for (Locale locale : Locale.getAvailableLocales()) {
            // Non distinguo tra maiuscole e minuscole per poter adattare i
            // codici della classe Locale a quelli delle intestazioni HTTP
            if (locale.toString().equalsIgnoreCase(name)) {
                return locale;
            }
        }
        
        myLogger.warn("Locale {} not found: using JVM default.", name);
        return Locale.getDefault();
    }

    /**
     * Restituisce una stringa localizzata attraverso una collezione che ha per
     * chiave la stringa corrispondente alla localizzazione.
     *  
     * @param  map          Collezione delle stringhe localizzate.
     * @param  locale       Localizzazione. Pu&ograve; essere {@code null} per
     *                      considerare le sole localizzazioni impostata per il
     *                      thread corrente e di default di JVM.
     * @param  defaultValue Valore di default da restituire se non &egrave;
     *                      rilevata la stringa localizzata. Pu&ograve; essere
     *                      {@code null}.                              
     * @return              Stringa localizzata.
     * @see                 #listCandidateLocales
     */
    public static String getLocalizedString(Map<String, String> map,
            Locale locale, String defaultValue) {
        String value;
        List<Locale> locales;
        
        if (map == null) {
            throw new ArgumentNullException("map");
        }
        
        locales = ResourceTools.listCandidateLocales(locale);
        for (Locale loc : locales) {
            value = map.get(loc.toString());
            if (!StringUtils.isBlank(value)) {
                return value;
            }
        }
        
        return defaultValue;
    }
    
    /**
     * Rileva l&rsquo;URL della versione localizzata di una risorsa.
     * 
     * @param  location Locazione della risorsa non localizzata.
     * @param  locale   Localizzazione. Pu&ograve; essere {@code null} per
     *                  considerare le sole localizzazioni impostata per il
     *                  thread corrente e di default di JVM.
     * @return          URL. Se la risorsa non &egrave; rilevata, restituisce
     *                  {@code null}.
     * @see             #listCandidateLocales
     */    
    public static URL getResource(String location, Locale locale) {
        URL url;
        List<String> locations;
        
        locations = ResourceTools.listCandidateLocalizedResourceLocations(
                location, locale);
        for (String loc : locations) {
            url = ReflectionTools.getResource(loc);
            if (url != null) {
                return url;
            }
        }
        
        return null;
    }
    
    /**
     * Apre il flusso di lettura della versione localizzata di una risorsa.
     * 
     * @param  location Locazione della risorsa non localizzata.
     * @param  locale   Localizzazione. Pu&ograve; essere {@code null} per
     *                  considerare le sole localizzazioni impostata per il
     *                  thread corrente e di default di JVM.
     * @return          Flusso di lettura. Se la risorsa non &egrave; rilevata,
     *                  restituisce {@code null}.
     * @see             #listCandidateLocales
     */    
    public static InputStream getResourceAsStream(String location,
            Locale locale) {
        InputStream in;
        List<String> locations;
        
        locations = ResourceTools.listCandidateLocalizedResourceLocations(
                location, locale);
        for (String loc : locations) {
            in = ReflectionTools.getResourceAsStream(loc);
            if (in != null) {
                return in;
            }
        }
        
        return null;
    }
    
    /**
     * Legge il testo della versione localizzata di una risorsa.
     * 
     * @param  location Locazione della risorsa non localizzata.
     * @param  locale   Localizzazione. Pu&ograve; essere {@code null} per
     *                  considerare le sole localizzazioni impostata per il
     *                  thread corrente e di default di JVM.
     * @return          Testo. Se la risorsa non &egrave; rilevata, restituisce
     *                  {@code null}.
     * @see             #listCandidateLocales
     */
    public static String getResourceAsString(String location, Locale locale) {
        String line;
        InputStream in = null;
        Reader reader = null;
        BufferedReader lineReader = null;
        StringWriter out = null;
        PrintWriter writer = null;
        
        try {
            in = ResourceTools.getResourceAsStream(location, locale);
            if (in == null) {
                return null;
            }
            
            reader = new InputStreamReader(in);
            in = null;
            lineReader = new BufferedReader(reader);
            reader = null;
            
            out = new StringWriter();
            writer = new PrintWriter(out);
            
            line = lineReader.readLine();
            while (line != null) {
                writer.println(line);
                line = lineReader.readLine();                
            }
            
            writer.flush();
        } catch (IOException ex) {
            myLogger.error(String.format("Failed to read resource %1$s.",
                    location), ex);            
            return null;                           
        } finally {
            in = IOTools.close(in);
            reader = IOTools.close(reader);
            lineReader = IOTools.close(lineReader);
            IOTools.close(out);
            writer = IOTools.close(writer);         
        }                  
        
        return out.toString();
    }
    
    /**
     * Restituisce la lista delle possibili locazioni delle versioni localizzate
     * di una risorsa.
     * 
     * @param  location Locazione della risorsa non localizzata.
     * @param  locale   Localizzazione. Pu&ograve; essere {@code null} per
     *                  considerare le sole localizzazioni impostata per il
     *                  thread corrente e di default di JVM.
     * @return          Collezione.              
     * @see             #listCandidateLocales
     */
    public static List<String> listCandidateLocalizedResourceLocations(
            String location, Locale locale) {
        int p;
        String ext;        
        List<String> locations;
        List<Locale> locales;
        
        if (StringUtils.isBlank(location)) {
            throw new ArgumentNullException("location");
        }
       
        locales = ResourceTools.listCandidateLocales(locale);

        p = location.lastIndexOf('.');
        if (p > 0 && p < location.length() - 1) {
            // Estraggo l'estensione
            ext = location.substring(p);
            location = location.substring(0, p);
        } else {
            ext = "";
        }
        
        locations = new ArrayList<String>();
        for (Locale loc : locales) {
            locations.add(location.concat("_").concat(loc.toString())
                    .concat(ext));            
        }
        
        // Risorsa non localizzata.
        locations.add(location.concat(ext));
        
        return locations;
    }   

    /**
     * Restituisce la lista delle possibili localizzazioni per la ricerca di una
     * risorsa localizzata.
     *    
     * <P>La ricerca della versione localizzata di una risorsa, considera le
     * seguenti localizzazioni alternative:</P>
     * 
     * <OL>
     * <LI>Localizzazione specificata con il parametro {@code locale}.
     * <LI>Localizzazione impostata per il thread corrente.
     * <LI>Localizzazione di default di JVM.
     * </OL>
     *     
     * @param  locale   Localizzazione. Pu&ograve; essere {@code null} per
     *                  considerare le sole localizzazioni impostata per il
     *                  thread corrente e di default di JVM.
     * @return          Collezione.
     * @see it.scoppelletti.programmerpower.threading.ThreadContext#setLocale
     */
    public static List<Locale> listCandidateLocales(Locale locale) {
        List<Locale> list;
        Set<String> checkSet;
        ThreadContext threadCtx = ThreadContext.tryGetCurrentThreadContext();
        
        list = new ArrayList<Locale>();
        checkSet = new HashSet<String>();
        if (locale != null) {
            ResourceTools.addCandidateLocales(list, locale, checkSet);
        }
        
        if (threadCtx != null) {
            ResourceTools.addCandidateLocales(list, threadCtx.getLocale(),
                    checkSet);
        }
        
        ResourceTools.addCandidateLocales(list, Locale.getDefault(), checkSet);
        
        return list;
    }
    
    /**
     * Aggiunge ad una lista le possibili localizzazioni di una risorsa 
     * corrispondenti ad una localizzazione.
     * 
     * @param list   Collezione.
     * @param locale Localizzazioni.
     */
    private static void addCandidateLocales(List<Locale> list,
            Locale locale, Set<String> checkSet) {
        // language[_country[_variant]]
        if (checkSet.add(locale.toString())) {
            list.add(locale);
        }
        
        if (!StringUtils.isBlank(locale.getVariant())) {
            // language_country
            locale = new Locale(locale.getLanguage(), locale.getCountry());
            if (checkSet.add(locale.toString())) {
                list.add(locale);
            }
        }
                
        if (!StringUtils.isBlank(locale.getCountry())) {
            // language
            locale = new Locale(locale.getLanguage());
            if (checkSet.add(locale.toString())) {
                list.add(locale);
            }        
        }       
    }

    /**
     * Emette le informazioni su un&rsquo;applicazione.
     * 
     * @param writer Flusso di scrittura.
     * @param about  Informazioni sull&rsquo;applicazione.
     */
    public static void write(Writer writer, ApplicationInfo about) {
        String value;
        if (writer == null) {
            throw new ArgumentNullException("writer");
        }
        if (about == null) {
            throw new ArgumentNullException("about");
        }
        
        PrintWriter printer;
        
        printer = new PrintWriter(writer);
        value = about.getApplicationName();
        if (!StringUtils.isBlank(value)) {
            printer.println(value);    
        }        
        
        value = about.getVersion();
        if (!StringUtils.isBlank(value)) {
            printer.println(value);
        }
         
        value = about.getPageUrl();
        if (!StringUtils.isBlank(value)) {
            printer.print("<");
            printer.print(value);
            printer.println("/>");
        }
        
        value = about.getCopyright();
        if (!StringUtils.isBlank(value)) {
            printer.println();
            printer.println(value);                
        }
        
        value = about.getLicense();
        if (!StringUtils.isBlank(value)) {
            printer.println();
            printer.println(value);                
        }
        
        printer.flush();        
    }
}

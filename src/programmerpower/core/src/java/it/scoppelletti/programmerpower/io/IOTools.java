/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.*;
import java.util.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;

/**
 * Funzioni di utilit&agrave; <ACRONYM TITLE="input/output">I/O</ACRONYM>.
 * 
 * @since 2.1.0
 */
public final class IOTools {

    /**
     * Nome del bean {@code MimetypesFileTypeMap} della mappa dei tipi MIME. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.  
     */
    public static final String BEAN_MIMETYPEMAP = "it-scoppelletti-mimetypeMap";

    private static final int BUFSIZE = 1024;
    private static final Logger myLogger = LoggerFactory.getLogger(
            IOTools.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private IOTools() {        
    }
    
    /**
     * Chiude un flusso.
     * 
     * <P>Il metodo {@code close} chiude un flusso ignorando l&rsquo;eventuale
     * eccezione inoltrata dall&rsquo;operazione (tale eccezione viene emessa
     * come segnalazione di log).<BR>
     * Il valore di ritorno {@code null} pu&ograve; essere utilizzato per
     * azzerare la stessa variabile {@code stream} in modo che tale variabile
     * faccia anche da indicatore di flusso aperto ({@code stream != null}) o
     * chiuso ({@code stream == null}).</P>
     * 
     * @param  <T>    Classe del flusso.
     * @param  stream Flusso. Se &egrave; {@code null}, si assume che il flusso
     *                sia gi&agrave; stato chiuso (o non sia mai stato aperto).
     * @return        {@code null}.
     */    
    public static <T extends Closeable> T close(T stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException|RuntimeException ex) {
                myLogger.error("Failed to close stream.", ex);
            }            
        }
        
        return null;
    }

    /**
     * Scarica gli eventuali byte scritti ma non ancora scaricati su un flusso
     * di scrittura.
     * 
     * <P>Il metodo {@code flush} scarica un flusso ignorando l&rsquo;eventuale
     * eccezione {@code IOException} inoltrata dall&rsquo;operazione (tale
     * eccezione viene emessa come segnalazione di log).</P>
     * 
     * @param out Flusso.
     */
    public static void flush(Flushable out) {
        try {
            out.flush();
        } catch (IOException ex) {
            myLogger.error("Failed to flush stream.", ex);
        }
    }
    
    /**
     * Copia un flusso su un altro.
     * 
     * @param in  Flusso di input.
     * @param out Flusso di output.
     */
    public static void copy(InputStream in, OutputStream out) throws
            IOException {
        int n;
        byte[] buf = new byte[IOTools.BUFSIZE];
        
        if (in == null) {
            throw new ArgumentNullException("in");
        }
        if (out == null) {
            throw new ArgumentNullException("out");
        }        
        
        n = in.read(buf, 0, IOTools.BUFSIZE);
        while (n > 0) {
            out.write(buf, 0, n);
            n = in.read(buf, 0, IOTools.BUFSIZE);
        }
        
        out.flush();        
    }
    
    /**
     * Cancella un direttorio e tutti i file e sotto-direttori in esso
     * contenuti.
     * 
     * @param  dir Direttorio.
     * @throws     it.scoppelletti.programmerpower.io.IOOperationException 
     */
    public static void deltree(File dir) {
        IOTools.deltree(dir, true);
    }
    
    /**
     * Cancella tutti i file e sotto-direttori contenuti in un direttorio.
     * 
     * @param  dir        Direttorio.
     * @param  deleteRoot Indica se cancellare anche il direttorio {@code dir}
     *                    oltre il suo contenuto. 
     */
    public static void deltree(File dir, boolean deleteRoot) {
        File file;
        File[] files;
        Deque<File> deletingStack = new ArrayDeque<File>();
        Queue<File> deletingQueue = new ArrayDeque<File>();
        
        if (dir == null) {
            throw new ArgumentNullException("dir");
        }

        // Un direttorio e' cancellabile solo se e' vuoto:
        // Visito l'albero dei file da cancellare scendendo progressivamente per
        // famiglie di livello in modo da costruire lo stack per la visita dello
        // stesso albero in ordine differito.
        file = dir;
        while (file != null) {            
            deletingStack.push(file);
            if (file.isDirectory()) {
                files = file.listFiles();
                deletingQueue.addAll(Arrays.asList(files));
            }
            
            file = deletingQueue.poll();
        }
        deletingQueue = null;
        
        // Cancello l'albero dei file visitandolo in ordine differito
        file = deletingStack.poll();
        while (file != null) {
            if (!deleteRoot && deletingStack.isEmpty()) {
                break;
            }
            
            if (file.exists()) {
                if (!file.delete()) {
                    throw new FileDeleteException(file.toString());
                }
            }            
            
            file = deletingStack.poll();
        }
    }

    /**
     * Verifica se un direttorio &egrave; vuoto.
     * 
     * @param  dir Direttorio.
     * @return     Esito della verifica.
     */
    public static boolean isDirectoryEmpty(File dir) {
        if (dir == null) {
            throw new ArgumentNullException("dir");
        }
        
        try {
            dir.list(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    throw new FileAlreadyExistException(name);
                }            
            });
        } catch (FileAlreadyExistException ex) {
            return false;
        }
        
        return true;
    }
}

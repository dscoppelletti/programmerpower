/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Eccezione nella deserializzazione.
 *
 * <P>L&rsquo;eccezione {@code DeserializeException} consente di inoltrare
 * un&rsquo;eccezione {@code InvalidObjectException} (dalla quale deriva) a
 * fronte di un altro tipo di eccezione rilevata durante la deserializzazione
 * di un oggetto.</P>
 * 
 * @since 1.0.0
 */
public class DeserializeException extends InvalidObjectException {
    private static final long serialVersionUID = 1;
    
    /**
     * Costruttore.
     * 
     * @param ex Eccezione originale.
     */
    public DeserializeException(Throwable ex) {
        super(ApplicationException.toString(ex));
        initCause(ex);
    }
}

/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Funzioni di utilit&agrave; sulle stringhe.
 *
 * @since 2.1.0
 */
public final class StringTools {  
       
    /**
     * Costruttore privato per classe statica.
     */
    private StringTools() {
    }
              
    /**
     * Restituisce una copia di una stringa nella quale sono soppressi i
     * caratteri di spaziatura iniziali.
     *
     * @param  s Stringa originale.
     * @return   Stringa elaborata. Se la stringa {@code s} &egrave;
     *           {@code null}, restituisce {@code null}.
     */
    public static String trimStart(String s) {
        int n, p;
        char c;
        
        if (s == null) {
            return null;
        }
        
        n = s.length();
        for (p = 0; p < n; p++) {
            c = s.charAt(p);
            if (!Character.isWhitespace(c)) {
                break;
            }            
        }
        if (p > 0) {
            return s.substring(p);
        }
        
        return s;
    }
    
    /**
     * Restituisce una copia di una stringa nella quale sono soppressi i
     * caratteri di spaziatura finali.
     *
     * @param  s Stringa originale.
     * @return   Stringa elaborata. Se la stringa {@code s} &egrave;
     *           {@code null}, restituisce {@code null}.
     */    
    public static String trimEnd(String s) {
        int n, p;
        char c;
        
        if (s == null) {
            return null;
        }
        
        n = s.length();
        for (p = n; p > 0; p--) {
            c = s.charAt(p - 1);
            if (!Character.isWhitespace(c)) {
                break;
            }
        }
        if (p < n) {
            return s.substring(0, p);
        }
        
        return s;
    }    
 
    /**
     * Verifica se due stringhe sono uguali.
     * 
     * <P>Il metodo {@code equals} della classe statica {@code StringTools}
     * pu&ograve; essere pi&ugrave; comodo da usare della coppia di metodi
     * {@code equals} e {@code equalsIgnoreCase} della classe {@code String}
     * quando l&rsquo;indicatore per ignorare o meno la differenza tra i
     * caratteri maiuscoli e minuscoli &egrave; un parametro variabile.</P>
     * 
     * @param s1         Prima stringa. Pu&ograve; essere {@code null}.
     * @param s2         Seconda stringa. Pu&ograve; essere {@code null}.
     * @param ignoreCase Indica se ignorare la differenza tra caratteri
     *                   maiuscoli e minuscoli.
     * @return           Esito della verifica.
     */
    public static boolean equals(String s1, String s2, boolean ignoreCase) {
        if (ignoreCase) {
            return StringUtils.equalsIgnoreCase(s1, s2);
        }
        
        return StringUtils.equals(s1, s2);
    }
    
    /**
     * Confronta due stringhe.
     * 
     * <P>Il metodo {@code compare} della classe statica {@code StringTools}
     * pu&ograve; essere pi&ugrave; comodo da usare della coppia di metodi
     * {@code compareTo} e {@code compareToIgnoreCase} della classe
     * {@code String} quando l&rsquo;indicatore per ignorare o meno la
     * differenza tra i caratteri maiuscoli e minuscoli &egrave; un parametro
     * variabile.</P>
     *  
     * @param s1         Prima stringa.
     * @param s2         Seconda stringa.
     * @param ignoreCase Indica se ignorare la differenza tra caratteri
     *                   maiuscoli e minuscoli.
     * @return           Risultato del confronto.
     */
    public static int compare(String s1, String s2, boolean ignoreCase) {
        if (s1 == null) {
            throw new ArgumentNullException("s1");
        }
        if (s2 == null) {
            throw new ArgumentNullException("s2");
        }
        
        if (ignoreCase) {
            return s1.compareToIgnoreCase(s2);
        }
        
        return s1.compareTo(s2);        
    }  
    
    /**
     * Converte una stringa in un vettore di codici Unicode corrispondente alla
     * sequenza di caratteri della stringa.
     * 
     * @param  s Stringa.
     * @return   Vettore. Se la stringa {@code s} &egrave; {@code null},
     *           restituisce un vettore di 0 elementi.
     */
    public static int[] toCodePointArray(String s) {
        int i, n, p;
        int[] v;
        
        if (s == null) {
            return new int[0];
        }
        
        n = s.codePointCount(0, s.length());
        v = new int[n];
        
        for (i = 0; i < n; i++) {
            p = s.offsetByCodePoints(0, i);
            v[i] = s.codePointAt(p);
        }
        
        return v;
    }          
 
    /**
     * Restituisce la concatenazione di due stringhe.
     * 
     * @param  s1 Prima stringa. Pu&ograve; essere {@code null}.
     * @param  s2 Seconda stringa. Pu&ograve; essere {@code null}.
     * @return    Stringa risultante. Se uno dei due parametri ({@code s1} o
     *            {@code s2}) &egrave; {@code null}, restituisce l&rsquo;altro
     *            parametro ({@code s2} o {@code s1}).
     */
    public static String concat(String s1, String s2) {
        if (s1 == null) {
            return s2;
        }
        if (s2 == null) {
            return s1;
        }
        
        return s1.concat(s2);
    }   
    
    /**
     * Sostituisce in una stringa tutte le occorrenze di un carattere con un
     * altro carattere.
     *  
     * @param buf     Stringa.
     * @param oldChar Carattere da sostituire.
     * @param newChar Carattere sostituito.
     */
    public static void replace(StringBuilder buf, char oldChar, char newChar) {
        int p;
        String old;
        
        if (buf == null) {
            throw new ArgumentNullException("buf");
        }
        
        old = String.valueOf(oldChar);
        p = buf.indexOf(old);
        while (p >= 0) {
            buf.setCharAt(p, newChar);
            p = buf.indexOf(old, p + 1);
        }
    }
    
    /**
     * Rimuove in una stringa tutte le occorrenze di un carattere.
     * 
     * @param buf Stringa.
     * @param c   Carattere da rimuovere.
     */
    public static void removeAll(StringBuilder buf, char c) {
        int p;
        String s;
        
        if (buf == null) {
            throw new ArgumentNullException("buf");
        }
        
        s = String.valueOf(c);
        p = buf.lastIndexOf(s);
        while (p >= 0) {
            buf.deleteCharAt(p);
            p = buf.lastIndexOf(s);
        }
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.threading.*;

/**
 * Funzioni di utilit&agrave; sul tempo.
 * 
 * @since 2.1.0
 */
public final class TimeTools {

    /**
     * Valore di un componente non noto di un istante. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final int NA = -1;
    
    /**
     * Nome della propriet&agrave; nell&rsquo;insieme dei file di
     * propriet&agrave; con base del nome
     * {@code it.scoppelletti.programmerpower.types.TimeTools} nel quale
     * pu&ograve; essere impostato l&rsquo;identificatore del fuso orario
     * corrispondente ad una localizzazione. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #getTimeZone(Locale)
     */
    public static final String PROP_TIMEZONE = "TZ";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            TimeTools.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private TimeTools() {        
    }
    
    /**
     * Restituisce l&rsquo;istante corrente secondo la localizzazione e il fuso
     * orario per il thread corrente.
     * 
     * @return Calendario. Se il contesto del thread non &egrave; stato
     *         impostato, considera la localizzazione di default di JVM e il
     *         fuso orario a questa associato.         
     * @see #getTimeZone(Locale)
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     * @see it.scoppelletti.programmerpower.threading.ThreadContext#getLocale
     * @see it.scoppelletti.programmerpower.threading.ThreadContext#getTimeZone    
     */
    public static Calendar getNow() {
        Locale locale;
        TimeZone tz;
        ThreadContext threadCtx = ThreadContext.tryGetCurrentThreadContext();
        
        if (threadCtx != null) {
            locale = threadCtx.getLocale();
            tz = threadCtx.getTimeZone();
        } else {
            locale = Locale.getDefault();
            tz = TimeTools.getTimeZone(locale);
        }
        
        return Calendar.getInstance(tz, locale);
    }
    
    /**
     * Restituisce il fuso orario per il thread corrente.
     *  
     * @return Fuso orario. Se il contesto del thread non &egrave; stato
     *         impostato, restituisce il fuso orario associato alla
     *         localizzazione di default di JVM.
     * @see #getTimeZone(Locale)
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     * @see it.scoppelletti.programmerpower.threading.ThreadContext#getTimeZone
     */    
    public static TimeZone getTimeZone() {
        ThreadContext threadCtx = ThreadContext.tryGetCurrentThreadContext();
        
        return (threadCtx != null) ? threadCtx.getTimeZone() :
            TimeTools.getTimeZone(Locale.getDefault());        
    }
    
    /**
     * Restituisce il fuso orario associato ad una localizzazione.
     * 
     * <P>Ad ogni localizzazione pu&ograve; essere associato un fuso orario
     * impostando la propriet&agrave; {@code TZ} nell&rsquo;insieme dei file di
     * propriet&agrave; con base del nome 
     * {@code it.scoppelletti.programmerpower.types.TimeTools}.</P>
     * 
     * @param  locale Localizzazione.
     * @return        Fuso orario. Se alla localizzazione {@code locale} non
     *                &egrave; stato associato il fuso orario, restituisce il
     *                fuso orario di default di JVM.
     */
    public static TimeZone getTimeZone(Locale locale) {
        String code;
        TimeZone tz = null;
        ResourceBundle res;
        
        if (locale == null) {
            throw new ArgumentNullException("locale");
        }
        
        try {
            res = ResourceBundle.getBundle(TimeTools.class.getCanonicalName(),
                    locale);
            code = res.getString(TimeTools.PROP_TIMEZONE);            
        } catch (MissingResourceException ex) {
            code = null;
        }
        
        if (!StringUtils.isBlank(code)) {
            tz = TimeZone.getTimeZone(code);            
        }
        if (tz == null) {
            tz = TimeZone.getDefault();
        }
        
        myLogger.trace("TimeZone {} mapped to Locale {} (orginal ID: {}).",
                new Object[] { tz.getID(), locale.toString(), code });
        
        return tz;
    }       
}

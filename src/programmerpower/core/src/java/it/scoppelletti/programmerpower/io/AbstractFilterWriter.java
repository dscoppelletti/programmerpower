/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Flusso di filtro in scrittura.
 * 
 * @since 1.0.0
 */
public abstract class AbstractFilterWriter extends Writer {
    private Writer myWriter = null;
    
    /**
     * Costruttore.
     * 
     * @param writer Flusso di scrittura sottostante.
     * @see          #setWriter
     */
    protected AbstractFilterWriter(Writer writer) {
        super(writer);        
    }
    
    /**
     * Chiude il flusso.
     */
    @Override
    public void close() throws IOException {
        if (myWriter != null) {
            myWriter.close();
            myWriter = null;
        }
    }
    
    /**
     * Imposta il flusso di scrittura sottostante.
     * 
     * <P>Le classi derivate devono eseguire il metodo {@code setWriter}
     * (normalmente nel costruttore) per impostare il flusso di scrittura
     * sottostante che altrimenti risulter&agrave; chiuso rendendo
     * inutilizzabile il filtro.</P>
     *  
     * @param writer Flusso di scrittura.
     */
    protected final void setWriter(Writer writer) {
        if (writer == null) {
            throw new ArgumentNullException("writer");
        }
        
        myWriter = writer;
    }
    
    /**
     * Scrive un vettore di caratteri.
     * 
     * @param cbuf Vettore di caratteri.
     * @param off  Indice del primo carattere da scrivere.
     * @param len  Numero di caratteri da scrivere.
     */
    @Override
    public void write(char cbuf[], int off, int len) throws IOException {
        int i;
        
        if (cbuf == null) {
            throw new ArgumentNullException("cbuf");
        }
        if (off < 0 || off > cbuf.length || len < 0 ||
                off + len > cbuf.length || off + len < 0) {
                throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return;
        }
        
        checkClosed();
        
        for (i = 0; i < len; i++) {
            writeImpl(cbuf[off + i]);
        }
    }    
    
    /**
     * Scrive un carattere.
     * 
     * @param c Carattere.
     */
    protected abstract void writeImpl(char c) throws IOException;
        
    /**
     * Scarica sul flusso sottostante gli eventuali caratteri scritti ma non
     * ancora scaricati.
     */
    @Override
    public void flush() throws IOException {
        checkClosed();

        myWriter.flush();
    }
    
    /**
     * Verifica che il flusso non sia chiuso.
     */
    protected final void checkClosed() throws IOException {
        IOResources res;
        
        if (myWriter == null) {
            res = new IOResources();
            throw new IOException(res.getStreamClosedException());
        }
    }    
}

/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;        

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse Reflection.
 * 
 * @since 1.0.0
 */
public final class ReflectionResources extends ResourceWrapper {

    /**
     * Costruttore.
     */
    public ReflectionResources() {
    }

    /**
     * Eccezione per risorsa non trovata.
     * 
     * @param  name Nome della risorsa.
     * @return      Testo.
     */
    public String getResourceNotFoundException(String name) {
        return format("ResourceNotFoundException", name);
    }
    
    /**
     * Eccezione per conversione di classe non ammessa.
     * 
     * @param  castingClassName Nome della classe da convertire.
     * @param  targetClassName  Nome della classe di destinazione.
     * @return                  Testo.
     */
    String getInvalidCastException(String castingClassName,
            String targetClassName) {    
        return format("InvalidCastException", castingClassName,
                targetClassName);
    }
    
    /**
     * Eccezione per metodo astratto o di interfaccia non implementato.
     * 
     * @param  className  Nome della classe astratta o interfaccia.
     * @param  methodName Nome del metodo.
     * @return            Testo.
     */
    String getNotImplementedException(String className, String methodName) {
        return format("NotImplementedException", className, methodName);
    }    
}

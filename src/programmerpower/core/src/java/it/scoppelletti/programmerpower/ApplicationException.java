/*
 * Copyright (C) 2008-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

import java.lang.reflect.*;
import org.apache.commons.lang3.*;
import org.xml.sax.*;

/**
 * Eccezione generica.
 * 
 * @since 1.0.0
 */
public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 1;
    
    /**
     * Costruttore.
     */
    protected ApplicationException() {
    }  
    
    /**
     * Costruttore.
     * 
     * @param message Messaggio.
     */
    public ApplicationException(String message) {
        super(message);
    }    
    
    /**
     * Costruttore.
     * 
     * @param message Messaggio.
     * @param ex      Eccezione originale.
     * @since         2.1.0
     */
    public ApplicationException(String message, Throwable ex) {
        super(message, ex);
    } 
    
    /**
     * Costruttore.
     *
     * <P>Questo sovraccarico del costruttore pu&ograve; essere utilizzato per
     * ridurre un&rsquo;eccezione {@code Exception} ad un&rsquo;eccezione
     * {@code RuntimeException}.</P>
     * 
     * @param ex Eccezione originale.
     */
    public ApplicationException(Throwable ex) {
        super(ApplicationException.toString(ex), ex);
    }                   
    
    /**
     * Rappresenta un&rsquo;eccezione con una stringa.
     * 
     * <P>La rappresentazione dell&rsquo;eccezione &egrave; rilevata dalle
     * seguenti alternative:</P>
     * 
     * <OL>
     * <LI>Messaggio (non vuoto) restituito dal metodo
     * {@code getLocalizedMessage} dell&rsquo;eccezione.
     * <LI>Messaggio (non vuoto) restituito dal metodo {@code getMessage}
     * dell&rsquo;eccezione.
     * <LI>Stringa (non vuota) restituita dal metodo {@code toString}
     * dell&rsquo;eccezione.
     * <LI>Nome della classe dell&rsquo;eccezione (completo di pacchetto).   
     * </OL>
     * 
     * @param  ex Eccezione.
     * @return    Stringa. Se l&rsquo;eccezione {@code ex} &egrave; {@code null},
     *            restituisce la stringa {@code "null"}.
     */
    public static String toString(Throwable ex) {
        String msg;
        SAXParseException xmlEx;
        CommonResources res;
        
        if (ex == null) {
            return "null";
        }
        
        if (ex instanceof InvocationTargetException && 
                ex.getCause() != null) {
            msg = ApplicationException.getMessage(ex.getCause());
        } else {
            msg = ApplicationException.getMessage(ex);
        }
        
        if (ex instanceof SAXParseException) {
            // Completo il messaggio con le informazioni per la locazione
            // dell'errore nel documento XML
            xmlEx = (SAXParseException) ex;            
            msg = String.format("%1$s [publicId=%2$s, systemId=%3$s, " +
                    "line=%4$d, column=%5$d]", msg, xmlEx.getPublicId(),
                    xmlEx.getSystemId(), xmlEx.getLineNumber(),
                    xmlEx.getColumnNumber());
        } else if (ex instanceof ClassNotFoundException) {
            // Il messaggio originale si limita al nome della classe:
            // Lo sostituisco almeno con una frase.
            res = new CommonResources();
            msg = res.getClassNotFoundException(msg);
        }
        
        return msg;
    }
    
    /**
     * Rappresenta un&rsquo;eccezione con una stringa.
     * 
     * @param  ex Eccezione.
     * @return    Stringa.
     */
    private static String getMessage(Throwable ex) {
        String s;
                
        s = ex.getLocalizedMessage();
        if (!StringUtils.isBlank(s)) {
            return s;
        }
        
        s = ex.getMessage();
        if (!StringUtils.isBlank(s)) {
            return s;
        }
        
        s = ex.toString();
        if (!StringUtils.isBlank(s)) {
            return s;
        }
        
        return ex.getClass().getCanonicalName();
    }
}

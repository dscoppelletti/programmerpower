/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse comuni.
 * 
 * @since 1.0.0
 */
public final class CommonResources extends ResourceWrapper {
    
    /**
     * Costruttore.
     */
    public CommonResources() {
    }
    
    /**
     * Eccezione per parametro {@code null} non valido.
     * 
     * @param  paramName Nome del parametro.
     * @return           Testo.
     */
    String getArgumentNullException(String paramName) {        
        return format("ArgumentNullException", paramName);
    }

    /**
     * Eccezione per valore di parametro non compreso tra quelli validi.
     * 
     * @param  paramName   Nome del parametro.
     * @param  actualValue Valore del parametro.
     * @param  validValues Insieme dei valori validi.
     * @return             Testo.
     */
    String getArgumentOutOfRangeException(String paramName,
            String actualValue, String validValues) {
        return format("ArgumentOutOfRangeException", paramName, actualValue,
                validValues);
    }

    /**
     * Eccezione valore di ritorno {@code null} non valido.
     * 
     * @param objectId   Identificatore dell&rsquo;oggetto.
     * @param methodName Nome del metodo.
     * @return           Testo.
     * @since            2.0.0
     */
    public String getReturnNullException(String objectId, String methodName) {
        return format("ReturnNullException", objectId, methodName);
    }  
    
    /**
     * Eccezione per propriet&agrave; di un oggetto non impostata.
     * 
     * @param  objectId     Identificatore dell&rsquo;oggetto.
     * @param  propertyName Nome della propriet&agrave;.
     * @return              Testo.
     * @since               2.0.0
     */
    public String getPropertyNotSetException(String objectId,
            String propertyName) {
        return format("PropertyNotSetException", objectId, propertyName);
    }  
        
    /**
     * Eccezione per oggetto non rilevato.
     * 
     * @param  objectId Identificatore dell&rsquo;oggetto.
     * @return          Testo.
     * @since           2.1.0
     */
    public String getObjectNotFoundException(String objectId) {
        return format("ObjectNotFoundException", objectId);
    }

    /**
     * Eccezione per oggetto dupplicato.
     * 
     * @param  objectId Identificatore dell&rsquo;oggetto.
     * @return          Testo.
     */
    public String getObjectDuplicateException(String objectId) {
        return format("ObjectDuplicateException", objectId);
    }

    /**
     * Eccezione per utilizzo di oggetto rilasciato.
     * 
     * @param  objectId Identificatore dell&rsquo;oggetto.
     * @return          Testo.
     */
    String getObjectDisposedException(String objectId) {
        return format("ObjectDisposedException", objectId);
    }
    
    /**
     * Eccezione per utilizzo non corretto di oggetto a creazione automatica.
     * 
     * @param  objectId Identificatore dell&rsquo;oggetto.
     * @return          Testo.
     */
    String getObjectAutoCreateException(String objectId) {
        return format("ObjectAutoCreateException", objectId);
    }
    
    /**
     * Eccezione per classe non trovata.
     * 
     * @param  className Nome della classe.
     * @return           Testo.
     */
    String getClassNotFoundException(String className) {    
        return format("ClassNotFoundException", className);
    }
    
    /**
     * Eccezione per lock di risorsa fallito.
     * 
     * @param  name Nome della risorsa..
     * @return      Testo.
     */
    String getLockAcquireException(String name) {
        return format("LockAcquireException", name);
    }      
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.util.*;
import org.apache.commons.collections4.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Funzioni di utilit&agrave; sulle collezioni.
 * 
 * @since 2.1.0
 */
public final class CollectionTools {

    /**
     * Costruttore privato per classe statica.
     */
    private CollectionTools() {      
    }
        
    /**
     * Applica gli aggiornamenti su una collezione.
     * 
     * @param target  Collezione da aggiornare.
     * @param updated Copia della collezione aggiornata.
     */
    public static <T> void update(Set<T> target, Set<? extends T> updated) {
        if (target == null) {
            throw new ArgumentNullException("target");
        }
        
        if (CollectionUtils.isEmpty(updated)) {
            target.clear();
            return;
        }
            
        target.retainAll(updated);
        target.addAll(updated);
    }
    
    /**
     * Applica gli aggiornamenti su una collezione.
     * 
     * @param target  Collezione da aggiornare.
     * @param updated Copia della collezione aggiornata.
     */
    public static <K, V> void update(Map<K, V> target,
            Map<? extends K, ? extends V> updated) {
        Set<K> deletingKeys;
        K updatedKey;
        V updatingValue, updatedValue;
        
        if (target == null) {
            throw new ArgumentNullException("target");
        }
        
        if (MapUtils.isEmpty(updated)) {
            target.clear();
            return;
        }
        
        deletingKeys = new HashSet<K>();            
        for (K key : target.keySet()) {
            if (!updated.containsKey(key)) {
                deletingKeys.add(key);
            }
        }            
        for (K key : deletingKeys) {
            target.remove(key);
        }
        
        for (Map.Entry<? extends K, ? extends V> updatedEntry :
            updated.entrySet()) {
            updatedKey = updatedEntry.getKey();
            updatedValue = updatedEntry.getValue();
            if (!target.containsKey(updatedKey)) {
                target.put(updatedKey, updatedValue);
                continue;
            }
            
            updatingValue = target.get(updatedKey);
            if (!Objects.equals(updatedValue, updatingValue)) {
                target.put(updatedKey, updatedValue);
            }
        }        
    }    

    /**
     * Legge un valore da una collezione.
     * 
     * @param  key          Chiave.
     * @param  map          Collezione. Pu&ograve; essere {@code null}.
     * @param  defaultValue Valore di default.
     * @return              Valore.
     */
    public static String getString(String key, Map<String, ?> map,
            String defaultValue) {
        Object obj;
        
        if (StringUtils.isBlank(key)) {
            throw new ArgumentNullException("name");
        }
        if (map == null) {
            return defaultValue;
        }
        
        if (!map.containsKey(key)) {
            return defaultValue;
        }
        
        obj = map.get(key);
        if (obj == null) {
            return null;
        }
               
        return obj.toString();
    }
    
    /**
     * Legge un valore da una collezione.
     * 
     * @param  key          Chiave.
     * @param  map          Collezione. Pu&ograve; essere {@code null}.
     * @param  defaultValue Valore di default.
     * @return              Valore.
     */
    public static boolean getBoolean(String key, Map<String, ?> map,
            boolean defaultValue) {
        Object obj;
        
        if (StringUtils.isBlank(key)) {
            throw new ArgumentNullException("name");
        }
                
        if (map == null) {
            return defaultValue;
        }
        
        if (!map.containsKey(key)) {
            return defaultValue;
        }
        
        obj = map.get(key);
        if (obj == null) {
            return false;
        }
        
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
                
        return Boolean.parseBoolean(obj.toString());
    }            
}

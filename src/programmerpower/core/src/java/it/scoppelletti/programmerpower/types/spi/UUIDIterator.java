/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types.spi;

import java.util.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Collezione di <ACRONYM TITLE="Universally Unique IDentifier">UUID</ACRONYM>.
 */
final class UUIDIterator implements Iterator<UUID> {
    private final short myClockSeq;
    private final long myNodeID;
    private long myTimestamp;        
    private int myCount;
    
    /**
     * Costruttore.
     * 
     * @param timestamp     Timestamp.
     * @param clockSequence Sequenza di clock.
     * @param nodeID        Id&#46; del nodo.
     * @param count         Numero di UUID.
     */
    UUIDIterator(long timestamp, short clockSequence, long nodeID, int count) {
        myTimestamp = timestamp; 
        myClockSeq = clockSequence;
        myNodeID = nodeID;
        myCount = count;
    }
    
    /**
     * Verifica se ci sono ancora elementi da visitare.
     * 
     * @return Esito della verifica.
     */
    public boolean hasNext() {
        return (myCount > 0);
    }

    /**
     * Restituisce l&rsquo;elemento successivo.
     * 
     * @return Elemento.
     */
    public UUID next() {
        UUID value;
        
        if (myCount <= 0) {
            throw new NoSuchElementException();
        }
    
        value = UUIDState.calcUUID(myTimestamp, myClockSeq, myNodeID);
        myCount--;            
        myTimestamp++;
        
        return value;
    }

    /**
     * Cancella l&rsquo;elemento corrente.
     */
    public void remove() {
        throw new NotImplementedException(getClass().getName(), "remove");        
    }
}

/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.resources;

import java.text.*;
import java.util.*;

/**
 * Contenitore di risorse localizzate.
 * 
 * <P>La classe {@code ResourceWrapper} fa da <I>wrapper</I> ad un oggetto
 * {@code ResourceBundle} e pu&ograve; essere utilizzata come classe base per
 * derivare una classe <I>fortemente tipizzata</I> per la lettura delle
 * risorse.<BR>
 * La base del nome del contenitore di risorse {@code ResourceBundle} coincide
 * con il nome completo della classe derivata da {@code ResourceWrapper} (es.
 * {@code it.scoppelletti.programmerpower.io.IOResources}).</P> 
 * 
 * <H4>1. Una classe fortemente tipizzata</H4>
 *
 * <P>La classe {@code ResourceBundle} consente di leggere delle risorse (es.
 * da un file di propriet&agrave; {@code Messages.properties}) specificando la
 * chiave delle risorse come parametro dei metodi di lettura.</P>
 *
 * <BLOCKQUOTE><PRE>
 * # Messages.properties
 * DivisionByZero=Division by zero error.
 * </PRE></BLOCKQUOTE>
 * 
 * <BLOCKQUOTE><PRE>
 * int remainder(int num, int den) {
 *     ResourceBundle res = ResourceBundle.getBundle("Messages");
 *
 *     if (den == 0) {
 *         throw new RuntimeException(res.getString("DivisionByZero"));
 *     }
 *
 *     return num % den;
 * }
 * </PRE></BLOCKQUOTE>
 * 
 * <P>&Egrave; molto facile commettere errori di scrittura della chiave delle
 * risorse; ad esempio, anzich&eacute; {@code "DivisionByZero"}, si potrebbero
 * scrivere delle varianti come {@code "divisionByZero"},
 * {@code "division-by-zero"}, {@code "division_by_zero"}, etc. Questo tipo di
 * errori vengono rilevati solo a run-time e si propagano molto facilmente nel
 * codice sorgente utilizzando il <I>copy-and-paste</I> come metodo di scrittura
 * del codice.<BR>
 * Nell&rsquo;esempio precedente, inoltre, la risorsa viene letta solo nel caso
 * di un errore da segnalare, quindi un eventuale errore di scrittura della
 * chiave non sar&agrave; riproducibile sistematicamente a run-time, ma solo in
 * dipendenza dei valori dei parametri del metodo {@code remainder}.</P>
 * 
 * <P>Una classe <I>fortemente tipizzata</I> derivata dalla classe
 * {@code ResourceWrapper} presenta i seguenti vantaggi:</P>
 * 
 * <OL>
 * <LI>Gli errori di scrittura del codice che utilizza la classe vengono
 * rilevati dalla compilazione.
 * <LI>&Egrave; possibile sfruttare le funzioni di autocompletamento del codice
 * comuni negli editor orientati alla programmazione.
 * </OL>
 * 
 * <BLOCKQUOTE><PRE>
 * public class Messages extends ResourceWrapper {
 *     public Messages() {
 *     }
 *
 *     public String getDivisionByZero() {
 *         return getString("DivisionByZero");
 *     }
 * }
 * </PRE></BLOCKQUOTE>
 * 
 * <BLOCKQUOTE><PRE>
 * int remainder(int num, int den) {
 *     Messages res = new Messages();
 *
 *     if (den == 0) {
 *         throw new RuntimeException(res.getDivisionByZero());
 *     }
 *
 *     return num % den;
 * }
 * </PRE></BLOCKQUOTE>
 *  
 * <H5>1.1. I parametri nei messaggi</H5>
 *
 * <P>Le risorse utilizzate per la localizzazione dei messaggi per
 * l&rsquo;utente includono talvolta dei parametri con la sintassi supportata
 * dalla classe {@code MessageFormat}.</P>
 *
 * <BLOCKQUOTE><PRE>
 * # Messages.properties
 * Age={0} is {1} years old.
 * </PRE></BLOCKQUOTE>
 *
 * <BLOCKQUOTE><PRE>
 * void age() {
 *     ResourceBundle res = ResourceBundle.getResource("Messages");
 *
 *     System.out.println(MessageFormat.format(res.getString("Age"), "Joe", 20));
 * }
 * </PRE></BLOCKQUOTE>
 *
 * <P>In questo caso, al problema della corretta scrittura della chiave della
 * risorsa, si aggiunge il problema che bisogna sempre dedurre dal testo della
 * risorsa il significato, il numero, il tipo e l&rsquo;ordine dei parametri:
 * gli errori sono quindi molto comuni.</P>
 *
 * <P>Una classe derivata dalla classe {@code ResourceWrapper} pu&ograve;
 * esporre le risorse attraverso dei metodi di lettura con una firma che
 * riproduce i parametri richiesti.</P>
 *
 * <BLOCKQUOTE><PRE>
 * public class Messages extends ResourceWrapper { 
 *     public Messages() {
 *     }
 *
 *     public String getAge(String name, int age) {
 *        return format("Age", name, age);
 *     }
 * }
 * </PRE></BLOCKQUOTE>
 *
 * <BLOCKQUOTE><PRE>
 * void age() {
 *     Messages res = new Messages();
 *
 *     System.out.println(res.getAge("Joe", 20));
 * }
 * </PRE></BLOCKQUOTE>
 * 
 * <H4>2. Creazione delle istanze</H4>
 * 
 * <P>Un oggetto {@code ResourceWrapper} dovrebbe essere istanziato quando
 * devono essere lette le risorse e poi rilasciato: eseguire pi&ugrave; volte il
 * costruttore di un oggetto {@code ResourceWrapper} non influisce negativamente
 * sulle prestazioni del sistema perch&egrave; la lettura delle risorse sfrutta
 * le politiche di cache implementate dal metodo statico {@code getBundle} della
 * classe {@code ResourceBundle}; anzi, istanziare un oggetto
 * {@code ResourceWrapper} all&rsquo;avvio di un&rsquo;applicazione e detenerne
 * il riferimento per tutta la durata dell&rsquo;esecuzione della stessa,
 * significa detenere anche il riferimento all&rsquo;oggetto
 * {@code ResourceBundle} sottostante inibendo cos&igrave; le politiche di cache
 * implementate da <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM>.</P>   
 * 
 * @since 1.0.0
 */
public abstract class ResourceWrapper {
    private final ResourceBundle myResources;

    /**
     * Costruttore. 
     */
    protected ResourceWrapper() {
        myResources = ResourceBundle.getBundle(getClass().getCanonicalName(),
                ResourceTools.getLocale());            
    }
    
    /**
     * Restituisce una risorsa stringa.
     *
     * <P>Le classi derivate devono utilizzare il metodo {@code getString}
     * per pubblicare i metodi di lettura delle risorse.</P>
     *  
     * @param  key Chiave.
     * @return     Valore.
     */
    protected final String getString(String key) {
        return myResources.getString(key);
    }
    
    /**
     * Costruisce la stringa risultante dalla sostituzione in una risorsa
     * stringa dei parametri previsti.
     * 
     * <P>Le classi derivate devono utilizzare il metodo {@code format}
     * per pubblicare i metodi di formattazione delle risorse con parametri.</P>
     *  
     * @param key  Chiave.
     * @param args Parametri.
     * @return     Valore.
     */
    protected final String format(String key, Object ... args) {
        MessageFormat msg;
        
        msg = new MessageFormat(myResources.getString(key));
 
        return msg.format(args);
    }
}

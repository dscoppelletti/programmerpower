/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

import java.lang.annotation.*;

/**
 * Indicazione di classe non derivabile o di metodo non virtuale.
 * 
 * <P>L&rsquo;implementazione di alcuni modelli di programmazione (ad esempio
 * <ACRONYM TITLE="Aspect Oriented Programming">AOP</ACRONYM>) utilizzano delle
 * librerie per la generazione e/o modifica a run-time del bytecode delle classi
 * (ad esempio <ACRONYM TITLE="Java programming Assistant">javassist</ACRONYM>,
 * <ACRONYM TITLE="Code Generation Library">cglib</ACRONYM>).<BR>
 * L&rsquo;applicazione di queste tecniche pu&ograve; limitare la
 * possibilit&agrave; di applicare il qualificatore {@code final} per dichiarare
 * le classi non derivabili o i metodi non virtuali; in questi casi &egrave;
 * possibile applicare l&rsquo;annotazione {@code Final} in sostituzione del
 * qualificatore {@code final}.</P>
 *   
 * <BLOCKQUOTE CLASS="note">
 * Allo stato dell&rsquo;arte, non sono previsti sistemi per la verifica del
 * rispetto dell&rsquo;annotazione {@code Final}.
 * </BLOCKQUOTE>
 *  
 * @since 2.0.0
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.TYPE , ElementType.METHOD })
public @interface Final {
}

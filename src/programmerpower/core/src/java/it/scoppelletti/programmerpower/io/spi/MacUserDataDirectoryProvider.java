/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io.spi;

import java.io.File;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.JVMTools;

/**
 * Implementazione del provider {@code UserDataDirectoryProvider} per i sistemi
 * operativi <ACRONYM TITLE="Macintosh">Mac</ACRONYM>
 * <ACRONYM TITLE="Operative System">OS</ACRONYM>.
 * 
 * @since 2.1.0
 */
public final class MacUserDataDirectoryProvider implements
        UserDataDirectoryProvider {
    
    /**
     * Tag per il riconoscimento dei sistemi operativi Mac OS. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     * 
     * <P>Se il valore della propriet&agrave; di sistema {@code os.name} include
     * il tag (ignore-case), il sistema operativo &egrave; riconosciuto.</CODE>.
     */
    public static final String TAG_OSNAME = "Mac";

    /**
     * Nome del direttorio direttorio dei dati riservato all&rsquo;utente che
     * sta eseguendo il programma. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String DIR_LIBRARY = "Library";
    
    /**
     * Nome del direttorio dedicato al sistema Programmer Power. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String DIR_NAME = "it-scoppelletti-programmerpower";
        
    private static final Logger myLogger = LoggerFactory.getLogger(
            MacUserDataDirectoryProvider.class);
    
    /**
     * Costruttore.
     */
    public MacUserDataDirectoryProvider() {        
    }
    
    public File getDirectory() {
        String value;
        File dir;
        
        value = JVMTools.getProperty(JVMTools.PROP_OSNAME);
        if (!StringUtils.containsIgnoreCase(value,
                MacUserDataDirectoryProvider.TAG_OSNAME)) {
            return null;
        }
        
        value = JVMTools.getProperty(JVMTools.PROP_USERHOME);
        if (StringUtils.isBlank(value)) {
            myLogger.warn("System property {} not set.",
                    JVMTools.PROP_USERHOME);
            return null;            
        }
        
        dir = new File(value);
        dir = new File(dir, MacUserDataDirectoryProvider.DIR_LIBRARY);
        dir = new File(dir, MacUserDataDirectoryProvider.DIR_NAME);
        
        return dir;
    }
}

/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;        

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse per l&rsquo;<ACRONYM TITLE="input/output">I/O</ACRONYM>.
 *      
 * @since 1.0.0
 */
public final class IOResources extends ResourceWrapper {

    /**
     * Costruttore.
     */
    public IOResources() {
    }

    /**
     * Eccezione per file non esistente.
     * 
     * @param  path Percorso del file.
     * @return      Testo.
     */
    String getFileNotExistException(String path) {
        return format("FileNotExistException", path);
    }

    /**
     * Eccezione per file gi&agrave; esistente.
     * 
     * @param  path Percorso del file.
     * @return      Testo.
     * @since       2.1.0
     */
    public String getFileAlreadyExistException(String path) {
        return format("FileAlreadyExistException", path);
    }
    
    /**
     * Eccezione nella creazione di un direttorio.
     * 
     * @param  path Percorso del direttorio.
     * @return      Testo
     */
    String getFileCreateException(String path) {
        return format("FileCreateException", path);
    }
    
    /**
     * Eccezione nella cancellazione di un file.
     * 
     * @param  path Percorso del file.
     * @return      Testo
     */
    String getFileDeleteException(String path) {
        return format("FileDeleteException", path);
    }
    
    /**
     * Eccezione per file corrotto.
     * 
     * @param  path Percorso del file.
     * @return      Testo.
     */
    String getFileCorruptedException(String path) {
        return format("FileCorruptedException", path);
    }
        
    /**
     * Eccezione per file non contenuto in un direttorio.
     * 
     * @param  path Percorso del file.
     * @param  dir  Direttorio.
     * @return      Testo.
     */
    public String getFileNotInDirectoryException(String path, String dir) {
        return format("FileNotInDirectoryException", path, dir);
    }
    
    /**
     * Eccezione per percorso non assoluto.
     * 
     * @param  path Percorso del file.
     * @return      Testo.
     */
    String getPathNotAbsoluteException(String path) {
        return format("PathNotAbsoluteException", path);
    }
    
    /**
     * Eccezione per accesso a flusso chiuso.
     * 
     * @return Testo.
     */
    public String getStreamClosedException() {
        return getString("StreamClosedException");
    }
            
    /**
     * Messaggio per rilevazione di un file.
     * 
     * @param  path Percorso del file.
     * @return      Testo.
     */
    public String getFileFoundMessage(String path) {    
        return format("FileFoundMessage", path);
    }
    
    /**
     * Messaggio per sovrascrittura di un file esistente.
     * 
     * @param  path Percorso del file.
     * @return      Testo.
     */
    public String getFileOverwriteMessage(String path) {    
        return format("FileOverwriteMessage", path);
    }
    
    /**
     * Messaggio per rilevazione di un direttorio.
     * 
     * @param  path Percorso del direttorio.
     * @return      Testo.
     * @since       2.1.0
     */
    public String getDirectoryFoundMessage(String path) {
        return format("DirFoundMessage", path);
    }
    
    /**
     * Messaggio per creazione di un direttorio.
     * 
     * @param  path Percorso del direttorio.
     * @return      Testo.
     * @since       2.1.0
     */
    public String getDirectoryCreatedMessage(String path) {
        return format("DirCreatedMessage", path);
    }        
    
    /**
     * Messaggio per direttorio non vuoto.
     * 
     * @param  path Percorso del direttorio.
     * @return      Testo.
     * @since       2.1.0
     */
    public String getDirectoryNotEmptyMessage(String path) {
        return format("DirNotEmptyMessage", path);
    }          
}

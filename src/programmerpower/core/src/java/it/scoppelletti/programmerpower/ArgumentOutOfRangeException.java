/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

import java.io.*;
import java.util.*;

/**
 * Eccezione per valore di parametro non valido.
 *
 * @since 1.0.0
 */
public class ArgumentOutOfRangeException extends IllegalArgumentException {
    private static final long serialVersionUID = 1;
    
    /** 
     * @serial Nome del parametro.
     */    
    private final String myParamName;
    
    private transient Object myActualValue;
    private transient Object myValidValues;
    
    /**
     * Costruttore.
     *
     * @param paramName   Nome del parametro.
     * @param actualValue Valore del parametro.
     * @param validValues Insieme dei valori validi.
     */
    public ArgumentOutOfRangeException(String paramName, Object actualValue,
            Object validValues) {
         myParamName = paramName;
         myActualValue = actualValue;
         myValidValues = validValues;
    }
    
    /**
     * Restituisce il nome del parametro.
     * 
     * @return Valore.
     */
    public final String getParameterName() {
        return myParamName;
    }
    
    /**
     * Restituisce il valore del parametro.
     * 
     * @return Valore.
     */
    public final Object getActualValue() {
        return myActualValue;
    }
    
    /**
     * Restituisce l&rsquo;insieme dei valori validi.
     * 
     * @return Valori.
     */
    public final Object getValidValues() {
        return myValidValues;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        CommonResources res = new CommonResources();
               
        return res.getArgumentOutOfRangeException(myParamName,
                String.valueOf(myActualValue), validValuesToString());
    }    
    
    /**
     * Rappresenta l&rsquo;insieme dei valori validi con una stringa.
     *
     * @return Stringa.
     */
    private String validValuesToString() {
        Object validValues;
        
        // Non gestisco volutamente la possibilita' che l'insieme sia
        // rappresentato da un vettore di vettori o da una collezione di
        // vettori; dovrebbe infatti essere un semplice elenco dei valori
        // ammessi, quindi ad un solo livello. 
        if (myValidValues instanceof Object[]) {
            validValues = Arrays.toString((Object[]) myValidValues);
        } else {
            validValues = myValidValues;        
        }
        
        return String.valueOf(validValues);
    }
    
    /**
     * Serializza l&rsquo;oggetto.
     * 
     * @param      out Flusso di scrittura.
     * @serialData     Formato di default seguito da:
     * 
     * <OL>
     * <LI>Rappresentazione con una stringa del valore del parametro (pu&ograve;
     * essere {@code null}).
     * <LI>Rappresentazione con una stringa dell&rsquo;insieme dei valori validi
     * (pu&ograve; essere {@code null}).
     * </OL>
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        
        if (myActualValue == null) {
            out.writeObject(null);
        } else {
            out.writeObject(myActualValue.toString());
        }
        
        if (myValidValues == null) {
            out.writeObject(null);
        } else {
            out.writeObject(validValuesToString());
        }        
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param in Flusso di lettura.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        in.defaultReadObject();
        
        myActualValue = in.readObject();
        myValidValues = in.readObject();
    }    
}

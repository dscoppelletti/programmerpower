/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

import java.security.*;

/**
 * Funzioni di utilit&agrave; di
 * <ACRONYM TITLE="Java Virtual Machine">JVM</ACRONYM>.
 * 
 * @since 2.1.0
 */
public final class JVMTools {
    
    /**
     * Codice di uscita di JVM per successo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final int EXIT_SUCCESS = 0;
        
    /**
     * Codice di uscita di JVM per errore. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final int EXIT_FAILURE = 1;
    
    /**
     * Propriet&agrave; di sistema {@code os.name}: Nome del sistema operativo.
     */
    public static final String PROP_OSNAME = "os.name";
    
    /**
     * Propriet&agrave; di sistema {@code user.home}: Nome del direttorio
     * dell&rsquo;utente.
     */
    public static final String PROP_USERHOME = "user.home";
    
    /**
     * Costruttore privato per classe statica.
     */
    private JVMTools() {        
    }

    /**
     * Restituisce il valore di una propriet&agrave; di sistema.
     * 
     * <P>Il metodo {@code getSystemProperty}, a differenza del corrispondente
     * metodo della classe statica {@code System}, abilita i privilegi per
     * superare gli eventuali controlli di accesso impostati con un oggetto
     * {@code SecurityManager}.</P>
     * 
     * @param  key Nome della propriet&agrave;.
     * @return     Valore. Se la propriet&agrave; non &egrave; impostata,
     *             restituisce {@code null}.
     */
    public static String getProperty(final String key) {
        String value;
        
        value = AccessController.doPrivileged(new PrivilegedAction<String>() {
            public String run() {
                return System.getProperty(key);
            }
        });
        
        return value;
    }

    /**
     * Restituisce il valore di una variabile di ambiente.
     * 
     * <P>Il metodo {@code getenv}, a differenza del corrispondente metodo della
     * classe statica {@code System}, abilita i privilegi per superare gli
     * eventuali controlli di accesso impostati con un oggetto
     * {@code SecurityManager}.</P>
     * 
     * @param  name Nome della variabile.
     * @return      Valore. Se la variabile non &egrave; impostata, restituisce
     *              {@code null}.              
     */    
    public static String getenv(final String name) {
        String value;
        
        value = AccessController.doPrivileged(new PrivilegedAction<String>() {
            public String run() {
                return System.getenv(name);
            }
        });
        
        return value;        
    }
    
    /**
     * Registra un thread di shutdown per essere eseguito al termine di JVM.
     * 
     * <P>Il metodo {@code addShutdownHook}, a differenza del corrispondente
     * metodo dell&rsquo;oggetto {@code Runtime}, abilita i privilegi per
     * superare gli eventuali controlli di accesso impostati con un oggetto
     * {@code SecurityManager}.</P>
     *  
     * <H4 ID="idJVMShutdown">1. Termine di JVM</H4>
     * 
     * <P>JVM termina solo quando tutti i thread non daemon
     * dell&rsquo;applicazione sono terminati: le risorse
     * dell&rsquo;applicazione che potrebbero essere ancora accedute non possono
     * essere semplicemente rilasciate al termine del metodo statico
     * {@code main} dell&rsquo;applicazione perch&eacute; l&rsquo;esecuzione del
     * thread principale potrebbe appunto terminare anche se ci sono ancora
     * altri thread in esecuzione. Inoltre l&rsquo;esecuzione del metodo
     * {@code exit} della classe statica {@code System} potrebbe terminare JVM
     * senza completare l&rsquo;esecuzione del metodo {@code main}.<BR>
     * Anche utilizzando un thread di shutdown, dopo il rilascio delle risorse,
     * potrebbero ancora esserci dei thread daemon in esecuzione: in questi casi
     * &egrave; demandato all&rsquo;implementazione dell&rsquo;applicazione
     * specifica evitare che questi thread non accedano alle risorse
     * rilasciate.</P>
     *              
     * <P>Si ricorda che eventuali eccezioni inoltrate da un thread registrato
     * mediante il metodo {@code addShutdownHook} vengono segnalate sul flusso
     * {@code stderr} e causano l&rsquo;interruzione del thread.</P>
     *       
     * @param thread Thread.
     * @see          #removeShutdownHook
     */
    public static void addShutdownHook(final Thread thread) {
        AccessController.doPrivileged(new PrivilegedAction<Void>() {
            public Void run() {
                Runtime.getRuntime().addShutdownHook(thread);
                return null;
            }
        });          
    }
    
    /**
     * Deregistra un thread di shutdown.
     * 
     * <P>Il metodo {@code removeShutdownHook}, a differenza del corrispondente
     * metodo dell&rsquo;oggetto {@code Runtime}, abilita i privilegi per
     * superare gli eventuali controlli di accesso impostati con un oggetto
     * {@code SecurityManager}.</P>
     *  
     * @param thread Thread.
     * @see          #addShutdownHook
     */
    public static void removeShutdownHook(final Thread thread) {
        AccessController.doPrivileged(new PrivilegedAction<Void>() {
            public Void run() {
                Runtime.getRuntime().removeShutdownHook(thread);
                return null;
            }
        });          
    }    
}

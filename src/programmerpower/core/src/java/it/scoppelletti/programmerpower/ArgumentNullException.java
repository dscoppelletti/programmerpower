/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

/**
 * Eccezione per parametro {@code null} non valido.
 *
 * @since 1.0.0
 */
public class ArgumentNullException extends NullPointerException {
    private static final long serialVersionUID = 1;
    
    /** 
     * @serial Nome del parametro.
     */
    private final String myParamName;
    
    /**
     * Costruttore.
     *
     * @param paramName Nome del parametro.
     */
    public ArgumentNullException(String paramName) {
        myParamName = paramName;
    }
    
    /**
     * Restituisce il nome del parametro.
     * 
     * @return Valore.
     */
    public final String getParameterName() {
        return myParamName;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        CommonResources res = new CommonResources();
        
        return res.getArgumentNullException(myParamName);
    }
}

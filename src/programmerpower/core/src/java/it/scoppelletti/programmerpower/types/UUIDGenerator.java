/*
 * Copyright (C) 2008-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.util.Iterator;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentOutOfRangeException;
import it.scoppelletti.programmerpower.JVMTools;
import it.scoppelletti.programmerpower.ReturnNullException;
import it.scoppelletti.programmerpower.reflect.ReflectionException;
import it.scoppelletti.programmerpower.types.spi.DefaultUUIDProvider;
import it.scoppelletti.programmerpower.types.spi.UUIDProvider;

/**
 * Generatore di <ACRONYM TITLE="Universally Unique IDentifier">UUID</ACRONYM>
 * (o <ACRONYM TITLE="Globally Unique IDentifier">GUID</ACRONYM>).
 * 
 * <P>La classe {@code UUID} di
 * <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM> modella la variante di
 * Leach-Salz degli UUID descritta nelle specifiche
 * <ACRONYM TITLE="Request for Comments">RFC</ACRONYM> 4122; tale classe
 * implementa tuttavia solo i generatori di UUID delle versioni 3 (basati su
 * <DFN>nomi</DFN> codificati con algoritmo
 * <ACRONYM TITLE="Message Digest 5">MD5</ACRONYM>) e 4 (basati su una sequenza
 * di numeri casuali) previste dalle specifiche.<BR>   
 * La classe {@code UUIDGenerator} consente di personalizzare l&rsquo;algoritmo
 * di generazione di UUID impostando sulla propriet&agrave; di sistema  
 * {@code it.scoppelletti.programmerpower.types.spi.UUIDProvider} il nome
 * di una classe che implementa l&rsquo;interfaccia {@code UUIDProvider}; se la
 * propriet&agrave; di sistema non &egrave; impostata, &egrave; utilizzata la
 * classe {@code DefaultUUIDProvider} che implementa un generatore di UUID
 * della versione 1 (basati sul tempo) secondo l&rsquo;algoritmo descritto nella
 * sezione 4.2 delle specifiche RFC.<BR>   
 * La personalizzazione del generatore di UUID pu&ograve; essere utile, ad
 * esempio, per fare in modo che tutte le applicazioni (anche non Programmer
 * Power) presenti sullo stesso sistema utilizzino la stessa implementazione di
 * generatore in modo da limitare la probabilit&agrave; di UUID dupplicati
 * generati da diverse applicazioni.</P>
 *
 * @see   it.scoppelletti.programmerpower.types.spi.DefaultUUIDProvider
 * @see   it.scoppelletti.programmerpower.types.spi.UUIDProvider
 * @see   <A HREF="http://www.ietf.org/rfc/rfc4122.txt" TARGET="_top">RFC
 *        4122: A Universally Unique IDentifier (UUID) URN Namespace</A>
 * @since 1.0.0
 */
public final class UUIDGenerator {
         
    /**
     * UUID nullo.
     * 
     * <P>La sezione 4.1.7 delle specifiche RFC 4122 definisce il valore
     * {@code NIL} come un UUID con tutti i bit azzerati.</P>
     * 
     * @see <A HREF="http://www.ietf.org/rfc/rfc4122.txt" TARGET="_top">RFC
     *      4122: A Universally Unique IDentifier (UUID) URN Namespace</A> 
     */
    public static final UUID NIL = new UUID(0, 0);
    
    private static final UUIDGenerator myInstance = new UUIDGenerator();
    private final UUIDProvider myProvider;

    /**
     * Costruttore privato per classe singleton.     
     */
    private UUIDGenerator() {                
        myProvider = initProvider();
    }
    
    /**
     * Restituisce l&rsquo;istanza del generatore di UUID.
     *
     * @return Oggetto.
     */
    public static UUIDGenerator getInstance() {
        return myInstance;
    }
    
    /**
     * Inizializza il provider {@code ApplicationContextProvider}.
     *
     * @return Oggetto.
     */
    private UUIDProvider initProvider() {
        String providerName;
        Class<?> providerClass;
        UUIDProvider provider;
        
        providerName = JVMTools.getProperty(UUIDProvider.PROP_SPI);
        if (StringUtils.isBlank(providerName)) {
            return new DefaultUUIDProvider();
        }
        
        try {
            providerClass = Class.forName(providerName);
            provider = (UUIDProvider) providerClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        return provider;
    }
       
    /**
     * Estrae un UUID.
     * 
     * @return Valore.
     */
    public UUID newUUID() {        
        UUID value;
        
        value = myProvider.newUUID();        
        if (value == null) {
            throw new ReturnNullException(myProvider.toString(), "newUUID");
        }
        
        return value;
    }
    
    /**
     * Estrae un certo numero di UUID.
     * 
     * @param  count Numero di UUID da estrarre.
     * @return       Collezione.
     */
    public Iterator<UUID> newUUIDs(int count) {
        Iterator<UUID> it;

        if (count < 1) {
            throw new ArgumentOutOfRangeException("count", count,
                    "[1, 2, ...]");
        }
        
        it = myProvider.newUUIDs(count);
        if (it == null) {
            throw new ReturnNullException(myProvider.toString(), "newUUIDs");
        }
        
        return it; 
    }
}

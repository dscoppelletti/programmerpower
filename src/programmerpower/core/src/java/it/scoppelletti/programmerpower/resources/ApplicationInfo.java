/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.resources;

import java.io.*;

/**
 * Informazioni su un&rsquo;applicazione (nome, numero di versione, nota di
 * copyright, etc).
 * 
 * @since 2.0.0
 */
public interface ApplicationInfo extends Serializable {

    /**
     * Restituisce il nome dell&rsquo;applicazione.
     * 
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.         
     */    
    String getApplicationName();
    
    /**
     * Restituisce il numero di versione dell&rsquo;applicazione.
     *
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.                                                         
     */    
    String getVersion();
    
    /**
     * Restituisce l&rsquo;avviso di copyright.
     *
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.                  
     */
    String getCopyright();  
    
    /**
     * Restituisce la nota di licenza.
     *
     * @return Testo. Se il testo non &egrave; rilevato, restituisce
     *         {@code null}.                  
     */
    String getLicense();
    
    /**
     * Restituisce l&rsquo;URL della guida dell&rsquo;applicazione.
     * 
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}. 
     */
    String getPageUrl();    
}

/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Percorso di un file.
 * 
 * <P>La classe {@code FilePath} rappresenta un percorso nel file system con
 * la sequenza dei <DFN>nomi</DFN> che lo compongono nella sua forma  
 * <DFN>canonica</DFN> ed &egrave; quindi adatto per le elaborazioni che devono
 * considerare appunto i percorsi in modo non ambiguo.<BR>
 * La rappresentazione di un percorso in forma canonica (metodo
 * {@code getCanonicalPath} della classe {@code File}) non &egrave; infatti la
 * semplice conversione del percorso in forma assoluta (metodo
 * {@code getAbsolutePath} della classe {@code File}), bens&igrave; prevede
 * anche l&rsquo;applicazione di altre funzioni per la risoluzione delle altre
 * ambiguit&agrave; del percorso, tra le quali le seguenti:</P>
 * 
 * <OL>
 * <LI>Risoluzione delle notazioni di navigazione tra i livelli di percorso
 * (es. {@code ".."}).
 * <LI>Risoluzione dei link simbolici (per i file system che li supportano).
 * <LI>Conversione dei caratteri da maiuscolo a minuscolo e viceversa per
 * corrispondere agli effettivi nomi del percorso nel file system (per i
 * file system che non distinguono tra caratteri maiuscoli e minuscoli e solo
 * per i percorsi fisicamente esistenti).
 * </OL>
 * 
 * <H4>1. Classe {@code File}</H4>
 * 
 * <P>Il frammento di codice sorgente riportato in questo paragrafo esemplifica
 * alcune ambiguit&agrave; nelle quali si pu&ograve; incorrere utilizzando la
 * classe {@code File} senza tenere presente che questa implementa la
 * rappresentazione <I>astratta</I> di un percorso e non necessariamente il
 * corrispondente percorso effettivo nel file system.</P>
 * 
 * <BLOCKQUOTE><PRE>
 * void testFile() {
 *     File file, file2;
 * 
 *     // Suppose current directory is /opt/programmerpower/etc
 * 
 *     // The following code prints "/opt/programmerpower/etc"
 *     file = new File("/opt/programmerpower/etc/test.txt");
 *     System.out.println(file.getParent()); 
 *      
 *     // The following code prints "null" and not "/opt/programmerpower/etc"
 *     file = new File("test.txt");
 *     System.out.println(file.getParent());  
 *     
 *     // The following code prints "/opt/programmerpower/etc/.." and not
 *     // "/opt/programmerpower"
 *     file = new File("/opt/programmerpower/etc/../test.txt");
 *     System.out.println(file.getParent()); 
 *     
 *     // The following code prints -15 and not 0
 *     file2 = new File("/opt/programmerpower/test.txt");
 *     System.out.println(file.compareTo(file2)); 
 * }
 * </PRE></BLOCKQUOTE>
 * 
 * <P>L&rsquo;unico modo per risolvere le ambiguit&agrave; esposte nel codice di
 * esempio &egrave; convertire i percorsi nella forma canonica (utilizzando il
 * metodo {@code getCanonicalFile} dell&rsquo;oggetto {@code File}) oppure
 * utilizzare la classe {@code FilePath}.</P>
 *  
 * @since 1.0.0
 */
public final class FilePath implements Comparable<FilePath>, Serializable {
    private static final long serialVersionUID = 1;    
    
    /**
     * Indicatore di confronto dei nomi dei file senza distinguere tra caratteri
     * maiuscoli e minuscoli.
     * 
     * <P>L&rsquo;indicatore {@code myIgnoreCase} &egrave; utilizzato per
     * evitare di istanziare un oggetto {@code File} per ogni nome della
     * sequenza del percorso solo per poterne utilizzare il metodo
     * {@code compareTo}.</P>  
     */
    private static final boolean myIgnoreCase = FilePath.ignoreCase();
    
    private transient File myFile;        
    private transient List<String> myPath;
    
    /**
     * Costruttore.
     * 
     * @param file File.
     */
    public FilePath(File file) {
        if (file == null) {
            throw new ArgumentNullException("file");
        }
        
        try {
            myFile = file.getCanonicalFile();
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        }
                
        myPath = Collections.unmodifiableList(listLevelNames());
    }      
    
    /**
     * Serializza l&rsquo;oggetto.
     * 
     * @param      out Flusso di scrittura.
     * @serialData     Formato di default seguito dall&rsquo;elenco dei nomi che
     *                 compongono il percorso:
     *
     * <P><OL>
     * <LI>Numero di nomi del percorso.
     * <LI>Sequenza dei nomi del percorso.
     * </OL></P>
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    
        out.writeInt(myPath.size());
        for (String name : myPath) {
            out.writeObject(name);
        }
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param in Flusso di lettura.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        int i, n;
        String name;
        File file;
        List<String> list = new ArrayList<String>();
        
        in.defaultReadObject();        
        
        n = in.readInt();
        file = new File("");
        for (i = 0; i < n; i++) {
            name = (String) in.readObject();
            if (name == null) {
                throw new ReturnNullException(in.toString(), "readObject");
            } 
            
            file = new File(file, name);
            list.add(name);
        }
        
        myFile = file;        
        myPath = Collections.unmodifiableList(list);
    }   
        
    /**
     * Restituisce la sequenza dei nomi del percorso.
     * 
     * @return Collezione.
     */
    public List<String> getPath() {
        return myPath;
    }
    
    /**
     * Scompone il percorso nei nomi di livello.
     * 
     * @return Collezione.
     */
    private List<String> listLevelNames() {
        File file;
        List<String> list = new ArrayList<String>();

        file = myFile;
        while (file != null) {
            list.add(file.getName());
            file = file.getParentFile();
        }
        
        Collections.reverse(list);
        return list;                
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        return myFile.getPath(); 
    }     
    
    /**
     * Restituisce il file.
     * 
     * @return File (percorso in forma canonica).
     */
    public File toFile() {
        return myFile;
    }
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        int i, n;
        FilePath op;
        
        if (!(obj instanceof FilePath)) {
            return false;
        }
        
        op = (FilePath) obj;
        n = myPath.size();
        if (n != op.getPath().size()) {
            return false;
        }
        
        for (i = 0; i < n; i++) {
            if (!StringTools.equals(myPath.get(i),
                    op.getPath().get(i), FilePath.myIgnoreCase)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myFile.hashCode();

        return value;
    }    

    /**
     * Confronto con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Risultato del confronto.
     */
    public int compareTo(FilePath obj) {
        int c, i, n;
        
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        if (myPath.size() < obj.getPath().size()) {
            n = myPath.size();
        } else {
            n = obj.getPath().size();
        }
        
        for (i = 0; i < n; i++) {
            c = StringTools.compare(myPath.get(i),
                    obj.getPath().get(i), FilePath.myIgnoreCase);
            if (c != 0) {
                return c;
            }
        }    

        if (myPath.size() > obj.getPath().size()) {
            return 1;
        } else if (myPath.size() < obj.getPath().size()) {
            return -1;
        }
        
        return 0;
    }  
        
    /**
     * Verifica se il percorso ne include un altro.
     * 
     * <P>Un percorso {@code P1} <DFN>include</DFN> un percorso {@code P2} se
     * il percorso {@code P2} rappresenta un file o un sotto-direttorio a
     * qualsiasi livello del direttorio {@code P1}. Ad esempio, il direttorio
     * {@code /opt/programmerpower} include il direttorio
     * {@code /opt/programmerpower/modules/it/scoppelletti/programmerpower};
     * il direttorio {@code /opt/programmerpower/bin} invece non include il
     * direttorio {@code /opt/programmerpower/etc}.<BR>
     * Per definizione, un percorso {@code P1} non pu&ograve; includere un
     * percorso {@code P2} costituito da una sequenza di nomi con un minor
     * numero di elementi della sequenza di nomi che costituisce il percorso
     * {@code P1}. Ad esempio il direttorio {@code /opt/programmerpower/bin} non
     * include il direttorio {@code /opt/programmerpower}.<BR>  
     * Per convenzione, un percorso include sempre se stesso.</P>
     * 
     * @param  path Percorso da verificare.
     * @return      Esito della verifica.
     */
    public boolean contains(FilePath path) {
        int i, n;
        
        if (path == null) {
            throw new ArgumentNullException("path");
        }
        
        n = myPath.size();
        if (path.myPath.size() < n) {
            return false;
        }
        
        for (i = 0; i < n; i++) {
            if (!StringTools.equals(myPath.get(i),
                    path.getPath().get(i), FilePath.myIgnoreCase)) {
                return false;
            }            
        }
        
        return true;
    }
    
    /**
     * Converte il percorso nella sua forma relativa ad un direttorio che lo
     * include.
     *  
     * @param  dir Direttorio rispetto al quale eseguire la conversione.
     * @return     Percorso relativo al direttorio {@code dir}. Se il percorso
     *             non &egrave; incluso nel direttorio {@code dir}, restituisce
     *             {@code null}; se il percorso coincide con il direttorio
     *             {@code dir}, restituisce il percorso vuoto
     *             {@code new File("")}.
     * @see        #contains               
     */
    public File toRelative(FilePath dir) {
        int i, m, n;
        File file;
        
        if (dir == null) {
            throw new ArgumentNullException("dir");
        }
        
        m = dir.myPath.size();
        n = myPath.size();
        if (m > n) {
            return null;
        }        
        
        for (i = 0; i < m; i++) {
            if (!StringTools.equals(dir.getPath().get(i),
                    myPath.get(i), FilePath.myIgnoreCase)) {
                return null;
            }                        
        }
        
        if (m == n) {
            return new File("");
        }
        
        file = new File(myPath.get(m));
        for (i = m + 1; i < n; i++) {
            file = new File(file, myPath.get(i));            
        }
        
        return file;
    }
    
    /**
     * Verifica se due percorsi sono disgiunti.
     * 
     * <P>Due percorsi sono <DFN>disgiunti</DFN> se un file o un
     * sotto-direttorio a qualsiasi livello di uno dei percorsi non &egrave;
     * incluso anche nell&rsquo;altro percorso. Ad esempio, i direttori
     * {@code /opt/programmerpower/bin} e {@code /opt/programmerpower/etc} sono
     * disgiunti; i direttori {@code /opt/programmerpower} e
     * {@code /opt/programmerpower/modules/it/scoppelletti/programmerpower}
     * invece non sono disgiunti.<BR>
     * Per definizione, due percorsi uguali non sono disgiunti.</P>
     *   
     * @param  path1 Primo percorso.
     * @param  path2 Secondo percorso.
     * @return       Esito della verifica.
     * @see          #contains
     */
    public static boolean disjoint(FilePath path1, FilePath path2) {
        int i, n;
                
        if (path1 == null) {
            throw new ArgumentNullException("path1");
        }
        if (path2 == null) {
            throw new ArgumentNullException("path2");
        }        
        
        if (path1.getPath().size() < path2.getPath().size()) {
            n = path1.getPath().size();
        } else {
            n = path2.getPath().size();
        }        
        
        for (i = 0; i < n; i++) {
            if (!StringTools.equals(path1.getPath().get(i),
                    path2.getPath().get(i), FilePath.myIgnoreCase)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Verifica se il file system confronta i nomi dei file senza distinguere
     * tra caratteri maiuscoli e minuscoli.
     * 
     * @return Esito della verifica. 
     */
    private static boolean ignoreCase() {
        File lower = new File("a");
        File upper = new File("A");
        
        if (lower.compareTo(upper) == 0) {
            return true;
        }
        
        return false;
    }
}

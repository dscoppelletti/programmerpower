/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.io.ObjectStreamException;
import java.util.Calendar;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.ArgumentOutOfRangeException;

/**
 * Orario.
 * 
 * <P>A differenza delle classi {@code Calendar} e {@code Date} di
 * <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM>, la classe
 * {@code SimpleTime} include le sole componenti (ora, minuto, secondo,
 * millisecondo) e le istanze sono immutabili.</P>
 * 
 * @since 2.0.0
 */
public final class SimpleTime implements Comparable<SimpleTime>, ValueType {
    private static final long serialVersionUID = 2L;
    
    /**
     * Istante nullo.
     * 
     * @see #isEmpty
     */
    public static final SimpleTime NIL = new SimpleTime();
    
    /**
     * @serial Ora.
     */
    private final int myHour;
    
    /**
     * @serial Minuto.
     */
    private final int myMinute;
    
    /**
     * @serial Secondo.
     */
    private final int mySecond;
    
    /**
     * @serial Millisecondo.
     */
    private final int myMillis;
    
    /**
     * Costruttore.
     */
    private SimpleTime() {
        myHour = TimeTools.NA;
        myMinute = TimeTools.NA;
        mySecond = TimeTools.NA;
        myMillis = TimeTools.NA;
    }
    
    /**
     * Costruttore.
     * 
     * @param hour   Ora.
     * @param minute Minuto.
     */
    public SimpleTime(int hour, int minute) {
        this(hour, minute, 0, 0);
    }
    
    /**
     * Costruttore.
     * 
     * @param hour   Ora.
     * @param minute Minuto.
     * @param second Secondo.
     */
    public SimpleTime(int hour, int minute, int second) {
        this(hour, minute, second, 0);
    }
    
    /**
     * Costruttore.
     * 
     * @param hour   Ora.
     * @param minute Minuto.
     * @param second Secondo.
     * @param millis Millisecondo.
     */
    public SimpleTime(int hour, int minute, int second, int millis) {
        if (hour < 0 || hour > 23) {
            throw new ArgumentOutOfRangeException("hour", hour, "[0, 23]");
        }
        if (minute < 0 || minute > 59) {
            throw new ArgumentOutOfRangeException("minute", minute, "[0, 59]");
        }
        if (second < 0 || second > 59) {
            throw new ArgumentOutOfRangeException("second", second, "[0, 59]");
        }
        if (millis < 0 || millis > 999) {
            throw new ArgumentOutOfRangeException("millis", millis, "[0, 999]");
        }        
        
        myHour = hour;
        myMinute = minute;
        mySecond = second;
        myMillis = millis;
    }

    /**
     * Costruttore.
     * 
     * @param value Calendario.
     */
    private SimpleTime(Calendar value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myHour = value.get(Calendar.HOUR_OF_DAY);
        myMinute = value.get(Calendar.MINUTE);
        mySecond = value.get(Calendar.SECOND);
        myMillis = value.get(Calendar.MILLISECOND);
    }
    
    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        if (myHour == TimeTools.NA && myMinute == TimeTools.NA &&
                mySecond == TimeTools.NA && myMillis == TimeTools.NA) {
            return SimpleTime.NIL;
        }
        
        return new SimpleTime(myHour, myMinute, mySecond, myMillis);
    }

    /**
     * Restituisce l&rsquo;ora corrente.
     * 
     * @return Valore.
     * @see    it.scoppelletti.programmerpower.types.TimeTools#getNow
     */
    public static SimpleTime getNow() {
        return new SimpleTime(TimeTools.getNow());
    }

    /**
     * Restituisce l&rsquo;istante corrispondente ad un calendario.
     * 
     * @param  value Calendario.
     * @return       Istante. Se il calendario {@code value} &egrave;
     *               {@code null}, restituisce l&rsquo;istante nullo.
     * @see          #NIL               
     */
    public static SimpleTime toSimpleTime(Calendar value) {
        if (value == null) {
            return SimpleTime.NIL;
        }
        
        return new SimpleTime(value);
    }
    
    /**
     * Restituisce l&rsquo;ora.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getHour() {
        return myHour;
    }
    
    /**
     * Restituisce il minuto.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getMinute() {
        return myMinute;
    }    
    
    /**
     * Restituisce il secondo.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getSecond() {
        return mySecond;
    }  
    
    /**
     * Restituisce il millisecondo.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getMillis() {
        return myMillis;
    }      
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * <P>Questa versione del metodo {@code toString} restituisce l&rsquo;orario
     * in formato ISO 8601 ({@code HH:MM:SS.mmm}).</P>
     * 
     * @return Stringa. Se l&rsquo;istante &egrave; nullo, restituisce la
     *         stringa vuota {@code ""}.
     * @see    #NIL
     * @see    #isEmpty         
     */
    @Override
    public String toString() {
        if (isEmpty()) {
            return StringUtils.EMPTY;
        }
        
        return String.format("%1$2d:%2$2d:%3$2d.%4$3d", myHour, myMinute,
                mySecond, myMillis);
    }      
       
    public boolean isEmpty() {
        return (myHour == TimeTools.NA);
    }
    
    /**
     * Converte l&rsquo;istante in un calendario.
     * 
     * @return Valore. Se l&rsquo;istante &egrave; nullo, restituisce
     *         {@code null}.
     * @see    #NIL
     * @see    #isEmpty 
     */
    public Calendar toCalendar() {
        if (isEmpty()) {
            return null;
        }
        
        Calendar value = TimeTools.getNow();
        
        value.set(Calendar.YEAR, value.getMinimum(Calendar.YEAR));
        value.set(Calendar.MONTH, value.getMinimum(Calendar.MONTH));
        value.set(Calendar.DAY_OF_MONTH,
                value.getMinimum(Calendar.DAY_OF_MONTH));
        value.set(Calendar.HOUR_OF_DAY, myHour);
        value.set(Calendar.MINUTE, myMinute);
        value.set(Calendar.SECOND, mySecond);
        value.set(Calendar.MILLISECOND, myMillis);
        
        return value;
    }
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        SimpleTime op;
        
        if (!(obj instanceof SimpleTime)) {
            return false;
        }
        
        op = (SimpleTime) obj;
        
        if (myHour != op.getHour() || myMinute != op.getMinute() ||
                mySecond != op.getSecond() || myMillis != op.getMillis()) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myHour;
        value = 37 * value + myMinute;
        value = 37 * value + mySecond;
        value = 37 * value + myMillis;
        
        return value;
    }

    /**
     * Confronto con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Risultato del confronto.
     */
    public int compareTo(SimpleTime obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }        

        if (isEmpty() && obj.isEmpty()) {
            return 0;
        } else if (isEmpty()) {
            return -1;
        } else if (obj.isEmpty()) {
            return 1;
        }
        
        if (myHour < obj.getHour()) {
            return -1;
        }
        if (myHour > obj.getHour()) {
            return 1;
        }
        
        if (myMinute < obj.getMinute()) {
            return -1;
        }
        if (myMinute > obj.getMinute()) {
            return 1;
        }
        
        if (mySecond < obj.getSecond()) {
            return -1;
        }
        if (mySecond > obj.getSecond()) {
            return 1;
        }
        
        if (myMillis < obj.getMillis()) {
            return -1;
        }
        if (myMillis > obj.getMillis()) {
            return 1;
        }
        
        return 0;
    }
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types.spi;

import java.util.*;

/**
 * Interfaccia per l&rsquo;implementazione del generatore di
 * <ACRONYM TITLE="Universally Unique IDentifier">UUID</ACRONYM>
 * (o <ACRONYM TITLE="Globally Unique IDentifier">GUID</ACRONYM>).
 * 
 * <P>Le implementazioni del generatore di UUID dovrebbero essere thread-safe e
 * dovrebbero anche gestire l&rsquo;accesso in concorrenza all&rsquo;eventuale
 * stato <DFN>persistente</DFN> condiviso tra tutte le applicazioni.</P>
 *  
 * @see   it.scoppelletti.programmerpower.types.UUIDGenerator
 * @since 1.0.0
 */
public interface UUIDProvider {
    
    /**
     * Nome della propriet&agrave; di sistema sulla quale pu&ograve; essere
     * impostato il nome della classe che implementa l&rsquo;interfaccia
     * {@code UUIDProvider}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String PROP_SPI =
            "it.scoppelletti.programmerpower.types.spi.UUIDProvider";
    
    /**
     * Estrae un UUID.
     * 
     * @return Valore. Non pu&ograve; essere {@code null}.
     */
    UUID newUUID();
    
    /**
     * Estrae un certo numero di UUID.
     * 
     * <P>Quando &egrave; possibile predeterminare il numero di UUID che
     * &egrave; necessario estrarre, l&rsquo;implementazione del generatore di
     * UUID dovrebbe essere ottimizzata di conseguenza.</P>  
     *  
     * @param  count Numero di UUID da estrarre.
     * @return       Collezione. Non pu&ograve; essere {@code null}.
     */
    Iterator<UUID> newUUIDs(int count);    
}

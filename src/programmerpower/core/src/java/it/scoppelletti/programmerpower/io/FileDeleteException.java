/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

/**
 * Eccezione nella cancellazione di un file.
 * 
 * @since 1.0.0
 */
public class FileDeleteException extends IOOperationException {
    private static final long serialVersionUID = 1;
    
    /**
     * @serial Percorso del file.
     */
    private final String myPath;

    /**
     * Costruttore.
     * 
     * @param path Percorso del file.
     */
    public FileDeleteException(String path) {
        myPath = path;
    }
    
    /**
     * Restituisce il percorso del file.
     * 
     * @return Valore.
     */
    public final String getPath() {
        return myPath;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        IOResources res = new IOResources();
        
        return res.getFileDeleteException(myPath);
    }        
}

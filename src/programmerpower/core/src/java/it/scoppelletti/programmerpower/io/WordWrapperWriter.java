/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Flusso di filtro in scrittura che esegue il word-wrap del testo.
 *  
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/textflat/wordwrap.html"
 *        TARGET="_top">Word-wrap</A>
 * @since 1.0.0
 */
public class WordWrapperWriter extends AbstractFilterWriter {
    private final int myLineLenMax;
    private final boolean myKeepNL;
    private BufferedWriter myWriter;    
    
    /**
     * Linea in corso di composizione.
     */
    private char[] myLine;
    
    /**
     * Numero di caratteri inseriti nella linea {@code myLine}.
     */
    private int myLineLen;
    
    /**
     * Indica se l&rsquo;ultimo carattere accodato &egrave; un {@code CR}.
     */
    private boolean myLastCharCR = false;
    
    /**
     * Costruttore.
     * 
     * @param writer      Flusso di scrittura sottostante.
     * @param lineLenMax  Lunghezza massima delle linee risultanti.
     * @param keepNewLine Indica se le interruzioni di linea del testo originale
     *                    devono essere mantenute.
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/textflat/wordwrap.html#idKeepNewline"
     *      TARGET="_top">Gestione delle interruzioni di linea originali</A>                 
     */
    public WordWrapperWriter(Writer writer, int lineLenMax,
            boolean keepNewLine) {
        super(writer);
        
        if (lineLenMax < 1) {
            throw new ArgumentOutOfRangeException("lineLenMax", lineLenMax,
                    "[1, 2, ...]");
        }
        
        myLineLenMax = lineLenMax;
        myKeepNL = keepNewLine;
        myLine = new char[myLineLenMax];
        myLineLen = 0;
        myWriter = new BufferedWriter(writer);
        setWriter(myWriter);
    }

    /**
     * Chiude il flusso.
     */
    @Override
    public void close() throws IOException {
        super.close();       
        myWriter = null;
    }
    
    /**
     * Scrive un'interruzione di linea.
     */
    public void newLine() throws IOException {
        checkClosed();
        flushLine();
    }
    
    /**
     * Scrive un carattere.
     * 
     * @param c Carattere.
     */
    @Override
    protected final void writeImpl(char c) throws IOException {
        if (c == '\n') {
            if (myLastCharCR) {
                // Il NL e' immediatamente seguente ad un CR:
                // Lo ignoro.
                myLastCharCR = false;
            } else if (myKeepNL) {
                flushLine(); 
            } else {            
                addBlank();
            }
            return;
        }
        
        if (c == '\r') {
            if (myKeepNL) {
                flushLine();
            } else {
                addBlank();                
            }
            
            // Il CR potrebbe essere seguito da un NL
            myLastCharCR = true;
            return;
        }
        
        if (Character.isWhitespace(c)) {
            addBlank();
        } else {
            addChar(c);
        }       
    }
        
    /**
     * Aggiunge un carattere (non {@code blank}).
     * 
     * @param c Carattere.
     */
    private void addChar(char c) throws IOException {
        int i, n;
        
        if (myLineLen < myLineLenMax) {
            // Il carattere ci sta ancora sulla linea corrente
            myLine[myLineLen++] = c;
            return;
        }
        
        // La linea corrente e' satura:
        // Cerco il primo blank a partire dal fondo per inserire nella sua
        // posizione un'interruzione di linea.
        for (n = myLineLen - 1; n >= 0; n--) {
            if (myLine[n] == ' ') {
                break;
            }
        }
        
        if (n < 0) {
            // Non ho trovato nessun blank sul quale interrompere la linea
            // corrente:
            // Non posso far altro che inserire un'interruzione di linea e
            // cominciare una nuova linea.
            flushLine();
            myLine[myLineLen++] = c;
            return;            
        }
        
        // Scarico la linea corrente fino al blank di interruzione escluso
        for (i = 0; i < n; i++) {
            myWriter.write(myLine[i]);
        }
        
        // Inserisco l'interruzione di linea
        myWriter.newLine();
        
        // Faccio scorrere all'indietro la linea corrente in modo che il primo
        // carattere diventi quello successivo al blank di interruzione
        myLineLen = myLineLen - n - 1;
        for (i = 0; i < myLineLen; i++) {
            myLine[i] = myLine[i + n + 1];
        }
        
        // Accodo il carattere
        myLine[myLineLen++] = c;
    }
    
    /**
     * Aggiunge un carattere {@code blank}.
     */
    private void addBlank() throws IOException {
        if (myLineLen < myLineLenMax) {
            // Il carattere ci sta ancora sulla linea corrente
            myLine[myLineLen++] = ' ';
            return;
        }
        
        // La linea corrente e' satura:
        // Il blank diventa un'interruzione di linea.
        flushLine();        
    }
                   
    /**
     * Scarica sul flusso sottostante gli eventuali caratteri scritti ma non
     * ancora scaricati.
     * 
     * <P>Nel caso si sia cominciato a scrivere una linea di testo non ancora
     * scaricata, tale linea di testo viene scaricata e di seguito viene
     * aggiunta un&rsquo;interruzione di linea.</P> 
     */
    @Override
    public void flush() throws IOException {
        // Se il flusso e' stato chiuso, e' stato anche gia' eseguito il flush e
        // dopo non e' piu' stato possibile scrivere altro e quindi non puo'
        // esserci una linea corrente da scaricare:
        // Non c'e' bisogno di controllare che il flusso sia ancora aperto prima
        // di verificare se c'e' una linea corrente da scaricare.
        if (myLineLen > 0) {
            flushLine();
        }
        
        super.flush();
    }
    
    /**
     * Scarica la linea corrente e scrive un'interruzione di linea.
     */
    private void flushLine() throws IOException {
        int i;
 
        for (i = 0; i < myLineLen; i++) {
            myWriter.write(myLine[i]);
        }
        myLineLen = 0;
        
        myWriter.newLine();
    }    
}

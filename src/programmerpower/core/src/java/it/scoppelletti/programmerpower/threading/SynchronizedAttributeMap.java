/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.threading;

import it.scoppelletti.programmerpower.*;

/**
 * Collezione di attributi sincronizzata.
 * 
 * @it.scoppelletti.tag.threadsafe
 * @since                          2.0.0
 */
public final class SynchronizedAttributeMap implements AttributeMap {
    private final SynchronizedAttributeMap.Source mySource;
    
    /**
     * Costruttore.
     * 
     * @param source Collezione sorgente.
     */
    public SynchronizedAttributeMap(SynchronizedAttributeMap.Source source) {
        if (source == null) {
            throw new ArgumentNullException("source");
        }

        mySource = source;
    }
    
    public Object getAttribute(String name) {
        synchronized (mySource.getSyncRoot()) {
            return mySource.getAttribute(name);
        }
    }
        
    public Object getAttribute(String name, RunnableWithResult<?> initializer) {
        Object value, newValue;
        
        if (initializer == null) {
            throw new ArgumentNullException("initializer");
        }
        
        synchronized (mySource.getSyncRoot()) {
            value = mySource.getAttribute(name);
        }        
        if (value != null) {
            return value;
        }
        
        // Rilascio il lock sulla sorgente per evitare il dead-lock con
        // l'inizializzatore.
        
        newValue = initializer.run();
        
        synchronized (mySource.getSyncRoot()) {
            value = mySource.getAttribute(name);
            if (value != null) {
                // L'attributo e' stato impostato da un altro thread durante
                // l'esecuzione dell'inizializzatore
                return value;
            }
            
            mySource.setAttribute(name, newValue);
        }                
        
        return newValue;
    }
    
    public void setAttribute(String name, Object value) {
        synchronized (mySource.getSyncRoot()) {
            mySource.setAttribute(name, value);
        }
    }
        
    public void removeAttribute(String name) {
        synchronized (mySource.getSyncRoot()) {
            mySource.removeAttribute(name);
        }
    }    
    
    /**
     * Collezione sorgente di un oggetto {@code SynchronizedAttributeMap}.
     * 
     * @since 2.0.0
     */
    public interface Source {
        /**
         * Restituisce la radice per la sincronizzazione.
         *  
         * @return Oggetto.
         */
        Object getSyncRoot();
        
        /**
         * Restituisce il valore di un attributo.
         * 
         * @param  name Nome dell&rsquo;attributo.
         * @return      Valore. Se l&rsquo;attributo non &egrave; impostato,
         *              restitutisce {@code null}.
         * @see         #setAttribute
         */
        Object getAttribute(String name);
        
        /**
         * Imposta il valore di un attributo.
         * 
         * @param name  Nome dell&rsquo;attributo.
         * @param value Valore.
         * @see         #getAttribute(String)
         * @see         #removeAttribute
         */
        void setAttribute(String name, Object value);
        
        /**
         * Rimuove un attributo.
         * 
         * @param name Nome dell&rsquo;attributo.
         * @see        #setAttribute
         */    
        void removeAttribute(String name);             
    }
}

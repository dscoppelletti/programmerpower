/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

import java.lang.annotation.*;

/**
 * Indicazione di elemento non utilizzabile dai moduli client.
 * 
 * <P>L&rsquo;implementazione di alcuni modelli di programmazione (ad esempio
 * <ACRONYM TITLE="Dependency Injection">DI</ACRONYM>) o l&rsquo;utilizzo di
 * alcuni framework di sviluppo (ad esempio le implementazioni di
 * <ACRONYM TITLE="Java Persistence API">JPA</ACRONYM>) richiedono che alcune
 * classi o alcuni elementi di queste siano pubblicamente accessibili in
 * deroga ai principi
 * <ACRONYM TITLE="Object Oriented Programming">OOP</ACRONYM>; poich&egrave;
 * l&rsquo;utilizzo di questi elementi dovrebbe comunque essere riservato ai
 * moduli che li definiscono ed ai framework adottati, ma evitato da parte dei
 * moduli client, almeno a fini di documentazione, &egrave; possibile applicare
 * l&rsquo;annotazione {@code Reserved} in sostituzione dei qualificatori
 * {@code private} o {@code protected}.</P>  
 *  
 * <BLOCKQUOTE CLASS="note">
 * Allo stato dell&rsquo;arte, non sono previsti sistemi per la verifica del
 * rispetto dell&rsquo;annotazione {@code Reserved}.
 * </BLOCKQUOTE>
 *  
 * @since 2.1.0
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.TYPE, ElementType.CONSTRUCTOR, ElementType.METHOD })
public @interface Reserved {
}

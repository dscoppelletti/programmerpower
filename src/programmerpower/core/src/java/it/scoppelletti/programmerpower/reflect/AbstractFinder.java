/*
 * Copyright (C) 2010-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

import java.io.*;
import java.security.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;

/**
 * Rilevamento di una risorsa.
 *
 * @param <T> Classe di accesso alla risorsa.
 */
abstract class AbstractFinder<T> implements PrivilegedAction<T> {
    protected static final Logger myLogger = LoggerFactory.getLogger(
            ReflectionTools.class);        
    private final String myProp;
    private final String myDefaultName;
    private String myResName;
    
    /**
     * Costruttore.
     * 
     * @param prop        Nome della propriet&agrave; di sistema sulla quale
     *                    pu&ograve; essere impostato un nome della risorsa da
     *                    ricercare alternativo a quello di default
     *                    {@code defaultName}.
     * @param defaultName Nome della risorsa di default.
     */
    AbstractFinder(String prop, String defaultName) {        
        if (StringUtils.isBlank(defaultName)) {
            throw new ArgumentNullException("defaultName");
        }
        
        myProp = prop;
        myDefaultName = defaultName;
    }
    
    /**
     * Rileva la risorsa.
     * 
     * @return Risorsa. Se la risorsa non &egrave; rilevata, restituisce
     *         {@code null}.
     */    
    final T findResource() {
        T res;
        
        if (!StringUtils.isBlank(myProp)) {
            res = findResource(myProp);
            if (res != null) {
                return res;
            }                                
        }              
        
        myResName = myDefaultName;
        res = AccessController.doPrivileged(this);
        if (res == null) {
            myLogger.debug("Resource {} not found.", myDefaultName);
            return null;
        }
        
        return res;
    }
    
    /**
     * Rileva un file.
     * 
     * @param  name Nome del file.
     * @return      File. Se il file non &egrave; rilevato, restituisce
     *              {@code null}.
     */
    abstract T findFileImpl(String name) throws IOException;
    
    /**
     * Rileva una risorsa.
     * 
     * @param  prop Nome della propriet&agrave; di sistema sulla quale
     *              pu&ograve; essere impostato il nome della risorsa.
     * @return      Risorsa. Se la risorsa non &egrave; rilevata,
     *              restituisce {@code null}.
     */    
    private T findResource(String prop) {
        T res;
        
        myResName = JVMTools.getProperty(prop);
        if (StringUtils.isBlank(myResName)) {
            return null;
        }        
            
        myLogger.trace("Searching for resource {} (name specified by " +
                "system property {}) instead of resource {}.",
                new Object[] { myResName, prop, myDefaultName } );
        
        res = AccessController.doPrivileged(this);
        if (res != null) {
            return res;
        }
        
        return null;
    }
    
    /**
     * Operazione di rilevamento di una risorsa.
     */
    public T run() {
        T res;
        ClassLoader loader;               
        
        // Class-loader del contesto del thread corrente
        loader = Thread.currentThread().getContextClassLoader();
         
        // La documentazione del metodo getContextClassLoader della classe
        // Thread spiega che il class-loader del contesto di un thread e'
        // quello impostato dal creatore del thread stesso, oppure e' lo
        // stesso del thread che ha creato il thread, e cosi' via fino al
        // thread primario che ha impostato il class-loader di caricamento
        // dell'applicazione.
        // Sembrerebbe quindi che il class-loader del contesto di un thread
        // sia sempre impostato, tuttavia ho trovato degli esempi di codice
        // sorgente che verificano che lo sia effettivamente (anche perche'
        // il concetto di class-loader del contesto di un thread e' stato
        // introdotto con JDK 1.2).
        if (loader != null) {
            res = findResourceImpl(loader, myResName);
            if (res != null) {
                myLogger.debug("Resource {} found by class-loader {}.",
                        myResName,
                        "Thread.currentThread().getContextClassLoader()");
                return res;
            }
        }
     
        // Class-loader che ha caricato la classe ReflectionTools
        loader = ReflectionTools.class.getClassLoader();
        
        // Anche in questo caso direi che qualsiasi classe e' sempre stata
        // caricata da un class-loader, ma anche in questo caso ho trovato
        // degli esempi di codice sorgente che verificano che verificano che
        // il class-loader di una classe sia effettivamente impostato.            
        if (loader != null) {
            res = findResourceImpl(loader, myResName);
            if (res != null) {
                myLogger.debug("Resource {} found by class-loader {}.",
                        myResName, "ReflectionUtils.class.getClassLoader()");                                        
                return res;
            }
        }
        
        // Class-loader di sistema
        res = findResourceImpl(ClassLoader.getSystemClassLoader(), myResName);
        if (res != null) {
            myLogger.debug("Resource {} found by class-loader {}.", myResName,
                    "ClassLoader.getSystemClassLoader()");                                    
            return res;
        }            
        
        return null;
    }
            
    /**
     * Rileva una risorsa.
     * 
     * @param loader Class-loader.
     * @param name   Nome della risorsa.
     * @return       Risorsa.
     */
    abstract T findResourceImpl(ClassLoader loader, String name);
}

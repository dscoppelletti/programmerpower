/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import it.scoppelletti.programmerpower.*;

/**
 * Eccezione in un&rsquo;operazione di
 * <ACRONYM TITLE="Input/Output">I/O<ACRONYM>.
 *
 * @since 1.0.0
 */
public class IOOperationException extends ApplicationException {
    private static final long serialVersionUID = 1;
    
    /**
     * Costruttore.
     */
    protected IOOperationException() {
        super();
    } 
    
    /**
     * Costruttore.
     *
     * @param message Messaggio.
     */
    public IOOperationException(String message) {
        super(message);
    } 
    
    /**
     * Costruttore.
     * 
     * <P>Questo sovraccarico del costruttore pu&ograve; essere utilizzato per
     * ridurre un&rsquo;eccezione {@code IOException} ad un&rsquo;eccezione
     * {@code RuntimeException}.</P>
     *  
     * @param ex Eccezione originale.
     */        
    public IOOperationException(Throwable ex) {
        super(ex);
    }
}

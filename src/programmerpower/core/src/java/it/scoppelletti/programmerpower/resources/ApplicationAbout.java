/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.resources;

import java.lang.annotation.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Informazioni su un&rsquo;applicazione (nome, numero di versione, nota di
 * copyright, etc).
 * 
 * @see   it.scoppelletti.programmerpower.resources.ApplicationAbout.Annotation
 * @since 1.0.0
 */
public final class ApplicationAbout implements ApplicationInfo {
    private static final long serialVersionUID = 1L;

    /**
     * @serial Nome dell&rsquo;applicazione.
     */
    private final String myApplName;
    
    /**
     * @serial Numero di versione dell&rsquo;applicazione.
     */
    private final String myVersion;
        
    /**
     * @serial Avviso di copyright.
     */
    private final String myCopyright;
    
    /**
     * @serial Nota di licenza.
     */
    private final String myLicense;
                
    /**
     * @serial URL della guida.
     */
    private final String myPageUrl;
    
    /**
     * Costruttore.
     *
     * @param applicationClass Classe che rappresenta l&rsquo;applicazione.
     * 
     * <P>Per classe che rappresenta un&rsquo;applicazione, si intende
     * normalmente la classe che implementa il metodo statico {@code main} di
     * entry-point dell&rsquo;applicazione stessa.</P>
     */
    public ApplicationAbout(Class<?> applicationClass) {
        Package pack;
        ApplicationAbout.Annotation attrs;
        
        if (applicationClass == null) {
            throw new ArgumentNullException("applicationClass");            
        }
                    
        pack = applicationClass.getPackage();
        attrs = applicationClass.getAnnotation(
                ApplicationAbout.Annotation.class);        
        myApplName = initApplName(applicationClass, pack, attrs);
        myVersion = initVersion(applicationClass, pack, attrs);
        myCopyright = initCopyright(attrs);
        myLicense = initLicense(attrs);
        myPageUrl = initPageUrl(attrs);
    }               
    
    /**
     * Restituisce il nome dell&rsquo;applicazione.
     * 
     * <P>Questa implementazione del metodo {@code getApplicationName} rileva
     * il nome dell&rsquo;applicazione dalle seguenti alternative:</P>
     *  
     * <OL>
     * <LI>Elemento {@code applicationName} dell&rsquo;annotazione 
     * {@code ApplicationAbout.Annotation} applicata alla classe
     * dell&rsquo;applicazione.
     * <LI>Titolo della specifica implementata dal pacchetto della classe
     * dell&rsquo;applicazione (attributo {@code Specification-Title} del
     * manifesto).
     * <LI>Titolo del pacchetto della classe dell&rsquo;applicazione (attributo
     * {@code Implementation-Title} del manifesto). 
     * <LI>Nome semplice della classe dell&rsquo;applicazione. Ad esempio, per
     * la classe {@code it.scoppelletti.games.Billiard}, il nome
     * dell&rsquo;applicazione sar&agrave; {@code Billiard}. 
     * </OL>
     * 
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.
     * @see it.scoppelletti.programmerpower.resources.ApplicationAbout.Annotation#applicationName         
     */
    public String getApplicationName() {
        return myApplName;        
    }
    
    /**
     * Inizializza il nome dell&rsquo;applicazione.
     *
     * @param  applClass Classe dell&rsquo;applicazione.
     * @param  pack      Pacchetto dell&rsquo;applicazione.
     * @param  attrs     Annotazione.
     * @return           Valore.
     */
    private String initApplName(Class<?> applClass, Package pack,
            ApplicationAbout.Annotation attrs) {
        String value;
        
        if (attrs != null) {
            value = attrs.applicationName();
            if (!StringUtils.isBlank(value)) {
                return value;
            }            
        }                
        
        if (pack != null) {
            value = pack.getSpecificationTitle();
            if (!StringUtils.isBlank(value)) {
                return value;
            }
            
            value = pack.getImplementationTitle();
            if (!StringUtils.isBlank(value)) {
                return value;
            }               
        }
        
        value = applClass.getSimpleName();
        if (!StringUtils.isBlank(value)) {
            return value;
        }
        
        return null; 
    }
    
    /**
     * Restituisce il numero di versione dell&rsquo;applicazione.
     *
     * <P>Questa implementazione del metodo {@code getVersion} rileva il numero
     * di versione dalle seguenti alternative:</P>
     *  
     * <OL>
     * <LI>Elemento {@code version} dell&rsquo;annotazione 
     * {@code ApplicationAbout.Annotation} applicata alla classe
     * dell&rsquo;applicazione.
     * <LI>Versione del pacchetto della classe dell&rsquo;applicazione
     * (attributo {@code Implementation-Version} del manifesto).
     * <LI>Versione della specifica implementata dal pacchetto della classe
     * dell&rsquo;applicazione (attributo {@code Specification-Version} del
     * manifesto). 
     * </OL>
     *  
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.                        
     * @see it.scoppelletti.programmerpower.resources.ApplicationAbout.Annotation#version                                 
     */
    public String getVersion() {
        return myVersion;
    }
            
    /**
     * Inizializza il numero di versione dell&rsquo;applicazione.
     * 
     * @param  applClass Classe dell&rsquo;applicazione.
     * @param  pack      Pacchetto dell&rsquo;applicazione.
     * @param  attrs     Annotazione.
     * @return           Valore.
     */
    private String initVersion(Class<?> applClass, Package pack,
            ApplicationAbout.Annotation attrs) {
        String value;

        if (attrs != null) {
            value = attrs.version();
            if (!StringUtils.isBlank(value)) {
                return value;
            }            
        }                
        
        if (pack != null) {        
            value = pack.getImplementationVersion();
            if (!StringUtils.isBlank(value)) {
                return value;
            }
            
            value = pack.getSpecificationVersion();
            if (!StringUtils.isBlank(value)) {
                return value;
            }                   
        }
        
        return null;
    }
              
    /**
     * Restituisce l&rsquo;avviso di copyright.
     *
     * <P>Questa implementazione del metodo {@code getCopyright} rileva
     * l&rsquo;avviso di copyright dall&rsquo;elemento {@code copyright}
     * dell&rsquo;annotazione {@code ApplicationAbout.Annotation} applicata alla
     * classe dell&rsquo;applicazione.</P>
     * 
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.
     * @see it.scoppelletti.programmerpower.resources.ApplicationAbout.Annotation#copyright                  
     */
    public String getCopyright() {
        return myCopyright;
    }
    
    /**
     * Inizializza l&rsquo;avviso di copyright.
     *
     * @param  attrs Annotazione.
     * @return       Valore.
     */
    private String initCopyright(ApplicationAbout.Annotation attrs) {
        String value;
       
        if (attrs == null) {
            return null;
        }
        
        value = attrs.copyright();
        if (!StringUtils.isBlank(value)) {
            return value;
        }            

        return null;
    }
    
    /**
     * Restituisce la nota di licenza.
     *
     * <P>Questa implementazione del metodo {@code getLicense} rileva la nota di
     * licenza dall&rsquo;elemento {@code licenseResource}
     * dell&rsquo;annotazione {@code ApplicationAbout.Annotation} applicata alla
     * classe dell&rsquo;applicazione.</P>
     * 
     * @return Testo. Se il testo non &egrave; rilevato, restituisce
     *         {@code null}.
     * @see it.scoppelletti.programmerpower.resources.ApplicationAbout.Annotation#licenseResource                  
     */
    public String getLicense() {
        return myLicense;
    }
    
    /**
     * Inizializza la nota di licenza.
     * 
     * @param  attrs Annotazione.
     * @return       Testo.               
     */
    private String initLicense(ApplicationAbout.Annotation attrs) {
        String location, text;        
        
        if (attrs == null) {
            return null;
        }
        
        location = attrs.licenseResource();
        if (StringUtils.isBlank(location)) {
            return null;
        }
        
        text = ResourceTools.getResourceAsString(location, null);
        if (!StringUtils.isBlank(text)) {
            return text;
        }
                           
        return null;                
    }
    
    /**
     * Restituisce l&rsquo;URL della guida dell&rsquo;applicazione.
     * 
     * <P>Questa implementazione del metodo {@code getPageUrl} rileva 
     * l&rsquo;URL della guida dall&rsquo;elemento {@code pageUrl}
     * dell&rsquo;annotazione {@code ApplicationAbout.Annotation} applicata alla
     * classe dell&rsquo;applicazione.</P>
     *  
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.
     * @see it.scoppelletti.programmerpower.resources.ApplicationAbout.Annotation#pageUrl
     * @since  2.0.0 
     */
    public String getPageUrl() {
        return myPageUrl;
    }
    
    /**
     * Inizializza l&rsquo;URL della guida dell&rsquo;applicazione.
     * 
     * @param  attrs Annotazione.
     * @return       Valore.
     */
    private String initPageUrl(ApplicationAbout.Annotation attrs) {
        String value;

        if (attrs == null) {
            return null;
        }
        
        value = attrs.pageUrl();
        if (!StringUtils.isBlank(value)) {
            return value;
        }                            
            
        return null;
    }
    
    /**
     * Informazioni su un&rsquo;applicazione (nome, numero di versione, nota di
     * copyright, etc).
     *
     * @see   it.scoppelletti.programmerpower.resources.ApplicationAbout
     * @since 1.0.0
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE) 
    public static @interface Annotation {
        
        /**
         * Nome dell&rsquo;applicazione.
         * 
         * @see it.scoppelletti.programmerpower.resources.ApplicationAbout#getApplicationName
         */
        String applicationName() default "";
        
        /**
         * Numero di versione dell&rsquo;applicazione.
         * 
         * @see it.scoppelletti.programmerpower.resources.ApplicationAbout#getVersion
         */
        String version() default "";
                                
        /**
         * Avviso di copyright.
         * 
         * @see it.scoppelletti.programmerpower.resources.ApplicationAbout#getCopyright
         */
        String copyright() default "";
        
        /**
         * Locazione della risorsa (eventualmente localizzata) dalla quale
         * leggere la nota di licenza.
         * 
         * @see it.scoppelletti.programmerpower.resources.ApplicationAbout#getLicense
         */
        String licenseResource() default "";
        
        /**
         * URL della guida dell&rsquo;applicazione.
         *  
         * @see   it.scoppelletti.programmerpower.resources.ApplicationAbout#getPageUrl
         * @since 2.0.0
         */
        String pageUrl() default "";
    }      
}

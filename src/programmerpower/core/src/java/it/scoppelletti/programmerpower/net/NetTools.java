/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.net;

/**
 * Funzioni di utilit&agrave; per i componenti di rete.
 *
 * @since 2.1.0
 */
public final class NetTools {

    /**
     * Separatore dei segmenti del percorso di un URL. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final char PATH_SEP_CHAR = '/';
    
    /**
     * Separatore dei segmenti del percorso di un URL. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String PATH_SEP = "/";
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.port.http}: Numero di porta
     * per il protocollo HTTP.
     * 
     * @it.scoppelletti.tag.default 8080
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_PORT_HTTP =
            "it.scoppelletti.programmerpower.web.port.http";
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.port.https}: Numero di porta
     * per il protocollo HTTPS.
     * 
     * @it.scoppelletti.tag.default 8443
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_PORT_HTTPS =
            "it.scoppelletti.programmerpower.web.port.https"; 
        
    /**
     * Costruttore privato per classe statica.
     */
    private NetTools() {
    }
}

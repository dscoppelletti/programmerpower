/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.util.*;
import java.security.*;

/**
 * Funzioni di utilit&agrave; per i
 * <ACRONYM TITLE="Random Number Generator">RNG</ACRONYM>.
 *  
 * @since 2.1.0
 */
public final class RNGTools {

    /**
     * Costruttore privato per classe statica.
     */
    private RNGTools() {
    }    

    /**
     * Restituisce un RNG crittograficamente sicuro.
     * 
     * <P>Il metodo {@code getCSRNG} pubblica un RNG <DFN>crittograficamente
     * sicuro</DFN> che tutti i componenti della stessa istanza di
     * un&rsquo;applicazione possono utilizzare senza che ogni componente
     * provveda a inizializzarsene uno dedicato.</P>
     * 
     * <P>Il metodo {@code getCSRNG} restituisce un oggetto
     * {@code SecureRandom}: questa classe utilizza l&rsquo;implementazione
     * impostata utilizzando il modello SPI del pacchetto {@code java.security},
     * oppure un&rsquo;implementazione di default del JRE specifico. La classe
     * {@code SecureRandom} &egrave; inoltre thread-safe.</P>
     * 
     * <P>Si ricorda che l&rsquo;implementazione di un RNG crittograficamente
     * sicuro deve rispettare i seguenti requisiti:</P>
     * 
     * <OL>
     * <LI>Il generatore deve essere conforme ai test statistici sui RNG
     * specificati nella sezione 4.9.1 dello standard FIPS 140-2.
     * <LI>Il generatore deve produrre un output non deterministico.
     * <LI>I valori dei semi usati per inizializzare le sequenze del generatore
     * devono essere non prevedibili.
     * <LI>Le sequenze prodotte dal generatore devono essere crittograficamente
     * sicure secondo quanto definito nelle raccomandazioni RFC 1750.
     * </OL>
     *  
     * @return Oggetto. 
     * @see    <A HREF="http://csrc.nist.gov/publications/PubsFIPS.html"
     *         TARGET="_top">FIPS 140-2: Security Requirements for
     *         Cryptographic Modules</A>
     * @see    <A HREF="http://www.ietf.org/rfc/rfc1750.txt" TARGET="_top">RFC
     *         1750: Randomness Recommendations for Security</A>
     */
    public static Random getCSRNG() {
        return RNGTools.SecureRandomHolder.myRNG;
    }
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */    
    private static final class SecureRandomHolder {
        static final Random myRNG = new SecureRandom();        
    }
}

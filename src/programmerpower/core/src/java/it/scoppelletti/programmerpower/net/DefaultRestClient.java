/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.net;

import java.net.InetAddress;
import java.net.URI;
import java.security.AccessController;
import java.security.PrivilegedAction;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.Protocol;
import org.restlet.data.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code RestClient}.
 * 
 * @since 2.1.0
 */
@Final
@Reserved
public class DefaultRestClient implements RestClient,
        InitializingBean {
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.host}: Nome dell&rsquo;host
     * del server REST.
     * 
     * @it.scoppelletti.tag.default {@code localhost}.
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_HOST =
            "it.scoppelletti.programmerpower.web.host";
    
    private static final String LOCALHOST = "localhost";
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultRestClient.class);
    private String myHost;
    private int myHttpPort;
    private int myHttpsPort;
    private URI myUri;
    private URI mySecureUri;
    
    /**
     * Costruttore.
     */
    public DefaultRestClient() {
    }
    
    /**
     * Inizializzazione.
     */
    public void afterPropertiesSet() throws Exception {
        String name;
        Reference ref;
        
        name = getHost();
        ref = new Reference(Protocol.HTTP, name, myHttpPort);
        myUri = ref.toUri();
        
        ref = new Reference(Protocol.HTTPS, name, myHttpsPort);
        mySecureUri = ref.toUri();
    }
   
    /**
     * Inizializza il nome dell&rsquo;host dell&rsquo;AS.
     * 
     * @return Valore.
     */
    private String getHost() {
        String name;
        
        if (!StringUtils.isEmpty(myHost)) {
            return myHost;
        }
        
        name = AccessController.doPrivileged(
                new PrivilegedAction<String>() {
                    public String run() {
                        return getLoopbackHost();
                    }
                });
        
        return name;
    }
    
    /**
     * Restituisce il nome dell&rsquo;host di loopback.
     * 
     * @return Valore.
     */
    private String getLoopbackHost() {
        String name;
        InetAddress addr;
        
        addr = InetAddress.getLoopbackAddress();
        if (addr == null) {
            myLogger.warn("Loopback host address not defined.");
            return null;
        }
        
        name = addr.getHostName();
        if (StringUtils.isBlank(name)) {
            myLogger.warn("Loopback host name not defined: using {}.",
                    DefaultRestClient.LOCALHOST);
            name = DefaultRestClient.LOCALHOST;
        } else {
            myLogger.trace("Loopback host name: {}", name);     
        }
        
        return name;
    }
    
    /**
     * Imposta il nome dell&rsquo;host dell&rsquo;AS.
     * 
     * @param value Valore.
     */
    public void setHost(String value) {
        myHost = value;
    }
    
    /**
     * Imposta il numero di porta per il protocollo HTTP.
     * 
     * @param value Valore.
     */
    @Required
    public void setHttpPort(int value) {
        myHttpPort = value;
    }
    
    /**
     * Imposta il numero di porta per il protocollo HTTPS.
     * 
     * @param value Valore.
     */
    @Required
    public void setHttpsPort(int value) {
        myHttpsPort = value;
    }
    
    public URI getServerUri() {
        return myUri;
    }
    
    public URI getServerSecureUri() {
        return mySecureUri;
    }
}

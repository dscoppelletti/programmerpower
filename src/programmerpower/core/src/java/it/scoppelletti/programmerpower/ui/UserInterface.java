/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.ui;

import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.slf4j.ext.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Implementazione di default della
 * <ACRONYM TITLE="User Interface">UI<ACRONYM> {@code UserInterfaceProvider}.
 *
 * <H4 ID="idSourceLocation">1. Locazione del codice sorgente</H4>
 * 
 * <P>Per un evento di log pu&ograve; essere determinata la <DFN>locazione del
 * codice sorgente</DFN> nella quale l&rsquo;evento di log &egrave; stato
 * creato a partire dal nome della classe che ha creato l&rsquo;evento
 * stesso.<BR>
 * Se il metodo {@code beep} di una classe {@code Alarm} emette un evento di
 * log, allora la locazione del codice sorgente nella quale l&rsquo;evento
 * risulta essere creato corrisponde al punto nel quale &egrave; stato
 * eseguito il metodo {@code beep}.<BR>
 * Normalmente gli eventi di log risultano creati dalla stessa classe del 
 * sistema di logging utilizzata, quindi le locazioni del codice sorgente nelle
 * quali gli eventi risultano essere creati corrispondono ai punti nei quali
 * sono stati eseguiti i metodi di tale classe.<BR>
 * Il costruttore della classe base {@code UserInterface} prevede invece un
 * parametro per impostare esplicitamente il nome della classe che dovr&agrave;
 * risultare come quella che ha creato gli eventi di log.</P> 
 *  
 * <P>Si tenga presente che il risultato dell&rsquo;algoritmo per
 * determinare la locazione di codice implementato dal sistema di logging
 * pu&ograve; essere non preciso (o del tutto errato) perch&eacute; il
 * <ACRONYM TITLE="Just-In-Time Compiler">JITter</ACRONYM> di
 * <ACRONYM TITLE="Java Virtual Machine">JVM</ACRONYM> potrebbe applicare delle
 * ottimizzazioni che sopprimono dei frame dallo stack delle chiamate.</P>
 *  
 * @it.scoppelletti.tag.threadsafe
 * @since 1.0.0
 */
public abstract class UserInterface implements UserInterfaceProvider {    
    private static final Logger myLogger = LoggerFactory.getLogger(
            UserInterface.class);
    private final String myCallerClassName;
        
    /**
     * Costruttore.
     *
     * @param callerClassName Nome della classe che crea gli eventi di log.
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/ui/UserInterface.html#idSourceLocation">Locazione
     * del codice sorgente degli eventi di log</A>
     */
    protected UserInterface(String callerClassName) {
        if (StringUtils.isBlank(callerClassName)) {
            throw new ArgumentNullException("callerClassName");            
        }
        
        myCallerClassName = callerClassName;
    }    
          
    @Final
    public void display(MessageType messageType, String message) {
        MessageEvent event;
        
        event = new MessageEvent(messageType, message, null);
        logMessage(event);
        display(event);
    }
    
    @Final
    public void display(MessageType messageType, Throwable ex) {
        MessageEvent event;
        
        event = new MessageEvent(messageType, null, ex);
        logMessage(event);
        display(event);        
    } 
    
    /**
     * Espone un messaggio.
     * 
     * <P>Le classi derivate devono implementare il metodo {@code display} per
     * esporre il messaggio sulla UI specifica dell&rsquo;applicazione.</P>
     *  
     * @param event Evento.
     */
    protected abstract void display(MessageEvent event);
    
    /**
     * Emette un messaggio come evento di log.
     * 
     * @param event Evento.
     */
    @Final
    protected void logMessage(MessageEvent event) {
        Logger logger = new LoggerWrapper(myLogger, myCallerClassName);
        
        if (event == null) {
            throw new ArgumentNullException("event");
        }
        
        switch (event.getMessageType()) {
        case ERROR:
            logger.error(event.getMessage(), event.getThrowable());
            break;
            
        case WARNING:
            logger.warn(event.getMessage(), event.getThrowable());
            break;
            
        case INFORMATION:
            logger.info(event.getMessage(), event.getThrowable());
            break;
            
        default:
            throw new ArgumentOutOfRangeException("event.messageType",
                    event.getMessageType(), MessageType.values());            
        }
    }                 
}

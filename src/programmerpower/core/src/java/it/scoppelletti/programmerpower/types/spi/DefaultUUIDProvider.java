/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * This file is part of Programmer Power.
 *
 * Programmer Power is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Programmer Power is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Programmer Power.  If not, see <http://www.gnu.org/licenses/>.
 */

package it.scoppelletti.programmerpower.types.spi;

import java.util.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code UUIDProvider}.
 * 
 * La classe {@code DefaultUUIDProvider} implementa un generatore di
 * <ACRONYM TITLE="Universally Unique IDentifier">UUID</ACRONYM> della versione
 * 1 (basati sul tempo) secondo l&rsquo;algoritmo descritto nella sezione 4.2
 * delle specifiche <ACRONYM TITLE="Request for Comments">RFC</ACRONYM>
 * 4122.</P>
 * 
 * <H4>1. Precisione dell&rsquo;orario di sistema</H4>
 * 
 * <P>Le specifiche RFC 4122 richiedono una precisione dell&rsquo;orario di
 * sistema in unit&agrave; di 100 nanosecondi; il metodo
 * {@code currentTimeMillis} della classe statica {@code System} ha invece una
 * precisione al millisecondo.<BR>
 * Per implementare la precisione richiesta, lo stato del generatore di UUID
 * mantiene anche un contatore delle estrazioni di UUID pervenute nello stesso
 * millisecondo: fin quando il contatore &egrave; compreso tra 0 e 9,999,
 * pu&ograve; essere combinato con l&rsquo;orario di sistema in modo da poter
 * estrarre fino a 10,000 UUID anche se le richieste pervengono nello stesso
 * millisecondo; se il contatore supera 9,999, allora il generatore azzera il
 * contatore e dovr&agrave; attendere che l&rsquo;orario di sistema avanzi
 * almeno di un millisecondo prima di estrarre il prossimo UUID.</P>
 * 
 * <H4>2. Sequenze di numeri casuali</H4>
 * 
 * <P>Il generatore di UUID utilizza il <ACRONYM
 * TITLE="Cryptographically Secure Random Number Generator">CSRNG</ACRONYM>  
 * pubblicato dalla classe {@code RNGUtils}.</P>
 * 
 * <H4 id="idMAC">3. Indirizzo MAC della scheda di rete</H4>
 *
 * <P>Una delle componenti per rendere univoci gli UUID estratti &egrave;
 * l&rsquo;indirizzo <ACRONYM TITLE="Media Access Control">MAC</ACRONYM> della
 * scheda di rete; questo indirizzo &egrave; rilevato dal metodo statico 
 * {@code getNetworkInterfaces} della classe {@code NetworkInterface}; tale
 * metodo, se la scheda di rete non &egrave; attiva, non la rileva tra le
 * interfacce di rete, e quindi l&rsquo;indirizzo della scheda non risulta
 * determinabile.<BR>
 * Un&rsquo;alternativa sarebbe eseguire il comando di sistema per elencare le
 * interfacce e intercettarne e analizzarne l&rsquo;output; ma questo comando
 * dipenderebbe dalla piattaforma (ad esempio, per Linux, il comando &egrave;
 * {@code /sbin/ifconfig -a} con l&rsquo; opzione {@code -a} che serve appunto
 * ad elencare anche le interfacce non attive). Piuttosto che introdurre una
 * dipendenza di Programmer Power dalla piattaforma, si &egrave; preferito
 * considerare che, al giorno d&rsquo;oggi, ormai i computer normalmente operano
 * costantemente collegati alla rete (quindi il metodo
 * {@code getNetworkInterfaces} dovrebbe essere sufficiente).<BR>
 * Se il generatore di UUID non riesce a rilevare l&rsquo;indirizzo MAC della
 * scheda di rete, lo segnala su log e procede con l&rsquo;estrazione di un
 * indirizzo casuale.</P>
 * 
 * <P>Per ogni estrazione di un nuovo UUID, il generatore verifica se
 * l&rsquo;indirizzo MAC della scheda di rete &egrave; cambiato rispetto alla
 * precedente estrazione e, in tal caso, estrae una nuova <DFN>sequenza di
 * clock</DFN> per limitare la probabilit&agrave; di generazione di UUID
 * dupplicati.<BR>
 * Se si pu&ograve; ragionevolmente garantire che, almeno durante
 * l&rsquo;esecuzione dell&rsquo;applicazione, l&rsquo;indirizzo MAC della
 * scheda di rete non pu&ograve; cambiare (magari perch&eacute; questo
 * richiederebbe un riavvio del sistema), &egrave; possibile impostare la
 * propriet&agrave; di sistema
 * {@code it.scoppelletti.programmerpower.types.UUIDGenerator.noMACChangingTest}
 * con il valore <code>&quot;true&quot;</code> in modo da eseguire la verifica
 * sulla variazione dell&rsquo;indirizzo MAC solo per il primo UUID estratto
 * (questa configurazione &egrave; segnalata su log).<BR>
 * Evitare il controllo della variazione dell&rsquo;indirizzo MAC pu&ograve;
 * migliorare le prestazioni del generatore di UUID.<BR>
 * Ovviamente, se l&rsquo;indirizzo MAC non &egrave; determinabile e viene
 * estratto casualmente, il generatore di UUID non ne verifica in ogni caso
 * l&rsquo;eventuale variazione.</P>
 *   
 * <H4 id="idState">4. Stato del generatore</H4>
 * 
 * <P>La classe {@code DefaultUUIDProvider} utilizza uno stato per la
 * generazione di UUID <DFN>volatile</DFN> ovvero globale per il solo processo
 * in esecuzione; le classi derivate possono implementare una versione
 * prevalente del metodo {@link #initState} per utilizzare
 * un&rsquo;implementazione alternativa dello stato derivata dalla classe
 * astratta {@code UUIDState}.</P>
 *  
 * @see   it.scoppelletti.programmerpower.types.RNGUtils#getCSRNG
 * @see   it.scoppelletti.programmerpower.types.UUIDGenerator
 * @see   it.scoppelletti.programmerpower.types.spi.UUIDState 
 * @see   <A HREF="http://www.ietf.org/rfc/rfc4122.txt" TARGET="_top">RFC
 *        4122: A Universally Unique IDentifier (UUID) URN Namespace</A>    
 * @since 2.1.0
 */
public class DefaultUUIDProvider implements UUIDProvider {
    
    /**
     * Numero massimo di UUID che &egrave; possibile estrarre in un&rsquo;unica
     * sessione di blocco sullo stato. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #newUUIDs
     */
    public static final int SLICE_SIZE = 30000;
                 
    /**
     * Differenza (in unit&agrave; da 100 nanosecondi) dalla mezzanotte del
     * 15 ottobre 1582 (data di decorrenza del calendario Gregoriano) alla
     * mezzanotte del 1&ordm; gennaio 1970 (data di base richiesta da RFC 4122).
     * Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    private static final long TIMESTAMP_OFFSET = 0x01B21DD213814000L;
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultUUIDProvider.class);
    private final Object mySyncRoot = new Object();    
    private UUIDState myState = null;

    /**
     * Costruttore.
     */
    public DefaultUUIDProvider() {        
    }

    // Come ottimizzazione, l'inizializzazione della costante TIMESTAMP_OFFSET
    // attraverso il metodo statico initTimestampOffset e' stata sostituita
    // con l'inizializzazione al valore restituito.
//    /**
//     * Calcola la differenza mezzanotte del 15 ottobre 1582 alla mezzanotte
//     * del 1&ordm; gennaio 1970.
//     * 
//     * @return Valore (in unit&agrave; da 100 nanosecondi).
//     */
//    private static long initTimestampOffset() {
//       long offset;
//       Calendar date1, date0;
//       
//       date0 = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
//       date0.set(1582, Calendar.OCTOBER, 15, 0, 0, 0);
//       date0.set(Calendar.MILLISECOND, 0);       
//       date1 = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
//       date1.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
//       date1.set(Calendar.MILLISECOND, 0);
//       
//       offset = date1.getTimeInMillis() - date0.getTimeInMillis();
//       
//       return offset * DefaultUUIDProvider.TIMESTAMPFRAC_COUNT;
//    }
    
    /**
     * Inizializza lo stato del generatore di UUID. 
     * 
     * @return Oggetto. Non pu&ograve; essere {@code null}.
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/types/spi/DefaultUUIDProvider.html#idState">Stato
     *      del generatore</A>
     */
    protected UUIDState initState() {
        return new UUIDVolatileState();
    }
        
    public final UUID newUUID() {
        long millis, nodeID;
        short clockSeq, frac;
        UUID value;
        
        synchronized (mySyncRoot) {
            if (myState == null) {
                myState = initState();
                if (myState == null) {
                    throw new ReturnNullException(toString(), "initState");
                }                
            }
            
            myState.lock();
            try {
                myState.nextUUID();                
                millis = myState.getTimestampMillis();
                frac = myState.getTimestampFrac();
                clockSeq = myState.getClockSequence();
                nodeID = myState.getNodeID();
                myState.save(1);
            } finally {
                myState.release();
            }
        }
        
        value = UUIDState.calcUUID(calcTimestamp(millis, frac), clockSeq,
                nodeID);
        
        return value;
    }
    
    /**
     * Estrae un certo numero di UUID.
     * 
     * <P>Quando &egrave; possibile predeterminare il numero di UUID che
     * &egrave; necessario estrarre, l&rsquo;implementazione di default del
     * metodo {@code newUUIDs} &egrave; ottimizzata per estrarre tutti gli UUID
     * richiesti eseguendo un solo aggiornamento dello stato del generatore.<BR>
     * Sostanzialmente l&rsquo;ottimizzazione consiste nel <I>sommare</I>
     * all&rsquo;orario di sistema il numero di UUID richiesti come unit&agrave;
     * da 100 nanosecondi, anche se poi ovviamente il metodo dovr&agrave;
     * rimanere in attesa che l&rsquo;orario di sistema raggiunga effettivamente
     * l&rsquo;orario corrispondente all&rsquo;ultimo UUID estratto prima di 
     * poter ritornare.<BR>
     * Ricordando che l&rsquo;orario di sistema Java ha una precisione al
     * millisecondo, risulterebbe un&rsquo;attesa massima di un solo
     * millisecondo ogni 10,000 UUID estratti (assumendo che i calcoli siano
     * istantanei).</P>
     * 
     * <P>Ovviamente, estraendo un numero di UUID dell&rsquo;ordine delle decine
     * di milioni, le attese diverrebbero dell&rsquo;ordine dei secondi, durante
     * i quali la risorsa dello stato del generatore risulterebbe bloccata e
     * quindi eventuali altri processi (e/o thread) che dovrebbero estrarre
     * degli UUID rimarrebbero in attesa.<BR>
     * Per migliorare la concorrenza, il numero di UUID da estrarre &egrave;
     * suddiviso in <I>porzioni</I> al massimo di 30,000 unit&agrave;: il
     * generatore aggiorna lo stato dopo l&rsquo;estrazione di ogni porzione e
     * quindi la sessione di blocco dello stato del generatore delimita
     * l&rsquo;estrazione di ogni singola porzione di UUID; tra una porzione e
     * l&rsquo;altra, &egrave; inserita un&rsquo;attesa di un millisecondo
     * durante la quale eventuali altri processi (e/o thread) possono inserirsi
     * per estrarre a loro volta degli UUID perch&eacute; lo stato risulta
     * sbloccato.</P> 
     * 
     * @param  count Numero di UUID da estrarre.
     * @return       Collezione.
     */    
    public final Iterator<UUID> newUUIDs(int count) {
        int i, n;
        boolean interruptPending;
        UUIDChainedIterator chain;
        
        if (count <= DefaultUUIDProvider.SLICE_SIZE) {
            return buildSlice(count);
        }
        
        chain = new UUIDChainedIterator();
        n = count % DefaultUUIDProvider.SLICE_SIZE;
        if (n > 0) {
            chain.add(buildSlice(n));
        }
        
        // Durante il ciclo di estrazione degli UUID ignoro le eventuali
        // richieste di interruzione ricevute per poi rispondere alla fine.
        interruptPending = Thread.interrupted();
        n = (count - n) / DefaultUUIDProvider.SLICE_SIZE;
        for (i = 0; i < n; i++) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                myLogger.warn("Sleep interrupted.", ex);
                interruptPending = true;
            }            
            chain.add(buildSlice(DefaultUUIDProvider.SLICE_SIZE));
        }
        
        if (interruptPending) {
            Thread.currentThread().interrupt();
        }
        
        return chain;
    }
    
    /**
     * Estrae un certo numero di UUID.
     * 
     * @param  count Numero di UUID da estrarre.
     * @return       Collezione.
     */
    private Iterator<UUID> buildSlice(int count) {
        int fracStart, fracEnd, fracAvail;
        long millisStart, millisEnd, nodeID;
        short clockSeq;
        Iterator<UUID> it;

        synchronized (mySyncRoot) {
            if (myState == null) {
                myState = initState();
                if (myState == null) {
                    throw new ReturnNullException(toString(), "initState");
                }
            }
        
            myState.lock();
            try {
                myState.nextUUID();            
                millisStart = myState.getTimestampMillis();
                fracStart = myState.getTimestampFrac();
            
                // Frazioni disponibili del timestamp corrente
                fracAvail = UUIDState.TIMESTAMPFRAC_COUNT - fracStart;
                if (fracAvail < count) {
                    // Le frazioni disponibili del primo timestamp non sono
                    // sufficienti
                    count -= fracAvail;
                    
                    fracEnd = count % UUIDState.TIMESTAMPFRAC_COUNT;
                    count -= fracEnd;
                    millisEnd = millisStart + 
                            (count / UUIDState.TIMESTAMPFRAC_COUNT);
                    
                    if (fracEnd > 0) {
                        // Devo allocare alcune frazioni di un ulteriore
                        // timestamp
                        millisEnd++;
                        fracEnd--;                    
                    } else {
                        // Alloco tutte le frazioni dell'ultimo timestamp
                        fracEnd = UUIDState.TIMESTAMPFRAC_COUNT - 1;
                    }                
                } else {
                    // Le frazioni disponibili del primo timestamp sono
                    // sufficienti
                    millisEnd = millisStart;
                    fracEnd = fracStart + count - 1;
                }
        
                myState.setTimestampMillis(millisEnd);
                myState.setTimestampFrac((short) fracEnd);
                clockSeq = myState.getClockSequence();
                nodeID = myState.getNodeID();       
                myState.save(millisEnd - millisStart + 1);      
            } finally {
                myState.release();
            }
        }
        
        it = new UUIDIterator(calcTimestamp(millisStart, fracStart), clockSeq,
                nodeID, count);
        
        return it;
    }
            
    /**
     * Calcola un timestamp.
     * 
     * <H4>1. RFC 4122, sezione 4.1.4</H4>
     * 
     * <P>Il timestamp deve essere un orario UTC espresso in unit&agrave; da 100
     * nanosecondi a partire dalla mezzanotte del 15 ottobre 1582; l&rsquo;ora
     * di sistema restituita dal metodo {@code currentTimeMillis} della classe
     * statica {@code System} &egrave; invece un orario UTC espresso in
     * millisecondi a partire dalla mezzanotte del 1&ordm; gennaio 1970.</P>
     *  
     * @param  millis Timestamp in millisecondi a partire dalla mezzanotte del
     *                1&ordm; gennaio 1970. 
     * @param  frac   Frazione del timestamp in unit&agrave; da 100 nanosecondi.
     * @return        Valore.
     */
    private long calcTimestamp(long millis, int frac) {
        long timestamp;

        timestamp = millis * UUIDState.TIMESTAMPFRAC_COUNT + frac +
            DefaultUUIDProvider.TIMESTAMP_OFFSET;        
        timestamp &= 0x0FFFFFFFFFFFFFFFL; // 60 bit
        
        return timestamp;
    }      
}

/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.spring.config;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.core.env.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Funzioni di utilit&agrave; per la configurazione Spring.
 *
 * @since 2.1.0
 */
public final class BeanConfigTools {    
    
    /**
     * Nome della risorsa nei moduli JAR che contribuisce alla configurazione
     * del contesto Spring di un&rsquo;applicazione. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.programmerpower.web.spring.XmlWebApplicationContextEx
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/console/spring/ConsoleApplicationRunner.html#idSpringCtx">Contesto
     *      Spring per le applicazioni su console.</A>
     */
    public static final String MODULE_CONTEXT =
        "/META-INF/it-scoppelletti-moduleContext.xml";
    
    /**
     * Nome della risorsa di configurazione delle propriet&agrave; di ambiente.
     * Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #loadPropertySource
     */
    public static final String CONFIG_FILE =
            "it-scoppelletti-programmerpower-beans.properties";

    private static final char CLASS_SEP = '.';
    private static final char BEAN_SEP = '-';    
    private static final Logger myLogger = LoggerFactory.getLogger(
            BeanConfigTools.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private BeanConfigTools() {        
    }
    
    /**
     * Restituisce la sorgente per la sostituzione dei riferimenti alle
     * propriet&agrave; nelle risorse di definizione dei bean.
     * 
     * @return Collezione. Se la sorgente delle propriet&agrave; non &egrave;
     *         rilevata, restituisce {@code null}.
     * @see #CONFIG_FILE
     * @see it.scoppelletti.programmerpower.console.spring.ConsoleApplicationRunner
     * @see it.scoppelletti.programmerpower.web.spring.config.WebApplicationContextInitializer
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A>
     */
    public static PropertySource<?> loadPropertySource() {
        InputStream in = null;
        Properties props;
        
        in = ReflectionTools.getResourceAsStream(BeanConfigTools.CONFIG_FILE);
        if (in == null) {
            return null;
        }
        
        props = new Properties();
        try {
            props.load(in);
        } catch (IOException ex) {
            myLogger.error(String.format("Failed to load %1$s.",
                    BeanConfigTools.CONFIG_FILE), ex);
            return null;
        } finally {
            in = IOTools.close(in);
        }
        
        return new PropertiesPropertySource(BeanConfigTools.CONFIG_FILE, props);
    }
        
    /**
     * Restituisce un possibile nome per un bean.
     * 
     * @param  clazz Classe che implementa il bean.
     * @return       Valore.
     */
    public static String getBeanName(Class<?> clazz) {
        String name;
        
        if (clazz == null) {
            throw new ArgumentNullException("clazz");
        }
        
        name = clazz.getCanonicalName();
        if (StringUtils.isBlank(name)) {
            name = BeanConfigTools.class.getCanonicalName().concat(
                    String.valueOf(clazz.hashCode()));
        }
        
        name = name.replace(BeanConfigTools.CLASS_SEP,
                BeanConfigTools.BEAN_SEP);        
        return name;        
    }
}

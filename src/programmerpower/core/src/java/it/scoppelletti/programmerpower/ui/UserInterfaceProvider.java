/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.ui;

/**
 * <ACRONYM TITLE="User Interface">UI</ACRONYM> generica delle applicazioni.
 *
 * @since 2.0.0
 */
public interface UserInterfaceProvider {

    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_NAME =
        "it-scoppelletti-programmerpower-ui-userInterface";
        
    /**
     * Espone un messaggio.
     *
     * @param messageType Tipo di messaggio.
     * @param message     Messaggio. Pu&ograve; essere {@code null}.
     */
    void display(MessageType messageType, String message);
    
    /**
     * Espone un&rsquo;eccezione.
     *
     * @param messageType Tipo di messaggio. Normalmente le eccezioni
     *                    rappresentano degli errori
     *                    ({@code MessageType.ERROR}); un&rsquo;eccezione 
     *                    pu&ograve; tuttavia anche essere esposta sulla UI
     *                    abbassando il livello di gravit&agrave; ad esempio ad
     *                    avviso ({@code MessageType.WARNING}).
     * @param ex          Eccezione. Pu&ograve; essere {@code null}.
     */    
    void display(MessageType messageType, Throwable ex);  
}

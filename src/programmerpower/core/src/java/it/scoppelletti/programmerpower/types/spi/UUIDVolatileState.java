/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types.spi;

import org.slf4j.*;

/**
 * Stato volatile per il generatore {@code DefaultUUIDProvider}.
 */
final class UUIDVolatileState extends UUIDState {
    private static final Logger myLogger = LoggerFactory.getLogger(
            UUIDVolatileState.class);            
    
    private boolean myFirstLoad = true;
    
    /**
     * Costruttore.
     */
    UUIDVolatileState() {
        myLogger.warn("Persistent state not implemented.");
    }
    
    protected void lock() {        
    }
    
    protected void release() {        
    }
        
    protected boolean load() {
        if (myFirstLoad) {
            myFirstLoad = false;
            return false;
        }
        
        return true;
    }
    
    protected void save() {       
    }    
}

/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types.spi;

import java.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Collezione composta di
 * <ACRONYM TITLE="Universally Unique IDentifier">UUID</ACRONYM>.
 */
final class UUIDChainedIterator implements Iterator<UUID> {
    private final Queue<Iterator<UUID>> myChain;
    
    /**
     * Costruttore.
     */
    UUIDChainedIterator() {        
        myChain = new LinkedList<Iterator<UUID>>();
    }
    
    /**
     * Verifica se ci sono ancora elementi da visitare.
     * 
     * @return Esito della verifica.
     */
    public boolean hasNext() {
        Iterator<UUID> it;
        
        it = myChain.peek();
        while (it != null && !it.hasNext()) {
            myChain.poll();
            it = myChain.peek();
        }        
        if (it == null) {
            return false;
        }
        
        return it.hasNext();
    }

    /**
     * Restituisce l&rsquo;elemento successivo.
     * 
     * @return Elemento.
     */
    public UUID next() {
        Iterator<UUID> it;
        
        it = myChain.peek();
        while (it != null && !it.hasNext()) {
            myChain.poll();
            it = myChain.peek();
        }
        if (it == null) {
            throw new NoSuchElementException();
        }
        
        return it.next();
    }

    /**
     * Aggiunge un iteratore.
     * 
     * @param it Collezione.
     */
    void add(Iterator<UUID> it) {
        if (it == null) {
            throw new ArgumentNullException("it"); 
        }
        
        myChain.add(it);
    }
    
    /**
     * Cancella l&rsquo;elemento corrente.
     */
    public void remove() {
        throw new NotImplementedException(Iterator.class.getName(),
                "remove");        
    }    
}

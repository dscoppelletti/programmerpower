/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

import java.lang.ref.*;
import java.security.*;
import java.util.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;

/**
 * Classe di base di un modello
 * <ACRONYM TITLE="Service Provider Interface">SPI</ACRONYM>.
 * 
 * <P>I provider alternativi di un servizio possono essere distribuiti secondo
 * le specifiche SPI dei file <ACRONYM TITLE="Java ARchive">JAR</ACRONYM>; in
 * particolare, nella cartella {@code META-INF/services} di ogni modulo JAR
 * pu&ograve; essere inserito un file di testo con le seguenti
 * caratteristiche:</P>
 * 
 * <OL>
 * <LI>Il nome del file coincide con il nome completo dell&rsquo;interfaccia che
 * deve essere implementata dai provider del servizio (o della classe di base
 * dalla quale gli stessi devono derivare).
 * <LI>Ogni linea di tale file deve risportare il nome completo di una delle
 * classi che implementano il servizio e distribuite con il modulo JAR.
 * </OL> 
 * 
 * @param <TProvider> Interfaccia dei provider del servizio (o classe di base
 *                    dalla quale gli stessi devono derivare).
 * @param <TResult>   Tipo di risultato ottenuto dal servizio.
 * @since             2.0.0
 */
public abstract class ServiceLoaderWrapper<TProvider, TResult> {
    private static final Logger myLogger = LoggerFactory.getLogger(
            ServiceLoaderWrapper.class);    
    private static final Map<Class<?>,
        ServiceLoaderWrapper.ServiceEntry> myRegistry =
        new HashMap<Class<?>, ServiceLoaderWrapper.ServiceEntry>();
    private final Class<TProvider> myProviderClass;
   
    /**
     * Costruttore.
     * 
     * @param providerClass Interfaccia dei provider del servizio (o classe di
     *                      base dalla quale gli stessi devono derivare).
     */
    protected ServiceLoaderWrapper(Class<TProvider> providerClass) {
        if (providerClass == null) {
            throw new ArgumentNullException("providerClass");
        }
        
        myProviderClass = providerClass;
    }
    
    /**
     * Esegue l&rsquo;interrogazione del servizio.
     * 
     * <P>Il metodo {@code query} interroga tutti i provider del servizio
     * rilevati fino a che uno di questi restituisce un risultato diverso da
     * {@code null} e restituisce tale risultato.<BR>
     * L&rsquo;ordine di interrogazione dei vari provider non &egrave;
     * determinabile.</P>
     *  
     * @return Risultato della richiesta. Se nessuno dei provider interrogati
     *         restituisce un risultato diverso da {@code null}, restituisce
     *         {@code null}.
     */
    public TResult query() {
        return AccessController.doPrivileged(new PrivilegedAction<TResult>() {
            public TResult run() {
                return doQuery();
            }
        });
    }
    
    /**
     * Esegue l&rsquo;interrogazione del servizio.
     * 
     * @return Risultato.
     */
    @SuppressWarnings("unchecked")
    private TResult doQuery() {
        TResult result;
        TProvider provider;
        ServiceLoader<TProvider> providers;
        Iterator<TProvider> it;
        ServiceLoaderWrapper.ServiceEntry serviceEntry;
        
        // Inizializzazione una-tantum del registro dei servizi
        synchronized (myRegistry) {
            serviceEntry = myRegistry.get(myProviderClass);
            if (serviceEntry != null) {
                providers = serviceEntry.getProviders();
            } else {
                providers = null;
                serviceEntry = new ServiceLoaderWrapper.ServiceEntry();
                myRegistry.put(myProviderClass, serviceEntry);                                
            }           
        }
                
        synchronized (serviceEntry) {
            if (providers == null) {
                myLogger.trace("Searching for {} providers.",
                        myProviderClass.getCanonicalName());
                try {
                    providers = ServiceLoader.load(myProviderClass);
                } catch (Exception ex) {
                    // Traccio l'eccezione e proseguo ignorando i provider
                    myLogger.error("Failed to load providers.", ex);
                    return null;
                }                    
                
                serviceEntry.setProviders(providers);
            }
            
            it = providers.iterator();
            while (true) {
                try {
                    if (!it.hasNext()) {
                        break;
                    }
                    provider = it.next();
                } catch (ServiceConfigurationError ex) {
                    // I metodi hasNext e next dell'iteratore possono inoltrare
                    // un errore ServiceConfigurationError per anomalie nella
                    // lettura dei file di configurazione del servizio:
                    // Traccio l'eccezione e interrompo il ciclo.
                    myLogger.error("Failed to query providers.", ex);
                    break;
                }
                
                myLogger.trace("Querying provider {} (service {}).",
                        provider.getClass().getCanonicalName(),
                        myProviderClass.getCanonicalName());                
                try {
                    result = query(provider);
                    if (result != null) {
                        return result;
                    }                    
                } catch (Exception ex) {
                    // Traccio l'eccezione e proseguo con gli eventuali provider
                    // successivi
                    myLogger.error("Failed to query provider.", ex);
                }
            }            
        }
        
        return null;
    }

    /**
     * Esegue l&rsquo;interrogazione di un provider.
     * 
     * @param  provider Provider.
     * @return          Risultato. Se il provider non &egrave; in grado di
     *                  rispondere all&rsquo;interrogazione, restituisce 
     *                  {@code null}.
     */
    protected abstract TResult query(TProvider provider);

    /**
     * Servizio.
     */
    @SuppressWarnings("rawtypes")
    private static class ServiceEntry {
        private SoftReference<ServiceLoader> myCache;
        
        /**
         * Costruttore.
         */
        ServiceEntry() {            
        }

        /**
         * Restituisce i provider del servizio.
         * 
         * @return Collezione.
         */
        ServiceLoader getProviders() {
            if (myCache == null) {
                return null;
            }
            
            return myCache.get();
        }
        
        /**
         * Imposta i provider del servizio.
         * 
         * @param providers Collezione.
         */
        void setProviders(ServiceLoader providers) {
            myCache = new SoftReference<ServiceLoader>(providers);
        }
    }
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.net;

import java.util.Locale;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.Language;
import org.restlet.data.Reference;
import org.restlet.representation.Representation;
import org.restlet.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.PropertyNotSetException;

/**
 * Funzioni di utilit&agrave;
 * <ACRONYM TITLE="REpresentional State Transfer">REST</ACRONYM>.
 * 
 * @see   <A HREF="${it.scoppelletti.token.restletUrl}"
 *        TARGET="_top">Restlet</A>
 * @since 2.1.0
 */
public final class RestTools {
    private static final Logger myLogger = LoggerFactory.getLogger(
            RestTools.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private RestTools() {
    }
    
    /**
     * Imposta il percorso di un URI.
     * 
     * @param  <T>  Classe dell&rsquo;URI.
     * @param  ref  URI.
     * @param  path Percorso.
     * @return      URI per metodi concatenati.
     */
    public static <T extends Reference> T setPath(T ref, String path) {
        if (ref == null) {
            throw new ArgumentNullException("ref");
        }
        
        if (StringUtils.isBlank(path)) {
            ref.setPath(StringUtils.EMPTY);
        } else {
            RestTools.addPath(ref, path, true);
        }
        
        return ref;
    }
    
    /**
     * Aggiunge un percorso ad un URI.
     * 
     * @param  <T>  Classe dell&rsquo;URI.
     * @param  ref  URI.
     * @param  path Percorso.
     * @return      URI per metodi concatenati.
     */
    public static <T extends Reference> T addPath(T ref, String path) {
        if (StringUtils.isBlank(path)) {
            return ref;
        }        
        if (ref == null) {
            throw new ArgumentNullException("ref");
        }

        RestTools.addPath(ref, path, true);
        return ref;
    }
    
    /**
     * Aggiunge un percorso ad un URI.
     * 
     * @param  ref  URI.
     * @param  path Percorso.
     * @return      URI per metodi concatenati.
     */
    private static void addPath(Reference ref, String path, boolean append) {
        int len, start, end;
        String value;
        
        start = 0;
        len = path.length();
        while (start < len) {
            end = path.indexOf(NetTools.PATH_SEP_CHAR, start);
            if (end < 0) {
                end = len;
            }
            
            if (end > start) {
                value = path.substring(start, end);
                if (append) {
                    ref.addSegment(value);
                } else {
                    ref.setPath(value);
                    append = true;
                }
            }
            
            start = end + 1;
        }
    }    
    
    /**
     * Codifica una localizzazione in un linguaggio.
     * 
     * @param  locale Localizzazione.
     * @return        Linguaggio.
     */
    public static Language toLanguage(Locale locale) {
        String country, value;
        
        if (locale == null) {
            throw new ArgumentNullException("locale");
        }
         
        value = locale.getLanguage();
        if (StringUtils.isBlank(value)) {
            throw new PropertyNotSetException(locale.toString(), "language");
        }
        
        country = locale.getCountry();
        if (!StringUtils.isBlank(country)) {
            value = StringUtils.join(value, "-", country.toLowerCase());
        }
        
        return Language.valueOf(value);        
    }
    
    /**
     * Rilascia una risorsa.
     * 
     * <P>Il metodo {@code release} rilascia una risorsa ignorando
     * l&rsquo;eventuale eccezione inoltrata (tale eccezione viene emessa come
     * segnalazione di log).<BR>
     * Il valore di ritorno {@code null} pu&ograve; essere utilizzato per
     * azzerare la stessa variabile {@code res} in modo che tale variabile
     * faccia anche da indicatore di risorsa attiva ({@code res != null}) o
     * rilasciata ({@code res == null}).</P>
     * 
     * @param  <T> Classe della risorsa.
     * @param  res Risorsa. Se &egrave; {@code null}, si assume che la risorsa
     *             sia gi&agrave; stata rilasciata.
     * @return     {@code null}.
     */    
    public static <T extends Resource> T release(T res) {
        if (res != null) {
            try {
                res.release();
            } catch (RuntimeException ex) {
                myLogger.error("Failed to release resource.", ex);
            }            
        }
        
        return null;
    }
    
    /**
     * Rilascia una rappresentazione.
     * 
     * <P>Il metodo {@code release} rilascia una rappresentazione ignorando
     * l&rsquo;eventuale eccezione inoltrata (tale eccezione viene emessa come
     * segnalazione di log).<BR>
     * Il valore di ritorno {@code null} pu&ograve; essere utilizzato per
     * azzerare la stessa variabile {@code rep} in modo che tale variabile
     * faccia anche da indicatore di rappresentazione attiva
     * ({@code rep != null}) o rilasciata ({@code rep == null}).</P>
     * 
     * @param  <T> Classe della rappresentazione.
     * @param  rep Rappresentazione. Se &egrave; {@code null}, si assume che la
     *             risorsa sia gi&agrave; stata rilasciata.
     * @return     {@code null}.
     */    
    public static <T extends Representation> T release(T rep) {
        if (rep != null) {
            try {
                rep.release();
            } catch (RuntimeException ex) {
                myLogger.error("Failed to release resource.", ex);
            }            
        }
        
        return null;
    }
}

/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Istante.
 * 
 * <P>A differenza delle classi {@code Calendar} e {@code Date} di
 * <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM>, le istanze della classe
 * {@code SimpleTimestamp} le istanze sono immutabili.</P>
 * 
 * @since 2.0.0
 */
public final class SimpleTimestamp implements Comparable<SimpleTimestamp>,
        ValueType {   
    private static final long serialVersionUID = 2L;

    /**
     * Istante nullo.
     * 
     * @see #isEmpty
     */
    public static final SimpleTimestamp NIL = new SimpleTimestamp();
    
    /**
     * @serial Data.
     */
    private final SimpleDate myDate;
    
    /**
     * @serial Ora.
     */
    private final SimpleTime myTime;
    
    /**
     * Costruttore.
     */
    private SimpleTimestamp() {
        myDate = SimpleDate.NIL;
        myTime = SimpleTime.NIL;
    }
    
    /**
     * Costruttore.
     * 
     * @param date Data.
     */
    public SimpleTimestamp(SimpleDate date) {
        this(date, new SimpleTime(0, 0, 0, 0));
    }
    
    /**
     * Costruttore.
     * 
     * @param date Data.
     * @param time Ora.
     */
    public SimpleTimestamp(SimpleDate date, SimpleTime time) {
        if (ValueTools.isNullOrEmpty(date)) {
            throw new ArgumentNullException("date");
        }
        if (ValueTools.isNullOrEmpty(time)) {
            throw new ArgumentNullException("time");
        }
        
        myDate = date;
        myTime = time;
    }
    
    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        if (ValueTools.isNullOrEmpty(myDate) &&
                ValueTools.isNullOrEmpty(myTime)) {
            return SimpleTimestamp.NIL;
        }
        
        return new SimpleTimestamp(myDate, myTime);
    }
        
    /**
     * Restituisce l&rsquo;istante corrente.
     * 
     * @return Valore.
     * @see    it.scoppelletti.programmerpower.types.TimeTools#getNow
     */
    public static SimpleTimestamp getNow() {
        return toSimpleTimestamp(TimeTools.getNow());
    }
    
    /**
     * Restituisce l&rsquo;istante corrispondente ad un calendario.
     * 
     * @param  value Calendario.
     * @return       Istante. Se il calendario {@code value} &egrave;
     *               {@code null}, restituisce l&rsquo;istante nullo.
     * @see          #NIL               
     */
    public static SimpleTimestamp toSimpleTimestamp(Calendar value) {
        if (value == null) {
            return SimpleTimestamp.NIL;
        }
        
        return new SimpleTimestamp(SimpleDate.toSimpleDate(value),
                SimpleTime.toSimpleTime(value));
    }
    
    /**
     * Restituisce la data.
     * 
     * @return Valore. Se l&rsquo;istante &egrave; nullo, restituisce la data
     *         nulla.
     * @see    #NIL
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.SimpleDate#NIL
     */
    public SimpleDate getDate() {
        return myDate;
    }
    
    /**
     * Restituisce l&rsquo;orario.
     * 
     * @return Valore. Se &rsquo;istante &egrave; nullo, restituisce
     *         l&rsquo;istante nullo.
     *         {@code null}.
     * @see    #isEmpty         
     * @see    it.scoppelletti.programmerpower.types.SimpleTime#NIL
     */
    public SimpleTime getTime() {
        return myTime;
    }
    
    /**
     * Restituisce l&rsquo;anno.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getYear() {
        return myDate.getYear();
    }
    
    /**
     * Restituisce il mese.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getMonth() {
        return myDate.getMonth();
    }      
    
    /**
     * Restituisce il giorno.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getDay() {
        return myDate.getDay();
    }
    
    /**
     * Restituisce l&rsquo;ora.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getHour() {
        return myTime.getHour();
    }
    
    /**
     * Restituisce il minuto.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getMinute() {
        return myTime.getMinute();
    }    
    
    /**
     * Restituisce il secondo.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getSecond() {
        return myTime.getSecond();
    }  
    
    /**
     * Restituisce il millisecondo.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getMillis() {
        return myTime.getMillis();
    }      
        
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * <P>Questa versione del metodo {@code toString} restituisce la data in
     * formato ISO 8601 ({@code YYYY-MM-DDTHH-MM-SS.mmm}).</P>
     * 
     * @return Stringa. Se l&rsquo;istante &egrave; nullo, restituisce la
     *         stringa vuota {@code ""}.
     * @see    #NIL
     * @see    #isEmpty
     */
    @Override
    public String toString() {
        if (isEmpty()) {
            return StringUtils.EMPTY;
        }
        
        return String.format("%1$sT%2$s", myDate, myTime);
    }   
     
    public boolean isEmpty() {
        return (myDate.isEmpty());
    }

    /**
     * Converte l&rsquo;istante in un calendario.
     * 
     * @return Valore. Se l&rsquo;istante &egrave; nullo, restituisce
     *         {@code null}.
     * @see    #NIL
     * @see    #isEmpty
     */
    public Calendar toCalendar() {
        if (isEmpty()) {
            return null;
        }
        
        Calendar value = TimeTools.getNow();
        
        value.set(Calendar.YEAR, myDate.getYear());
        value.set(Calendar.MONTH, myDate.getMonth());
        value.set(Calendar.DAY_OF_MONTH, myDate.getDay());
        value.set(Calendar.HOUR_OF_DAY, myTime.getHour());
        value.set(Calendar.MINUTE, myTime.getMinute());
        value.set(Calendar.SECOND, myTime.getSecond());
        value.set(Calendar.MILLISECOND, myTime.getMillis());
        
        return value;
    }
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        SimpleTimestamp op;
        
        if (!(obj instanceof SimpleTimestamp)) {
            return false;
        }
        
        op = (SimpleTimestamp) obj;
        
        if (!myDate.equals(op.getDate()) ||
                !Objects.equals(myTime, op.getTime())) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myDate.hashCode();
        value = 37 * value + myTime.hashCode();
        
        return value;
    }

    /**
     * Confronto con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Risultato del confronto.
     */
    public int compareTo(SimpleTimestamp obj) {
        int cmp;
        
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }        

        if (isEmpty() && obj.isEmpty()) {
            return 0;
        } else if (isEmpty()) {
            return -1;
        } else if (obj.isEmpty()) {
            return 1;
        }
        
        cmp = myDate.compareTo(obj.getDate());
        if (cmp != 0) {
            return cmp;
        }
        
        cmp = myTime.compareTo(obj.getTime());
        if (cmp != 0) {
            return cmp;
        }
        
        return 0;
    }    
}

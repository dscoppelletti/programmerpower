/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.threading;

import java.util.*;
import org.apache.commons.lang3.*;
import org.apache.commons.lang3.builder.*;
import org.slf4j.*;
import it.scoppelletti.diagnostic.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Un oggetto {@code ThreadContext} definisce lo <I>spazio del processo</I>, o
 * appunto il <DFN>contesto</DFN>, corrispondente ad un thread
 * dell&rsquo;applicazione.
 *
 * <P>Un <DFN>thread logico</DFN> &egrave; un <I>task</I> che potrebbe essere
 * eseguito sia attraverso un thread dedicato che ricorrendo ad un pool di
 * thread. La classe {@code ThreadContext} fornisce il supporto
 * per l&rsquo;accesso ad alcune informazioni e servizi relativi alla gestione
 * dei thread logici.<BR> 
 * Il contesto di un thread logico deve essere delimitato esplicitamente dalla
 * creazione di un oggetto {@code ThreadContext} (mediante il
 * {@linkplain #ThreadContext(String) costruttore} all&rsquo;avvio del thread) e
 * dal rilascio dello stesso oggetto (eseguendo il metodo {@link #dispose} al
 * termine del thread).</P> 
 * 
 * <BLOCKQUOTE CLASS="note">
 * Utilizzando un pool di thread, se al termine del thread logico il contesto
 * {@code ThreadContext} non viene rilasciato, quando lo stesso thread del pool
 * sar&agrave; riutilizzato, lo stesso oggetto {@code ThreadContext} potrebbe
 * essere riutilizzato come contesto del nuovo thread logico (esattamente come
 * avviene per gli oggetti {@code ThreadLocal} di
 * <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM>) e questo potrebbe
 * causare comportamenti anomali non prevedibili dell&rsquo;applicazione (il
 * costruttore di un nuovo contesto {@code ThreadContext} inoltrer&agrave;
 * un&rsquo;eccezione).
 * </BLOCKQUOTE>
 * 
 * <P>Si potrebbe dire che l&rsquo;utilizzo dei contesti {@code ThreadContext}
 * potrebbe limitare i vantaggi offerti dai pool di thread, ma in realt&agrave;
 * il principale di questi vantaggi &egrave; l&rsquo;ottimizzazione della
 * gestione di risorse critiche come i thread di sistema, e su questo non
 * influiscono i contesti {@code ThreadContext} che sono semplicemente
 * astrazioni in memoria per rappresentare i thread logici.</P>
 *
 * <H4 ID="idAutoCreate">1. Creazione automatica del contesto dei thread</H4>
 * 
 * <P>Impostando la propriet&agrave; di sistema
 * {@code it.scoppelletti.programmerpower.threading.ThreadContext.allowAutoCreate}
 * con il valore {@code true}, &egrave; possibile evitare di dover creare
 * esplicitamente un oggetto {@code ThreadContext} per ogni thread logico e
 * la creazione sar&agrave; eventualmente eseguita automaticamente solo se
 * necessario.<BR>
 * Un contesto {@code ThreadContext} creato automaticamente non pu&ograve;
 * essere rilasciato eseguendo il metodo {@code dispose}; al termine
 * di ogni thread logico dovrebbe invece essere eseguito il metodo statico
 * {@link #disposeAuto} per rilasciare l&rsquo;eventuale oggetto
 * {@code ThreadContext} istanziato automaticamente.<BR>
 * Utilizzando un pool di thread, se al termine del thread logico
 * il contesto {@code ThreadContext} non viene rilasciato, quando lo stesso
 * thread del pool sar&agrave; riutilizzato, lo stesso oggetto
 * {@code ThreadContext} potrebbe essere riutilizzato come contesto del nuovo
 * thread logico esattamente come avviene per gli oggetti {@code ThreadLocal} di
 * <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM>, e questo potrebbe
 * causare comportamenti anomali non prevedibili dell&rsquo;applicazione.<BR>
 * L&rsquo;abilitazione della creazione automatica del contesto dei thread non
 * preclude comunque la possibilit&agrave; di creare esplicitamente il contesto
 * {@code ThreadContext} per alcuni thread logici.</P>
 *
 * @since 1.0.0
 */
public final class ThreadContext implements Disposable {    
    
    /**
     * Nome della propriet&agrave; di sistema sulla quale pu&ograve; essere
     * impostato il valore {@code "true"} per abilitare la creazione automatica
     * del contesto dei thread. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.  
     * 
     * @it.scoppelletti.tag.default {@code false}
     */    
    public static final String PROP_ALLOWAUTOCREATE =
            "it.scoppelletti.programmerpower.threading.ThreadContext.allowAutoCreate";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            ThreadContext.class);    
    private final String myName;
    private final UUID myId;    
    private final boolean myAutoCreated;
    private Locale myLocale;
    private TimeZone myTimeZone;
    private TimeZone myTimeZoneDefault;
    private volatile boolean myIsDisposed = false;    
    
    /**
     * Costruttore.
     *
     * <P>Il costruttore di un oggetto {@code ThreadContext} ne registra il
     * riferimento in un oggetto {@code ThreadLocal} relativo al thread
     * corrente.</P>
     * 
     * @param  name Nome descrittivo del contesto del thread. Puo&ograve; essere
     *              {@code null}. Lo stesso nome, se impostato, &egrave;
     *              assegnato anche al thread corrente, a meno che questo sia
     *              impedito dai controlli di accesso impostati con un oggetto
     *              {@code SecurityManager}.
     * @throws it.scoppelletti.programmerpower.ObjectDuplicateException
     *         Creazione di un secondo oggetto {@code ThreadContext} durante
     *         l&rsquo;esecuzione dello stesso thread.
     */
    public ThreadContext(String name) {
        this(name, UUIDGenerator.getInstance().newUUID());
    }
    
    /**
     * Costruttore.
     *
     * <P>Il costruttore di un oggetto {@code ThreadContext} ne registra il
     * riferimento in un oggetto {@code ThreadLocal} relativo al thread
     * corrente.</P>
     * 
     * @param  name Nome descrittivo del contesto del thread. Puo&ograve; essere
     *              {@code null}. Lo stesso nome, se impostato, &egrave;
     *              assegnato anche al thread corrente, a meno che questo sia
     *              impedito dai controlli di accesso impostati con un oggetto
     *              {@code SecurityManager}.
     * @param  id   UUID del contesto del thread. &Egrave; responsabilit&agrave;
     *              del chiamante mantenere univoci gli UUID tra i contesti dei
     *              thread.
     * @throws it.scoppelletti.programmerpower.ObjectDuplicateException
     *         Creazione di un secondo oggetto {@code ThreadContext} durante
     *         l&rsquo;esecuzione dello stesso thread.
     * @since  2.0.0
     */
    public ThreadContext(String name, UUID id) {
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();
        
        if (holder.get() != null) {
            throw new ObjectDuplicateException("ThreadContext");
        }
        if (id == null) {
            throw new ArgumentNullException("ex");
        }
                
        myName = name;
        MDC.put(DiagnosticSystem.MDC_THREADNAME, myName);
        
        myId = id;
        MDC.put(DiagnosticSystem.MDC_THREADID, myId.toString());
        
        if (!StringUtils.isBlank(myName)) {
            try {
                Thread.currentThread().setName(myName);
            } catch (SecurityException ex) {
                myLogger.error("Failed to set current thread name.", ex);
            }
        }
        
        myAutoCreated = false;
        
        holder.set(this);
        myLogger.trace("Thread context initialized."); 
    }
    
    /**
     * Costruttore privato per creazione automatica.              
     */    
    private ThreadContext() {
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();
        
        myName = null;
        MDC.put(DiagnosticSystem.MDC_THREADNAME, myName);
        
        myId = UUIDGenerator.getInstance().newUUID();        
        MDC.put(DiagnosticSystem.MDC_THREADID, myId.toString());
        
        myAutoCreated = true;
        holder.set(this);
        myLogger.trace("Thread context automatically initialized.");
    }
    
    /**
     * Rilascia le risorse locali al contesto del thread.
     *
     * <P>Il metodo {@code dispose} esegue le seguenti operazioni:</P>
     * 
     * <OL>
     * <LI>Rimuove tutti gli oggetti dal contesto di diagnosi del thread
     * corrente.
     * <LI>rimuove il riferimento al contesto {@code ThreadContext}
     * dall&rsquo;oggetto {@code ThreadLocal} che lo mantiene; in questo modo
     * pu&ograve; essere istanziato un nuovo oggetto {@code ThreadContext} per
     * lo stesso thread, ad esempio, se il thread appartiene ad un pool e
     * pu&ograve; quindi essere riutilizzato per l&rsquo;esecuzione di altri
     * thread logici.
     * </OL>
     *
     * @throws it.scoppelletti.programmerpower.ObjectDisposedException
     *         Contesto gi&agrave; rilasciato.
     * @throws it.scoppelletti.programmerpower.threading.ObjectOutOfThreadContextException
     *         Contesto rilasciato non nel thread di appartenenza.
     * @throws it.scoppelletti.programmerpower.ObjectAutoCreateException
     *         Contesto del thread creato automaticamente.
     * @see it.scoppelletti.diagnostic.DiagnosticSystem#removeDiagnosticContext
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/threading/ThreadContext.html#idAutoCreate">Creazione automatica del contesto dei thread</A>         
     */
    public void dispose() {
        checkThreadContext();
        
        if (myAutoCreated) {
            throw new ObjectAutoCreateException(toString());
        }
        
        myLogger.trace("Thread context disposing.");   
        disposeImpl();
    }
        
    /**
     * Rilascia le risorse locali al contesto del thread eventualmente creato
     * automaticamente.
     * 
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/threading/ThreadContext.html#idAutoCreate">Creazione automatica del contesto dei thread</A> 
     */
    public static void disposeAuto() {
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();
        ThreadContext threadCtx = holder.get();
        
        if (threadCtx == null || threadCtx.isDisposed() ||
                !threadCtx.isAutoCreated()) {
            return;
        }
    
        myLogger.trace("Automatic thread context disposing.");
        threadCtx.disposeImpl();                
    }
    
    /**
     * Rilascia le risorse locali al contesto del thread.
     */
    private void disposeImpl() {
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();
        
        MDC.remove(DiagnosticSystem.MDC_THREADNAME);
        MDC.remove(DiagnosticSystem.MDC_THREADID);
        DiagnosticSystem.getInstance().removeDiagnosticContext();
        holder.remove();
        myIsDisposed = true;             
    }
    
    /**
     * Restituisce il contesto del thread corrente.
     *
     * @return Oggetto.
     * @throws it.scoppelletti.programmerpower.ReturnNullException
     *         Il contesto del thread corrente non &egrave; stato istanziato.
     */
    public static ThreadContext getCurrentThreadContext() {
        ThreadContext threadCtx;        
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();
        
        threadCtx = holder.getOrCreate();
        if (threadCtx == null) {
            throw new ReturnNullException(ThreadContext.class.getName(),
                    "getThreadContext");                                
        }
        
        return threadCtx;
    }

    /**
     * Restituisce il contesto del thread corrente.
     * 
     * @return Oggetto. Se il contesto per il thread corrente non &egrave; stato
     *         istanziato, restituisce {@code null}.
     */    
    public static ThreadContext tryGetCurrentThreadContext() {
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();
        
        return holder.get();
    }
    
    /**
     * Restituisce il nome descrittivo del contesto del thread.
     * 
     * @return Valore. Puo&ograve; essere {@code null}.
     */
    public String getName() {
        return myName;
    }
    
    /**
     * Restituisce l&rsquo;UUID del contesto del thread.
     * 
     * @return Valore.
     */
    public UUID getId() {
        return myId;
    }
    
    /**
     * Restituisce la localizzazione per il contesto del thread.
     * 
     * @return Oggetto. Se la localizzazione non &egrave; stata esplicitamente
     *         impostata, restituisce la localizzazione di default di JVM.
     * @see    #setLocale
     * @since  2.0.0
     */
    public Locale getLocale() {
        if (myLocale == null) {
            return Locale.getDefault();
        }
        
        return myLocale;
    }
    
    /**
     * Imposta la localizzazione per il contesto del thread.
     * 
     * @param obj Oggetto. Pu&ograve; essere {@code null} per utilizzare la
     *            localizzazione di default di JVM.
     * @see       #getLocale
     * @since     2.0.0
     */
    public void setLocale(Locale obj) {
        myLocale = obj;
        myTimeZoneDefault = null;
    }
    
    /**
     * Restituisce il fuso orario per il contesto del thread.
     * 
     * @return Oggetto. Se il fuso orario non &egrave; stato esplicitamente
     *         impostato, restituisce il fuso orario associato alla
     *         localizzazione per il contesto del thread.
     * @see #setTimeZone
     * @see #getLocale
     * @see it.scoppelletti.programmerpower.types.TimeTools#getTimeZone(Locale)
     * @since  2.1.0
     */
    public TimeZone getTimeZone() {
        if (myTimeZone == null) {
            if (myTimeZoneDefault == null) {
                myTimeZoneDefault = TimeTools.getTimeZone(getLocale());
            }
            
            return myTimeZoneDefault;
        }
        
        return myTimeZone; 
    }
    
    /**
     * Imposta il fuso orario per il contesto del thread.
     * 
     * @param obj Oggetto. Pu&ograve; essere {@code null} per utilizzare il
     *            fuso orario associato alla localizzazione per il contesto del
     *            thread.
     * @see #getTimeZone
     * @see #getLocale
     * @see it.scoppelletti.programmerpower.types.TimeTools#getTimeZone(Locale)
     * @since     2.1.0
     */
    public void setTimeZone(TimeZone obj) {
        myTimeZone = obj;
    }
    
    public boolean isDisposed() {
        return myIsDisposed;
    }   

    /**
     * Indica se il contesto del thread &egrave; stato creato automaticamente.
     * 
     * @return Indicatore.
     */
    private boolean isAutoCreated() {
        return myAutoCreated; 
    }
        
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("name", myName)
            .append("id", myId).toString();
    }
        
    /**
     * Aggiunge un oggetto al contesto di diagnosi del thread corrente.
     * 
     * @param  obj Oggetto.
     * @throws it.scoppelletti.programmerpower.threading.ObjectOutOfThreadContextException
     *         Contesto utilizzato non nel thread di appartenenza.
     * @see    #removeDiagnosticContextObject
     * @see    it.scoppelletti.diagnostic.DiagnosticSystem#addDiagnosticContextObject
     * @since  2.0.0 
     */
    public void addDiagnosticContextObject(Object obj) {
        checkThreadContext();
        
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }

        DiagnosticSystem.getInstance().addDiagnosticContextObject(obj, false);
    }
    
    /**
     * Rimuove un oggetto dal contesto di diagnosi del thread corrente.
     * 
     * @param  obj Oggetto.
     * @throws it.scoppelletti.programmerpower.threading.ObjectOutOfThreadContextException
     *         Contesto utilizzato non nel thread di appartenenza.         
     * @see    #addDiagnosticContextObject 
     * @see    it.scoppelletti.diagnostic.DiagnosticSystem#removeDiagnosticContextObject
     * @since  2.0.0
     */
    public void removeDiagnosticContextObject(Object obj) {
        checkThreadContext();
        
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }

        DiagnosticSystem.getInstance().removeDiagnosticContextObject(obj);
    }
    
    /**
     * Verifica che il contesto sia utilizzato all&rsquo;interno del thread che
     * rappresenta.
     *
     * @throws it.scoppelletti.programmerpower.ObjectDisposedException
     *         Contesto rilasciato.
     * @throws it.scoppelletti.programmerpower.threading.ObjectOutOfThreadContextException
     *         Contesto utilizzato non nel thread di appartenenza.
     */
    private void checkThreadContext() {
        ThreadContext.Holder holder = ThreadContext.Holder.getInstance();        
        
        if (myIsDisposed) {
            throw new ObjectDisposedException(toString());
        }
        
        if (this != holder.get()) {
            throw new ObjectOutOfThreadContextException(toString());
        }        
    }

    /**
     * Collegamento tra il thread corrente e il suo contesto.
     */
    private static final class Holder extends ThreadLocal<ThreadContext> {        
        private static final ThreadContext.Holder myInstance =
            new ThreadContext.Holder();
        private static final Logger myLogger = LoggerFactory.getLogger(
                ThreadContext.class);        
        private final boolean myAllowAutoCreate;    

        /**
         * Costruttore privato per oggetto singleton.
         */
        private Holder() {
            String value;
            
            value = JVMTools.getProperty(ThreadContext.PROP_ALLOWAUTOCREATE);
            myAllowAutoCreate = Boolean.parseBoolean(value);        
            if (myAllowAutoCreate) {
                myLogger.warn(
                        "Automatic thread context initialization enabled.");
            }                    
        }  

        /**
         * Restituisce il collegamento tra il thread corrente e il suo contesto.
         * 
         * @return Oggetto.
         */
        static ThreadContext.Holder getInstance() {
            return myInstance;
        }
        
        /**
         * Restituisce il contesto del thread corrente oppure ne istanzia uno
         * automaticamente (se la creazione automatica &egrave; abilitata).
         * 
         * @return Oggetto. Se la creazione automatica non &egrave; abilitata,
         *         pu&ograve; essere {@code null}.
         */
        ThreadContext getOrCreate() {
            ThreadContext ctx = get();
            
            if (ctx == null && myAllowAutoCreate) {
                return new ThreadContext();
            }
            
            return ctx;
        }
    }
}

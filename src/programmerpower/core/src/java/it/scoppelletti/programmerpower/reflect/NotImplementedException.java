/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

/**
 * Eccezione per metodo astratto o di interfaccia non implementato.
 * 
 * @since 1.0.0
 */
public class NotImplementedException extends UnsupportedOperationException {
    private static final long serialVersionUID = 1;
    
    /**
     * @serial Nome della classe astratta o interfaccia.
     */
    private final String myClassName;
    
    /** 
     * @serial Nome del metodo.
     */
    private final String myMethodName;
    
    /**
     * Costruttore.
     *
     * @param className  Nome della classe astratta o interfaccia.
     * @param methodName Nome del metodo.
     */
    public NotImplementedException(String className, String methodName) {
        myClassName = className;
        myMethodName = methodName;
    }
    
    /**
     * Restituisce il nome della classe astratta o interfaccia.
     * 
     * @return Valore.
     */
    public final String getClassName() {
        return myClassName;
    }
    
    /**
     * Restituisce il nome del metodo.
     * 
     * @return Valore.
     */    
    public final String getMethodName() {
        return myMethodName;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        ReflectionResources res = new ReflectionResources();
        
        return res.getNotImplementedException(myClassName, myMethodName);
    }    
}

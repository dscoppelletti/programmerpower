/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io.spi;

import java.io.File;

/**
 * Interfaccia del servizio per la rilevazione del direttorio dei dati
 * riservato all&rsquo;utente che esegue il programma.
 *                
 * <P>I provider alternativi del servizio {@code UserDataDirectoryProvider}
 * possono essere distribuiti secondo le specifiche
 * <ACRONYM TITLE="Service Provider Interface">SPI</ACRONYM> dei file 
 * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM>: in particolare, nella cartella
 * {@code META-INF/services} di ogni modulo JAR pu&ograve; essere inserito un
 * file di testo 
 * {@code it.scoppelletti.programmerpower.io.spi.UserDataDirectoryProvider} nel
 * quale ogni linea riporta il nome di una delle classi che implementano
 * l&rsquo;interfaccia {@code UserDataDirectoryProvider} e distribuite con il
 * modulo JAR.</P>
 *             
 * @see   it.scoppelletti.programmerpower.io.UserDataDirectory                        
 * @since 2.1.0
 */
public interface UserDataDirectoryProvider {
        
    /**
     * Restituisce il direttorio.
     * 
     * @return Oggetto. Se il direttorio non &egrave; rilevato, restituisce
     *         {@code null}.
     */
    File getDirectory();
}

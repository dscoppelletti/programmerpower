/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io.spi;

import java.io.File;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.JVMTools;

/**
 * Implementazione del provider {@code UserDataDirectoryProvider} per il sistema
 * operativo Microsoft Windows.
 * 
 * @since 2.1.0
 */
public final class WindowsUserDataDirectoryProvider implements
        UserDataDirectoryProvider {
    
    /**
     * Tag per il riconoscimento dei sistemi operativi Microsoft Windows. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * <P>Se il valore della propriet&agrave; di sistema {@code os.name} include
     * il tag (ignore-case), il sistema operativo &egrave; riconosciuto.</CODE>.
     */
    public static final String TAG_OSNAME = "Windows";
    
    /**
     * Variabile di ambiente {@code LOCALAPPDATA}: direttorio dei dati riservato
     * all&rsquo;utente che sta eseguendo il programma.
     */
    public static final String ENV_APPDATA = "LOCALAPPDATA";
    
    /**
     * Nome del direttorio dedicato a Programmer Power. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String DIR_NAME = "it-scoppelletti-programmerpower";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            WindowsUserDataDirectoryProvider.class);
    
    /**
     * Costruttore.
     */
    public WindowsUserDataDirectoryProvider() {        
    }
    
    public File getDirectory() {
        String value;
        File dir;
        
        value = JVMTools.getProperty(JVMTools.PROP_OSNAME);
        if (!StringUtils.containsIgnoreCase(value,
                WindowsUserDataDirectoryProvider.TAG_OSNAME)) {
            return null;
        }
        
        value = JVMTools.getenv(WindowsUserDataDirectoryProvider.ENV_APPDATA);
        if (StringUtils.isBlank(value)) {
            myLogger.warn("Environment variable {} not set.",
                    WindowsUserDataDirectoryProvider.ENV_APPDATA);
            return null;
        }
        
        dir = new File(value);
        dir = new File(dir, WindowsUserDataDirectoryProvider.DIR_NAME);

        return dir;
    }
}

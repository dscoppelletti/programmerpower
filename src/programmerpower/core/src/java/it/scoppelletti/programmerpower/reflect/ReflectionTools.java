/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.reflect;

import java.io.*;
import java.net.*;

/**
 * Funzioni di utilit&agrave; Reflection.
 * 
 * @since 2.1.0
 */
public final class ReflectionTools {

    /**
     * Costruttore privato per classe statica.
     */
    private ReflectionTools() {
    }

    /**
     * Rileva l&rsquo;URL di una risorsa.
     * 
     * @param  name Nome della risorsa.
     * @return      URL. Se la risorsa non &egrave; rilevata, restituisce
     *              {@code null}.
     */
    public static URL getResource(String name) {
        URLFinder finder;

        finder = new URLFinder(null, name);

        return finder.findResource();
    }

    /**
     * Apre il flusso di lettura di una risorsa.
     * 
     * @param  name Nome della risorsa.
     * @return      Flusso di lettura. Se la risorsa non &egrave; rilevata,
     *              restituisce {@code null}.
     */
    public static InputStream getResourceAsStream(String name) {
        StreamFinder finder;

        finder = new StreamFinder(null, name);

        return finder.findResource();
    }
}

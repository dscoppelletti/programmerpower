/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.types;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Data.
 * 
 * <P>A differenza delle classi {@code Calendar} e {@code Date} di
 * <ACRONYM TITLE="Java Development Kit">JDK</ACRONYM>, la classe
 * {@code SimpleDate} include le sole componenti (anno, mese, giorno) e le
 * istanze sono immutabili.</P>
 * 
 * @since 2.0.0
 */
public final class SimpleDate implements Comparable<SimpleDate>, ValueType {
    private static final long serialVersionUID = 2L;
    
    /**
     * Data nulla.
     * 
     * @see #isEmpty
     */
    public static final SimpleDate NIL = new SimpleDate();
    
    /**
     * @serial Anno.
     */
    private final int myYear;
        
    /**
     * @serial Mese.
     */
    private final int myMonth;
    
    /**
     * @serial Giorno.
     */
    private final int myDay;
    
    /**
     * Costruttore.
     */
    private SimpleDate() {
        myYear = TimeTools.NA;
        myMonth = TimeTools.NA;
        myDay = TimeTools.NA;
    }
        
    /**
     * Costruttore.
     * 
     * @param year  Anno.
     * @param month Mese.
     * @param day   Giorno.
     */
    public SimpleDate(int year, int month, int day) {
        int min, max;
        Calendar cal = Calendar.getInstance(ResourceTools.getLocale());
        
        min = cal.getMinimum(Calendar.YEAR);
        max = cal.getMaximum(Calendar.YEAR);        
        if (year < min || year > max) {
            throw new ArgumentOutOfRangeException("year", year,
                    String.format("[%1$d, %2$d]", min, max));            
        }
        cal.set(Calendar.YEAR, year);
        
        min = cal.getActualMinimum(Calendar.MONTH);
        max = cal.getActualMaximum(Calendar.MONTH);
        if (month < min || month > max) {
            throw new ArgumentOutOfRangeException("month", month,
                    String.format("[%1$d, %2$d]", min, max));            
        }
        cal.set(Calendar.MONTH, month);
        
        min = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        max = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (day < min || day > max) {
            throw new ArgumentOutOfRangeException("day", day,
                    String.format("[%1$d, %2$d]", min, max));            
        }
        
        myYear = year;
        myMonth = month;
        myDay = day;
    }

    /**
     * Costruttore.
     * 
     * @param value Calendario.
     */
    private SimpleDate(Calendar value) {
        myYear = value.get(Calendar.YEAR);
        myMonth = value.get(Calendar.MONTH);
        myDay = value.get(Calendar.DAY_OF_MONTH);
    }
    
    /**
     * Costruttore.
     * 
     * @param value Data.
     */
    @SuppressWarnings("deprecation")
    private SimpleDate(Date value) {
        myYear = 1900 + value.getYear();
        myMonth = value.getMonth();
        myDay = value.getDate();
    }
    
    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        if (myYear == TimeTools.NA && myMonth == TimeTools.NA &&
                myDay == TimeTools.NA) {
            return SimpleDate.NIL;
        }
        
        return new SimpleDate(myYear, myMonth, myDay);
    }
    
    /**
     * Restituisce la data corrente.
     * 
     * @return Valore.
     * @see    it.scoppelletti.programmerpower.types.TimeTools#getNow
     */
    public static SimpleDate getToday() {
        return new SimpleDate(TimeTools.getNow());
    }
    
    /**
     * Restituisce la data corrispondente ad un calendario.
     * 
     * @param  value Calendario.
     * @return       Data. Se il calendario {@code value} &egrave; {@code null},
     *               restituisce la data nulla.
     * @see          #NIL               
     */
    public static SimpleDate toSimpleDate(Calendar value) {
        if (value == null) {
            return SimpleDate.NIL;
        }
        
        return new SimpleDate(value);
    }
       
    /**
     * Restituisce la data corrispondente ad una data JDK.
     * 
     * @param  value Data JDK.
     * @return       Data. Se la data {@code value} &egrave; {@code null},
     *               restituisce la data nulla.
     * @see          #NIL               
     */
    public static SimpleDate toSimpleDate(Date value) {
        if (value == null) {
            return SimpleDate.NIL;
        }
        
        return new SimpleDate(value);
    }
    
    /**
     * Restituisce l&rsquo;anno.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getYear() {
        return myYear;
    }
    
    /**
     * Restituisce il mese.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA
     */
    public int getMonth() {
        return myMonth;
    }      
    
    /**
     * Restituisce il giorno.
     * 
     * @return Valore.
     * @see    #isEmpty
     * @see    it.scoppelletti.programmerpower.types.TimeTools#NA 
     */
    public int getDay() {
        return myDay;
    }
        
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * <P>Questa versione del metodo {@code toString} restituisce la data in
     * formato ISO 8601 ({@code YYYY-MM-DD}).</P>
     * 
     * @return Stringa. Se la data &egrave; nulla, restituisce la stringa vuota
     *         {@code ""}.
     * @see    #NIL
     * @see    #isEmpty
     */
    @Override
    public String toString() {
        if (isEmpty()) {
            return StringUtils.EMPTY;
        }
        
        return String.format("%1$4d-%2$2d-%3$2d", myYear, myMonth + 1, myDay);
    }   
     
    public boolean isEmpty() {
        return (myDay == TimeTools.NA);
    }
    
    /**
     * Converte la data in un calendario.
     * 
     * @return Valore. Se la data &egrave; nulla, restituisce {@code null}.
     * @see    #NIL
     * @see    #isEmpty
     */
    public Calendar toCalendar() {
        if (isEmpty()) {
            return null;
        }
        
        Calendar value = TimeTools.getNow();
        
        value.set(Calendar.YEAR, myYear);
        value.set(Calendar.MONTH, myMonth);
        value.set(Calendar.DAY_OF_MONTH, myDay);
        value.set(Calendar.HOUR_OF_DAY, 0);
        value.set(Calendar.MINUTE, 0);
        value.set(Calendar.SECOND, 0);
        value.set(Calendar.MILLISECOND, 0);
        
        return value;
    }
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        SimpleDate op;
        
        if (!(obj instanceof SimpleDate)) {
            return false;
        }
        
        op = (SimpleDate) obj;
        
        if (myYear != op.getYear() || myMonth != op.getMonth() ||
                myDay != op.getDay()) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myYear;
        value = 37 * value + myMonth;
        value = 37 * value + myDay;
        
        return value;
    }

    /**
     * Confronto con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Risultato del confronto.
     */
    public int compareTo(SimpleDate obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }        

        if (isEmpty() && obj.isEmpty()) {
            return 0;
        } else if (isEmpty()) {
            return -1;
        } else if (obj.isEmpty()) {
            return 1;
        }
        
        if (myYear < obj.getYear()) {
            return -1;
        }
        if (myYear > obj.getYear()) {
            return 1;
        }
        
        if (myMonth < obj.getMonth()) {
            return -1;
        }
        if (myMonth > obj.getMonth()) {
            return 1;
        }
        
        if (myDay < obj.getDay()) {
            return -1;
        }
        if (myDay > obj.getDay()) {
            return 1;
        }
        
        return 0;
    }               
}

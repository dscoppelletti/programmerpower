/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

/**
 * Eccezione per propriet&agrave; di un oggetto non impostata.
 *
 * @since 1.0.0
 */
public class PropertyNotSetException extends IllegalStateException {
    private static final long serialVersionUID = 1;
    
    /** 
     * @serial Identificatore dell&rsquo;oggetto.
     */
    private final String myObjectId;
    
    /**
     * @serial Nome della propriet&agrave;.
     */
    private final String myPropertyName;
    
    /**
     * Costruttore.
     *
     * @param objectId     Identificatore dell&rsquo;oggetto.
     * @param propertyName Nome della propriet&agrave;
     */
    public PropertyNotSetException(String objectId, String propertyName) {
        myObjectId = objectId;
        myPropertyName = propertyName;
    }
    
    /**
     * Restituisce l&rsquo;identificatore dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    public final String getObjectId() {
        return myObjectId;
    }
    
    /**
     * Restituisce il nome della propriet&agrave;.
     * 
     * @return Valore.
     */
    public final String getPropertyName() {
        return myPropertyName;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        CommonResources res = new CommonResources();
        
        return res.getPropertyNotSetException(myObjectId, myPropertyName);
    }    
}


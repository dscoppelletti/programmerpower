/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower;

/**
 * Eccezione per utilizzo di oggetto rilasciato.
 *
 * @see   it.scoppelletti.programmerpower.Disposable
 * @since 1.0.0
 */
public class ObjectDisposedException extends IllegalStateException {
    private static final long serialVersionUID = 1;
    
    /** 
     * @serial Identificatore dell&rsquo;oggetto.
     */
    private final String myObjectId;
    
    /**
     * Costruttore.
     *
     * @param objectId Identificatore dell&rsquo;oggetto.
     */
    public ObjectDisposedException(String objectId) {
        myObjectId = objectId;
    }
    
    /**
     * Restituisce l&rsquo;identificatore dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    public final String getObjectId() {
        return myObjectId;
    }
    
    /**
     * Restituisce il messaggio.
     *
     * @return Messaggio.
     */
    @Override
    public String getMessage() {
        CommonResources res = new CommonResources();
        
        return res.getObjectDisposedException(myObjectId);
    }    
}

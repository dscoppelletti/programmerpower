/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.io;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.io.spi.UserDataDirectoryProvider;
import it.scoppelletti.programmerpower.reflect.ServiceLoaderWrapper;

/**
 * Direttorio dei dati riservato all&rsquo;utente che esegue il programma.
 * 
 * <P>Le applicazioni Programmer Power possono accedere (in lettura o scrittura)
 * ad alcuni dati da un direttorio <DFN>riservato</DFN> all&rsquo;utente che
 * esegue il programma, ovvero sul quale solo quel particolare utente ha i
 * permessi sia in lettura che in scrittura.</P>
 * 
 * <P>Programmer Power utilizza un modello
 * <ACRONYM TITLE="Service Provider Interface">SPI</ACRONYM> per rilevare il
 * direttorio dei dati riservato all&rsquo;utente che esegue il programma a
 * seconda della piattaforma sulla quale il programma &egrave; in esecuzione.
 * Ad esempio, per l&rsquo;utente {@code joe}, il direttorio dei dati riservato
 * potrebbe essere uno dei seguenti:</P>
 * 
 * <DL>
 * <DT>Piattaforma UNIX/Linux:</DT>
 * <DD>{@code /home/joe/.it-scoppelletti-programmerpower}</DD>
 * <DT>Piattaforma Microsoft Windows:</DT>
 * <DD>{@code C:\Users\Joe\AppData\Local\it-scoppelletti-programmerpower}</DD>
 * <DT>Piattaforma <ACRONYM TITLE="Macintosh">Mac</ACRONYM>
 * <ACRONYM TITLE="Operative System">OS</ACRONYM>:</DT>
 * <DD>{@code /Users/joe/Library/it-scoppelletti-programmerpower}</DD>
 * </DL>
 * 
 * @see   it.scoppelletti.programmerpower.io.spi.UserDataDirectoryProvider
 * @since 2.1.0
 */
public final class UserDataDirectory extends
    ServiceLoaderWrapper<UserDataDirectoryProvider, File> {
    private static final Logger myLogger = LoggerFactory.getLogger(
            UserDataDirectory.class);    
    private final boolean myReadOnly;
    
    /**
     * Costruttore.
     * 
     * @param readOnly Indica se il direttorio &egrave; utilizzato per la sola
     *                 lettura.
     */
    private UserDataDirectory(boolean readOnly) {
        super(UserDataDirectoryProvider.class);
        
        myReadOnly = readOnly;
    }
        
    /**
     * Restituisce il direttorio.
     * 
     * @param  readOnly Indica se il direttorio &egrave; utilizzato per la sola
     *                  lettura. 
     * @return          Oggetto.
     * @throws          it.scoppelletti.programmerpower.io.FileNotExistException
     *                  Il direttorio non esiste. 
     * @see             #tryGet                
     */
    public static File get(boolean readOnly) {
        File dir;
        UserDataDirectory service;
        
        service = new UserDataDirectory(readOnly);
        dir = service.query();
        if (dir == null) {
            throw new FileNotExistException(UserDataDirectory.class.getName());
        }        
        
        return dir;                               
    }  
    
    /**
     * Restituisce il direttorio.
     * 
     * @param  readOnly Indica se il direttorio &egrave; utilizzato per la sola
     *                  lettura. 
     * @return          Oggetto. Se il direttorio non esiste, restituisce 
     *                  {@code null}.
     * @see             #get
     */
    public static File tryGet(boolean readOnly) {
        File dir;
        UserDataDirectory service;
        
        service = new UserDataDirectory(readOnly);
        dir = service.query();
        if (dir == null) {
            myLogger.warn("User data directory not found.");
            return null;
        }
        
        return dir;
    }
    
    protected File query(UserDataDirectoryProvider provider) {
        File dir;
        
        dir = provider.getDirectory();
        if (dir == null) {
            return null;
        }
        
        if (myReadOnly) {
            if (!checkRead(dir)) {
                return null;
            }
        } else {
            if (!checkWrite(dir)) {
                return null;
            }            
        }
        
        return dir;
    }     
    
    /**
     * Verifica la validit&agrave; di un direttorio per la lettura.
     * 
     * @param  dir Direttorio.
     * @return     Esito della verifica.
     */
    private boolean checkRead(File dir) {
        try {            
            if (!dir.isDirectory()) {
                myLogger.warn("Directory {} not found.", dir);
                return false;
            }
            
            if (!dir.canRead()) {
                myLogger.warn("Directory {} not readable.", dir);
                return false;            
            }            
        } catch (SecurityException ex) {
            myLogger.warn(String.format("Failed to access directory %1$s.",
                    dir), ex);
            return false;
        }        
        
        return true;
    }
    
    /**
     * Verifica la validit&agrave; di un direttorio per la scrittura.
     * 
     * @param  dir Direttorio.
     * @return     Esito della verifica.
     */    
    private boolean checkWrite(File dir) {
        try {            
            if (!dir.isDirectory()) {
                dir.mkdirs();
                if (!dir.isDirectory()) {
                    myLogger.error("Failed to create directory {}.", dir);
                    return false;
                }
                
                myLogger.info("Directory {} created.", dir);
            }
            
            if (!dir.canWrite()) {
                myLogger.warn("Directory {} not writable.", dir);
                return false;            
            }            
        } catch (SecurityException ex) {
            myLogger.warn(String.format("Failed to access directory %1$s.",
                    dir), ex);
            return false;
        }        
        
        return true;
    }    
}

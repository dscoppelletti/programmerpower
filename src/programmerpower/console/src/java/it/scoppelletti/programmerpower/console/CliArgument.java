/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import it.scoppelletti.programmerpower.*;
        
/**
 * Argomento sulla linea di comando.
 */
final class CliArgument {
    private final String myValue;
    private CliOption myOption = null;
    private String myOptionValue = null;
    
    /**
     * Costruttore.
     *
     * @param value Valore dell&rsquo;argomento.
     */
    CliArgument(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        // Sulla linea di comando posso inserire dei blanks all'inizio o al 
        // fondo degli argomenti delimitandoli con le virgolette:
        // In generale non eseguo il trim.
        myValue = value;        
    }
    
    /**
     * Restituisce il valore dell&rsquo;argomento.
     *
     * @return Valore.
     */
    String getValue() {
        return myValue;
    }
    
    /**
     * Restituisce l&rsquo;opzione corrispondente all&rsquo;argomento.
     *
     * @return Opzione.
     */
    CliOption getOption() {
        return myOption;
    }
    
    /**
     * Imposta l&rsquo;opzione corrispondente all&rsquo;argomento.
     *
     * @param option Opzione.
     */
    void setOption(CliOption option) {
        myOption = option;
    }

    /**
     * Restituisce il valore di opzione corrispondente all&rsquo;argomento.
     *
     * @return Valore.
     */
    String getOptionValue() {
        return myOptionValue;
    }
    
    /**
     * Imposta il valore di opzione corrispondente all&rsquo;argomento.
     *
     * @param value Valore.
     */
    void setOptionValue(String value) {
        myOptionValue = value;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        return myValue;
    }    
    
    /**
     * Controlli di validazione.
     *
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    void check() {
        ConsoleResources res = new ConsoleResources();
        
        if (myOption == null && myOptionValue == null) {
            throw new CommandLineArgumentException(
                    res.getArgumentInvalidException(myValue));
        }
    }        
}

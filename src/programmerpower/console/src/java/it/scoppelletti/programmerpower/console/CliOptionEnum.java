/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.resources.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Opzione sulla linea di comando che prevede un valore di un tipo enumerato.
 *
 * @param <T> Tipo di enumerato.
 * @see       it.scoppelletti.programmerpower.console.CliOptionSet
 * @since     2.1.0
 */
public class CliOptionEnum<T extends Enum<T>> extends CliOption {
    private Class<T> myEnumType;
    private T myValue;
    private T myDefaultValue;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     * @param enumType         Tipo di enumerato.
     * @param defaultValue     Valore di default.
     */
    public CliOptionEnum(String name, String resourceBaseName,
            Class<T> enumType, T defaultValue) {
        super(name, false, resourceBaseName, "mode");
        
        if (enumType == null) {
            throw new ArgumentNullException("enumType");
        }
        if (defaultValue == null) {
            throw new ArgumentNullException("defaultValue");
        }
        
        myEnumType = enumType;
        myDefaultValue = defaultValue;
        myValue = myDefaultValue;
    }    
    
    /**
     * Restituisce il valore.
     *
     * @return Valore.
     * @see    #setValue
     */
    public final T getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     *
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@link #getDefaultValue}
     * @see                               #getValue
     */
    public final void setValue(T value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myValue = value;
    }    
    
    /**
     * Valore di default.
     * 
     * @return Valore.
     */
    public final T getDefaultValue() {
        return myDefaultValue;
    }
    
    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     */    
    @Override
    public String getStringValue() {
        return myValue.name();
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myValue.name());
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        try {
            myValue = EnumTools.valueOf(myEnumType, value);
        } catch (Exception ex) {
            throw new CommandLineArgumentException(ex);
        }
    }
    
    @Override
    protected final void removeValues() {
        myValue = myDefaultValue;
    }
    
    @Override
    public void writeUsage(Writer writer) {
        String label, sep;
        PrintWriter printer = null;
        Locale locale = ResourceTools.getLocale();
        
        super.writeUsage(writer);
        
        try {
            printer = new PrintWriter(writer);
            
            sep = "[";
            for (T value : myEnumType.getEnumConstants()) {
                label = EnumTools.getLabel(value, locale);
                printer.print(sep);
                printer.print(value.name());
                printer.print(" : \"");
                printer.print(label);
                printer.print("\"");
                sep = ", ";
            }            
            printer.println("]");
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }                
    }
}

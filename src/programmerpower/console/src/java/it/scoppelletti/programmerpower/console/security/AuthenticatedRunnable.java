/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.security;

import org.slf4j.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.security.login.*;
import it.scoppelletti.programmerpower.threading.*;

/**
 * Componente per l&rsquo;esecuzione di un&rsquo;operazione da eseguire
 * all&rsquo;interno di una sessione autenticata via <ACRONYM
 * TITLE="Java Authentication and Authorization Service">JAAS</ACRONYM>.
 * 
 * @since 2.1.0
 */
@Final
public class AuthenticatedRunnable implements Runnable, InitializingBean,
        ApplicationEventPublisherAware {
    
    /**
     * Nome del bean di default per la configurazione del contesto di login. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #setAppConfiguration
     */
    public static final String BEAN_APPCONFIGURATION =
            "it-scoppelletti-programmerpower-console-security-appConfiguration";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            AuthenticatedRunnable.class);
    private Runnable myOperation;
    private AppConfiguration myAppCfg;
    private ApplicationEventPublisher myEventPublisher;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    @javax.annotation.Resource(name = SecurityTools.BEAN_AUTHENTICATIONMANAGER)
    private AuthenticationManager myAuthMgr;
           
    /**
     * Costruttore.
     */
    public AuthenticatedRunnable() {        
    }
    
    /**
     * Inizializzazione.
     */
    @Reserved
    public void afterPropertiesSet() throws Exception {
        if (myAppCfg == null) {
            myAppCfg = myApplCtx.getBean(
                    AuthenticatedRunnable.BEAN_APPCONFIGURATION,
                    AppConfiguration.class);
        }
    }
    
    /**
     * Imposta l&rsquo;operazione da eseguire.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setOperation(Runnable obj) {
        myOperation = obj;
    }
    
    /**
     * Imposta la configurazione del contesto di login.
     * 
     * @param                       obj Oggetto.
     * @it.scoppelletti.tag.default     Bean
     * {@code it-scoppelletti-programmerpower-console-security-appConfiguration}.
     */
    public void setAppConfiguration(AppConfiguration obj) {
        myAppCfg = obj;
    }
    
    /**
     * Imposta il pubblicatore degli eventi.
     * 
     * @param obj Oggetto 
     */
    @Reserved
    public void setApplicationEventPublisher(ApplicationEventPublisher obj) {
        myEventPublisher = obj;
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {
        Authentication auth;
        
        if (myOperation == null) {
            throw new PropertyNotSetException(toString(), "operation");
        }
        if (myAppCfg == null) {
            throw new PropertyNotSetException(toString(), "appConfiguration");
        }
        
        auth = new JaasAuthenticationRequest(myAppCfg);
        auth = myAuthMgr.authenticate(auth);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        try {
            myOperation.run();
        } finally {
            shutdown();
        }
    }
    
    /**
     * Termine dell&rsquo;operazione.
     */
    private void shutdown() {
        String sessionId;
        SecurityContext securityCtx;
        ThreadContext threadCtx = ThreadContext.tryGetCurrentThreadContext();
        
        securityCtx = SecurityContextHolder.getContext();
        
        if (myEventPublisher != null) {
            if (threadCtx != null) {
                sessionId = threadCtx.getId().toString();
            } else {
                sessionId = String.valueOf(Thread.currentThread().getId());
            }
            
            myEventPublisher.publishEvent(new ConsoleSessionDestroyedEvent(
                    this, sessionId, securityCtx));            
        } else {
            myLogger.warn("Property applicationEventPublisher not set.");
        }
                
        securityCtx.setAuthentication(null);
        SecurityContextHolder.clearContext();
    }
}

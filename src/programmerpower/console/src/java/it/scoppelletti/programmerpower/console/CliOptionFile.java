/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Opzione sulla linea di comando che prevede come valore il nome di un file.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 1.0.0
 */
public class CliOptionFile extends CliOption {
    private File myFile;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionFile(String name, String resourceBaseName) {
        super(name, false, resourceBaseName, "path");
    }
    
    /**
     * Restituisce il file.
     *
     * @return File. Pu&ograve; essere {@code null}.
     * @see    #setFile
     */
    public final File getFile() {
        return myFile;
    }
    
    /**
     * Imposta il file.
     *
     * @param                       file File.
     * @see                              #getFile
     */
    public final void setFile(File file) {
        if (file == null) {
            throw new ArgumentNullException("file");
        }
        
        myFile = file;
    }

    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore. Pu&ograve; essere {@code null}.
     * @since  2.0.0
     */    
    @Override
    public String getStringValue() {
        if (myFile == null) {
            return null;
        }
        
        return myFile.toString();
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myFile);
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        myFile = new File(value);
    }
    
    @Override
    protected final void removeValues() {
        myFile = null;
    }  
}

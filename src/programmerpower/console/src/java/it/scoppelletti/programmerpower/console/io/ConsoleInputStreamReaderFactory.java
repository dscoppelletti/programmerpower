/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.io;

import java.io.*;
import java.nio.charset.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Classe di factory di un flusso di lettura da file di testo.
 * 
 * @since 2.1.0
 */
@Final
public class ConsoleInputStreamReaderFactory implements
        InputStreamReaderFactory {
    private File myFile;
    private Charset myCharset;
    private DefaultInputStream myDefaultInputStream;
    
    /**
     * Costruttore.
     */
    public ConsoleInputStreamReaderFactory() {        
        myDefaultInputStream = DefaultInputStream.NONE;
    }
    
    /**
     * Imposta il file.
     * 
     * @param                        file File.
     * @it.scoppelletti.tag.required      Se non &egrave; previsto un flusso di
     *                                    default. 
     * @see                               #setDefaultInputStream
     */
    public void setFile(File file) {
        myFile = file;
    }
    
    /**
     * Imposta la codifica dei caratteri.
     * 
     * <P>La propriet&agrave; {@code charset} &egrave; presa in considerazione
     * solo se il flusso non &egrave; {@code stdin}.</P>
     * 
     * @param                       obj Oggetto.
     * @it.scoppelletti.tag.default     Charset di default di JVM.
     * @see                             #setFile
     * @see                             #setDefaultInputStream
     */
    public void setCharset(Charset obj) {
        myCharset = obj;
    }
    
    /**
     * Imposta il flusso di default nel caso il file non sia specificato.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code NONE}
     * @see                               #setFile
     */
    public void setDefaultInputStream(DefaultInputStream value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myDefaultInputStream = value;
    }
        
    public BufferedReader open() throws IOException {
        boolean closeable;
        InputStream in = null;
        Reader fileReader = null;
        BufferedReader reader = null;
                           
        try {
            if (myFile == null) {
                closeable = false;
                switch (myDefaultInputStream) {
                case STDIN:
                    in = System.in;
                    break;                
                    
                default: // NONE
                    throw new PropertyNotSetException(toString(), "file");
                }
                
                
                fileReader = new InputStreamReader(in);
            } else {
                closeable = true;
                in = new FileInputStream(myFile);
                if (myCharset == null) {
                    fileReader = new InputStreamReader(in);
                } else {
                    fileReader = new InputStreamReader(in, myCharset);
                }            
            }
            in = null;
            
            reader = new ConsoleInputStreamReader(fileReader, closeable);
            fileReader = null;            
        } finally {
            in = IOTools.close(in);
            fileReader = IOTools.close(fileReader);
        }
        
        return reader;
    }
}

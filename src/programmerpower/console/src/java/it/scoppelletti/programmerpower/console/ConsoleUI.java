/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.diagnostic.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * <ACRONYM TITLE="User Interface">UI</ACRONYM> di un&rsquo;applicazione su
 * console.
 * 
 * <P>I sovraccarichi del metodo {@code display} che non richiedono
 * esplicitamente il flusso di scrittura scrivono sul flusso {@code stderr}.</P>
 *  
 * @it.scoppelletti.tag.threadsafe
 * @since 1.0.0
 */
@Final
public class ConsoleUI extends UserInterface {

    /**
     * Icona metagrafica corrispondente al tipo di messaggio
     * {@code MessageType.ERROR}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>. 
     */
    public static final String ICON_ERROR = "[X]";
        
    /**
     * Icona metagrafica corrispondente al tipo di messaggio
     * {@code MessageType.WARNING}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>. 
     */
    public static final String ICON_WARNING = "[!]";
    
    /**
     * Icona metagrafica corrispondente al tipo di messaggio
     * {@code MessageType.INFORMATION}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>. 
     */
    public static final String ICON_INFORMATION = "[i]";

    /**
     * Costruttore.
     *
     * <P>Questo sovraccarico del costruttore imposta come classe che crea gli
     * eventi di log la stessa classe {@code ConsoleUI}.</P>
     * 
     * @see #ConsoleUI(String)
     */    
    public ConsoleUI() {
        super(ConsoleUI.class.getName());
    }
    
    /**
     * Costruttore.
     *
     * @param callerClassName Nome della classe che crea gli eventi di log.
     * @see <A HREF="{@docRoot}/it/scoppelletti/programmerpower/ui/UserInterface.html#idSourceLocation">Locazione
     * del codice sorgente degli eventi di log</A>   
     */
    public ConsoleUI(String callerClassName) {
        super(callerClassName);
    }     
    
    /**
     * Scrive un messaggio.
     *
     * @param writer      Flusso di scrittura.
     * @param messageType Tipo di messaggio.
     * @param message     Messaggio. Pu&ograve; essere {@code null}.
     */
    public void display(Writer writer, MessageType messageType,
            String message) {
        MessageEvent event;
        
        event = new MessageEvent(messageType, message, null);
        logMessage(event);
        display(writer, event);
    }
          
    /**
     * Scrive un&rsquo;eccezione.
     * 
     * @param writer      Flusso di scrittura.
     * @param messageType Tipo di messaggio. Normalmente le eccezioni
     *                    rappresentano degli errori
     *                    ({@code MessageType.ERROR}); un&rsquo;eccezione 
     *                    pu&ograve; tuttavia anche essere esposta sulla UI
     *                    abbassando il livello di gravit&agrave; ad esempio ad
     *                    avviso ({@code MessageType.WARNING}).
     * @param ex          Eccezione. Pu&ograve; essere {@code null}.
     */    
    public void display(Writer writer, MessageType messageType, Throwable ex) {
        MessageEvent event;
        
        event = new MessageEvent(messageType, null, ex);  
        logMessage(event);
        display(writer, event);        
    } 
    
    /**
     * Espone un messaggio.
     * 
     * @param event Evento.
     * 
     * <P>Questa implementazione del metodo {@code display} espone il messaggio
     * sul flusso {@code stderr}.</P>
     */    
    @Override
    protected void display(MessageEvent event) {
        Writer writer = null;
        
        try {
            writer = new ConsoleWriter(new OutputStreamWriter(System.err));
            display(writer, event);
        } finally {
            if (writer != null) {
                IOTools.flush(writer);
                writer = null;
            }
        }        
    }
    
    /**
     * Scrive un messaggio.
     * 
     * @param writer Flusso di scrittura.
     * @param event  Evento.
     */
    private void display(Writer writer, MessageEvent event) {
        String icon;
        String msg = null;
        String exMsg = null;
        String lastMsg = "";
        Object obj;
        Iterator<Object> it;
        PrintWriter printer = null;
        Throwable ex;        
        ConsoleResources res = new ConsoleResources();       
                
        switch (event.getMessageType()) {
        case ERROR:
            icon = ConsoleUI.ICON_ERROR;
            break;            
            
        case WARNING:
            icon = ConsoleUI.ICON_WARNING;
            break; 
            
        case INFORMATION:
            icon = ConsoleUI.ICON_INFORMATION;
            break;
            
        default:
            throw new EnumConstantNotPresentException(MessageType.class,
                    event.getMessageType().toString());
        }

        try {
            printer = new PrintWriter(writer);
            printer.println("<message>");        
            printer.print(icon);
        
            if (!StringUtils.isBlank(event.getMessage())) {
                msg = event.getMessage();
            }            
            if (event.getThrowable() != null) {
                if (msg == null) {
                    msg = ApplicationException.toString(event.getThrowable());
                } else {
                    exMsg = ApplicationException.toString(event.getThrowable());
                }
            }
        
            if (msg == null && exMsg == null) {
                printer.println();
            } else {
                printer.print(' ');
            }
            if (msg != null) {
                printer.println(msg);
                lastMsg = msg;
                if (exMsg != null) {
                    printer.print(res.getExceptionLabel());
                    printer.print(": ");
                }
            }
            if (exMsg != null) {
                printer.println(exMsg);
                lastMsg = exMsg;
            }
            
            // Scrivo anche la catena delle eccezioni annidate avendo pero'
            // cura di non scrivere piu' volte consecutivamente lo stesso
            // messaggio nel caso un'eccezione sia originata da un'altra
            // eccezione solo per cambiarne la "natura" (es. per farla
            // diventare un'eccezione RuntimeException).
            if (event.getThrowable() != null) {
                ex = event.getThrowable().getCause();
                while (ex != null) {
                    exMsg = ApplicationException.toString(ex);
                    if (!lastMsg.equals(exMsg)) {
                        printer.println(exMsg);
                        lastMsg = exMsg;
                    }
                    ex = ex.getCause();
                }                
            }

            it = DiagnosticSystem.getInstance().getDiagnosticContext();
            if (it.hasNext()) {
                printer.print("- ");
                printer.println(res.getDiagnosticContextLabel());                
            }
            while (it.hasNext()) {
                obj = it.next();
                printer.println(String.valueOf(obj));
            }
                   
            printer.println("</message>");
        } finally {
            if (printer != null) {                
                printer.flush();
                printer = null;
            }
        }       
    }       
}

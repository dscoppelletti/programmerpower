/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.security;

import java.util.*;
import org.apache.commons.lang3.*;
import org.springframework.security.core.context.*;
import org.springframework.security.core.session.*;
import it.scoppelletti.programmerpower.*;

/**
 * Termine di una sessione autenticata.
 */
final class ConsoleSessionDestroyedEvent extends SessionDestroyedEvent {
    private static final long serialVersionUID = 1L;
    
    /**
     * @serial Id&#46; della sessione.
     */
    private final String mySessionId;
    
    /**
     * @serial Contesti di sicurezza associati alla sessione.
     */
    private final List<SecurityContext> mySecurityCtx;
    
    /**
     * Costruttore.
     * 
     * @param source      Sorgente dell&rsquo;evento.
     * @param sessionId   Id&#46; della sessione.
     * @param securityCtx Contesto di sicurezza.
     */
    ConsoleSessionDestroyedEvent(Object source, String sessionId,
            SecurityContext securityCtx) {
        super(source);
        
        if (StringUtils.isBlank(sessionId)) {
            throw new ArgumentNullException("sessionId");
        }
        if (securityCtx == null) {
            throw new ArgumentNullException("securityCtx");
        }
        
        mySessionId = sessionId;
        mySecurityCtx = Collections.singletonList(securityCtx);
    }
    
    /**
     * Restituisce l&rsquo;id&#46; della sessione terminata.
     */
    public String getId() {
        return mySessionId;
    }
    
    /**
     * Restituisce i contesti di sicurezza associati alla sessione terminata.
     * 
     * @return Collezione.
     */
    public List<SecurityContext> getSecurityContexts() {
        return mySecurityCtx;
    }
}

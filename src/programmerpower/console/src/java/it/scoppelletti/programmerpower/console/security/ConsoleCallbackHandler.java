/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.security;

import java.io.*;
import java.util.*;
import javax.security.auth.callback.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.spring.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Gestore della comunicazione via callback tra i moduli di login e 
 * un&rsquo;applicazione con <ACRONYM TITLE="User Interface">UI</ACRONYM> su
 * console.
 * 
 * <P>Questa implementazione dell&rsquo;interfaccia {@code CallbackHandler}
 * gestisce i seguenti tipi di <DFN>richiesta di comunicazione</DFN>:</P>
 * 
 * <OL>
 * <LI>{@code TextOutputCallback}
 * <LI>{@code NameCallback}
 * <LI>{@code PasswordCallback}
 * </OL>
 * 
 * <P>Una classe derivata pu&ograve; integrare la gestione di altri tipi di
 * richiesta di comunicazione (o sostituire la gestione per i tipi di richiesta
 * gi&agrave; supportati) implementando opportunamente una versione prevalente
 * del metodo {@link #handle(Callback)}.</P>
 * 
 * @see   it.scoppelletti.programmerpower.console.security.CliOptionUserName
 * @see   it.scoppelletti.programmerpower.console.security.CliOptionPasswordFile
 * @see   it.scoppelletti.programmerpower.console.ConsoleUI
 * @since 2.1.0
 */
public class ConsoleCallbackHandler implements CallbackHandler {
    private static final Logger myLogger = LoggerFactory.getLogger(
            ConsoleCallbackHandler.class);
    
    @javax.annotation.Resource(name = ConsoleApplicationRunner.BEAN_NAME)
    private CliOptionSet myCli;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
    
    /**
     * Costruttore.
     */
    public ConsoleCallbackHandler() {        
    }
    
    /**
     * Gestore della comunicazione.
     * 
     * @param callbacks Richieste di comunicazione.
     */
    @Final
    public void handle(Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {
        if (callbacks == null) {
            return;
        }
        
        for (Callback callback : callbacks) {
            handle(callback);
        }
    }

    /**
     * Gestore della comunicazione.
     * 
     * @param callback Richiesta di comunicazione.
     */    
    protected void handle(Callback callback) throws IOException,
        UnsupportedCallbackException {
        if (callback instanceof TextOutputCallback) {
            doHandle((TextOutputCallback) callback);
        } else if (callback instanceof NameCallback) {
            doHandle((NameCallback) callback);
        } else if (callback instanceof PasswordCallback) {
            doHandle((PasswordCallback) callback);
        } else {
            throw new UnsupportedCallbackException(callback);
        }
    }
    
    /**
     * Espone un messaggio.
     * 
     * @param cb Richiesta di comunicazione.
     */
    private void doHandle(TextOutputCallback cb) {
        MessageType msgType;
        
        switch (cb.getMessageType()) {
        case TextOutputCallback.ERROR:
            msgType = MessageType.ERROR;
            break;
            
        case TextOutputCallback.WARNING:
            msgType = MessageType.WARNING;
            break;
            
        case TextOutputCallback.INFORMATION:
            msgType = MessageType.INFORMATION;
            break;            
            
        default:
            throw new EnumConstantNotPresentException(MessageType.class,
                    String.valueOf(cb.getMessageType()));
        }
        
        myUI.display(msgType, cb.getMessage());
    }
    
    /**
     * Richiede il nome di un utente.
     * 
     * @param cb Richiesta di comunicazione.
     */
    private void doHandle(NameCallback cb) throws IOException {
        String value, defName;
        CliOption opt;
        CliOptionUserName userOpt;
        InputStreamReader in;
        BufferedReader reader;
        
        opt = myCli.getOption(CliOptionUserName.NAME);
        if (opt != null && opt.isSpecified() &&
                (opt instanceof CliOptionUserName)) {
            userOpt = (CliOptionUserName) opt;
            value = userOpt.getValue();
            if (!StringUtils.isBlank(value)) {
                cb.setName(value);
                return;
            }
        }
                        
        in = new InputStreamReader(System.in);
        reader = new BufferedReader(in);
        
        while (true) {
            value = cb.getPrompt();
            if (!StringUtils.isBlank(value)) {
                System.out.print(value);
            }
            
            defName = cb.getDefaultName();
            if (!StringUtils.isBlank(defName)) {
                System.out.print(" [");
                System.out.print(defName);
                System.out.print("]");
            }
            
            System.out.print(": ");
            System.out.flush();
            
            value = reader.readLine();
            value = StringUtils.strip(value);
            if (!StringUtils.isBlank(value)) {
                break;
            }
        
            if (!StringUtils.isBlank(defName)) {
                value = defName;
                break;
            }
        }
        
        cb.setName(value);
    }
    
    /**
     * Richiede una password.
     * 
     * @param cb Richiesta di comunicazione.
     */    
    private void doHandle(PasswordCallback cb) throws IOException {
        char[] v;
        String value;
        SecureString s = null;
        CliOption opt;
        CliOptionPasswordFile pwdOpt;
        
        try {
            opt = myCli.getOption(CliOptionPasswordFile.NAME);
            if (opt != null && opt.isSpecified() &&
                    (opt instanceof CliOptionPasswordFile)) {
                pwdOpt = (CliOptionPasswordFile) opt;
                s = pwdOpt.getPassword();
                if (!ValueTools.isNullOrEmpty(s)) {
                    cb.setPassword(s.getValue());
                    return;
                }
            }           
        } finally {
            if (s != null) {
                s.clear();
                s = null;
            }
        }
        
        while (true) {
            value = cb.getPrompt();
            if (!StringUtils.isBlank(value)) {
                System.out.print(value);
            }
            
            System.out.print(": ");
            System.out.flush();
            
            v = readPassword();
            if (!ArrayUtils.isEmpty(v)) {
                break;
            }
        }
        
        cb.setPassword(v);
        Arrays.fill(v, (char) 0);
    }       
    
    /**
     * Legge una password attraverso la console, oppure, se la console non 
     * &egrave; disponibile, dal flusso {@code stdin}.
     * 
     * @return Vettore di caratteri. Pu&ograve; essere {@code null}.
     */
    private char[] readPassword() {
        int c, i, n;
        char[] v;
        StringBuilder buf;
        InputStreamReader in;
        Console console = System.console();
        
        if (console != null) {
            return console.readPassword();
        }

        myLogger.warn("Console is not available: reading password from STDIN.");                
        buf = new StringBuilder();
        
        try {
            in = new InputStreamReader(System.in);
            c = in.read();
            while (c >= 0 && c != '\n' && c != '\r') {
                buf.append((char) c);
                c = in.read();
            }                        
        } catch (IOException ex) {
            myLogger.error("Failed to read password.", ex);
        }
                
        n = buf.length();
        if (n == 0) {
            return null;
        }
        
        v = new char[n];
        for (i = 0; i < n; i++) {
            v[i] = buf.charAt(i);
            buf.setCharAt(i, (char) 0);
        }
        
        return v;
    }
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Opzione sulla linea di comando.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet 
 * @since 1.0.0
 */
public abstract class CliOption {
    
    /**
     * Prefisso delle opzioni. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String PREFIX = "-";
    
    private final String myName;
    private final boolean myIsMultiple;
    private final String myResourceBaseName;
    private final String myDefaultValueName;
    private CliOption.SpecifiedCheck mySpecifiedCheck = 
        CliOption.SpecifiedCheck.NONE;
    private boolean myIsSpecified = false;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param multiple         Indica se l&rsquo;opzione pu&ograve; essere
     *                         inserita pi&ugrave; di una volta sulla linea di
     *                         comando.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     * @param defaultValueName Nome simbolico del valore dell&rsquo;opzione
     *                         assunto per default nel caso non sia rilevabile
     *                         dalle risorse per la localizzazione.                         
     */
    protected CliOption(String name, boolean multiple, String resourceBaseName,
            String defaultValueName) {
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name");
        }
        if (StringUtils.isBlank(resourceBaseName)) {
            throw new ArgumentNullException("resourceBaseName");
        }
        
        myName = name;
        myIsMultiple = multiple;
        myResourceBaseName = resourceBaseName;
        myDefaultValueName = defaultValueName;
    }    
    
    /**
     * Restituisce il nome dell&rsquo;opzione.
     *
     * @return Valore.
     */
    public final String getName() {
        return myName;
    }
    
    /**
     * Indica se l&rsquo;opzione pu&ograve; essere inserita pi&ugrave; di
     * una volta sulla linea di comando.
     *
     * @return Indicatore.
     */
    public final boolean isMultiple() {
        return myIsMultiple;
    }
    
    /**
     * Restituisce la modalit&agrave; di controllo sul fatto che
     * un&rsquo;opzione sia inserita o meno sulla linea di comando.
     *
     * @return Valore.
     * @see    #setSpecifiedCheck
     */
    public final CliOption.SpecifiedCheck getSpecifiedCheck() {
        return mySpecifiedCheck;
    }
    
    /**
     * Imposta la modalit&agrave; di controllo sul fatto che un&rsquo;opzione
     * sia inserita o meno sulla linea di comando.
     *
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code CliOption.SpecifiedCheck.NONE}
     * @see                               #getSpecifiedCheck
     */
    public final void setSpecifiedCheck(CliOption.SpecifiedCheck value) {
        if (value == null) {
            throw new ArgumentNullException("value");           
        }
        
        mySpecifiedCheck = value;
    }
    
    /**
     * Indica se l&rsquo;opzione &egrave; stata inserita sulla linea di comando.
     *
     * @return Indicatore.
     * @see    #setSpecified
     */
    public final boolean isSpecified() {
        return myIsSpecified;
    }
        
    /**
     * Imposta l&rsquo;indicatore di opzione inserita sulla linea di comando.
     *
     * @param value Valore.
     * @see         #isSpecified
     */
    public final void setSpecified(boolean value) {
        myIsSpecified = value;
    }
    
    /**
     * Restituisce la descrizione.
     *
     * <P>La chiave della risorsa della descrizione &egrave; composta
     * aggiungendo al nome dell&rsquo;opzione il suffisso
     * {@code Description}; se la risorsa non viene rilevata, si assume come
     * descrizione la stringa vuota {@code ""}.</P>
     *
     * @return Valore.
     * @see    #getName
     */
    public final String getDescription() {
        String key = myName.concat("Description");
        String desc;
        ResourceBundle res;
                        
        try {
            res = ResourceBundle.getBundle(myResourceBaseName);
            desc = res.getString(key);
        } catch (MissingResourceException ex) {
            desc = "";
        }
         
        return desc;
    }
    
    /**
     * Restituisce il nome simbolico del valore dell&rsquo;opzione.
     *
     * <P>La chiave della risorsa per il nome del valore &egrave; composta
     * aggiungendo al nome dell&rsquo;opzione il suffisso
     * {@code ValueName}; se la risorsa non viene rilevata, si assume come nome
     * la stringa impostata nel parametro {@code defaultValueName} del
     * {@linkplain #CliOption costruttore}; se nemmeno questa stringa &egrave;
     * valorizzata, viene utilizzato il nome {@code "value"}.</P>
     *
     * @return Valore.
     * @see    #getName    
     */
    public final String getValueName() {
        String key = myName.concat("ValueName");
        String name;
        ResourceBundle res;
                
        try {
            res = ResourceBundle.getBundle(myResourceBaseName);
            name = res.getString(key);
        } catch (MissingResourceException ex) {
            name = null;
        }
        if (StringUtils.isBlank(name)) {
            name = myDefaultValueName;
        }
        if (StringUtils.isBlank(name)) {
            name = "value";
        }
        
        return name;
    }

    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * <P>La versione di default del metodo {@code getStringValue} restituisce
     * {@code null}; le classi derivate possono implementare una versione
     * prevalente per restituire l&rsquo;eventuale rappresentazione
     * prevista.</P>
     *  
     * @return Valore.
     * @since  2.0.0
     */
    public String getStringValue() {
        return null;
    }
    
    /**
     * Aggiunge un valore.
     *
     * <P>Le classi derivate devono implementare il metodo {@code addValue} per
     * impostare il valore dell&rsquo;opzione oppure aggiungere un nuovo valore
     * alla lista dei valori (se l&rsquo;opzione pu&ograve; essere inserita
     * pi&ugrave; volte).</P>
     *
     * @param  value Valore.
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     * @see    #isMultiple
     */
    protected abstract void addValue(String value);
    
    /**
     * Cancella i valori.
     * 
     * <P>Le classi derivate devono implementare il metodo {@code removeValues}
     * per azzerare il valore dell&rsquo;opzione oppure rimuovere tutti gli
     * elementi della lista dei valori (se l&rsquo;opzione pu&ograve; essere
     * inserita pi&ugrave; volte).</P>
     *
     * @see #isMultiple
     */
    protected abstract void removeValues();
    
    /**
     * Rileva che per l&rsquo;opzione non &egrave; stato inserito il valore
     * sulla linea di comando.
     * 
     * <P>La versione di default del metodo {@code onValueMissing} inoltra
     * un&rsquo;eccezione; le classi derivate possono implementare una versione
     * prevalente, ad esempio, per impostare un valore predefinito.</P>
     * 
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    protected void onValueMissing() {
        ConsoleResources res = new ConsoleResources();
        
        throw new CommandLineArgumentException(
                res.getOptionValueMissingException(myName));        
    }
    
    /**
     * Controlli di validazione.
     *
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    public void check() {
        ConsoleResources res = new ConsoleResources();
        
        switch (mySpecifiedCheck) {
        case REQUIRED:
            if (!myIsSpecified) {
                throw new CommandLineArgumentException(
                        res.getRequiredOptionException(myName));
            }
            break;
         
        case INCOMPATIBLE:
            if (myIsSpecified) {
                throw new CommandLineArgumentException(
                        res.getIncompatibleOptionException(myName));                
            }
            break;
            
        default:
            break;
        }
    } 
    
    /**
     * Emette la descrizione della sintassi dell&rsquo;opzione.
     *
     * @param writer Flusso di scrittura.
     * @see   it.scoppelletti.programmerpower.console.CliOptionSet#writeUsage
     */
    public void writeUsage(Writer writer) {
        String desc;
        PrintWriter printer = null;
        
        try {
            printer = new PrintWriter(writer);
        
            printer.print(CliOption.PREFIX);
            printer.print(myName);
        
            printer.print(" <");
            printer.print(getValueName());
            printer.print('>');
            
            if (myIsMultiple) {
                printer.print(" ...");
            }
        
            printer.println();
            desc = getDescription();
            if (!desc.isEmpty()) {
                printer.println(desc);
            }
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }
    }
    
    /**
     * Modalit&agrave; di controllo sul fatto che un&rsquo;opzione sia inserita
     * o meno sulla linea di comando.
     * 
     * @since 1.0.0
     */
    public static enum SpecifiedCheck {
        
        /**
         * Nessun controllo.
         */
        NONE,
        
        /**
         * Opzione obbligatoria.
         */
        REQUIRED,
        
        /**
         * Opzione non compatibile con le altre.
         */
        INCOMPATIBLE
    }
}

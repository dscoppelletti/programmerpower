/*
 * Copyright (C) 2008-2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Opzione sulla linea di comando che prevede un valore <I>logico</I> del tipo
 * {@code true}/{@code false}.
 * 
 * <P>Per un&rsquo;opzione {@code CliOptionBoolean}, oltre alla classica
 * notazione Java dei valori {@code true}/{@code false}, pu&ograve; anche essere
 * inserito sulla linea di comando un valore da una delle seguenti coppie:</P>
 * 
 * <UL>
 * <LI>{@code on}/{@code off}
 * <LI>{@code yes}/{@code no}
 * <LI>{@code 1}/{@code 0}
 * </UL>
 * 
 * <P>Se sulla linea di comando &egrave; inserita un&rsquo;opzione
 * {@code CliOptionBoolean} ma non &egrave; inserito un valore, si assume per
 * l&rsquo;opzione il valore {@code true}.</P>
 * 
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 1.0.0
 */
public class CliOptionBoolean extends CliOption {    
    private static final String[] VALUES = new String[] { "true", "false",
        "on", "off", "yes", "no", "1", "0" };
    private boolean myValue = false;
     
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionBoolean(String name, String resourceBaseName) {
        super(name, false, resourceBaseName, "switch");
    }
    
    /**
     * Restituisce il valore.
     *
     * @return Valore.
     * @see    #setValue
     */
    public final boolean getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     *
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}.
     * @see                               #getValue
     */
    public final void setValue(boolean value) {
        myValue = value;
    }
    
    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     * @since  2.0.0
     */    
    @Override
    public String getStringValue() {
        return String.valueOf(myValue);
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myValue);
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        int i, n;
        ConsoleResources res = new ConsoleResources();
        
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        value = value.trim().toLowerCase();
        n = CliOptionBoolean.VALUES.length;
        for (i = 0; i < n; i++) {
            if (value.equals(CliOptionBoolean.VALUES[i])) {
                myValue = ((i % 2) == 0);
                return;
            }
        }
        
        throw new CommandLineArgumentException(
                res.getOptionValueBooleanException(getName()));
    }
    
    @Override
    protected final void removeValues() {
    }
    
    /**
     * Rileva che per l&rsquo;opzione non &egrave; stato inserito il valore
     * sulla linea di comando.
     * 
     * <P>Questa versione del metodo {@code onValueMissing} imposta il valore
     * dell&rsquo;opzione {@code true}.</P> 
     */
    @Override
    protected final void onValueMissing() {
        myValue = true;
    }
    
    @Override
    public void writeUsage(Writer writer) {
        int i, n;
        String comma;
        PrintWriter printer = null;
        
        super.writeUsage(writer);
        
        try {
            printer = new PrintWriter(writer);        
            n = CliOptionBoolean.VALUES.length;
            comma = "[";
            for (i = 0; i < n - 1; i += 2) {
                printer.print(comma);
                printer.print(CliOptionBoolean.VALUES[i]);
                printer.print('/');
                printer.print(CliOptionBoolean.VALUES[i + 1]);
                comma = ", ";
            }
            printer.println(']');
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }        
    }
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Insieme delle opzioni sulla linea di comando.
 *
 * <P>La classe {@code CliOptionSet} consente di definire la
 * <ACRONYM TITLE="Command Line Interface">CLI</ACRONYM> delle applicazioni,
 * ovvero di interpretare e validare gli argomenti specificati sulla linea di
 * comando.<BR>
 * Ognuna delle <DFN>opzioni</DFN> previste &egrave; rappresentata da
 * un&rsquo;istanza di una delle classi derivate dalla classe {@code CliOption}
 * a seconda del tipo di opzione.</P>
 *
 * <P>Qualsiasi tipo di applicazione viene avviata da una linea di comando, 
 * anche se poi avvia una
 * <ACRONYM TITLE="Graphical User Interface">GUI</ACRONYM> oppure resta attiva
 * in background come servizio di sistema; anche in questi casi possono essere
 * previsti dei parametri sulla linea di comando, ma talvolta
 * l&rsquo;implementazione resta approssimativa e non omogenea tra le diverse
 * applicazioni anche se realizzate dallo stesso produttore.<BR>
 * Effettivamente l&rsquo;implementazione di un buon parser di linee di comando
 * pu&ograve; risultare pi&ugrave; complessa di quanto sembri a prima vista, e
 * spesso si preferisce investire di pi&ugrave; sulle <I>vere</I>
 * funzionalit&agrave; del programma facendo affidamento sul fatto che la
 * predisposizione delle linee di comando &egrave; normalmente parte integrante
 * della procedura di installazione (es. configurazioni di voci di men&ugrave;
 * sul desktop) oppure &egrave; demandata all&rsquo;amministratore del sistema
 * (o comunque ad un utente evoluto; es. script di Shell).<BR>
 * Tutte le applicazioni Programmer Power utilizzano le funzionalit&agrave;
 * della classe {@code CliOptionSet} per garantire l&rsquo;omogeneit&agrave;
 * della CLI dei programmi.</P>
 *  
 * @see   it.scoppelletti.programmerpower.console.CliOption
 * @see   it.scoppelletti.programmerpower.console.CliStandardOptionSet
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/using/cli.html"
 *        TARGET="_top">Sintassi delle linee di comando</A>
 * @since 1.0.0
 */
public abstract class CliOptionSet {
    
    /**
     * Prefisso per specificare un response file sulla linea di comando. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String PREFIX_RESPONSEFILE = "@";
    
    /**
     * Carattere iniziale delle linee di commento in un response file. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String RESPONSEFILE_COMMENT = "#";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            CliOptionSet.class);
    private final Map<String, CliOption> myOptions;
    
    /**
     * Costruttore.
     */
    protected CliOptionSet() {
        List<CliOption> list = new ArrayList<CliOption>();
        
        initOptions(list);
        
        myOptions = new LinkedHashMap<String, CliOption>();        
        for (CliOption option : list) {
            if (myOptions.put(option.getName(), option) != null) {
                throw new ObjectDuplicateException(option.getName());
            }
        }       
    }
    
    /**
     * Inizializza le opzioni.
     *
     * <P>Le classi derivate devono implementare il metodo {@code initOptions}
     * per aggiungere alla lista {@code list} le opzioni previste dalla
     * CLI.</P>
     *
     * @param list Collezione.
     */
    protected abstract void initOptions(List<CliOption> list);
    
    /**
     * Restituisce un&rsquo;opzione.
     * 
     * <P>Le classi derivate devono utilizzare il metodo {@code getOption} per
     * pubblicare i metodi di lettura delle opzioni.</P>
     *
     * @param  name Nome dell&rsquo;opzione.
     * @return      Opzione. Se l&rsquo;opzione non &egrave; prevista,
     *              restituisce {@code null}.
     */
    public final CliOption getOption(String name) {
        return myOptions.get(name);
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        String value;
        StringBuilder buf = new StringBuilder();
        
        for (CliOption option : myOptions.values()) {
            value = option.toString();
            if (value.isEmpty()) {
                continue;
            }
            
            if (buf.length() > 0) {
                buf.append(' ');
            }
            buf.append(value);
        }
        
        return buf.toString();
    }    
    
    /**
     * Parser degli argomenti sulla linea di comando.
     *
     * <P>Il metodo {@code parseOptions} non esegue i controlli di validazione
     * della CLI che sono invece demandati al metodo {@link #checkOptions}.</P>
     *
     * @param  args Argomenti.
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    public final void parseOptions(String[] args) {
        List<CliArgument> argList;
                
        if (args == null) {
            throw new ArgumentNullException("args");
        }
        
        if (myLogger.isDebugEnabled()) {
            myLogger.debug("Command line argument: {}.", Arrays.toString(args));
        }
        
        for (CliOption option : myOptions.values()) {
            option.setSpecified(false);
            option.removeValues();
        }                        
        argList = buildArgumentList(args);
        lookForOptions(argList);
        acceptOptions(argList);
        
        for (CliArgument arg : argList) {
            arg.check();
        }        
    }

    /**
     * Costruisce la lista degli argomenti.
     *
     * @param  args Argomenti sulla linea di comando.
     * @return      Lista degli argomenti.
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    private List<CliArgument> buildArgumentList(String[] args) {
        int i, n;
        String arg;
        boolean responseFile = false;
        List<CliArgument> list = new ArrayList<CliArgument>();
        
        n = args.length;
        for (i = 0; i < n; i++) {
            arg = args[i];
            
            // Sulla linea di comando posso anche inserire una stringa vuota
            // (oppure di soli blanks) delimitandola con le virgolette:
            // Inoltro eccezioni per argomenti null e non per argomenti vuoti.
            if (arg == null) {
                throw new ArgumentNullException(String.format("args[%1$d]", i));
            }
            
            if (arg.startsWith(CliOptionSet.PREFIX_RESPONSEFILE)) {
                responseFile = true;
                try {                    
                    loadResponseFile(list, arg.substring(1));
                } catch (IOException ex) {
                    throw new CommandLineArgumentException(ex);
                }
            } else {
                list.add(new CliArgument(arg));
            }                       
        }
        
        if (responseFile) {
            myLogger.debug("Command line argument (after response file " +
                    "resolutions): {}.", list);
        }
        
        return list;
    }
    
    /**
     * Legge gli argomenti da un response file.
     *
     * @param  list Lista degli argomenti.
     * @param  path Nome del response file.
     * @throws java.io.IOException
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    private void loadResponseFile(List<CliArgument> list, String path) throws
            IOException {
        String line;
        Reader stream = null;
        BufferedReader reader = null;        
        ConsoleResources res = new ConsoleResources();
        
        if (StringUtils.isBlank(path)) {
            throw new CommandLineArgumentException(
                    res.getResponseFileNameMissingException());
        }
        
        try {
            stream = new FileReader(path);
            reader = new BufferedReader(stream);
            stream = null;
            
            line = reader.readLine();
            while (line != null) {
                splitResponseFileLine(list, line);
                line = reader.readLine();
            }
        } finally {
            if (stream != null) {
                stream.close();
                stream = null;
            }
            if (reader != null) {
                reader.close();
                reader = null;
            }
        }               
    }
    
    /**
     * Suddivide una linea del response file nella coppia (opzione, valore).
     *
     * @param list Lista degli argomenti.
     * @param line Linea del response file.
     */
    private void splitResponseFileLine(List<CliArgument> list, String line) {
        char c;
        int n, p;
        String option, value;
        
        if (line.startsWith(CliOptionSet.RESPONSEFILE_COMMENT)) {
            return;
        }
        
        // Nel response file e' molto facile che ci siano dei caratteri blank
        // non desiderati al fondo delle linee:
        // Eseguo il trim.
        line = StringTools.trimEnd(line);
        if (line.isEmpty()) {
            return;
        }
        
        n = line.length();
        for (p = 0; p < n; p++) {
            c = line.charAt(p);
            if (Character.isWhitespace(c)) {
                break;
            }                
        }
        if (p < n) {
            option = line.substring(0, p);
            value = line.substring(p + 1);
        } else {
            option = line;
            value = "";
        }
        
        // Nel response file e' molto facile che ci siano dei caratteri blank
        // non desiderati tra l'opzione e il valore:
        // Eseguo il trim.
        option = StringTools.trimEnd(option);
        value = StringTools.trimStart(value);
        
        if (!option.startsWith(CliOption.PREFIX)) {
            option = CliOption.PREFIX.concat(option);
        }
        
        list.add(new CliArgument(option));
        if (!value.isEmpty()) {
            list.add(new CliArgument(value));
        }
    }   
    
    /**
     * Riconosce le opzioni tra gli argomenti sulla linea di comando.
     *
     * @param args Argomenti.
     */
    private void lookForOptions(List<CliArgument> args) {
        int prefixLen = CliOption.PREFIX.length();
        String name;
        CliOption option;
        
        for (CliArgument arg : args) {
            if (arg.getOption() != null) {
                // L'argomento e' gia' stato riconosciuto
                continue;
            }            
            if (arg.getValue().length() < prefixLen + 1) {
                // L'argomento non puo' essere un'opzione
                continue;
            }
            if (!arg.getValue().startsWith(CliOption.PREFIX)) {
                // L'argomento non e' un'opzione
                continue;
            }
            
            // L'argomento inizia con il prefisso delle opzioni:
            // Verifico se e' una delle opzioni previste, ma, se non lo e', non
            // segnalo perche' potrebbe anche essere il valore di un'altra
            // opzione (es. un numero negativo).
            name = arg.getValue().substring(prefixLen);
            option = myOptions.get(name);
            if (option != null) {
                // L'argomento e' un'opzione.
                arg.setOption(option);
            }
        }
    }
    
    /**
     * Accetta le opzioni riconosciute tra gli argomenti sulla linea di comando.
     *
     * @param args Argomenti.
     */
    private void acceptOptions(List<CliArgument> args) {
        int i, j, n;
        CliArgument arg;
        CliOption option;
        ConsoleResources res = new ConsoleResources();
        
        n = args.size();
        for (i = 0; i < n; i++) {
            arg = args.get(i);
            option = arg.getOption();
            
            if (option == null) {
                // L'argomento non e' un'opzione
                continue;
            }
            
            if (option.isSpecified() && !option.isMultiple()) {
                throw new CommandLineArgumentException(
                        res.getDuplicateOptionException(option.getName()));
            }
            
            option.setSpecified(true);
            
            // Verifico se l'argomento successivo e' il valore dell'opzione
            j = i + 1;
            if (j == n) {
                // Non ci sono altri argomenti
                option.onValueMissing();
                break;
            }
            
            arg = args.get(j);
            if (arg.getOption() != null) {
                // L'argomento successivo e' un'altra opzione
                option.onValueMissing();
                continue;
            }
            
            i = j;
            option.addValue(arg.getValue());
            arg.setOptionValue(arg.getValue());
        }        
    }
    
    /**
     * Controlli di validazione.
     *
     * <P>La versione di default del metodo {@code checkOptions} esegue il
     * metodo {@link it.scoppelletti.programmerpower.console.CliOption#check} di
     * ogni singola opzione.<BR>
     * Le classi derivate possono implementare una versione prevalente del
     * metodo {@code checkOptions} per eseguire ulteriori controlli
     * specifici.</P>
     *
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */    
    public void checkOptions() {
        for (CliOption option : myOptions.values()) {
            option.check();
        }
    }
         
    /**
     * Emette la descrizione della sintassi della linea di comando.
     *
     * @param writer Flusso di scrittura.
     * @see   it.scoppelletti.programmerpower.console.CliOption#writeUsage
     */
    public final void writeUsage(Writer writer) {
        boolean first = true;
        PrintWriter printer = null;
        
        try {
            printer = new PrintWriter(writer);        
            for (CliOption option : myOptions.values()) {
                if (first) {
                    first = false;
                } else {
                    printer.println();
                }
                
                option.writeUsage(printer);
            }
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }
    }    
}

/*
 * Copyright (C) 2008-2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

/**
 * Opzione sulla linea di comando che prevede un valore intero.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 1.0.0
 */
public class CliOptionInteger extends CliOption {
    private int myValue = 0;
    private int myValueMin = Integer.MIN_VALUE;
    private int myValueMax = Integer.MAX_VALUE;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionInteger(String name, String resourceBaseName) {
        super(name, false, resourceBaseName, null);
    }
    
    /**
     * Restituisce il valore.
     *
     * @return Valore.
     * @see    #setValue
     */
    public final int getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     *
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code 0}
     * @see                               #getValue
     */
    public final void setValue(int value) {
        myValue = value;
    }
    
    /**
     * Restituisce il valore minimo.
     *
     * @return Valore.
     * @see    #setValueMin
     */
    public final int getValueMin() {
        return myValueMin;
    }
    
    /**
     * Imposta il valore minimo.
     *
     * @param                       value Valore. 
     * @it.scoppelletti.tag.default       {@code Integer.MIN_VALUE}
     * @see                               #getValueMin
     */
    public final void setValueMin(int value) {
        myValueMin = value;
    }
    
    /**
     * Restituisce il valore massimo.
     *
     * @return Valore.
     * @see    #setValueMax
     */
    public final int getValueMax() {
        return myValueMax;
    }
    
    /**
     * Imposta il valore massimo.
     *
     * @param                       value Valore. 
     * @it.scoppelletti.tag.default       {@code Integer.MAX_VALUE}
     * @see                               #getValueMax
     */
    public final void setValueMax(int value) {
        myValueMax = value;
    }

    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     * @since  2.0.0
     */    
    @Override
    public String getStringValue() {
        return String.valueOf(myValue);
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myValue);
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        ConsoleResources res = new ConsoleResources();
        
        try {
            myValue = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            throw new CommandLineArgumentException(
                    res.getOptionValueIntegerException(getName()));
        }
    }
    
    @Override
    protected final void removeValues() {
        myValue = 0;
    }
    
    @Override
    public void check() {
        ConsoleResources res = new ConsoleResources();
        
        super.check();
        if (!isSpecified()) {
            return;
        }
        
        if (myValue < myValueMin || myValue > myValueMax) {
            throw new CommandLineArgumentException(
                    res.getOptionValueIntegerRangeException(getName(),
                    myValue, myValueMin, myValueMax));
        }
    }  
}

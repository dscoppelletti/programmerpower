/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.io;

import java.io.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Classe di factory di un flusso di scrittura su file di testo.
 * 
 * @since 2.1.0
 */
public class ConsoleOutputStreamWriterFactory implements
        OutputStreamWriterFactory {
    private File myFile;
    private boolean myOverwrite;
    private DefaultOutputStream myDefaultOutputStream;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
    
    /**
     * Costruttore.
     */
    public ConsoleOutputStreamWriterFactory() {
        myDefaultOutputStream = DefaultOutputStream.NONE;
    }
    
    /**
     * Imposta il file.
     * 
     * @param                        file File.
     * @it.scoppelletti.tag.required      Se non &egrave; previsto un flusso di
     *                                    default. 
     * @see                               #setDefaultOutputStream
     */
    @Final
    public void setFile(File file) {
        myFile = file;
    }
    
    /**
     * Imposta l&rsquo;indicatore di possibilit&agrave; di sovrascrivere il
     * file anche se esiste gi&agrave;.
     *
     * <P>La propriet&agrave; {@code overwrite} &egrave; presa in considerazione
     * solo se il flusso non &egrave; {@code stdout} n&eacute;
     * {@code stderr}.</P>
     *   
     * @param                       value Indicatore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #setFile
     * @see                               #setDefaultOutputStream
     */
    @Final
    public void setOverwrite(boolean value) {
        myOverwrite = value;
    }
    
    /**
     * Imposta il flusso di default nel caso il file non sia specificato.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code NONE}
     * @see                               #setFile
     */
    public void setDefaultOutputStream(DefaultOutputStream value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myDefaultOutputStream = value;
    }
    
    @Final
    public BufferedWriter open() throws IOException {
        Writer writer = null;
        Writer filtered = null;        
        BufferedWriter buffered;
                      
        try {
            writer = openWriter();
            filtered = activateFilters(writer);
            writer = null;
            buffered = new BufferedWriter(filtered);
            filtered = null;
        }  finally {
            filtered = IOTools.close(filtered);
            writer = IOTools.close(writer);
        }
        
        return buffered; 
    }
    
    /**
     * Apre il flusso di scrittura.
     * 
     * @return Flusso di scrittura.
     */
    private Writer openWriter() throws IOException {
        PrintStream out;
        IOResources ioRes = new IOResources();
      
        if (myFile == null) {
            switch (myDefaultOutputStream) {
            case STDOUT:
                out = System.out;
                break;
                
            case STDERR:
                out = System.err;
                break;
                
            default: // NONE
                throw new PropertyNotSetException(toString(), "file");
            }            
            
            return new OutputStreamWriter(new ConsolePrintStream(out));            
        }        
        
        if (myFile.exists()) {
            if (!myOverwrite) {
                throw new IOException(ioRes.getFileAlreadyExistException(
                        myFile.toString()));
            }            

            myUI.display(MessageType.WARNING, ioRes.getFileOverwriteMessage(
                  myFile.toString()));
        }
      
        return new FileWriter(myFile);              
    }
    
    /**
     * Attiva i filtri sul flusso di scrittura.
     * 
     * <P>L&rsquo;implementazione di default del metodo {@code activateFilters}
     * si limita a restituire il flusso {@code writer} originale.<BR>
     * Le classi derivate possono implementare una versione prevalente del 
     * metodo {@code activateFilters} per applicare i filtri desiderati al
     * flusso di scrittura prima che sia applicato il filtro
     * {@code BufferedWriter}.</P>
     * 
     * @param  writer Flusso originale.
     * @return        Flusso filtrato.
     * @throws        IOException
     */
    protected Writer activateFilters(Writer writer) throws IOException {
        return writer;
    }
}

/*
 * Copyright (C) 2009-2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import java.nio.charset.*;
import java.util.*;
import it.scoppelletti.programmerpower.*;

/**
 * Opzione sulla linea di comando che prevede come valore il nome di una
 * codifica di caratteri.
 * 
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 1.0.0
 */
public class CliOptionCharset extends CliOption {
    private Charset myCharset = Charset.defaultCharset();
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionCharset(String name, String resourceBaseName) {
        super(name, false, resourceBaseName, "charset");
    }
    
    /**
     * Restituisce la codifica dei caratteri.
     *
     * @return Oggetto.
     * @see    #setCharset
     */
    public final Charset getCharset() {
        return myCharset;
    }
    
    /**
     * Imposta la codifica dei caratteri.
     *
     * @param                       obj Oggetto.
     * @it.scoppelletti.tag.default     Charset di default di JVM.
     * @see                             #getCharset
     */
    public final void setCharset(Charset obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myCharset = obj;
    }
    
    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     * @since  2.0.0
     */    
    @Override
    public String getStringValue() {
        return String.valueOf(myCharset.name());
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myCharset.name());
        
        return buf.toString();
    } 
    
    @Override
    protected final void addValue(String value) {
        ConsoleResources res = new ConsoleResources();
        
        try {
            myCharset = Charset.forName(value);
        } catch (Exception ex) {
            throw new CommandLineArgumentException(
                    res.getCharsetInvalidException(value), ex);
        }
    }
    
    @Override
    protected final void removeValues() {
        myCharset = Charset.defaultCharset();
    }
    
    @Override
    public void writeUsage(Writer writer) {
        PrintWriter printer = null;
        Map<String, Charset> charsetMap;
        
        super.writeUsage(writer);
        
        try {
            printer = new PrintWriter(writer);        
            charsetMap = Charset.availableCharsets();
            printer.println(charsetMap.keySet().toString());
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }
    }  
}

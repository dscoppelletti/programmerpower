/*
 * Copyright (C) 2008-2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import it.scoppelletti.programmerpower.*;

/**
 * Opzione sulla linea di comando che prevede un valore stringa.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 1.0.0
 */
public class CliOptionString extends CliOption {
    private String myValue = "";
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionString(String name, String resourceBaseName) {
        super(name, false, resourceBaseName, null);
    }
    
    /**
     * Restituisce il valore.
     *
     * @return Valore.
     * @see    #setValue
     */
    public final String getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     *
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Stringa vuota {@code ""}.
     * @see                               #getValue
     */
    public final void setValue(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myValue = value;
    }

    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     * @since  2.0.0
     */    
    @Override
    public String getStringValue() {
        return myValue;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myValue);
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myValue = value;
    }
    
    @Override
    protected final void removeValues() {
        myValue = "";
    }    
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Flusso di filtro per la scrittura sulla console.
 * 
 * <P>Il filtro {@code ConsoleWriter} pu&ograve; essere utilizzato per scrivere
 * sulla console del testo eseguendone il word-wrap in modo che le linee di
 * testo non superino la lunghezza massima visibile delle linee sul
 * terminale.<BR>
 * Sulla propriet&agrave; di sistema
 * {@code it.scoppelletti.programmerpower.console.lineLen} dovrebbe essere
 * impostata impostata la lunghezza massima visibile delle linee di testo sulla
 * console; se tale propriet&agrave; non &egrave; impostata, viene assunto il
 * valore di default {@code 80}.<BR>
 * Il filtro {@code ConsoleWriter} mantiene le interruzioni di linea originali
 * del testo esposto.</P>
 * 
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/textflat/wordwrap.html#idKeepNewLine"
 *      TARGET="_top">Gestione delle interruzioni di linea originali</A>                  
 * @since 1.0.0
 */
public class ConsoleWriter extends WordWrapperWriter {

    /**
     * Lunghezza massima visibile delle linee di testo sulla console assunta
     * per default se la propriet&agrave; di sistema 
     * {@code it.scoppelletti.programmerpower.console.lineLen} non &egrave;
     * stata impostata. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final int LINELEN = 80;
    
    /**
     * Nome della propriet&agrave; di sistema sulla quale pu&ograve; essere
     * impostata la lunghezza massima visibile delle linee di testo sulla
     * console. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     *
     * @it.scoppelletti.tag.default {@code 80}
     */
    public static final String PROP_LINELEN =
        "it.scoppelletti.programmerpower.console.lineLen";
    
    /**
     * Costruttore.
     * 
     * @param writer Flusso di scrittura sottostante.                 
     */
    public ConsoleWriter(Writer writer) {
        super(writer, ConsoleHolder.LINELEN, true);
    }
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */    
    private static final class ConsoleHolder {
        static final int LINELEN = initLineLen();
        
        /**
         * Inizializza la lunghezza massima visibile delle linee di testo sulla
         * console.
         * 
         * @return Valore.
         */
        static int initLineLen() {
            int len;
            String value;
            Logger logger;
            
            logger = LoggerFactory.getLogger(ConsoleWriter.class);            
            value = JVMTools.getProperty(ConsoleWriter.PROP_LINELEN);
            if (StringUtils.isBlank(value)) {
                len = ConsoleWriter.LINELEN;                
            } else {
                try {
                    len = Integer.parseInt(value);
                } catch (NumberFormatException ex) {
                    logger.error(String.format("Value \"%1$s\" is not integer.",
                            value), ex);
                    len = ConsoleWriter.LINELEN;
                } 
            }
            
            logger.debug("Max visible line length of text upon console: {}.",
                    len);
            
            return len;
        }
    }    
}

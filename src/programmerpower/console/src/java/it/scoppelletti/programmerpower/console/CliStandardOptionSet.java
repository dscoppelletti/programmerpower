/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;        

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.resources.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Opzioni standard sulla linea di comando.
 * 
 * @since 1.0.0
 */
public abstract class CliStandardOptionSet extends CliOptionSet {
    
    /**
     * Valore di ritorno del metodo {@link #acceptOptions} per il quale
     * l&rsquo;applicazione pu&ograve; proseguire con le proprie funzioni. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final int CONTINUE = -1;

    /**
     * Opzione per impostare il profilo di configurazione per l&rsquo;accesso
     * allo strato di persistenza.  Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see   it.scoppelletti.programmerpower.data.DataTools#CONFIG_FACTORY
     * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
     *        TARGET="_top">Sistema di configurazione</A>    
     * @since 2.1.0  
     */
    public static final String OPTION_DATACONFIG = "dataconfig";
    
    /**
     * Opzione per disattivare la visualizzazione delle informazioni di
     * copyright. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     *
     * @see #getNoLogo
     */
    public static final String OPTION_NOLOGO = "nologo";
    
    /**
     * Opzione per attivare il protocollo SSL. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     *
     * @since 2.1.0
     */
    public static final String OPTION_SSL = "ssl";
    
    /**
     * Opzione per visualizzare la sintassi della linea di comando. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     *     
     * @see #getUsage
     * @see it.scoppelletti.programmerpower.console.CliOptionSet#writeUsage
     */
    public static final String OPTION_USAGE = "usage";

    /**
     * Base del nome del contenitore di risorse per la localizzazione delle
     * opzioni sulla linea di comando. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @since 2.1.0
     */    
    public static final String RESOURCE_BASENAME = 
            "it.scoppelletti.programmerpower.console.CliStandardOptionSet";
    
    /**
     * Costruttore.
     */
    public CliStandardOptionSet() {
    }

    /**
     * Inizializza le opzioni.
     *
     * <P>Le applicazioni Programmer Power normalmente implementano una versione
     * prevalente del metodo {@code initOptions} per inserire le opzioni
     * specifiche della loro CLI prima di quelle standard previste dalla classe
     * {@code CliStandardOptionSet}.</P>  
     * 
     * @param list Collezione.
     */
    @Override
    protected void initOptions(List<CliOption> list) { 
        CliOption option;

        option = new CliOptionBoolean(CliStandardOptionSet.OPTION_NOLOGO,
                CliStandardOptionSet.RESOURCE_BASENAME);
        list.add(option);
        option = new CliOptionBoolean(CliStandardOptionSet.OPTION_USAGE,
                CliStandardOptionSet.RESOURCE_BASENAME);
        list.add(option);
    }

    /**
     * Opzione per disattivare la visualizzazione delle informazioni di
     * copyright.
     * 
     * @return Opzione.
     * @see    #OPTION_NOLOGO
     */
    public final CliOptionBoolean getNoLogo() {
        return (CliOptionBoolean) getOption(CliStandardOptionSet.OPTION_NOLOGO);
    }

    /**
     * Opzione per visualizzare la sintassi della linea di comando.
     *              
     * @return Opzione.
     * @see    #OPTION_USAGE
     */
    public final CliOptionBoolean getUsage() {
        return (CliOptionBoolean) getOption(CliStandardOptionSet.OPTION_USAGE);
    }
    
    /**
     * Parser degli argomenti sulla linea di comando.
     * 
     * <P>Il metodo {@code acceptOptions} combina le funzioni dei metodi 
     * {@link it.scoppelletti.programmerpower.console.CliOptionSet#parseOptions}
     * e
     * {@link it.scoppelletti.programmerpower.console.CliOptionSet#checkOptions}
     * della classe base {@code CliOptionSet} e le integra con la gestione delle
     * opzioni standard della CLI previste dalla classe
     * {@code CliStandardOptionSet}; l&rsquo;utilizzo del metodo
     * {@code acceptOptions} consente quindi all&rsquo;implementazione
     * dell&rsquo;applicazione specifica di non provvedere alla gestione delle
     * opzioni standard.</P>   
     *  
     * @param  args Argomenti.
     * @return      Se l&rsquo;applicazione pu&ograve; proseguire con le proprie
     *              funzioni, restituisce {@code CliStandardOptionSet.CONTINUE},
     *              altrimenti restituisce il codice di uscita con il quale
     *              terminare JVM. 
     */
    public final int acceptOptions(String[] args) {
        int ret = CliStandardOptionSet.CONTINUE;
        Exception pendingEx = null;
        PrintWriter writer = null;
        ApplicationInfo about;
        ConsoleUI ui = new ConsoleUI(getClass().getName());
        
        try {
            parseOptions(args);
        } catch (CommandLineArgumentException ex) {
            ret = JVMTools.EXIT_FAILURE;
            pendingEx = ex;
            getUsage().setValue(true);           
        } catch (Exception ex) {
            ret = JVMTools.EXIT_FAILURE;
            pendingEx = ex;
            getUsage().setValue(false);
        }   
        
        try {
            writer = new PrintWriter(new ConsoleWriter(new OutputStreamWriter(
                    System.err)));            
            if (!getNoLogo().getValue()) {
                about = new ApplicationAbout(getApplicationClass());
            
                writer.println("<about>");
                ResourceTools.write(writer, about);
                writer.println("</about>");
                writer.flush();
            }
        
            if (ret == CliStandardOptionSet.CONTINUE) {
                if (getUsage().getValue()) {
                    // Espongo la sintassi e termino ignorando le altre opzioni
                    ret = JVMTools.EXIT_SUCCESS;
                } else {
                    try {
                        checkOptions();
                    } catch (CommandLineArgumentException ex) {
                        ret = JVMTools.EXIT_FAILURE;
                        pendingEx = ex;
                        getUsage().setValue(true);           
                    } catch (Exception ex) {
                        ret = JVMTools.EXIT_FAILURE;
                        pendingEx = ex;
                        getUsage().setValue(false);
                    }                   
                }     
            }
                
            if (pendingEx != null) {
                ui.display(MessageType.ERROR, pendingEx);
            }
            if (getUsage().getValue()) {            
                writer.println("<usage>");
                writeUsage(writer);
                writer.println("</usage>");
            }
        } finally {
            if (writer != null) {
                writer.flush();
                writer = null;
            }
        }
        
        return ret;
    }
    
    /**
     * Restituisce la classe che rappresenta l&rsquo;applicazione.
     *
     * <P>Per classe che rappresenta un&rsquo;applicazione, si intende
     * normalmente la classe che implementa il metodo statico {@code main} di
     * entry-point dell&rsquo;applicazione stessa; questa informazione
     * pu&ograve; essere utilizzata per accedere ad alcune risorse specifiche
     * dell&rsquo;applicazione.</P>
     *
     * @return Classe.
     */
    protected abstract Class<?> getApplicationClass();    
}

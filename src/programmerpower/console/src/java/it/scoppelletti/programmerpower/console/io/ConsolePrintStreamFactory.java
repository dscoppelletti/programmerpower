/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.io;

import java.io.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Classe di factory di un flusso di stampa.
 * 
 * @since 2.1.0
 */
@Final
public class ConsolePrintStreamFactory implements PrintStreamFactory {
    private File myFile;
    private boolean myOverwrite;
    private DefaultOutputStream myDefaultOutputStream;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
    
    /**
     * Costruttore.
     */
    public ConsolePrintStreamFactory() {
        myDefaultOutputStream = DefaultOutputStream.NONE;
    }
    
    /**
     * Imposta il file.
     * 
     * @param                        file File.
     * @it.scoppelletti.tag.required      Se non &egrave; previsto un flusso di
     *                                    default. 
     * @see                               #setDefaultOutputStream
     */
    public void setFile(File file) {
        myFile = file;
    }
    
    /**
     * Imposta l&rsquo;indicatore di possibilit&agrave; di sovrascrivere il
     * file anche se esiste gi&agrave;.
     *
     * <P>La propriet&agrave; {@code overwrite} &egrave; presa in considerazione
     * solo se il flusso non &egrave; {@code stdout} n&eacute;
     * {@code stderr}.</P>
     *   
     * @param                       value Indicatore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #setFile
     * @see                               #setDefaultOutputStream
     */
    public void setOverwrite(boolean value) {
        myOverwrite = value;
    }
    
    /**
     * Imposta il flusso di default nel caso il file non sia specificato.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code NONE}
     * @see                               #setFile
     */
    public void setDefaultOutputStream(DefaultOutputStream value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myDefaultOutputStream = value;
    }
    
    public PrintStream open() throws IOException {
        PrintStream out;
        IOResources ioRes = new IOResources();
      
        if (myFile == null) {
            switch (myDefaultOutputStream) {
            case STDOUT:
                out = System.out;
                break;
                
            case STDERR:
                out = System.err;
                break;
                
            default: // NONE
                throw new PropertyNotSetException(toString(), "file");
            }
            
            return new ConsolePrintStream(out);
        }
      
        if (myFile.exists()) {
            if (!myOverwrite) {
                throw new IOException(ioRes.getFileAlreadyExistException(
                        myFile.toString()));
            }            
            
            myUI.display(MessageType.WARNING, ioRes.getFileOverwriteMessage(
                  myFile.toString()));
        }
      
        out = new PrintStream(myFile);
      
        return out;
    }
}

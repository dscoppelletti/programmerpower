/*
 * Copyright (C) 2008-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse per la gestione della console.
 */
final class ConsoleResources extends ResourceWrapper {
    
    /**
     * Costruttore.
     */
    ConsoleResources() {
    }
    
    /**
     * Eccezione per argomento non valido.
     * 
     * @param  arg Valore dell&rsquo;argomento.
     * @return     Testo.
     */
    String getArgumentInvalidException(String arg) {        
        return format("ArgumentInvalidException", arg);
    }

    /**
     * Eccezione per opzione obbligatoria non specificata.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @return        Testo.
     */
    String getRequiredOptionException(String option) {
        return format("RequiredOptionException", option);
    }

    /**
     * Eccezione per opzione non compatibile con le altre inserite sulla linea
     * di comando.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @return        Testo.
     */
    String getIncompatibleOptionException(String option) {
        return format("IncompatibleOptionException", option);
    }
    
    /**
     * Eccezione per opzione duplicata.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @return        Testo.
     */
    String getDuplicateOptionException(String option) {
        return format("DuplicateOptionException", option);
    }

    /**
     * Eccezione per valore di opzione non specificato.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @return        Testo.
     */
    String getOptionValueMissingException(String option) {
        return format("OptionValueMissingException", option);
    }

    /**
     * Eccezione per nome del response file mancante.
     * 
     * @return Testo.
     */
    String getResponseFileNameMissingException() {
        return getString("ResponseFileNameMissingException");
    }

    /**
     * Eccezione per valore di opzione non valido.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @return        Testo.
     */
    String getOptionValueIntegerException(String option) {        
        return format("OptionValueIntegerException", option);
    }

    /**
     * Eccezione per valore di opzione non valido.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @param  value  Valore specificato.
     * @param  min    Valore minimo.
     * @param  max    Valore massimo.
     * @return        Testo.
     */
    String getOptionValueIntegerRangeException(String option, int value,
            int min, int max) {        
        return format("OptionValueIntegerRangeException", option, value, min,
                max);
    }

    /**
     * Eccezione per valore di opzione non valido.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @return        Testo.
     */
    String getOptionValueBooleanException(String option) {        
        return format("OptionValueBooleanException", option);
    }
    
    /**
     * Eccezione per codifica dei caratteri non valida.
     *  
     * @param  charset Nome del charset.
     * @return         Testo.
     */
    String getCharsetInvalidException(String charset) {
        return format("CharsetInvalidException", charset);
    }    
        
    /**
     * Etichetta per un&rsquo;eccezione.
     * 
     * @return Testo.
     */
    String getExceptionLabel() {
        return getString("ExceptionLabel");
    }
    
    /**
     * Etichetta per il contesto del thread corrente.
     * 
     * @return Testo.
     */
    String getDiagnosticContextLabel() {
        return getString("DiagnosticContextLabel");
    }
          
    /**
     * Eccezione per console non disponibile.
     * 
     * @return Testo.
     */
    String getConsoleNotAvailableException() {
        return getString("ConsoleNotAvailableException");
    }      
    
    /**
     * Eccezione per nome di profilo di configurazione non valido.
     * 
     * @param  option    Nome dell&rsquo;opzione.
     * @param  groupName Nome del gruppo.
     * @param  cfgName   Nome del profilo.
     * @return           Testo.
     */
    String getConfigInvalidException(String option, String groupName,
            String cfgName) {
        return format("ConfigInvalidException", option, groupName, cfgName);
    }
    
    /**
     * Eccezione per numero di valori di opzione non valido.
     * 
     * @param  option Nome dell&rsquo;opzione.
     * @param  size   Numero di valori.
     * @param  min    Numero di valori minimo.
     * @param  max    Numero di valori massimo.
     * @return        Testo.
     */
    String getOptionListSizeRangeException(String option, int size,
            int min, int max) {        
        return format("OptionListSizeRangeException", option, size, min, max);
    }      
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.security;

import it.scoppelletti.programmerpower.console.*;

/**
 * Opzione sulla linea di comando che prevede come valore il nome di un utente.
 *
 * @see it.scoppelletti.programmerpower.console.security.ConsoleCallbackHandler
 * @see it.scoppelletti.programmerpower.console.CliOptionSet 
 * @since 2.1.0
 */
public final class CliOptionUserName extends CliOptionString {

    /**
     * Nome dell&rsquo;opzione. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.     
     */
    public static final String NAME = "user";
    
    /**
     * Costruttore.
     */
    public CliOptionUserName() {
        super(CliOptionUserName.NAME, CliStandardOptionSet.RESOURCE_BASENAME);
    }
}

/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.spring.config;

import org.apache.commons.lang3.*;
import org.springframework.core.env.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.spring.*;

/**
 * Sorgente per la sostituzione dei riferimenti alle propriet&agrave; nelle
 * risorse di definizione dei bean.
 * 
 * <P>Il componente {@code ConsolePropertySource} implementa la lettura del
 * valore delle opzioni impostate sulla linea di comando: il valore di
 * un&rsquo;opzione corrisponde alla propriet&agrave; della quale il nome
 * &egrave; composto applicando il prefisso {@code it.scoppelletti.param.} al
 * nome dell&rsquo;opzione; ad esempio, il valore della propriet&agrave; 
 * {@code it.scoppelletti.param.dataconfig} corrisponde al valore
 * dell&rsquo;opzione {@code -dataconfig}.</P>
 * 
 * <BLOCKQUOTE CLASS="note">
 * Non tutti i tipi di opzione sono presi in considerazione dal componente
 * {@code ConsolePropertySource}: affinch&egrave; un&rsquo;opzione sia presa
 * in considerazione, la corrispondente classe derivata dalla classe
 * {@code CliOption} deve implementare opportunamente una versione prevalente
 * del metodo
 * {@link it.scoppelletti.programmerpower.console.CliOption#getStringValue}.
 * </BLOCKQUOTE> 
 *
 * @see it.scoppelletti.programmerpower.console.CliOption
 * @see it.scoppelletti.programmerpower.console.spring.ConsoleApplicationRunner
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
 *      TARGET="_top">Propriet&agrave; di ambiente</A> 
 * @since 2.0.0
 */
public final class ConsolePropertySource extends
    PropertySource<ConsoleApplicationRunner> {

    /**
     * Prefisso applicato al nome delle opzioni sulla linea di comando per
     * comporre i nomi delle propriet&agrave; corrispondenti. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String PREFIX_OPT = "it.scoppelletti.param.";
    
    /**
     * Costruttore.
     * 
     * @param name Nome del componente.
     * @param appl Applicazione.
     */
    public ConsolePropertySource(String name, ConsoleApplicationRunner appl) {
        super(name, appl);
    }

    /**
     * Restituisce il valore di una propriet&agrave;
     * 
     * @param  name Nome della propriet&agrave;.
     * @return      Valore. Se la propriet&agrave; non &egrave; impostata,
     *              restituisce {@code null}.
     */    
    @Override
    public Object getProperty(String name) {
        CliOption option;
        
        if (StringUtils.isBlank(name)) {
            return null;
        }
       
        if (name.length() <= ConsolePropertySource.PREFIX_OPT.length() ||
                !name.startsWith(ConsolePropertySource.PREFIX_OPT)) {
            return null;
        }
        
        name = name.substring(ConsolePropertySource.PREFIX_OPT.length());        
        option = getSource().getOption(name);
        if (option == null || !option.isSpecified()) {
            return null;
        }
        
        return option.getStringValue();
    }           
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.*;
import java.util.*;

/**
 * Opzione sulla linea di comando che prevede una lista di nomi di file.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 2.1.0
 */
public class CliOptionFileList extends CliOption {
    private final List<File> myValues = new ArrayList<File>();
    private int mySizeMin = 0;
    private int mySizeMax = Integer.MAX_VALUE;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionFileList(String name, String resourceBaseName) {
        super(name, true, resourceBaseName, "path");
    }
    
    /**
     * Restituisce la lista di valori.
     *
     * @return Collezione.
     */
    public final List<File> getList() {
        return myValues;
    }
    
    /**
     * Restituisce il numero di valori minimo.
     *
     * @return Valore.
     * @see    #setSizeMin
     */
    public final int getSizeMin() {
        return mySizeMin;
    }
    
    /**
     * Imposta il numero di valori minimo.
     *
     * @param                       value Valore. 
     * @it.scoppelletti.tag.default       {@code 0}
     * @see                               #getSizeMin
     */
    public final void setSizeMin(int value) {
        mySizeMin = value;
    }
    
    /**
     * Restituisce il numero di valori massimo.
     *
     * @return Valore.
     * @see    #setSizeMax
     */
    public final int getSizeMax() {
        return mySizeMax;
    }
    
    /**
     * Imposta il numero di valori massimo.
     *
     * @param                       value Valore. 
     * @it.scoppelletti.tag.default       {@code Integer.MAX_VALUE}
     * @see                               #getSizeMax
     */
    public final void setSizeMax(int value) {
        mySizeMax = value;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myValues.toString());
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        myValues.add(new File(value));
    }
    
    @Override
    protected final void removeValues() {
        myValues.clear();
    }
    
    @Override
    public void check() {
        int size;
        ConsoleResources res = new ConsoleResources();
        
        super.check();
        if (!isSpecified()) {
            return;
        }
        
        switch (getSpecifiedCheck()) {
        case REQUIRED:
            if (mySizeMin < 1) {
                mySizeMin = 1;
            }            
            break;
            
        default:
            break;
        }
        
        size = myValues.size();
        if (size < mySizeMin || size > mySizeMax) {
            throw new CommandLineArgumentException(
                    res.getOptionListSizeRangeException(getName(), size,
                    mySizeMin, mySizeMax));
        }
    }      
}
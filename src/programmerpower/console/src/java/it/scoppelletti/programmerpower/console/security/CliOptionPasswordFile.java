/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console.security;

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.security.*;

/**
 * Opzione sulla linea di comando che prevede come valore il nome di un file di
 * testo dal quale acquisire una password.
 * 
 * <P>Per preservare la riservatezza di una password, un programma non dovrebbe
 * mai prevederne l&rsquo;inserimento in chiaro tra i parametri sulla linea di
 * comando perch&eacute; spesso i sistemi operativi consentono di interrogare
 * le linee di comando eseguite.<BR>
 * L&rsquo;opzione sulla linea di comando {@code -password} consente di
 * specificare il nome di un file di testo dalla prima riga del quale acquisire
 * la password; naturalmente tale file deve essere mantenuto in un ambiente
 * sicuro ed in particolare deve essere:</P>
 * 
 * <OL>
 * <LI>Accessibile in lettura dai soli utenti autorizzati a conoscere la
 * password.
 * <LI>Accessibile in scrittura dal solo amministratore di sistema per poter
 * eventualmente modificare la password.
 * </OL> 
 *
 * @see it.scoppelletti.programmerpower.console.security.ConsoleCallbackHandler
 * @see it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 2.1.0
 */
public final class CliOptionPasswordFile extends CliOptionFile {

    /**
     * Nome dell&rsquo;opzione. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.     
     */
    public static final String NAME = "password";
    
    /**
     * Costruttore.
     */
    public CliOptionPasswordFile() {
        super(CliOptionPasswordFile.NAME,
                CliStandardOptionSet.RESOURCE_BASENAME);
    }   
    
    /**
     * Restituisce la password.
     * 
     * @return Valore. Pu&ograve; essere {@code null}
     */
    @SuppressWarnings("resource")
    public SecureString getPassword() throws IOException {
        int c, i, n;
        char[] v;
        SecureString s;
        StringBuilder buf;
        FileReader reader = null;
        File file = getFile();
        
        if (file == null) {
            return null;
        }
        
        buf = new StringBuilder();
        try {
            reader = new FileReader(file);
            
            c = reader.read();
            while (c != -1) {
                if (c == '\n' || c == '\r') {
                    break;
                }
                
                buf.append((char) c);
                c = reader.read();
            }
        } finally {
            reader = IOTools.close(reader);
        }
        
        n = buf.length();
        v = new char[n];
        buf.getChars(0, n, v, 0); 
        for (i = 0; i < n; i++) {
            buf.setCharAt(i, (char) 0);
        }
        
        s = new SecureString(v);
        Arrays.fill(v, (char) 0);
        
        return s;
    }
}

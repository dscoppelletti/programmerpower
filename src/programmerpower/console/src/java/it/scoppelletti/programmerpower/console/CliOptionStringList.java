/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.util.*;

/**
 * Opzione sulla linea di comando che prevede una lista di valori stringa.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet
 * @since 2.0.0
 */
public class CliOptionStringList extends CliOption {    
    private List<String> myValues = new ArrayList<String>();
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    public CliOptionStringList(String name, String resourceBaseName) {
        super(name, true, resourceBaseName, null);
    }
    
    /**
     * Restituisce la lista di valori.
     *
     * @return Collezione.
     */
    public final List<String> getList() {
        return myValues;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myValues.toString());
        
        return buf.toString();
    }    
    
    @Override
    protected final void addValue(String value) {
        myValues.add(value);
    }
    
    @Override
    protected final void removeValues() {
        myValues.clear();
    }    
}

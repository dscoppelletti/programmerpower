/*
 * Copyright (C) 2010-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.console;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Set;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.config.ConfigContext;

/**
 * Opzione sulla linea di comando che prevede come valore il nome di un profilo
 * di configurazione.
 *
 * @see   it.scoppelletti.programmerpower.console.CliOptionSet 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html"
 *        TARGET="_top">Sistema di configurazione</A>       
 * @since 2.0.0
 */
public class CliOptionConfig extends CliOption {
    private final ConfigContext myConfigCtx;
    private String myProfileName = "";
    
    /**
     * Costruttore.
     *
     * @param optName          Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     * @param configFactory    Oggetto di factory del contesto di
     *                         configurazione.   
     * @since 2.1.0                  
     */
    public CliOptionConfig(String optName, String resourceBaseName,
            ConfigContext.Factory configFactory) {
        super(optName, false, resourceBaseName, "name");
        
        if (configFactory == null) {
            throw new ArgumentNullException("configFactory");
        }
        
        myConfigCtx = configFactory.newConfigContext();
    }
    
    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     */    
    @Override
    public String getStringValue() {
        return myProfileName;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     *
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (!isSpecified()) {
            return "";
        }
        
        StringBuilder buf = new StringBuilder(CliOption.PREFIX);
        
        buf.append(getName());
        buf.append(' ');
        buf.append(myProfileName);
        
        return buf.toString();
    }  
    
    @Override
    protected final void addValue(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myProfileName = value;
    }
    
    @Override
    protected final void removeValues() {
        myProfileName = "";
    }
    
    /**
     * Rileva che per l&rsquo;opzione non &egrave; stato inserito il valore
     * sulla linea di comando.
     * 
     * <P>Questa versione del metodo {@code onValueMissing}, se il contesto di
     * configurazione prevede un solo profilo, imposta  l&rsquo;unico profilo di
     * configurazione come valore dell&rsquo;opzione, altrimenti inoltra 
     * un&rsquo;eccezione (versione di default del metodo).</P> 
     * 
     * @throws it.scoppelletti.programmerpower.console.CommandLineArgumentException
     */
    @Override
    protected final void onValueMissing() {
        Set<String> names;
        
        names = myConfigCtx.listProfiles();
        if (names != null && names.size() == 1) {
            for (String name : names) {
                myProfileName = name;
                break;
            }
        } else {
            super.onValueMissing();
        }
    }
    
    @Override
    public void check() {
        Set<String> names = null;
        ConsoleResources res = new ConsoleResources();
                    
        if (!isSpecified() &&
                getSpecifiedCheck() == CliOption.SpecifiedCheck.REQUIRED) {
            // L'opzione non e' specificata ma e' obbligatoria
            names = myConfigCtx.listProfiles();
            if (names != null && names.size() == 1) {
                // Il contesto di configurazione prevede un solo profilo:
                // Assumo che l'opzione sia stata specificata con l'unico
                // profilo di configurazione.
                setSpecified(true);
                for (String name : names) {
                    myProfileName = name;
                    break;
                }               
            }
        }
        
        super.check();
        if (!isSpecified()) {
            return;
        }
        
        if (names == null) {
            // Non ho ancora avuto occasione di leggere l'elenco dei profili di
            // configurazione previsti dal contesto
            names = myConfigCtx.listProfiles();
        }
        
        if (names == null || !names.contains(myProfileName)) {
            throw new CommandLineArgumentException(
                    res.getConfigInvalidException(getName(),
                    myConfigCtx.getName(), myProfileName));                               
        }       
    }        
    
    @Override
    public void writeUsage(Writer writer) {
        Set<String> names;   
        PrintWriter printer = null;
        
        super.writeUsage(writer);
        
        try {
            printer = new PrintWriter(writer);
            names = myConfigCtx.listProfiles();  
            printer.println(names);            
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }
    }  
}

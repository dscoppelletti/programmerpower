/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.config.spi;

import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.xml.XmlDefaultHandler;

/**
 * Collezione delle propriet&agrave; in un profilo di configurazione.
 */
final class ConfigProfileParser extends XmlDefaultHandler {
    private final String myCtxName;
    private final String myProfileName;
    private Boolean myCtxFound;
    private boolean myProfileFound;
    private String myCurrentProp;
    private Properties myProps;
    
    /**
     * Costruttore.
     * 
     * @param ctxName     Nome del contesto.
     * @param profileName Nome del profilo.
     */
    ConfigProfileParser(String ctxName, String profileName) {
        if (StringUtils.isBlank(ctxName)) {
            throw new ArgumentNullException("ctxName");
        }        
        if (StringUtils.isBlank(profileName)) {
            throw new ArgumentNullException("profileName");
        }

        myCtxName = ctxName;
        myProfileName = profileName;
        setEntityResolver(
                AbstractFileConfigContextProvider.getEntityResolver());
    }    
    
    /**
     * Restituisce le propriet&agrave; rilevate.
     * 
     * @return Collezione. Se il profilo non &egrave; rilevato, restituisce
     *         {@code null}.
     */
    Properties getProperties() {
        return myProps;
    }
    
    /**
     * Apertura del documento.
     */
    @Override
    public void startDocument() throws SAXException {
        myCtxFound = false;
        myProfileFound = false;
        myCurrentProp = null;
        myProps = null;
    }  
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {
        switch (localName) {
        case "context":
        case "group":
            startElementContext(attrs);  
            break;
            
        case "profile":
        case "config":
            startElementProfile(attrs);
            break;
            
        case "entry":
            startElementProperty(attrs);
            break;
        }
    }    
    
    /**
     * Tag di chiusura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws
            SAXException {        
        switch (localName) {
        case "profile":
        case "config":
            endElementProfile();
            break;
            
        case "entry":
            endElementProperty();
            break;
        }
    }  
    
    /**
     * Tag di apertura di un elemento {@code <context>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementContext(Attributes attrs) {
        if (myCtxFound != null) {
            if (myCtxFound) {
                myCtxFound = null;
            } else {
                if (myCtxName.equals(attrs.getValue("name"))) {
                    myCtxFound = true;
                }                
            }
        }
    }
    
    /**
     * Tag di apertura di un elemento {@code <profile>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementProfile(Attributes attrs) {       
        if (myCtxFound == null || !myCtxFound.booleanValue()) {
            return;
        }   
        
        if (myProfileName.equals(attrs.getValue("name"))) {
            myProfileFound = true;
            myProps = new Properties();
        }      
    }    
    
    /**
     * Tag di chiusura di un elemento {@code <profile>}.
     */
    private void endElementProfile() {
        if (myCtxFound == null || !myCtxFound) {
            return;
        }   
        
        myProfileFound = false;
    }  
    
    /**
     * Tag di apertura di un elemento {@code <entry>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementProperty(Attributes attrs) {
        if (myProfileFound) {
            myCurrentProp = attrs.getValue("key");
            collectContent();
        }       
    }   
    
    /**
     * Tag di chiusura di un elemento {@code <entry>}.
     */
    private void endElementProperty() {
        if (myCurrentProp != null) {
            myProps.setProperty(myCurrentProp, getCollectedContent());            
            myCurrentProp = null;            
        }              
    }
}

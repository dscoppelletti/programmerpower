/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.config.spi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import javax.xml.parsers.SAXParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.SAXException;
import it.scoppelletti.programmerpower.io.IOOperationException;
import it.scoppelletti.programmerpower.io.IOTools;
import it.scoppelletti.programmerpower.xml.SchemaEntityResolver;
import it.scoppelletti.programmerpower.xml.XmlException;
import it.scoppelletti.programmerpower.xml.XmlTools;

/**
 * Classe di base per l&rsquo;implementazione di un sistema di configurazione
 * basato su file.
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/programmerpower/config/spi/config2.xsd}
 * @see it.scoppelletti.programmerpower.config.ConfigContext
 * @since 1.1.0
 */
public abstract class AbstractFileConfigContextProvider implements
        ConfigContextProvider {
    
    /**
     * Nome del file di configurazione inserito nel direttorio dei dati
     * condiviso e/o nel direttorio dei dati riservato all&rsquo;utente che
     * esegue il programma. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.programmerpower.io.SharedDataDirectory
     * @see it.scoppelletti.programmerpower.io.UserDataDirectory
     */
    public static final String CONFIG_FILE =
            "it-scoppelletti-programmerpower-config.xml";

    private static final Logger myLogger = LoggerFactory.getLogger(
            AbstractFileConfigContextProvider.class);
    
    /**
     * Costruttore.
     */
    protected AbstractFileConfigContextProvider() {
    }
    
    /**
     * Restituisce le propriet&agrave; di un profilo di configurazione.
     * 
     * @param  file        File di configurazione. Pu&ograve; essere
     *                     {@code null}.
     * @param  ctxName     Nome del contesto.
     * @param  profileName Nome del profilo.
     * @return             Collezione. Se il profilo non &egrave; rilevato,
     *                     restituisce {@code null}.
     */     
    protected final Properties getProperties(File file, String ctxName,
            String profileName) {
        InputStream in = null;        
        SAXParser saxParser;
        ConfigProfileParser handler;
        
        try {
            in = openResource(file);
            if (in == null) {
                return null;
            }
            
            saxParser = XmlTools.newSAXParser();
            handler = new ConfigProfileParser(ctxName, profileName);
            saxParser.parse(in, handler);
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } catch (SAXException ex) {
            throw new XmlException(ex);            
        } finally {
            in = IOTools.close(in);
        }        
        
        return handler.getProperties();
    }
    
    /**
     * Restituisce l&rsquo;elenco dei nomi dei profili di configurazione in un
     * contesto.
     *  
     * @param  file    File di configurazione.
     * @param  ctxName Nome del contesto.
     * @return         Collezione. Se il contesto non &egrave; rilevato,
     *                 restituisce {@code null}.
     */    
    protected final Set<String> listProfiles(File file, String ctxName) {
        InputStream in = null;        
        SAXParser saxParser;
        ConfigContextParser handler;
        
        try {
            in = openResource(file);
            if (in == null) {
                return null;
            }
            
            saxParser = XmlTools.newSAXParser();
            handler = new ConfigContextParser(ctxName);
            saxParser.parse(in, handler);
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } catch (SAXException ex) {
            throw new XmlException(ex);            
        } finally {
            in = IOTools.close(in);
        }        
        
        return handler.getProfiles();
    }
    
    /**
     * Apre il flusso di lettura di un file.
     * 
     * @param  file File.
     * @return      Flusso di lettura.
     */
    private InputStream openResource(File file) {
        InputStream in;
        
        if (file == null) {
            return null;
        }
        
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            myLogger.warn(String.format("Failed to open file %1$s.", file), ex);
            return null;
        }
        
        return in;
    }
    
    /**
     * Restituisce il gestore della risoluzione delle entit&agrave;.
     * 
     * @return Oggetto.
     */    
    static EntityResolver getEntityResolver() {
        return new SchemaEntityResolver("programmerpower/config/spi/config", 1,
                2);
    }
}

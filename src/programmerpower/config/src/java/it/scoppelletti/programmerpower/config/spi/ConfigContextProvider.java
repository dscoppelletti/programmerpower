/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.config.spi;

import java.util.Properties;
import java.util.Set;

/**
 * Interfaccia per l&rsquo;implementazione del sistema di configurazione.
 * 
 * <P>Le implementazioni del sistema di configurazione dovrebbero essere
 * thread-safe.</P>
 * 
 * @see   it.scoppelletti.programmerpower.config.ConfigContext
 * @since 1.1.0
 */
public interface ConfigContextProvider {

    /**
     * Propriet&agrave; di sistema
     * {@code it.scoppelletti.programmerpower.config.spi.ConfigContextProvider}:
     * Nome della classe che implementa l&rsquo;interfaccia
     * {@code ConfigContextProvider}.
     * 
     * @it.scoppelletti.tag.default {@code it.scoppelletti.programmerpower.config.spi.FileConfigContextProvider}
     */
    public static final String PROP_SPI =
            "it.scoppelletti.programmerpower.config.spi.ConfigContextProvider";
    
    /**
     * Restituisce le propriet&agrave; di un profilo di configurazione.
     * 
     * @param  ctxName     Nome del contesto.
     * @param  profileName Nome del profilo.
     * @param  mode        Modalit&agrave; di accesso.
     * @return             Collezione. Se il profilo non &egrave; rilevato,
     *                     restituisce {@code null}.
     * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_SHARED
     * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_USER
     * @see <A
     * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
     * TARGET="_top">Modalit&agrave; di accesso</A> 
     */
    Properties getProperties(String ctxName, String profileName, int mode);
    
    /**
     * Restituisce l&rsquo;elenco dei nomi dei profili di configurazione in un
     * contesto.
     *  
     * @param  ctxName Nome del contesto.
     * @param  mode    Modalit&agrave; di accesso.
     * @return         Collezione. Se il contesto non &egrave; rilevato,
     *                 restituisce {@code null}.  
     * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_SHARED
     * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_USER
     * @see <A
     * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
     * TARGET="_top">Modalit&agrave; di accesso</A>                           
     */
    Set<String> listProfiles(String ctxName, int mode);
}

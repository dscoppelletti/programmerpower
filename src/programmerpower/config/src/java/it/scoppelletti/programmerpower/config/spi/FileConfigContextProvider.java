/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.config.spi;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.config.ConfigContext;
import it.scoppelletti.programmerpower.io.SharedDataDirectory;
import it.scoppelletti.programmerpower.io.UserDataDirectory;

/**
 * Implementazione di default del sistema di configurazione.
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/programmerpower/config/spi/config2.xsd}
 * @see it.scoppelletti.programmerpower.config.ConfigContext
 * @see <A
 *      HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idDefault"
 *      TARGET="_top">Implementazione del sistema di configurazione</A>
 * @since 1.1.0
 */
public final class FileConfigContextProvider extends
        AbstractFileConfigContextProvider {
    private static final Logger myLogger = LoggerFactory.getLogger(
            FileConfigContextProvider.class);
    
    /**
     * Costruttore.
     */
    public FileConfigContextProvider() {
    }
    
    public Properties getProperties(String ctxName, String profileName,
            int mode) {
        String key, value;
        Object sharedValue;
        Properties sharedProps, userProps;
        
        if (StringUtils.isBlank(ctxName)) {
            throw new ArgumentNullException("ctxName");
        }
        if (StringUtils.isBlank(profileName)) {
            throw new ArgumentNullException("profileName");
        }
        
        if ((mode & ConfigContext.MODE_SHARED) == ConfigContext.MODE_SHARED) {
            sharedProps = getProperties(
                    FileConfigContextProvider.SharedConfigHolder.myFile,
                    ctxName, profileName);
        } else {
            sharedProps = null;
        }
        
        if ((mode & ConfigContext.MODE_USER) == ConfigContext.MODE_USER) {
            userProps = getProperties(
                    FileConfigContextProvider.UserConfigHolder.myFile, ctxName,
                    profileName);
        } else {
            userProps = null;
        }
        
        if (userProps == null) {
            return sharedProps; // Maybe null, too
        }
        if (sharedProps == null) {
            return userProps; // Maybe null, too
        }
        
        // Merge shared configuration and user configuration
        for (Map.Entry<Object, Object> entry : userProps.entrySet()) {
            key = Objects.toString(entry.getKey(), null);
            value = Objects.toString(entry.getValue(), null);
            sharedValue = sharedProps.setProperty(key, value);
            if (sharedValue != null) {
                myLogger.warn("Overriding value \"{}\" of property {} " +
                        "(shared configuration) with value \"{}\" "+
                        "(user configuration).", sharedValue, key, value);
            }     
            
            sharedProps.setProperty(key, value);
        }
        
        return sharedProps;
    }    
    
    public Set<String> listProfiles(String ctxName, int mode) {
        Set<String> dupls, sharedList, userList;
        
        if (StringUtils.isBlank(ctxName)) {
            throw new ArgumentNullException("ctxName");
        }
        
        if ((mode & ConfigContext.MODE_SHARED) == ConfigContext.MODE_SHARED) {
            sharedList = listProfiles(
                    FileConfigContextProvider.SharedConfigHolder.myFile,
                    ctxName);
        } else {
            sharedList = null;
        }
        
        if ((mode & ConfigContext.MODE_USER) == ConfigContext.MODE_USER) {
            userList = listProfiles(
                    FileConfigContextProvider.UserConfigHolder.myFile, ctxName);
        } else {
            userList = null;
        }
        
        if (userList == null) {
            return sharedList; // Maybe null, too
        }
        if (sharedList == null) {
            return userList; // Maybe null, too
        }        
        
        // Merge shared configuration and user configuration
        dupls = new HashSet<>(sharedList);
        dupls.retainAll(userList);
        
        if (!dupls.isEmpty()) {
            myLogger.warn("Duplicate configuration profiles in context {}: {}.",
                    ctxName, dupls);
        }
        
        sharedList.addAll(userList);
        return sharedList;
    }    
    
    /**
     * Inizializza il nome del file di configurazione condiviso.
     * 
     * @return Oggetto.
     */
    private static File initSharedFile() {
        File dir;
        
        dir = SharedDataDirectory.tryGet();
        if (dir == null) {
            return null;
        }
        
        return new File(dir, AbstractFileConfigContextProvider.CONFIG_FILE);
    }
    
    /**
     * Inizializza il nome del file di configurazione riservato
     * all&rsquo;utente che esegue il programma.
     * 
     * @return Oggetto.
     */
    private static File initUserFile() {
        File dir;
        
        dir = UserDataDirectory.tryGet(true);
        if (dir == null) {
            return null;
        }
        
        return new File(dir, AbstractFileConfigContextProvider.CONFIG_FILE);
    }
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */
    private static final class SharedConfigHolder {
        private static final File myFile =
                FileConfigContextProvider.initSharedFile();
    }     
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */
    private static final class UserConfigHolder {
        private static final File myFile =
                FileConfigContextProvider.initUserFile();
    }            
}

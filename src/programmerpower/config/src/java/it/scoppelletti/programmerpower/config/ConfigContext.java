/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.config;

import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.JVMTools;
import it.scoppelletti.programmerpower.config.spi.ConfigContextProvider;
import it.scoppelletti.programmerpower.config.spi.FileConfigContextProvider;
import it.scoppelletti.programmerpower.reflect.ReflectionException;

/**
 * Contesto di configurazione.
 * 
 * La classe {@code ConfigContext} consente di personalizzare il sistema di
 * configurazione impostando sulla propriet&agrave; di sistema  
 * {@code it.scoppelletti.programmerpower.config.spi.ConfigContextProvider} il
 * nome di una classe che implementa l&rsquo;interfaccia
 * {@code ConfigContextProvider}; se la propriet&agrave; di sistema non &egrave;
 * impostata, &egrave; utilizzata la classe
 * {@code FileConfigContextProvider}.</P>
 * 
 * @see   it.scoppelletti.programmerpower.config.spi.ConfigContextProvider
 * @see   it.scoppelletti.programmerpower.config.spi.FileConfigContextProvider
 * @since 1.1.0
 */
public final class ConfigContext {    
    
    /**
     * Modalit&agrave; di accesso condiviso. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #getMode
     * @see <A
     * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
     * TARGET="_top">Modalit&agrave; di accesso</A>
     */
    public static final int MODE_SHARED = 0x01;
    
    /**
     * Modalit&agrave; di accesso riservato all&rsquo;utente che esegue il
     * programma. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #getMode
     * @see <A
     * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
     * TARGET="_top">Modalit&agrave; di accesso</A> 
     */
    public static final int MODE_USER = 0x02;
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            ConfigContext.class);
    
    private final String myCtxName;
    private final int myMode;
    
    /**
     * Costruttore.
     * 
     * @param name Nome del contesto. 
     * @param mode Modalit&agrave; di accesso.
     */
    private ConfigContext(String name, int mode) {
        myCtxName = name;
        myMode = mode;
    }
    
    /**
     * Restituisce il nome del contesto.
     * 
     * @return Valore.
     */    
    public String getName() {
        return myCtxName;
    }
    
    /**
     * Restituisce la modalit&agrave; di accesso;.
     * 
     * @return Valore.
     * @see    #MODE_SHARED
     * @see    #MODE_USER
     * @see    <A
     * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
     * TARGET="_top">Modalit&agrave; di accesso</A>
     */   
    public int getMode() {
        return myMode;
    }
    
    /**
     * Restituisce le propriet&agrave; di un profilo di configurazione.
     * 
     * @param  profileName Nome del profilo.
     * @return             Collezione. Se il profilo non &egrave; rilevato,
     *                     restituisce {@code null}.
     */
    public Properties getProperties(String profileName) {
        Properties props;
        
        if (StringUtils.isBlank(profileName)) {
            throw new ArgumentNullException("profileName");
        }
        
        props = ProviderHolder.myProvider.getProperties(myCtxName, profileName,
                myMode);
        if (props == null) {
            myLogger.warn("Configuration profile {} not found in context {}.",
                    profileName, myCtxName);
        } else if (props.isEmpty()) {
            myLogger.warn("No properties found for configuration profile " +
                    "{} in context {}.", profileName, myCtxName);            
        }
        
        return props;
    }
     
    /**
     * Restituisce l&rsquo;elenco dei nomi dei profili di configurazione nel
     * contesto.
     *  
     * @return Collezione. Se il contesto non &egrave; rilevato, restituisce
     *         {@code null}.
     */
    public Set<String> listProfiles() {
        return ProviderHolder.myProvider.listProfiles(myCtxName, myMode);
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("name", myCtxName)
            .append("mode", myMode).toString();
    }
    
    /**
     * Inizializza il provider {@code ContextConfigProvider}.
     * 
     * @return Oggetto.
     */
    private static ConfigContextProvider initProvider() {
        String providerName;
        Class<?> providerClass;
        ConfigContextProvider provider;
        
        providerName = JVMTools.getProperty(ConfigContextProvider.PROP_SPI);
        if (StringUtils.isBlank(providerName)) {
            return new FileConfigContextProvider();
        }
        
        try {
            providerClass = Class.forName(providerName);
            provider = (ConfigContextProvider) providerClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        return provider;
    }
    
    /**
     * Oggetto di factory di un contesto di configurazione.
     * 
     * @since 1.1.0
     */
    public static final class Factory {
        private final String myName;
        private final int myMode;
        
        /**
         * Costruttore.
         * 
         * @param name Nome del contesto.
         * @param mode Modalit&agrave; di accesso.
         * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_SHARED
         * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_USER
         * @see <A
         * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
         * TARGET="_top">Modalit&agrave; di accesso</A>
         */
        public Factory(String name, int mode) {
            if (StringUtils.isBlank(name)) {
                throw new ArgumentNullException("name");
            }
            
            myName = name;
            myMode = mode;
        }
        
        /**
         * Costruttore.
         * 
         * @param clazz Classe della quale il nome canonico corrisponde al nome
         *              del contesto.
         * @param mode  Modalit&agrave; di accesso.
         * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_SHARED
         * @see it.scoppelletti.programmerpower.config.ConfigContext#MODE_USER
         * @see <A
         * HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idMode"
         * TARGET="_top">Modalit&agrave; di accesso</A>
         */
        public Factory(Class<?> clazz, int mode) {
            if (clazz == null) {
                throw new ArgumentNullException("clazz");
            }
            
            myName = clazz.getCanonicalName();
            if (StringUtils.isBlank(myName)) {
                throw new ArgumentNullException("clazz.getCanonicalName()");
            }
            
            myMode = mode;
        }
        
        /**
         * Istanzia un contesto di configurazione.
         * 
         * @return Oggetto.
         */
        public ConfigContext newConfigContext() {
            return new ConfigContext(myName, myMode);
        }
        
        /**
         * Rappresenta l&rsquo;oggetto con una stringa.
         * 
         * @return Stringa.
         */
        @Override
        public String toString() {
            return new ToStringBuilder(this)
                .append("name", myName)
                .append("mode", myMode).toString();
        }            
    }
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */
    private static final class ProviderHolder {
        private static final ConfigContextProvider myProvider =
                ConfigContext.initProvider();
    }        
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.config.spi;

import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.xml.XmlDefaultHandler;

/**
 * Elenco dei profili di configurazione in un contesto.
 */
final class ConfigContextParser extends XmlDefaultHandler {
    private final String myCtxName;
    private Boolean myCtxFound;    
    private Set<String> myProfiles;
    
    /**
     * Costruttore.
     * 
     * @param ctxName Nome del gruppo.
     */
    ConfigContextParser(String ctxName) {
        if (StringUtils.isBlank(ctxName)) {
            throw new ArgumentNullException("ctxName");
        }
        
        myCtxName = ctxName;     
        setEntityResolver(
                AbstractFileConfigContextProvider.getEntityResolver());
    }    
    
    /**
     * Restituisce i nomi dei profili di configurazione.
     * 
     * @return Collezione. Se il contesto di configurazione non &egrave;
     *         rilevato, restituisce {@code null}.
     */
    Set<String> getProfiles() {
        return myProfiles;
    }
    
    /**
     * Apertura del documento.
     */
    @Override
    public void startDocument() throws SAXException {
        myCtxFound = false;
        myProfiles = null;
    }  
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {
        switch (localName) {
        case "context":
        case "group":
            startElementContext(attrs);
            break;
            
        case "profile":
        case "config":
            startElementProfile(attrs); 
            break;
        }
    }    
        
    /**
     * Tag di apertura di un elemento {@code <context>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementContext(Attributes attrs) {
        if (myCtxFound != null) {
            if (myCtxFound) {
                myCtxFound = null;
            } else {
                if (myCtxName.equals(attrs.getValue("name"))) {
                    myCtxFound = true;
                    myProfiles = new TreeSet<String>();
                }                
            }
        }
    }
    
    /**
     * Tag di apertura di un elemento {@code <profile>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementProfile(Attributes attrs) {
        if (myCtxFound == null || !myCtxFound) {
            return;
        }
                
        myProfiles.add(attrs.getValue("name"));
    }        
}

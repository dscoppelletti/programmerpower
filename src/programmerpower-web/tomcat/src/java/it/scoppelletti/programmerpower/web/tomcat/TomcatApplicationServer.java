/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.tomcat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import it.scoppelletti.programmerpower.io.IOOperationException;
import it.scoppelletti.programmerpower.io.IOTools;
import it.scoppelletti.programmerpower.net.RestClient;
import it.scoppelletti.programmerpower.net.RestTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;
import it.scoppelletti.programmerpower.web.ApplicationServer;

/**
 * Implementazione dell&rsquo;interfaccia {@code ApplicationServer} per Apache
 * Tomcat.
 *
 * @since 1.0.0
 */
@Final
@Reserved
public class TomcatApplicationServer implements ApplicationServer {
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.tomcat.user}: Nome
     * dell&rsquo;utente per l&rsquo;accesso all&rsquo;application server.
     * 
     * <P>L&rsquo;utente deve essere definito tra gli utenti di Apache Tomcat
     * e gli deve essere stato assegnato il ruolo {@code manager-script}.</P>
     *  
     * @it.scoppelletti.tag.default {@code tomcat}
     * @see #setUserName
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_USERNAME =
            "it.scoppelletti.programmerpower.tomcat.user";
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.tomcat.pwd}: Password
     * dell&rsquo;utente per l&rsquo;accesso all&rsquo;application server.
     * 
     * @it.scoppelletti.tag.default {@code changeit}
     * @see #setPassword
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_PASSWORD =
            "it.scoppelletti.programmerpower.tomcat.pwd";
    
    private static final String PATH_LIST = "manager/text/list";
    private static final Logger myLogger = LoggerFactory.getLogger(
            TomcatApplicationServer.class);
    private String myUser;
    private String myPwd;
    
    @javax.annotation.Resource(name = RestClient.BEAN_NAME)
    private RestClient myRestClient;
    
    /**
     * Costruttore.
     */
    public TomcatApplicationServer() {
    }
    
    /**
     * Imposta il nome dell&rsquo;utente per l&rsquo;accesso
     * all&rsquo;application server.
     * 
     * <P>L&rsquo;utente deve essere definito tra gli utenti di Apache Tomcat
     * e gli deve essere stato assegnato il ruolo {@code manager-script}.</P>
     * 
     * @param value Valore.
     * @see         #setPassword
     */
    @Required
    public void setUserName(String value) {
        myUser = value;
    }
    
    /**
     * Imposta la password per l&rsquo;accesso all&rsquo;application server.
     * 
     * @param value Valore.
     * @see         #setUser
     */
    @Required
    public void setPassword(String value) {
        myPwd = value;
    }
    
    public List<ApplicationServer.Application> listApplications() {
        Reference url;
        ClientResource clientRes = null;
        Representation resp = null;   
        List<ApplicationServer.Application> list;
        
        list = new ArrayList<>();
        
        url = new Reference(myRestClient.getServerSecureUri());
        RestTools.addPath(url, TomcatApplicationServer.PATH_LIST);
 
        try {
            clientRes = new ClientResource(url); 
            clientRes.setChallengeResponse(ChallengeScheme.HTTP_BASIC,
                    myUser, myPwd);
            resp = clientRes.get();
            load(list, url, resp);
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } finally {              
            resp = RestTools.release(resp);
            clientRes = RestTools.release(clientRes);
        }
        
        return list;
    }
    
    /**
     * Legge l&rsquo;elenco delle applicazioni installatate.
     * 
     * @param list Collezione.
     * @param url  URL della richiesta.
     * @param resp Risposta.
     */
    @SuppressWarnings("resource")
    private void load(List<ApplicationServer.Application> list,
            Reference url, Representation resp) throws IOException {
        int count;
        String line;
        TomcatApplication item;
        InputStream in = null;
        Reader reader = null;
        BufferedReader lineReader = null; 
        
        try {
            in = resp.getStream();
            if (in == null) {
                myLogger.warn("GET {} return EmptyRepresentation.", url);
                return;
            }
                       
            reader = new InputStreamReader(in);
            in = null;
            lineReader = new BufferedReader(reader);
            reader = null;           
            
            count = 0;
            line = lineReader.readLine();
            while (line != null) {
                count++;
                if (count == 1) {
                    if (line.startsWith(HttpStatus.OK.getReasonPhrase())) {
                        myLogger.trace(line);
                    } else {
                        throw new ResourceException(
                                Status.SERVER_ERROR_INTERNAL, line);
                    }
                } else {
                    try {
                        item = new TomcatApplication(line);
                        list.add(item);
                    } catch (RuntimeException ex) {
                        myLogger.error(String.format(
                                "Failed to add line #%1$d: %2$s", count, line),
                                ex);
                    }
                }
                
                line = lineReader.readLine();
            }
        } finally {
            in = IOTools.close(in);
            reader = IOTools.close(reader);
            lineReader = IOTools.close(lineReader);               
        }
    }
}

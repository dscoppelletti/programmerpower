/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.tomcat;

import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.PropertyNotSetException;
import it.scoppelletti.programmerpower.web.ApplicationServer;

/**
 * Applicazione.
 */
final class TomcatApplication implements ApplicationServer.Application {
    private static final int FIELD_NAME = 3;
    private static final int FIELD_PATH = 0;
    private static final int FIELD_STATUS = 1;
    private static final String SEP = ":";
    private static final String STATUS_RUNNING = "running";
    private final String myName;
    private final String myPath;
    private final boolean myRunning;
    private final String myLine;
    
    /**
     * Costruttore.
     * 
     * @param line Rappresentazione dell&rsquo;oggetto come stringa.
     */
    TomcatApplication(String line) {
        int n;
        String[] fields;
        
        if (StringUtils.isBlank(line)) {
            throw new ArgumentNullException("line");
        }
        
        myLine = line;
        fields = myLine.split(TomcatApplication.SEP);
        n = fields.length;
        
        if (TomcatApplication.FIELD_NAME < n) {
            myName = fields[TomcatApplication.FIELD_NAME];
        } else {
            throw new PropertyNotSetException(
                    TomcatApplication.class.getName(), "name");
        }
        
        if (TomcatApplication.FIELD_PATH < n) {
            myPath = fields[TomcatApplication.FIELD_PATH];
        } else {
            throw new PropertyNotSetException(
                    TomcatApplication.class.getName(), "contextPath");                
        }
        
        if (TomcatApplication.FIELD_STATUS < n) {
            myRunning = TomcatApplication.STATUS_RUNNING.equalsIgnoreCase(
                    fields[TomcatApplication.FIELD_STATUS]);
        } else {
            throw new PropertyNotSetException(
                    TomcatApplication.class.getName(), "running");                 
        }
    }
    
    public String getName() {
        return myName;
    }
    
    public String getContextPath() {
        return myPath;
    }

    public boolean isRunning() {
        return myRunning;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return myLine;
    }
}

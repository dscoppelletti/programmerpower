/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring.config;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.ManagedMap;
import org.springframework.beans.factory.xml.AbstractBeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.access.expression.ExpressionBasedFilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.xml.DomUtils;
import org.w3c.dom.Element;
import it.scoppelletti.programmerpower.web.security.CompositeDecisionManagerContributor;

/**
 * Parser dell&rsquo;elemento <CODE>&gt;security-filter&lt;</CODE>.
 */
final class SecurityFilterBeanDefinitionParser extends
        AbstractBeanDefinitionParser {
    private static final Logger myLogger = LoggerFactory.getLogger(
            SecurityFilterBeanDefinitionParser.class);
    
    /**
     * Costruttore.
     */
    SecurityFilterBeanDefinitionParser() {        
    }
    
    /**
     * Indica se l&rsquo;id&#46; del bean deve essere generato in assenza di
     * quello impostato esplicitamente.
     * 
     * @return {@code true}
     */
    @Override
    protected boolean shouldGenerateIdAsFallback() {
        return true;
    }
    
    /**
     * Implementazione del parser.
     * 
     * @param  el        Elemento XML.
     * @param  parserCtx Contesto del parser.
     * @return           Definizione del bean.
     */
    @Override
    protected AbstractBeanDefinition parseInternal(Element el,
            ParserContext parserCtx) {
        BeanDefinitionBuilder builder;
        
        builder = BeanDefinitionBuilder.rootBeanDefinition(
                CompositeDecisionManagerContributor.class);
        builder.setScope(BeanDefinition.SCOPE_PROTOTYPE);
        builder.addPropertyValue("targetName",
                el.getAttribute("access-decision-manager-ref"));
        builder.addPropertyValue("securityMetadataSource",
                createSecurityMetadataSource(el));        
                
        return builder.getBeanDefinition();
    }
        
    /**
     * Restituisce la definizione del componente 
     * {@code FilterInvocationSecurityMetadataSource}.
     * 
     * @param  el Elemento XML.
     * @return    Oggetto.
     */
    private BeanDefinition createSecurityMetadataSource(Element el) {
        BeanDefinitionBuilder builder;
        
        builder = BeanDefinitionBuilder.rootBeanDefinition(
                ExpressionBasedFilterInvocationSecurityMetadataSource.class);
        builder.addConstructorArgValue(parseInterceptors(el));
        builder.addConstructorArgReference(
                el.getAttribute("expression-handler-ref"));
        
        return builder.getBeanDefinition();
    }
    
    /**
     * Restituisce la corrispondenza tra gli URL e le configurazioni di accesso
     * richieste.
     * 
     * @param  filterEl Definizione XML del filtro.
     * @return          Collezione.
     */
    private Map<BeanDefinition, BeanDefinition> parseInterceptors(
            Element filterEl) {
        String method, pattern;
        BeanDefinition keyBean;
        BeanDefinitionBuilder exprBuilder, keyBuilder;
        List<Element> interceptList;
        Map<BeanDefinition, BeanDefinition> interceptMap;
        
        interceptList = DomUtils.getChildElementsByTagName(filterEl,
                "intercept-url");
        interceptMap = new ManagedMap<BeanDefinition, BeanDefinition>();
        for (Element interceptEl : interceptList) {            
            keyBuilder = BeanDefinitionBuilder.rootBeanDefinition(
                    AntPathRequestMatcher.class);
            
            pattern = interceptEl.getAttribute("pattern"); 
            keyBuilder.addConstructorArgValue(pattern.toLowerCase());
            
            method = interceptEl.getAttribute("method");
            if (!StringUtils.isBlank(method)) {
                keyBuilder.addConstructorArgValue(method);
            }            
                        
            keyBean = keyBuilder.getBeanDefinition();
                
            exprBuilder = BeanDefinitionBuilder.rootBeanDefinition(
                    SecurityConfig.class);
            exprBuilder.addConstructorArgValue(
                    interceptEl.getAttribute("access"));
            exprBuilder.setFactoryMethod("createList");
              
            if (interceptMap.put(keyBean, exprBuilder.getBeanDefinition()) !=
                null) {
                myLogger.warn("URL {} defined more than once: only last used.",
                        pattern);
            }                       
        }    
        
        return interceptMap;
    }
}

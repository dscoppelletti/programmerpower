/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring.config;

import java.util.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Contributo alla configurazione di un componente
 * {@code CompositeRequestMatcher} che riconosce un insieme di
 * <ACRONYM TITLE="Uniform Resource Locator">URL</ACRONYM>.
 * 
 * <P>Il componente {@code CompositeRequestMatcherContributor} consente di
 * inserire nella registrazione di un bean un insieme dei modelli di URL
 * che devono essere riconosciuti da un particolare componente 
 * {@code CompositeRequestMatcher}.<BR>
 * Se un componente {@code CompositeRequestMatcher} deve verificare se un URL
 * &egrave; incluso nell&rsquo;insieme di quelli che deve riconoscere, rileva i
 * bean {@code CompositeRequestMatcherContributor} registrati nel contesto
 * dell&rsquo;applicazione e collegati al componente stesso attraverso la
 * propriet&agrave; {@link #setTargetName targetName}; in questo modo ogni
 * modulo che costituisce un&rsquo;applicazione pu&ograve;
 * <I>contribuire</I> alla configurazione del componente.</P> 
 *
 * @see   it.scoppelletti.programmerpower.web.spring.config.CompositeRequestMatcher
 * @see   <A HREF="${it.scoppelletti.token.springSecurityUrl}"
 *        TARGET="_top">Spring Security</A>      
 * @since 1.0.0
 */
@Final
public class CompositeRequestMatcherContributor {
    private String myTargetName;
    private List<String> myPatterns;
    
    /**
     * Costruttore.
     */
    public CompositeRequestMatcherContributor() {        
    }

    /**
     * Restituisce il nome del bean al quale il componente &egrave; collegato.
     * 
     * @return Valore.
     * @see    #setTargetName
     */
    public String getTargetName() {
        return myTargetName;
    }
    
    /**
     * Imposta il nome del bean al quale il componente &egrave; collegato.
     *  
     * @param value Valore.
     * @see         #getTargetName
     */
    @Required
    public void setTargetName(String value) {
        myTargetName = value;
    }

    /**
     * Restituisce la collezione dei modelli di URL.
     * 
     * @return Collezione.
     */
    public List<String> getPatterns() {
        return myPatterns;
    }
    
    /**
     * Imposta la collezione dei modelli di URL.
     * 
     * @param list Collezione.
     */
    @Required
    public void setPatterns(List<String> list) {
        if (list == null) {
            throw new ArgumentNullException("list");
        }
        
        myPatterns = list;
    }
}

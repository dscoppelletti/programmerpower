/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring.config;

import org.springframework.context.*;
import org.springframework.core.env.*;
import org.springframework.web.context.support.*;
import it.scoppelletti.programmerpower.data.*;
import it.scoppelletti.programmerpower.spring.config.*;

/**
 * Personalizzazione del contesto Spring di un&rsquo;applicazione Web.
 * 
 * <P>Il componente {@code WebApplicationContextInitializer} esegue le seguenti
 * personalizzazioni:</P>
 * 
 * <OL TYPE="1">
 * <LI>Aggiunge nell&rsquo;ambiente del contesto Spring i seguenti profili:
 *      <OL TYPE="a">
 *      <LI>Profili rilevati secondo quanto implementato dallo stesso ambiente
 *      del contesto Spring.
 *      <LI>Profilo {@code it-scoppelletti-web}.
 *      <LI>Profilo {@code it-scoppelletti-data}.
 *      </OL>
 * <LI>Inserisce come sorgenti delle propriet&agrave; di ambiente i seguenti
 * componenti:
 *      <OL TYPE="a">      
 *      <LI>File di propriet&agrave;
 *      {@code it-scoppelletti-programmerpower-beans.properties} rilevato
 *      attraverso il class-path. 
 *      <LI>Componente {@code WebPropertySource}. 
 *      </OL> 
 * </OL> 
 *
 * <P>Le applicazioni Web possono gestire la personalizzazione del contesto
 * Spring configurando il componente Spring {@code ContextLoaderListener};
 * questo componente pu&ograve; rilevare il nome della classe che implementa la
 * personalizzazione dal parametro {@code contextInitializerClasses} del
 * contesto dell&rsquo;applicazione impostato nel descrittore di deployment
 * {@code web.xml}.</P>
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;web-app ... &gt;
 *     ...
 *     &lt;context-param&gt;
 *         &lt;param-name&gt;contextInitializerClasses&lt;/param-name&gt;
 *         &lt;param-value&gt;it.scoppelletti.programmerpower.web.spring.config.WebApplicationContextInitializer&lt;/param-value&gt;
 *     &lt;context-param&gt;
 *     ...        
 *     &lt;listener&gt;
 *         &lt;listener-class&gt;org.springframework.web.context.ContextLoaderListener&lt;/listener-class&gt;
 *     &lt;/listener&gt;           
 *     ...     
 * &lt;/web-app&gt;
 * </PRE><BLOCKQUOTE> 
 * 
 * @see   it.scoppelletti.programmerpower.web.spring.config.WebPropertySource
 * @see   it.scoppelletti.programmerpower.web.spring.ApplicationContextListener
 * @see   it.scoppelletti.programmerpower.web.spring.ContextLoaderListenerEx 
 * @see   it.scoppelletti.programmerpower.spring.config.BeanConfigUtils#loadPropertySource 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
 *        TARGET="_top">Propriet&agrave; di ambiente</A> 
 * @since 1.0.0
 */
public final class WebApplicationContextInitializer implements
    ApplicationContextInitializer<AbstractRefreshableWebApplicationContext> {
    
    /**
     * Profilo aggiunto nell&rsquo;ambiente del contesto Spring. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String PROFILE = "it-scoppelletti-web";
        
    /**
     * Costruttore.
     */
    public WebApplicationContextInitializer() {        
    }
    
    /**
     * Inizializzazione.
     * 
     * @param applCtx Contesto dell&rsquo;applicazione.
     */
    public void initialize(AbstractRefreshableWebApplicationContext applCtx) {
        ConfigurableEnvironment env;
        PropertySource<?> propSource;
        MutablePropertySources propSources;
            
        env = applCtx.getEnvironment();
        
        // Acquisisco gli eventuali profili configurati prima di aggiungere
        // quelli di Programmer Power
        env.getActiveProfiles();
        
        env.addActiveProfile(WebApplicationContextInitializer.PROFILE);
        env.addActiveProfile(DataTools.PROFILE);
        
        propSources = env.getPropertySources();        
        
        propSource = BeanConfigTools.loadPropertySource();
        if (propSource != null) {
            propSources.addFirst(propSource);
        }
        
        propSources.addLast(new WebPropertySource(
                WebPropertySource.class.getName(), applCtx));        
    }
}


/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import javax.activation.MimetypesFileTypeMap;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.opensymphony.xwork2.util.ClassLoaderUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.RequestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;
import it.scoppelletti.programmerpower.io.IOTools;
import it.scoppelletti.programmerpower.reflect.Final;

/**
 * Filtro delle risorse incorporate.
 *
 * <P>Il filtro {@code EmbeddedResourceFilter} consente gestire alcune risorse
 * Web statiche come risorse rilevabili attraverso il class-path di
 * un&rsquo;applicazione Web; in questo modo le risorse statiche possono essere
 * non solo incluse come contenuto nelle applicazioni Web, ma anche
 * <DFN>incorporate</DFN> nei moduli <ACRONYM TITLE="Java ARchive">JAR</ACRONYM>
 * inseriti nel class-path delle applicazioni Web e quindi condivise tra
 * pi&ugrave; applicazioni come risorse <I>di libreria</I>.</P>
 * 
 * @since 1.0.0
 */
@Final
public class EmbeddedResourceFilter extends GenericFilterBean {
    
    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String BEAN_NAME =
            "it-scoppelletti-embeddedResourceFilter";
    
    /**
     * Suffisso applicato al nome dei file compressi. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String MINIFIED_SUFFIX = "-min";
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.resources.EmbeddedResourceFilter.resourceTimeout}:
     * Timeout di validit&agrave; (in giorni) delle risorse incorporate gestite
     * dal cache del browser.
     * 
     * @it.scoppelletti.tag.default {@code 1}
     * @see                         #setResourceTimeout
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_RESOURCETIMEOUT =
"it.scoppelletti.programmerpower.web.resources.EmbeddedResourceFilter.resourceTimeout";
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.resources.EmbeddedResourceFilter.compress}: 
     * Indica se deve essere utilizzata la versione compressa dele risorse
     * incorporate.
     * 
     * @it.scoppelletti.tag.default {@code true}
     * @see                         #setCompress
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A> 
     */
    public static final String PROP_COMPRESS =
"it.scoppelletti.programmerpower.web.resources.EmbeddedResourceFilter.compress";
    
    private static final char PATH_EXT = '.';
    private static final String PATH_SEP = "/";
    private static final String HEADER_CACHECONTROL = "Cache-Control";
    private static final String HEADER_DATE = "Date";
    private static final String HEADER_EXPIRES = "Expires";
    private static final String HEADER_LASTMODIFIED = "Last-Modified";
    private static final String HEADER_PRAGMA = "Pragma";    
    private static final String HEADER_RETRYAFTER = "Retry-After";    
    private static final String CACHE_PUBLIC = "public";
    private static final String CACHE_NOCACHE = "no-cache";
    private static final String EXPIRES_NOCACHE = "-1";    
    private static final Logger myLogger = LoggerFactory.getLogger(
            EmbeddedResourceFilter.class);
    private final long myLastModified;
    private int myTimeout;
    private boolean myCompress;
    
    @javax.annotation.Resource(name = IOTools.BEAN_MIMETYPEMAP)
    private MimetypesFileTypeMap myMimetypeMap;
    
    /**
     * Costruttore.
     */
    public EmbeddedResourceFilter() {
        Calendar cal = Calendar.getInstance();
        
        myLastModified = cal.getTimeInMillis();
        myTimeout = 1;
        myCompress = true;
    }
    
    /**
     * Imposta il timeout di validit&agrave; delle risorse rilevate dal cache
     * del browser.
     * 
     * @param                       value Valore (giorni). Se il valore &egrave;
     *                                    minore o uguale a {@code 0}, 
     *                                    l&rsquo;utilizzo del cache del browser
     *                                    &egrave; disattivato per le risorse
     *                                    statiche accedute attraverso il
     *                                    filtro.
     * @it.scoppelletti.tag.default       {@code 1}.
     */
    public void setResourceTimeout(int value) {
        myTimeout = value;
    }
        
    /**
     * Imposta l&rsquo;indicatore di utilizzo della versione compressa delle
     * risorse incorporate.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code true} 
     */
    public void setCompress(boolean value) {
        myCompress = value;
    }
    
    /**
     * Filtro di una richiesta.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     * @param chain Catena dei filtri.
     */
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {  
        if (!filter(req, resp)) {
            chain.doFilter(req, resp);
        }
    }
    
    /**
     * Filtro di una richiesta.
     * 
     * @param  req  Richiesta.
     * @param  resp Risposta.
     * @return      Indica se la richiesta &egrave; stata gestita.
     */    
    private boolean filter(ServletRequest req, ServletResponse resp) throws
            IOException {
        int pos;
        String minPath, path;
        StringBuilder buf;
        HttpServletRequest httpReq;
        HttpServletResponse httpResp;
        
        if (!(req instanceof HttpServletRequest)) {
            myLogger.warn("No HTTP request.");
            return false;
        }
        if (!(resp instanceof HttpServletResponse)) {
            myLogger.warn("No HTTP response.");
            return false;
        }
        
        httpReq = (HttpServletRequest) req;
        httpResp = (HttpServletResponse) resp;
        
        path = RequestUtils.getServletPath(httpReq);
        if (StringUtils.isBlank(path)) {
            path = httpReq.getPathInfo();
        }               
        if (StringUtils.isBlank(path)) {
            myLogger.warn("No path for request {}.", httpReq.getRequestURI());
            return false;
        }
        
        if (path.startsWith(EmbeddedResourceFilter.PATH_SEP)) {
            path = path.substring(EmbeddedResourceFilter.PATH_SEP.length());
            if (StringUtils.isBlank(path)) {
                myLogger.warn("No path for request {}.",
                        httpReq.getRequestURI());
                return false;
            }            
        }        
            
        if (myCompress) {
            buf = new StringBuilder(path);
            pos = path.lastIndexOf(EmbeddedResourceFilter.PATH_EXT);
            if (pos < 0) {
                buf.append(EmbeddedResourceFilter.MINIFIED_SUFFIX);
            } else {
                buf.insert(pos, EmbeddedResourceFilter.MINIFIED_SUFFIX);
            }
        
            minPath = buf.toString();    
            if (filter(minPath, httpResp)) {
                return true;
            }        
        
            myLogger.warn("Resource {} not found.", minPath);
        }
        
        if (!filter(path, httpResp)) {
            myLogger.warn("Resource {} not found.", path);
            return false;
        }          
        
        return true;
    }
    
    /**
     * Invio di una risorsa su una risposta.
     * 
     * @param  path Percorso.
     * @param  resp Risposta.
     * @return      Indica se la richiesta &egrave; stata gestita.   
     */    
    private boolean filter(String path, HttpServletResponse resp) throws
            IOException {
        long expires, now;
        String contentType;
        URL url;
        Calendar cal;
        InputStream in = null;
        
        url = ClassLoaderUtil.getResource(path, getClass());
        if (url == null) {           
            return false;
        }        
        
        try {
            in = url.openStream();
            contentType = myMimetypeMap.getContentType(path);
            
            if (StringUtils.isBlank(contentType)) {
                myLogger.warn("Content-type of {} is undefined.", path);
            } else {
                myLogger.debug("Content-type of {} is {}.", path, contentType);
                resp.setContentType(contentType);            
            }
            
            if (myTimeout > 0) {
                cal = Calendar.getInstance();
                now = cal.getTimeInMillis();
                cal.add(Calendar.DAY_OF_MONTH, myTimeout);
                expires = cal.getTimeInMillis();
                
                resp.setDateHeader(EmbeddedResourceFilter.HEADER_DATE, now);
                resp.setDateHeader(EmbeddedResourceFilter.HEADER_EXPIRES,
                        expires);
                resp.setDateHeader(EmbeddedResourceFilter.HEADER_RETRYAFTER,
                        expires);
                resp.setHeader(EmbeddedResourceFilter.HEADER_CACHECONTROL,
                        EmbeddedResourceFilter.CACHE_PUBLIC);
                resp.setDateHeader(EmbeddedResourceFilter.HEADER_LASTMODIFIED,
                        myLastModified);
            } else {
                resp.setHeader(EmbeddedResourceFilter.HEADER_CACHECONTROL,
                        EmbeddedResourceFilter.CACHE_NOCACHE);
                resp.setHeader(EmbeddedResourceFilter.HEADER_PRAGMA,
                        EmbeddedResourceFilter.CACHE_NOCACHE);
                resp.setHeader(EmbeddedResourceFilter.HEADER_EXPIRES,
                        EmbeddedResourceFilter.EXPIRES_NOCACHE);            
            }
            
            IOTools.copy(in, resp.getOutputStream());           
        } finally {
            in = IOTools.close(in);
        }
        
        return true;
    }
}


/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse Web.
 * 
 * @since 1.0.0
 */
public final class WebResources extends ResourceWrapper {
    
    /**
     * Costruttore.
     */
    public WebResources() {        
    }
       
    /**
     * Eccezione per tipo di media non supportato.
     * 
     * @return Testo.
     */
    public String getUnsupportedMediaTypeException() {
        return getString("UnsupportedMediaTypeException");
    }  
    
    /**
     * Eccezione per codice di stato inatteso.
     * 
     * @param  statusCode Codice di stato.
     * @param  reason     Descrizione.
     * @return            Testo.
     */
    public String getUnexpectedStatusCodeException(int statusCode,
            String reason) {
        return format("UnexpectedStatusCodeException", statusCode, reason);
    }  
    
    /**
     * Eccezione per risposta vuota.
     * 
     * @return Testo.
     */
    public String getEmptyResponseException() {
        return getString("EmptyResponseException");
    }  
    
    /**
     * Eccezione per risposta non valida.
     * 
     * @param  body Risposta.
     * @return      Testo.
     */
    public String getInvalidResponseException(String body) {
        return format("InvalidResponseException", body);
    }
}

/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.access.*;
import org.springframework.security.web.access.*;
import org.springframework.security.web.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Gestore delle eccezioni per accesso negato.
 * 
 * <P>La classe {@code RedirectAccessDeniedHandler} gestisce le eccezioni
 * {@code AccessDeniedException} con la redirezione ad un
 * {@linkplain #setTargetUrl indirizzo di destinazione}.<BR>
 * A differenza della classe Spring {@code AccessDeniedHandlerImpl} che esegue
 * sempre un reinoltro della richiesta ({@code forward}), la classe
 * {@code RedirectAccessDeniedHandler} consente di impostare la
 * {@linkplain #setRedirectStrategy modalit&agrave; di redirezione}; in
 * particolare la modalit&agrave; di redirezione di default esegue una
 * redirezione della risposta ({@code sendRedirect}).</P>
 * 
 * @since 1.0.0
 */
@Final
public class RedirectAccessDeniedHandler implements AccessDeniedHandler {
    private String myTargetUrl;
    private RedirectStrategy myRedirectStrategy;
        
    /**
     * Costruttore.
     */
    public RedirectAccessDeniedHandler() {
        myRedirectStrategy = new DefaultRedirectStrategy();
    }
    
    /**
     * Imposta l&rsquo;indirizzo di destinazione.
     * 
     * @param value Valore.
     */
    @Required
    public void setTargetUrl(String value) {
        myTargetUrl = value;
    }
    
    /**
     * Imposta la strategia per la redirezione.
     * 
     * @param                       obj Oggetto.
     * @it.scoppelletti.tag.default {@code org.springframework.security.web.DefaultRedirectStrategy}
     */
    public void setRedirectStrategy(RedirectStrategy obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myRedirectStrategy = obj;
    }
    
    /**
     * Esegue l&rsquo;operazione.
     * 
     * @param req  Richiesta.
     * @param resp Risposta.
     * @param ex   Eccezione.
     */
    public void handle(HttpServletRequest req, HttpServletResponse resp,
            AccessDeniedException ex) throws IOException,
            ServletException {
        if (StringUtils.isBlank(myTargetUrl)) {
            throw new PropertyNotSetException(toString(), "targetUrl");
        }
        
        myRedirectStrategy.sendRedirect(req, resp, myTargetUrl);
    }   
}

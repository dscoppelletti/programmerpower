/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web;

import java.net.URI;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.Protocol;
import org.restlet.data.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import it.scoppelletti.programmerpower.PropertyNotSetException;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code ApplicationServer}.
 * 
 * @since 1.1.0
 */
@Final
@Reserved
public class DefaultWebClient implements WebClient,
        InitializingBean {
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultWebClient.class);
    private int myHttpPort;
    private int myHttpsPort;
    private URI myUri;
    private URI mySecureUri;

    /**
     * Costruttore.
     */
    public DefaultWebClient() {
    }

    public void afterPropertiesSet() throws Exception {
        String name;
        UriComponents uri;
        Reference ref;
        
        uri = ServletUriComponentsBuilder.fromCurrentContextPath().build();
        name = uri.getHost();
        
        if (StringUtils.isBlank(name)) {
            myLogger.warn("Host name not defined.");
        } else {
            myLogger.trace("Host name: {}", name); 
            
            ref = new Reference(Protocol.HTTP, name, myHttpPort);
            myUri = ref.toUri();
            
            ref = new Reference(Protocol.HTTPS, name, myHttpsPort);
            mySecureUri = ref.toUri();            
        }        
    }

    /**
     * Imposta il numero di porta per il protocollo HTTP.
     * 
     * @param value Valore.
     */
    @Required
    public void setHttpPort(int value) {
        myHttpPort = value;
    }
    
    /**
     * Imposta il numero di porta per il protocollo HTTPS.
     * 
     * @param value Valore.
     */
    @Required
    public void setHttpsPort(int value) {
        myHttpsPort = value;
    }
    
    public URI getServerUri() {
        if (myUri == null) {
            throw new PropertyNotSetException(toString(), "uri");
        }
        
        return myUri;
    }
    
    public URI getServerSecureUri() {
        if (mySecureUri == null) {
            throw new PropertyNotSetException(toString(), "secureUri");
        }
        
        return mySecureUri;
    }
}

/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import it.scoppelletti.diagnostic.DiagnosticSystem;
import it.scoppelletti.programmerpower.AttributeMap;
import it.scoppelletti.programmerpower.Disposable;
import it.scoppelletti.programmerpower.threading.ThreadContext;
import it.scoppelletti.programmerpower.types.UUIDGenerator;
import it.scoppelletti.programmerpower.web.ApplicationServer;
import it.scoppelletti.programmerpower.web.WebTools;

/**
 * Contesto di un evento.
 */
final class EventContext implements Disposable {
    private static final Logger myLogger = LoggerFactory.getLogger(
            EventContext.class);    
        
    private ThreadContext myThreadCtx;
    private boolean myDisposed = false;
    
    /**
     * Costruttore.
     * 
     * @param threadName Nome del thread.
     * @param servletCtx Contesto dell&rsquo;applicazione.
     */
    EventContext(String threadName, ServletContext servletCtx) {
        UUID applInstanceId;
        ThreadContext threadCtx;
        AttributeMap servletCtxMap;
        
        threadCtx = ThreadContext.tryGetCurrentThreadContext();
        if (threadCtx != null) {
            // Evento gestito all'interno di una richiesta con contesto
            // del thread gia' assegnato
            myThreadCtx = null;
            return;
        }
        
        MDC.put(DiagnosticSystem.MDC_APPLICATIONIDENTITY,
                StringUtils.defaultIfBlank(servletCtx.getContextPath(),
                ApplicationServer.DEFAULT_CONTEXT_NAME));
        
        servletCtxMap = WebTools.getSynchronizedAttributeMap(servletCtx);
        applInstanceId = (UUID) servletCtxMap.getAttribute(
                WebTools.ATTR_APPLICATIONINSTANCEID);
        if (applInstanceId == null) {
            myLogger.warn("Servlet context attribute {} not set.",
                    WebTools.ATTR_APPLICATIONINSTANCEID);
            applInstanceId = UUIDGenerator.NIL;
        }
        MDC.put(DiagnosticSystem.MDC_APPLICATIONINSTANCEID,
                applInstanceId.toString());    
        
        myThreadCtx = new ThreadContext(threadName, applInstanceId);            
    }
    
    public void dispose() {
        if (myThreadCtx != null) {
            myThreadCtx.dispose();
            myThreadCtx = null;
            
            MDC.remove(DiagnosticSystem.MDC_APPLICATIONIDENTITY);
            MDC.remove(DiagnosticSystem.MDC_APPLICATIONINSTANCEID);
        }
        
        myDisposed = true;
    }
    
    public boolean isDisposed() {
        return myDisposed;
    }      
}

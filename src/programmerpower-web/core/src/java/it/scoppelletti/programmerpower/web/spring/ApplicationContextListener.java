/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.slf4j.*;
import org.springframework.context.*;
import org.springframework.web.context.support.*;
import it.scoppelletti.programmerpower.AttributeMap;
import it.scoppelletti.programmerpower.types.*;
import it.scoppelletti.programmerpower.web.*;

/**
 * Gestione del ciclo di vita di un&rsquo;applicazione Web.
 * 
 * <P>La classe {@code ApplicationContextListener} unifica la gestione del
 * ciclo di vita dei componenti dell&rsquo;applicazione:</P>
 * 
 * <OL TYPE="1">
 * <LI>La gestione del ciclo di vita del contesto Spring &egrave; delegato alla
 * classe {@code ContextLoaderListenerEx}.
 * <LI>All&rsquo;inizializzazione dell&rsquo;applicazione Web (metodo
 * {@link #contextInitialized}), sono rilevati i bean configurati nel contesto
 * Spring che implementano almeno una delle seguenti interfacce:
 *      <OL TYPE="a">
 *      <LI>{@code ServletContextListener}
 *      <LI>{@code HttpSessionListener}
 *      </OL>
 * <LI>La gestione di ogni evento del ciclo di vita dell&rsquo;applicazione Web
 * rilevato &egrave; delegato ai bean rilevati che implementano la specifica
 * interfaccia.
 * </OL>
 * 
 * <P>Il componente {@code ApplicationContextListener} rende quindi sufficiente
 * configurare un solo gestore di eventi nel descrittore di deployment
 * {@code web.xml}.</P>
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;web-app ... &gt;
 *     ...
 *     &lt;listener&gt;
 *         &lt;listener-class&gt;it.scoppelletti.programmerpower.web.spring.ApplicationContextListener&lt;/listener-class&gt;
 *     &lt;/listener&gt;           
 *     ...        
 * &lt;/web-app&gt;
 * </PRE><BLOCKQUOTE> 
 * 
 * @see   it.scoppelletti.programmerpower.web.spring.ContextLoaderListenerEx
 * @see   it.scoppelletti.programmerpower.web.spring.XmlWebApplicationContextEx
 * @since 1.0.0
 */
public final class ApplicationContextListener implements
        ServletContextListener, HttpSessionListener {
    
    /**
     * Nome del thread di inizializzazione del contesto dell&rsquo;applicazione
     * Web. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #contextInitialized
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     */
    public static final String THREAD_SERVLETCONTEXT_INIT =
        "servletContextInit";
    
    /**
     * Nome del thread di rilascio del contesto dell&rsquo;applicazione Web. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #contextDestroyed
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     */
    public static final String THREAD_SERVLETCONTEXT_DESTROY =
        "servletContextDestroy";
    
    /**
     * Nome del thread di inizializzazione di una sessione
     * dell&rsquo;applicazione Web. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #sessionCreated(HttpSessionEvent)
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     */
    public static final String THREAD_SESSION_CREATE =
        "sessionCreate";
    
    /**
     * Nome del thread di rilascio di una sessione dell&rsquo;applicazione Web.
     * Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #sessionDestroyed
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     */
    public static final String THREAD_SESSION_DESTROY =
        "sessionDestroy";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            ApplicationContextListener.class);    
    
    private final ServletContextListener mySpringSvc;
    private Map<String, ServletContextListener> myApplListeners;
    private Map<String, HttpSessionListener> mySessionListeners;
    
    /**
     * Costruttore.
     */
    public ApplicationContextListener() {                               
        mySpringSvc = new ContextLoaderListenerEx();        
    }
    
    /**
     * Inizializzazione di una&rsquo;applicazione Web.
     * 
     * @param event Evento.
     */
    public void contextInitialized(ServletContextEvent event) {
        UUID applInstanceId;
        ApplicationContext applCtx;
        ServletContext servletCtx = event.getServletContext();
        AttributeMap servletCtxMap;
        EventContext eventCtx = null;
        
        try {
            applInstanceId = UUIDGenerator.getInstance().newUUID();
        } catch (Exception ex) {            
            myLogger.error("Failed to extract UUID.", ex);
            applInstanceId = UUIDGenerator.NIL;
        }
        
        try {            
            servletCtxMap = WebTools.getSynchronizedAttributeMap(servletCtx);
            servletCtxMap.setAttribute(WebTools.ATTR_APPLICATIONINSTANCEID,
                    applInstanceId);            
            
            eventCtx = new EventContext(
                    ApplicationContextListener.THREAD_SERVLETCONTEXT_INIT,
                    servletCtx);
                        
            mySpringSvc.contextInitialized(event);            
            applCtx =
                WebApplicationContextUtils.getRequiredWebApplicationContext(
                servletCtx);
            myApplListeners = applCtx.getBeansOfType(
                    ServletContextListener.class, false, true);
            mySessionListeners = applCtx.getBeansOfType(
                    HttpSessionListener.class, false, true);
            
            for (Map.Entry<String, ServletContextListener> entry :
                myApplListeners.entrySet()) {
                myLogger.trace("Calling method contextInitialized of " +
                        "ServletContextListener {}.", entry.getKey());
                try {
                    entry.getValue().contextInitialized(event);
                } catch (Exception ex) {
                    myLogger.error(entry.getKey(), ex);
                }
            }                                              
        } finally {
            eventCtx = ObjectTools.dispose(eventCtx);
        }
    }
    
    /**
     * Termine di un&rsquo;applicazione Web.
     * 
     * @param event Evento.
     */    
    public void contextDestroyed(ServletContextEvent event) {
        EventContext eventCtx = null;
        
        try {
            eventCtx = new EventContext(
                    ApplicationContextListener.THREAD_SERVLETCONTEXT_DESTROY,
                    event.getServletContext());            

            if (myApplListeners != null) {
                for (Map.Entry<String, ServletContextListener> entry :
                    myApplListeners.entrySet()) {
                    myLogger.trace("Calling method contextDestroyed of " +
                            "ServletContextListener {}.", entry.getKey());
                    try {
                        entry.getValue().contextDestroyed(event);
                    } catch (Exception ex) {
                        myLogger.error(entry.getKey(), ex);
                    }       
                }
            }
            
            mySpringSvc.contextDestroyed(event);            
        } finally {
            eventCtx = ObjectTools.dispose(eventCtx);    
        }
    }

    /**
     * Inizializzazione di una sessione.
     * 
     * @param event Evento.
     */
    public void sessionCreated(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        EventContext eventCtx = null;
        
        if (mySessionListeners == null) {
            return;            
        }
        
        try {
            eventCtx = new EventContext(
                    ApplicationContextListener.THREAD_SESSION_CREATE,
                    session.getServletContext());
              
            for (Map.Entry<String, HttpSessionListener> entry :
                mySessionListeners.entrySet()) {
                myLogger.trace("Calling method sessionCreated({}) of " +
                        "HttpSessionListener {}.", session.getId(),
                        entry.getKey());
                try {
                    entry.getValue().sessionCreated(event);
                } catch (Exception ex) {
                    myLogger.error(entry.getKey(), ex);
                }
            }               
        } finally {
            eventCtx = ObjectTools.dispose(eventCtx);    
        }               
    }

    /**
     * Termine di una sessione.
     * 
     * @param event Evento.
     */
    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        EventContext eventCtx = null;
        
        if (mySessionListeners == null) {
            return;            
        }
        
        try {
            eventCtx = new EventContext(
                    ApplicationContextListener.THREAD_SESSION_DESTROY,
                    session.getServletContext());
            
            for (Map.Entry<String, HttpSessionListener> entry :
                mySessionListeners.entrySet()) {
                myLogger.trace("Calling method sessionDestroyed({}) of " +
                        "HttpSessionListener {}.", session.getId(),
                        entry.getKey());
                try {
                    entry.getValue().sessionDestroyed(event);
                } catch (Exception ex) {
                    myLogger.error(entry.getKey(), ex);
                }
            }    
        } finally {
            eventCtx = ObjectTools.dispose(eventCtx);                       
        }   
    }
}

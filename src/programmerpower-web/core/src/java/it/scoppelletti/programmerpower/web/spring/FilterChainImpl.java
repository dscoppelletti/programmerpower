/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.io.*;
import java.util.*;
import javax.servlet.*;

/**
 * Catena di filtri.
 */
final class FilterChainImpl implements FilterChain {
    private final List<Filter> myList;
    private final FilterChain myForward;
    private int myCursor;
    
    /**
     * Costruttore.
     * 
     * @param list    Lista di filtri.
     * @param forward Catena di filtri.
     */
    FilterChainImpl(List<Filter> list, FilterChain forward) {
        myList = list;
        myForward = forward;
        myCursor = 0;
    }
   
    /**
     * Filtro di una richiesta.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     */        
    public void doFilter(ServletRequest req, ServletResponse resp) throws
        IOException, ServletException {
        Filter current;
        
        if (myCursor < myList.size()) {
            current = myList.get(myCursor);
            myCursor++;
            current.doFilter(req, resp, this);               
        } else {
            myForward.doFilter(req, resp);
        }            
    }
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.config;

import java.io.File;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.config.spi.AbstractFileConfigContextProvider;
import it.scoppelletti.programmerpower.io.UserDataDirectory;

/**
 * Implementazione del sistema di configurazione per le applicazioni Web.
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/programmerpower/config/spi/config2.xsd}
 * @see it.scoppelletti.programmerpower.config.ConfigContext
 * @see <A
 *      HREF="${it.scoppelletti.token.referenceUrl}/setup/config.html#idWeb"
 *      TARGET="_top">Sistema di configurazione per le applicazioni Web</A>
 * @since 1.1.0
 */
public final class FileConfigContextProvider extends
        AbstractFileConfigContextProvider {

    /**
     * Costruttore.
     */
    public FileConfigContextProvider() {
    }
    
    public Properties getProperties(String ctxName, String profileName,
            int mode) {
        Properties props;
        
        if (StringUtils.isBlank(ctxName)) {
            throw new ArgumentNullException("ctxName");
        }
        if (StringUtils.isBlank(profileName)) {
            throw new ArgumentNullException("profileName");
        }
        
        props = getProperties(FileConfigContextProvider.UserConfigHolder.myFile,
                ctxName, profileName);
        
        return props;
    }    
    
    public Set<String> listProfiles(String ctxName, int mode) {
        Set<String> list;
        
        if (StringUtils.isBlank(ctxName)) {
            throw new ArgumentNullException("ctxName");
        }
        
        list = listProfiles(FileConfigContextProvider.UserConfigHolder.myFile,
                ctxName);

        return list;
    }    
        
    /**
     * Inizializza il nome del file di configurazione riservato
     * all&rsquo;utente che esegue il programma.
     * 
     * @return Oggetto.
     */
    private static File initUserFile() {
        File dir;
        
        dir = UserDataDirectory.tryGet(true);
        if (dir == null) {
            return null;
        }
        
        return new File(dir, AbstractFileConfigContextProvider.CONFIG_FILE);
    }
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */
    private static final class UserConfigHolder {
        private static final File myFile =
                FileConfigContextProvider.initUserFile();
    }            
}

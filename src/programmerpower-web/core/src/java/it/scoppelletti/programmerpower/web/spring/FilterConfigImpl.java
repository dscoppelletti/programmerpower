/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.util.*;
import javax.servlet.*;

/**
 * Configurazione di un filtro.
 */
final class FilterConfigImpl implements FilterConfig {
    private final String myName;
    private final ServletContext myServletCtx;
    private final Properties myProps;
    
    /**
     * Costruttore.
     * 
     * @param name       Nome del filtro.
     * @param servletCtx Contesto del servlet.
     * @param props      Parametri di inizializzazione.
     */
    FilterConfigImpl(String name, ServletContext servletCtx,
            Properties props) {
        myName = name;
        myServletCtx = servletCtx;
        myProps = (props != null) ? props : new Properties();            
    }
    
    /**
     * Restituisce il nome del filtro.
     * 
     * @return Valore.
     */
    public String getFilterName() {
        return myName;
    }
    
    /**
     * Restituisce il contesto del servlet.
     * 
     * @return Oggetto.
     */
    public ServletContext getServletContext() {
        return myServletCtx;
    }
    
    /**
     * Restituisce i nomi dei parametri.
     * 
     * @return Collezione.
     */
    @SuppressWarnings("unchecked")
    public Enumeration<String> getInitParameterNames() {
        return (Enumeration<String>) myProps.propertyNames();
    }
    
    /**
     * Restituisce il valore di un parametro.
     * 
     * @param  name Nome del parametro.
     * @return      Valore.
     */
    public String getInitParameter(String name) {
        return myProps.getProperty(name);
    }
}

/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Componente che implementa una catena di filtri registrabile come bean.
 * 
 * @since 1.0.0
 */
@Final
public class FilterChainBean implements Filter {
    private List<Filter> myChain;
    
    /**
     * Costruttore.
     */
    public FilterChainBean() {        
    }
    
    /**
     * Inizializzazione.
     * 
     * @param  filterConfig Configurazione.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     *         Il metodo non deve essere eseguito.
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        throw new NotImplementedException(getClass().getName(), "init");
    }
    
    /**
     * Rilascia le risorse.
     */
    public void destroy() {
    }      
    
    /**
     * Imposta la catena di filtri.
     * 
     * @param list Collezione.
     */
    @Required
    public void setChain(List<Filter> list) {
        myChain = list;
    }
    
    /**
     * Filtro di una richiesta.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     * @param chain Catena dei filtri.
     */
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {
        FilterChain filter;
        
        if (myChain == null) {
            throw new PropertyNotSetException(toString(), "chain");    
        }
        
        filter = new FilterChainImpl(myChain, chain);
        filter.doFilter(req, resp);        
    }
}

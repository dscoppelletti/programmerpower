/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;

/**
 * Controllore degli accessi.
 * 
 * <P>Il componente {@code CompositeAccessDecisionManager} estende le
 * funzionalit&agrave; del componente Spring {@code AffirmativeBased} con la
 * possibilit&agrave; di integrare la configurazione del controllo degli
 * accessi con i componenti {@code CompositeDecisionManagerContributor}
 * registrati come bean nel contesto dell&rsquo;applicazione.</P>
 *   
 * @see   it.scoppelletti.programmerpower.web.security.CompositeDecisionManagerContributor
 * @since 1.0.0 
 */
@Final
public class CompositeAccessDecisionManager extends AffirmativeBased
    implements BeanNameAware {
    private final Object mySyncRoot = new Object();
    private String myBeanName;
    private List<FilterInvocationSecurityMetadataSource>
        mySecurityMetadataSource;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     * 
     * @param decisionVoters Collezione delle implementazioni. 
     */
    public CompositeAccessDecisionManager(
            List<AccessDecisionVoter<?>> decisionVoters) {
        super(decisionVoters);
    }
    
    /**
     * Imposta il nome del bean.
     * 
     * @param value Valore.
     */
    @Reserved
    public void setBeanName(String value) {
        myBeanName = value;
    }
    
    /**
     * Applica il controllo degli accessi.
     * 
     * @param authentication   Stato dell&rsquo;autenticazione.
     * @param object           Componente chiamante.
     * @param configAttributes Configurazione di accesso da applicare.
     */
    @Override
    public void decide(Authentication authentication, Object object,
            Collection<ConfigAttribute> configAttributes) throws
            AccessDeniedException {
        Collection<ConfigAttribute> attrs;
        List<FilterInvocationSecurityMetadataSource> list;
                
        synchronized (mySyncRoot) {
            if (mySecurityMetadataSource == null) {
                mySecurityMetadataSource = CompositeDecisionManagerContributor
                    .listSecurityMetadataSource(myBeanName, myApplCtx);
            }
            
            // Uso una copia della collezione per rilasciare il lock appena
            // possibile
            list = new ArrayList<FilterInvocationSecurityMetadataSource>(
                    mySecurityMetadataSource);
        }
        
        for (FilterInvocationSecurityMetadataSource securityMds : list) {
            attrs = securityMds.getAttributes(object);
            if (attrs != null && !attrs.isEmpty()) {
                super.decide(authentication, object, attrs);
                return;
            }
        }
        
        super.decide(authentication, object, configAttributes);
    }
}

/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.util.*;
import org.springframework.web.context.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Componente che implementa un filtro registrabile come bean.
 * 
 * <P>Il componente Spring {@code FilterChainProxy} non esegue il metodo
 * {@code init} dell&rsquo;interfaccia {@code Filter} dei filtri della catena
 * indipendentemente dal parametro di inizializzazione 
 * {@code targetFilterLifecycle} del filtro {@code DelegatingFilterProxy}.<BR>
 * Il componente {@code FilterBean} fa invece da <I>wrapper</I> ad un oggetto
 * {@code Filter} garantendo l&rsquo;esecuzione del metodo di interfaccia
 * {@code init} all&rsquo;inizializzazione del bean oppure alla prima richiesta
 * filtrata a seconda della propriet&agrave; 
 * {@link #setDeferInit deferInit}.</P>
 *
 * @since 1.0.0
 */
@Final
public class FilterBean implements Filter, BeanNameAware, ServletContextAware,
        InitializingBean, DisposableBean {
    private String myName;
    private String myFilterClass;
    private boolean myInitialized = false;
    private boolean myDeferInit = false;
    private ServletContext myServletCtx;
    private Properties myProps = null;
    private Filter myFilter = null;
        
    /**
     * Costruttore.
     */
    public FilterBean() {        
    }
    
    /**
     * Inizializzazione.
     * 
     * @param  filterConfig Configurazione.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     *         Il metodo non deve essere eseguito.
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        throw new NotImplementedException(getClass().getName(), "init");
    }
    
    /**
     * Inizializzazione.
     */
    @Reserved
    public void afterPropertiesSet() throws Exception {
        Class<?> filterClass;
        
        if (StringUtils.isBlank(myFilterClass)) {
            throw new PropertyNotSetException(toString(), "filterClass");
        }
                
        try {
            // Il metodo statico forName della classe Class non rileva le classi
            // definite direttamente nel contesto dell'applicazione Web:
            // Utilizzo l'apposita funzione Spring.
            filterClass = ClassUtils.forName(myFilterClass,
                    ClassUtils.getDefaultClassLoader());
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }        
        
        myFilter = (Filter) filterClass.newInstance();
        if (!myDeferInit) {
            myFilter.init(new FilterConfigImpl(myName, myServletCtx, myProps));
            myInitialized = true;            
        }        
    }
    
    /**
     * Rilascia le risorse.
     */
    @Reserved
    public void destroy() {
        if (myInitialized && myFilter != null) {
            myFilter.destroy();
            myFilter = null;
        }
    }    
        
    /**
     * Imposta il nome del bean.
     * 
     * @param value Valore.
     */
    @Reserved
    public void setBeanName(String value) {
        myName = value;        
    }
    
    /**
     * Imposta il contesto dell&rsquo;applicazione Web.
     * 
     * @param obj Oggetto.
     */
    @Reserved
    public void setServletContext(ServletContext obj) {
        myServletCtx = obj;
    }
    
    /**
     * Imposta il nome della classe che implementa il filtro.
     * 
     * @param value Valore.
     */
    @Required
    public void setFilterClass(String value) {
        myFilterClass = value;
    }
   
    /**
     * Imposta l&rsquo;indicatore di inizializzazione rimandata alla prima
     * richiesta filtrata.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     */
    public void setDeferInit(boolean value) {
        myDeferInit = value;
    }
    
    /**
     * Imposta i parametri di inizializzazione.
     * 
     * @param props Collezione.
     */
    public void setInitParameters(Properties props) {
        myProps = props;
    }
    
    /**
     * Filtro di una richiesta.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     * @param chain Catena dei filtri.
     */
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {
        if (myFilter == null) {
            chain.doFilter(req, resp);
            return;
        }
        
        synchronized (myFilter) {
            if (!myInitialized) {
                myFilter.init(new FilterConfigImpl(myName, myServletCtx,
                        myProps));
                myInitialized = true;
            }
            
            myFilter.doFilter(req, resp, chain);
        }
    }
}

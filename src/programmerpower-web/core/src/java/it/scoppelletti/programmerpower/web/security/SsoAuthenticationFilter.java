/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.io.*;
import javax.servlet.http.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.cas.web.*;
import org.springframework.security.core.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Filtro di autenticazione <ACRONYM TITLE="Single Sign-On">SSO</ACRONYM>.
 *  
 * <P>Il filtro {@code SsoAuthenticationFilter} estende le funzionalit&agrave;
 * del filtro Spring Security {@code CasAuthenticationFilter} con la collezione
 * delle sessioni autenticate ai fini del Single Sign-Out (per questa funzione
 * specifica, <ACRONYM TITLE="Central Authentication Service">CAS</ACRONYM>
 * prevederebbe un filtro separato {@code SingleSignOutFilter}).</P>  
 * 
 * @see   it.scoppelletti.programmerpower.web.security.SingleSignOutFilter
 * @see   it.scoppelletti.programmerpower.web.security.SingleSignOutSessionListener
 * @since 1.0.0
 */
@Final
public class SsoAuthenticationFilter extends CasAuthenticationFilter {
    private CasClient myCasClient;
    
    /**
     * Costruttore.
     */
    public SsoAuthenticationFilter() {        
    }
    
    /**
     * Imposta il client CAS.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setCasClient(CasClient obj) {
        myCasClient = obj;
    }
    
    /**
     * Implementazione dell&rsquo;autenticazione.
     * 
     * @param  req  Richiesta.
     * @param  resp Risposta.
     * @return      Token autenticato. Se l&rsquo;autenticazione non avviene,
     *              restituisce {@code null}.
     */
    @Override
    public Authentication attemptAuthentication(final HttpServletRequest req,
            final HttpServletResponse resp) throws AuthenticationException,
            IOException {
        String ticket;
        Authentication result;
        
        if (myCasClient == null) {
            throw new PropertyNotSetException(toString(), "casClient");
        }
                
        result = super.attemptAuthentication(req, resp);
        if (result == null) {
            return null;
        }
          
        ticket = (String) result.getCredentials();        
        myCasClient.addAuthenticatedSession(ticket, req.getSession(true));

        return result;
    }
}

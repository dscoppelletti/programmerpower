/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring.config;

import javax.servlet.*;
import org.springframework.core.env.*;
import org.springframework.web.context.*;

/**
 * Sorgente per la sostituzione dei riferimenti alle propriet&agrave; nelle
 * risorse di definizione dei bean.
 * 
 * <P>Il componente {@code WebPropertySource} implementa alcune propriet&agrave;
 * specifiche di un&rsquo;applicazione Web Servlet.</P>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Propriet&agrave;</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>
 * <TR>
 *      <TD>{@code it.scoppelletti.param.contextPath}</TD>
 *      <TD>Valore della propriet&agrave; {@code contextPath} dell&rsquo;oggetto
 *      {@code ServletContext}.</TD>      
 * </TR> 
 * </TBODY>
 * </TABLE></P> 
 * 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
 *        TARGET="_top">Propriet&agrave; di ambiente</A> 
 * @since 1.0.0
 */
public final class WebPropertySource extends
        PropertySource<WebApplicationContext> {
    
    /**
     * Nome della propriet&agrave; corrispondente alla propriet&agrave;
     * {@code contextPath} dell&rsquo;oggetto {@code ServletContext}. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String PROP_CONTEXTPATH =
        "it.scoppelletti.param.contextPath";
    
    /**
     * Costruttore.
     * 
     * @param name    Nome del componente.
     * @param applCtx Contesto dell&rsquo;applicazione.
     */
    public WebPropertySource(String name, WebApplicationContext applCtx) {
        super(name, applCtx);
    }

    /**
     * Restituisce il valore di una propriet&agrave;
     * 
     * @param  name Nome della propriet&agrave;.
     * @return      Valore. Se la propriet&agrave; non &egrave; impostata,
     *              restituisce {@code null}.
     */
    @Override
    public Object getProperty(String name) {        
        ServletContext servletCtx;
        
        if (WebPropertySource.PROP_CONTEXTPATH.equals(name)) {
            servletCtx = getSource().getServletContext();
            if (servletCtx == null) {
                logger.warn("Servlet content not set.");
                return null;
            }
            
            return getContextPath(servletCtx);
        }
        
        return null;
    }
    
    /**
     * Restituisce il percorso del contesto dell&rsquo;applicazione Web.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione Web.
     * @return            Valore.
     */
    private String getContextPath(ServletContext servletCtx) {
        String value = servletCtx.getContextPath();
        
        if (value == null) {
            return null;
        }
        
        if (value.startsWith("/")) {
            return value.substring(1);
        }
        
        return value;
    }    
}


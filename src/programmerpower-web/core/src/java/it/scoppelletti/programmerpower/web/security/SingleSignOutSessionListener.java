/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import javax.servlet.http.*;
import org.jasig.cas.client.session.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Gestore delle sessioni.
 * 
 * <P>Il gestore {@code SingleSignOutSessionListener} si occupa di rimuovere
 * le sessioni invalidate dalla collezione delle sessioni autenticate mantenuta
 * ai fini del Single Sign-Out.</P>
 * 
 * @see   it.scoppelletti.programmerpower.web.security.SingleSignOutFilter
 * @see   it.scoppelletti.programmerpower.web.security.SsoAuthenticationFilter
 * @see   it.scoppelletti.programmerpower.web.security.SsoAuthenticationService
 * @see   it.scoppelletti.programmerpower.web.security.SsoRememberMeServices 
 * @since 1.0.0
 */
@Final
public class SingleSignOutSessionListener implements HttpSessionListener {
    private SessionMappingStorage mySessionStorage;
    
    /**
     * Costruttore.
     */
    public SingleSignOutSessionListener() {        
    }
    
    /**
     * Imposta la collezione delle sessioni di autenticazione.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setSessionMappingStorage(SessionMappingStorage obj) {
        mySessionStorage = obj;
    }
        
    /**
     * Inizializzazione di una sessione.
     * 
     * @param event Evento.
     */
    public void sessionCreated(HttpSessionEvent event) {
    }

    /**
     * Termine di una sessione.
     * 
     * @param event Evento.
     */
    public void sessionDestroyed(HttpSessionEvent event) {
        if (mySessionStorage == null) {
            throw new PropertyNotSetException(toString(),
                    "sessionMappingStorage");
        }

        try {
            mySessionStorage.removeBySessionById(event.getSession().getId());
        } catch (Exception ex) {
            // NOP
        }
    }        
}

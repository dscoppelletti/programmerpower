/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web;

import javax.servlet.*;
import javax.servlet.http.*;
import it.scoppelletti.diagnostic.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.threading.*;

/**
 * Funzioni di utilit&agrave; Web.
 * 
 * @since 1.1.0
 */
public final class WebTools {
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.cookie.domain}: Dominio per il
     * quale registrare i cookie (ad esempio, {@code .scoppelletti.it}).
     * 
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A>
     */
    public static final String PROP_COOKIE_DOMAIN =
        "it.scoppelletti.programmerpower.web.cookie.domain";
    
    /**
     * Attributo di applicazione
     * {@code it.scoppelletti.diagnostic.ApplicationInstanceId}: UUID 
     * dell&rsquo;istanza dell&rsquo;applicazione.
     */
    public static final String ATTR_APPLICATIONINSTANCEID =
            DiagnosticSystem.MDC_APPLICATIONINSTANCEID;
    
    /**
     * Attributo di sessione
     * {@code it.scoppelletti.diagnostic.ApplicationInstanceId}: UUID 
     * della sessione dell&rsquo;applicazione.
     */
    public static final String ATTR_SESSIONID =
            DiagnosticSystem.MDC_APPLICATIONINSTANCEID;
    
    /**
     * Costruttore privato per classe statica.
     */
    private WebTools() {        
    }
    
    /**
     * Restituisce una collezione per l&rsquo;accesso sincronizzato agli
     * attributi del contesto di un&rsquo;applicazione.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @return            Collezione.
     */
    public static AttributeMap getSynchronizedAttributeMap(
            ServletContext servletCtx) {
        return new SynchronizedAttributeMap(
                new ServletContextAttributeMap(servletCtx));
    }
    
    /**
     * Restituisce una collezione per l&rsquo;accesso sincronizzato agli
     * attributi di una sessione.
     * 
     * @param  session Sessione.
     * @return         Collezione.
     */
    public static AttributeMap getSynchronizedAttributeMap(
            HttpSession session) {
        return new SynchronizedAttributeMap(
                new HttpSessionAttributeMap(session));
    }
}

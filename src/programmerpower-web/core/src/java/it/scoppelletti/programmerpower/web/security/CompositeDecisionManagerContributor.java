/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.ApplicationContext;
import org.springframework.security.web.access.intercept.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Contributo alla configurazione di un bean di controllo.
 * 
 * <P>Il componente {@code CompositeDecisionManagerContributor} consente di
 * inserire nella registrazione di un bean un frammento della configurazione
 * degli attributi per i controlli di sicurezza sull&rsquo;accesso alle 
 * risorse.<BR>
 * Un componente che applica uno specifico controllo di sicurezza pu&ograve;
 * rilevare i bean {@code CompositeDecisionManagerContributor} registrati nel
 * contesto dell&rsquo;applicazione e collegati al componente stesso attraverso
 * la propriet&agrave; {@link #setTargetName targetName}; in questo modo ogni
 * modulo che costituisce un&rsquo;applicazione pu&ograve; <I>contribuire</I>
 * alla configurazione dei componenti per il controllo della sicurezza definiti
 * dall&rsquo;applicazione stessa.</P> 
 *
 * @since 1.0.0
 */
@Final
public class CompositeDecisionManagerContributor {
    private static final Logger myLogger = LoggerFactory.getLogger(
            CompositeDecisionManagerContributor.class);
    
    private String myTargetName;
    private FilterInvocationSecurityMetadataSource mySecurityMetadataSource;
    
    /**
     * Costruttore.
     */
    public CompositeDecisionManagerContributor() {        
    }

    /**
     * Restituisce il nome del bean al quale il componente &egrave; collegato.
     * 
     * @return Valore.
     * @see    #setTargetName
     */
    public String getTargetName() {
        return myTargetName;
    }
    
    /**
     * Imposta il nome del bean al quale il componente &egrave; collegato.
     *  
     * @param value Valore.
     * @see         #getTargetName
     */
    @Required
    public void setTargetName(String value) {
        myTargetName = value;
    }
    
    /**
     * Restituisce la configurazione del controllo degli accessi.
     * 
     * @return Oggetto.
     * @see    #setSecurityMetadataSource
     */
    public FilterInvocationSecurityMetadataSource getSecurityMetadataSource() {
        return mySecurityMetadataSource;        
    }
    
    /**
     * Imposta la configurazione del controllo degli accessi.
     * 
     * @param obj Oggetto.
     * @see       #getSecurityMetadataSource
     */
    @Required
    public void setSecurityMetadataSource(
            FilterInvocationSecurityMetadataSource obj) {
        mySecurityMetadataSource = obj;
    }
    
    /**
     * Restituisce la lista dei configurazioni di attributi di controllo che
     * devono essere considerate da un componente di controllo.
     * 
     * @param  beanName Nome del bean.
     * @param  applCtx  Contesto dell&rsquo;applicazione.
     * @return          Collezione.
     */
    static List<FilterInvocationSecurityMetadataSource>
        listSecurityMetadataSource(String beanName,
        ApplicationContext applCtx) {
        String targetName;
        FilterInvocationSecurityMetadataSource securityMds;
        List<FilterInvocationSecurityMetadataSource> list;
        Map<String, CompositeDecisionManagerContributor> contributorMap;

        if (StringUtils.isBlank(beanName)) {
            throw new ArgumentNullException("beanName");
        }
        if (applCtx == null) {
            throw new ArgumentNullException("applCtx");
        }
        
        list = new ArrayList<FilterInvocationSecurityMetadataSource>();
        
        myLogger.trace("Searching for CompositeDecisionManagerContributor " +
                "beans with targetName={}.", beanName);                
        contributorMap = applCtx.getBeansOfType(
                CompositeDecisionManagerContributor.class, true, true);
        if (contributorMap == null || contributorMap.isEmpty()) {
            myLogger.warn("No CompositeDecisionManagerContributor bean found.");
            return list;
        }
        
        for (Map.Entry<String, CompositeDecisionManagerContributor> entry :
            contributorMap.entrySet()) {
            targetName = entry.getValue().getTargetName();
            if (StringUtils.isBlank(targetName)) {
                myLogger.warn(
                        "Invalid CompositeDecisionManagerContributor bean.",
                        new PropertyNotSetException(entry.getKey(),
                        "targetName"));
                continue;                
            }
            if (!beanName.equals(targetName)) {
                continue;
            }
            
            myLogger.trace("Found CompositeDecisionManagerContributor bean {}" +
            		"for bean {}.", entry.getKey(), beanName);
            
            securityMds = entry.getValue().getSecurityMetadataSource();
            if (securityMds == null) {
                myLogger.warn(
                        "Invalid CompositeDecisionManagerContributor bean.",                        
                        new PropertyNotSetException(entry.getKey(),
                        "securityMetadataSource"));
                continue;
            }
                        
            list.add(securityMds);
        }
        if (list.isEmpty()) {
            myLogger.warn("SecurityMetadataSource list is empty for bean {}.",
                    beanName);            
        }
        
        return list;
    }    
}

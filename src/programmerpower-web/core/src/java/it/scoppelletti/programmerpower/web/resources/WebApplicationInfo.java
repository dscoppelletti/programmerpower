/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.io.IOTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.resources.ApplicationInfo;
import it.scoppelletti.programmerpower.resources.ResourceTools;
import it.scoppelletti.programmerpower.web.ApplicationServer;

/**
 * Informazioni su un&rsquo;applicazione Web (nome, numero di versione, avviso
 * di copyright, etc).
 * 
 * @since 1.0.0
 */
@Final
public class WebApplicationInfo implements ApplicationInfo {
    private static final long serialVersionUID = 1L;

    /**
     * Parametro di inizializzazione dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.version}:
     * Numero di versione dell&rsquo;applicazione.
     */
    public static final String PARAM_VERSION =
"it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.version";
    
    /**
     * Parametro di inizializzazione dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.pageurl}:
     * URL della guida dell&rsquo;applicazione.
     */
    public static final String PARAM_PAGEURL =
"it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.pageurl";
    
    /**
     * Parametro di inizializzazione dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.copyright}:
     * Avviso di copyright.
     */
    public static final String PARAM_COPYRIGHT =
"it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.copyright";
    
    /**
     * Parametro di inizializzazione dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.licenseResource}:
     * Locazione della risorsa (eventualmente localizzata) dalla quale leggere
     * la nota di licenza.
     */
    public static final String PARAM_LICENSE =
"it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.licenseResource";
        
    private static final Logger myLogger = LoggerFactory.getLogger(
            WebApplicationInfo.class);
        
    /**
     * @serial Nome dell&rsquo;applicazione.
     */
    private final String myApplName;
    
    /**
     * @serial Numero di versione dell&rsquo;applicazione.
     */
    private final String myVersion;
    
    /**
     * @serial URL della guida dell&rsquo;applicazione.
     */
    private final String myPageUrl;
    
    /**
     * @serial Avviso di copyright.
     */
    private final String myCopyright;
    
    /**
     * @serial Nota di licenza.
     */
    private final String myLicense;
        
    /**
     * Costruttore.
     * 
     * @param servletCtx Contesto dell&rsquo;applicazione.
     */
    public WebApplicationInfo(ServletContext servletCtx) {
        String value;
        Manifest mf;
        Attributes attrs;
        
        if (servletCtx == null) {
            throw new ArgumentNullException("servletCtx");
        }

        mf = loadManifest(servletCtx);
        attrs = (mf != null) ? mf.getMainAttributes() : null;
        
        myApplName = initApplName(servletCtx, attrs);       
        myVersion = initVersion(servletCtx, attrs);
            
        value = servletCtx.getInitParameter(WebApplicationInfo.PARAM_COPYRIGHT);
        myCopyright = StringUtils.isBlank(value) ? null : value;
        
        myLicense = initLicense(servletCtx.getInitParameter(
                WebApplicationInfo.PARAM_LICENSE));
        
        value = servletCtx.getInitParameter(WebApplicationInfo.PARAM_PAGEURL);
        myPageUrl = StringUtils.isBlank(value) ? null : value;               
    }
            
    /**
     * Restituisce il nome dell&rsquo;applicazione.
     *
     * <P>Questa implementazione del metodo {@code getApplicationName} rileva
     * il nome dell&rsquo;applicazione dalle seguenti alternative:</P>
     *  
     * <OL>
     * <LI>Elemento {@code <deploy-name>} del descrittore di deployment
     * {@code web.xml}.
     * <LI>Attributo {@code Specification-Title} del manifesto.
     * <LI>Attributo {@code Implementation-Title} del manifesto.
     * <LI>Percorso del contesto dell&rsquo;applicazione.
     * <LI>Costante {@code "ROOT"}.
     * </OL>
     *  
     * @return Valore.         
     */
    public String getApplicationName() {
        return myApplName;        
    }    
    
    /**
     * Inizializza il nome dell&rsquo;applicazione.
     *
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @param  attrs      Attributi del manifesto.
     * @return            Valore.
     */
    private String initApplName(ServletContext servletCtx, Attributes attrs) {
        String value;
 
        value = servletCtx.getServletContextName();
        if (!StringUtils.isBlank(value)) {
            return value;
        }
        
        if (attrs != null) {
            value = attrs.getValue(Attributes.Name.SPECIFICATION_TITLE);
            if (!StringUtils.isBlank(value)) {
                return value;
            }
            
            value = attrs.getValue(Attributes.Name.IMPLEMENTATION_TITLE);
            if (!StringUtils.isBlank(value)) {
                return value;
            }
        }
        
        value = servletCtx.getContextPath();
        if (!StringUtils.isBlank(value)) {
            return value;
        }
        
        return ApplicationServer.DEFAULT_CONTEXT_NAME;
    }
     
    /**
     * Restituisce il numero di versione dell&rsquo;applicazione.
     *
     * <P>Questa implementazione del metodo {@code getVersion} rileva il numero
     * di versione dalle seguenti alternative:</P>
     * 
     * <OL>
     * <LI>Parametro di inizializzazione dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.version}
     * impostato con un elemento {@code <context-param>} nel descrittore di
     * deployment {@code web.xml}.
     * <LI>Attributo {@code Implementation-Version} del manifesto.
     * <LI>Attributo {@code Specification-Version} del manifesto. 
     * </OL> 
     *  
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}.                                                         
     */    
    public String getVersion() {
        return myVersion;
    }
    
    /**
     * Inizializza il numero di versione dell&rsquo;applicazione.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @param  attrs      Attributi del manifesto.
     * @return            Valore.
     */    
    private String initVersion(ServletContext servletCtx, Attributes attrs) {
        String value;
        
        value = servletCtx.getInitParameter(WebApplicationInfo.PARAM_VERSION);
        if (!StringUtils.isBlank(value)) {
            return value;
        }
        
        if (attrs != null) {
            value = attrs.getValue(Attributes.Name.IMPLEMENTATION_VERSION);
            if (!StringUtils.isBlank(value)) {
                return value;
            }
            
            value = attrs.getValue(Attributes.Name.SPECIFICATION_VERSION);
            if (!StringUtils.isBlank(value)) {
                return value;
            }        
        }
        
        return null;
    }
    
    /**
     * Restituisce l&rsquo;avviso di copyright.
     *
     * <P>Questa implementazione del metodo {@code getCopyright} rileva
     * l&rsquo;avviso di copyright dal parametro di inizializzazione
     * dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.copyright}
     * impostato con un elemento {@code <context-param>} nel descrittore di
     * deployment {@code web.xml}.</P>
     * 
     * @return Testo HTML. Se il testo non &egrave; rilevato, restituisce
     *         {@code null}.                   
     */
    public String getCopyright() {
        return myCopyright;
    }
    
    /**
     * Restituisce la nota di licenza.
     *
     * <P>Questa implementazione del metodo {@code getLicense} rileva la nota di
     * licenza dal parametro di inizializzazione dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.licenseResource}
     * impostato con un elemento {@code <context-param>} nel descrittore di
     * deployment {@code web.xml}; questo parametro deve essere impostato con la
     * locazione della risorsa (eventualmente localizzata) dalla quale leggere
     * la nota di licenza (ad esempio,
     * {@code it/scoppelletti/programmerpower/web/resources/asl2.html}).</P>
     *    
     * @return Testo HTML. Se il testo non &egrave; rilevato, restituisce
     *         {@code null}.                  
     */
    public String getLicense() {
        return myLicense;
    }
       
    /**
     * Inizializza la nota di licenza.
     * 
     * @param  location Locazione della risorsa non localizzata.
     * @return          Testo.
     */
    private String initLicense(String location) {
        String text;
        
        if (StringUtils.isBlank(location)) {
            return null;
        }
        
        text = ResourceTools.getResourceAsString(location, null);
        if (!StringUtils.isBlank(text)) {
            return text;
        }     
        
        return null;
    }
    
    /**
     * Restituisce l&rsquo;URL della guida dell&rsquo;applicazione.
     * 
     * <P>Questa implementazione del metodo {@code getPageUrl} rileva 
     * l&rsquo;URL della guida dal parametro di inizializzazione
     * dell&rsquo;applicazione
     * {@code it.scoppelletti.programmerpower.web.resources.WebApplicationInfo.pageurl}
     * impostato con un elemento {@code <context-param>} nel descrittore di
     * deployment {@code web.xml}.</P>
     *  
     * @return Valore. Se il valore non &egrave; rilevato, restituisce
     *         {@code null}. 
     */
    public String getPageUrl() {
        return myPageUrl;
    }
        
    /**
     * Legge il manifesto dell&rsquo;applicazione.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @return            Oggetto.
     */
    private Manifest loadManifest(ServletContext servletCtx) {
        InputStream in = null;
        Manifest mf;
        
        in = servletCtx.getResourceAsStream("/META-INF/MANIFEST.MF");
        if (in == null) {
            myLogger.warn("MANIFEST.MF not found for application {}.",
                    StringUtils.defaultIfBlank(servletCtx.getContextPath(),
                    ApplicationServer.DEFAULT_CONTEXT_NAME));
            return null;
        }
        
        try {
            mf = new Manifest(in);
        } catch (IOException ex) {
            myLogger.error(String.format("Failed to load MANIFEST.MF for " +
                    "application %1$s.", StringUtils.defaultIfBlank(
                    servletCtx.getContextPath(),
                    ApplicationServer.DEFAULT_CONTEXT_NAME)), ex);
            return null;                    
        } finally {
            in = IOTools.close(in);                
        }
        
        return mf;        
    }
}

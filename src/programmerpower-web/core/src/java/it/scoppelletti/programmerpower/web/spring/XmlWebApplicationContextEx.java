/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.spring;

import java.io.*;
import org.springframework.beans.factory.xml.*;
import org.springframework.web.context.support.*;

/**
 * Contesto Spring di un&rsquo;applicazione Web.
 *
 * <P>La classe {@code XmlWebApplicationContextEx} personalizza il rilevamento
 * delle risorse di configurazione del contesto dell&rsquo;applicazione Spring
 * rispetto alla classe di base Spring {@code XmlWebApplicationContext}.<BR>
 * In particolare, un oggetto {@code XmlWebApplicationContextEx} inizializza il
 * contesto Spring rilevando le seguenti risorse attraverso il class-path:</P>  
 * 
 * <OL>
 * <LI>La risorsa
 * {@code /it/scoppelletti/programmerpower/web/applicationContext.xml}.
 * <LI>La risorsa  {@code /WEB-INF/it-scoppelletti-applicationContext.xml}
 * specifica dell&rsquo;applicazione.
 * <LI>Tutte le risorse {@code /META-INF/it-scoppelletti-moduleContext.xml}
 * rilevate attraverso il class-path.
 * </OL>
 *
 * <P>Le applicazioni Web possono gestire il ciclo di vita del contesto Spring
 * configurando il componente Spring {@code ContextLoaderListener}; questo
 * componente pu&ograve; rilevare il nome della classe che implementa il
 * contesto Spring dal parametro {@code contextClass} del contesto
 * dell&rsquo;applicazione impostato nel descrittore di deployment
 * {@code web.xml}.</P>
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;web-app ... &gt;
 *     ...
 *     &lt;context-param&gt;
 *         &lt;param-name&gt;contextClass&lt;/param-name&gt;
 *         &lt;param-value&gt;it.scoppelletti.programmerpower.web.spring.XmlWebApplicationContextEx&lt;/param-value&gt;
 *     &lt;context-param&gt;
 *     ...        
 *     &lt;listener&gt;
 *         &lt;listener-class&gt;org.springframework.web.context.ContextLoaderListener&lt;/listener-class&gt;
 *     &lt;/listener&gt;           
 *     ...     
 * &lt;/web-app&gt;
 * </PRE><BLOCKQUOTE>    
 * 
 * @see   it.scoppelletti.programmerpower.web.spring.ApplicationContextListener
 * @see   it.scoppelletti.programmerpower.web.spring.ContextLoaderListenerEx
 * @since 1.0.0
 */
public final class XmlWebApplicationContextEx extends XmlWebApplicationContext {
    
    /**
     * Nome della risorsa che contribuisce alla configurazione del contesto
     * Spring di un&rsquo;applicazione Web. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String WEB_CONTEXT =
        "classpath:/it/scoppelletti/programmerpower/web/applicationContext.xml";
    
    /**
     * Nome della risorsa in un&rsquo;applicazione Web che contribuisce alla
     * configurazione del contesto Spring. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String APPL_CONTEXT =
        "/WEB-INF/it-scoppelletti-applicationContext.xml";
    
    /**
     * Costruttore.
     */
    public XmlWebApplicationContextEx() {        
    }

    /**
     * Carica le definizioni dei bean.
     * 
     * @param reader Flusso di lettura.
     */
    @Override
    protected void loadBeanDefinitions(XmlBeanDefinitionReader reader) throws
            IOException {        
        reader.loadBeanDefinitions(XmlWebApplicationContextEx.WEB_CONTEXT);
        super.loadBeanDefinitions(reader);
    }
    
    /**
     * Restituisce il nome di default della risorsa del contesto.
     * 
     * @return valore.
     */
    @Override
    protected String[] getDefaultConfigLocations() {
        return new String[] { XmlWebApplicationContextEx.APPL_CONTEXT };
    }       
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web;

import javax.servlet.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.threading.*;

/**
 * Collezione degli attributi del contesto di un&rsquo;applicazione Web.
 */
final class ServletContextAttributeMap implements
        SynchronizedAttributeMap.Source {
    private final ServletContext myServletCtx;
    
    /**
     * Costruttore.
     * 
     * @param servletCtx Contesto dell&rsquo;applicazione.
     */
    ServletContextAttributeMap(ServletContext servletCtx) {
        if (servletCtx == null) {
            throw new ArgumentNullException("servletCtx");
        }
        
        myServletCtx = servletCtx;
    }

    public Object getSyncRoot() {
        return myServletCtx;
    }
    
    public Object getAttribute(String name) {
        return myServletCtx.getAttribute(name);
    }

    public void setAttribute(String name, Object value) {
        myServletCtx.setAttribute(name, value);
    }

    public void removeAttribute(String name) {
        myServletCtx.removeAttribute(name);
    }    
}

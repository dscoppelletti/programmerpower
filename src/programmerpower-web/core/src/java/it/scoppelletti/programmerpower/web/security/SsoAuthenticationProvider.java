/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import org.jasig.cas.client.validation.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.security.authentication.*;
import org.springframework.security.cas.*;
import org.springframework.security.cas.authentication.*;
import org.springframework.security.core.*;
import org.springframework.security.core.userdetails.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Authenticazione CAS
 * <ACRONYM TITLE="Central Authentication Service">CAS</ACRONYM>.
 * 
 * @since 1.1.0
 */
@Final
public class SsoAuthenticationProvider implements AuthenticationProvider {
    
    /**
     * Nome del bean che implementa l&rsquo;autenticazione CAS. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_AUTHENTICATIONPROVIDER = 
            "it-scoppelletti-casProvider";
    
    private AuthenticationUserDetailsService<CasAssertionAuthenticationToken>
        myUserDetailsService;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     */
    public SsoAuthenticationProvider() {
    }
    
    /**
     * Imposta il servizio di lettura dei dati degli utenti.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setUserDetailsService(
            AuthenticationUserDetailsService<CasAssertionAuthenticationToken>
            obj) {
        myUserDetailsService = obj;
    }
    
    /**
     * Esegue l&rsquo;autenticazione.
     * 
     * @param  authentication Token da autenticare.
     * @return                Token autenticato.
     */
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        AuthenticationProvider provider;
        
        provider = myApplCtx.getBean(
                SsoAuthenticationProvider.BEAN_AUTHENTICATIONPROVIDER,
                AuthenticationProvider.class);
        
        return provider.authenticate(authentication);
    }

    /**
     * Verifica se il componente supporta una classe di autenticazione.
     * 
     * @param  authentication Classe di autenticazione.
     * @return                Esito della verifica.
     */
    public boolean supports(Class<?> authentication) {        
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(
                authentication)) ||
                CasAuthenticationToken.class.isAssignableFrom(authentication) ||
                CasAssertionAuthenticationToken.class.isAssignableFrom(
                        authentication);
    }
    
    /**
     * Istanzia le propriet&agrave; del servizio CAS per la richiesta
     * corrente.
     * 
     * @return Oggetto.
     */
    public ServiceProperties createServiceProperties() {
        ServiceProperties props;
        SsoServiceProperties serviceProps;
        
        serviceProps = myApplCtx.getBean(SsoServiceProperties.BEAN_NAME,
                SsoServiceProperties.class);
        
        props = new ServiceProperties();
        props.setService(serviceProps.getService());
        props.setSendRenew(false);
        
        return props;
    }
    
    /**
     * Istanzia il provider dell&rsquo;autenticazione per la richiesta
     * corrente.
     * 
     * @return Oggetto.
     */
    public CasAuthenticationProvider createAuthenticationProvider() {
        TicketValidator ticketValidator;
        CasAuthenticationProvider provider;
        SsoServiceProperties serviceProps;
        
        if (myUserDetailsService == null) {
            throw new PropertyNotSetException(toString(), "userDetailsService");
        }
        
        provider = new CasAuthenticationProvider();
        provider.setAuthenticationUserDetailsService(myUserDetailsService);
        
        serviceProps = myApplCtx.getBean(SsoServiceProperties.BEAN_NAME,
                SsoServiceProperties.class);
        
        ticketValidator = new Cas20ServiceTicketValidator(
                serviceProps.getServerUri().toString());
        provider.setTicketValidator(ticketValidator);
        
        // Serve per verificare che un token sia stato autenticato da questo
        // provider:
        // Assumo di avere un solo provider per applicazione e quindi posso
        // usare come chiave il servizio corrispondente all'applicazione.
        provider.setKey(serviceProps.getService());
        
        return provider;
    }
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.net.URI;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.Reference;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;
import it.scoppelletti.programmerpower.PropertyNotSetException;
import it.scoppelletti.programmerpower.net.RestClient;
import it.scoppelletti.programmerpower.net.RestTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;

/**
 * Implementazione di default dell&rsquo;interfaccia
 * {@code SsoServiceProperties}.
 * 
 * @since 1.1.0
 */
@Final
@Reserved
public class CasServiceProperties implements SsoServiceProperties,
        ServletContextAware, InitializingBean {
    
    /**
     * Percorso del server CAS. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #getServerUri
     */
    public static final String PATH = "cas";
    
    private URI myServerUri;
    private String myService;
    private ServletContext myServletCtx;
    
    @javax.annotation.Resource(name = RestClient.BEAN_NAME)
    private RestClient myRestClient;
    
    /**
     * Costruttore.
     */
    public CasServiceProperties() {
    }

    /**
     * Inizializzazione.
     */
    public void afterPropertiesSet() throws Exception {
        Reference ref;
        
        if (myServletCtx == null) {
            throw new PropertyNotSetException(toString(), "servletContext");
        }
        
        ref = new Reference(myRestClient.getServerSecureUri());

        if (myServerUri == null) {
            RestTools.addPath(ref, CasServiceProperties.PATH);
            myServerUri = ref.toUri();
        }
        
        RestTools.setPath(ref, myServletCtx.getContextPath());
        RestTools.addPath(ref, SingleSignOutFilter.DEF_FILTERPROCESSESURL);
        myService = ref.toString();
    }
    
    /**
     * Imposta il contesto dell&rsquo;applicazione Web.
     * 
     * @param obj Oggetto.
     */
    public void setServletContext(ServletContext obj) {  
        myServletCtx = obj;
    }
    
    public URI getServerUri() {
        return myServerUri;
    }

    /**
     * Imposta l&rsquo;URL del server CAS. 
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Determinato dall&rsquo;URL HTTPS
     *                                    dell&rsquo;AS.
     * @see #getServerUri              
     * @see it.scoppelletti.programmerpower.net.RestClient#getServerSecureUri          
     */
    public void setServerUrl(String value) {
        if (StringUtils.isBlank(value)) {
            myServerUri = null;
        } else {
            myServerUri = URI.create(value);
        }
    }
    
    public String getService() {
        return myService;
    }
}

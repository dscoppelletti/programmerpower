package it.scoppelletti.programmerpower.web.spring;

import java.util.*;
import javax.servlet.*;
import org.springframework.context.*;
import org.springframework.web.context.*;
import it.scoppelletti.programmerpower.web.spring.config.*;

/**
 * Gestione del ciclo di vita del contesto Spring di un&rsquo;applicazione Web.
 *
 * <P>Il componente {@code ContextLoaderListenerEx} personalizza la gestione del
 * ciclo di vita del contesto Spring rispetto alla classe di base Spring
 * {@code ContextLoaderListener}:</P>
 * 
 * <OL>
 * <LI>Il contesto Spring dell&rsquo;applicazione &egrave; implementato dalla
 * classe {@code XmlWebApplicationContextEx} indipendentemente
 * dall&rsquo;eventuale parametro {@code contextClass} del contesto
 * dell&rsquo;applicazione Web impostato nel descrittore di deployment
 * {@code web.xml}.
 * <LI>Il contesto Spring dell&rsquo;applicazione &egrave; personalizzato dal
 * componente {@code WebApplicationContextInitializer} indipendentemente
 * dall&rsquo;eventuale parametro {@code contextInitializerClasses} del contesto
 * dell&rsquo;applicazione Web impostato nel descrittore di deployment
 * {@code web.xml}. 
 * </OL>
 * 
 * <P>Il componente {@code ContextLoaderListenerEx} pu&ograve; essere
 * configurato come gestore di eventi nel descrittore di deployment
 * {@code web.xml}.</P>
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;web-app ... &gt;
 *     ...
 *     &lt;listener&gt;
 *         &lt;listener-class&gt;it.scoppelletti.programmerpower.web.spring.ContextLoaderListenerEx&lt;/listener-class&gt;
 *     &lt;/listener&gt;           
 *     ...        
 * &lt;/web-app&gt;
 * </PRE><BLOCKQUOTE>
 *    
 * @see   it.scoppelletti.programmerpower.web.spring.ApplicationContextListener
 * @see   it.scoppelletti.programmerpower.web.spring.XmlWebApplicationContextEx
 * @see   it.scoppelletti.programmerpower.web.spring.config.WebApplicationContextInitializer     
 * @since 1.0.0
 */
public final class ContextLoaderListenerEx extends ContextLoaderListener {
    
    /**
     * Costruttore.
     */
    public ContextLoaderListenerEx() {        
    }
    
    /**
     * Restituisce la classe che implementa il contesto Spring
     * dell&rsquo;applicazione.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @return {@code it.scoppelletti.programmerpower.web.spring.XmlWebApplicationContextEx}
     */
    @Override
    protected Class<?> determineContextClass(ServletContext servletCtx) {
        return XmlWebApplicationContextEx.class;
    }
        
    /**
     * Restituisce la classe che implementa la personalizzazione del contesto\
     * dell&rsquo;applicazione.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @return {@code it.scoppelletti.programmerpower.web.spring.config.WebApplicationContextInitializer}
     */
    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected List<Class<ApplicationContextInitializer<
        ConfigurableApplicationContext>>>
    determineContextInitializerClasses(ServletContext servletCtx) {
        List list;
        
        list = new ArrayList<Class<?>>(1);               
        list.add(WebApplicationContextInitializer.class);
        
        return list;
    }   
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.net.URI;

/**
 * Propriet&agrave; del servizio
 * <ACRONYM TITLE="Central Authentication Service">CAS</ACRONYM>.
 *
 * @since 1.1.0
 */
public interface SsoServiceProperties {

    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_NAME = "it-scoppelletti-ssoProps";
    
    /**
     * Restituisce l&rsquo;URL del server CAS. 
     * 
     * @return Valore. 
     */
    URI getServerUri();
    
    /**
     * Restituisce il servizio corrispondente all&rsquo;applicazione per la
     * quale eseguire l&rsquo;autenticazione.
     * 
     * @return Valore.
     */
    String getService();
}

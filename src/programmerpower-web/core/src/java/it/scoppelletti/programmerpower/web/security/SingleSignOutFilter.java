/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.lang3.*;
import org.jasig.cas.client.util.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.filter.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Filtro di Single Sign-Out.
 * 
 * <P>Il filtro {@code SingleSignOutFilter} intercetta la richiesta di logout
 * inviata dal server
 * <ACRONYM TITLE="Central Authentication Service">CAS</ACRONYM> a tutte le
 * applicazioni che condividono la stessa autenticazione
 * <ACRONYM TITLE="Single Sign-On">SSO</ACRONYM> in seguito alla richiesta
 * del Single Sign-Out.<BR>
 * L&rsquo;implementazione si basa su una collezione mantenuta da ogni
 * applicazione nella quale sono inserite le sessioni autenticate; questa
 * collezione &egrave; gestita dai componenti {@code SsoAuthenticationFilter},
 * {@code SsoAuthenticationService} e {@code SsoRememberMeServices}.</P> 
 * 
 * @see   it.scoppelletti.programmerpower.web.security.SingleSignOutSessionListener
 * @see   it.scoppelletti.programmerpower.web.security.SsoAuthenticationFilter
 * @see   it.scoppelletti.programmerpower.web.security.SsoAuthenticationService
 * @see   it.scoppelletti.programmerpower.web.security.SsoRememberMeServices
 * @since 1.0.0
 */
@Final
public class SingleSignOutFilter extends GenericFilterBean {
    
    /**
     * Valore di default della propriet&agrave; {@code filterProcessesUrl}. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #setFilterProcessesUrl
     */
    public static final String DEF_FILTERPROCESSESURL =
        "/j_spring_cas_security_check";
    
    /**
     * Valore di default della propriet&agrave; {@code logoutParameterName}. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #setLogoutParameterName
     */
    public static final String DEF_LOGOUTPARAMETERNAME = "logoutRequest";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            SingleSignOutFilter.class);    
    private String myProcessesUrl;
    private String myLogoutParamName;
    private CasClient myCasClient;
        
    /**
     * Costruttore.
     */
    public SingleSignOutFilter() {
        myProcessesUrl = SingleSignOutFilter.DEF_FILTERPROCESSESURL;
        myLogoutParamName = SingleSignOutFilter.DEF_LOGOUTPARAMETERNAME;
    }
    
    /**
     * Imposta l&rsquo;URL delle richieste processate dal filtro.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code /j_spring_cas_security_check} 
     */
    public void setFilterProcessesUrl(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myProcessesUrl = value;
    }
    
    /**
     * Imposta il nome del parametro delle richieste sul quale &egrave;
     * impostato il messaggio per il logout.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code logoutRequest}
     */
    public void setLogoutParameterName(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myLogoutParamName = value;
    }
    
    /**
     * Imposta il client CAS.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setCasClient(CasClient obj) {
        myCasClient = obj;
    }
    
    /**
     * Implementazione del filtro.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     * @param chain Catena dei filtri.
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {
        String ticket;
        
        if (myCasClient == null) {
            throw new PropertyNotSetException(toString(), "casClient");
        }
        
        ticket = getTicket(req);
        if (ticket == null) {
            chain.doFilter(req, resp);
        } else {                
            myCasClient.removeAuthenticatedSession(ticket);
        }
    }
    
    /**
     * Restituisce il ticket da rimuovere.
     * 
     * @param  req  Richiesta.
     * @return      Valore. Se il filtro non deve rimuovere alcun ticket,
     *              restituisce {@code null}.
     */
    private String getTicket(ServletRequest req) {
        int p;
        String msg, path, target, ticket, uri;
        HttpServletRequest httpReq;
        
        if (!(req instanceof HttpServletRequest)) {
            myLogger.warn("No HTTP request.");
            return null;
        }
        httpReq = (HttpServletRequest) req;
        
        msg = httpReq.getParameter(myLogoutParamName);
        if (StringUtils.isBlank(msg)) {
            return null;
        }
        
        uri = httpReq.getRequestURI();
        p = uri.indexOf(';');
        if (p >= 0) {
            uri = uri.substring(0, p);
        }

        p = uri.indexOf('?');
        if (p >= 0) {
            uri = uri.substring(0, p);
        }

        path = httpReq.getContextPath();
        if (StringUtils.isBlank(path))  {
            target = myProcessesUrl;
        } else {
            target = path.concat(myProcessesUrl);
        }
        
        if (!uri.endsWith(target)) {
            return null;
        }
        
        myLogger.debug("Logout message: {}.", msg);        
        ticket = XmlUtils.getTextForElement(msg, "SessionIndex");
        if (StringUtils.isBlank(ticket)) {
            return null;
        }
        
        return ticket;
    }    
}

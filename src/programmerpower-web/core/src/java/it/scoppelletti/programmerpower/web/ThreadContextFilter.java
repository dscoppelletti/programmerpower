/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import it.scoppelletti.diagnostic.DiagnosticSystem;
import it.scoppelletti.programmerpower.AttributeMap;
import it.scoppelletti.programmerpower.RunnableWithResult;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.threading.ThreadContext;
import it.scoppelletti.programmerpower.types.ObjectTools;
import it.scoppelletti.programmerpower.types.UUIDGenerator;
import it.scoppelletti.programmerpower.web.ApplicationServer;

/**
 * Gestione del contesto del thread di processamento di una richiesta.
 * 
 * @see   it.scoppelletti.programmerpower.threading.ThreadContext
 * @since 1.0.0
 */
@Final
public class ThreadContextFilter implements Filter {
     
    /**
     * Nome del thread di processamento di una richiesta. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.programmerpower.threading.ThreadContext
     */
    public static final String THREAD_NAME = "request";

    private static final Logger myLogger = LoggerFactory.getLogger(
            ThreadContextFilter.class);
    private static final RunnableWithResult<UUID> mySessionIdInitializer =
            new ThreadContextFilter.SessionIdInitializer();
    
    /**
     * Costruttore.
     */
    public ThreadContextFilter() {        
    }

    /**
     * Inizializzazione.
     * 
     * @param filterConfig Configurazione.
     */
    public void init(FilterConfig filterConfig) throws ServletException { 
    }
    
    /**
     * Rilascia le risorse.
     */    
    public void destroy() {        
    }
        
    /**
     * Filtro di una richiesta.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     * @param chain Catena dei filtri.
     */
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {
        String applId;
        UUID sessionId;
        HttpServletRequest httpReq;
        ThreadContext threadCtx = null;
        AttributeMap sessionMap;
        
        try {             
            applId = StringUtils.defaultIfBlank(
                    req.getServletContext().getContextPath(),
                    ApplicationServer.DEFAULT_CONTEXT_NAME);
            
            if (req instanceof HttpServletRequest) {
                httpReq = (HttpServletRequest) req;                
                sessionMap = WebTools.getSynchronizedAttributeMap(
                        httpReq.getSession(true));                              
                sessionId = (UUID) sessionMap.getAttribute(
                        WebTools.ATTR_SESSIONID, mySessionIdInitializer);
            } else {
                myLogger.warn("No HTTP request.");
                sessionId = UUIDGenerator.NIL;
            }
            
            MDC.put(DiagnosticSystem.MDC_APPLICATIONIDENTITY, applId);            
            MDC.put(DiagnosticSystem.MDC_APPLICATIONINSTANCEID,
                    sessionId.toString());               
            
            threadCtx = new ThreadContext(ThreadContextFilter.THREAD_NAME,
                    sessionId);
            threadCtx.setLocale(req.getLocale());
            
            chain.doFilter(req, resp);
        } finally {
            threadCtx = ObjectTools.dispose(threadCtx);

            MDC.remove(DiagnosticSystem.MDC_APPLICATIONIDENTITY);
            MDC.remove(DiagnosticSystem.MDC_APPLICATIONINSTANCEID);            
        }
    }
    
    /**
     * Inizializzatore dell&rsquo;id&#46; di sessione.
     */
    private static final class SessionIdInitializer implements
        RunnableWithResult<UUID> {

        /**
         * Costruttore.
         */
        SessionIdInitializer() {            
        }
        
        public UUID run() {
            UUID value;
            
            try {
                value = UUIDGenerator.getInstance().newUUID();
            } catch (Exception ex) {                
                myLogger.error("Failed to extract UUID.", ex);
                return UUIDGenerator.NIL;
            }
            
            return value;
        }               
    }
}

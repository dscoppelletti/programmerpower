/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.security;

import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.reflect.NotImplementedException;

/**
 * Associazione (comando, espressione della configurazione di accesso
 * richiesta).
 *
 * @see it.scoppelletti.programmerpower.web.security.AuthorizeAccessMap.Provider
 * @since 1.0.0
 */
@Final
public class AuthorizeAccessMap implements Map<String, String> {
    private final AuthorizeAccessMap.Provider myProvider;
    
    /**
     * Costruttore.
     */
    public AuthorizeAccessMap(AuthorizeAccessMap.Provider provider) {
        if (provider == null) {
            throw new ArgumentNullException("provider");
        }
        
        myProvider = provider;
    }
    
    /**
     * Restituisce un valore.
     * 
     * @param  key Chiave.
     * @return     Valore. Se l&rsquo;elemento non &egrave; presente nella
     *             collezione restituisce {@code null}. 
     */
    public String get(Object key) {
        String value;
        
        if (key == null) {
            return "permitAll";
        }
        
        value = myProvider.authorizeAccess(key.toString());
        if (StringUtils.isBlank(value)) {
            return "permitAll";
        }
        
        return value;
    }
    
    /**
     * Assegna un valore ad una chiave.
     * 
     * @param  key   Chiave.
     * @param  value Valore.
     * @return       Se esisteva gi&agrave; un elemento con la stessa chiave, ne
     *               restituisce il valore, altrimenti restituisce {@code null}.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     *         Collezione a sola lettura.
     */
    public String put(String key, String value) {
        throw new NotImplementedException(getClass().getName(), "put");
    }
    
    /**
     * Rimuove un elemento.
     * 
     * @param  key Chiave.
     * @return     Se esisteva un elemento assegnato alla chiave, ne restituisce
     *             il valore, altrimentir restituisce {@code null}.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     *         Collezione a sola lettura.             
     */
    public String remove(Object key) {
        throw new NotImplementedException(getClass().getName(), "remove");
    }

    /**
     * Rimuove tutti gli elementi.
     * 
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     *         Collezione a sola lettura.              
     */
    public void clear() {
        throw new NotImplementedException(getClass().getName(), "put");
    }
    
    /**
     * Restituisce il numero di elementi.
     * 
     * @return Valore.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     */
    public int size() {
        throw new NotImplementedException(getClass().getName(), "size");
    }

    /**
     * Indica se la collezione &egrave; vuota.
     * 
     * @return Indicatore.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     */
    public boolean isEmpty() {
        throw new NotImplementedException(getClass().getName(), "isEmpty");
    }

    /**
     * Verifica se una chiave &egrave; presente nella collezione.
     * 
     * @param  key Chiave.
     * @return     Esito della verifica.
     */
    public boolean containsKey(Object key) {
        if (key == null) {
            return false;
        }
        
        return (myProvider.authorizeAccess(key.toString()) != null);
    }

    /**
     * Verifica se un valore &egrave; presente nella collezione.
     * 
     * @param  value Valore.
     * @return       Esito della verifica.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     */
    public boolean containsValue(Object value) {
        throw new NotImplementedException(getClass().getName(),
                "containsValue");
    }

    /**
     * Restituisce la collezione delle chiavi.
     * 
     * @return Collezione.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     */
    public Set<String> keySet() {
        throw new NotImplementedException(getClass().getName(), "keySet");
    }

    /**
     * Restituisce la collezione dei valori.
     * 
     * @return Collezione.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     */    
    public Collection<String> values() {
        throw new NotImplementedException(getClass().getName(), "values");
    }

    /**
     * Restituisce la collezione degli elementi.
     * 
     * @return Collezione.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException 
     */
    public Set<Map.Entry<String, String>> entrySet() {
        throw new NotImplementedException(getClass().getName(), "entrySet");
    }
    
    /**
     * Copia tutti gli elementi da un&rsquo;altra collezione.
     * 
     * @param  map Collezione sorgente.
     * @throws it.scoppelletti.programmerpower.reflect.NotImplementedException
     *         Collezione a sola lettura. 
     */
    public void putAll(Map<? extends String, ? extends String> map) {
        throw new NotImplementedException(getClass().getName(), "putAll");
    }
    
    /**
     * Provider dell&rsquo;associazione (comando, espressione della
     * configurazione di accesso richiesta).
     *
     * @see   it.scoppelletti.programmerpower.web.security.AuthorizeAccessMap
     * @since 1.0.0
     */
    public interface Provider {
        
        /**
         * Restituisce l&rsquo;espressione della configurazione di accesso
         * richiesta per un comando.
         * 
         * @param  cmd Comando.
         * @return     Espressione.
         */
        String authorizeAccess(String cmd);
    }    
}

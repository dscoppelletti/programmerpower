/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web;

import java.util.List;

/**
 * Application Server.
 * 
 * @since 1.1.0
 */
public interface ApplicationServer {
    
    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String BEAN_NAME =
            "it-scoppelletti-programmerpower-web-applicationServer";
    
    /**
     * Nome del contesto di default dell&rsquo;AS. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String DEFAULT_CONTEXT_NAME = "ROOT";
    
    /**
     * Legge l&rsquo;elenco delle applicazioni server installate.
     * 
     * @return Collezione.
     */
    List<ApplicationServer.Application> listApplications();
    
    /**
     * Applicazione server.
     * 
     * @since 1.1.0
     */
    public interface Application {
        
        /**
         * Restituisce il nome dell&rsquo;applicazione.
         * 
         * @return Valore.
         */
        String getName();      
        
        /**
         * Restituisce il percorso del contesto.
         * 
         * @return Valore.
         */
        String getContextPath();
        
        /**
         * Indica se l&rsquo;applicazione &egrave; in esecuzione.
         * 
         * @return Indicatore.
         */
        boolean isRunning();
    }
}

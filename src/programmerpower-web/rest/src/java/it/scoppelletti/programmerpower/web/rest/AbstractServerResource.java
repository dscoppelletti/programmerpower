/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.rest;

import java.util.List;
import java.util.Locale;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.ClientInfo;
import org.restlet.data.Language;
import org.restlet.data.Preference;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.resources.ResourceTools;
import it.scoppelletti.programmerpower.threading.ThreadContext;

/**
 * Classe di base delle risorse
 * <ACRONYM TITLE="REpresentional State Transfer">REST</ACRONYM>.
 * 
 * @since 1.0.0.
 */
public abstract class AbstractServerResource extends ServerResource {

    /**
     * Parametro di richiesta {@code locale}: Codice di localizzazione.
     * 
     * @see #getLocale
     */
    public static final String QUERY_LOCALE = "locale";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            AbstractServerResource.class);
    private Locale myLocale;
    
    /**
     * Costruttore.
     */
    protected AbstractServerResource() {
    }    
    
    /**
     * Inizializzazione.
     */
    @Override
    protected void doInit() throws ResourceException {
        String value;
        ThreadContext threadCtx = ThreadContext.tryGetCurrentThreadContext();
        
        value = initLocale();
        myLocale = StringUtils.isBlank(value) ? null :
            ResourceTools.getLocale(value);
                 
        if (threadCtx != null) {
            if (myLocale == null) {
                myLocale = threadCtx.getLocale();
            } else {
                threadCtx.setLocale(myLocale);
            }
        } else if (myLocale == null) {
            myLocale = Locale.getDefault();
        }
    }
    
    /**
     * Restituisce la localizzazione della richiesta.
     * 
     * @return Oggetto.
     */
    @Final
    protected Locale getLocale() {
        return myLocale;
    }
 
    /**
     * Inizializza la localizzazione della richiesta.
     * 
     * @return Valore.
     */
    private String initLocale() {
        String country, value;
        Language lang;
        ClientInfo clientInfo;
        List<String> tags;
        
        value = getQuery().getFirstValue(AbstractServerResource.QUERY_LOCALE);
        if (!StringUtils.isBlank(value)) {
            myLogger.trace("Query parameter {}={}",
                    AbstractServerResource.QUERY_LOCALE, value);
            return value;
        }    
        
        clientInfo = getClientInfo(); 
        for (Preference<Language> pref : clientInfo.getAcceptedLanguages()) {
            lang = pref.getMetadata();
            if (lang == null || Language.ALL.equals(lang)) {
                continue;
            }
            
            value = lang.getPrimaryTag();
            if (StringUtils.isBlank(value)) {
                continue;
            }
            
            tags = lang.getSubTags();
            country = CollectionUtils.isEmpty(tags) ? null : tags.get(0);
            if (!StringUtils.isBlank(country)) {
                value = StringUtils.join(value, "_", country.toUpperCase());
            }          
            
            myLogger.trace("Preferred language from client: {}.", value);
            return value;            
        }
 
        myLogger.trace("No preferred language from client.");
        return null;
    }
    
    /**
     * Restituisce il valore di un indicatore. che &egrave; possibile inserire
     * tra i parametri dell&rsquo;interrogazione della risorsa.
     * 
     * <OL>
     * <LI>Se il parametro {@code name} non &egrave; presente
     * nell&rsquo;interrogazione della risorsa, si assume che il valore 
     * dell&rsquo;indicatore sia {@code false}.
     * <LI>Se il parametro {@code name} &egrave; presente
     * nell&rsquo;interrogazione della risorsa ma senza alcun valore impostato,
     * si assume che il valore dell&rsquo;indicatore sia {@code true}.
     * <LI>Se il parametro {@code name} &egrave; presente
     * nell&rsquo;interrogazione della risorsa con un valore impostato, tale
     * valore &egrave; convertito nel valore dell&rsquo;indicatore utilizzando
     * il metodo statico parseBoolean della classe JDK {@code Boolean}. 
     * </OL>
     *  
     * @param  name Nome del parametro.
     * @return      Indicatore.
     */
    @Final
    protected boolean testFlag(String name) {
        String value;
        
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name");            
        }
        
        if (!getQuery().getNames().contains(name)) {
            return false;
        }
        
        value = getQuery().getFirstValue(name, Boolean.TRUE.toString());
        return Boolean.parseBoolean(value);
    }
    
    /**
     * Gestione di un&rsquo;eccezione.
     * 
     * @param ex Eccezione.
     */
    @Override
    protected void doCatch(Throwable ex) { 
        myLogger.error("REST Resource failed.", ex);
        super.doCatch(ex);
    }
}

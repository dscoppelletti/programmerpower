/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.rest;

import javax.servlet.*;
import org.apache.commons.lang3.*;
import org.restlet.ext.spring.*;
import org.restlet.resource.*;
import org.restlet.routing.*;
import org.slf4j.*;
import org.springframework.context.ApplicationContext;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.context.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Distribuzione delle richieste 
 * <ACRONYM TITLE="REpresentional State Transfer">REST</ACRONYM>.
 * 
 * @since 1.0.0
 */
@Final
public class RestRouter extends Router implements InitializingBean,
    ServletContextAware {
    private static final Logger myLogger = LoggerFactory.getLogger(
            RestRouter.class);
    private ServletContext myServletCtx;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     */
    public RestRouter() {        
    }

    /**
     * Inizializzazione.
     */
    @Reserved
    public void afterPropertiesSet() throws Exception {
        // Restlet 2.0.11: Il componente SpringBeanRouter implementa la
        // rilevazione delle risorse REST da collegare con l'interfaccia
        // BeanFactoryPostProcessor (il che e' non mi sembra corretto) anziche'
        // con l'interfaccia InitializingBean.        
        String[] aliases, beanNames;

        if (myServletCtx == null) {
            myLogger.warn("Property servletContext not set.");
        }        
        if (getRoutingMode() != Router.MODE_BEST_MATCH) {
            myLogger.warn("Routing mode should be MODE_BEST_MATCH (= 1).");
        }
        
        beanNames = myApplCtx.getBeanNamesForType(ServerResource.class, true,
                true);
        for (String beanName : beanNames) {
            if (!myApplCtx.isPrototype(beanName)) {
                myLogger.warn(
                        "Cannot attach not prototype bean {} to REST router.",
                        beanName);
                continue;
            }
            
            if (isUri(beanName)) {
                attach(beanName, beanName);
            }
            
            aliases = myApplCtx.getAliases(beanName);
            for (String alias : aliases) {
                if (isUri(alias)) {
                    attach(alias, beanName);
                }
            }
        }               
    }
        
    /**
     * Imposta il contesto dell&rsquo;applicazione Web.
     * 
     * @param obj Oggetto.
     */    
    @Reserved
    public void setServletContext(ServletContext obj) {
        myServletCtx = obj;
    }
    
    /**
     * Associa un URI al bean che ne implementa la corrispondente risorsa.
     * 
     * @param uri      URI.
     * @param beanName Nome del bean.
     */
    private void attach(String uri, String beanName) {
        String ctxPath;
        
        myLogger.trace("Attach URI {} to REST resource {}.", uri, beanName);
        
        // Restlet 2.0.11: Il componente ServletAdapter funziona correttamente
        // se e solo se gli URI ai quali sono collegate le risorse REST
        // includono anche il contesto dell'applicazione Web.
        if (myServletCtx != null) {
            ctxPath = myServletCtx.getContextPath();
            if (!StringUtils.isBlank(ctxPath)) {
                uri = ctxPath.concat(uri);
            }
        }
        
        attach(uri, new SpringBeanFinder(this, myApplCtx, beanName));
    }
    
    /**
     * Verifica se un nome potrebbe essere un URI.
     * 
     * @param  name Nome.
     * @return      Esito della verifica.
     */
    private boolean isUri(String name) {
        return name.startsWith("/"); 
    }
}

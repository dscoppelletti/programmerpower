/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.rest;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.restlet.*;
import org.restlet.data.*;
import org.restlet.ext.servlet.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.filter.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/** 
 * Filtro di gestione delle richieste 
 * <ACRONYM TITLE="REpresentional State Transfer">REST</ACRONYM>.
 * 
 * @since 1.0.0
 */
@Final
public class RestServerFilter extends GenericFilterBean {
    
    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_NAME = "it-scoppelletti-restFilter";

    private static final Logger myLogger = LoggerFactory.getLogger(
            RestServerFilter.class);
    
    private Component myComponent;
    private ServletAdapter myAdapter;
    
    /**
     * Costruttore.
     */
    public RestServerFilter() {        
    }
    
    /**
     * Inizializzazione.
     */
    @Override
    protected void initFilterBean() throws ServletException {
        if (myComponent == null) {
            throw new PropertyNotSetException(toString(), "component");
        }

        myAdapter = new ServletAdapter(getServletContext());       
        myAdapter.setNext(myComponent);
        
        myComponent.setContext(myAdapter.getContext());

        // Abilito l'utilizzo di oggetti ClientResource all'interno della
        // gestione delle richieste dei componenti ServerResource
        myComponent.getClients().add(Protocol.HTTP);
        myComponent.getClients().add(Protocol.HTTPS);
    }
    
    /**
     * Imposta il componente REST.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setComponent(Component obj) {
        myComponent = obj;
    }

    /**
     * Filtro di una richiesta.
     * 
     * @param req   Richiesta.
     * @param resp  Risposta.
     * @param chain Catena dei filtri.
     */
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpReq;
        HttpServletResponse httpResp;
        
        if (!(req instanceof HttpServletRequest)) {
            myLogger.warn("No HTTP request.");
            chain.doFilter(req, resp);
            return;
        }
        if (!(resp instanceof HttpServletResponse)) {
            myLogger.warn("No HTTP response.");
            chain.doFilter(req, resp);
            return;
        }
     
        httpReq = (HttpServletRequest) req;
        httpResp = (HttpServletResponse) resp;
        
        myAdapter.service(httpReq, httpResp);
    }       
}

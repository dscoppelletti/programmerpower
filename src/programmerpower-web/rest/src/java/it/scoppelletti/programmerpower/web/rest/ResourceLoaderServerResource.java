/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.rest;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.restlet.data.Status;
import org.restlet.representation.*;
import org.restlet.resource.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.core.io.*;
import org.springframework.core.io.Resource;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorsa inclusa nell&rsquo;applicazione.
 * 
 * @since 1.0.0
 */
@Final
public class ResourceLoaderServerResource extends AbstractServerResource
        implements ResourceLoaderAware {
    private String myLocation;
    private ResourceLoader myResLoader;

    /**
     * Parametro di richiesta {@code strict}: Indica se all&rsquo;assenza della
     * risorsa deve conseguire un&rsquo;eccezione ({@code true}) oppure la
     * restituzione di un flusso di lettura vuoto ({@code false}).
     */
    public static final String QUERY_STRICT = "strict";
    
    /**
     * Costruttore.
     */
    public ResourceLoaderServerResource() {        
    }
    
    @Override
    protected void doInit() throws ResourceException {
        Exception ex;
        
        super.doInit();
        
        if (StringUtils.isBlank(myLocation)) {
            ex = new PropertyNotSetException(toString(), "location");
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);            
        }
        
        if (myResLoader == null) {
            ex = new PropertyNotSetException(toString(), "resourceLoader");
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);                        
        }
    }
    
    /**
     * Imposta la locazione della risorsa da rilevare.
     * 
     * @param value Valore.
     */
    @Required
    public void setLocation(String value) {
        myLocation = value;
    }
    
    /**
     * Imposta il rilevatore delle risorse.
     * 
     * @param obj Oggetto.
     */
    @Reserved
    public void setResourceLoader(ResourceLoader obj) {
        myResLoader = obj;
    }        
   
    /**
     * Restituisce il flusso di lettura della risorsa.
     * 
     * @return Rappresentazione.
     */
    @Get
    public Representation getResource() throws ResourceException {
        InputStream in = null;
        Resource res;
        List<String> locations;        
        IOResources ioRes = new IOResources();
        
        locations = ResourceTools.listCandidateLocalizedResourceLocations(
                myLocation, getLocale());
        for (String loc : locations) {
            res = myResLoader.getResource(loc);
            if (!res.exists()) {
                continue;
            }
            
            try {
                in = res.getInputStream();
            } catch (Exception ex) {
                throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                        ex.getMessage(), ex);
            }
            break;
        }
        
        if (in == null) {
            if (testFlag(ResourceLoaderServerResource.QUERY_STRICT)) {
                throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND,
                     ioRes.getFileFoundMessage(myLocation));   
            } else {
                return new EmptyRepresentation();
            }
        }
        
        return new InputRepresentation(in);
    }
}

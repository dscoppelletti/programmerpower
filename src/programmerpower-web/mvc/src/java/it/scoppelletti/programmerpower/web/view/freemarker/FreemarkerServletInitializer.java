/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import java.util.*;
import javax.servlet.*;

/**
 * Inizializzatore dell&rsquo;oggetto {@code Servlet} che rappresenta
 * FreeMarker.
 */
final class FreemarkerServletInitializer implements ServletConfig {
    private final ServletContext myServletCtx;
    
    /**
     * Costruttore.
     * 
     * @param servletCtx Contesto dell&rsquo;applicazione.
     */
    FreemarkerServletInitializer(ServletContext servletCtx) {
        myServletCtx = servletCtx;
    }

    /**
     * Restituisce il nome del servlet.
     * 
     * @return Valore.
     */
    public String getServletName() {
        return "FreeMarker Result Servlet";
    }        
    
    /**
     * Restituisce il contesto del servlet.
     * 
     * @return Oggetto.
     */
    public ServletContext getServletContext() {
        return myServletCtx;
    }
    
    /**
     * Restituisce il valore di un parametro di inizializzazione.
     * 
     * @param  name Nome del parametro.
     * @return      Valore del parametro.
     */
    public String getInitParameter(String name) {
        return null;
    }

    /**
     * Restituisce la collezione dei nomi dei parametri di inizializzazione.
     * 
     * @return Collezione.
     */
    public Enumeration<String> getInitParameterNames() {
        return Collections.enumeration(Collections.<String>emptyList());
    }
}


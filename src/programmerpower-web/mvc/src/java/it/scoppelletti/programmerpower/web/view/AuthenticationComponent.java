/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.io.*;
import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.commons.lang3.*;
import org.apache.struts2.components.*;
import org.springframework.beans.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.web.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Reimplementazione del tag Spring Security {@code authentication} come
 * componente Struts 2.
 *
 * <P>FreeMarker &egrave; in grado di importare le librerie di tag dei soli
 * moduli <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> inclusi nello stesso
 * pacchetto <ACRONYM TITLE="Web application ARchive">WAR</ACRONYM> con il quale
 * &egrave; distribuita l&rsquo;applicazione Web, e non &egrave; prevista  
 * un&rsquo;<ACRONYM TITLE="Application Programming Interface">API</ACRONYM>
 * pubblica per l&rsquo;estensione di questa funzionalit&agrave;.<BR>
 * Il tag Spring Security {@code authentication} &egrave; stato reimplementato
 * come componente Struts 2 in modo da poter essere importato in FreeMarker
 * sfruttando le funzionalit&agrave; di integrazione di Struts 2.</P>
 * 
 * @see   <A HREF="${it.scoppelletti.token.freemarkerUrl}"
 *        TARGET="_top">FreeMarker</A>
 * @see   <A HREF="${it.scoppelletti.token.springSecurityUrl}"
 *        TARGET="_top">Spring Security</A>  
 * @since 2.0.0
 */
@Final
public class AuthenticationComponent extends ContextBean {
    private final HttpServletRequest myReq;
    private String myProp;
    private String myHtmlEscape;
    
    /**
     * Costruttore.
     * 
     * @param valueStack Stack dei valori.
     * @param req        Richiesta.
     * @param resp       Risposta.
     */
    public AuthenticationComponent(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp) {
        super(valueStack);
        
        if (req == null) {
            throw new ArgumentNullException("req");
        }
        if (resp == null) {
            throw new ArgumentNullException("resp");
        }
        
        myReq = req;
    }
    
    /**
     * Imposta la propriet&agrave; da interrogare.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     */
    public void setProperty(String value) {
        myProp = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di escape delle entit&agrave; HTML.
     * 
     * @param value Valore.
     */
    public void setHtmlEscape(String value) {
        myHtmlEscape = value;
    }
    
    /**
     * Emette il tag.
     * 
     * @param  writer Flusso di scrittura.
     * @param  body   Contenuto.
     * @return        Indica se il contenuto deve essere ulteriormente valutato.
     */
    @Override
    public boolean end(Writer writer, String body) {
        String s;
        Boolean htmlEscape;
        Object value;
        BeanWrapperImpl bean;
        Authentication currentUser;
        
        if (StringUtils.isBlank(myProp)) {
            throw new PropertyNotSetException(toString(), "property");
        }
        
        currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (currentUser == null) {
            return super.end(writer, body);
        }
        
        bean = new BeanWrapperImpl(currentUser);
        value = bean.getPropertyValue(myProp);
        
        var = getVar();
        
        if (StringUtils.isBlank(var)) {
            s = String.valueOf(value);
            
            htmlEscape = (myHtmlEscape == null) ? null : (Boolean)
                    findValue(myHtmlEscape, Boolean.class);
            if (htmlEscape == null || htmlEscape.booleanValue()) {
                s = TextEscapeUtils.escapeEntities(s);
            }
            
            try {
                writer.write(s);
            } catch (IOException ex) {
                throw new IOOperationException(ex);
            }            
        } else {
            putInContext(value);
            myReq.setAttribute(var, value);
        }
        
        return super.end(writer, body);
    }
}

/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import java.util.*;
import javax.servlet.http.*;
import freemarker.core.*;
import freemarker.ext.servlet.*;
import org.apache.tiles.*;
import org.apache.tiles.awareness.*;
import org.apache.tiles.context.*;
import org.apache.tiles.freemarker.*;
import org.apache.tiles.freemarker.context.*;
import org.apache.tiles.servlet.context.*;
import org.slf4j.*;

/**
 * Oggetto di factory del contesto Tiles delle richieste FreeMarker.
 *
 * <P>La gestione delle richieste &egrave; delegata alla classe Struts 2
 * {@code FreemarkerResult}.</P>
 * 
 * @since 1.0.0
 */
public final class FreemarkerRequestContextFactory implements 
    TilesRequestContextFactory, TilesRequestContextFactoryAware {
    private static final Logger myLogger = LoggerFactory.getLogger(
            FreemarkerRequestContextFactory.class); 
    private TilesRequestContextFactory myParent;

    /**
     * Costruttore.
     */
    public FreemarkerRequestContextFactory() {
    }
    
    /**
     * Imposta l&rsquo;oggetto di factory del contesto delle richieste Tiles.
     * 
     * @param obj Oggetto.
     */
    public void setRequestContextFactory(
            TilesRequestContextFactory obj) {
        myParent = obj;
    }

    /**
     * Inizializza i parametri di configurazione.
     * 
     * @param configParams Collezione.
     * @deprecated L&rsquo;interfaccia {@code org.apache.tiles.Initializable}
     *             &egrave; obsoleta e quindi devo rendere obsoleta 
     *             l&rsquo;implementazione del metodo {@code init} per evitare
     *             un avviso in compilazione.  
     */
    public void init(Map<String, String> configParams) {
    }
    
    /**
     * Restituisce il contesto di una richiesta.
     * 
     * @param  tilesCtx Contesto Tiles.
     * @param  items    Elementi della richiesta.
     * @return          Oggetto.
     */
    public TilesRequestContext createRequestContext(
            TilesApplicationContext tilesCtx, Object... items) {
        Environment freemarkerEnv;
        HttpServletRequest req;
        HttpServletResponse resp;
        TilesRequestContext tilesReq;
        HttpRequestHashModel reqModel;
            
        if (items.length == 1 && items[0] instanceof Environment) {
            freemarkerEnv = (Environment) items[0];
            
            try {
                reqModel = FreeMarkerUtil.getRequestHashModel(freemarkerEnv);
            } catch (FreeMarkerTilesException ex) {
                myLogger.warn("Request model not found.", ex);
                return null;
            }
                        
            req = reqModel.getRequest();
            resp = reqModel.getResponse();
                        
            if (myParent != null) {
                tilesReq = myParent.createRequestContext(tilesCtx, req,
                        resp);
            } else {                  
                tilesReq = new ServletTilesRequestContext(tilesCtx, req,
                        resp);
            }
            
            return new FreemarkerRequestContext(tilesReq, freemarkerEnv);
        }
        
        return null;
    }
}

/*
 * Copyright (C) 2011-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Intercettore di supporto per l&rsquo;interfaccia {@code Preparable}.
 * 
 * <P>L&rsquo;intercettore {@code PrepareInterceptor} &egrave;
 * un&rsquo;alternativa all&rsquo;omonimo intercettore Struts 2:
 * quest&rsquo;ultimo ignora l&rsquo;eventuale eccezione inoltrata dal metodo di
 * preparazione specifico per il metodo in esecuzione (limitandosi a segnalarla
 * su log); la presente implementazione invece lascia la gestione
 * dell&rsquo;eventuale eccezione al metodo chiamante.</P>
 * 
 * @since 1.0.0
 */
@Final
public class PrepareInterceptor extends MethodFilterInterceptor {
    private static final long serialVersionUID = 1L;    
    private static final String PREPARE_PREFIX = "prepare";
    private static final String ALT_PREPARE_PREFIX = "prepareDo";
    private boolean myAlwaysInvokePrepare = true;
    private boolean myFirstCallPrepareDo = false;
    
    /**
     * Costruttore.
     */
    public PrepareInterceptor() {        
    }
    
    /**
     * Imposta l&rsquo;indicatore di esecuzione del metodo {@code prepare} oltre
     * che dell&rsquo;eventuale metodo di preparazione specifico per il metodo
     * in esecuzione.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code true}
     */
    public void setAlwaysInvokePrepare(String value) {
        myAlwaysInvokePrepare = Boolean.parseBoolean(value);
    }

    /**
     * Imposta l&rsquo;indicatore di priorit&agrave; del metodo con prefisso
     * {@code doPrepare} rispetto al metodo con prefisso {@code prepare} per il
     * rilevamento nell&rsquo;azione dell&rsquo;eventuale metodo di preparazione
     * specifico per il metodo in esecuzione.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     */
    public void setFirstCallPrepareDo(String value) {
        myFirstCallPrepareDo = Boolean.parseBoolean(value);
    }
    
    /**
     * Esegue l&rsquo;intercettore.
     * 
     * @param  invocation Richiesta.
     * @return            Risultato.
     */    
    protected String doIntercept(ActionInvocation invocation) throws Exception {
        String[] prefixes;
        Preparable action;

        if (invocation.getAction() instanceof Preparable) {
            action = (Preparable) invocation.getAction();
            
            if (myFirstCallPrepareDo) {
                prefixes = new String[] { PrepareInterceptor.ALT_PREPARE_PREFIX,
                        PrepareInterceptor.PREPARE_PREFIX };
            } else {
                prefixes = new String[] { PrepareInterceptor.PREPARE_PREFIX,
                        PrepareInterceptor.ALT_PREPARE_PREFIX };
            }
            PrefixMethodInvocationUtil.invokePrefixMethod(invocation,
                    prefixes);

            if (myAlwaysInvokePrepare) {            
                action.prepare();
            }
        }

        return invocation.invoke();
    }
}

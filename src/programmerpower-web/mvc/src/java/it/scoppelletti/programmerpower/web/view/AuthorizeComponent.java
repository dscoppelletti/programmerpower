/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.io.*;
import javax.servlet.*;
import com.opensymphony.xwork2.util.*;
import org.apache.commons.lang3.*;
import org.apache.struts2.*;
import org.apache.struts2.components.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.expression.*;
import org.springframework.security.access.expression.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.web.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.reflect.NotImplementedException;
import it.scoppelletti.programmerpower.web.security.*;

/**
 * Reimplementazione del tag Spring Security {@code authorize} come componente
 * Struts 2.
 *
 * <P>FreeMarker &egrave; in grado di importare le librerie di tag dei soli
 * moduli <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> inclusi nello stesso
 * pacchetto <ACRONYM TITLE="Web application ARchive">WAR</ACRONYM> con il quale
 * &egrave; distribuita l&rsquo;applicazione Web, e non &egrave; prevista  
 * un&rsquo;<ACRONYM TITLE="Application Programming Interface">API</ACRONYM>
 * pubblica per l&rsquo;estensione di questa funzionalit&agrave;.<BR>
 * Il tag Spring Security {@code authorize} &egrave; stato di reimplementato
 * come componente Struts 2 in modo da poter essere importato in FreeMarker
 * sfruttando le funzionalit&agrave; di integrazione di Struts 2.</P>
 * 
 * @see   <A HREF="${it.scoppelletti.token.freemarkerUrl}"
 *        TARGET="_top">FreeMarker</A>
 * @see   <A HREF="${it.scoppelletti.token.springSecurityUrl}"
 *        TARGET="_top">Spring Security</A>  
 * @since 2.0.0
 */
@Final
public class AuthorizeComponent extends Component {
    private String myAccess;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    @javax.annotation.Resource(name =
            WebSecurityTools.BEAN_WEBSECURITYEXPRESSIONHANDLER)
    private SecurityExpressionHandler<FilterInvocation> myExprHandler;
    
    /**
     * Costruttore.
     * 
     * @param valueStack Stack dei valori.
     */
    public AuthorizeComponent(ValueStack valueStack) {
        super(valueStack);
    }
    
    /**
     * Imposta l&rsquo;espressione che specifica l&rsquo;accesso richiesto per
     * l&rsquo;utente correntemente autenticato.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     */
    public void setAccess(String value) {
       myAccess = value; 
    }
    
    /**
     * Emette l&rsquo;apertura del tag e verifica se deve essere emesso il
     * contenuto del tag stesso.
     * 
     * @param  writer Flusso di scrittura.
     * @return        Esito della verifica.
     */
    @Override
    public boolean start(Writer writer) {
        Authentication currentUser;
        FilterInvocation filter;
        Expression accessExpr;        
        
        if (StringUtils.isBlank(myAccess)) {
            throw new PropertyNotSetException(toString(), "access");
        }
        
        currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (currentUser == null) {
            throw new ObjectNotFoundException(Authentication.class.getName());
        }
        
        accessExpr = myExprHandler.getExpressionParser().parseExpression(
                myAccess);
        
        filter = new FilterInvocation(ServletActionContext.getRequest(),
                ServletActionContext.getResponse(),
                new AuthorizeComponent.DummyChain());

        if (ExpressionUtils.evaluateAsBoolean(accessExpr,
                myExprHandler.createEvaluationContext(currentUser, filter))) {
            return true;
        }
        
        return false;
    }
        
    /**
     * Catena di filtri.
     */
    private static final class DummyChain implements FilterChain {

        /**
         * Costruttore.
         */
        DummyChain() {            
        }
        
        /**
         * Esegue il filtro.
         * 
         * @param req  Richiesta.
         * @param resp Risposta.
         */
        public void doFilter(ServletRequest req, ServletResponse resp)
                throws IOException, ServletException {
            throw new NotImplementedException(getClass().toString(),
                    "doFilter");
        }        
    }    
}

/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import com.opensymphony.xwork2.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * <ACRONYM TITLE="User Interface">UI</ACRONYM> di un&rsquo;applicazione Web.
 * 
 * @see   it.scoppelletti.programmerpower.web.view.WebUI.Provider
 * @since 1.0.0
 */
@Final
public class WebUI extends UserInterface {
    private static final Logger myLogger = LoggerFactory.getLogger(WebUI.class);
    private volatile WebUI.Provider myProvider = null;
    
    /**
     * Costruttore.
     */
    public WebUI() {
        super(WebUI.class.getName());
    }     
    
    /**
     * Espone un messaggio.
     * 
     * @param event Evento.
     * 
     * <P>Questa implementazione del metodo {@code display} demanda 
     * l&rsquo;esposizione del messaggio all&rsquo;azione in corso,
     * purch&eacute; implementi l&rsquo;interfaccia {@code WebUI.Provider}</P>
     * 
     * @see it.scoppelletti.programmerpower.web.view.WebUI.Provider
     */        
    @Override
    protected void display(MessageEvent event) {
        if (myProvider == null) {
            myProvider = initProvider();
        }
        
        if (myProvider != null) {
            myProvider.display(event);
        }
    }
    
    /**
     * Inizializza il provider per l&rsquo;esposizione di un messaggio.
     * 
     * @return Oggetto.
     */
    private WebUI.Provider initProvider() {
        Object action;
        ActionContext actionCtx;
        ActionInvocation actionInvocation;
        
        actionCtx = ActionContext.getContext();
        actionInvocation = actionCtx.getActionInvocation();
        if (actionInvocation == null) {
            myLogger.warn("ActionInvocation not set.");
            return null;
        }
            
        action = actionInvocation.getAction();
        if (action == null) {
            myLogger.warn("Action not set.");
            return null;
        }
        
        if (!(action instanceof WebUI.Provider)) {
            myLogger.warn("Action {} not implement WebUI.Provider.",
                    action.getClass().getName());
            return null;
        }
        
        return (WebUI.Provider) action;
    }
    
    /**
     * Provider per l&rsquo;esposizione di un messaggio.
     *
     * @see   it.scoppelletti.programmerpower.web.view.WebUI
     * @since 1.0.0
     */
    public interface Provider {

        /**
         * Espone un messaggio.
         * 
         * @param event Evento.
         */
        void display(MessageEvent event);        
    }
}

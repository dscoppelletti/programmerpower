/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package it.scoppelletti.programmerpower.web.control;

import java.util.*;
import com.opensymphony.xwork2.validator.*;
import com.opensymphony.xwork2.validator.validators.*;
import org.apache.commons.collections4.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Verifica che tutti i valori di una collezione {@code Map} siano specificati.
 * 
 * @since 2.0.0
 */
@Final
public class MapValueRequiredValidator extends FieldValidatorSupport {

    /**
     * Nome del validatore. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NAME = "it-scoppelletti-mapValueRequired";
    
    /**
     * Costruttore.
     */
    public MapValueRequiredValidator() {        
    }
        
    /**
     * Validazione.
     * 
     * @param obj Modello da validare.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void validate(Object obj) throws ValidationException {        
        String name, itemName;
        boolean valid;
        Object value;
        Map<String, ?> map;
        
        name = getFieldName();
        map = (Map<String, ?>) getFieldValue(name, obj);
        if (MapUtils.isEmpty(map)) {
            return;
        }
        
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            value = entry.getValue();
            if (value instanceof String) {
                valid = !StringUtils.isBlank((String) value);
            } else if (value instanceof ValueType) {
                valid = !ValueTools.isNullOrEmpty((ValueType) value);
            } else {
                valid = (value != null);
            }
            
            if (!valid) {
                itemName = name.concat(".").concat(entry.getKey());
                addFieldError(itemName, obj);
            }
        }
    }
}

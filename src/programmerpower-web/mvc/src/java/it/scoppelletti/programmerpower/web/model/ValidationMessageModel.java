/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package it.scoppelletti.programmerpower.web.model;

import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Modello di un messaggio di validazione.
 *  
 * @since 2.0.0
 */
public final class ValidationMessageModel {
    private final Map<String, Object> myParams;
    
    /**
     * Costruttore.
     */
    public ValidationMessageModel() {
        myParams = new HashMap<String, Object>();
    }
    
    /**
     * Costruttore.
     * 
     * @param name  Nome del parametro.
     * @param value Valore del parametro.
     * @see         #getParameters
     */    
    public ValidationMessageModel(String name, Object value) {
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name");
        }
                        
        myParams = new HashMap<String, Object>();
        myParams.put(name, value);
    }
    
    /**
     * Restituisce i parametri.
     * 
     * @return Collezione.
     * @see    #addParameter
     */
    public Map<String, Object> getParameters() {
        return myParams;
    }
    
    /**
     * Aggiunge un parametro.
     * 
     * @param  name  Nome del parametro.
     * @param  value Valore del parametro.
     * @return       Questo stesso oggetto.
     * @see          #getParameters
     */
    public ValidationMessageModel addParameter(String name, Object value) {
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name");
        }
        
        myParams.put(name, value);      
        return this;
    }
}

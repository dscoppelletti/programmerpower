/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.tiles;

import org.apache.tiles.startup.*;
import org.apache.tiles.web.startup.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Gestione del ciclo di vita del contesto dell&rsquo;applicazione Tiles.
 * 
 * <P>La versione 2.2 di Tiles ha sostituito la modalit&agrave; di
 * configurazione del motore attraverso i parametri del contesto
 * dell&rsquo;applicazione Web con l&rsquo;inizializzazione attraverso un
 * componente {@code Servlet} (classe base
 * {@code AbstractTilesInitializerServlet}) oppure un componente 
 * {@code ServletContextListener} (classe base
 * {@code AbstractTilesListener}).<BR>
 * In particolare, la classe {@code TilesContextListener} implementa la
 * configurazione di Tiles come componente {@code ServletContextListener} che
 * quindi deve essere configurato nel descrittore di deployment
 * dell&rsquo;applicazione Web.</P>
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;web-app ... &gt;
 *     ...
 *     &lt;listener&gt;
 *         &lt;listener-class&gt;it.scoppelletti.programmerpower.web.view.tiles.TilesContextListener&lt;/listener-class&gt;
 *     &lt;/listener&gt;           
 *     ...        
 * &lt;/web-app&gt;
 * </PRE><BLOCKQUOTE>   
 * 
 * <BLOCKQUOTE CLASS="note">
 * Le applicazioni Web Programmer Power normalmente configurano il solo
 * componente {@code ApplicationContextListener} che incorpora gi&agrave; il
 * componente {@code TilesContextListener}.
 * </BLOCKQUOTE>    
 *   
 * <H4>1. Risorse di configurazione</H4>
 * 
 * <P>La configurazione dell&rsquo;applicazione Web specifica &egrave; impostata
 * dalla risorsa {@code /WEB-INF/it-scoppelletti-tiles.xml} inclusa nel modulo
 * <ACRONYM TITLE="Web application ARchive">WAR</ACRONYM>
 * dell&rsquo;applicazione stessa; &egrave; rilevata anche la configurazione dei
 * vari moduli <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> dalle eventuali
 * risorse {@code /META-INF/it-scoppelletti-tiles.xml} incluse in ciascun
 * modulo.</P>
 *  
 * <H4>2. Tipi di attributo</H4>
 * 
 * <P><TABLE WIDTH="100%" BORDER="1" CELLPADDING="5">
 * <THEAD>
 * <TR>
 *     <TH>Codice</TH>
 *     <TH>Descrizione</TH>     
 * </TR>
 * </THEAD>
 * <TBODY>
 * <TR>
 *      <TD>{@code string}</TD>
 *      <TD>Stringa (tipo di default).</TD>            
 * </TR> 
 * <TR>
 *      <TD>{@code template}</TD>
 *      <TD>Modello Tiles.</TD>            
 * </TR>
 * <TR>
 *      <TD>{@code definition}</TD>
 *      <TD>Definizione Tiles.</TD>            
 * </TR>  
 * <TR>
 *      <TD>{@code freemarker}</TD>
 *      <TD>Modello FreeMarker.</TD>            
 * </TR> 
 * </TBODY>
 * </TABLE></P>   
 * 
 * <H4>3. Tecnologie supportate</H4>
 * 
 * <OL>
 * <LI>Servlet.
 * <LI>JSP.
 * <LI>FreeMarker.
 * <OL>   
 *
 * <H4>4. Funzionalit&agrave; non attivate</H4>
 * 
 * <OL>
 * <LI>Utilizzo di pi&ugrave; <DFN>container</DFN> alternativi.
 * <LI>Linguaggi di espressione.
 * <LI>Personalizzazione del rendering degli attributi.
 * <LI>Personalizzazione dell&rsquo;istanziazione dei componenti
 * {@code ViewPreparer} (i componenti sono quindi utilizzati come oggetti
 * singleton). 
 * </OL>
 *  
 * @see   it.scoppelletti.programmerpower.web.spring.ApplicationContextListener
 * @see   <A HREF="${it.scoppelletti.token.freemarkerUrl}"
 *        TARGET="_top">FreeMarker</A>
 * @since 1.0.0
 */
@Final
public class TilesContextListener extends AbstractTilesListener {

    /**
     * Nome della risorsa delle definizioni Tiles di un&rsquo;applicazione Web.
     * Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String APPL_DEFINITIONS =
        "WEB-INF/it-scoppelletti-tiles.xml";
    
    /**
     * Nome della risorsa nei moduli JAR che costituisce il contributo di
     * definizioni Tiles del modulo specifico. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String MODULE_DEFINITIONS =
        "classpath*:META-INF/it-scoppelletti-tiles.xml";
    
    /**
     * Costruttore.
     */
    public TilesContextListener() {        
    }

    /**
     * Istanzia l&rsquo;inizializzatore.
     */
    @Override
    protected TilesInitializer createTilesInitializer() {
        return new TilesInitializerEx();
    }
}

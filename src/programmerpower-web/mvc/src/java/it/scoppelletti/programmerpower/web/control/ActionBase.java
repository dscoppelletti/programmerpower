/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.io.*;
import java.util.*;
import javax.annotation.*;
import com.opensymphony.xwork2.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.core.env.*;
import org.springframework.web.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;
import it.scoppelletti.programmerpower.web.security.*;
import it.scoppelletti.programmerpower.web.view.*;

/**
 * Classe base delle azioni.
 * 
 * @since 1.0.0
 */
public abstract class ActionBase extends ActionSupport implements Disposable,
        InterceptorExclusionSupport, WebUI.Provider {
    private static final long serialVersionUID = 1L;
        
    /**
     * Risultato {@code cancel}: Annullamento.
     */
    public static final String CANCEL = "cancel";

    /**
     * Propriet&agrave; dell&rsquo;azione {@code label.title}: Titolo della
     * vista.
     * 
     * @see #getViewTitle
     */
    public static final String PROP_VIEWTITLE = "label.title";
    
    /**
     * Propriet&agrave; dell&rsquo;azione {@code path.help}: Percorso della
     * guida dell&rsquo;azione.
     *
     * @see #getHelpUrl
     */
    public static final String PROP_HELPPATH = "path.help";
    
    /**
     * Propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.help.url}: URL di base della
     * guida di riferimento di Programmer Power.
     * 
     * @see #getHelpUrl
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A>  
     */
    public static final String PROP_HELPURL =
            "it.scoppelletti.programmerpower.web.help.url";
    
    /**
     * Valore di default dell&rsquo;URL di base della guida di riferimento di 
     * Programmer Power. Il valore della costante &egrave; 
     * <CODE>{@value}</CODE>.
     * 
     * @see #getHelpUrl
     */
    public static final String DEF_HELPURL = 
            "http://www.scoppelletti.it/programmerpower";

    private static final String ACTION_SUFFIX = "Action";

    /**
     * @serial Stato della vista.
     */
    private ViewState myViewState;
    
    /**
     * @serial Indicatore di risorse rilasciate.
     */
    private boolean myIsDisposed = false;
    
    private transient String myActionNameBase;
    private transient List<ActionMessage> myMessages;    
    private transient Map<String, String> myAuthorizeAccessMap;
    
    @Resource(name = UserInterfaceProvider.BEAN_NAME)
    private transient UserInterfaceProvider myUI;
        
    @Autowired
    private transient ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     */
    protected ActionBase() {
        myMessages = new ArrayList<ActionMessage>();
    }    
    
    public void dispose() {
        myIsDisposed = true;
    }
    
    /**
     * Serializza l&rsquo;oggetto.
     * 
     * @param      out Flusso di scrittura.
     * @serialData     Formato di default seguito dai messaggi:
     * 
     * <P><OL>
     * <LI>Numero dei messaggi.
     * <LI>Sequenza dei messaggi.
     * </OL></P> 
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        
        out.writeInt(myMessages.size());
        for (ActionMessage msg : myMessages) {
            out.writeObject(msg);
        }
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param in Flusso di lettura.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        int i, n;
        
        in.defaultReadObject();
        
        myMessages = new ArrayList<ActionMessage>();
        n = in.readInt();
        for (i = 0; i < n; n++) {
            myMessages.add((ActionMessage) in.readObject());
        }
    }
       
    /**
     * Restituisce lo stato della vista.
     * 
     * @return Oggetto.
     * @see    #setViewState
     */
    @Final
    public ViewState getViewState() {
        if (myViewState == null) {
            myViewState = new ViewState();
        }
        
        return myViewState;
    }
        
    /**
     * Imposta lo stato della vista.
     * 
     * @param obj Oggetto.
     * @see       #getViewState
     */
    @Final
    public void setViewState(ViewState obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");        
        }
                
        myViewState = obj;
    }
    
    /**
     * Restituisce la base del nome delle operazioni dell&rsquo;azione.
     * 
     * @return Valore.
     */    
    @Final
    public String getActionNameBase() {
        if (myActionNameBase == null) {
            myActionNameBase = initActionNameBase();
        }
        
        return myActionNameBase;
    }
    
    /**
     * Inizializza la base del nome delle operazioni dell&rsquo;azione;.
     * 
     * @return Valore.
     */    
    protected String initActionNameBase() {
        char c;
        String value;
        
        value = getClass().getSimpleName();
        if (StringUtils.isBlank(value)) {
            return "";
        }
        
        if (value.endsWith(ActionBase.ACTION_SUFFIX)) {
            value = value.substring(0, value.length() -
                    ActionBase.ACTION_SUFFIX.length());
            if (value.isEmpty()) {
                return "";
            }
        }
                
        c = value.charAt(0);
        if (Character.isUpperCase(c)) {
            value = String.valueOf(Character.toLowerCase(c)).concat(
                    value.substring(1));
        }
        
        return value;
    }        
    
    /**
     * Restituisce il titolo della vista.
     * 
     * <P>La versione di default del metodo {@code getViewTitle} restituisce
     * il valore della propriet&agrave; dell&rsquo;azione
     * {@code label.title}.</P>
     * 
     * @return Valore.
     */
    public String getViewTitle() {
        return getText(ActionBase.PROP_VIEWTITLE);
    }
    
    /**
     * Restituisce l&rsquo;URL della guida dell&rsquo;azione.
     * 
     * <P>La versione di default del metodo {@code getHelpUrl} rileva il valore 
     * della propriet&agrave; dell&rsquo;azione {@code path.help}: se tale
     * propriet&agrave; &egrave; impostata, &egrave; considerata come un
     * percorso relativo all&rsquo;URL di base della guida di riferimento di
     * Programmer Power
     * ({@code http://www.scoppelletti.it/programmerpower}); questo URL di base
     * pu&ograve; essere sovrascritto dal valore impostato sulla
     * propriet&agrave; di ambiente
     * {@code it.scoppelletti.programmerpower.web.help.url}.<BR>
     * Le classi derivate da terze parti dovrebbero implementare una versione
     * prevalente del metodo {@code getHelpUrl} per restituire l&rsquo;URL
     * opportuno.</P>
     * 
     * @return Valore. Se la guida non &egrave; prevista, restituisce
     *         {@code null}.
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/setup/envprops.html"
     *      TARGET="_top">Propriet&agrave; di ambiente</A>          
     */
    public String getHelpUrl() {
        String base, path;
        UriComponentsBuilder uriBuilder;
        Environment env;
        
        path = getText(ActionBase.PROP_HELPPATH);
        if (StringUtils.isBlank(path) ||
                ActionBase.PROP_HELPPATH.equals(path)) {
            return null;
        }
     
        env = myApplCtx.getEnvironment();
        base = env.getProperty(ActionBase.PROP_HELPURL, ActionBase.DEF_HELPURL);     
        
        uriBuilder = UriComponentsBuilder.fromUriString(base).path(path);
        return uriBuilder.toUriString();
    }
    
    /**
     * Restituisce l&rsquo;associazione (comando, espressione della
     * configurazione di accesso richiesta).
     * 
     * @return Collezione.
     * @since  2.0.0
     */
    @Final
    public Map<String, String> getAuthorizeAccessMap() {
        if (myAuthorizeAccessMap == null) {
            myAuthorizeAccessMap = new AuthorizeAccessMap(
                    new AuthorizeAccessMap.Provider() {            
                        public String authorizeAccess(String cmd) {
                            return authorizeAccessImpl(cmd);
                        }
                    });
        }
        
        return myAuthorizeAccessMap;
    }
    
    /**
     * Restituisce l&rsquo;espressione della configurazione di accesso
     * richiesta per un comando.
     * 
     * <P>L&rsquo;implementazione di default del metodo
     * {@code authorizeAccessImpl} autorizza l&rsquo;accesso a tutti i
     * comandi.</P>
     * 
     * @param  cmd Comando.
     * @return     Espressione. Se il comando deve essere autorizzato per tutti,
     *             restituisce {@code null}.
     * @since      2.0.0
     */
    protected String authorizeAccessImpl(String cmd) {
        return null;
    }
        
    /**
     * Restituisce una copia non modificabile della collezione dei messaggi.
     * 
     * @return Collezione.
     */
    @Final
    public List<ActionMessage> getMessages() {
        synchronized (myMessages) {
            return Collections.unmodifiableList(myMessages);
        }
    }
    
    /**
     * Restituisce l&rsquo;interfaccia utente.
     * 
     * @return Oggetto.
     */
    @Final
    protected UserInterfaceProvider getUserInterface() {
        return myUI;
    }
       
    @Final
    public boolean isDisposed() {
        return myIsDisposed;
    }
    
    /**
     * Verifica se un intercettore deve essere escluso per l&rsquo;esecuzione di
     * un metodo.
     * 
     * <P>Questa implementazione del metodo
     * {@code isInterceptorExcludedForMethod} non esclude alcun
     * intercettore.</P>
     * 
     * @param  interceptor Nome dell&rsquo;intercettore.
     * @param  method      Nome del metodo.
     * @return             Esito della verifica.
     */    
    public boolean isInterceptorExcludedForMethod(String interceptor,
            String method) {
        return false;
    }
    
    /**
     * Aggiunge un messaggio di azione.
     * 
     * @param      msg Messaggio.
     * @deprecated     Utilizzare l&rsquo;{@linkplain #getUserInterface
     *                 interfaccia utente}.             
     */
    @Override
    public void addActionMessage(String msg) {
        MessageEvent event;
        
        if (myUI != null) {
            myUI.display(MessageType.INFORMATION, msg);
        } else {
            event = new MessageEvent(MessageType.INFORMATION, msg, null);
            display(event);
        }
    }  
    
    /**
     * Rimuove tutti i messaggi di azione.
     */
    @Override
    public void clearMessages() {
        int i;
        
        super.clearMessages();
        
        synchronized (myMessages) {
            for (i = myMessages.size() - 1; i >= 0; i--) {
                switch (myMessages.get(i).getEvent().getMessageType()) {
                case ERROR:
                    break;
                    
                default:
                    myMessages.remove(i);
                    break;
                }
            }
        }        
    }
    
    /**
     * Aggiunge un messaggio di errore.
     * 
     * @param      msg Messaggio.
     * @deprecated     Utilizzare l&rsquo;{@linkplain #getUserInterface
     *                 interfaccia utente}.            
     */
    @Override
    public void addActionError(String msg) {
        MessageEvent event;
        
        if (myUI != null) {
            myUI.display(MessageType.ERROR, msg);
        } else {
            event = new MessageEvent(MessageType.ERROR, msg, null);
            display(event);
        }
    }
    
    /**
     * Rimuove tutti i messaggi di errore.
     */
    @Override
    public void clearActionErrors() {
        int i;
        
        super.clearActionErrors();
        
        synchronized (myMessages) {
            for (i = myMessages.size() - 1; i >= 0; i--) {
                switch (myMessages.get(i).getEvent().getMessageType()) {
                    case ERROR:
                        myMessages.remove(i);
                        break;
                        
                    default:
                        break;
                }
            }
        }
    }
    
    /**
     * Rimuove tutti i messaggi sia di errore che di azione.
     */
    @Override
    public void clearErrorsAndMessages() {
        super.clearErrorsAndMessages();
        
        synchronized (myMessages) {
            myMessages.clear();
        }
    }    
    
    public void display(MessageEvent event) {
        synchronized (myMessages) {
            myMessages.add(new ActionMessage(event));
            
            switch (event.getMessageType()) {
            case ERROR:
                super.addActionError(event.getMessage());                
                break;
                
            case INFORMATION:
                super.addActionMessage(event.getMessage());
                break;
                
            case WARNING:
                super.addActionMessage(event.getMessage());
                break;
                
            default:
                throw new EnumConstantNotPresentException(MessageType.class,
                        event.getMessageType().name());
            }            
        }
    }
}

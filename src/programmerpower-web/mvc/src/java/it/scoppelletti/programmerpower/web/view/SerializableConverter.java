/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;
import java.util.Map;
import com.opensymphony.xwork2.conversion.TypeConversionException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.util.StrutsTypeConverter;
import it.scoppelletti.programmerpower.ApplicationException;
import it.scoppelletti.programmerpower.io.IOTools;

/**
 * Convertitore degli oggetti serializzabili.
 * 
 * @since 1.0.0
 */
public final class SerializableConverter extends StrutsTypeConverter {
    
    /**
     * Costruttore.
     */
    public SerializableConverter() {       
    }

    /**
     * Converte una stringa in oggetto.
     * 
     * @param  context Contesto.
     * @param  values  Vettore di stringhe da convertire. &Egrave; considerato
     *                 il solo primo elemento.
     * @param  toClass Tipo nel quale convertire il vettore di stringhe.
     * @return         Oggetto risultante.                                
     */    
    @Override
    @SuppressWarnings("rawtypes")
    public Object convertFromString(Map context, String[] values,
            Class toClass) {
        Object obj;
        String s;
        byte[] data;
        InputStream buf = null;
        ObjectInputStream in = null;
        
        if (ArrayUtils.isEmpty(values)) {
            return null;
        }
        
        s = values[0];
        if (StringUtils.isBlank(s)) {
            return null;
        }
        
        data = Base64.getDecoder().decode(s);
        try {
            buf = new ByteArrayInputStream(data);
            in = new ObjectInputStream(buf);
            buf = null;
            obj = in.readObject();
        } catch (Exception ex) {
            throw new TypeConversionException(ApplicationException.toString(ex),
                    ex);            
        } finally {
            buf = IOTools.close(buf);       
            in = IOTools.close(in);
        }
        
        return obj;
    }

    /**
     * Converte un oggetto in stringa.
     * 
     * @param  context Contesto.
     * @param  obj     Oggetto.
     * @return         Stringa risultante.
     */    
    @Override
    @SuppressWarnings("rawtypes")
    public String convertToString(Map context, Object obj) {        
        ByteArrayOutputStream buf = null;
        ObjectOutputStream out = null;
        
        try {
            buf = new ByteArrayOutputStream();
            out = new ObjectOutputStream(buf);
            
            out.writeObject(obj);
        } catch (IOException ex) {
            throw new TypeConversionException(ApplicationException.toString(ex),
                    ex);
        } finally {
            out = IOTools.close(out);
            IOTools.close(buf);            
        }
        
        return Base64.getEncoder().encodeToString(buf.toByteArray());   
    }    
}

/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.util.*;
import javax.servlet.http.*;
import org.apache.struts2.dispatcher.*;

/**
 * Wrapper di accesso alla collezione degli attributi di sessione.
 *
 * @param <K> Tipo delle chiavi.
 * @param <V> Tipo dei valori.
 */
final class SessionMapEx<K, V> extends SessionMap<K, V> {
    // - Struts 2.3.16
    // La classe SessionMap non prevede l'eventualita' che la sessione risulti
    // invalidata
    private static final long serialVersionUID = 1L;

    /**
     * Costruttore.
     * 
     * @param req Richiesta.
     */
    SessionMapEx(HttpServletRequest req) {
        super(req);
    }

    /**
     * Restituisce la richiesta.
     * 
     * @return Oggetto.
     */
    private HttpServletRequest getRequest() {
        return this.request;
    }
    
    /**
     * Restituisce la sessione.
     * 
     * @return Oggetto.
     * @see    #setSession
     */
    private HttpSession getSession() {
        return this.session;
    }
    
    /**
     * Imposta la sessione.
     * 
     * @return Oggetto.
     * @see    #getSession
     */
    private void setSession(HttpSession obj) {
        this.session = obj;
    }
    
    /**
     * Imposta il cache degli attributi di sessione.
     * 
     * @param obj Oggetto.
     */
    private void setEntries(Set<Map.Entry<K, V>> obj) {
        this.entries = obj; 
    }
    
    @Override
    public void invalidate() {
        checkSession();
        super.invalidate();
    }
    
    @Override
    public void clear() {
        checkSession();
        super.clear();
    }
    
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        checkSession();
        return super.entrySet();
    }
    
    @Override
    public V get(Object key) {
        checkSession();
        return super.get(key);
    }
    
    @Override
    public V put(K key, V value) {
        checkSession();
        return super.put(key, value);
    }
    
    @Override
    public V remove(Object key) {
        checkSession();
        return super.remove(key);
    }
    
    @Override
    public boolean containsKey(Object key) {
        checkSession();
        return super.containsKey(key);
    }

    /**
     * Verifica se la sessione &egrave; stata invalidata.
     */
    private void checkSession() {
        HttpSession session = getSession();
            
        if (session == null) {
            return;
        }
        
        synchronized (session) {
            if (getRequest().getSession(false) == null) {
                setSession(null);
                setEntries(null);
            }
        }
    }
}

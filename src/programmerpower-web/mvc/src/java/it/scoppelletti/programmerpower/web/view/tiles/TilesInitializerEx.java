/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.tiles;

import javax.servlet.*;
import org.apache.tiles.*;
import org.apache.tiles.factory.*;
import org.apache.tiles.servlet.wildcard.*;
import org.apache.tiles.startup.*;

/**
 * Inizializzatore di Tiles.
 * 
 * <OL>
 * <LI>Supporto di un solo container.
 * </OL>
 */
final class TilesInitializerEx extends AbstractTilesInitializer {

    /**
     * Costruttore.
     */
    TilesInitializerEx() {            
    }

    /**
     * Istanzia il contesto dell&rsquo;applicazione.
     * 
     * @param  preliminaryCtx Contesto preliminare.
     * @return                Contesto. 
     */
    @Override
    protected TilesApplicationContext createTilesApplicationContext(
            TilesApplicationContext preliminaryCtx) {
        // Supporto standard dei wildcard nei file di definizione
        return new WildcardServletTilesApplicationContext(
                (ServletContext) preliminaryCtx.getContext());
    }
            
    /**
     * Istanzia l&rsquo;oggetto di factory del container.
     * 
     * @param  ctx Contesto.
     * @return     Oggetto di factory.   
     */
    @Override
    protected AbstractTilesContainerFactory createContainerFactory(
            TilesApplicationContext ctx) {
        return new BasicTilesContainerFactoryEx();
    }        
}

/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.util.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;

/**
 * Intercettore delle eccezioni. 
 * 
 * <P>Il componente {@code ExceptionInterceptor} modifica il funzionamento 
 * dell&rsquo;intercettore Struts 2 {@code ExceptionMappingInterceptor} 
 * pubblicando l&rsquo;eventuale eccezione rilevata come attributo di sessione
 * anzich&eacute; nello stack dei valori.</P>
 * 
 * @since 1.0.0
 */
public class ExceptionInterceptor extends ExceptionMappingInterceptor {
    private static final long serialVersionUID = 1L;
    
    /**
     * Nome dell&rsquo;intercettore. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NAME = "it-scoppelletti-exception";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            ExceptionInterceptor.class);
        
    /**
     * @serial Nome dell&rsquo;attributo di sessione nel quale &egrave;
     *         impostata l&rsquo;eccezione.
     */
    private String myExceptionKey;
    
    /**
     * Costruttore.
     */
    public ExceptionInterceptor() {        
    }
        
    /**
     * Imposta il nome dell&rsquo;attributo di sessione nel quale &egrave;
     * impostata l&rsquo;eccezione.
     * 
     * <P>Se la propriet&agrave; {@code exceptionKey} non &egrave; impostata, il
     * componente {@code ExceptionInterceptor} ricade nel funzionamento standard
     * dell&rsquo;intercettore Struts 2 {@code ExceptionMappingInterceptor}.</P>
     * 
     * @param value Valore.
     */
    public void setExceptionKey(String value) {
        myExceptionKey = value;
    }
        
    /**
     * Pubblica un&rsquo;eccezione.
     * 
     * @param invocation      Contesto dell&rsquo;azione.
     * @param exceptionHolder Eccezione.
     */
    @Override
    protected void publishException(ActionInvocation invocation,
            ExceptionHolder exceptionHolder) {
        Map<String, Object> session;
        
        if (StringUtils.isBlank(myExceptionKey)) {
            myLogger.warn("Property exceptionKey of object {} not set.",
                    toString());
            super.publishException(invocation, exceptionHolder);
            return;
        }

        session = invocation.getInvocationContext().getSession(); 
        session.put(myExceptionKey, exceptionHolder.getException());
    }
}

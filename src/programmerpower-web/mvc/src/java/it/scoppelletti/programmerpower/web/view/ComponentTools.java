/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.util.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Funzioni di utilit&agrave; per i componenti.
 * 
 * @since 2.0.0
 */
public final class ComponentTools {

    /**
     * Riferimento alla cima dello stack dei valori. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String STACK_TOP = "top";
    
    /**
     * Costruttore privato per classe statica.
     */
    private ComponentTools() {        
    }
    
    /**
     * Decora il nome di una variabile in modo che si riferisca al contesto
     * della pagina.
     * 
     * @param  var Nome originale.
     * @return     Nome decorato.
     */
    public static String pageContext(String var) {
        if (StringUtils.isBlank(var)) {
            throw new ArgumentNullException("var");
        }
        
        return "#attr['".concat(var).concat("']");
    }
    
    /**
     * Restituisce la localizzazione per un componente.
     * 
     * @param  valueStack Stack dei valori.
     * @return            Localizzazione.
     */
    public static Locale getLocale(ValueStack valueStack) {
        Locale locale;
        
        if (valueStack == null) {
            throw new ArgumentNullException("valueStack");
        }
        
        locale = (Locale) (Locale) valueStack.getContext().get(
                ActionContext.LOCALE);
        if (locale == null) {
            locale = ResourceTools.getLocale();
        }
        
        return locale;
    }
    
    /**
     * Restituisce le localizzazioni disponibili escludendo le eventuali
     * localizzazioni per variante.
     * 
     * @param  locale Localizzazione per le descrizioni.
     * @return        Collezione.
     */
    public static Map<String, String> getAvailableLocales(Locale locale) {
        Map<String, String> localeMap;
        
        if (locale == null) {
            throw new ArgumentNullException("locale");
        }
        
        localeMap = new TreeMap<String, String>();        
        for (Locale item : Locale.getAvailableLocales()) {
            if (!StringUtils.isBlank(item.getVariant())) {
                continue;
            }
            
            localeMap.put(item.toString(), item.getDisplayName(locale));
        }        
        
        return localeMap;
    }
}

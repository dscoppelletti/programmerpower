/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.io.*;
import javax.persistence.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.interceptor.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.data.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Azione per la gestione
 * <ACRONYM TITLE="Create-Retrieve-Update-Delete">CRUD</ACRONYM> di
 * un&rsquo;entit&agrave;.
 * 
 * @param <K> Tipo della chiave primaria dell&rsquo;entit&agrave;.
 * @param <V> Classe dell&rsquo;entit&agrave;.
 * @since     1.0.0
 */
public abstract class EntityFormAction<K extends Serializable,
    V extends Serializable> extends ActionBase implements ModelDriven<V>,
    Preparable, PreResultListener {
    private static final long serialVersionUID = 1L;

    /**
     * Comando {@code addNew}: Nuova entit&egrave;.
     */
    public static final String CMD_ADDNEW = "addNew";
    
    /**
     * Comando {@code save}: Salva un&rsquo;entit&agrave;.
     */
    public static final String CMD_SAVE = "save";
    
    /**
     * Comando {@code view}: Visualizza un&rsquo;entit&agrave;.
     */
    public static final String CMD_VIEW = "view";
    
    /**
     * Comando {@code edit}: Modifica un&rsquo;entit&agrave;.
     */
    public static final String CMD_EDIT = "edit";
    
    /**
     * Comando {@code update}: Aggiorna un&rsquo;entit&agrave;.
     */
    public static final String CMD_UPDATE = "update";
    
    /**
     * Comando {@code delete}: Cancella un&rsquo;entit&agrave;.
     */
    public static final String CMD_DELETE = "delete";
    
    /**
     * Valore di default dell&rsquo;id&#46; HTML del modulo
     * dell&rsquo;entit&agrave;. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see   #getFormId
     * @since 2.0.0
     */
    public static final String DEF_FORMID = "it-scoppelletti-entityForm";
    
    /**
     * Stato della vista {@code entityId}: chiave primaria
     * dell&rsquo;entit&agrave; corrente.
     * 
     * @see it.scoppelletti.programmerpower.web.control.ActionBase#getViewState
     */
    public static final String VIEWSTATE_ENTITYID = "entityId";
    
    /**
     * Stato della vista {@code entityVersion}: numero di revisione
     * dell&rsquo;entit&agrave; corrente.
     * 
     * @see it.scoppelletti.programmerpower.web.control.ActionBase#getViewState
     */
    public static final String VIEWSTATE_ENTITYVERSION = "entityVersion";
    
    /**
     * Stato della vista {@code formMode}: modalit&agrave; della vista.
     * 
     * @see it.scoppelletti.programmerpower.web.control.ActionBase#getViewState
     */
    public static final String VIEWSTATE_FORMMODE = "formMode";
    
    /**
     * Modalit&agrave; {@code new}: Nuova entit&egrave;.
     */
    public static final String MODE_NEW = "new";
    
    /**
     * Modalit&agrave; {@code view}: Visualizzazione di un&rsquo;entit&agrave;.
     */
    public static final String MODE_VIEW = "view";
    
    /**
     * Modalit&agrave; {@code edit}: Modifica di un&rsquo;entit&agrave;.
     */
    public static final String MODE_EDIT = "edit"; 
    
    /**
     * @serial Entit&agrave;.
     */
    private V myModel;

    /**
     * Costruttore.
     */
    protected EntityFormAction() {
    }    

    @Override
    public void dispose() {
        Disposable obj;
        
        super.dispose();
        if (myModel instanceof Disposable) {
            obj = (Disposable) myModel;
            myModel = null;
            obj.dispose();
        }        
    }
    
    /**
     * Restituisce l&rsquo;entit&agrave; corrente.
     */
    @Final
    public V getModel() {
        return myModel;
    }
   
    /**
     * Restituisce l&rsquo;id&#46; HTML del modulo dell&rsquo;entit&agrave;.
     * 
     * <P>La versione di default del metodo {@code getFormId} restituisce
     * il valore {@code "it-scoppelletti-entityForm"}.</P>
     * 
     * @return Valore.
     * @since  2.0.0
     */
    public String getFormId() {
        return EntityFormAction.DEF_FORMID;
    }
    
    /**
     * Restituisce il titolo della vista.
     * 
     * <P>Questa versione del metodo {@code getViewTitle}, se la modalit&agrave;
     * del form &egrave; {@code new"}, restituisce il valore della
     * propriet&agrave; {@code label.newEntity} tra le risorse associate 
     * all&rsquo;azione, altrimenti restituisce il valore della propriet&agrave;
     * {@code label.entity} tra le risorse associate all&rsquo;azione.</P>
     * 
     * @return Valore.
     */
    @Override
    public String getViewTitle() {
        if (EntityFormAction.MODE_NEW.equals(getViewState().get(
                EntityFormAction.VIEWSTATE_FORMMODE))) {
            return getText("label.newEntity");
        }
        
        return getText("label.entity");
    }
       
    /**
     * Restituisce la chiave primaria dell&rsquo;entit&agrave; corrente.
     * 
     * <P>L&rsquo;implementazione del metodo {@code getModelId} normalmente
     * restituisce la stringa restituita dal metodo {@code getModelIdAsString}
     * convertita nel tipo <CODE>&lt;K&gt;</CODE>.</P>.
     * 
     * @return Valore.
     * @see    #getModelIdAsString
     */
    protected abstract K getModelId();
    
    /**
     * Restituisce la chiave primaria dell&rsquo;entit&agrave; corrente come
     * stringa.
     * 
     * <P>Il metodo {@code getModelIdAsString} &egrave; normalmente utilizzato
     * per l&rsquo;implementazione del metodo {@code getModelId}.</P>
     * 
     * @return Valore.
     * @see    #getModelId
     * @see    #VIEWSTATE_ENTITYID
     */
    @Final
    protected String getModelIdAsString() {
        String value;
        
        value = getViewState().get(EntityFormAction.VIEWSTATE_ENTITYID);
        if (StringUtils.isBlank(value)) {
            throw new PropertyNotSetException(toString(),
                    EntityFormAction.VIEWSTATE_ENTITYID);
        }
        
        return value;
    }
        
    /**
     * Verifica se un intercettore deve essere escluso per l&rsquo;esecuzione di
     * un metodo.
     * 
     * <P>Questa implementazione del metodo
     * {@code isInterceptorExcludedForMethod} esclude, oltre agli intercettori
     * gi&agrave; esclusi dalla classe di base, gli intercettori {@code params},
     * {@code validation} e {@code workflow} per i comandi {@code addNew},
     * {@code view} e {@code edit}.</P>
     * 
     * @param  interceptor Nome dell&rsquo;intercettore.
     * @param  method      Nome del metodo.
     * @return             Esito della verifica.
     */        
    @Override
    public boolean isInterceptorExcludedForMethod(String interceptor,
            String method) {
        if (super.isInterceptorExcludedForMethod(interceptor, method)) {
            return true;
        }
        
        if (ActionInvocationEx.INTERCEPTOR_PARAMS.equals(interceptor)) {
            if (EntityFormAction.CMD_ADDNEW.equals(method) ||
                    EntityFormAction.CMD_VIEW.equals(method) ||
                    EntityFormAction.CMD_EDIT.equals(method)) {
                return true;
            }
            
            return false;
        }
        
        if (ActionInvocationEx.INTERCEPTOR_VALIDATION.equals(interceptor)) {
            if (EntityFormAction.CMD_ADDNEW.equals(method) ||
                    EntityFormAction.CMD_VIEW.equals(method) ||
                    EntityFormAction.CMD_EDIT.equals(method)) {
                return true;
            }
            
            return false;
        }
        
        if (ActionInvocationEx.INTERCEPTOR_WORKFLOW.equals(interceptor)) {
            if (EntityFormAction.CMD_ADDNEW.equals(method) ||
                    EntityFormAction.CMD_VIEW.equals(method) ||
                    EntityFormAction.CMD_EDIT.equals(method)) {
                return true;
            }
            
            return false;
        }            
        
        return false;
    }
           
    /**
     * Preparazione generica.
     * 
     * <P>Questa implementazione del metodo {@code prepare} non esegue
     * nulla.</P> 
     */
    public void prepare() throws Exception {
    }
    
    /**
     * Esegue le operazioni preventive alla produzione del risultato.
     * 
     * <P>Questa implementazione del metodo {@code beforeResult} esegue le
     * seguenti operazioni:</P>
     * 
     * <OL>
     * <LI>Se l&rsquo;azione ha avuto successo, imposta il numero di versione
     * dell&rsquo;entit&agrave; corrente nello stato della vista.
     * </OL>
     * 
     * @param invocation Stato di esecuzione dell&rsquo;azione.
     * @param resultCode Codice del risultato. 
     * @see   #VIEWSTATE_ENTITYVERSION  
     * @see   it.scoppelletti.programmerpower.data.OptimisticLockSupport 
     */
    public void beforeResult(ActionInvocation invocation, String resultCode) {
        OptimisticLockSupport model;
        
        if (Action.SUCCESS.equals(resultCode) &&
                myModel instanceof OptimisticLockSupport) {
            model = (OptimisticLockSupport) myModel;        
            getViewState().put(EntityFormAction.VIEWSTATE_ENTITYVERSION,
                    Integer.toString(model.getVersion()));            
        }               
    }
    
    /**
     * Legge il modello corrente dallo strato di persistenza.
     */
    private void loadModel() {
        myModel = loadEntity(getModelId());
        if (myModel == null) {            
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete")); 
        }        
    }
    
    /**
     * Verifica che il numero di versione dell&rsquo;entit&agrave; corrente
     * coincida con l&rsquo;ultimo impostato nello stato della vista.
     */
    private void checkVersion() {
        String ver;
        OptimisticLockSupport model;
        
        if (myModel instanceof OptimisticLockSupport) {
            model = (OptimisticLockSupport) myModel;
            ver = getViewState().get(EntityFormAction.VIEWSTATE_ENTITYVERSION);
            if (!Integer.toString(model.getVersion()).equals(ver)) {
                throw new OptimisticLockException(getText(
                      "error.concurrency.update"));
            }            
        }         
    }
        
    /**
     * Restituisce la chiave primaria di un&rsquo;entit&agrave;.
     * 
     * @param  obj Entit&agrave;.
     * @return     Valore.
     */
    protected abstract K getEntityId(V obj);
        
    /**
     * Istanzia una nuova entit&agrave;
     * 
     * @return Entit&agrave;.
     */
    protected abstract V newEntity();
    
    /**
     * Applica i valori di default ad un&rsquo;entit&agrave;.
     * 
     * <P>La versione di default del metodo {@code setDefaults} non esegue
     * nulla.</P>
     * 
     * @param obj Entit&agrave;.
     * @see       #prepareAddNew
     */
    protected void setDefaults(V obj) {        
    }
        
    /**
     * Legge un&rsquo;entit&agrave; dallo strato di persistenza.
     * 
     * @param  id Chiave primaria.
     * @return    Entit&agrave;.
     */
    protected abstract V loadEntity(K id);
    
    /**
     * Registra una nuova entit&agrave; sullo strato di persistenza.
     * 
     * @param obj Entit&agrave;.
     */
    protected abstract void saveEntity(V obj);

    /**
     * Aggiorna un&rsquo;entit&agrave; sullo strato di persistenza.
     * 
     * @param obj Entit&agrave;.
     */
    protected abstract void updateEntity(V obj); 
       
    /**
     * Cancella un&rsquo;entit&agrave; sullo strato di persistenza.
     * 
     * @param obj Entit&agrave;.
     */
    protected abstract void deleteEntity(V obj);
    
    /**
     * Comando {@code addNew}.
     * 
     * <P>La versione di default del metodo {@code addNew} imposta la
     * modalit&agrave; del form a {@code new}.</P>
     * 
     * @return Risultato.
     * @see    #VIEWSTATE_FORMMODE
     */    
    public String addNew() {    
        getViewState().put(EntityFormAction.VIEWSTATE_FORMMODE,
                EntityFormAction.MODE_NEW);

        return Action.SUCCESS;
    }
    
    /**
     * Preparazione per il comando {@link #addNew}.
     * 
     * <P>L&rsquo;implementazione di default del metodo {@code prepareAddNew}
     * esegue le seguenti operazioni:</P>
     * 
     * <OL>
     * <LI>Istanzia una nuova entit&agrave; come entit&agrave; corrente.
     * <LI>Applica i valori di default all&rsquo;entit&agrave;.
     * </OL>
     * 
     * @see #newEntity
     * @see #setDefaults
     */
    public void prepareAddNew() {
        myModel = newEntity();
        setDefaults(myModel);
    }
    
    /**
     * Comando {@code save}.
     * 
     * <P>La versione di default del metodo {@code save} esegue le seguenti
     * operazioni:</P>
     * 
     * <OL>
     * <LI>Registra l&rsquo;entit&agrave; corrente come nuova entit&agrave;.
     * <LI>Metodo {@link #view}.
     * </OL>
     *  
     * @return Risultato.
     * @see    #saveEntity
     */
    public String save() {
        K entityId;
        
        saveEntity(myModel);
        entityId = getEntityId(myModel);
        if (entityId == null) {
            throw new PropertyNotSetException(myModel.toString(),
                    EntityFormAction.VIEWSTATE_ENTITYID);
        }
        
        getViewState().put(EntityFormAction.VIEWSTATE_ENTITYID,
                entityId.toString());
        getUserInterface().display(MessageType.INFORMATION,
                getText("message.save"));

        return view();
    }
    
    /**
     * Preparazione per il comando {@link #save}.
     * 
     * <P>L&rsquo;implementazione di default del metodo {@code prepareSave}
     * istanzia una nuova entit&agrave; come entit&agrave; corrente.</P>
     * 
     * @see #newEntity
     */
    public void prepareSave() {
        myModel = newEntity();
    }    
    
    /**
     * Comando {@code view}.
     * 
     * <P>La versione di default del metodo {@code view} imposta la
     * modalit&agrave; del form a {@code view}.</P> 
     * 
     * @return Risultato.
     * @see    #VIEWSTATE_FORMMODE
     */
    public String view() {
        getViewState().put(EntityFormAction.VIEWSTATE_FORMMODE,
                EntityFormAction.MODE_VIEW);        
    
        return Action.SUCCESS;
    }    
    
    /**
     * Preparazione per il comando {@link #view}.
     * 
     * <P>L&rsquo;implementazione di default del metodo {@code prepareView}
     * esegue le seguenti operazioni:</P>
     * 
     * <OL>
     * <LI>Legge l&rsquo;entit&agrave; corrente dallo strato di persistenza.
     * </OL>
     *  
     * @see #getModelId
     * @see #loadEntity
     */
    public void prepareView() {
        loadModel();
    }
    
    /**
     * Comando {@code edit}.
     * 
     * <P>La versione di default del metodo {@code edit} imposta la
     * modalit&agrave; del form a {@code edit}.</P> 
     *  
     * @return Risultato.
     * @see    #VIEWSTATE_FORMMODE
     */
    public String edit() {        
        getViewState().put(EntityFormAction.VIEWSTATE_FORMMODE,
                EntityFormAction.MODE_EDIT);
    
        return Action.SUCCESS;
    }
    
    /**
     * Preparazione per il comando {@link #edit}.
     * 
     * <P>L&rsquo;esecuzione del metodo {@code prepareEdit} rimanda a quella del
     * metodo {@link #prepareView}.</P>
     */
    @Final
    public void prepareEdit() {
        prepareView();
    }
    
    /**
     * Comando {@code update}.
     * 
     * <P>La versione di default del metodo {@code update} esegue le seguenti
     * operazioni:</P>
     * 
     * <OL>
     * <LI>Aggiorna l&rsquo;entit&agrave; corrente sullo strato di persistenza.
     * <LI>Metodo {@link #view}.
     * </OL>
     *  
     * @return Risultato.
     * @see    #updateEntity
     */
    public String update() {
        updateEntity(myModel);
        getUserInterface().display(MessageType.INFORMATION,
                getText("message.update"));
        
        return view();
    }  
    
    /**
     * Preparazione per il comando {@link #update}.
     * 
     * <P>L&rsquo;implementazione di default del metodo {@code prepareUpdate}
     * esegue le seguenti operazioni:</P>
     * 
     * <OL>
     * <LI>Legge l&rsquo;entit&agrave; corrente dallo strato di persistenza.
     * <LI>Verifica l&rsquo;eventuale concorrenza nell&rsquo;accesso
     * all&rsquo;entit&agrave;.
     * </OL>
     * 
     * @see #getModelId
     * @see #loadEntity
     * @see it.scoppelletti.programmerpower.data.OptimisticLockSupport    
     */
    public void prepareUpdate() {
        prepareUpdateImpl();
    }
    
    /**
     * Implementazione di default del metodo {@code prepareUpdate}.
     */
    private void prepareUpdateImpl() {
        loadModel();
        checkVersion();     
    }
    
    /**
     * Comando {@code delete}.
     * 
     * <P>La versione di default del metodo {@code delete} esegue le seguenti 
     * operazioni:</P>
     * 
     * <OL>
     * <LI>Cancella l&rsquo;entit&agrave; corrente sullo strato di persistenza.
     * <LI>Metodo {@link #addNew}.
     * </OL>
     *  
     * @return Risultato.
     * @see    #deleteEntity
     */
    public String delete() {
        deleteEntity(myModel);
        getUserInterface().display(MessageType.INFORMATION,
                getText("message.delete"));
        
        return addNew();
    }  
    
    /**
     * Preparazione per il comando {@link #delete}.
     * 
     * <P>L&rsquo;implementazione del metodo {@code prepareDelete} esegue le
     * seguenti operazioni:</P>
     * 
     * <OL>
     * <LI>Legge l&rsquo;entit&agrave; corrente dallo strato di persistenza.
     * <LI>Verifica l&rsquo;eventuale concorrenza nell&rsquo;accesso
     * all&rsquo;entit&agrave;.
     * </OL>
     * 
     * @see #getModelId
     * @see #loadEntity
     * @see it.scoppelletti.programmerpower.data.OptimisticLockSupport
     */
    @Final
    public void prepareDelete() {
        prepareUpdateImpl();
    }
}

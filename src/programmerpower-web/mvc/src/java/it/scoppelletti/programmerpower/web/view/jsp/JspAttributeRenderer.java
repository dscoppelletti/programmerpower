/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.jsp;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.tiles.*;
import org.apache.tiles.context.*;
import org.apache.tiles.renderer.impl.*;
import org.apache.tiles.servlet.context.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Rendering Tiles di una pagina
 * <ACRONYM TITLE="Java Servlet Pages">JSP</ACRONYM>.
 * 
 * @since 1.0.0
 */
public final class JspAttributeRenderer extends TemplateAttributeRenderer {
    // - Tiles 2.2.2, FreeMarker 2.3.18
    // L'inclusione di una pagina JSP in un modello FreeMarker attraverso
    // la direttiva Tiles <@insertAttribute> non processa la pagina JSP e 
    // inserisce invece il codice sorgente della stessa come pagina HTML
    // statica.
    
    /**
     * Tipo di attributo pagina JSP. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */    
    public static final String NAME = "jsp";
    
    /**
     * Costruttore.
     */
    public JspAttributeRenderer() {        
    }
    
    /**
     * Rendering di un valore di un attributo.
     * 
     * @param value    Valore.
     * @param attr     Attributo.
     * @param reqTiles Richiesta Tiles.
     */    
    @Override
    public void write(Object value, Attribute attr,
            TilesRequestContext reqTiles) throws IOException {  
        String path;
        ServletTilesRequestContext servletReq;
        HttpServletRequest httpReq;
        HttpServletResponse httpResp;
        RequestDispatcher dispatcher;
        
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        if (!(value instanceof String)) {
            throw new InvalidCastException(value.getClass().getName(),
                    "String");
        }
        path = (String) value;
        
        servletReq = ServletUtil.getServletRequest(reqTiles);        
        httpReq = servletReq.getRequest();        
        httpResp = servletReq.getResponse();
        
        dispatcher = httpReq.getRequestDispatcher(path);
        if (dispatcher == null) {
            httpResp.sendError(404, String.format("Result %1$s not found.",
                    path));
            return;
        }
        
        try {
            dispatcher.include(httpReq, httpResp);
        } catch (ServletException ex) {
            throw new IOException(ApplicationException.toString(ex), ex);
        }                
    }      
}

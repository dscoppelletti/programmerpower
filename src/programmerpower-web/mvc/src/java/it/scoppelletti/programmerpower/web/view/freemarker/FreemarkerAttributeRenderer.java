/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import java.io.*;
import javax.servlet.http.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.inject.Container;
import freemarker.template.*;
import org.apache.struts2.*;
import org.apache.struts2.views.freemarker.*;
import org.apache.tiles.Attribute;
import org.apache.tiles.context.*;
import org.apache.tiles.renderer.impl.*;
import org.apache.tiles.servlet.context.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Rendering Tiles di un modello FreeMarker.
 * 
 * <P>Il rendering &egrave; delegato alla classe Struts 2
 * {@code FreemarkerResult}.</P>
 * 
 * @since 1.0.0
 */
public final class FreemarkerAttributeRenderer extends
    AbstractTypeDetectingAttributeRenderer {
    private static final String EXT = ".ftl";
    
    /**
     * Tipo di attributo modello FreeMarker. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NAME = "freemarker";
    
    /**
     * Costruttore.
     */
    public FreemarkerAttributeRenderer() {        
    }
    
    /**
     * Rendering di un valore di un attributo.
     * 
     * @param value    Valore.
     * @param attr     Attributo.
     * @param reqTiles Richiesta Tiles.
     */
    @Override
    public void write(Object value, Attribute attr,
            TilesRequestContext reqTiles) throws IOException {
        String path;
        ServletTilesRequestContext servletReq;
        HttpServletRequest httpReq;
        HttpServletResponse httpResp;
        ExternalWriterHttpServletResponse respWriter;
        ActionContext actionCtx;
        ActionInvocation invocation;
        Container container;
        FreemarkerResult result;
        
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        if (!(value instanceof String)) {
            throw new InvalidCastException(value.getClass().getName(),
                    "String");
        }
        path = (String) value;
        
        servletReq = ServletUtil.getServletRequest(reqTiles);        
        httpReq = servletReq.getRequest();
        httpResp = servletReq.getResponse();
        respWriter = new ExternalWriterHttpServletResponse(httpResp,
                reqTiles.getPrintWriter());
        
        actionCtx = ServletActionContext.getActionContext(httpReq);
        invocation = actionCtx.getActionInvocation();
        
        result = new FreemarkerResult();
        result.setWriter(respWriter.getWriter());        
        
        container = actionCtx.getContainer();
        container.inject(result);

        try {                
            result.doExecute(path, invocation);
        } catch (TemplateException ex) {
            throw new IOException(ApplicationException.toString(ex), ex);
        }            
    }
    
    /**
     * Verifica se un attributo pu&ograve; essere gestito dalla classe
     * {@code FreemarkerAttributeRenderer}.
     * 
     * @param  value Valore.
     * @param  attr  Attributo.
     * @param  req   Richiesta.
     * @return       Esito della verifica.
     */
    @Override
    public boolean isRenderable(Object value, Attribute attr,
            TilesRequestContext req) {
        String path;
        
        if (value instanceof String) {
            path = (String) value;
            return path.startsWith("/") && path.endsWith(
                    FreemarkerAttributeRenderer.EXT);
        }
        
        return false;
    }    
}

/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.struts2.views.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.web.view.freemarker.*;

/**
 * Libreria dei tag Programmer Power pubblicati in Struts 2.
 * 
 * @since 1.0.0
 */
@Final
public class WebTagLibrary implements TagLibraryModelProvider {

    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_NAME = "web";

    /**
     * Costruttore.
     */
    public WebTagLibrary() { 
    }
    
    /**
     * Restituisce il modello delle direttive FreeMarker.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta.
     * @return            Oggetto.
     */
    public Object getModels(ValueStack valueStack, HttpServletRequest req,
            HttpServletResponse resp) {
        return new FreemarkerModelRepository(valueStack, req, resp);
    }
}

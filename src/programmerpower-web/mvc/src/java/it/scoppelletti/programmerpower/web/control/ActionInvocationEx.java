/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.util.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.config.entities.*;
import com.opensymphony.xwork2.interceptor.*;
import org.slf4j.*;

/**
 * Stato di esecuzione di un&rsquo;azione.
 * 
 * <P>La classe {@code ActionInvocationEx} estende la classe Struts 2
 * {@code DefaultActionInvocation} con le seguenti fuzioni:</P>
 * 
 * <OL>
 * <LI>Supporto dell&rsquo;interfaccia {@code InterceptorExclusionSupport}
 * eventualmente implementata dall&rsquo;azione.
 * <LI>Supporto dell&rsquo;interfaccia Struts 2 {@code PreResultListener}
 * eventualmente implementata dall&rsquo;azione.
 * </OL>
 * 
 * @see   it.scoppelletti.programmerpower.web.control.ActionProxyFactoryEx#createActionProxy(String, String, String, Map, boolean, boolean)
 * @see   it.scoppelletti.programmerpower.web.control.InterceptorExclusionSupport
 * @since 1.0.0
 */
public class ActionInvocationEx extends DefaultActionInvocation {

    /**
     * Intercettore {@code params}.
     */
    public static final String INTERCEPTOR_PARAMS = "params";
    
    /**
     * Intercettore {@code validation}.
     */
    public static final String INTERCEPTOR_VALIDATION = "validation";
    
    /**
     * Intercettore {@code workflow}.
     */
    public static final String INTERCEPTOR_WORKFLOW = "workflow";
    
    private static final long serialVersionUID = 1L;
    private static final Logger myLogger = LoggerFactory.getLogger(
            ActionInvocationEx.class);

    /**
     * Costruttore.
     * 
     * @param extraContext Collezione del parametri extra.
     */
    public ActionInvocationEx(final Map<String, Object> extraContext) {
        super(extraContext, true);                
    }
    
    /**
     * Inizializzazione.
     * 
     * @param proxy Proxy per l&rsquo;esecuzione dell&rsquo;azione.
     */
    @Override
    public void init(ActionProxy proxy) {
        Object action;
        
        super.init(proxy);
        applyExcludeInterceptors(proxy);
        
        action = proxy.getAction();
        if (action instanceof PreResultListener) {
            addPreResultListener((PreResultListener) action);
        }
    }
    
    /**
     * Restituisce lo stack degli intercettori.
     * 
     * @return Collezione.
     */
    private Iterator<InterceptorMapping> getInterceptors() {
        return this.interceptors;
    }

    /**
     * Imposta lo stack degli intercettori.
     * 
     * @param stack Collezione.
     */
    private void setInterceptors(Iterator<InterceptorMapping> stack) {
        this.interceptors = stack;
    }
    
    /**
     * Applica le eventuali annotazioni {@code ExcludeInterceptors} applicate
     * sul metodo da eseguire dell&rsquo;azione.
     * 
     * @param proxy Proxy per l&rsquo;esecuzione dell&rsquo;azione.
     */
    private void applyExcludeInterceptors(ActionProxy proxy) {
        InterceptorExclusionSupport action;
        MethodFilterInterceptor methodFilter;
        InterceptorMapping interceptor;
        List<InterceptorMapping> interceptorList;
        
        if (!(proxy.getAction() instanceof InterceptorExclusionSupport)) {
            return;
        }
        
        action = (InterceptorExclusionSupport) proxy.getAction();                        
        interceptorList = new ArrayList<InterceptorMapping>();
        while (getInterceptors().hasNext()) {
            interceptor = getInterceptors().next();
            
            if (interceptor.getInterceptor() instanceof
                    MethodFilterInterceptor) {
                methodFilter = (MethodFilterInterceptor)
                        interceptor.getInterceptor();
                if (!methodFilter.getIncludeMethodsSet().isEmpty()) {
                    interceptorList.add(interceptor);
                    myLogger.trace("Collection includeMethods is not empty " +
                            "for interceptor {}: ignoring " +
                            "InterceptorExclusionSupport interface on class " +
                            "{}.", interceptor.getName(),
                            action.getClass().getName());
                    continue;
                }
            }
            
            if (action.isInterceptorExcludedForMethod(interceptor.getName(),
                    proxy.getMethod())) {                
                myLogger.trace("Interceptor {} is excluded by " +
                        "InterceptorExclusionSupport interface of class {} " +
                        "for method {}.", new Object[] { interceptor.getName(),
                        action.getClass().getName(), proxy.getMethod() });           
                continue;
            }
            
            interceptorList.add(interceptor);
        }
        
        setInterceptors(interceptorList.iterator());
    }
}

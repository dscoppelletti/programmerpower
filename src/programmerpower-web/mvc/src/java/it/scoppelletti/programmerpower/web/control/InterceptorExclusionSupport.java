/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

/**
 * Supporto dell&rsquo;esclusione di alcuni intercettori per l&rsquo;esecuzione
 * di un&rsquo;azione.
 * 
 * @see   it.scoppelletti.programmerpower.web.control.ActionInvocationEx
 * @since 1.0.0
 */
public interface InterceptorExclusionSupport {
    
    /**
     * Verifica se un intercettore deve essere escluso per l&rsquo;esecuzione
     * di un metodo.
     *  
     * @param  interceptor Nome dell&rsquo;intercettore.
     * @param  method      Nome del metodo.
     * @return             Esito della verifica.
     */
    boolean isInterceptorExcludedForMethod(String interceptor, String method);
}

/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.tiles;

import org.apache.tiles.template.*;
import org.apache.tiles.freemarker.template.*;

/**
 * Modello delle direttive FreeMarker che implementano i tag Tiles. 
 */
final class TilesModelRepository extends TilesFMModelRepository {    
    private final InsertAttributeFMModel myInsertAttribute;

    /**
     * Costruttore.
     */
    TilesModelRepository() {
        myInsertAttribute = new InsertAttributeFMModel(new InsertAttributeModel(
                new AttributeResolverEx())); 
    }

    /**
     * Restituisce la direttiva <CODE>&lt;&#64;insertAttribute&gt;</CODE>.
     * 
     * @return Oggetto.
     */
    @Override
    public InsertAttributeFMModel getInsertAttribute() {
        return myInsertAttribute;
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.struts2.components.*;
import org.apache.struts2.views.freemarker.tags.*;
import org.springframework.beans.factory.config.*;
import org.springframework.context.*;
import org.springframework.web.context.support.*;
import it.scoppelletti.programmerpower.spring.config.*;

/**
 * Classe di base per la versione FreeMarker di un componente Struts 2.
 * 
 * @since 2.0.0
 */
public abstract class AbstractTagModel extends TagModel {
    private final AutowireCapableBeanFactory myBeanFactory;
    
    /**
     * Costruttore.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta. 
     */
    protected AbstractTagModel(ValueStack valueStack, HttpServletRequest req,
            HttpServletResponse resp) {
        super(valueStack, req, resp);
        
        ApplicationContext applCtx;
        
        applCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(
                req.getServletContext());
        myBeanFactory = applCtx.getAutowireCapableBeanFactory();        
    }
    
    /**
     * Restituisce lo stack dei valori.
     * 
     * @return Oggetto.
     */
    private ValueStack getValueStack() {
        return this.stack;
    }
    
    /**
     * Restituisce la richiesta.
     * 
     * @return Oggetto.
     */
    private HttpServletRequest getRequest() {
        return this.req;
    }
    
    /**
     * Restituisce la risposta.
     * 
     * @return Oggetto.
     */
    private HttpServletResponse getResponse() {
        return this.res;
    }
    
    /**
     * Restituisce il componente che implementa la direttiva.
     * 
     * @return Oggetto.
     */
    @Override
    protected final Component getBean() {
        Component c;
        
        c = newBean(getValueStack(), getRequest(), getResponse());
        myBeanFactory.autowireBean(c);
        c = (Component) myBeanFactory.initializeBean(c,
                BeanConfigTools.getBeanName(c.getClass()));
        
        return c;
    }    
    
    /**
     * Istanzia il componente che implementa la direttiva.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta.
     * @return            Oggetto.
     */        
    protected abstract Component newBean(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp);       
}

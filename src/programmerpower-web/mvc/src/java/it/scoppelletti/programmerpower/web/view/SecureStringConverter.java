/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.util.*;
import org.apache.commons.lang3.*;
import org.apache.struts2.util.*;
import it.scoppelletti.programmerpower.security.*;

/**
 * Convertitore delle stringhe sicure.
 * 
 * @see   it.scoppelletti.programmerpower.security.SecureString
 * @since 1.0.0
 */
public final class SecureStringConverter extends StrutsTypeConverter {
    
    /**
     * Costruttore.
     */
    public SecureStringConverter() {       
    }
    
    /**
     * Converte una stringa in oggetto.
     * 
     * @param  context Contesto.
     * @param  values  Vettore di stringhe da convertire. &Egrave; considerato
     *                 il solo primo elemento.
     * @param  toClass Tipo nel quale convertire il vettore di stringhe. Il tipo 
     *                 specificato &egrave; ignorato e la stringa &egrave;
     *                 sempre convertita in un oggetto {@code SecureString}.
     * @return         Oggetto risultante.                                
     */
    @SuppressWarnings("rawtypes")
    public Object convertFromString(Map context, String[] values,
            Class toClass) {        
        if (ArrayUtils.isEmpty(values)) {
            return null;
        }
        
        return new SecureString(values[0]);
    }

    /**
     * Converte un oggetto in stringa.
     * 
     * @param  context Contesto.
     * @param  obj     Oggetto.
     * @return         Stringa risultante.
     */
    @SuppressWarnings("rawtypes")
    public String convertToString(Map context, Object obj) {        
        SecureString s = (SecureString) obj;
                       
        return s.toString();        
    }    
}

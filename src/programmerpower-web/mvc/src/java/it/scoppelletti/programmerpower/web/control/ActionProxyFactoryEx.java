/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.util.*;
import javax.servlet.http.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.inject.*;
import org.apache.struts2.*;
import org.apache.struts2.impl.*;
import it.scoppelletti.programmerpower.*;

/**
 * Classe di factory degli oggetti proxy per l&rsquo;esecuzione delle azioni.
 * 
 * <P>L&rsquo;utilizzo della classe di factory {@code ActionProxyFactoryEx}
 * pu&ograve; essere attivato impostando l&rsquo;apposita costante 
 * {@code struts.actionProxyFactory} con il nome del bean
 * {@code it-scoppelletti-programmerpower-web-control-actionProxyFactory}.</P>
 * 
 * @since 1.0.0
 */
public class ActionProxyFactoryEx extends StrutsActionProxyFactory {

    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String BEAN_NAME =
            "it-scoppelletti-programmerpower-web-control-actionProxyFactory";
    
    /**
     * Parametro {@code session}: Collezione degli attributi di sessione
     * acceduta dall&rsquo;oggetto Struts 2 {@code AttributeMap}.
     */
    private static final String SESSION = "session";
    
    /**
     * Costruttore.
     */
    public ActionProxyFactoryEx() {        
    }
 
    /**
     * Restituisce il container.
     * 
     * @return Oggetto.
     */
    private Container getContainer() {
        return this.container;
    }
    
    /**
     * Crea l&rsquo;oggetto proxy per l&rsquo;esecuzione di un&rsquo;azione.
     * 
     * <P>Questa versione prevalente del metodo {@code createActionProxy}
     * istanzia un oggetto {@code ActionInvocationEx} per rappresentare lo stato
     * di esecuzione dell&rsquo;azione.</P>
     * 
     * @param  namespace      Spazio dei nomi.
     * @param  actionName     Nome dell&rsquo;azione.
     * @param  methodName     Nome del metodo.
     * @param  extraContext   Collezione del parametri extra.
     * @param  executeResult  Indica se eseguire il risultato.
     * @param  cleanupContext Indica se il contesto originale deve essere
     *                        preservato durante l&rsquo;esecuzione
     *                        dell&rsquo;azione.
     * @return                Oggetto di proxy.
     * @see    it.scoppelletti.programmerpower.web.control.ActionInvocationEx
     */
    @Override
    public ActionProxy createActionProxy(String namespace, String actionName,
            String methodName, Map<String, Object> extraContext,
            boolean executeResult, boolean cleanupContext) {
        HttpServletRequest req;
        ActionInvocation inv;
        Map<Object, Object> sessionMap;
        
        if (extraContext == null) {
            throw new ArgumentNullException("extraContext");
        }
        
        // Sostituisco l'implementazione Struts 2 della collezione degli
        // attributi di sessione con la mia
        req = (HttpServletRequest) extraContext.get(StrutsStatics.HTTP_REQUEST);
        sessionMap = new SessionMapEx<Object, Object>(req);
        extraContext.put(ActionContext.SESSION, sessionMap);
        extraContext.put(ActionProxyFactoryEx.SESSION, sessionMap);
        
        inv = new ActionInvocationEx(extraContext);
        getContainer().inject(inv);
        
        return createActionProxy(inv, namespace, actionName, methodName,
                executeResult, cleanupContext);
    }     
}

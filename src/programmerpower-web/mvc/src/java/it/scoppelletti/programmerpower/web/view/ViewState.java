/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Stato di una vista.
 * 
 * @since 1.0.0
 */
@Final
public class ViewState implements Serializable, Map<String, String> {
    private static final long serialVersionUID = 1L;
    
    private transient Map<String, String> myProps;
    
    /**
     * Costruttore.
     */
    public ViewState() {
        myProps = new HashMap<String, String>();
    }

    /**
     * Serializza l&rsquo;oggetto.
     * 
     * @param      out Flusso di scrittura.
     * @serialData     Formato di default seguito dalle propriet&agrave;:
     *                 
     * <P><OL>
     * <LI>Numero delle propriet&agrave;.
     * <LI>Sequenza delle coppie (chiave, valore).
     * </OL></P>                  
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        
        out.writeInt(myProps.size());
        for (Map.Entry<String, String> prop : myProps.entrySet()) {
            out.writeObject(prop.getKey());
            out.writeObject(prop.getValue());
        }
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param in Flusso di lettura.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {        
        int i, n;
        
        in.defaultReadObject();        
        
        myProps = new HashMap<String, String>();
        n = in.readInt();
        for (i = 0; i < n; i++) {
            myProps.put((String) in.readObject(), (String) in.readObject()); 
        }        
    }
    
    /**
     * Restituisce un valore.
     * 
     * @param  key Chiave.
     * @return     Valore. Se l&rsquo;elemento non &egrave; presente nella
     *             collezione restituisce {@code null}. 
     */
    public String get(Object key) {
        return myProps.get(key);
    }
    
    /**
     * Assegna un valore ad una chiave.
     * 
     * @param  key   Chiave.
     * @param  value Valore.
     * @return       Se esisteva gi&agrave; un elemento con la stessa chiave, ne
     *               restituisce il valore, altrimenti restituisce {@code null}.
     */
    public String put(String key, String value) {
        return myProps.put(key, value);
    }
    
    /**
     * Rimuove un elemento.
     * 
     * @param  key Chiave.
     * @return     Se esisteva un elemento assegnato alla chiave, ne restituisce
     *             il valore, altrimentir restituisce {@code null}.
     */
    public String remove(Object key) {
        return myProps.remove(key);
    }

    /**
     * Rimuove tutti gli elementi.
     */
    public void clear() {
        myProps.clear();
    }
    
    /**
     * Restituisce il numero di elementi.
     * 
     * @return Valore.
     */
    public int size() {
        return myProps.size();
    }

    /**
     * Indica se la collezione &egrave; vuota.
     * 
     * @return Indicatore.
     */
    public boolean isEmpty() {
        return myProps.isEmpty();
    }

    /**
     * Verifica se una chiave &egrave; presente nella collezione.
     * 
     * @param  key Chiave.
     * @return     Esito della verifica.
     */
    public boolean containsKey(Object key) {
        return myProps.containsKey(key);
    }

    /**
     * Verifica se un valore &egrave; presente nella collezione.
     * 
     * @param  value Valore.
     * @return       Esito della verifica.
     */
    public boolean containsValue(Object value) {
        return myProps.containsValue(value);
    }

    /**
     * Restituisce la collezione delle chiavi.
     * 
     * @return Collezione.
     */
    public Set<String> keySet() {
        return myProps.keySet();
    }

    /**
     * Restituisce la collezione dei valori.
     * 
     * @return Collezione.
     */    
    public Collection<String> values() {
        return myProps.values();
    }

    /**
     * Restituisce la collezione degli elementi.
     * 
     * @return Collezione.
     */
    public Set<Map.Entry<String, String>> entrySet() {
        return myProps.entrySet();
    }
    
    /**
     * Copia tutti gli elementi da un&rsquo;altra collezione.
     * 
     * @param map Collezione sorgente.
     */
    public void putAll(Map<? extends String, ? extends String> map) {
        myProps.putAll(map);        
    }   
}

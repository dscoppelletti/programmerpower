/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import javax.servlet.*;
import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import freemarker.ext.jsp.*;
import freemarker.ext.servlet.*;
import freemarker.template.*;
import org.apache.struts2.views.freemarker.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.web.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Configurazione FreeMarker.
 * 
 * @since 1.0.0
 */
@Final
public class FreemarkerManagerEx extends FreemarkerManager {
    private static final String ATTR_APPLICATION_MODEL =
            ".freemarker.Application";
    private static final String ATTR_JSP_TAGLIBS_MODEL =
            ".freemarker.JspTaglibs";
    private static final String ATTR_REQUEST_MODEL = ".freemarker.Request";
    private static final String ATTR_REQUEST_PARAMETERS_MODEL =
            ".freemarker.RequestParameters";    
    
    private final Object mySyncRoot = new Object();
    private GenericServlet myServlet = null;
        
    /**
     * Costruttore.
     */
    public FreemarkerManagerEx() {
    }
    
    /**
     * Costruisce il modello di dati da trasmettere a FreeMarker.
     * 
     * @param  servletCtx   Contesto dell&rsquo;applicazione.
     * @param  req          Richiesta.
     * @param  resp         Risposta.
     * @param  modelWrapper Oggetto di wrapper.
     * @param  valueStack   Stack dei valori.
     * @return              Modello di dati.
     */
    @Override
    protected ScopesHashModel buildScopesHashModel(ServletContext servletCtx,
            HttpServletRequest req, HttpServletResponse resp,
            ObjectWrapper modelWrapper, ValueStack valueStack) {
        HttpSession session;  
        ScopesHashModel model;
        ServletContextHashModel servletModel;
        HttpRequestHashModel reqModel;
        HttpRequestParametersHashModel reqParamsModel;
        TaglibFactory taglibs;
        AttributeMap map;
        
        model = new ScopesHashModel(modelWrapper, servletCtx, req, valueStack);
    
        map = WebTools.getSynchronizedAttributeMap(servletCtx);
        servletModel = (ServletContextHashModel) map.getAttribute(
                FreemarkerManagerEx.ATTR_APPLICATION_MODEL,
                new FreemarkerManagerEx.ApplicationModelInitializer(this,
                servletCtx, modelWrapper));
        taglibs = (TaglibFactory) map.getAttribute(
                FreemarkerManagerEx.ATTR_JSP_TAGLIBS_MODEL,
                new FreemarkerManagerEx.TaglibInitializer(servletCtx));
        
        model.put(FreemarkerServlet.KEY_APPLICATION, servletModel);
        model.putUnlistedModel(FreemarkerServlet.KEY_APPLICATION_PRIVATE,
                servletModel);        
        model.put(FreemarkerServlet.KEY_JSP_TAGLIBS, taglibs);
        
        session = req.getSession(false);
        if (session != null) {
            model.put(FreemarkerServlet.KEY_SESSION,
                    new HttpSessionHashModel(session, modelWrapper));
        }
        
        reqModel = (HttpRequestHashModel) req.getAttribute(
                FreemarkerManagerEx.ATTR_REQUEST_MODEL);    
        if (reqModel == null || reqModel.getRequest() != req) {
            reqModel = new HttpRequestHashModel(req, resp, modelWrapper);
            req.setAttribute(FreemarkerManagerEx.ATTR_REQUEST_MODEL, reqModel);
        }
    
        model.put(FreemarkerServlet.KEY_REQUEST, reqModel);
        
        reqParamsModel = (HttpRequestParametersHashModel) req.getAttribute(
                FreemarkerManagerEx.ATTR_REQUEST_PARAMETERS_MODEL);
        if (reqParamsModel == null || reqModel.getRequest() != req) {
            reqParamsModel = new HttpRequestParametersHashModel(req);
            req.setAttribute(FreemarkerManagerEx.ATTR_REQUEST_PARAMETERS_MODEL,
                    reqParamsModel);
        }
    
        model.put(FreemarkerManagerEx.ATTR_REQUEST_PARAMETERS_MODEL,
                reqParamsModel);
        model.put(FreemarkerManagerEx.KEY_REQUEST_PARAMETERS_STRUTS,
                reqParamsModel);
                
        return model;        
    }
        
    /**
     * Restituisce l&rsquo;oggetto {@code Servlet} che rappresenta FreeMarker.
     * 
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @return            Servlet.
     */
    private GenericServlet getServlet(ServletContext servletCtx) throws
            ServletException {
        GenericServlet servlet;
        
        synchronized (mySyncRoot) {
            if (myServlet == null) {
                servlet = new FreemarkerServlet();
                servlet.init(new FreemarkerServletInitializer(servletCtx));                
                myServlet = servlet; 
            }
        
            return myServlet;
        }
    }
    
    /**
     * Inzializzatore del modello dell&rsquo;applicazione Web.
     */
    private static final class ApplicationModelInitializer implements
        RunnableWithResult<ServletContextHashModel> {

        private final FreemarkerManagerEx myFreemarkerMgr;
        private final ServletContext myServletCtx;
        private final ObjectWrapper myModelWrapper;
    
        /**
         * Costruttore.
         * 
         * @param freemarkerMgr Configurazione FreeMarker.
         * @param servletCtx    Contesto dell&rsquo;applicazione.
         * @param modelWrapper  Oggetto di wrapper.
         */
        ApplicationModelInitializer(FreemarkerManagerEx freemarkerMgr,
                ServletContext servletCtx, ObjectWrapper modelWrapper) {
            myFreemarkerMgr = freemarkerMgr;
            myServletCtx = servletCtx;
            myModelWrapper = modelWrapper;
        }
    
        public ServletContextHashModel run() {
            GenericServlet servlet;
            
            try {
                servlet = myFreemarkerMgr.getServlet(myServletCtx);
            } catch (ServletException ex) {
                throw new ApplicationException(ex);
            }
            
            return new ServletContextHashModel(servlet, myModelWrapper);            
        }        
    }    
    
    /**
     * Inizializzatore delle librerie di tag.
     */
    private static final class TaglibInitializer implements
            RunnableWithResult<TaglibFactory> {
        private final ServletContext myServletCtx;
        
        /**
         * Costruttore.
         * 
         * @param servletCtx Contesto dell&rsquo;applicazione.
         */
        TaglibInitializer(ServletContext servletCtx) {
            myServletCtx = servletCtx;
        }

        public TaglibFactory run() {
            return new TaglibFactory(myServletCtx);
        }
    }
}

/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

/**
 * Provider dell&rsquo;indirizzo di una pagina di guida per un&rsquo;azione.
 * 
 * @since 1.0.0
 */
public interface HelpUrlProvider {

    /**
     * Restituisce l&rsquo;indirizzo della pagina di guida.
     * 
     * @return Valore. Se la guida non &egrave; prevista, restituisce
     *         {@code null}.
     */
    String getHelpUrl();
}

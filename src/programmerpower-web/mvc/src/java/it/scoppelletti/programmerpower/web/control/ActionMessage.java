/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.control;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.diagnostic.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Messaggio di un&rsquo;azione.
 * 
 * <P>Un oggetto {@code ActionMessage} collega un evento di messaggistica
 * {@code MessageEvent} con una copia non modificabile della collezione degli
 * oggetti aggiunti al contesto di diagnosi del thread corrente; gli stessi
 * oggetti sono mantenuti rappresentati come stringhe risultando cos&igrave;
 * anch&rsquo;essi immutabili.</P>
 * 
 * @it.scoppelletti.tag.threadsafe
 * @see   it.scoppelletti.programmerpower.threading.ThreadContext#addDiagnosticContextObject
 * @see   it.scoppelletti.programmerpower.ui.MessageEvent
 * @since 1.0.0
 */
public final class ActionMessage implements Serializable {
    private static final long serialVersionUID = 1;
    
    /**
     * @serial Evento. 
     */
    private final MessageEvent myEvent;
    
    private transient List<String> myDiagnosticCtx;
    private transient volatile List<String> myLines;
    
    /**
     * Costruttore.
     * 
     * @param event Evento.
     */
    public ActionMessage(MessageEvent event) {
        Object obj;
        Iterator<Object> it;
        List<String> list;
        
        if (event == null) {
            throw new ArgumentNullException("event");
        }
        
        myEvent = event;
        
        list = new ArrayList<String>();        
        it = DiagnosticSystem.getInstance().getDiagnosticContext();
        while (it.hasNext()) {
            obj = it.next();
            list.add(String.valueOf(obj));
        }   
        myDiagnosticCtx = Collections.unmodifiableList(list);
        myLines = null;
    }
    
    /**
     * Serializza l&rsquo;oggetto.
     * 
     * @param      out Flusso di scrittura.
     * @serialData     Formato di default seguito dagli oggetti del contesto di
     *                 diagnosi collegato al messaggio:
     * 
     * <P><OL>
     * <LI>Numero degli oggetti del contesto di diagnosi.
     * <LI>Sequenza degli oggetti del contesto di diagnosi.
     * </OL></P> 
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        
        out.writeInt(myDiagnosticCtx.size());
        for (String obj : myDiagnosticCtx) {
            out.writeObject(obj);
        }
    }
    
    /**
     * Deserializza l&rsquo;oggetto.
     * 
     * @param in Flusso di lettura.
     */
    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        int i, n;
        List<String> list;
        
        in.defaultReadObject();
        
        list = new ArrayList<String>();
        n = in.readInt();
        for (i = 0; i < n; n++) {
            myDiagnosticCtx.add(String.valueOf(in.readObject()));
        }
        myDiagnosticCtx = Collections.unmodifiableList(list);
        
        myLines = null;
    }
    
    /**
     * Restituisce l&rsquo;evento.
     * 
     * @return Oggetto.
     */
    public MessageEvent getEvent() {
        return myEvent;
    }
    
    /**
     * Restituisce la collezione degli oggetti del contesto di diagnosi
     * collegati al messaggio.
     * 
     * @return Collezione.
     */
    public List<String> getDiagnosticContext() {
        return myDiagnosticCtx;
    }
    
    /**
     * Restituisce le linee di testo corrispondente al messaggio.
     *  
     * @return Collezione.
     */
    public List<String> getLines() {
        if (myLines == null) {
            myLines = Collections.unmodifiableList(initLines());
        }
        
        return myLines;
    }
    
    /**
     * Inizializza le linee di testo corrispondenti al messaggio.
     * 
     * @return Collezione.
     */
    private List<String> initLines() {
        String lastMsg, msg;
        Throwable ex;
        List<String> list;
        
        list = new ArrayList<String>();
        lastMsg = null;
        
        if (!StringUtils.isBlank(myEvent.getMessage())) {
            lastMsg = myEvent.getMessage();
            list.add(lastMsg);
        }            

        // Accodo anche la catena delle eccezioni annidate avendo pero' cura di
        // non scrivere piu' volte consecutivamente lo stesso messaggio nel caso
        // un'eccezione sia originata da un'altra eccezione solo per cambiarne
        // la "natura" (es. per farla diventare un'eccezione RuntimeException).        
        ex = myEvent.getThrowable();
        while (ex != null) {
            msg = ApplicationException.toString(ex);
            if (!StringUtils.isBlank(msg) && !msg.equals(lastMsg)) {
                list.add(msg);
                lastMsg = msg;
            }
            
            ex = ex.getCause();
        }

        // Accodo infine gli oggetti del contesto di diagnosi
        for (String obj : myDiagnosticCtx) {
            if (!StringUtils.isBlank(obj)) {
                list.add(obj);
            }
        }
        
        return list;
    }
}

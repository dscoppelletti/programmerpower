/*
 * Copyright (C) 2011-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Strato View nel modello
 * <ACRONYM TITLE="Model-View-Controller">MVC</ACRONYM>.
 * 
 * @it.scoppelletti.tag.module {@code it.scoppelletti.programmerpower.mvc}
 * @version                    2.1.0
 * @see     <A HREF="${it.scoppelletti.token.tilesUrl}" TARGET="_top">Apache
 *          Tiles</A>
 */
package it.scoppelletti.programmerpower.web.view.tiles;


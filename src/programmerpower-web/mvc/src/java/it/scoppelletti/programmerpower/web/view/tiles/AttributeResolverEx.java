/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.tiles;

import org.apache.tiles.*;
import org.apache.tiles.template.*;
import it.scoppelletti.programmerpower.web.view.jsp.*;

/**
 * Risoluzione di un attributo.
 */
final class AttributeResolverEx extends DefaultAttributeResolver {
    private static final String TYPE_TEMPLATE = "template";
    
    /**
     * Costruttore.
     */
    AttributeResolverEx() {            
    }
    
    /**
     * Rivolve un attributo.
     * 
     * @param  container        Container Tiles.
     * @param  attribute        Attributo.
     * @param  name             Nome dell&rsquo;attributo.
     * @param  role             Ruoli per i quali l&rsquo;attributo &egrave;
     *                          abilitato.
     * @param  ignore           Indica se ignorare
     *                          l&rsquo;impossibilit&agrave; di assegnare un
     *                          valore all&rsquo;attributo.
     * @param  defaultValue     Valore di default dell&rsquo;attributo.
     * @param  defaultValueRole Ruoli di default per i quali
     *                          l&rsquo;attributo &egrave; abilitato.
     * @param  defaultValueType Tipo del valore di default
     *                          dell&rsquo;attributo.
     * @param  reqItems         Elementi della richiesta.
     * @return                  Attributo risolto.
     */
    @Override
    public Attribute computeAttribute(TilesContainer container,
            Attribute attribute, String name, String role, boolean ignore,
            Object defaultValue, String defaultValueRole,
            String defaultValueType, Object... reqItems) {
        Attribute attr;
        
        attr = super.computeAttribute(container, attribute, name, role,
                ignore, defaultValue, defaultValueRole, defaultValueType,
                reqItems);            
        if (AttributeResolverEx.TYPE_TEMPLATE.equals(attr.getRenderer())) {
            attr = attr.clone(); // Non modifico l'attributo originale
            attr.setRenderer(JspAttributeRenderer.NAME);
        }
                    
        return attr;
    }
}


/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.components.ContextBean;
import org.apache.struts2.views.annotations.StrutsTag;
import org.apache.struts2.views.annotations.StrutsTagAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.io.IOOperationException;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.web.WebClient;

/**
 * <ACRONYM TITLE="Uniform Resource Locator">URL</ACRONYM>
 * dell&rsquo;<ACRONYM TITLE="Application Server">AS</ACRONYM>.
 * 
 * @see   it.scoppelletti.programmerpower.web.WebApplicationServer
 * @since 2.0.0
 */
@Final
@StrutsTag(name = "applServerUrl", 
        tldTagClass = "it.scoppelletti.programmerpower.web.view.jsp." +
                "ApplicationServerUrlTag",
        tldBodyContent = "empty",
        allowDynamicAttributes = false,
        description = "URL dell'application server.")
public class ApplicationServerUrlComponent extends ContextBean {
    private final HttpServletRequest myReq;
    private final HttpServletResponse myResp;
    private String mySecure;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     * 
     * @param valueStack Stack dei valori.
     * @param req        Richiesta.
     * @param resp       Risposta.
     */
    public ApplicationServerUrlComponent(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp) {
        super(valueStack);
        
        if (req == null) {
            throw new ArgumentNullException("req");
        }
        if (resp == null) {
            throw new ArgumentNullException("resp");
        }
        
        myReq = req;
        myResp = resp;
    }
    
    /**
     * Imposta l&rsquo;indicatore di URL sicuro.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(description = "Indicatore di URL sicuro.")
    public void setSecure(String value) {
        mySecure = value;
    }
    
    /**
     * Emette il tag.
     * 
     * @param  writer Flusso di scrittura.
     * @param  body   Contenuto.
     * @return        Indica se il contenuto deve essere ulteriormente valutato.
     */
    @Override
    public boolean end(Writer writer, String body) {
        String value, var;
        Boolean secure;
        WebClient webClient;
        
        webClient = myApplCtx.getBean(WebClient.BEAN_NAME, WebClient.class);
        
        secure = (mySecure == null) ? null : (Boolean) findValue(mySecure,
                Boolean.class);
        if (secure != null && secure.booleanValue()) {
            value = webClient.getServerSecureUri().toString();
        } else {
            value = webClient.getServerUri().toString();
        }
        
        value = myResp.encodeURL(value);
        var = getVar();
        
        if (StringUtils.isBlank(var)) {
            try {
                writer.write(value);
            } catch (IOException ex) {
                throw new IOOperationException(ex);
            }            
        } else {
            putInContext(value);
            myReq.setAttribute(var, value);
        }
        
        return super.end(writer, body);
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.jsp;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.struts2.components.*;
import org.apache.struts2.views.jsp.ui.*;
import org.springframework.beans.factory.config.*;
import org.springframework.context.*;
import org.springframework.web.context.support.*;
import it.scoppelletti.programmerpower.spring.config.*;

/**
 * Classe di base per la versione
 * <ACRONYM TITLE="Java Server Pages">JSP</ACRONYM> di un componente Struts 2.
 * 
 * @since 2.0.0
 */
public abstract class AbstractUIComponentTag extends AbstractUITag {
    private static final long serialVersionUID = 1L;
    
    /**
     * Costruttore.
     */
    protected AbstractUIComponentTag() {        
    }    
    
    /**
     * Restituisce il componente che implementa il tag.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta.
     * @return            Oggetto.
     */
    public final Component getBean(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp) {
        Component c;
        ApplicationContext applCtx;
        AutowireCapableBeanFactory beanFactory;
        
        applCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(
                req.getServletContext());
        beanFactory = applCtx.getAutowireCapableBeanFactory();
                
        c = newBean(valueStack, req, resp);
        beanFactory.autowireBean(c);
        c = (Component) beanFactory.initializeBean(c,
                BeanConfigTools.getBeanName(c.getClass()));
        
        return c;
    }
    
    /**
     * Istanzia il componente che implementa il tag.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta.
     * @return            Oggetto.
     */    
    protected abstract Component newBean(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp);
}

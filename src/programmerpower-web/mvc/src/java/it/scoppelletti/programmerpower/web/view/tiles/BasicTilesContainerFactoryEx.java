/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.tiles;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.tiles.*;
import org.apache.tiles.context.*;
import org.apache.tiles.definition.*;
import org.apache.tiles.evaluator.*;
import org.apache.tiles.factory.*;
import org.apache.tiles.jsp.context.*;
import org.apache.tiles.locale.*;
import org.apache.tiles.renderer.*;
import org.apache.tiles.renderer.impl.*;
import org.apache.tiles.servlet.context.*;
import org.apache.tiles.util.*;
import it.scoppelletti.programmerpower.web.view.freemarker.*;
import it.scoppelletti.programmerpower.web.view.jsp.*;

/**
 * Classe di factory del container Tiles.
 */
final class BasicTilesContainerFactoryEx extends BasicTilesContainerFactory {
     
    /**
     * Costruttore.
     */
    BasicTilesContainerFactoryEx() {            
    }
    
    /**
     * Definisce il processo di determinazione dell&rsquo;oggetto di
     * factory del contesto delle richieste.
     *
     * @param  parent Oggetto di factory parent.
     * @return        Collezione.
     */
    @Override
    protected List<TilesRequestContextFactory>
            getTilesRequestContextFactoriesToBeChained(
            ChainedTilesRequestContextFactory parent) {
        List<TilesRequestContextFactory> factories;
        
        factories = new ArrayList<TilesRequestContextFactory>();
        registerRequestContextFactory(
                ServletTilesRequestContextFactory.class.getName(),
                factories, parent);
        registerRequestContextFactory(
                JspTilesRequestContextFactory.class.getName(), factories,
                parent);
        registerRequestContextFactory(
                FreemarkerRequestContextFactory.class.getName(),
                factories, parent);
        
        return factories;
    }

    /**
     * Istanzia il rilevatore della localizzazione.
     * 
     * @param  applCtx    Contesto dell&rsquo;applicazione.
     * @param  ctxFactory Oggetto di factory del contesto delle richieste.
     * @return            Rilevatore.
     */
    @Override
    protected LocaleResolver createLocaleResolver(TilesApplicationContext
            applCtx, TilesRequestContextFactory ctxFactory) {
        return new TilesLocaleResolver();
    }
    
    /**
     * Registra i tipi di attributo.
     * 
     * @param rendererFactory      Oggetto di factory dei renderer.
     * @param applCtx              Contesto dell&rsquo;applicazione.
     * @param ctxFactory           Oggetto di factory del contesto delle
     *                             richieste.
     * @param container            Container.
     * @param attrEvaluatorFactory Oggetto di factory dei valutatori degli
     *                             attributi.
     */
    @Override
    protected void registerAttributeRenderers(
            BasicRendererFactory rendererFactory,
            TilesApplicationContext applCtx,
            TilesRequestContextFactory ctxFactory,
            TilesContainer container,
            AttributeEvaluatorFactory attrEvaluatorFactory) {
        FreemarkerAttributeRenderer freemarkerRenderer;
   
        super.registerAttributeRenderers(rendererFactory, applCtx,
                ctxFactory, container, attrEvaluatorFactory);            
        
        freemarkerRenderer = new FreemarkerAttributeRenderer();
        freemarkerRenderer.setApplicationContext(applCtx);
        freemarkerRenderer.setAttributeEvaluatorFactory(
                attrEvaluatorFactory);
        freemarkerRenderer.setRequestContextFactory(ctxFactory);
        rendererFactory.registerRenderer(FreemarkerAttributeRenderer.NAME,
                freemarkerRenderer);
        
        JspAttributeRenderer jspRenderer = new JspAttributeRenderer();
        jspRenderer.setApplicationContext(applCtx);
        jspRenderer.setAttributeEvaluatorFactory(attrEvaluatorFactory);
        jspRenderer.setRequestContextFactory(ctxFactory);
        rendererFactory.registerRenderer(JspAttributeRenderer.NAME,
                jspRenderer);            
    }
    
    /**
     * Definisce il processo di determinazione automatica del tipo degli
     * attributi.
     * 
     * @param rendererFactory      Oggetto di factory dei renderer.
     * @param applCtx              Contesto dell&rsquo;applicazione.
     * @param ctxFactory           Oggetto di factory del contesto delle
     *                             richieste.
     * @param container            Container.
     * @param attrEvaluatorFactory Oggetto di factory dei valutatori degli
     *                             attributi.
     * @return                     Oggetto.
     */        
    @Override
    protected AttributeRenderer createDefaultAttributeRenderer(
            BasicRendererFactory rendererFactory,
            TilesApplicationContext applCtx,
            TilesRequestContextFactory ctxFactory,
            TilesContainer container,
            AttributeEvaluatorFactory attrEvaluatorFactory) {            
        ChainedDelegateAttributeRenderer renderer =
            new ChainedDelegateAttributeRenderer();

        renderer.addAttributeRenderer((TypeDetectingAttributeRenderer)
                rendererFactory.getRenderer(
                BasicTilesContainerFactory.STRING_RENDERER_NAME));
        renderer.setApplicationContext(applCtx);
        renderer.setRequestContextFactory(ctxFactory);
        renderer.setAttributeEvaluatorFactory(attrEvaluatorFactory);
        
        return renderer;
    }
    
    /**
     * Rileva gli URL dei file delle definizioni.
     * 
     * @param  applCtx    Contesto dell&rsquo;applicazione.
     * @param  ctxFactory Oggetto di factory del contesto delle richieste.
     * @return            Collezione.          
     */
    @Override
    protected List<URL> getSourceURLs(TilesApplicationContext applCtx,
            TilesRequestContextFactory ctxFactory) {
        URL url;
        Set<URL> set, urlSet;            
        
        urlSet = new HashSet<URL>();
        try {
            set = applCtx.getResources(
                    TilesContextListener.MODULE_DEFINITIONS);
            if (set != null) {
                urlSet.addAll(set);
            }
            
            try {
                url = applCtx.getResource(
                        TilesContextListener.APPL_DEFINITIONS);
            } catch (FileNotFoundException ex) {
                url = null;
            }
            if (url != null) {
                urlSet.add(url);
            }
        } catch (IOException ex) {
            throw new DefinitionsFactoryException(ex.getMessage(), ex);
        }
        
        return URLUtil.getBaseTilesDefinitionURLs(urlSet); 
    }        
}       

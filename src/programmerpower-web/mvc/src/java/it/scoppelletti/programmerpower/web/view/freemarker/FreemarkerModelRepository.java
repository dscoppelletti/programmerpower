/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;

/**
 * Modello delle direttive FreeMarker che implementano i tag Programmer Power.
 *    
 * @since 1.0.0
 */
public final class FreemarkerModelRepository {
    private final ValueStack myValueStack;
    private final HttpServletRequest myReq;
    private final HttpServletResponse myResp;    
    private ApplicationServerUrlModel myApplServerUrl =  null;
    private AuthenticationModel myAuthentication = null;
    private AuthorizeModel myAuthorize = null;
    
    /**
     * Costruttore.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta.  
     */
    public FreemarkerModelRepository(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp) {
        myValueStack = valueStack;
        myReq = req;
        myResp = resp;
    }
        
    /**
     * Restituisce la direttiva <CODE>&lt;&#64;applServerUrl&gt;</CODE>.
     * 
     * @return Oggetto.
     * @since  2.0.0
     */
    public ApplicationServerUrlModel getApplServerUrl() {
        if (myApplServerUrl == null) {
            myApplServerUrl = new ApplicationServerUrlModel(myValueStack, myReq,
                    myResp);
        }
        
        return myApplServerUrl;
    }       
    
    /**
     * Restituisce la direttiva <CODE>&lt;&#64;authentication&gt;</CODE>.
     * 
     * @return Oggetto.
     * @since  2.0.0
     */
    public AuthenticationModel getAuthentication() {
        if (myAuthentication == null) {
            myAuthentication = new AuthenticationModel(myValueStack, myReq,
                    myResp);
        }
        
        return myAuthentication;
    }       
    
    /**
     * Restituisce la direttiva <CODE>&lt;&#64;authorize&gt;</CODE>.
     * 
     * @return Oggetto.
     * @since  2.0.0
     */
    public AuthorizeModel getAuthorize() {
        if (myAuthorize == null) {
            myAuthorize = new AuthorizeModel(myValueStack, myReq, myResp);
        }
        
        return myAuthorize;
    }       
}

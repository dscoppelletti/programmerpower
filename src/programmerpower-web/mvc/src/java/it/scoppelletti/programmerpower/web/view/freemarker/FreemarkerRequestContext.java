/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.web.view.freemarker;

import java.io.*;
import javax.servlet.http.*;
import com.opensymphony.xwork2.*;
import com.opensymphony.xwork2.inject.*;
import freemarker.core.*;
import freemarker.template.*;
import org.apache.struts2.*;
import org.apache.struts2.views.freemarker.*;
import org.apache.tiles.context.*;
import it.scoppelletti.programmerpower.*;

/**
 * Contesto di una richiesta FreeMarker.
 */
final class FreemarkerRequestContext extends TilesRequestContextWrapper
        implements TilesRequestContext {
    private Environment myFreemarkerEnv;
    private Object[] myItems;

    /**
     * Costruttore.
     * 
     * @param tilesReq      Richiesta Tiles.
     * @param freemarkerEnv Ambiente FreeMarker.
     */
    FreemarkerRequestContext(TilesRequestContext tilesReq,
            Environment freemarkerEnv) {
        super(tilesReq);
        myFreemarkerEnv = freemarkerEnv;
    }

    /**
     * Restituisce il flusso di scrittura.
     * 
     * @return Flusso.
     */
    @Override
    public Writer getWriter() throws IOException {
        return myFreemarkerEnv.getOut();
    }
    
    /**
     * Restituisce il flusso di stampa.
     * 
     * @return Flusso.
     */
    @Override
    public PrintWriter getPrintWriter() throws IOException {
        Writer writer = myFreemarkerEnv.getOut();
        
        if (writer instanceof PrintWriter) {
            return (PrintWriter) writer;
        }

        return new PrintWriter(writer);
    }

    /**
     * Restituisce gli elementi della richiesta.
     * 
     * @return Vettore.
     */
    @Override
    public Object[] getRequestObjects() {
        if (myItems == null) {
            myItems = new Object[1];
            myItems[0] = myFreemarkerEnv;
        }
        
        return myItems;
    }

    /**
     * Distribuisce la richiesta.
     * 
     * @param path Percorso del modello FreeMarker.
     */
    @Override
    public void dispatch(String path) throws IOException {
        include(path);
    }
    
    /**
     * Include la risposta della richiesta.
     * 
     * @param path Percorso del modello FreeMarker.
     */
    @Override
    public void include(String path) throws IOException {
        HttpServletRequest req;
        HttpServletResponse resp;
        ActionContext ctx;
        ActionInvocation invocation;
        Container container;
        FreemarkerResult result;
        TilesRequestContext requestCtx;
        
        requestCtx = getWrappedRequest();
        req = (HttpServletRequest) requestCtx.getRequestObjects()[0];
        resp = (HttpServletResponse) requestCtx.getRequestObjects()[1];
        
        ctx = ServletActionContext.getActionContext(req);
        invocation = ctx.getActionInvocation();
        
        result = new FreemarkerResult();
        result.setWriter(resp.getWriter()); 
        
        container = ctx.getContainer();
        container.inject(result);

        try {
            result.doExecute(path, invocation);
        } catch (TemplateException ex) {
            throw new IOException(ApplicationException.toString(ex), ex);
        }
    }
}    

<#--
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<#assign hasFieldErrors = parameters.name?? && fieldErrors?? && fieldErrors[parameters.name]??/>
<#include "/${parameters.templateDir}/${parameters.expandTheme}/fieldErrorList.ftl" />
<div<#rt/>
<#if hasFieldErrors>
 class="form-group has-error has-feedback"<#rt/>
<#else>
 class="form-group"<#rt/>
</#if>
>
	<div class="col-md-offset-2 col-md-10">
		<div class="checkbox">
        	<label>
				<input type="checkbox"<#rt/>
 name="${parameters.name?html}"<#rt/>
 value="${parameters.fieldValue?html}"<#rt/>
<#if parameters.cssClass??>
 class="${parameters.cssClass?html}"<#rt/>
</#if>  
<#if parameters.nameValue?? && parameters.nameValue>
 checked="checked"<#rt/>
</#if>
<#if parameters.disabled?default(false)>
 disabled="disabled"<#rt/>
</#if>
<#if parameters.readonly?default(false)>
 readonly="readonly"<#rt/>
</#if>
<#if parameters.tabindex??>
 tabindex="${parameters.tabindex?html}"<#rt/>
</#if>
<#if parameters.id??>
 id="${parameters.id?html}"<#rt/>
</#if>  								
<#if parameters.title??>
 title="${parameters.title?html}"<#rt/>
</#if>
<#include "/${parameters.templateDir}/${parameters.expandTheme}/common-attributes.ftl" />
<#include "/${parameters.templateDir}/${parameters.expandTheme}/dynamic-attributes.ftl" />
 /><#rt/>
<input type="hidden" id="__checkbox_${parameters.id?html}"<#rt/>
 name="__checkbox_${parameters.name?html}"<#rt/>
 value="${parameters.fieldValue?html}"<#rt/> 
<#if parameters.disabled?default(false)>
 disabled="disabled"<#rt/>
</#if>
 /><#rt/> 
<#if parameters.label??>
 ${parameters.label?html}<#rt/>
	<#if parameters.required?default(false)>
 <span class="text-danger">*</span><#rt/>
	</#if> 	
</#if>				
			</label>
		</div>
<#if hasFieldErrors>
 <span class="glyphicon glyphicon-remove form-control-feedback"></span>
</#if> 		
	</div>
<#include "/${parameters.templateDir}/${parameters.expandTheme}/controlfooter.ftl" />
			
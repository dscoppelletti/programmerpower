<#--
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<#include "/${parameters.templateDir}/${parameters.expandTheme}/controlheader.ftl" />
<div class="col-md-10">
	<input<#rt/>
 type="password"<#rt/>
 name="${parameters.name?default("")?html}"<#rt/>
<#if parameters.cssClass??>
 class="form-control ${parameters.cssClass?html}"<#rt/>
<#else>
 class="form-control"<#rt/>  
</#if>  
<#-- Non imposto l'attributo size per non sovrappormi alla classe form-control
<#if parameters.get("size")??>
 size="${parameters.get("size")?html}"<#rt/>
</#if>
-->
<#if parameters.maxlength??>
 maxlength="${parameters.maxlength?html}"<#rt/>
</#if>
<#if parameters.nameValue?? && parameters.showPassword?default(false)>
 value="${parameters.nameValue?html}"<#rt/>
</#if>
<#if parameters.disabled?default(false)>
 disabled="disabled"<#rt/>
</#if>
<#if parameters.readonly?default(false)>
 readonly="readonly"<#rt/>
</#if>
<#if parameters.tabindex??>
 tabindex="${parameters.tabindex?html}"<#rt/>
</#if>
<#if parameters.id??>
 id="${parameters.id?html}"<#rt/>
</#if>		
<#if parameters.title??>
 title="${parameters.title?html}"<#rt/>
</#if>
<#include "/${parameters.templateDir}/${parameters.expandTheme}/common-attributes.ftl" />
<#include "/${parameters.templateDir}/${parameters.expandTheme}/dynamic-attributes.ftl" />
 />
<#if hasFieldErrors>
 <span class="glyphicon glyphicon-remove form-control-feedback"></span>
</#if> 
</div>
<#include "/${parameters.templateDir}/${parameters.expandTheme}/controlfooter.ftl" />

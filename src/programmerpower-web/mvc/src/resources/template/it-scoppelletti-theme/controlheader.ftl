<#--
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<#assign hasFieldErrors = parameters.name?? && fieldErrors?? && fieldErrors[parameters.name]?? />
<#include "/${parameters.templateDir}/${parameters.expandTheme}/fieldErrorList.ftl" />
<div<#rt/>
<#if hasFieldErrors>
 class="form-group has-error has-feedback"<#rt/>
<#else>
 class="form-group"<#rt/>
</#if>
>
<#if parameters.label??>
	<label<#rt/>
	<#if parameters.name??>
 for="${parameters.name?html}"<#rt/>
	</#if>	
 class="col-md-2 control-label">${parameters.label?html}<#rt/>
	<#if parameters.required?default(false)>
 <span class="text-danger">*</span><#rt/>
	</#if>
${parameters.labelseparator?default(":")?html}<#t/>
</label>	    	
</#if>
  


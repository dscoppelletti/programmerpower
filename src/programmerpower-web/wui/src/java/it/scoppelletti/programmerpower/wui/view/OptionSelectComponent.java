/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package it.scoppelletti.programmerpower.wui.view;

import java.util.*;
import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.commons.lang3.*;
import org.apache.struts2.components.*;
import org.apache.struts2.views.annotations.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Selezione di opzioni.
 * 
 * @since 2.0.0
 */
@Final
@StrutsTag(name = "optionSelect", 
        tldTagClass = "it.scoppelletti.programmerpower.wui.view.jsp." +
                "OptionSelectTag",
        tldBodyContent = "empty",
        allowDynamicAttributes = false,
        description = "Selezione di opzioni.")
public class OptionSelectComponent extends UIBean {

    /**
     * Nome del modello che implementa la vista del componente. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String TEMPLATE = "optionSelect";
    
    /**
     * Parametro {@code optionNameValue}: Collezione delle opzioni supportate.
     */
    public static final String PARAM_OPTIONNAMEVALUE = "optionNameValue";
        
    private String myOptionName;
    private String myReadOnly;
    private int mySelectedSize;
    private int myAvailSize;
    private String mySelectedLabel;
    private String myAvailLabel;
    
    /** 
     * Costruttore.
     * 
     * @param valueStack Stack dei valori.
     * @param req        Richiesta.
     * @param resp       Risposta.
     */
    public OptionSelectComponent(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp) {
        super(valueStack, req, resp);
    }
    
    /**
     * Imposta il nome della collezione delle opzioni supportate.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(required = true,
            description = "Nome della collezione delle opzioni supportate.")
    public void setOptionName(String value) {
        myOptionName = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di controllo a sola lettura.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(description = "Indicatore di controllo a sola lettura.")
    public void setReadOnly(String value) {
        myReadOnly = value;
    }
       
    /**
     * Imposta il numero di opzioni visibili nella lista delle opzioni
     * selezionate.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(description = "Numero di opzioni visibili nella " +
            "lista delle opzioni selezionate.")
    public void setSelectedSize(int value) {
        mySelectedSize = value;
    }
    
    /**
     * Imposta il numero di opzioni visibili nella lista delle opzioni
     * disponibili.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(description = "Numero di opzioni visibili nella " +
            "lista delle opzioni disponibili.")
    public void setAvailableSize(int value) {
        myAvailSize = value;
    }
    
    /**
     * Imposta l&rsquo;etichetta delle opzioni selezionate.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(description = "Etichetta delle opzioni selezionate.")
    public void setSelectedLabel(String value) {
        mySelectedLabel = value;
    }
    
    /**
     * Imposta l&rsquo;etichetta delle opzioni disponibili.
     * 
     * @param value Valore.
     */
    @StrutsTagAttribute(description = "Etichetta delle opzioni disponibili.")    
    public void setAvailableLabel(String value) {
        myAvailLabel = value;
    }
    
    /**
     * Restituisce il nome del modello che implementa la vista del componente.
     * 
     * @return Valore.
     */
    protected String getDefaultTemplate() {
        return OptionSelectComponent.TEMPLATE;        
    }
    
    /**
     * Restituisce la classe del valore dell&rsquo;espressione {@code name}.
     * 
     * @return Classe.
     */
    @Override
    protected Class<?> getValueClassType() {
        return java.util.Set.class;
    }    
    
    /**
     * Imposta i parametri aggiuntivi.
     */
    @Override
    protected void evaluateExtraParams() {
        String value;
        Object obj;
 
        if (StringUtils.isBlank(this.name)) {
            throw new PropertyNotSetException(toString(), "name");
        }       
        if (getParameters().get("nameValue") == null) {
            throw new IllegalStateException("Selected option map not set.");
        }
        if (StringUtils.isBlank(myOptionName)) {
            throw new PropertyNotSetException(toString(), "optionName");
        }
        
        value = completeExpressionIfAltSyntax(findString(myOptionName));
        obj = findValue(value, Map.class);
        if (obj == null) {
            throw new IllegalStateException("Option map not set.");
        }        
        
        addParameter(OptionSelectComponent.PARAM_OPTIONNAMEVALUE, obj);
                
        if (myReadOnly != null) {
            addParameter("readOnly", findValue(myReadOnly, Boolean.class));
        }
        if (mySelectedSize > 0) {
            addParameter("selectedSize", mySelectedSize);
        }        
        if (myAvailSize > 0) {
            addParameter("availableSize", myAvailSize);
        }
        if (mySelectedLabel != null) {
            addParameter("selectedLabel", findString(mySelectedLabel));
        }
        if (myAvailLabel != null) {
            addParameter("availableLabel", findString(myAvailLabel));
        }
    }   
}

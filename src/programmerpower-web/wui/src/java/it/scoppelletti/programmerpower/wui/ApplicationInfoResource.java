/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui;

import org.restlet.representation.*;
import org.restlet.resource.*;

/**
 * Informazioni su un&rsquo;applicazione (nome, numero di versione, nota di
 * copyright, etc).
 * 
 * @see   it.scoppelletti.programmerpower.resources.ApplicationInfo
 * @since 1.0.0
 */
public interface ApplicationInfoResource {

    /**
     * Percorso della risorsa. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>. 
     */
    public static final String PATH = "/rest/about";
    
    /**
     * Propriet&agrave; {@code applName}: Nome dell&rsquo;applicazione.
     * 
     * @since 2.0.0
     */
    public static final String PROP_APPLNAME = "applName";
    
    /**
     * Propriet&agrave; {@code version}: Numero di versione
     * dell&rsquo;applicazione.
     * 
     * @since 2.0.0
     */
    public static final String PROP_VERSION = "version";
    
    /**
     * Propriet&agrave; {@code copyright}: Avviso di copyright.
     * 
     * @since 2.0.0
     */
    public static final String PROP_COPYRIGHT = "copyright";
    
    /**
     * Propriet&agrave; {@code license}: Nota di licenza.
     * 
     * @since 2.0.0
     */
    public static final String PROP_LICENSE = "license";
    
    /**
     * Propriet&agrave; {@code pageUrl}: URL della guida
     * dell&rsquo;applicazione.
     * 
     * @since 2.0.0
     */
    public static final String PROP_PAGEURL = "pageUrl";
    
    /**
     * Restituisce le informazioni sull&rsquo;applicazione.
     * 
     * @return Rappresentazione.
     * @since  2.0.0
     */
    @Get("json")
    Representation getApplicationInfo() throws ResourceException;
}

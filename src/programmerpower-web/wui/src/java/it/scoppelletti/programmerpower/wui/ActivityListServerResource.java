/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui;

import java.util.*;
import org.json.*;
import org.restlet.data.Status;
import org.restlet.ext.json.*;
import org.restlet.representation.*;
import org.restlet.resource.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.web.rest.*;

/**
 * Server della risorsa {@code ActivityListResource}.
 * 
 * @since 1.0.0
 */
@Final
@Reserved
public class ActivityListServerResource extends AbstractServerResource
    implements ActivityListResource {
    private static final Logger myLogger = LoggerFactory.getLogger(
            ActivityListServerResource.class);
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    /**
     * Costruttore.
     */
    public ActivityListServerResource() {        
    }
        
    public Representation listActivities() throws ResourceException {
        JSONObject obj;
        JSONArray items;
        
        try {
            obj = new JSONObject();
            items = listItems();
            obj.put(ActivityListResource.PROP_ITEMS, items);
        } catch (RuntimeException ex) {
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ApplicationException.toString(ex), ex);
        }
        
        return new JsonRepresentation(obj);
    }
    
    /**
     * Restituisce il vettore delle attivit&agrave;.
     * 
     * @return Vettore.
     */
    private JSONArray listItems() {
        JSONObject obj;
        JSONArray items;
        ActivityManager activityMgr;
        List<Activity> list;
        
        items = new JSONArray();
        
        activityMgr = initActivityManager();
        if (activityMgr == null) {
            return items;
        }
        
        list = activityMgr.listActivities();
        for (Activity activity : list) {
            obj = new JSONObject();
            obj.put(ActivityListResource.PROP_TITLE, activity.getTitle());
            obj.put(ActivityListResource.PROP_ACTIVITYURL,
                    activity.getActivityUrl());
            obj.put(ActivityListResource.PROP_ICONURL, activity.getIconUrl());
            
            items.put(obj);
        }
        
        return items;
    }
    
    /**
     * Inizializza il gestore delle attivit&agrave;
     * 
     * @return Oggetto. Se l&rsquo;applicazione non ha definito un gestore delle
     *         attivit&agrave;, restituisce {@code null}.
     */
    private ActivityManager initActivityManager() {
        ActivityManager activityMgr;
        
        try {
            activityMgr = myApplCtx.getBean(ActivityManager.BEAN_NAME,
                ActivityManager.class);
        } catch (RuntimeException ex) {
            myLogger.warn(String.format("Bean %1$s not found.",
                    ActivityManager.BEAN_NAME), ex);
            return null;
        }       
        
        return activityMgr;
    }       
}

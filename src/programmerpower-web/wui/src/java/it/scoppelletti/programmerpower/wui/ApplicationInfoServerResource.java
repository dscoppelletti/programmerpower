/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui;

import javax.servlet.*;
import org.apache.commons.lang3.*;
import org.json.*;
import org.restlet.data.Status;
import org.restlet.ext.json.*;
import org.restlet.representation.*;
import org.restlet.resource.*;
import org.springframework.web.context.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.resources.*;
import it.scoppelletti.programmerpower.web.resources.*;
import it.scoppelletti.programmerpower.web.rest.*;

/**
 * Server della risorsa {@code ApplicationInfoResource}.
 * 
 * @since 1.0.0
 */
@Final
@Reserved
public class ApplicationInfoServerResource extends AbstractServerResource
        implements ApplicationInfoResource, ServletContextAware {
    private ServletContext myServletCtx;
    
    /**
     * Costruttore.
     */
    public ApplicationInfoServerResource() {        
    }
    
    /**
     * Imposta il contesto dell&rsquo;applicazione Web.
     * 
     * @param obj Oggetto.
     */
    @Reserved
    public void setServletContext(ServletContext obj) {
        myServletCtx = obj;
    }
    
    public Representation getApplicationInfo() throws ResourceException {
        String value;
        JSONObject obj;
        ApplicationInfo info;
        
        try {
            info = new WebApplicationInfo(myServletCtx);
            
            obj = new JSONObject();
            value = info.getApplicationName();
            if (!StringUtils.isBlank(value)) {
                obj.put(ApplicationInfoResource.PROP_APPLNAME, value);
            }
            
            value = info.getVersion();
            if (!StringUtils.isBlank(value)) {
                obj.put(ApplicationInfoResource.PROP_VERSION, value);
            }
            
            value = info.getCopyright();
            if (!StringUtils.isBlank(value)) {
                obj.put(ApplicationInfoResource.PROP_COPYRIGHT, value);
            }
            
            value = info.getLicense();
            if (!StringUtils.isBlank(value)) {
                obj.put(ApplicationInfoResource.PROP_LICENSE, value);
            }
            
            value = info.getPageUrl();
            if (!StringUtils.isBlank(value)) {
                obj.put(ApplicationInfoResource.PROP_PAGEURL, value);
            }
        } catch (RuntimeException ex) {
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ApplicationException.toString(ex), ex);            
        }
        
        return new JsonRepresentation(obj);
    }
}

/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui;

import org.restlet.representation.*;
import org.restlet.resource.*;

/**
 * Collezione delle attivit&agrave; implementate da un&rsquo;applicazione.
 * 
 * @see   it.scoppelletti.programmerpower.wui.Activity
 * @since 1.0.0
 */
public interface ActivityListResource {

    /**
     * Percorso della risorsa. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>. 
     */
    public static final String PATH = "/rest/activities";
    
    /**
     * Propriet&agrave; {@code items}: Vettore delle attivit&agrave;.
     * 
     * @since 2.0.0
     */
    public static final String PROP_ITEMS = "items";
    
    /**
     * Propriet&agrave; {@code title}: Titolo.
     * 
     * @since 2.0.0
     */
    public static final String PROP_TITLE = "title";
    
    /**
     * Propriet&agrave; {@code activityUrl}: URL dell&rsquo;attivit&agrave;.
     * 
     * @since 2.0.0
     */
    public static final String PROP_ACTIVITYURL = "activityUrl";
    
    /**
     * Propriet&agrave; {@code icon URL}: URL dell&rsquo;icona.
     * 
     * @since 2.0.0
     */
    public static final String PROP_ICONURL = "iconUrl";
    
    /**
     * Restituisce le attivit&agrave;.
     * 
     * @return Rappresentazione.
     * @since  2.0.0
     */
    @Get("json")
    Representation listActivities() throws ResourceException;
}

/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui.control;

import java.util.*;
import com.opensymphony.xwork2.*;
import org.apache.commons.lang3.*;
import org.apache.struts2.interceptor.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;
import it.scoppelletti.programmerpower.web.control.*;

/**
 * Azione per eccezione.
 * 
 * @since 2.0.0
 */
@Final
public class ExceptionAction extends ActionBase implements
    SessionAware {
    private static final long serialVersionUID = 1L;
    private static final Logger myLogger = LoggerFactory.getLogger(
            ExceptionAction.class);
    
    /**
     * @serial Nome dell&rsquo;attributo di sessione nel quale &egrave;
     *         impostata l&rsquo;eccezione.
     */
    private String myExceptionKey;
    
    private transient Map<String, Object> mySessionMap;
    
    /**
     * Costruttore.
     */
    public ExceptionAction() {
    }    
    
    /**
     * Imposta il nome dell&rsquo;attributo di sessione nel quale &egrave;
     * impostata l&rsquo;eccezione.
     * 
     * @param value Valore.
     */
    public void setExceptionKey(String value) {
        myExceptionKey = value;
    }
        
    /**
     * Imposta la sessione.
     * 
     * @param map Collezione.
     */
    @Reserved
    public void setSession(Map<String, Object> map) {
        mySessionMap = map;
    }
    
    /**
     * Verifica se un intercettore deve essere escluso per l&rsquo;esecuzione di
     * un metodo.
     * 
     * <P>Questa implementazione del metodo
     * {@code isInterceptorExcludedForMethod} esclude, oltre agli intercettori
     * gi&agrave; esclusi dalla classe di base, l&rsquo;intercettore
     * {@code it-scoppelletti-interceptor}.</P>
     * 
     * @param  interceptor Nome dell&rsquo;intercettore.
     * @param  method      Nome del metodo.
     * @return             Esito della verifica.
     */    
    @Override
    public boolean isInterceptorExcludedForMethod(String interceptor,
            String method) {
        if (super.isInterceptorExcludedForMethod(interceptor, method)) {
            return true;
        }
        
        if (ExceptionInterceptor.NAME.equals(interceptor)) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Esegue l&rsquo;azione.
     */
    @Override
    public String execute() throws Exception {
        Exception ex;
                
        ex = getException();
        if (ex == null) {
           ex = new RuntimeException(getText("message.undefined"));
        }
        
        getUserInterface().display(MessageType.ERROR, ex);
        
        return Action.SUCCESS;
    }    
    
    /**
     * Restituisce l&rsquo;eccezione.
     * 
     * @return Oggetto.
     */
    private Exception getException() {
        Exception ex;
        
        if (StringUtils.isBlank(myExceptionKey)) {
            myLogger.warn("Property exceptionKey not set.");                       
            return null;
        }
        
        if (mySessionMap == null) {
            myLogger.warn("Property session not set.");            
            return null;
        }            

        ex = (Exception) mySessionMap.get(myExceptionKey);
        if (ex != null) {
            mySessionMap.remove(myExceptionKey);
        }
                                  
        return ex;
    }
}

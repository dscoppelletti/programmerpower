/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package it.scoppelletti.programmerpower.wui.view.freemarker;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.struts2.components.*;
import it.scoppelletti.programmerpower.web.view.freemarker.*;
import it.scoppelletti.programmerpower.wui.view.*;

/**
 * Versione FreeMarker del componente {@code OptionSelectComponent}.
 * 
 * @see   it.scoppelletti.programmerpower.wui.view.OptionSelectComponent 
 * @since 2.0.0
 */
public final class OptionSelectModel extends AbstractTagModel {
    
    /**
     * Costruttore.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta. 
     */
    public OptionSelectModel(ValueStack valueStack, HttpServletRequest req,
            HttpServletResponse resp) {
        super(valueStack, req, resp);
    }
    
    @Override
    protected Component newBean(ValueStack valueStack, HttpServletRequest req,
            HttpServletResponse resp) {
        return new OptionSelectComponent(valueStack, req, resp);
    }
}

/*
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui.security;

import javax.servlet.http.*;
import com.opensymphony.xwork2.*;
import org.apache.struts2.interceptor.*;
import org.springframework.security.web.savedrequest.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;
import it.scoppelletti.programmerpower.ui.*;
import it.scoppelletti.programmerpower.web.control.*;
import it.scoppelletti.programmerpower.web.security.*;

/**
 * Azione di login.
 * 
 * @since 2.0.0
 */
@Final
public class LoginAction extends ActionBase implements
        ServletRequestAware, ServletResponseAware {
    private static final long serialVersionUID = 1L;
    
    /**
     * @serial Codice dell&rsquo;utente.
     */
    private String myUserCode;
    
    /**
     * @serial Indica se l&rsquo;autenticazione deve essere persistente.
     */
    private boolean myRememberMe;
    
    /**
     * @serial Indica se l&rsquo;autenticazione &egrave; stata richiesta
     *         esplicitamente dall&rsquo;utente.
     */
    private boolean myUserRequest;
    
    private transient SecureString myPwd;            
    private transient String myAuthTicket;
    private transient HttpServletRequest myReq;
    private transient HttpServletResponse myResp;
    private transient RequestCache myRequestCache;
    
    @javax.annotation.Resource(name = AuthenticationService.BEAN_NAME)
    private transient AuthenticationService myAuthService;
        
    /**
     * Costruttore.
     */
    public LoginAction() {
    }    

    @Override
    public void dispose() {        
        if (myPwd != null) {
            myPwd.clear();
            myPwd = null;
        }        

        super.dispose();
    }    
        
    /**
     * Restituisce il codice dell&rsquo;utente.
     * 
     * @return Valore.
     * @see    #setUserCode
     */
    public String getUserCode() {
        return myUserCode;
    }
    
    /**
     * Imposta il codice dell&rsquo;utente.
     * 
     * @param value Valore.
     * @see         #getUserCode
     */
    public void setUserCode(String value) {
        myUserCode = value;
    }
    
    /**
     * Restituisce la password.
     * 
     * @return Valore.
     * @see    #setPassword
     */
    public SecureString getPassword() {
        return myPwd;
    }
    
    /**
     * Imposta la password.
     * 
     * @param value Valore.
     * @see         #getPassword
     */
    public void setPassword(SecureString value) {
        myPwd = value;
    }        
    
    /**
     * Restituisce l&rsquo;indicatore di autenticazione persistente.
     * 
     * @return Valore.
     * @see    #setRememberMe
     */
    public boolean isRememberMe() {
        return myRememberMe;
    }
    
    /**
     * Imposta l&rsquo;indicatore di autenticazione persistente.
     * 
     * @param value Valore.
     * @see         #isRememberMe
     */
    public void setRememberMe(boolean value) {
        myRememberMe = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di richiesta di autenticazione esplicita
     * dall&rsquo;utente.
     * 
     * @param value Valore.
     */
    public void setUserRequest(boolean value) {
        myUserRequest = value;
    }
    
    /**
     * Restituisce il ticket di autenticazione.
     * 
     * @return Valore.
     */
    public String getAuthenticationTicket() {
        return myAuthTicket;
    }

    /**
     * Imposta la richiesta.
     * 
     * @param obj Oggetto.
     */
    @Reserved
    public void setServletRequest(HttpServletRequest obj) {
        myReq = obj;
    }
    
    /**
     * Imposta la risposta.
     * 
     * @param obj Oggetto.
     */
    @Reserved
    public void setServletResponse(HttpServletResponse obj) {
        myResp = obj;
    }

    /**
     * Imposta il cache della richiesta originale che ha inoltrato la richiesta
     * di autenticazione.
     * 
     * @param obj Oggetto.
     */
    public void setRequestCache(RequestCache obj) {
        myRequestCache = obj;
    }
    
    /**
     * Validazione.
     */
    @Override
    public void validate() {
        if (ValueTools.isNullOrEmpty(myPwd)) {
            addFieldError("password", getText("password.required"));
        }
    }
    
    /**
     * Comando {@code input}.
     * 
     * @return Risultato.
     */
    @Override
    public String input() throws Exception {    
        if (myUserRequest && myRequestCache != null) {
            myRequestCache.removeRequest(myReq, myResp);
        }
        
        return super.input();
    }
    
    /**
     * Comando {@code login}.
     * 
     * @return Risultato.
     */
    @Override
    public String execute() throws Exception {
        if (myReq == null) {
            throw new PropertyNotSetException(toString(), "servletRequest");            
        }
        if (myResp == null) {
            throw new PropertyNotSetException(toString(), "servletResponse");            
        }
        
        try {
            myAuthTicket = myAuthService.newAuthenticationTicket(myReq, myResp,
                myUserCode, myPwd);
        } catch (Exception ex) {
            getUserInterface().display(MessageType.ERROR, ex);
            return Action.INPUT;
        }
        
        return Action.SUCCESS;
    }
}

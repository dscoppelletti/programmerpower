/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.   
 */

package it.scoppelletti.programmerpower.wui.view.jsp;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.struts2.components.*;
import it.scoppelletti.programmerpower.web.view.jsp.*;
import it.scoppelletti.programmerpower.wui.view.*;

/**
 * Versione <ACRONYM TITLE="Java Server Pages">JSP</ACRONYM> del componente
 * {@code OptionValueSelectComponent}.
 * 
 * @see   it.scoppelletti.programmerpower.wui.view.OptionValueSelectComponent 
 * @since 2.0.0
 */
public final class OptionValueSelectTag extends AbstractUIComponentTag {
    private static final long serialVersionUID = 1L;
        
    /**
     * @serial Nome della collezione delle opzioni supportate.
     */    
    private String myOptionName;
    
    /**
     * @serial Indicatore di controllo a sola lettura.
     */    
    private String myReadOnly;
    
    /**
     * @serial Indicatore valore obbligatorio per le opzioni selezionate.
     */    
    private String myValueRequired;
    
    /**
     * @serial Lunghezza massima del valore delle opzioni.
     */    
    private int myValueMaxLen;
    
    /**
     * @serial Numero di opzioni visibili nella lista di selezione.
     */    
    private int myOptionSize;  
    
    /**
     * @serial Etichetta delle opzioni selezionate.
     */
    private String mySelectedLabel;
    
    /**
     * @serial Etichetta delle opzioni disponibili.
     */
    private String myAvailLabel;
    
    /**
     * Costruttore.
     */
    public OptionValueSelectTag() {        
    }

    /**
     * Imposta il nome della collezione delle opzioni supportate.
     * 
     * @param value Valore.
     */
    public void setOptionName(String value) {
        myOptionName = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di controllo a sola lettura.
     * 
     * @param value Valore.
     */
    public void setReadOnly(String value) {
        myReadOnly = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore valore obbligatorio per le opzioni
     * selezionate.
     * 
     * @param value Valore.
     */
    public void setValueRequired(String value) {
        myValueRequired = value;
    }
    
    /**
     * Imposta la lunghezza massima del valore delle opzioni.
     * 
     * @param value Valore.
     */
    public void setValueMaxLen(int value) {
        myValueMaxLen = value;
    }
   
    /**
     * Imposta il numero di opzioni visibili nella lista di selezione.
     * 
     * @param value Valore.
     */
    public void setOptionSize(int value) {
        myOptionSize = value;
    }
      
    /**
     * Imposta l&rsquo;etichetta delle opzioni selezionate.
     * 
     * @param value Valore.
     */
    public void setSelectedLabel(String value) {
        mySelectedLabel = value;
    }
    
    /**
     * Imposta l&rsquo;etichetta delle opzioni disponibili.
     * 
     * @param value Valore.
     */    
    public void setAvailableLabel(String value) {
        myAvailLabel = value;
    }
    
    protected Component newBean(ValueStack valueStack, HttpServletRequest req,
            HttpServletResponse resp) {
        return new OptionValueSelectComponent(valueStack, req, resp);
    }
    
    /**
     * Imposta i parametri sul componente.
     */
    @Override
    protected void populateParams() {
        OptionValueSelectComponent c;
        
        super.populateParams();
        
        c = (OptionValueSelectComponent) getComponent();
        c.setOptionName(myOptionName);
        c.setReadOnly(myReadOnly);
        c.setValueRequired(myValueRequired);
        c.setValueMaxLen(myValueMaxLen);
        c.setOptionSize(myOptionSize);
        c.setSelectedLabel(mySelectedLabel);
        c.setAvailableLabel(myAvailLabel);
    }        
}

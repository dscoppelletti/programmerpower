/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.   
 */

package it.scoppelletti.programmerpower.wui.view.jsp;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;
import org.apache.struts2.components.*;
import it.scoppelletti.programmerpower.web.view.jsp.*;
import it.scoppelletti.programmerpower.wui.view.*;

/**
 * Versione <ACRONYM TITLE="Java Server Pages">JSP</ACRONYM> del componente
 * {@code OptionSelectComponent}.
 * 
 * @see   it.scoppelletti.programmerpower.wui.view.OptionSelectComponent 
 * @since 2.0.0
 */
public final class OptionSelectTag extends AbstractUIComponentTag {
    private static final long serialVersionUID = 1L;
        
    /**
     * @serial Nome della collezione delle opzioni supportate.
     */    
    private String myOptionName;
    
    /**
     * @serial Indicatore di controllo a sola lettura.
     */    
    private String myReadOnly;

    /**
     * @serial Numero di opzioni visibili nella lista delle opzioni selezionate.
     */    
    private int mySelectedSize;
    
    /**
     * @serial Numero di opzioni visibili nella lista delle opzioni disponibili.
     */    
    private int myAvailSize;  
    
    /**
     * @serial Etichetta delle opzioni selezionate.
     */
    private String mySelectedLabel;
    
    /**
     * @serial Etichetta delle opzioni disponibili.
     */
    private String myAvailLabel;
    
    /**
     * Costruttore.
     */
    public OptionSelectTag() {        
    }

    /**
     * Imposta il nome della collezione delle opzioni supportate.
     * 
     * @param value Valore.
     */
    public void setOptionName(String value) {
        myOptionName = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di controllo a sola lettura.
     * 
     * @param value Valore.
     */
    public void setReadOnly(String value) {
        myReadOnly = value;
    }
        
    /**
     * Imposta il numero di opzioni visibili nella lista delle opzioni
     * selezionate.
     * 
     * @param value Valore.
     */
    public void setSelectedSize(int value) {
        mySelectedSize = value;
    }
    
    /**
     * Imposta il numero di opzioni visibili nella lista delle opzioni
     * disponibili.
     * 
     * @param value Valore.
     */
    public void setAvailableSize(int value) {
        myAvailSize = value;
    }
      
    /**
     * Imposta l&rsquo;etichetta delle opzioni selezionate.
     * 
     * @param value Valore.
     */
    public void setSelectedLabel(String value) {
        mySelectedLabel = value;
    }
    
    /**
     * Imposta l&rsquo;etichetta delle opzioni disponibili.
     * 
     * @param value Valore.
     */    
    public void setAvailableLabel(String value) {
        myAvailLabel = value;
    }
    
    protected Component newBean(ValueStack valueStack, HttpServletRequest req,
            HttpServletResponse resp) {
        return new OptionSelectComponent(valueStack, req, resp);
    }
    
    /**
     * Imposta i parametri sul componente.
     */
    @Override
    protected void populateParams() {
        OptionSelectComponent c;
        
        super.populateParams();
        
        c = (OptionSelectComponent) getComponent();
        c.setOptionName(myOptionName);
        c.setReadOnly(myReadOnly);
        c.setSelectedSize(mySelectedSize);
        c.setAvailableSize(myAvailSize);
        c.setSelectedLabel(mySelectedLabel);
        c.setAvailableLabel(myAvailLabel);
    }        
}

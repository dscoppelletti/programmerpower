/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.Protocol;
import org.restlet.data.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.web.context.ServletContextAware;
import it.scoppelletti.programmerpower.net.NetTools;
import it.scoppelletti.programmerpower.net.RestTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.reflect.Reserved;
import it.scoppelletti.programmerpower.resources.ResourceTools;
import it.scoppelletti.programmerpower.web.WebClient;

/**
 * Implementazione di default dell&rsquo;interfaccia {@code ActivityManager}.
 * 
 * @since 1.0.0
 */
@Final
public class DefaultActivityManager implements ActivityManager,
        ServletContextAware {
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultActivityManager.class);     
    private List<Activity> myActivities;
    private MessageSource myStringSource;
    private ServletContext myServletCtx;
    
    @javax.annotation.Resource(name = WebClient.BEAN_NAME)
    private WebClient myWebClient;
    
    /**
     * Costruttore.
     */
    public DefaultActivityManager() {        
    }
            
    /**
     * Imposta le attivit&agrave;.
     * 
     * @param obj Collezione.
     */
    public void setActivities(List<Activity> obj) {
        myActivities = obj;
    }
        
    /**
     * Imposta la sorgente delle stringhe localizzate.
     * 
     * @param obj Oggetto.
     */
    public void setStringSource(MessageSource obj) {
        myStringSource = obj;
    }
     
    /**
     * Imposta il contesto dell&rsquo;applicazione Web.
     * 
     * @param obj Oggetto.
     */   
    @Reserved
    public void setServletContext(ServletContext obj) {
        myServletCtx = obj;
    }
    
    public List<Activity> listActivities() {
        DefaultActivity obj;
        ArrayList<Activity> activityList;
        Locale locale = ResourceTools.getLocale();       
        
        activityList = new ArrayList<Activity>();
        if (CollectionUtils.isEmpty(myActivities)) {
            myLogger.warn("Activity list is empty.");
            return activityList;
        }
           
        if (myStringSource == null) {
            myLogger.warn("Property stringSource not set.");        
        }
                   
        for (Activity activity : myActivities) {
            obj = new DefaultActivity();
            obj.setTitle(getLocalizedString(activity.getTitle(), locale));           
            obj.setActivityUrl(getAbsoluteUrl(activity.getActivityUrl(),
                    false));
            obj.setIconUrl(getAbsoluteUrl(activity.getIconUrl(), true));        
                                    
            activityList.add(obj);
        }
        
        return activityList;
    }    
    
    /**
     * Restituisce una stringa localizzata.
     * 
     * @param value  Stringa da localizzare.
     * @param locale Localizzazione.
     * @return       Stringa localizzata.
     */
    private String getLocalizedString(String value, Locale locale) {
        if (!StringUtils.isBlank(value) && myStringSource != null) {
            value = myStringSource.getMessage(value, null, value, locale);
        }                       
                
        return value;
    }   
    
    /**
     * Restituisce l&rsquo;URL assoluto corrispondente ad un URL che potrebbe
     * essere gi&agrave; assoluto, relativo all&rsquo;AS oppure relativo 
     * al contesto dell&rsquo;applicazione Web.
     * 
     * @param  url    URL originale.
     * @param  secure Indica se l&rsquo;URL restituito deve essere sicuro.
     * @return        URL assoluto.
     */
    private String getAbsoluteUrl(String url, boolean secure) {
        String ctxPath;
        URI serverUri, uri;
        Reference ref;
        
        serverUri = (secure) ? myWebClient.getServerSecureUri() :
            myWebClient.getServerUri();
        uri = URI.create(url);
        if (uri.isAbsolute()) {
            ref = new Reference(uri);
            ref.setProtocol((secure) ? Protocol.HTTPS : Protocol.HTTP);
            ref.setHostPort(uri.getPort());
            return ref.toString();
        }
        
        ref = new Reference(serverUri);
        
        if (!url.startsWith(NetTools.PATH_SEP)) {
            ctxPath = myServletCtx.getContextPath();
            if (!StringUtils.isEmpty(ctxPath)) {
                RestTools.setPath(ref, ctxPath);
            }
        }
        
        return RestTools.addPath(ref, url).toString();
    }
}


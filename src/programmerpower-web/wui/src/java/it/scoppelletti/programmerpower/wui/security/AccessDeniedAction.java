/*
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui.security;

import com.opensymphony.xwork2.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;
import it.scoppelletti.programmerpower.web.control.*;

/**
 * Azione per accesso negato.
 * 
 * @since 2.0.0
 */
@Final
public class AccessDeniedAction extends ActionBase {
    private static final long serialVersionUID = 1L;
    
    /**
     * Costruttore.
     */
    public AccessDeniedAction() {
    }    
    
    /**
     * Esegue l&rsquo;azione.
     */
    @Override
    public String execute() throws Exception {
        getUserInterface().display(MessageType.ERROR,
                getText("error.accessDenied"));
        
        return Action.SUCCESS;
    }
}

/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui.control;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import org.restlet.data.Reference;
import it.scoppelletti.programmerpower.net.RestTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.web.WebClient;

/**
 * Azione di redirezione ad un
 * <ACRONYM TITLE="Uniform Resource Locator">URL</ACRONYM> assoluto.
 * 
 * @since 2.0.0
 */
@Final
public class RedirectAction extends ActionSupport {
    private static final long serialVersionUID = 1L;
    
    /**
     * @serial Percorso dell&rsquo;URL di destinazione.
     */
    private String myTargetPath;
    
    /**
     * @serial URL di destinazione.
     */
    private String myTargetUrl;
    
    @javax.annotation.Resource(name = WebClient.BEAN_NAME)
    private WebClient myWebClient;
    
    /**
     * Costruttore.
     */
    public RedirectAction() {
    }
    
    /**
     * Imposta il percorso dell&rsquo;URL di destinazione.
     * 
     * @param value Valore.
     */
    public void setTargetPath(String value) {
        myTargetPath = value;
    }
    
    /**
     * Restituisce l&rsquo;URL di destinazione.
     * 
     * @return Valore.
     */
    public String getTargetUrl() {
        return myTargetUrl;
    }
    
    /**
     * Esegue l&rsquo;azione.
     */
    @Override
    public String execute() throws Exception { 
        Reference ref;
        
        ref = new Reference(myWebClient.getServerUri());
        myTargetUrl = RestTools.addPath(ref, myTargetPath).toString();
        return Action.SUCCESS;
    }
}

/*
 * Copyright (C) 2012-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui;

import java.util.*;

/**
 * Gestore delle attivit&agrave; implementate da un&rsquo;applicazione.
 * 
 * @see   it.scoppelletti.programmerpower.wui.Activity
 * @since 1.0.0
 */
public interface ActivityManager {
    
    /**
     * Nome del bean. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String BEAN_NAME =
            "it-scoppelletti-programmerpower-wui-activityManager";
    
    /**
     * Restituisce le attivit&agrave;.
     * 
     * @return Collezione.
     */
    List<Activity> listActivities(); 
}

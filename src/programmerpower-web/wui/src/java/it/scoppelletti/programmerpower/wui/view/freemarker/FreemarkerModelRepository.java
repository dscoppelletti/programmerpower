/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.programmerpower.wui.view.freemarker;

import javax.servlet.http.*;
import com.opensymphony.xwork2.util.*;

/**
 * Modello delle direttive FreeMarker che implementano i tag Programmer Power
 * <ACRONYM TITLE="Web User Interface">WUI</ACRONYM>.
 *    
 * @since 1.0.0
 */
public final class FreemarkerModelRepository {
    private final ValueStack myValueStack;
    private final HttpServletRequest myReq;
    private final HttpServletResponse myResp;    
    private OptionSelectModel myOptionSelect;
    private OptionValueSelectModel myOptionValueSelect;
    
    /**
     * Costruttore.
     * 
     * @param  valueStack Stack dei valori.
     * @param  req        Richiesta.
     * @param  resp       Risposta.  
     */
    public FreemarkerModelRepository(ValueStack valueStack,
            HttpServletRequest req, HttpServletResponse resp) {
        myValueStack = valueStack;
        myReq = req;
        myResp = resp;
    }
        
    /**
     * Restituisce la direttiva <CODE>&lt;&#64;optionSelect&gt;</CODE>.
     * 
     * @return Oggetto.
     * @since  2.0.0
     */
    public OptionSelectModel getOptionSelect() {
        if (myOptionSelect == null) {
            myOptionSelect = new OptionSelectModel(myValueStack, myReq, myResp);
        }
        
        return myOptionSelect;
    }  
    
    /**
     * Restituisce la direttiva <CODE>&lt;&#64;optionValueSelect&gt;</CODE>.
     * 
     * @return Oggetto.
     * @since  2.0.0
     */
    public OptionValueSelectModel getOptionValueSelect() {
        if (myOptionValueSelect == null) {
            myOptionValueSelect = new OptionValueSelectModel(
                    myValueStack, myReq, myResp);
        }
        
        return myOptionValueSelect;
    }                  
}

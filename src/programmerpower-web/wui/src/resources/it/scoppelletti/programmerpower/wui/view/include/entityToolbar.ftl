<#--
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--> 
<#if viewState.formMode == "new">
    <@s.a cssClass="btn btn-success"
        dynamicAttributes={ "role" : "button",
            "data-submit" : "#${formId}",
            "data-action" : "${actionNameBase}Xsave" }>
        <span class="glyphicon glyphicon-save"></span>
        <@s.text name="label.save" />
    </@s.a>
    <@tiles.insertAttribute name="toolbarXnew" />
<#elseif viewState.formMode == "view">
    <@web.authorize access="${authorizeAccessMap.update}">
        <@s.a action="${actionNameBase}Xedit" cssClass="btn btn-default"
            dynamicAttributes={ "role" : "button" }>
            <@s.param name="viewState.entityId">${viewState.entityId}</@s.param>
            <span class="glyphicon glyphicon-edit"></span>  
            <@s.text name="label.edit" />
        </@s.a>    
    </@web.authorize>
    <@tiles.insertAttribute name="toolbarXview" />
    <@web.authorize access="${authorizeAccessMap.delete}">
        <@s.text name="prompt.delete" var="promptDelete" />
        <@s.a cssClass="btn btn-danger"
            dynamicAttributes={ "role" : "button",
                "data-submit" : "#${formId}",
                "data-action" : "${actionNameBase}Xdelete",
                "data-prompt" : "${promptDelete}" }>
            <span class="glyphicon glyphicon-trash"></span>                   
            <@s.text name="label.delete" />
        </@s.a>           
    </@web.authorize>
<#elseif viewState.formMode == "edit">
    <@s.a cssClass="btn btn-success"
        dynamicAttributes={ "role" : "button",
            "data-submit" : "#${formId}",
            "data-action" : "${actionNameBase}Xupdate" }>
        <span class="glyphicon glyphicon-save"></span>
        <@s.text name="label.update" />
    </@s.a>
    <@s.a action="${actionNameBase}Xview" cssClass="btn btn-default"
        dynamicAttributes={ "role" : "button" }>
        <@s.param name="viewState.entityId">${viewState.entityId}</@s.param>
        <span class="glyphicon glyphicon-remove"></span>
        <@s.text name="label.cancel" />
    </@s.a>   
</#if>  
               
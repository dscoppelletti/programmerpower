<#--
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<#assign msgSeverityMax=0>
<@s.iterator value="messages" var="msg">
	<#if "${msg.event.messageType}" == "INFORMATION">
		<#assign msgSeverity=0>
	<#elseif "${msg.event.messageType}" == "WARNING">
		<#assign msgSeverity=1>
	<#else> <#-- if "${msg.event.messageType}" == "ERROR" -->
		<#assign msgSeverity=2>
	</#if>						
	<#if msgSeverity &gt; msgSeverityMax>
		<#assign msgSeverityMax=msgSeverity>
	</#if>
</@s.iterator>
<#if msgSeverityMax == 0>
	<#assign ctxClass="panel-info">
	<#assign iconClass="glyphicon-info-sign">
<#elseif msgSeverityMax == 1>
	<#assign ctxClass="panel-warning">
	<#assign iconClass="glyphicon-warning-sign">
<#else> <#-- if msgSeverityMax == 2 -->
	<#assign ctxClass="panel-danger">
	<#assign iconClass="glyphicon-remove-circle">
</#if>
<#assign msgBoxDismissible>
    <@tiles.insertAttribute name="isMsgBoxDismissible" />
</#assign>
<div class="panel ${ctxClass}" role="alert">
	<div class="panel-heading">
<#if msgBoxDismissible?boolean>
        <@s.text name="label.close" var="labelClose" />
        <button type="button" class="close" aria-label="${labelClose}">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">${labelClose}</span>
        </button>	
</#if>				
		<span class="glyphicon ${iconClass}"></span>
		<@s.text name="label.messages" />							
	</div>
	<@s.text name="label.expand" var="labelExpand" />
	<@s.text name="label.collapse" var="labelCollapse" />
	<ul class="list-group">
<@s.iterator value="messages" var="msg" status="msgStatus">
		<li class="list-group-item">
			<#if "${msg.event.messageType}" == "INFORMATION">
				<#assign iconClass="glyphicon-info-sign text-info">
			<#elseif "${msg.event.messageType}" == "WARNING">
				<#assign iconClass="glyphicon-warning-sign text-warning">
			<#else> <#-- if "${msg.event.messageType}" == "ERROR" -->
				<#assign iconClass="glyphicon-remove-circle text-danger">
			</#if>			
			<span class="glyphicon ${iconClass}"></span>
			<#if msg.lines?size &gt; 1>					
<span class="it-scoppelletti-wui-collapseExpand glyphicon<#rt/>
 glyphicon-expand" title="${labelExpand}"></span>
<span class="sr-only">${labelExpand}</span>
<span class="hidden">${labelCollapse}</span>
			<#else>
<span class="glyphicon glyphicon-unchecked"></span>					
			</#if>
			<#if msg.lines?size &gt; 0>		
				${msg.lines[0]!?html}					
			</#if>										
			<#if msg.lines?size &gt; 1>
				<div class="collapse">
                	<#list msg.lines as msgLine>
                    	<#if msgLine_index &gt; 0>
<#if msgLine_index &gt; 1><br/></#if>${msgLine!?html}
                    	</#if>                        
                	</#list>							
				</div>
			</#if>					
		</li>				
</@s.iterator>
	</ul>
</div>

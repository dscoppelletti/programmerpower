<#--
 * Copyright (C) 2011-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<@s.form action="${actionNameBase}Xexecute">
    <@s.hidden name="viewState" />
    <@s.textfield key="userCode" maxlength="16" requiredLabel="true"
        cssClass="it-scoppelletti-lowercase" />
    <@s.password key="password" showPassword="false" requiredLabel="true" />
    <@s.checkbox key="rememberMe" dynamicAttributes={ "aria-describedby" :
        "it-scoppelletti-rememberMe-help" } />
    <@s.label id="it-scoppelletti-rememberMe-help" key="rememberMe.warning"
        label="" labelSeparator="" cssClass="text-danger" />
    <@s.submit cssClass="btn-success">
        <span class="glyphicon glyphicon-log-in"></span>  
        <@s.text name="label.ok" />
    </@s.submit>
</@s.form>

    


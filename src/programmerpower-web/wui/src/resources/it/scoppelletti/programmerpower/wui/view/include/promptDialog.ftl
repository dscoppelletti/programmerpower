<#--
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<@s.text name="label.cancel" var="labelCancel" />
<div id="it-scoppelletti-wui-promptDialog" class="modal fade"<#rt/>
 tabindex="-1" role="dialog" aria-hidden="true"<#rt/>
 aria-labelledby="it-scoppelletti-wui-promptTitle">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"<#rt/>
 aria-label="${labelCancel}">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="it-scoppelletti-wui-promptTitle"<#rt/>
 class="modal-title text-danger">
                </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">
    <span class="glyphicon glyphicon-remove"></span>
    ${labelCancel}
</button>
<button type="button" class="btn btn-danger" data-dismiss="modal">
</button>
            </div>
        </div>
    </div>
</div>    

<#--
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<@web.applServerUrl secure="true" var="baseUrl" />
<title><@s.text name="project.name" /></title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />            
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
    href="${baseUrl}/it-scoppelletti-wui/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
    href="${baseUrl}/it-scoppelletti-wui/css/bootstrap-theme.min.css" />    
<link rel="stylesheet" type="text/css"
    href="${baseUrl}/it-scoppelletti-wui/it/scoppelletti/wui/css/wui.css" />
<@s.text name="project.icon" var="projectIcon">
    <@s.param>${baseUrl}</@s.param>
</@s.text>
<link rel="shortcut icon" href="${projectIcon}" type="image/png" />
<#-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media
queries
WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script type="text/javascript"
        src="${baseUrl}/it-scoppelletti-wui/js/html5shiv.min.js"></script>          
    <script type="text/javascript"
        src="${baseUrl}/it-scoppelletti-wui/js/respond.min.js"></script>
<![endif]--> 
<@s.head />
<@tiles.insertAttribute name="head" />

<#--
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->        
<#assign loginEnabled>
    <@tiles.insertAttribute name="isLoginEnabled" />
</#assign>
<#if loginEnabled?default("true")?boolean>
    <@web.authorize access="isAnonymous()">
        <li><button type="button" class="btn btn-success navbar-btn"<#rt/>
 data-submit="#it-scoppelletti-loginForm">
            <span class="glyphicon glyphicon-log-in"></span>
            <@s.text name="label.login" />   
        </button></li>
    </@web.authorize>  
</#if> 

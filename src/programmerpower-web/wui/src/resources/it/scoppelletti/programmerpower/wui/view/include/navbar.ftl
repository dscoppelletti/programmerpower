<#--
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--> 
<#macro actionActivityPanel>
    <#assign activityPanelActive>
        <@tiles.insertAttribute name="isActivityPanelActive" />
    </#assign>
    <#if !activityPanelActive?boolean>      
        <@web.applServerUrl var="baseUrl" />
        <#assign actionActivityPanel = "${baseUrl}/it-scoppelletti-wui/activityPanel.action">
        <li><@s.a value="${actionActivityPanel}">
            <@s.text name="label.activityPanel" />
        </@s.a></li>
    </#if>
</#macro>
<#macro actionCurrent>
    <#assign actionTab>
        <@tiles.insertAttribute name="isActionTab" />
    </#assign>
    <#if actionTab?boolean>
        <li class="active"><a href="#">${viewTitle}
            <span class="sr-only"><@s.text name="label.current" /></span>
        </a></li>
    </#if>
</#macro>
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"<#rt/>
 data-toggle="collapse" data-target="#it-scoppelletti-wui-navbar">
                <span class="sr-only"><@s.text name="label.nav.toggle" /></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <@s.text name="project.url" var="projectUrl" />
            <a class="navbar-brand" href="${projectUrl}" target="_blank">
                <@s.text name="project.name" />
            </a>          
        </div>
        <div id="it-scoppelletti-wui-navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <@actionActivityPanel />    
                <@tiles.insertAttribute name="navbar" />
                <@actionCurrent /> 
<#include "/it/scoppelletti/programmerpower/wui/security/actionUser.ftl">
            </ul>
            <ul class="nav navbar-nav navbar-right">
<#include "/it/scoppelletti/programmerpower/wui/security/loginButton.ftl"> 
<#include "/it/scoppelletti/programmerpower/wui/security/logoutButton.ftl"> 
                <#if helpUrl??>
                    <li><@s.a value="${helpUrl}" cssClass="navbar-link"
                        dynamicAttributes={ "target" : "_blank" }>
                        <span class="glyphicon glyphicon-question-sign"></span>
                        <@s.text name="label.help" />
                    </@s.a></li>          
                </#if>
            </ul>
        </div>
    </div>
</div>
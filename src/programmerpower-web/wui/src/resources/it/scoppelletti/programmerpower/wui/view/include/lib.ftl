<#--
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.     
-->
<#macro defer>
    <div class="hidden">
<#include "/it/scoppelletti/programmerpower/wui/security/loginForm.ftl">
<#include "/it/scoppelletti/programmerpower/wui/security/logoutForm.ftl">
    </div>
    <#include "clientError.ftl">
    <#include "promptDialog.ftl">
	<#include "defer.ftl">
</#macro>
<#macro head>
    <#include "head.ftl">
</#macro>
<#macro messageBox>
	<#if messages?? && messages?size &gt; 0>
		<#include "messageBox.ftl">
	</#if>		
</#macro>
<#macro navbar>
	<#include "navbar.ftl">
</#macro>
<#macro title>
    <#assign actionTab>
        <@tiles.insertAttribute name="isActionTab" />
    </#assign>
    <#if !actionTab?boolean>
        <h1>${viewTitle}</h1>
    </#if>
</#macro>
<#macro toolbar>
    <#assign toolbarEnabled>
        <@tiles.insertAttribute name="isToolbarEnabled" />
    </#assign>
    <#if toolbarEnabled?boolean>
        <div class="btn-toolbar" role="toolbar">
            <@tiles.insertAttribute name="toolbar" />
        </div>
    </#if>
</#macro>
<#--
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<@s.text name="label.close" var="labelClose" />
<div id="it-scoppelletti-wui-clientError" class="modal fade" tabindex="-1"<#rt/>
 role="dialog" aria-hidden="true"<#rt/>
 aria-labelledby="it-scoppelletti-wui-clientErrorTitle">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"<#rt/>
 aria-label="${labelClose}">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-danger">
<span class="glyphicon glyphicon-remove-sign"></span>
                    <span id="it-scoppelletti-wui-clientErrorTitle"></span>
                </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">
    <span class="glyphicon glyphicon-remove"></span>
    <@s.text name="label.cancel" />
</button>
            </div>
        </div>
    </div>
</div>    

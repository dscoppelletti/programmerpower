<#--
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<#assign hasFieldErrors = parameters.name?? && fieldErrors?? && fieldErrors[parameters.name]?? />
<#include "/${parameters.templateDir}/${parameters.expandTheme}/fieldErrorList.ftl" />
<div class="form-group it-scoppelletti-optionSelect"<#rt/>
<#if parameters.id??>
 id="${parameters.id?html}"<#rt/>
</#if>
<#if !parameters.readOnly?default(false)>
    <@s.text name="label.error" var="labelError" />
    <@s.text name="error.noOptionSelected" var="errorNoOptionSelected" />
 data-name="${parameters.name?html}"<#rt/>
 data-labelerror="${labelError?html}"<#rt/>
 data-error1="${errorNoOptionSelected?html}"<#rt/>
</#if>
>
<#if parameters.label??>
	<label class="col-md-2 control-label">${parameters.label?html}<#rt/>
	   <#if parameters.required?default(false)>
 <span class="text-danger">*</span><#rt/>
	   </#if>
	   ${parameters.labelseparator?default(":")?html}<#t/>
    </label><#t/>	    	
</#if>
<#if parameters.readOnly?default(false)>
	<div class="col-md-10">
<#include "/${parameters.templateDir}/${parameters.expandTheme}/optionSelect-sel.ftl" />
	</div>
<#else>
	<div class="col-md-5">
<#include "/${parameters.templateDir}/${parameters.expandTheme}/optionSelect-sel.ftl" />
	</div>
	<div class="col-md-5">
<#include "/${parameters.templateDir}/${parameters.expandTheme}/optionSelect-avail.ftl" />
	</div>
</#if>
<#include "/${parameters.templateDir}/${parameters.expandTheme}/controlfooter.ftl" />

<#--
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
-->
<#if !parameters.readOnly?default(false)>
	<div class="form-group">
		<label class="col-md-12"><#rt/>
            <#if parameters.selectedLabel??>
                ${parameters.selectedLabel?html}<#t/>
            <#else>
                <@s.text name="label.options.selected" /><#t/>
            </#if>					
        </label><#t/>	
	</div>	
</#if>
<div<#rt/>
<#if parameters.nameValue.size() == 0>
 class="form-group it-scoppelletti-empty"<#rt/>
<#else>
 class="form-group it-scoppelletti-empty hidden"<#rt/>	 
</#if>
>
	<div class="col-md-12">
		<p class="form-control-static">
			<@s.text name="label.empty" />      	
		</p>
	</div>		
</div>		 	
<@s.text name="label.remove" var="labelRemove" />
<@s.iterator value="parameters.nameValue">
	<#assign optionKey = stack.findValue('key') />
	<#assign optionDesc =
		parameters.optionNameValue[optionKey]?default(optionKey) />
	<#assign optionValue = stack.findValue('value') />
	<#assign fieldName = "${parameters.name}.${optionKey}" />
	<#assign hasFieldErrors = fieldErrors?? && fieldErrors[fieldName]?? />
	<#if hasFieldErrors>
		<div class="form-group">
			<div class="col-md-12">
				<p class="form-control-static text-danger">
    				<#list fieldErrors[fieldName] as error>
						<#if error_index &gt; 0><br/></#if>${error?html}
					</#list>
				</p>			
			</div>
		</div>        
	</#if>
	<div data-key="${optionKey?html}" data-desc="${optionDesc?html}"<#rt/>
<#if hasFieldErrors>
 class="form-group has-error"<#rt/>
<#else>
 class="form-group"<#rt/>
</#if>
>
		<label for="${fieldName?html}"<#rt/>
 class="col-md-5 control-label">${optionDesc?html}<#rt/>
	<#if !parameters.readOnly?default(false) &&
		parameters.valueRequired?default(false)>
 <span class="text-danger">*</span><#rt/>
	</#if>
${parameters.labelseparator?default(":")?html}<#rt/>
        </label><#t/>	    	
		<div class="col-md-7">
<#if !parameters.readOnly?default(false)>		
			<div class="input-group">		
</#if>			
				<input type="text" name="${fieldName?html}"<#rt/> 
 class="form-control"<#rt/>               
        <#if parameters.readOnly?default(false)>
 readonly="readonly"<#rt/>
        </#if>    
        <#if optionValue??>                    
 value="${optionValue?html}"<#rt/>
        </#if>                     
        <#if parameters.valueMaxLen??>
 maxlength="${parameters.valueMaxLen}"<#rt/>
        </#if>
 />					
<#if !parameters.readOnly?default(false)>
				<span class="input-group-btn"><#rt/>
                    <button type="button" class="btn btn-danger"<#t/>
 title="${labelRemove}"><#rt/>
                        <span class="glyphicon glyphicon-remove"></span><#t/>
 <span class="sr-only">${labelRemove}</span><#rt/>
                    </button><#t/>
                </span><#t/>		
			</div>
</#if>
		</div>
	</div>
</@s.iterator>

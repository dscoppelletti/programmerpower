<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%--
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="it-scoppelletti-wui-about" class="panel-group" role="tablist"
    aria-multiselectable="true">
<s:iterator value="applications" status="applStatus">	
    <div class="panel panel-default">  
		<div id="it-scoppelletti-wui-about-heading-${applStatus.count}"
		    class="panel-heading" role="tab">
			<h4 class="panel-title">
                <a data-toggle="collapse" aria-expanded="true"
                    data-parent="#it-scoppelletti-wui-about"
href="#it-scoppelletti-wui-about-collapse-${applStatus.count}" 
aria-controls="it-scoppelletti-wui-about-collapse-${applStatus.count}">										
                    <s:property value="applicationName" />
                    <s:if test="version != null">
                        &ndash; <s:property value="version" />
                    </s:if>
				</a>	
			</h4>
		</div>
		<div id="it-scoppelletti-wui-about-collapse-${applStatus.count}"
		    class="panel-collapse collapse" role="tabpanel"
aria-labelledby="it-scoppelletti-wui-about-heading-${applStatus.count}">
			<div class="panel-body">
    			<p>
    				<s:if test="pageUrl != null">
                        <a href="${pageUrl}"
target="_blank"><s:property value="applicationName" /></a>
    				</s:if> 
    				<s:else>
    					<s:property value="applicationName" />
    				</s:else>
    				<s:if test="version != null">
        				<br /><s:property value="version" />
    				</s:if>
    			</p>
    			<s:if test="copyright != null">
					<p><s:property value="copyright" escapeHtml="false" /></p>
    			</s:if>
    			<s:if test="license != null">
        			<div>
            			<s:property value="license" escapeHtml="false" />
	        		</div>
    			</s:if>			
			</div>
		</div>
	</div>    
</s:iterator>	
</div>
        
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%--
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> 
<s:if test="viewState.formMode == 'editPwd'">
    <s:set var="updatePwd" value="%{'updatePwd'}" />
    <sec:authorize
access="hasRole('it.scoppelletti.admin') and '${code}' != authentication.name">
        <s:set var="updatePwd" value="%{'updatePwdAdmin'}" />  
    </sec:authorize>   
    <s:a data-submit="#%{formId}" data-action="%{actionNameBase}X%{updatePwd}"
        cssClass="btn btn-warning" role="button">
        <span class="glyphicon glyphicon-certificate"></span>    
        <s:text name="label.updatePwd" />        
    </s:a>
    <s:a action="%{actionNameBase}Xview" cssClass="btn btn-default"
         role="button">
        <s:param name="viewState.entityId">${viewState.entityId}</s:param>
        <span class="glyphicon glyphicon-remove"></span>    
        <s:text name="label.cancel" />    
    </s:a>                    
</s:if>    
<s:elseif test="viewState.formMode == 'editAdmin'">
    <s:a data-submit="#%{formId}" data-action="%{actionNameBase}XupdateAdmin"
        cssClass="btn btn-warning"  role="button">
        <span class="glyphicon glyphicon-user"></span>    
        <s:text name="label.updateAdmin" />        
    </s:a>
    <s:a action="%{actionNameBase}Xview" cssClass="btn btn-default"
        role="button">
        <s:param name="viewState.entityId">${viewState.entityId}</s:param>
        <span class="glyphicon glyphicon-remove"></span>    
        <s:text name="label.cancel" />    
    </s:a>         
</s:elseif>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>    
<%--
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="wui" uri="/it-scoppelletti-programmerpower-wui-tags" %>
<s:form id="%{formId}">	
	<s:hidden name="viewState" />
	<s:textfield key="code" maxlength="16" cssClass="it-scoppelletti-lowercase"
		readonly="viewState.formMode != 'new'"
		requiredLabel="viewState.formMode == 'new'" />
	<s:textfield key="name" maxlenght="100"
readonly="viewState.formMode != 'new' && viewState.formMode != 'edit'"
requiredLabel="viewState.formMode == 'new' || viewState.formMode == 'edit'" />
    <s:if test="viewState.formMode == 'new'">
        <s:password key="newPwd" showPassword="true" requiredLabel="true" />
        <s:password key="pwdConfirm" showPassword="true" requiredLabel="true" />
    </s:if>
	<s:if test="viewState.formMode == 'editPwd'">	   
        <sec:authorize
access="!hasRole('it.scoppelletti.admin') or '${code}' == authentication.name">	       
            <s:password key="oldPwd" showPassword="true" requiredLabel="true" />
		</sec:authorize>
		<s:text name="label.newPwd" var="labelNewPwd" />
        <s:password key="newPwd" label="%{labelNewPwd}" showPassword="true"
            requiredLabel="true" />
        <s:text name="label.pwdConfirm" var="labelPwdConfirm" />
        <s:password key="pwdConfirm" label="%{labelPwdConfirm}"
            showPassword="true" requiredLabel="true" />		
	</s:if>
    <s:text name="label.role.granted" var="labelRoleGranted" />       
    <s:text name="label.role.available" var="labelRoleAvailable" />
    <s:checkbox key="enabled" value="enabled"
disabled="viewState.formMode != 'new' && viewState.formMode != 'editAdmin'" />	
    <wui:optionSelect key="grantedRoles" optionName="roles"
readOnly="viewState.formMode != 'new' && viewState.formMode != 'editAdmin'"
        selectedSize="10" availableSize="10"
        selectedLabel="%{labelRoleGranted}"
        availableLabel="%{labelRoleAvailable}" />															
</s:form>

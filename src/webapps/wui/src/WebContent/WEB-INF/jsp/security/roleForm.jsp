<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>    
<%--
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="wui" uri="/it-scoppelletti-programmerpower-wui-tags" %>
<s:form id="%{formId}">	
	<s:hidden name="viewState" />
	<s:textfield key="code" maxlength="255" cssClass="it-scoppelletti-lowercase"
		readonly="viewState.formMode != 'new'"
		requiredLabel="viewState.formMode == 'new'" />
    <s:checkbox key="system" value="system" disabled="true" />
	<s:textfield key="description" maxlenght="255"
readonly="viewState.formMode != 'new' && viewState.formMode != 'edit'"
requiredLabel="viewState.formMode == 'new' || viewState.formMode == 'edit'" />
    <s:text name="label.locale.selected" var="labelLocaleSelected" />    	
    <s:text name="label.locale.available" var="labelLocaleAvailable" />
    <wui:optionValueSelect key="localizedDescriptions" optionName="locales"
        readOnly="viewState.formMode != 'new' && viewState.formMode != 'edit'"
        valueRequired="true" valueMaxLen="255" optionSize="10"
        selectedLabel="%{labelLocaleSelected}"
        availableLabel="%{labelLocaleAvailable}" />        
</s:form>

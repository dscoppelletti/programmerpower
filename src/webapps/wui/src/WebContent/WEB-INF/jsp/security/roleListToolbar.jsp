<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%--
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:a action="roleFormXaddNew" cssClass="btn btn-default" role="button">
    <span class="glyphicon glyphicon-plus"></span>
    <s:text name="label.addNew" />
</s:a>
<s:a action="roleList" cssClass="btn btn-default" role="button">
    <span class="glyphicon glyphicon-refresh"></span>    
    <s:text name="label.refresh" />
</s:a>

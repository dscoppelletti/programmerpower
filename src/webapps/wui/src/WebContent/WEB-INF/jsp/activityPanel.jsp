<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%--
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="web" uri="/it-scoppelletti-programmerpower-web-tags" %>
<s:set var="itemCount" value="activities.size" />
<s:set var="colCount" value="5" />
<s:set var="rem" value="#itemCount % #colCount" />  
<s:set var="rowCount" value="(#itemCount - #rem) / 5" />
<s:if test="#rem > 0">
    <s:set var="rowCount" value="#rowCount + 1" />
</s:if>   
<s:iterator var="rowIdx" begin="0" end="#rowCount - 1">
    <s:if test="#rowIdx % 2 == 0">
        <s:set var="offset" value="" />
    </s:if>
    <s:else>
        <s:set var="offset" value="%{'col-md-offset-1'}" />
    </s:else>
    <div class="row">
        <s:set var="begin" value="#rowIdx * #colCount" />
        <s:set var="end" value="#begin + #colCount - 1" />
        <s:if test="#end >= #itemCount">
            <s:set var="end" value="#itemCount - 1" />
        </s:if>
        <s:iterator value="activities" begin="#begin" end="#end">
            <div class="col-md-2 ${offset}">
                <a href="${activityUrl}" class="thumbnail">
                    <img src="${iconUrl}" alt="" />
                    <span class="caption">
                        <s:property value="title" />
                    </span>
                </a>
            </div>
            <s:set var="offset" value="" />
        </s:iterator>
    </div>
</s:iterator>

<%--
 * Copyright (C) 2011-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
--%>   
<%-- Non ho trovato il modo di far eseguire direttamente un'azione Struts 2 come
Welcome Page:
1) Alcuni post sul Web testimoniano di essere riusciti ad impostare un'azione
come <welcome-file>, ma sembra trattarsi sempre di un'azione che non esegue
nulla se non emettere una pagina JSP come risultato.
2) Il tag <jsp:forward> non sembra riuscire a rilevare un'azione.
3) Il tag <s:action> funziona correttamente per includere il risultato di
un'azione, ma, se questa inoltra un'eccezione, fallisce anche la redirezione
alla pagina di errore perche' il flusso di scrittura della pagina JSP risulta
gia' chiuso.
4) Ho indagato sulla possibilita' di gestire la pagina di errore con un inoltro
anziche' con una redirezione (sempre che questo non presenti lo stesso 
problema di cui al punto 3), ma l'unica possibilita' sembra il risultato
ActionChainResult, che pero' lo stessa documentazione di Struts 2 raccomanda,
come regola generale, di non usare (rif.
struts.apache.org/docs/action-chaining.html).
--%>
<%
response.sendRedirect("activityPanel.action");
%>



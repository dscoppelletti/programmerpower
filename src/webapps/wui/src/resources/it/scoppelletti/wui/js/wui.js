/*!
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
(function(global) {
	var init = function() {
		var $ = jQuery;
    
        /*
         * showDialog:
         * Apre un dialogo chiudendo prima l'eventuale dialogo gia' aperto:
         * Bootstrap 3.1.1 non supporta l'apertura di piu' di un dialogo alla
         * volta.
         * 
         * Parametri:
         * $dlg - Dialogo da aprire.
         */ 
        var showDialog = function($dlg) {
            var $currentDlg = $("div.modal.fade.in");
            if ($currentDlg.size() == 0) {
                $dlg.modal( {
                    backdrop: "static",
                    keyboard: false
                });
                
                return;
            }
            
            $currentDlg.one("hidden.it.scoppelletti.wui.modal", function(e) {
                $dlg.modal( {
                    backdrop: "static",
                    keyboard: false
                });
            });
            
            $currentDlg.modal("hide");
        };
        
        /*
         * onClientError:
         * Notifica un errore client.
         * 
         * Parametri:
         * title - Titolo del dialogo.
         * msg - Messaggio di errore.
         */
        var onClientError = function(title, msg) {
            var $dlg = $("#it-scoppelletti-wui-clientError");            
            
            $("#it-scoppelletti-wui-clientErrorTitle").html(title);
            $dlg.find("div.modal-body").html(msg);
            
            showDialog($dlg);
        };
        
	    /*
	     * bindMsgBox:
	     * Registra il gestore degli eventi in un pannello di messaggi.
	     * 
	     * Parametri:
	     * idx - Indice.
	     * el - Elemento.
	     */
	    var bindMsgBox = function(idx, el) {	
	    	var $panel = $(el);
	
	    	var $close = $panel.find("button.close");
	    	if ($close.size() > 0) {
	    		$close.click(function(e) {
	    			$panel.remove();
	    		});
	    	}
	    	
	        $panel.find(
	        		"span.it-scoppelletti-wui-collapseExpand").each(function() {
	           var $icon = $(this);
	
	           var $div = $icon.nextAll("div.collapse");
	           if ($div.size() != 1) {
	               return true;
	           }
	           
	           if ($div.hasClass("in")) {
	               if (!$icon.hasClass("glyphicon-collapse-down")) {
	                   return true;
	               } 
	           } else {
	               if (!$icon.hasClass("glyphicon-expand")) {
	                   return true;
	               }                        
	           }
	                              
	           var $labels = $icon.nextAll("span");
	           var setTitle = function(idx, el) {
	                var $label = $(el);
	                if ($label.hasClass("sr-only")) {
	                    $icon.attr("title", $label.html());
	                    return false;
	                }
	                
	                return true;
	           };
	           
	           $icon.click(function(e) {
	               $div.collapse("toggle");
	           });
	           $div.on("shown.bs.collapse", function() {
	               $icon.removeClass("glyphicon-expand");
	               $icon.addClass("glyphicon-collapse-down");
	               $labels.toggleClass("sr-only hidden");
	               $labels.each(setTitle);
	           });
	           $div.on("hidden.bs.collapse", function() {
	               $icon.removeClass("glyphicon-collapse-down");
	               $icon.addClass("glyphicon-expand");
	               $labels.toggleClass("sr-only hidden");
	               $labels.each(setTitle);
	           });                   
	        });        	
	    };
	    
	    /*
	     * submitForm:
	     * Invia un modulo.
	     * 
	     * Parametri:
	     * $form - Modulo.
	     * $button - Bottone.
	     */
	    var submitForm = function($form, $button) {
	    	if ($button.is("[data-action]")) {
	    		$form.attr("action", $button.attr("data-action"));
	    	}
	    	
	    	$form.get(0).submit();
	    };
	    
	    /*
	     * promptDialog:
	     * Apre il dialogo di conferma per l'invio di un modulo.
	     * 
	     * Parametri:
	     * $form - Modulo.
	     * $button - Bottone.
	     */
	    var promptDialog = function($form, $button) {
	    	var $dlg = $("#it-scoppelletti-wui-promptDialog");
	    	
	    	var title = $button.html();
	    	$dlg.find("h4.modal-title").html(title);
	    	
	        $dlg.find("div.modal-body").html($button.attr("data-prompt"));
	            
	        var $accept = $dlg.find("button.btn-danger");
	        $accept.html(title);
	        
	        $accept.one("click", function() {
	        	$dlg.modal("hide");
	        	submitForm($form, $button);
	        });
	        
	    	showDialog($dlg);
	    };
	    
	    /*
	     * bindForm: 
	     * Registra i gestori degli eventi per un modulo.
	     * 
	     * Parametri:
	     * idx - Indice.
	     * el - Elemento.
	     */
	    var bindForm = function(idx, el) {
    		var $button = $(el);
    		var $form = $($button.attr("data-submit"));
    		if ($form.size() == 1) {
    	   		if ($button.is("[data-prompt]")) {
    	           	$button.click(function(e) {
    	           		e.preventDefault()
    	           		promptDialog($form, $button);
    	        	});	    	    			
        		} else {
        			$button.click(function(e) {
        				e.preventDefault()
        				submitForm($form, $button);
        			});
        		}    		
    		}
	    };
	    
	    /*
	     * optionSelectAdd:
	     * Aggiunge le opzioni selezionate ad una lista.
	     * 
	     * Parametri:
	     * $group - Controllo.
	     * $sel - Lista delle opzioni selezionate.
	     * $avail - Lista delle opzioni disponibili.
	     */
	    var optionSelectAdd = function($group, $sel, $avail) {
	    	var $div = $sel.parent();
	    	if ($div.size() != 1) {
	    		return;
	    	}
	    		    		    	
	    	var $optList = $avail.find("option:selected");
	    	if ($optList.size() == 0) {
	    		onClientError($group.attr("data-labelerror"),
	    				$group.attr("data-error1"));
	    		return;
	    	}
	    	
	    	var name = $group.attr("data-name");
	    	$optList.each(function() {
	    		var $opt = $(this);        	
	    		var html = '<input type="hidden" name="' + name + '" value="' +
	    			$opt.val() + '" />';
	    		
	    		$opt.removeAttr("selected");
	    		$sel.append($opt);        		
	    		$div.append(html);
	    	});
	    };
	    
	    /*
	     * optionSelectRemove:
	     * Rimuove le opzioni selezionate da una lista.
	     * 
	     * Parametri:
	     * $group - Controllo.
	     * $sel - Lista delle opzioni selezionate.
	     * $avail - Lista delle opzioni disponibili.
	     */
	    var optionSelectRemove = function($group, $sel, $avail) {
	    	var $optList = $sel.find("option:selected");
	    	if ($optList.size() == 0) {
	    		onClientError($group.attr("data-labelerror"),
	    				$group.attr("data-error1"));
	    		return;
	    	}
	    	
	    	$optList.each(function() {
	    		var $opt = $(this);
	    		
	    		$sel.nextAll('input[type="hidden"]').each(function() {
	    			var $input = $(this);
	    			
	    			if ($input.val() == $opt.val()) {
	    				$input.remove();
	    				return false;
	    			}
	    			
	    			return true;
	    		});
	    		
	    		$opt.removeAttr("selected");
	    		$avail.append($opt);
	    	});
	    };
	    
	    /*
	     * bindOptionSelect:
	     * Registra i gestori degli eventi per una selezione di opzioni.
	     * 
	     * Parametri:
	     * idx - Indice.
	     * el - Elemento.
	     */
	    var bindOptionSelect = function(idx, el) {
	    	var $group = $(el);
	    	var $cols = $group.find("select");
	    	if ($cols.size() != 2) {
	    		return;
	    	}
	    	
	    	var $sel = $cols.first();
	    	var $avail = $cols.last();
	    	
	    	var $button = $group.find("span.glyphicon-plus");
	    	if ($button.size() == 1) {
	    		$button = $button.parent();
	    		$button.click(function() {
	    			optionSelectAdd($group, $sel, $avail);
	    		});
	    	}
	    	
	    	$button = $group.find("span.glyphicon-minus");
	    	if ($button.size() == 1) {
	    		$button.click(function() {
	    			optionSelectRemove($group, $sel, $avail);
	    		});
	    	}
	    };
	    
	    /*
	     * optionValueSelectRemove:
	     * Rimuove un valore assegnato ad un'opzione.
	     * 
	     * Parametri:
	     * $sel - Lista delle opzioni selezionate.
	     * $avail - Lista delle opzioni disponibili.
	     * $button - Bottone dell'opzione da rimuovere.
	     */
	    var optionValueSelectRemove = function($sel, $avail, $button) {
	    	var $input = $button.parent("span.input-group-btn").prev("input");
	    	if ($input.size() != 1) {
	    		return;
	    	}
	    	
	    	var $value = $input.parent("div.input-group").parent("div").parent(
	    			"div.form-group");
	    	if ($value.size() != 1) {
	    		return;
	    	}
	    	
	    	var html = '<option value="' + $value.attr("data-key") + '">' +
				$value.attr("data-desc") + '</option>';
	    	
	    	if ($value.hasClass("has-error")) {
	    		// Rimuovo anche la notifica di errore
	    		$value.prev("div.form-group").remove();
	    	}
	    	
	    	$value.remove();
	    	$avail.append(html);
	    	
	    	if ($sel.find("input").size() == 0) {
	    		// Etichetta di nessuna opzione con un valore assegnato
	    		$sel.children("div.it-scoppelletti-empty").removeClass(
	    				"hidden");
	    	}
	    };
	    
	    /*
	     * optionValueSelectAdd:
	     * Aggiunge le opzioni selezionate ad una lista di opzioni alle quali
	     * assegnare un valore.
	     * 
	     * Parametri:
	     * $group - Controllo.
	     * $sel - Lista delle opzioni selezionate.
	     * $avail - Lista delle opzioni disponibili.
	     */
	    var optionValueSelectAdd = function($group, $sel, $avail) {
	    	var $optList = $avail.find("option:selected");
	    	var count = $optList.size();
	    	if (count == 0) {
	    		onClientError($group.attr("data-labelerror"),
	    				$group.attr("data-error1"));
	    		return;	    		
	    	}
	    	
	    	var name = $group.attr("data-name");
	    	var labelRemove = $group.attr("data-labelremove");
	    	var labelSep = $group.attr("data-labelsep");
	    	
	    	var required = "";
	    	if ($group.attr("data-required") === "true") {
	    		required = ' <span class="text-danger">*</span>';
	    	}
	    	
	    	var maxlen = $group.attr("data-maxlen");
	    	if (maxlen !== "0") {
	    		maxlen = ' maxlength="' + maxlen + '"';
	    	} else {
	    		maxlen = "";
	    	}
	    	
	    	$optList.each(function() {
	    		var $opt = $(this);        	
	    		var key = $opt.val();
	    		var desc = $opt.html();
	    		var fieldName = name + "." + key;
	    		var html = '<div data-key="' + key + '" data-desc="' + desc +
	    			'" class="form-group">';
	    		html = html + '<label for="' + fieldName +
	    			'" class="col-md-5 control-label">' + desc + required +
	    			labelSep + '</label>';
	    		html = html + '<div class="col-md-7">';
	    		html = html + '<div class="input-group">';
	    		html = html + '<input type="text" name="' + fieldName +
	    			'" class="form-control" value=""' + maxlen + ' />';
	    		html = html + '<span class="input-group-btn">';
	    		html = html + '<button type="button" class="btn btn-danger" ' +
	    			'tile="' +  labelRemove + '">';
	    		html = html +
	    			'<span class="glyphicon glyphicon-remove"></span>';
	    		html = html +
	    			' <span class="sr-only">' + labelRemove + '</span>';
	    		html = html + '</button></span></div></div></div>';
	    		
	    		$opt.remove();
	    		$sel.append(html);
	    	});    	
	    	
	    	if ($sel.find("input").size() > 0) {
	    		// Etichetta di nessuna opzione con un valore assegnato
	    		$sel.children("div.it-scoppelletti-empty").addClass("hidden");
	    	}    	
	    	
    		// Registro il gestore dell'evento di cancellazione per le
    		// opzioni aggiunte	    		
        	$sel.find("button").slice(-count).one("click", function() {
        		var $button = $(this);
        		
        		optionValueSelectRemove($sel, $avail, $button);
        	});    		
	    };
	    
	    /*
	     * bindOptionValueSelect:
	     * Registra i gestori degli eventi per una selezione di opzioni alle
	     * quali assegnare un valore.
	     * 
	     * Parametri:
	     * idx - Indice.
	     * el - Elemento.
	     */
	    var bindOptionValueSelect = function(idx, el) {
	    	var $group = $(el);
	    	var $cols = $group.children("div");
	    	if ($cols.size() != 2) {
	    		return;
	    	}
	    	
	    	var $sel = $cols.first();
	    	var $avail = $cols.last();
	    	
	    	var $button = $avail.find("button");
	    	if ($button.size() != 1) {
	    		return;
	    	}
	    	
	    	$avail = $avail.find("select");
	    	if ($avail.size() != 1) {
	    		return;
	    	}
	    	
	    	$button.click(function() {
	    		optionValueSelectAdd($group, $sel, $avail);
	    	});    	
	    	
	    	$sel.find("button").one("click", function() {
	    		var $button = $(this);
	    		
	    		optionValueSelectRemove($sel, $avail, $button);
	    	});
	    };
	    
	    /*
	     * initUI:
	     * Inizializza l'interfaccia utente.
	     */        
	    var initUI = function() {     
            var $dlg = $("#it-scoppelletti-wui-clientError");
            $dlg.on("hidden.bs.modal", function(e) {
                var $dlg = $(this);
                
                var $el = $("#it-scoppelletti-wui-clientErrorTitle");
                $el.empty();
                
                $el = $dlg.find("div.modal-body");
                $el.empty();                
                
                $dlg.trigger("hidden.it.scoppelletti.wui.modal");
            });            
            
            var $dlg = $("#it-scoppelletti-wui-promptDialog");
            $dlg.on("hidden.bs.modal", function(e) {
                var $dlg = $(this);
                
                var $el = $("h4.modal-title");
                $el.empty();
                
                $el = $dlg.find("div.modal-body");
                $el.empty();                
                
                $el = $dlg.find("button.btn-danger");
                $el.empty();    
                $el.off("click");
                
                $dlg.trigger("hidden.it.scoppelletti.wui.modal");
            });     
            
	    	$('div[role="alert"]').filter(".panel").each(bindMsgBox);
	    	$("a[data-submit],button[data-submit]").each(bindForm);
	    	$("div.it-scoppelletti-optionSelect").each(bindOptionSelect);
	    	$("div.it-scoppelletti-optionValueSelect").each(
	    			bindOptionValueSelect);
	    };
    
	    initUI();
	    
        return {
        	showDialog : showDialog,
        	onClientError : onClientError
        };
	};
	
    var ext = null;
    if (global.it) {
        ext = global.it;
        if (typeof ext !== "object") {
            throw new Error("Cannot add Programmer Power WUI's JavaScript " +
                    "to already existing global object 'it' of type '" +
                    typeof ext + "'.");
        }
    } else {
        ext = { };
    }

    ext.scoppelletti = {
		wui : {
			JsUtils : init()
        }
    };

    global.it = ext;
})(window);

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui.security;

import java.io.*;
import java.util.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.data.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Modello di un utente.
 */
public final class UserModel implements Serializable, Disposable,
        OptimisticLockSupport {
    private static final long serialVersionUID = 1L;
    private static final Logger myLogger = LoggerFactory.getLogger(
            UserModel.class);
    
    /**
     * @serial Id&#46. dell&rsquo;entit&agrave;.
     */
    private Integer myId;
    
    /**
     * @serial Codice.
     */
    private String myCode;
    
    /**
     * @serial Nome.
     */
    private String myName;
    
    /**
     * @serial Vecchia password.
     */
    private SecureString myOldPwd;
    
    /**
     * @serial Nuova password.
     */
    private SecureString myNewPwd;    
    
    /**
     * @serial Conferma della nuova password.
     */
    private SecureString myPwdConfirm;

    /**
     * @serial Indicatore di utente abilitato.
     */
    private boolean myEnabled;

    /**
     * @serial Ruoli.
     */
    private Set<String> myRoleSet;

    /**
     * @serial Indicatore di risorse rilasciate.
     */
    private volatile boolean myIsDisposed = false;
    
    /**
     * @serial Numero di versione.
     */
    private int myVersion;
    
    /**
     * Costruttore.
     */
    UserModel() {
        myRoleSet = new TreeSet<String>();
    }
    
    public void dispose() {
        myIsDisposed = true;
        if (myOldPwd != null) {
            myOldPwd.clear();
            myOldPwd = null;
        }                
        if (myNewPwd != null) {
            myNewPwd.clear();
            myNewPwd = null;
        }        
        if (myPwdConfirm != null) {
            myPwdConfirm.clear();
            myPwdConfirm = null;
        }        
    }
        
    /**
     * Restituisce l&rsquo;id&#46. dell&rsquo;entit&agrave;.
     * 
     * @return Valore.
     */    
    public Integer getId() {
        return myId;
    }

    /**
     * Restituisce il codice dell&rsquo;utente.
     * 
     * @return Valore.
     * @see    #setCode
     */
    public String getCode() {
        return myCode;
    }

    /**
     * Imposta il codice dell&rsquo;utente.
     * 
     * @param value Valore.
     * @see         #getCode
     */
    public void setCode(String value) {
        myCode = (value != null) ? value.toLowerCase() : null;
    }
    
    /**
     * Restituisce il nome.
     * 
     * @return Valore.
     * @see    #setName
     */
    public String getName() {
        return myName;
    }
    
    /**
     * Imposta il nome.
     * 
     * @param value Valore.
     * @see         #getName
     */
    public void setName(String value) {
        myName = value;
    }

    /**
     * Restituisce la vecchia password.
     * 
     * @return Valore.
     * @see    #setOldPwd
     */
    public SecureString getOldPwd() {
        return myOldPwd;
    }
    
    /**
     * Imposta la vaecchia password.
     * 
     * @param value Valore.
     * @see         #getOldPwd
     */
    public void setOldPwd(SecureString value) {
        myOldPwd = value;
    }
    
    /**
     * Restituisce la nuova password.
     * 
     * @return Valore.
     * @see    #setNewPwd
     */
    public SecureString getNewPwd() {
        return myNewPwd;
    }
    
    /**
     * Imposta la nuova password.
     * 
     * @param value Valore.
     * @see         #getNewPwd
     */
    public void setNewPwd(SecureString value) {
        myNewPwd = value;
    }
    
    /**
     * Restituisce la conferma della nuova password.
     * 
     * @return Valore.
     * @see    #setPwdConfirm
     */
    public SecureString getPwdConfirm() {
        return myPwdConfirm;
    }
    
    /**
     * Imposta la conferma della nuova password.
     * 
     * @param value Valore.
     * @see         #getPwdConfirm
     */
    public void setPwdConfirm(SecureString value) {
        myPwdConfirm = value;
    }
    
    /**
     * Indica se l&rsquo;utente &egrave; abilitato.
     * 
     * @return Indicatore.
     * @see    #setEnabled
     */
    public boolean isEnabled() {
        return myEnabled;
    }
    
    /**
     * Imposta l&rsquo;indicatore di utente abilitato.
     * 
     * @param value Valore.
     * @see         #isEnabled
     */
    public void setEnabled(boolean value) {
        myEnabled = value;
    }

    /**
     * Restituisce i ruoli.
     * 
     * @return Collezione.
     * @see    #setGrantedRoles
     */    
    public Set<String> getGrantedRoles() {
        return myRoleSet;
    }
       
    /**
     * Imposta i ruoli.
     * 
     * @return Collezione.
     * @see    #getGrantedRoles
     */    
    public void setGrantedRoles(Set<String> obj) {
        myRoleSet = obj;
    }
    
    public boolean isDisposed() {
        return myIsDisposed;
    }
    
    public int getVersion() {
        return myVersion;
    }
    
    /**
     * Copia i dati di un utente.
     * 
     * @param source Utente sorgente.
     */
    void copyFrom(User source) {
        if (source == null) {
            throw new ArgumentNullException("source");
        }
        
        myId = source.getId();
        myCode = source.getCode();
        myName = source.getName();
        myOldPwd = null;
        myNewPwd = null;
        myPwdConfirm = null;
        myEnabled = source.isEnabled();
        
        myRoleSet = new TreeSet<String>();
        for (Role role : source.getGrantedRoles()) {
            myRoleSet.add(role.getCode());
        }
        
        myVersion = source.getVersion();        
    }
    
    /**
     * Copia i dati su un utente.
     * 
     * @param target Utente di destinazione.
     */
    void copyTo(User target) {
        if (target == null) {
            throw new ArgumentNullException("target");
        }
        
        target.setName(myName);
    }
    
    /**
     * Copia i dati amministrativi su un utente.
     * 
     * @param target Utente di destinazione.
     */
    void copyAdminTo(User target, RoleManager roleMgr) {
        Role role;
        Set<Role> roleSet;
        
        if (target == null) {
            throw new ArgumentNullException("target");
        }
        if (roleMgr == null) {
            throw new ArgumentNullException("roleMgr");
        }
        
        target.setEnabled(myEnabled);
        
        roleSet = new HashSet<Role>();
        for (String code : myRoleSet) {
            role = roleMgr.loadRole(code);
            if (role == null) {
                myLogger.error("Role {} not found.", code);
                continue;
            }
            
            roleSet.add(role);
        }
        
        CollectionTools.update(target.getGrantedRoles(), roleSet);
    }    
}

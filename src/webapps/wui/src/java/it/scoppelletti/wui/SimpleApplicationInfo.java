/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import it.scoppelletti.programmerpower.ArgumentNullException;
import it.scoppelletti.programmerpower.resources.ApplicationInfo;
import it.scoppelletti.programmerpower.web.ApplicationServer;
import it.scoppelletti.programmerpower.wui.ApplicationInfoResource;

/**
 * Implementazione minimale dell&rsquo;interfaccia {@code ApplicationInfo}.
 */
final class SimpleApplicationInfo implements ApplicationInfo {
    private static final long serialVersionUID = 1L;

    /**
     * @serial Nome dell&rsquo;applicazione.
     */
    private final String myApplName;

    /**
     * @serial Numero di versione dell&rsquo;applicazione.
     */
    private final String myVersion;
    
    /**
     * @serial Avviso di copyright.
     */
    private final String myCopyright;
    
    /**
     * @serial Nota di licenza.
     */
    private final String myLicense;
       
    /**
     * @serial URL della guida dell&rsquo;applicazione.
     */
    private final String myPageUrl;
    
    /**
     * Costruttore.
     * 
     * @param appl Applicazione.
     * @param obj  Oggetto sorgente. Pu&ograve; essere {@code null}.
     */
    SimpleApplicationInfo(ApplicationServer.Application appl, JSONObject obj) {
        String name;
        
        if (appl == null) {
            throw new ArgumentNullException("appl");
        }
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        name = StringUtils.isBlank(appl.getName()) ? appl.getContextPath() : 
            appl.getName();
        
        myApplName = obj.optString(ApplicationInfoResource.PROP_APPLNAME,
                name);
        myVersion = obj.optString(ApplicationInfoResource.PROP_VERSION);
        myCopyright = obj.optString(ApplicationInfoResource.PROP_COPYRIGHT);
        myLicense = obj.optString(ApplicationInfoResource.PROP_LICENSE);
        myPageUrl = obj.optString(ApplicationInfoResource.PROP_PAGEURL);
    }
    
    public String getApplicationName() {
        return myApplName;
    }

    public String getVersion() {
        return myVersion;
    }

    public String getCopyright() {
        return myCopyright;
    }

    public String getLicense() {
        return myLicense;
    }
    
    public String getPageUrl() {
        return myPageUrl;
    }
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui.security;

import java.util.*;
import javax.persistence.*;
import com.opensymphony.xwork2.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.web.control.*;
import it.scoppelletti.programmerpower.web.view.*;

/**
 * Gestione di un ruolo.
 */
@Final
public class RoleFormAction extends EntityFormAction<Integer, RoleModel> {
    private static final long serialVersionUID = 1L;
         
    private transient Map<String, String> myLocaleMap;
    
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private transient RoleManager myRoleMgr;
            
    /**
     * Costruttore.
     */
    public RoleFormAction() {
    }

    /**
     * Restituisce le localizzazioni supportate.
     * 
     * @return Collezione.
     */
    public Map<String, String> getLocales() {
        return myLocaleMap;
    }
    
    protected Integer getModelId() {
        return Integer.parseInt(getModelIdAsString());
    }        
    
    protected Integer getEntityId(RoleModel obj) {
        return obj.getId();
    }
   
    @Override
    protected String authorizeAccessImpl(String cmd) {
        RoleModel model;
        
        switch (cmd) {
        case EntityFormAction.CMD_DELETE:
            model = getModel();
            if (model.isSystem()) {
                return "denyAll";
            }
            
            break;
        }
        
        return "hasRole('it.scoppelletti.admin')";
    }

    @Override
    public void beforeResult(ActionInvocation invocation, String resultCode) {
        super.beforeResult(invocation, resultCode);
        
        myLocaleMap = ComponentTools.getAvailableLocales(getLocale());                
    }
    
    protected RoleModel newEntity() {
        return new RoleModel();
    }
    
    protected RoleModel loadEntity(Integer id) {
        Role role;
        RoleModel model;
        
        role = myRoleMgr.loadRole(id);
        if (role == null) {
            return null;
        }
        
        model = new RoleModel();
        model.copyFrom(role);
        
        return model;
    }
        
    protected void saveEntity(RoleModel obj) {
        Role role;
        
        role = myRoleMgr.newRole(obj.getCode());
        obj.copyTo(role);
        myRoleMgr.saveRole(role);
        obj.copyFrom(role);        
    }

    protected void updateEntity(RoleModel obj) {
        Role role;
        
        role = myRoleMgr.loadRole(obj.getId());
        if (role == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
        
        obj.copyTo(role);
        role = myRoleMgr.updateRole(role);
        obj.copyFrom(role);        
    }
   
    protected void deleteEntity(RoleModel obj) {
        Role role;
        
        role = myRoleMgr.loadRole(obj.getId());
        if (role == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
        
        myRoleMgr.deleteRole(role);
    }
        
    @Override
    public void prepareUpdate() {
        super.prepareUpdate();
        getModel().getLocalizedDescriptions().clear();
    }         
}

/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.opensymphony.xwork2.Action;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.restlet.data.MediaType;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.io.IOTools;
import it.scoppelletti.programmerpower.net.RestClient;
import it.scoppelletti.programmerpower.net.RestTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.web.ApplicationServer;
import it.scoppelletti.programmerpower.web.control.ActionBase;
import it.scoppelletti.programmerpower.wui.Activity;
import it.scoppelletti.programmerpower.wui.ActivityListResource;
import it.scoppelletti.programmerpower.wui.DefaultActivity;

/**
 * Pannello delle attivit&agrave;.
 */
@Final
public class ActivityPanelAction extends ActionBase {
    private static final long serialVersionUID = 1L;
    private static final Logger myLogger = LoggerFactory.getLogger(
            ActivityPanelAction.class);
    private transient List<Activity> myActivities;
    
    @javax.annotation.Resource(name = ApplicationServer.BEAN_NAME)
    private transient ApplicationServer myApplServer;
    
    @javax.annotation.Resource(name = RestClient.BEAN_NAME)
    private transient RestClient myRestClient;
    
    /**
     * Costruttore.
     */
    public ActivityPanelAction() { 
    }

    /**
     * Restituisce la collezione delle attivit&agrave;.
     * 
     * @return Collezione.
     */
    public List<Activity> getActivities() {
        return myActivities;
    }
    
    @Override
    public String execute() {
        Reference url;
        List<ApplicationServer.Application> applList;
        
        applList = myApplServer.listApplications();
        myActivities = new ArrayList<>();
        
        for (ApplicationServer.Application appl : applList) {
            if (!appl.isRunning()) {
                myLogger.debug("Application {} is not running.", appl);
                continue;
            }
            
            url = new Reference(myRestClient.getServerUri());
            RestTools.addPath(url, appl.getContextPath());
            RestTools.addPath(url, ActivityListResource.PATH);
            
            try {
                listActivities(url, myActivities);
            } catch (IOException|RuntimeException ex) {
                myLogger.warn(String.format(
                        "Failed to get %1$s (%2$s).", url, ex.getClass()), ex);
                continue;
            }
        }                

        return Action.SUCCESS;
    }
    
    /**
     * Legge l&rsquo;elenco delle attivit&agrave; pubblicate da
     * un&rsquo;applicazione.
     * 
     * @param  url  URL della risorsa.
     * @param  list Collezione delle attivi&agrave;.
     */
    private void listActivities(Reference url, List<Activity> list) throws
            IOException {
        int i, n;
        JSONObject obj;
        JSONArray items;           
        MediaType mediaType;
        InputStream in = null;        
        ClientResource clientRes = null;
        Representation resp = null;
        DefaultActivity activity;
        
        try {
            clientRes = new ClientResource(url);
            clientRes.accept(MediaType.APPLICATION_JSON);
            clientRes.accept(RestTools.toLanguage(getLocale()));
            
            resp = clientRes.get();            
            in = resp.getStream();
            if (in == null) {
                myLogger.warn("GET {} return EmptyRepresentation.");
                return;
            }
            
            mediaType = resp.getMediaType();
            if (!MediaType.APPLICATION_JSON.equals(mediaType)) {
                // Alcune applicazioni potrebbero rispondere con una pagina di
                // default o di errore alla richiesta di una risorsa non
                // esistente
                myLogger.debug("GET {} return a \"{}\" content.", url,
                        mediaType);
                return;
            }          
            
            obj = new JSONObject(new JSONTokener(in));            
        } catch (ResourceException ex) {
            if (Status.CLIENT_ERROR_NOT_FOUND.equals(ex.getStatus())) {
                if (myLogger.isDebugEnabled()) {
                    myLogger.debug(String.format(
                            "Failed to get %1$s.", url), ex);
                }
                
                return;
            } else {
                throw ex;
            }
        } finally {
            in = IOTools.close(in);
            resp = RestTools.release(resp);
            clientRes = RestTools.release(clientRes);
        }
        
        items = obj.getJSONArray(ActivityListResource.PROP_ITEMS);
        n = items.length();
        
        for (i = 0; i < n; i++) {
            obj = items.getJSONObject(i);
            if (obj == null) {
                continue;
            }
            
            activity = new DefaultActivity();
            activity.setTitle(obj.getString(
                    ActivityListResource.PROP_TITLE));
            activity.setActivityUrl(obj.getString(
                    ActivityListResource.PROP_ACTIVITYURL));
            activity.setIconUrl(obj.getString(
                    ActivityListResource.PROP_ICONURL));
            list.add(activity);
        }                   
    }
}

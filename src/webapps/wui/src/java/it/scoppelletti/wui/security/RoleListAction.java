/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui.security;

import java.util.*;
import com.opensymphony.xwork2.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.web.control.*;

/**
 * Elenco dei ruoli.
 */
@Final
public class RoleListAction extends ActionBase {
    private static final long serialVersionUID = 1L;
     
    private transient List<Role> myList;

    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private transient RoleManager myRoleMgr;
    
    /**
     * Costruttore.
     */
    public RoleListAction() {        
    }
    
    /**
     * Elenco dei ruoli.
     * 
     * @return Collezione.
     */
    public List<Role> getList() {
        return myList;
    }
    
    @Override
    public String execute() {
        myList = myRoleMgr.listRoles();
        
        return Action.SUCCESS;
    }    
}

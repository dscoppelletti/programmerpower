/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui.security;

import java.util.*;
import javax.persistence.*;
import com.opensymphony.xwork2.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.resources.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;
import it.scoppelletti.programmerpower.ui.*;
import it.scoppelletti.programmerpower.web.control.*;

/**
 * Gestione di un utente.
 */
@Final
public class UserFormAction extends EntityFormAction<Integer, UserModel> {
    private static final long serialVersionUID = 1L;
    
    /**
     * Comando {@code editPwd}: Modifica la password.
     */
    public static final String CMD_EDITPWD = "editPwd";
    
    /**
     * Comando {@code updatePwd}: Aggiorna la password.
     */
    public static final String CMD_UPDATEPWD = "updatePwd";
        
    /**
     * Comando {@code updatePwdAdmin}: Aggiorna la password di un utente di
     * amministrazione.
     */
    public static final String ADMIN_UPDATEPWD = "updatePwdAdmin";
    
    /**
     * Comando {@code editAdmin}: Modifica i dati di amministrazione.
     */
    public static final String CMD_EDITADMIN = "editAdmin";
    
    /**
     * Comando {@code updateAdmin}: Aggiorna i dati di amministrazione.
     */
    public static final String CMD_UPDATEADMIN = "updateAdmin";
    
    /**
     * Modalit&agrave; {@code editPwd}: modifica della password.
     */
    public static final String MODE_EDITPWD = "editPwd";
    
    /**
     * Modalit&agrave; {@code editAdmin}: modifica della password di un utente
     * di amministrazione.
     */
    public static final String MODE_EDITADMIN = "editAdmin";
        
    private transient boolean myLoggedUserActive;
    
    private transient Map<String, String> myRoleMap;
     
    @javax.annotation.Resource(name = UserManager.BEAN_NAME)
    private transient UserManager myUserMgr;
    
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private transient RoleManager myRoleMgr;
    
    @javax.annotation.Resource(name = PasswordDigester.BEAN_NAME)
    private transient PasswordDigester myPwdDigester;
    
    @javax.annotation.Resource(name = UserSecurityService.BEAN_NAME)
    private transient UserSecurityService mySecurityService;
        
    /**
     * Costruttore.
     */
    public UserFormAction() {
    }
        
    /**
     * Indica se l&rsquo;entit&agrave; corrente corrisponde all&rsquo;utente
     * collegato.
     * 
     * @return Indicatore.
     */
    public boolean isLoggedUserActive() {
        return myLoggedUserActive;
    }
    
    /**
     * Verifica se l&rsquo;entit&agrave; corrente corrisponde all&rsquo;utente
     * collegato.
     * 
     * @return Esito della verifica.
     */
    private boolean initLoggedUserActive() {
        User loggedUser;
        UserModel model;
        
        model = getModel();
        if (model.getId() == null) {
            return false;
        }
        
        loggedUser = myUserMgr.loadLoggedUser();
        if (loggedUser == null) {
            return false;
        }
        
        return model.getId().equals(loggedUser.getId());
    }
    
    /**
     * Restituisce i ruoli supportati.
     * 
     * @return Collezione.
     */
    public Map<String, String> getRoles() {
        return myRoleMap;
    }
           
    protected Integer getModelId() {
        return Integer.parseInt(getModelIdAsString());
    }        
    
    protected Integer getEntityId(UserModel obj) {
        return obj.getId();
    }
   
    @Override
    protected String authorizeAccessImpl(String cmd) {
        switch (cmd) {       
        case EntityFormAction.CMD_DELETE:
            if (myLoggedUserActive) {
                return "denyAll";
            }
            
            return "hasRole('it.scoppelletti.admin')";      
        }
                
        return null;
    }
    
    @Override
    public boolean isInterceptorExcludedForMethod(String interceptor,
            String method) {
        if (super.isInterceptorExcludedForMethod(interceptor, method)) {
            return true;
        }
        
        if (ActionInvocationEx.INTERCEPTOR_PARAMS.equals(interceptor)) {
            if (UserFormAction.CMD_EDITPWD.equals(method) ||
                    UserFormAction.CMD_EDITADMIN.equals(method)) {
                return true;
            }
            
            return false;
        }
        
        if (ActionInvocationEx.INTERCEPTOR_VALIDATION.equals(interceptor)) {
            if (UserFormAction.CMD_EDITPWD.equals(method) ||
                    UserFormAction.CMD_EDITADMIN.equals(method)) {
                return true;
            }
            
            return false;
        }
        
        if (ActionInvocationEx.INTERCEPTOR_WORKFLOW.equals(interceptor)) {
            if (UserFormAction.CMD_EDITPWD.equals(method) ||
                    UserFormAction.CMD_EDITADMIN.equals(method)) {
                return true;
            }
            
            return false;
        }            
        
        return false;
    }
    
    @Override
    public void beforeResult(ActionInvocation invocation, String resultCode) {
        String desc;
        List<Role> list;
                               
        list = myRoleMgr.listRoles();
        
        myRoleMap = new TreeMap<String, String>();        
        for (Role role : list) {
            desc = ResourceTools.getLocalizedString(
                    role.getLocalizedDescriptions(), getLocale(),
                    role.getDescription());
            if (StringUtils.isBlank(desc)) {
                desc = role.getCode();
            }
            
            myRoleMap.put(role.getCode(), desc);
        }
        
        myLoggedUserActive = initLoggedUserActive();
        super.beforeResult(invocation, resultCode);
    }
    
    protected UserModel newEntity() {
        return new UserModel();
    }
    
    @Override
    protected void setDefaults(UserModel obj) {
        obj.setEnabled(true);
    }
    
    protected UserModel loadEntity(Integer id) {
        User user;
        UserModel model;
        
        user = myUserMgr.loadUser(id);
        if (user == null) {
            return null;
        }
        
        model = new UserModel();
        model.copyFrom(user);
        
        return model;
    }
        
    protected void saveEntity(UserModel obj) {
        User user;
        
        user = myUserMgr.newUser(obj.getCode());
        obj.copyTo(user);
        user.setPassword(myPwdDigester.digestPassword(obj.getNewPwd()));
        obj.copyAdminTo(user, myRoleMgr);
        myUserMgr.saveUser(user);
        obj.copyFrom(user);        
    }

    protected void updateEntity(UserModel obj) {
        User user;
        
        user = myUserMgr.loadUser(obj.getId());
        if (user == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
        
        obj.copyTo(user);
        user = myUserMgr.updateUser(user);
        obj.copyFrom(user);        
    }
   
    protected void deleteEntity(UserModel obj) {
        User user;
        
        user = myUserMgr.loadUser(obj.getId());
        if (user == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
        
        myUserMgr.deleteUser(user);
    }
    
    /**
     * Validazione per il comando {@code save}.
     */
    public void validateSave() {
        UserModel model = getModel();
        
        if (ValueTools.isNullOrEmpty(model.getNewPwd())) {
            addFieldError("newPwd", getText("pwd.required"));
        }
        if (ValueTools.isNullOrEmpty(model.getPwdConfirm())) {
            addFieldError("pwdConfirm", getText("pwd.required"));
        } else if (!ValueTools.isNullOrEmpty(model.getNewPwd()) &&
                !model.getPwdConfirm().equals(model.getNewPwd())) {                
            addFieldError("pwdConfirm", getText("pwdConfirm.notValid"));
        }        
    }
        
    /**
     * Comando {@code editPwd}.
     * 
     * @return Risultato.
     */
    public String editPwd() {
        getViewState().put(EntityFormAction.VIEWSTATE_FORMMODE,
                UserFormAction.MODE_EDITPWD);
    
        return Action.SUCCESS;
    }
    
    /**
     * Preparazione per il comando {@code editPwd}.
     */
    public void prepareEditPwd() {
        prepareEdit();
    }
    
    /**
     * Comando {@code updatePwd}.
     * 
     * @return Risultato.
     */
    public String updatePwd() {
        UserModel model;
        
        model = getModel();             
        mySecurityService.updatePwd(model);        
        return doUpdatePwd(model);
    }      
    
    /**
     * Implementazione del comando {@code updatePwd}.
     * 
     * @param  model Modello.
     * @return       Risultato.
     */
    private String doUpdatePwd(UserModel model) {
        User user;
        
        user = myUserMgr.loadUser(model.getId());
        if (user == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
        
        user.setPassword(myPwdDigester.digestPassword(model.getNewPwd()));            
        user = myUserMgr.updateUser(user);
        model.copyFrom(user);
        getUserInterface().display(MessageType.INFORMATION, getText(
                "message.updatePwd"));

        return view();        
    }
    
    /**
     * Preparazione per il comando {@code updatePwd}.
     */
    public void prepareUpdatePwd() {
        prepareUpdate();
    }
    
    /**
     * Validazione per il comando {@code updatePwd}.
     */
    public void validateUpdatePwd() {
        validateUpdatePwd(true);
    }    
    
    /**
     * Validazione per il comando {@code updatePwd}.
     * 
     * @param oldPwd Indica se validare la password precedente.
     */
    private void validateUpdatePwd(boolean oldPwd) {
        String pwd;
        User user;
        UserModel model;
                         
        validateSave();
        
        model = getModel();
        user = myUserMgr.loadUser(model.getId());
        if (user == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
                
        if (oldPwd) {
            if (ValueTools.isNullOrEmpty(model.getOldPwd())) {
                addFieldError("oldPwd", getText("pwd.required"));
            } else {
                model = getModel();
                pwd = myPwdDigester.digestPassword(model.getOldPwd());
                if (!pwd.equals(user.getPassword())) {
                    addFieldError("oldPwd", getText("oldPwd.notValid"));
                }
            }
        }
    }
    
    /**
     * Comando {@code updatePwdAdmin}.
     * 
     * @return Risultato.
     */
    public String updatePwdAdmin() {
        UserModel model;
        
        model = getModel();             
        mySecurityService.updatePwdAdmin(model);
        return doUpdatePwd(model);
    }  
    
    /**
     * Preparazione per il comando {@code updatePwdAdmin}.
     */
    public void prepareUpdatePwdAdmin() {
        prepareUpdate();
    }
    
    /**
     * Validazione per il comando {@code updatePwdAdmin}.
     */
    public void validateUpdatePwdAdmin() {
        validateUpdatePwd(false);
    }
    
    /**
     * Comando {@code editAdmin}.
     * 
     * @return Risultato.
     */
    public String editAdmin() {
        getViewState().put(EntityFormAction.VIEWSTATE_FORMMODE,
                UserFormAction.MODE_EDITADMIN);
    
        return Action.SUCCESS;
    }
    
    /**
     * Preparazione per il comando {@code editAdmin}.
     */
    public void prepareEditAdmin() {
        prepareEdit();
    }
    
    /**
     * Comando {@code updateAdmin}.
     * 
     * @return Risultato.
     */
    public String updateAdmin() {                           
        User user;
        UserModel model;
        
        mySecurityService.updateAdmin();
        
        model = getModel();                     
        user = myUserMgr.loadUser(model.getId());
        if (user == null) {
            throw new OptimisticLockException(getText(
                    "error.concurrency.delete"));             
        }
        
        model.copyAdminTo(user, myRoleMgr);
        user = myUserMgr.updateUser(user);
        model.copyFrom(user);
        getUserInterface().display(MessageType.INFORMATION, getText(
                "message.updateAdmin"));
        
        return view();
    }      
     
    /**
     * Preparazione per il comando {@code updateAdmin}.
     */
    public void prepareUpdateAdmin() {
        prepareUpdate();
        getModel().getGrantedRoles().clear();        
    }       
}

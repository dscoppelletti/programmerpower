/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.opensymphony.xwork2.Action;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.restlet.data.MediaType;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.scoppelletti.programmerpower.io.IOTools;
import it.scoppelletti.programmerpower.net.RestClient;
import it.scoppelletti.programmerpower.net.RestTools;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.resources.ApplicationInfo;
import it.scoppelletti.programmerpower.web.ApplicationServer;
import it.scoppelletti.programmerpower.web.control.ActionBase;
import it.scoppelletti.programmerpower.wui.ApplicationInfoResource;

/**
 * Informazioni sulle applicazioni Web.
 */
@Final
public class AboutAction extends ActionBase {
    private static final long serialVersionUID = 1L;    
    private static final Logger myLogger = LoggerFactory.getLogger(
            AboutAction.class);
    
    private transient List<ApplicationInfo> myApplList;
    
    @javax.annotation.Resource(name = ApplicationServer.BEAN_NAME)
    private transient ApplicationServer myApplClient;
    
    @javax.annotation.Resource(name = RestClient.BEAN_NAME)
    private transient RestClient myRestClient;
   
    /**
     * Costruttore.
     */
    public AboutAction() {        
    }
    
    /**
     * Elenco delle applicazioni in esecuzione.
     * 
     * @return Collezione.
     */
    public List<ApplicationInfo> getApplications() {
        return myApplList;
    }
    
    @Override
    public String execute() {
        Reference url;
        ApplicationInfo about;
        List<ApplicationServer.Application> applList;
        
        applList = myApplClient.listApplications();      
        myApplList = new ArrayList<>();
        
        for (ApplicationServer.Application appl : applList) {
            if (!appl.isRunning()) {
                myLogger.debug("Application {} is not running.", appl);
                continue;
            }
            
            url = new Reference(myRestClient.getServerUri());
            RestTools.addPath(url, appl.getContextPath());
            RestTools.addPath(url, ApplicationInfoResource.PATH); 
            
            try {
                about = loadAbout(appl, url);
            } catch (IOException|RuntimeException ex) {
                about = null;
                myLogger.warn(String.format("Failed to add %1$s.",
                        appl.getContextPath()), ex);
            }
            
            if (about != null) {
                myApplList.add(about);
            }
        }
                
        return Action.SUCCESS;
    } 
    
    /**
     * Legge le informazioni su un&rsquo;applicazione.
     * 
     * @param  appl Applicazione.
     * @param  url  URL della risorsa.
     * @return      Oggetto. Se l&rsquo;applicazione non espone la risorsa,
     *              restituisce {@code null}.
     */
    private ApplicationInfo loadAbout(ApplicationServer.Application appl,
            Reference url) throws IOException {
        JSONObject obj;    
        MediaType mediaType;
        InputStream in = null;
        ClientResource clientRes = null;
        Representation resp = null;   
        
        try {
            clientRes = new ClientResource(url);
            clientRes.accept(MediaType.APPLICATION_JSON);
            clientRes.accept(RestTools.toLanguage(getLocale()));
            
            resp = clientRes.get();
            in = resp.getStream();
            if (in == null) {
                myLogger.warn("GET {} return EmptyRepresentation.",  url);
                return null;
            }
            
            mediaType = resp.getMediaType();
            if (!MediaType.APPLICATION_JSON.equals(mediaType)) {
                // Alcune applicazioni potrebbero rispondere con una pagina di
                // default o di errore alla richiesta di una risorsa non
                // esistente
                myLogger.debug("GET {} return a \"{}\" content.", url,
                        mediaType);
                return null;
            }          
            
            obj = new JSONObject(new JSONTokener(in));             
        } catch (ResourceException ex) {
            if (Status.CLIENT_ERROR_NOT_FOUND.equals(ex.getStatus())) {
                if (myLogger.isDebugEnabled()) {
                    myLogger.debug(String.format(
                            "Failed to get %1$s.", url), ex);
                }
                
                return null;
            } else {
                throw ex;
            }
        } finally {
            in = IOTools.close(in);
            resp = RestTools.release(resp);
            clientRes = RestTools.release(clientRes);
        }       
        
        return new SimpleApplicationInfo(appl, obj);
    }
}

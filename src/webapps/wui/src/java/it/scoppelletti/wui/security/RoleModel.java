/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.wui.security;

import java.io.*;
import java.util.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.data.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Modello di un ruolo.
 */
public final class RoleModel implements Serializable, OptimisticLockSupport {
    private static final long serialVersionUID = 1L;
    
    /**
     * @serial Id&#46; dell&rsquo;entit&agrave;.
     */
    private Integer myId;
    
    /**
     * @serial Codice.
     */
    private String myCode;
    
    /**
     * @serial Indicatore di ruolo di sistema.
     */
    private boolean mySystem;
    
    /**
     * @serial Descrizione.
     */
    private String myDesc;
        
    /**
     * @serial Descrizioni localizzate.
     */
    private Map<String, String> myDescMap;
         
    /**
     * @serial Numero di versione.
     */
    private int myVersion;
        
    /**
     * Costruttore.
     */
    RoleModel() {
        myDescMap = new TreeMap<String, String>();
    }
       
    /**
     * Restituisce l&rsquo;id&#46. dell&rsquo;entit&agrave;.
     * 
     * @return Valore.
     */    
    public Integer getId() {
        return myId;
    }

    /**
     * Restituisce il codice del ruolo.
     * 
     * @return Valore.
     * @see    #setCode
     */
    public String getCode() {
        return myCode;
    }

    /**
     * Imposta il codice del ruolo.
     * 
     * @param value Valore.
     * @see         #getCode
     */
    public void setCode(String value) {
        myCode = (value != null) ? value.toLowerCase() : null;
    }

    /**
     * Indica se il ruolo &egrave; di sistema.
     * 
     * @return Indicatore.
     */
    public boolean isSystem() {
        return mySystem;
    }
    
    /**
     * Restituisce la descrizione.
     * 
     * @return Valore.
     * @see    #setDescription
     */
    public String getDescription() {
        return myDesc;
    }
    
    /**
     * Imposta la descrizione.
     * 
     * @param value Valore.
     * @see         #getDescription
     */
    public void setDescription(String value) {
        myDesc = value;
    }
    
    /**
     * Restituisce le descrizioni localizzate.
     * 
     * @return Collezione.
     * @see    #setLocalizedDescriptions
     */
    public Map<String, String> getLocalizedDescriptions() {
        return myDescMap;
    }
        
    /**
     * Imposta le descrizioni localizzate.
     * 
     * @param obj Collezione.
     * @see       #getLocalizedDescriptions
     */
    public void setLocalizedDescriptions(Map<String, String> obj) {        
        myDescMap = obj;
    }
    
    public int getVersion() {
        return myVersion;
    }
        
    /**
     * Copia i dati di un ruolo.
     * 
     * @param source Ruolo sorgente.
     */
    void copyFrom(Role source) {
        if (source == null) {
            throw new ArgumentNullException("source");
        }
        
        myId = source.getId();
        myCode = source.getCode();
        mySystem = source.isSystem();
        myDesc = source.getDescription();
        myDescMap = new TreeMap<String, String>(
                source.getLocalizedDescriptions());
        myVersion = source.getVersion();        
    }
    
    /**
     * Copia i dati su un ruolo.
     * 
     * @param target Ruolo di destinazione.
     */
    void copyTo(Role target) {
        if (target == null) {
            throw new ArgumentNullException("target");
        }
                
        target.setDescription(myDesc);                
        CollectionTools.update(target.getLocalizedDescriptions(), myDescMap);        
    }      
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.fileshuffle;

import java.io.*;
import java.util.*;
import org.apache.commons.collections4.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Implementazione dell&rsquo;applicazione.
 * 
 * @since 1.0.0
 */
@Final
public class FileShuffler implements Runnable {
    private int myCount;
    private boolean myEmptyDir;
    private FileShuffler.NoRepeat myNoRepeat;
    private String myPrefix;
    private List<File> myInFiles;
    private List<File> myOutDirs;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
    
    /**
     * Costruttore. 
     */
    public FileShuffler() {        
    }
    
    /**
     * Imposta la collezione dei file in input.
     * 
     * @param list Collezione.
     */
    @Required
    public void setInputFiles(List<File> list) {
        myInFiles = list;
    }
    
    /**
     * Imposta la collezione dei direttori di output.
     * 
     * @param list Collezione.
     */
    @Required
    public void setOutputDirectories(List<File> list) {
        myOutDirs = list;
    }
    
    /**
     * Imposta il numero di file da copiare in ogni direttorio.
     * 
     * @param value Valore.
     */
    @Required
    public void setCount(int value) {
        if (value < 1) {
            throw new ArgumentOutOfRangeException("count", value, "[1, ...]");
        }
        
        myCount = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di svuotamento dei direttori di output
     * gi&agrave; esistenti.
     * 
     * @param value Valore.
     */
    public void setEmptyDir(boolean value) {
        myEmptyDir = value;
    }
    
    /**
     * Imposta il prefisso per comporre il nome dei file copiati.
     * 
     * @param value Valore.
     */
    @Required
    public void setPrefix(String value) {
        myPrefix = value;
    }
    
    /**
     * Imposta il controllo sulla non ripetibilit&agrave; dei file copiati.
     * 
     * @param value Valore.
     */
    @Required
    public void setNoRepeat(FileShuffler.NoRepeat value) {
        myNoRepeat = value;
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {
        Set<File> noRepeatSet;
        
        if (CollectionUtils.isEmpty(myInFiles)) {
            throw new PropertyNotSetException(toString(), "inputFiles");
        }
        if (CollectionUtils.isEmpty(myOutDirs)) {
            throw new PropertyNotSetException(toString(), "outputDirectories");
        }        
        if (myCount < 1) {
            throw new PropertyNotSetException(toString(), "count");
        }
        if (StringUtils.isBlank(myPrefix)) {
            throw new PropertyNotSetException(toString(), "prefix");
        }
        if (myNoRepeat == null) {
            throw new PropertyNotSetException(toString(), "noRepeat");
        }                
    
        checkOptions();
        
        noRepeatSet = FileShuffler.NoRepeat.OFF.equals(myNoRepeat) ? null :
            new HashSet<File>();
        for (File dir : myOutDirs) {
            try {
                createDir(dir, noRepeatSet);
            } catch (Exception ex) {
                myUI.display(MessageType.ERROR, ex);
            }            
            
            if (myInFiles.isEmpty()) {
                break;
            }
            
            if (FileShuffler.NoRepeat.OFF.equals(myNoRepeat)) {
                noRepeatSet = null; // free memory
            }            
        }
    }
    
    /**
     * Crea un direttorio.
     * 
     * @param dir         Direttorio.
     * @param noRepeatSet Collezione per controllo sulla non ripetibilit&agrave;
     *                    dei file copiati.
     */
    private void createDir(File dir, Set<File> noRepeatSet) {
        int i, lastNum;
        IOResources ioRes = new IOResources();
                
        if (dir.exists()) {
            if (!dir.isDirectory()) {
                throw new FileAlreadyExistException(dir.toString()); 
            }
            
            if (IOTools.isDirectoryEmpty(dir)) {
                myUI.display(MessageType.INFORMATION,
                        ioRes.getDirectoryFoundMessage(dir.toString()));
            } else {
                myUI.display(MessageType.WARNING,
                        ioRes.getDirectoryNotEmptyMessage(dir.toString()));                
                if (myEmptyDir) { 
                    IOTools.deltree(dir, false);                
                }
            }
        } else {
            if (!dir.mkdirs()) {
                throw new FileCreateException(dir.toString());                
            }
            
            myUI.display(MessageType.INFORMATION,
                    ioRes.getDirectoryCreatedMessage(dir.toString())); 
        } 
        
        lastNum = 0;        
        if (FileShuffler.NoRepeat.INDIRECTORY.equals(myNoRepeat)) {
            noRepeatSet.clear();
        }
        
        for (i = 0; i < myCount; i++) {
            try {
                lastNum = copyFile(dir, lastNum, noRepeatSet);
            } catch (Exception ex) {
                myUI.display(MessageType.ERROR, ex);
                break;
            }           
            
            if (FileShuffler.NoRepeat.OFF.equals(myNoRepeat)) {
                noRepeatSet = null; // free memory
            }                        
        }        
    }
    
    /**
     * Copia uno dei file in input in un direttorio.
     * 
     * @param dir         Direttorio.
     * @param lastNum     Numero identificativo dell&rsquo;ultimo file creato
     *                    nel direttorio di output.                       
     * @param noRepeatSet Collezione di controllo sulla non ripetibilit&agrave;
     *                    dei file copiati.
     * @return            Numero identificativo del file creato nel
     *                    direttorio di output.
     */
    @SuppressWarnings("resource")
    private int copyFile(File dir, int lastNum, Set<File> noRepeatSet) {
        int p;
        int inFileIdx, suffix;
        String name, ext;        
        File inFile, outFile;
        InputStream in = null;
        OutputStream out = null;
        ProgramResources res = new ProgramResources();
        IOResources ioRes = new IOResources();
        
        try {
            // Estraggo il file in input da copiare
            inFile = null; // avoid warning
            while (in == null) {
                inFileIdx = (int) Math.floor(Math.random() *
                        ((double) myInFiles.size()));
                inFile = myInFiles.get(inFileIdx);
                if (noRepeatSet != null && noRepeatSet.contains(inFile)) {
                    continue;
                }
                        
                try {
                    in = new FileInputStream(inFile);
                } catch (FileNotFoundException ex) {
                    myInFiles.remove(inFileIdx);
                    if (myInFiles.isEmpty()) {
                        throw new ApplicationException(
                            res.getNoMoreInputFilesException(), ex);
                    }
                
                    myUI.display(MessageType.ERROR, ex);
                    checkOptions();
                }            
            }

            // Determino l'estensione del nome del file di input
            name = inFile.getName();
            p = name.lastIndexOf('.');
            ext = (p < 0) ? "" : name.substring(p);
            
            // Determino il nome del file di output
            outFile = null;
            suffix = lastNum;            
            while (outFile == null) {
                suffix++;
                name = String.format("%1$s%2$03d%3$s", myPrefix, suffix, ext);
                outFile = new File(dir, name);
                if (outFile.exists()) {
                    myUI.display(MessageType.WARNING, ioRes.getFileFoundMessage(
                            outFile.toString()));
                    outFile = null;
                }               
            }
            
            try {
                out = new FileOutputStream(outFile);
                
                myUI.display(MessageType.INFORMATION, res.getFileCopyingMessage(
                        inFile.toString(), outFile.toString()));
                IOTools.copy(in, out);
            } catch (IOException ex) {
                throw new FileCreateException(outFile.toString(), ex);
            }                                               
        } finally {
            in  = IOTools.close(in);
            out = IOTools.close(out);
        }
             
        if (noRepeatSet != null) {
            noRepeatSet.add(inFile);
        }

        return suffix;
    }
    
    /**
     * Verifica della compatibili&agrave; delle opzioni.
     */
    private void checkOptions() {
        int n;
        ProgramResources res = new ProgramResources();
        
        switch (myNoRepeat) {
        case INDIRECTORY:            
            if (myInFiles.size() < myCount) {
                myNoRepeat = FileShuffler.NoRepeat.OFF;
                myUI.display(MessageType.WARNING, res.getNoRepeatInDirException(
                        myInFiles.size(), myCount));
            }            
            break;
            
        case ATALL:
            n = myCount * myOutDirs.size();
            if (myInFiles.size() < n) {
                myNoRepeat = FileShuffler.NoRepeat.OFF;
                myUI.display(MessageType.WARNING, res.getNoRepeatAtAllException(
                        myInFiles.size(), n));
            }                        
            break;
            
        default:
            // NOP
            break;
        }               
    }
    
    /**
     * Controllo sulla non ripetibilit&agrave; dei file copiati.
     * 
     * @since 1.0.0 
     */
    public static enum NoRepeat {
        
        /**
         * Nessun controllo.
         */
        OFF,
        
        /**
         * Lo stesso file non pu&ograve; essere copiato pi&ugrave; volte nello
         * stesso direttorio. 
         */
        INDIRECTORY,
        
        /**
         * Lo stesso file non pu&ograve; essere copiato pi&ugrave; volte.
         */
        ATALL
    }
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.fileshuffle;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse dell&rsquo;applicazione.
 */
final class ProgramResources extends ResourceWrapper {

    /**
     * Costruttore.
     */
    ProgramResources() {        
    }
    
    /**
     * Eccezione per l&rsquo;applicazione dell&rsquo;opzione
     * {@code FileShuffler.NoRepeat.INDIRECTORY}. 
     * 
     * @param  count Numero di file in input.   
     * @param  min   Numero minimo di file.
     * @return       Testo.
     */
    String getNoRepeatInDirException(int count, int min) {
        return format("NoRepeatInDirException", count, min);                
    }
    
    /**
     * Eccezione per l&rsquo;applicazione dell&rsquo;opzione
     * {@code FileShuffler.NoRepeat.ATALL}. 
     * 
     * @param  count Numero di file in input.   
     * @param  min   Numero minimo di file.
     * @return       Testo.
     */
    String getNoRepeatAtAllException(int count, int min) {
        return format("NoRepeatAtAllException", count, min);                
    }  
    
    /**
     * Eccezione per esaurimento dei file in input validi.
     * 
     * @return Testo.
     */
    String getNoMoreInputFilesException() {
        return getString("NoMoreInputFilesException");
    }
    
    /**
     * Messaggio di copia di un file su un altro.
     * 
     * @param  in  File in input.
     * @param  out File di output.
     * @return     Testo.   
     */
    String getFileCopyingMessage(String in, String out) {
        return format("FileCopyingMessage", in, out);
    }
}

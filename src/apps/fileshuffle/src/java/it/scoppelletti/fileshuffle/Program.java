/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.fileshuffle;

import java.util.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.spring.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
@ApplicationAbout.Annotation(
        copyright = "Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.",
        licenseResource = "it/scoppelletti/programmerpower/resources/asl2.txt",
        pageUrl = "http://www.scoppelletti.it/programmerpower/fileshuffle")
public final class Program extends ConsoleApplicationRunner {
    private static final String RESOURCE_BASENAME =
        "it.scoppelletti.fileshuffle.ProgramOptionSet";
    private static final String OPTION_FILE = "file";
    private static final String OPTION_DIR = "dir";
    private static final String OPTION_COUNT = "count";
    private static final String OPTION_EMPTYDIR = "emptydir";
    private static final String OPTION_PREFIX = "prefix";
    private static final String OPTION_NOREPEAT = "norepeat";
        
    /**
     * Costruttore.
     */
    private Program() {
    }

    @Override
    protected void initOptions(List<CliOption> list) {
        CliOption option;
        CliOptionFileList fileOption;
        CliOptionInteger intOption;
        
        fileOption = new CliOptionFileList(Program.OPTION_FILE,
                Program.RESOURCE_BASENAME);
        fileOption.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        fileOption.setSizeMin(2);
        list.add(fileOption);

        fileOption = new CliOptionFileList(Program.OPTION_DIR,
                Program.RESOURCE_BASENAME);
        fileOption.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        fileOption.setSizeMin(1);
        list.add(fileOption);
        
        intOption = new CliOptionInteger(Program.OPTION_COUNT,
                Program.RESOURCE_BASENAME);
        intOption.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        intOption.setValueMin(1);
        list.add(intOption);
        
        option = new CliOptionBoolean(Program.OPTION_EMPTYDIR,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionString(Program.OPTION_PREFIX,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionEnum<FileShuffler.NoRepeat>(
                Program.OPTION_NOREPEAT, Program.RESOURCE_BASENAME,
                FileShuffler.NoRepeat.class, FileShuffler.NoRepeat.OFF);
        list.add(option);                
        
        super.initOptions(list);
    }    
         
    /**
     * Restituisce la collezione dei file in input.
     * 
     * @return Opzione
     */
    public CliOptionFileList getInputFiles() {
        return (CliOptionFileList) getOption(Program.OPTION_FILE);
    }
    
    /**
     * Restituisce la collezione dei direttori di output.
     * 
     * @return Opzione
     */
    public CliOptionFileList getOutputDirectories() {
        return (CliOptionFileList) getOption(Program.OPTION_DIR);
    }
    
    /**
     * Entry-point.
     * 
     * @param args Argomenti sulla linea di comando.
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/fileshuffle/cli.html"
     *      TARGET="_top">Linea di comando</A> 
     */    
    public static void main(String args[]) {
        Program appl = new Program();

        appl.run(args);
    }
     
    /**
     * Controlli di validazione.
     */          
    @Override
    @SuppressWarnings("unchecked")
    public void checkOptions() {
        int n;
        int sizeMin;
        CliOptionEnum<FileShuffler.NoRepeat> noRepeatOption;
        CliOptionInteger intOption;
        CliOptionFileList fileOption;
                
        noRepeatOption = (CliOptionEnum<FileShuffler.NoRepeat>)
                getOption(Program.OPTION_NOREPEAT);
        
        sizeMin = 1;
        switch (noRepeatOption.getValue()) {
        case INDIRECTORY:
            intOption = (CliOptionInteger) getOption(Program.OPTION_COUNT);            
            if (intOption.isSpecified()) {
                n = intOption.getValue();
                if (n >= intOption.getValueMin()) {
                    sizeMin *= n;
                }
            }            
            break;
            
        case ATALL:
            intOption = (CliOptionInteger) getOption(Program.OPTION_COUNT);            
            if (intOption.isSpecified()) {
                n = intOption.getValue();
                if (n >= intOption.getValueMin()) {
                    sizeMin *= n;
                }
            }    
            
            fileOption = getOutputDirectories();
            if (fileOption.isSpecified()) {
                n = fileOption.getList().size();
                if (n >= fileOption.getSizeMin()) {
                    sizeMin *= n;
                }
            }            
            break;
            
        default: // OFF
            break;
        }
               
        fileOption = getInputFiles();
        if (sizeMin > fileOption.getSizeMin()) {
            fileOption.setSizeMin(sizeMin);
        }
        
        super.checkOptions();
    }
}


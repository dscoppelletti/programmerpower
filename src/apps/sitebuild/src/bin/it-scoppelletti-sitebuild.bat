@echo off
setlocal
set _APPLNAME=${module.name}
set _APPLVER=${module.version}
it-scoppelletti-runtime.bat %_APPLNAME% %_APPLVER% %*
set _EXITCODE=%ERRORLEVEL%
:finish
endlocal
exit /b %_EXITCODE%

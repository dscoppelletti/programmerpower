/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.beans.*;
import java.lang.reflect.*;
import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.reflect.NotImplementedException;
import it.scoppelletti.sitebuild.spi.*;

/**
 * Wrapper di una macro.
 */
final class MacroWrapper {
    private static final String PROP_WRITE_PREFIX = "set";
    private static final Logger myLogger = LoggerFactory.getLogger(
            MacroWrapper.class);    
    private final String myKey;
    private final Class<?> myMacroClass;
    private final PropertyDescriptor[] myProps;
    private Map<String, MacroPropertySetter> myPropSetters;
    
    /**
     * Costruttore.
     * 
     * @param  key       Chiave della macro.
     * @param  className Nome della classe che implementa la macro. 
     * @throws it.scoppelletti.programmerpower.reflect.ReflectionException
     */
    MacroWrapper(String key, String className) {        
        SiteMacro macro;
        BeanInfo beanInfo;
        PropertyDescriptor[] props;

        if (StringUtils.isBlank(key)) {
            throw new ArgumentNullException("key"); 
        }        
        if (StringUtils.isBlank(className)) {
            throw new ArgumentNullException("className");
        }
        
        myKey = key;
        
        try {
            myMacroClass = Class.forName(className);
        } catch (ClassNotFoundException ex) {
            throw new ReflectionException(ex);
        }
               
        // Provo ad istanziare un oggetto solo per verificare che implementi
        // l'interfaccia SiteMacro con un costruttore senza parametri
        try {
            macro = (SiteMacro) myMacroClass.newInstance();
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }
        
        try {
            // Acquisisco la classe dal provider anziche' utilizzare
            // direttamente myMacroClass, cosi' il compilatore la smette di
            // darmi il warning perche' ho inizializzato la variabile macro
            // ma non l'ho mai usata.
            beanInfo = Introspector.getBeanInfo(macro.getClass());
            macro = null;
        } catch (IntrospectionException ex) {
            throw new ReflectionException(ex);
        }
        
        props = beanInfo.getPropertyDescriptors();
        if (props != null) {
            myProps = props;
        } else {
            myProps = new PropertyDescriptor[0];
        }
        
        myPropSetters = new LinkedHashMap<String, MacroPropertySetter>();
    }
    
    /**
     * Termine dell&rsquo;inizializzazione.
     */
    void endInit() {
        myPropSetters = Collections.unmodifiableMap(myPropSetters);
    }
    
    /**
     * Restituisce la chiave della macro.
     * 
     * @return Valore.
     */
    String getKey() {
        return myKey;
    }  
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("%1$s(%2$s)", myMacroClass.getName(), myKey);
    }
    
    /**
     * Apre il flusso di lettura del testo della macro.
     *  
     * @param  page Pagina nella quale deve essere inserita la macro.
     * @return      Flusso di lettura.
     */
    Reader openReader(SitePage page) {
        Reader reader;
        Disposable macroDisp;
        SiteMacro macro;
        
        if (page == null) {
            throw new ArgumentNullException("page");
        }
        
        myLogger.trace("Start rebuilding macro {} for page {}.", this, page);        
        try {
            try {
                macro = (SiteMacro) myMacroClass.newInstance();
            } catch (Exception ex) {
                throw new ReflectionException(ex);
            }
            if (macro instanceof Disposable) {
                macroDisp = (Disposable) macro;
            } else {
                macroDisp = null;
            }
    
            try {
                for (MacroPropertySetter setter : myPropSetters.values()) {
                    setter.run(macro, page);
                }
            
                reader = macro.openReader(myKey, page);
            } finally {
                if (macroDisp != null) {
                    macroDisp.dispose();
                    macroDisp = null;
                }
            }
        } finally {
            myLogger.trace("End rebuilding macro {} for page {}.", this, page);     
        }
                
        return reader;
    }
    
    /**
     * Aggiunge la valorizzazione di una propriet&agrave;.
     *
     * @param name Nome della propriet&agrave;.
     * @param text Testo di valorizzazione della propriet&agrave;.
     */
    void putPropertyValue(String name, String text) {
        Method setter;
        MacroPropertySetter value;
        
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name"); 
        }        
     
        name = name.toLowerCase();
        if (myPropSetters.containsKey(name)) {
            throw new ObjectDuplicateException("name");
        }
        
        setter = getPropertySetter(name);
        if (setter == null) {
            throw new NotImplementedException(myMacroClass.getName(),
                    MacroWrapper.PROP_WRITE_PREFIX.concat(name));
        }
        
        value = new MacroPropertySetter(name, setter, text);
        myPropSetters.put(value.getName(), value);
    }
    
    /**
     * Restituisce il metodo accessore di scrittura di una propriet&agrave;.
     * 
     * @param  name Nome della propriet&agrave;
     * @return      Metodo. Se il metodo non esiste, restituisce {@code null}.
     */
    private Method getPropertySetter(String name) {
        PropertyDescriptor prop;
        Method setter;
                 
        prop = null;
        for (PropertyDescriptor desc : myProps) {
            if (name.equalsIgnoreCase(desc.getName())) {
                prop = desc;
                break;
            }
        }                
        if (prop == null) {
            return null;                      
        }
     
        // Non supporto le proprieta' indicizzate
        if (prop instanceof IndexedPropertyDescriptor) {
            return null;                      
        }
        
        setter = prop.getWriteMethod();
        if (setter == null) {
            return null;            
        }
        
        // Credo che l'introspezione non dovrebbe riconoscere la proprieta' se
        // il metodo accessore di scrittura non ha uno ed un solo parametro, ma
        // io ho rilevato degli esempi di utilizzo che controllano comunque.
        if (setter.getParameterTypes().length != 1) {
            return null;
        }
        
        return setter;
    }   
}

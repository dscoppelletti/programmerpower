/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Macro sostituita con il testo letto dal file impostato come propriet&agrave;.
 * 
 * @see   <A
 *        HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/macrofile.html"
 *        TARGET="_top">Macro <CODE>MacroFile</CODE></A>  
 * @since 1.0.0
 */
public class MacroFile extends AbstractSiteMacro {
    private String myPath;

    /**
     * Costruttore.
     */
    public MacroFile() {        
    }
    
    /**
     * Restituisce il percorso del file.
     * 
     * @return Valore.
     * @see    #setPath
     */
    public String getPath() {
        return myPath;
    }
    
    /**
     * Imposta percorso del file.
     * 
     * @param path Percorso. Pu&ograve; essere assoluto oppure relativo al
     *             direttorio di progetto.
     * @it.scoppelletti.tag.required
     * @see        #getPath
     * @see        it.scoppelletti.sitebuild.SiteProject#getProjectDirectory
     */
    public void setPath(String path) {
        if (path == null) {
            throw new ArgumentNullException("path");
        }
        
        myPath = path;
    }
        
    @Override
    protected Reader openReader() {
        File file;
        Reader reader;
        
        if (StringUtils.isBlank(myPath)) {
            throw new PropertyNotSetException(toString(), "path");
        }
        
        file = new File(myPath);
        if (!file.isAbsolute()) {
            file = new File(getPage().getProject().getProjectDirectory(),
                    myPath);
        }
        
        try {
            reader = new FileReader(file);
        } catch (IOException ex) {
            throw new IOOperationException(ex); 
        }
        
        return reader;
    }
}

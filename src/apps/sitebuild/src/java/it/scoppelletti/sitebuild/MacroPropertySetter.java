/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import java.lang.reflect.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.xml.*;
import it.scoppelletti.sitebuild.spi.*;

/**
 * Impostazione del valore di una propriet&agrave; di una macro.
 */
final class MacroPropertySetter {
    private final String myName;
    private final Method mySetter;
    private final String myText;
    
    /**
     * Costruttore.
     * 
     * @param name   Nome della propriet&agrave;.
     * @param setter Metodo accessore di scrittura della propriet&agrave;.
     * @param text   Testo di valorizzazione della propriet&agrave;.
     */
    MacroPropertySetter(String name, Method setter, String text) {
        if (StringUtils.isBlank(name)) {
            throw new ArgumentNullException("name");
        }
        if (setter == null) {
            throw new ArgumentNullException("setter");
        }
        if (text == null) {
            throw new ArgumentNullException("text");
        }
        
        myName = name;
        mySetter = setter;
        myText = text;
    }

    /**
     * Restituisce il nome della propriet&agrave;
     * 
     * @return Valore.
     */
    String getName() {
        return myName;
    }
    
    /**
     * Esegue l&rsquo;operazione.
     * 
     * @param macro Macro sulla quale impostare la propriet&agrave;.
     * @param page  Pagina per l&rsquo;espansione di eventuali macro nel testo
     *              del valore da impostare. 
     */
    void run(SiteMacro macro, SitePage page) {
        String text;
        Object value;
        Reader reader = null;
        Writer writer = null;
        Class<?>[] paramTypes;
        SitePageBuilder textBuilder;
        
        if (macro == null) {
            throw new ArgumentNullException("macro");
        }
        if (page == null) {
            throw new ArgumentNullException("page");
        }
        
        try {
            reader = new StringReader(myText);
            writer = new StringWriter();
            textBuilder = new SitePageBuilder(page, reader, writer);
            textBuilder.run();
            text = writer.toString();
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } finally {
            reader = IOTools.close(reader);
            writer = IOTools.close(writer);
        }
        
        paramTypes = mySetter.getParameterTypes();
        value = XmlTools.parseObject(text, paramTypes[0]);
        try {            
            mySetter.invoke(macro, new Object[] { value });
        } catch (Exception ex) {
            throw new ReflectionException(ex);
        }                        
    }
}

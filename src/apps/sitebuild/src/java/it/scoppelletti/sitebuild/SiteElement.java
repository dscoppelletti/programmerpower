/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Elemento di un sito.
 * 
 * @since 1.0.0
 */
public abstract class SiteElement {
    private final SiteProject myProject;
    private final SiteElement myParentEl;
    private String myNumber;
    private SiteNumberingStyle myNumberingStyle;
    private Map<String, MacroWrapper> myMacros;
       
    /**
     * Costruttore.
     */
    SiteElement() {
        myProject = (SiteProject) this;
        myParentEl = null;
        myNumber = null;
        myNumberingStyle = null;
        myMacros = new HashMap<String, MacroWrapper>();
    }
    
    /**
     * Costruttore.
     * 
     * @param project  Progetto.
     * @param parentEl Elemento parent.
     */
    SiteElement(SiteProject project, SiteElement parentEl) {
        if (project == null) {
            throw new ArgumentNullException("project");
        }
        if (parentEl == null) {
            throw new ArgumentNullException("parentEl");
        }
        
        myProject = project;
        myParentEl = parentEl;
        myNumber = null;
        myMacros = new HashMap<String, MacroWrapper>();
    }
    
    /**
     * Termine dell&rsquo;inizializzazione.
     */
    void endInit() {
        myMacros = Collections.unmodifiableMap(myMacros);
    }
    
    /**
     * Restituisce il progetto.
     * 
     * @return Oggetto.
     */
    public SiteProject getProject() {
        return myProject;
    }
    
    /**
     * Restituisce l&rsquo;elmento parent.
     * 
     * @return Oggetto. Se l&rsquo;elemento non ha un elemento parent,
     *         restituisce {@code null}.
     */
    public final SiteElement getParentElement() {
        return myParentEl;
    }
    
    /**
     * Restituisce il numero d&rsquo;ordine dell&rsquo;elemento.
     * 
     * @return Valore.
     * @see    #getNumberingStyle
     * @since  1.1.0
     */
    public final String getNumber() {
        return myNumber;
    }
    
    /**
     * Imposta il numero d&rsquo;ordine dell&rsquo;elemento.
     * 
     * @param value Valore.
     */
    final void setNumber(String value) {
        myNumber = value;
    }
    
    /**
     * Restituisce lo stile della numerazione degli elementi sottostanti.
     * 
     * @return Valore.
     * @see    #getNumber
     * @since  1.1.0
     */
    public final SiteNumberingStyle getNumberingStyle() {
        return myNumberingStyle;
    }
    
    /**
     * Imposta lo stile della numerazione degli elementi sottostanti.
     * 
     * @return Valore.
     */    
    final void setNumberingStyle(SiteNumberingStyle value) {
        myNumberingStyle = value;
    }
        
    /**
     * Restituisce la collezione delle macro.
     * 
     * @return Collezione.
     */
    final Map<String, MacroWrapper> getMacros() {
        return myMacros;
    }
    
    /**
     * Restituisce una macro.
     * 
     * @param  key Chiave della macro.
     * @return     Oggetto. Se la macro non esiste, restituisce {@code null}.
     */    
    final MacroWrapper getMacro(String key) {
        SiteElement el;
        MacroWrapper macro;
        
        if (StringUtils.isBlank(key)) {
            throw new ArgumentNullException("key");
        }
        
        el = this;
        while (el != null) {
            macro = el.getMacros().get(key);
            if (macro != null) {
                return macro;
            }
            
            el = el.getParentElement();
        }
        
        return null;
    }
    
    /**
     * Aggiunge una macro.
     * 
     * @param macro Macro.
     */
    final void putMacro(MacroWrapper macro) {
        if (macro == null) {
            throw new ArgumentNullException("macro");
        }
        
        if (myMacros.put(macro.getKey(), macro) != null) {
            throw new ObjectDuplicateException(macro.toString());
        }                  
        
        macro.endInit();
    }        
}

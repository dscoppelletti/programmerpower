/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.util.*;
import it.scoppelletti.programmerpower.*;

/**
 * Lista di sezioni di un sito.
 * 
 * @since 1.0.0
 */
public abstract class SiteSectionList extends SiteElement {
    private List<SiteSection> mySections;
    
    /**
     * Costruttore.
     */
    SiteSectionList() {
        mySections = new ArrayList<SiteSection>();
    }
    
    /**
     * Costruttore.
     * 
     * @param project  Progetto.
     * @param parentEl Elemento parent.
     */
    SiteSectionList(SiteProject project, SiteElement parentEl) {
        super(project, parentEl);
       
        mySections = new ArrayList<SiteSection>();
    }
    
    /**
     * Termine dell&rsquo;inizializzazione.
     */
    @Override
    final void endInit() {
        mySections = Collections.unmodifiableList(mySections);                
        super.endInit();
    }
    
    /**
     * Restituisce la lista delle sezioni.
     * 
     * @return Collezione.
     */
    public final List<SiteSection> getSections() {
        return mySections;
    }
    
    /**
     * Aggiunge una sezione.
     * 
     * @param section Sezione.
     */
    final void addSection(SiteSection section) {
        if (section == null) {
            throw new ArgumentNullException("section");
        }
        
        section.setNumber(getNumberingStyle().toString(mySections.size() + 1));
        mySections.add(section);
    }   
    
    /**
     * Ricompila le sezioni.
     */
    final void rebuildSections() {
        for (SiteSection section : getSections()) {
            for (SitePage page : section.getPages()) {
                page.rebuild();       
            }
        }        
    }       
}

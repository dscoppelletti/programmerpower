/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Implementazione dell&rsquo;applicazione.
 * 
 * @since 1.1.1
 */
@Final
public class SiteProjectBuilder implements Runnable {
    private File myProjectFile;
    
    /**
     * Costruttore.
     */
    public SiteProjectBuilder() {        
    }
    
    /**
     * Imposta il file di configurazione.
     * 
     * @param file File.
     */
    @Required
    public void setProjectFile(File file) {
        myProjectFile = file;
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */    
    public void run() {  
        if (myProjectFile == null) {
            throw new PropertyNotSetException(toString(), "projectFile");
        }
        
        SiteProject project;
        SiteProjectFactory projectFactory;
        
        projectFactory = new SiteProjectFactory();
        project = projectFactory.loadProject(myProjectFile);
        project.rebuild();        
    }
}

/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.sitebuild.*;
import it.scoppelletti.programmerpower.*;

/**
 * Classe di base per l&rsquo;implementazione dell&rsquo;interfaccia
 * {@code SiteMacro}.
 * 
 * <P>Una terza parte pu&ograve; utilizzare la classe {@code AbstractSiteMacro}
 * come classe di base da derivare per implementare una propria macro
 * {@code SiteMacro}; questo consente anche di mantenere la compatibilit&agrave;
 * in caso di future estensioni dell&rsquo;interfaccia
 * {@code SiteMacro}.</P>
 *  
 * @since 1.0.0
 */
public abstract class AbstractSiteMacro implements SiteMacro {
    private String myMacroKey;
    private SitePage myPage;
    
    /**
     * Costruttore.
     */
    protected AbstractSiteMacro() {        
    }
    
    public final Reader openReader(String macroKey, SitePage page) {
        if (StringUtils.isBlank(macroKey)) {
            throw new ArgumentNullException("macroKey");
        }
        if (page == null) {
            throw new ArgumentNullException("page");
        }        
        
        myMacroKey = macroKey;
        myPage = page;
        
        return openReader();
    }
    
    /**
     * Apre il flusso di lettura del testo della macro.
     * 
     * @return Flusso di lettura.
     */
    protected abstract Reader openReader();
    
    /**
     * Restituisce la chiave che identifica la macro.
     * 
     * @return Valore.
     */
    protected final String getMacroKey() {
        return myMacroKey;
    }
    
    /**
     * Restituisce la pagina nella quale deve essere inserito il testo della
     * macro.
     * 
     * @return Oggetto.
     */
    protected final SitePage getPage() {
        return myPage;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("%1$s(%2$s)", getClass().getName(), myMacroKey);
    }    
}

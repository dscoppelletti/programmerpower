/*
 * Copyright (C) 2009-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.util.*;
import it.scoppelletti.programmerpower.*;

/**
 * Sezione di un sito.
 * 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/project.html"
 *        TARGET="_top">Progetto</A> 
 * @since 1.0.0
 */
public final class SiteSection extends SiteElement {
    private List<SitePage> myPages;
    
    /**
     * Costruttore.
     * 
     * @param project     Progetto.
     * @param sectionList Lista delle sezioni.
     */
    SiteSection(SiteProject project, SiteSectionList sectionList) {
        super(project, sectionList);
        
        myPages = new ArrayList<SitePage>();
        sectionList.addSection(this);
    }
 
    /**
     * Termine dell&rsquo;inizializzazione.
     */
    @Override
    void endInit() {
        int i, n;
                       
        // Imposto le relazioni d'ordine tra le pagine della sezione
        n = myPages.size();
        for (i = 0; i < n; i++) {
            if (i > 0) {
                myPages.get(i).setPreviousPage(myPages.get(i - 1));
            }
            if (i < n - 1) {
                myPages.get(i).setNextPage(myPages.get(i + 1));                
            }
        }
        
        myPages = Collections.unmodifiableList(myPages);
        super.endInit();
    }
        
    /**
     * Restituisce la lista delle pagine.
     * 
     * @return Collezione.
     */
    public final List<SitePage> getPages() {
        return myPages;
    }
 
    /**
     * Aggiunge una pagina.
     * 
     * @param page Pagina.
     */
    final void addPage(SitePage page) {
        if (page == null) {
            throw new ArgumentNullException("page");
        }

        page.setNumber(getNumberingStyle().toString(myPages.size() + 1));
        myPages.add(page);
    }
}

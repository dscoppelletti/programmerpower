/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.scoppelletti.sitebuild;

import java.util.*;

final class RomanNumbering {
    private final List<RomanNumbering.Symbol> mySymbols;

    /**
     * Costruttore.
     * 
     * @param lowerCase Indica se utilizzare le lettere minuscole.
     */
    RomanNumbering(boolean lowerCase) {
        List<RomanNumbering.Symbol> list;
            
        list = new ArrayList<RomanNumbering.Symbol>(13);
        list.add(new RomanNumbering.Symbol("M", 1000, lowerCase));
        list.add(new RomanNumbering.Symbol("CM", 900, lowerCase));
        list.add(new RomanNumbering.Symbol("D", 500, lowerCase));
        list.add(new RomanNumbering.Symbol("CD", 400, lowerCase));
        list.add(new RomanNumbering.Symbol("C", 100, lowerCase));
        list.add(new RomanNumbering.Symbol("XC", 90, lowerCase));
        list.add(new RomanNumbering.Symbol("L", 50, lowerCase));
        list.add(new RomanNumbering.Symbol("XL", 40, lowerCase));
        list.add(new RomanNumbering.Symbol("X", 10, lowerCase));
        list.add(new RomanNumbering.Symbol("IX", 9, lowerCase));
        list.add(new RomanNumbering.Symbol("V", 5, lowerCase));
        list.add(new RomanNumbering.Symbol("IV", 4, lowerCase));
        list.add(new RomanNumbering.Symbol("I", 1, lowerCase));
        
        mySymbols = Collections.unmodifiableList(list);
    }
        
    /**
     * Converte un numero nella sua rappresentazione.
     * 
     * @param  n Numero da convertire.
     * @return   Rappresentazione.
     */    
    String toString(int n) {
        StringBuilder buf = new StringBuilder();
        
        for (RomanNumbering.Symbol symbol : mySymbols) {
            if (n <= 0) {
                break;
            }
            
            while (n >= symbol.getValue()) {
                buf.append(symbol.getSymbol());
                n -= symbol.getValue();
            }
        }
        
        return buf.toString();
    }
    
    /**
     * Simbolo.
     */
    private static final class Symbol {
        private final String mySymbol;
        private final int myValue;
       
        /**
         * Costruttore.
         * 
         * @param symbol    Simbolo.
         * @param value     Valore.
         * @param lowerCase Indica se utilizzare le lettere minuscole.
         */
        Symbol(String symbol, int value, boolean lowerCase) {
            mySymbol = (lowerCase) ? symbol.toLowerCase() : symbol;
            myValue = value;
        }
        
        /**
         * Restituisce il simbolo.
         * 
         * @return Valore.
         */
        String getSymbol() {
            return mySymbol;
        }
        
        /**
         * Restituisce il valore.
         * 
         * @return Valore.
         */
        int getValue() {
            return myValue;
        }
    }
}

/*
 * Copyright (C) 2009-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Macro sostituita con il testo impostato come propriet&agrave;.
 * 
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/macrotext.html"
 *      TARGET="_top">Macro <CODE>MacroText</CODE></A> 
 * @since 1.0.0
 */
public class MacroText extends AbstractSiteMacro {
    private String myText = "";

    /**
     * Costruttore.
     */
    public MacroText() {        
    }
    
    /**
     * Restituisce il testo della macro.
     * 
     * @return Valore.
     * @see    #setText
     */
    public String getText() {
        return myText;
    }
        
    /**
     * Imposta il testo della macro.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Stringa vuota {@code ""}.
     * @see                               #getText
     */
    public void setText(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myText = value;
    }
    
    @Override
    protected Reader openReader() {
        return new StringReader(myText);
    }
}

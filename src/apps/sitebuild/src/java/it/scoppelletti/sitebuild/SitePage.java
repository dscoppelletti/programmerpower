/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Pagina di un sito.
 * 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/project.html"
 *        TARGET="_top">Progetto</A> 
 * @since 1.0.0
 */
public final class SitePage extends SiteSectionList {
    private static final Logger myLogger = LoggerFactory.getLogger(
            SitePage.class);        
    private final File myFile;
    private final SitePage myParent;
    private String myBuildPath = null;
    private String myRootPath = null;
    private SitePage myNext = null;
    private SitePage myPrev = null;
        
    /**
     * Costruttore.
     *
     * @param project Progetto.
     * @param section Sezione nella quale la pagina &egrave; inclusa.
     * @param file    File sorgente della pagina (percorso relativo al
     *                direttorio dei file sorgente del progetto).
     */
    SitePage(SiteProject project, SiteSection section, File file) {
        super(project, section);
        
        SiteElement el;
        
        if (file == null) {
            throw new ArgumentNullException("file");            
        }
         
        myFile = file;
                        
        el = getParentElement().getParentElement();        
        if (el instanceof SitePage) {
            myParent = (SitePage) el;
        } else {
            myParent = null;            
        }
        
        section.addPage(this);
    }
   
    /**
     * Restituisce il file sorgente della pagina.
     * 
     * @return File (percorso relativo al direttorio dei file sorgente del
     *         progetto).
     * @see    it.scoppelletti.sitebuild.SiteProject#getSourceDirectory
     */
    public File getSourceFile() {
        return myFile;
    }
    
    /**
     * Restituisce il percorso del file compilato della pagina.
     * 
     * <P>Il percorso restituito utilizza il carattere {@code "/"} come
     * carattere separatore dei nomi nei percorsi.</P>
     *  
     * @return Percorso (relativo al direttorio di compilazione del progetto).
     * @see    it.scoppelletti.sitebuild.SiteProject#getBuildDirectory
     */    
    public String getBuildPath() {
        if (myBuildPath == null) {
            myBuildPath = initBuildPath();
        }
        
        return myBuildPath;        
    }
    
    /**
     * Inizializza il percorso del file compilato della pagina.
     * 
     * @return Percorso. 
     */
    private String initBuildPath() {
        String path;
        
        path = myFile.getPath();
        if (File.separatorChar != '/') {
            path = path.replace(File.separatorChar, '/');
        }
        
        return path;
    }
    
    /**
     * Restituisce il percorso relativo del direttorio di compilazione del
     * progetto rispetto alla pagina.
     * 
     * <P>Il percorso restituito utilizza il carattere {@code "/"} come
     * carattere separatore dei nomi nei percorsi.</P>
     * 
     * <H4>1. Esempio</H4>
     * 
     * <P>Se il percorso del file sorgente della pagina relativo al direttorio
     * dei file sorgente del progetto &egrave; {@code news/2007/art0320.htm},
     * il percorso restituito &egrave; {@code ../../../}.</P>
     * 
     * @return Percorso.
     * @see    #getSourceFile
     * @see    it.scoppelletti.sitebuild.SiteProject#getBuildDirectory
     */
    public String getBuildRootPath() {
        if (myRootPath == null) {
            myRootPath = initBuildRootPath();
        }
        
        return myRootPath;
    }
    
    /**
     * Inizializza il percorso relativo del direttorio di compilazione del
     * progetto rispetto alla pagina.
     * 
     * @return Percorso.
     */
    private String initBuildRootPath() {
        int i, n, p;
        String path;
        StringBuilder buf = new StringBuilder();
        
        path = myFile.getPath();
        i = 0;
        n = path.length();
        while (i < n) {
            p = path.indexOf(File.separatorChar, i);
            if (p < 0) {
                break;
            }
        
            buf.append("../");
            i = p + 1;
        }
     
        return buf.toString();
    }
    
    /**
     * Restituisce la pagina parent.
     * 
     * @return Oggetto. Se la pagina non ha una pagina parent, restituisce
     *         {@code null}.
     */
    public SitePage getParentPage() {        
        return myParent; 
    }
    
    /**
     * Restituisce la pagina successiva.
     * 
     * @return Oggetto. Se la pagina non ha una pagina successiva, restituisce
     *         {@code null}.
     */
    public SitePage getNextPage() {
        return myNext;
    }
    
    /**
     * Imposta la pagina successiva.
     * 
     * @param page Oggetto.
     */
    void setNextPage(SitePage page) {
        if (page == null) {
            throw new ArgumentNullException("page");
        }
        
        myNext = page;
    }
    
    /**
     * Restituisce la pagina precedente.
     * 
     * @return Oggetto. Se la pagina non ha una pagina precedente, restituisce
     *         {@code null}.
     */
    public SitePage getPreviousPage() {
        return myPrev;
    }
    
    /**
     * Imposta la pagina precedente.
     * 
     * @param page Oggetto.
     */
    void setPreviousPage(SitePage page) {
        if (page == null) {
            throw new ArgumentNullException("page");
        }
        
        myPrev = page;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        File file;
        
        file = new File(getProject().getSourceDirectory(), myFile.getPath());
        
        return String.format("%1$s(%2$s)", getClass().getName(), file);
    }
        
    /**
     * Restituisce il testo di una macro per la pagina.
     * 
     * @return Testo.
     */
    public String getMacroAsString(String key) {
        String in;
        Reader reader = null;
        Writer writer = null;
        SitePageBuilder builder;
        
        if (StringUtils.isBlank(key)) {
            throw new ArgumentNullException("key");
        }
        
        in = getProject().getMacroStart().concat(key).concat(
                getProject().getMacroEnd());        
        writer = new StringWriter();
        try {
            reader = new StringReader(in);                        
            builder = new SitePageBuilder(this, reader, writer);
            builder.run();
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } finally {
            reader = IOTools.close(reader);
            IOTools.close(writer);
        }
        
        return writer.toString();
    }
        
    /**
     * Ricompila la pagina.
     */
    void rebuild() {
        File sourceFile, targetFile, targetDir;
        Reader reader = null;
        OutputStream out = null;
        Writer writer = null;
        SitePageBuilder pageBuilder;
        
        myLogger.trace("Start rebuilding page {}.", this);
        try {
            sourceFile = new File(getProject().getSourceDirectory(),
                    myFile.getPath());
            targetFile = new File(getProject().getBuildDirectory(),
                    myFile.getPath());
        
            targetDir = targetFile.getParentFile();
            if (!targetDir.exists()) {
                if (!targetDir.mkdirs()) {
                    throw new FileCreateException(targetDir.toString());
                }
            }
        
            try {
                reader = new FileReader(sourceFile);
                out = new FileOutputStream(targetFile);
                writer = new OutputStreamWriter(out,
                        getProject().getOutputEncoding());
                out = null;
                writer = new BufferedWriter(writer);
                pageBuilder = new SitePageBuilder(this, reader, writer);
                pageBuilder.run();
            } catch (IOException ex) {
                throw new IOOperationException(ex);
            } finally {
                reader = IOTools.close(reader);
                out = IOTools.close(out);
                writer = IOTools.close(writer);
            }
        } finally {
            myLogger.trace("End rebuilding page {}.", this);
        }
        
        rebuildSections();
    }
}

/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import java.nio.charset.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Progetto di un sito.
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/sitebuild/project1.xsd}
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/project.html"
 *        TARGET="_top">Progetto</A> 
 * @since 1.0.0
 */
public final class SiteProject extends SiteSectionList {
    private static final Logger myLogger = LoggerFactory.getLogger(
            SiteProject.class);                
    private File myDir;
    private File mySourceDir;
    private File myBuildDir;     
    private String myMacroStart = "{{";
    private String myMacroEnd = "}}";
    private Charset myOutputEncoding = Charset.defaultCharset();
       
    /**
     * Costruttore.
     * 
     * @param dir Direttorio del progetto (percorso assoluto).
     */
    SiteProject(File dir) {
        if (dir == null) {
            throw new ArgumentNullException("dir");
        }
        
        myDir = dir;        
        mySourceDir = new File(dir, "src");
        myBuildDir = new File(dir, "build");
    }
    
    /**
     * Restituisce il direttorio del progetto.
     * 
     * @return Direttorio (percorso assoluto).
     */
    public File getProjectDirectory() {
        return myDir;
    }   
 
    /**
     * Restituisce il direttorio dei file sorgente del progetto.
     * 
     * @return Direttorio (percorso assoluto).
     */
    public File getSourceDirectory() {
        return mySourceDir;
    }
    
    /**
     * Imposta il direttorio dei file sorgente del progetto.
     * 
     * @param dir Direttorio (percorso assoluto).
     */
    void setSourceDirectory(File dir) {
        if (dir == null) {
            throw new ArgumentNullException("dir");            
        }
        
        mySourceDir = dir;
    }
    
    /**
     * Restituisce il direttorio di compilazione del progetto.
     * 
     * @return Direttorio (percorso assoluto).
     */
    public File getBuildDirectory() {        
        return myBuildDir;
    }
    
    /**
     * Imposta il direttorio di compilazione del progetto.
     * 
     * @param dir Direttorio (percorso assoluto).
     */
    void setBuildDirectory(File dir) {
        if (dir == null) {
            throw new ArgumentNullException("dir");            
        }
        
        myBuildDir = dir;
    }
    
    /**
     * Restituisce il tag di inizio del riferimento ad una macro.
     * 
     * @return Valore.
     */
    public String getMacroStart() {
        return myMacroStart;        
    }
    
    /**
     * Imposta il tag di inizio del riferimento ad una macro.
     * 
     * @param value Valore.
     */
    void setMacroStart(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
            
        myMacroStart = value;
    }
    
    /**
     * Restituisce il tag di fine del riferimento ad una macro.
     * 
     * @return Valore.
     */
    public String getMacroEnd() {
        return myMacroEnd;        
    }
    
    /**
     * Imposta il tag di fine del riferimento ad una macro.
     * 
     * @param value Valore.
     */
    void setMacroEnd(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
            
        myMacroEnd = value;
    }
    
    /**
     * Restituisce la codifica dei caratteri per la scrittura dei file generati.
     * 
     * @return Oggetto.
     */
    public Charset getOutputEncoding() {
        return myOutputEncoding;
    }
    
    /**
     * Imposta la codifica dei caratteri per la scrittura dei file generati.
     * 
     * @param charset Oggetto.
     */
    void setOutputEncoding(Charset charset) {
        if (charset == null) {
            throw new ArgumentNullException("cs");
        }
        
        myOutputEncoding = charset;     
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("%1$s(%2$s)", getClass().getName(), myDir);
    }
    
    /**
     * Ricompila il progetto.
     */
    public void rebuild() {
        File buildDir;        
        
        myLogger.trace("Start rebuilding project {}.", this);
        try {
            buildDir = getBuildDirectory();
            
            IOTools.deltree(buildDir);
            if (!buildDir.mkdirs()) {
                throw new FileCreateException(buildDir.toString());
            }
            
            rebuildSections();
        } finally {
            myLogger.trace("End rebuilding project {}.", this);            
        }
    }    
}

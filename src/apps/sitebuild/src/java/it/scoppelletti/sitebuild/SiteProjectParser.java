/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import java.nio.charset.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.xml.sax.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.xml.*;

/**
 * Parser di un progetto.
 */
final class SiteProjectParser extends XmlDefaultHandler {    
    private SiteProject myProject;
    private FilePath myProjectDir;
    private FilePath mySourceDir;
    private Deque<SiteElement> myElementStack;
    private Set<String> myPages;
    private MacroWrapper myCurrentMacro = null;
    private String myCurrentPropName = null;
    
    /**
     * Costruttore.
     * 
     * @param dir Direttorio del progetto.
     */
    SiteProjectParser(File dir) {
        if (dir == null) {
            throw new ArgumentNullException("dir");
        }
        
        myProjectDir = new FilePath(dir);
        myElementStack = new ArrayDeque<SiteElement>();
        myPages = new HashSet<String>();
        setEntityResolver(new SchemaEntityResolver("sitebuild/project", 1, 1));
    }
    
    /**
     * Restituisce il progetto.
     * 
     * @return Oggetto.
     */
    SiteProject getProject() {
        return myProject;
    }
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {
        if (localName.equals("project")) {
            startElementProject(attrs);          
        } else if (localName.equals("sections")) {
            startElementSectionList(attrs);                           
        } else if (localName.equals("section")) {
            startElementSection();                
        } else if (localName.equals("pages")) {
            startElementPageList(attrs);                      
        } else if (localName.equals("page")) {
            startElementPage(attrs);     
        } else if (localName.equals("macro")) {
            startElementMacro(attrs);                    
        } else if (localName.equals("inline")) {
            startElementInline(attrs);                  
        } else if (localName.equals("text")) {
            startElementText(attrs);
        } else if (localName.equals("file")) {
            startElementFile(attrs);            
        }
    }      
    
    /**
     * Tag di chiusura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws
            SAXException {        
        if (localName.equals("project")) {
            endElement();
        } else if (localName.equals("section")) {
            endElement();            
        } else if (localName.equals("page")) {
            endElement();               
        } else if (localName.equals("macro")) {
            endElementMacro();            
        } else if (localName.equals("text")) {
            endElementText();                                                    
        }
    }    
        
    /**
     * Tag di apertura dell&rsquo;elemento {@code <project>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementProject(Attributes attrs) throws SAXException {
        String path, value;
        File dir;
        FilePath buildDir;
        Charset charset;
        
        path = attrs.getValue("dir");
        dir = new File(path);
        if (!dir.isAbsolute()) {
            dir = new File(myProjectDir.toFile(), path);                    
        }                
        myProjectDir = new FilePath(dir);
        
        myProject = new SiteProject(myProjectDir.toFile());
        
        path = attrs.getValue("src");
        dir = new File(path);
        if (!dir.isAbsolute()) {
            dir = new File(myProjectDir.toFile(), path);                    
        }                
        mySourceDir = new FilePath(dir);
        myProject.setSourceDirectory(mySourceDir.toFile());
        
        path = attrs.getValue("build");
        dir = new File(path);
        if (!dir.isAbsolute()) {
            dir = new File(myProjectDir.toFile(), path);                    
        }                
        buildDir = new FilePath(dir);
        myProject.setBuildDirectory(buildDir.toFile());
        
        myProject.setMacroStart(attrs.getValue("macroStart"));
        myProject.setMacroEnd(attrs.getValue("macroEnd"));
        
        value = attrs.getValue("outputEncoding");
        if (!StringUtils.isBlank(value)) {
            try {
                charset = Charset.forName(value);
            } catch (Exception ex) {
                throw new SAXParseException(ApplicationException.toString(ex),
                        getDocumentLocator(), ex);
            }
            myProject.setOutputEncoding(charset);
        }
        
        myElementStack.push(myProject);                
    }
    
    /**
     * Tag di chiusura di un elemento.
     */
    private void endElement() {
        SiteElement el = myElementStack.pop();
        
        el.endInit();
    }  
       
    /**
     * Tag di apertura di un elemento {@code <sections>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementSectionList(Attributes attrs) {
        SiteSectionList sectList;
        
        sectList = (SiteSectionList) myElementStack.getFirst(); 
        sectList.setNumberingStyle(SiteNumberingStyle.parse(attrs.getValue(
                "numbering")));
    }
    
    /**
     * Tag di apertura di un elemento {@code <section>}.
     */
    private void startElementSection() {
        SiteSection sect;
        SiteSectionList sectList;
        
        sectList = (SiteSectionList) myElementStack.getFirst(); 
        sect = new SiteSection(myProject, sectList);
        myElementStack.push(sect);
    }
       
    /**
     * Tag di apertura di un elemento {@code <pages>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementPageList(Attributes attrs) {
        SiteSection sectList;
        
        sectList = (SiteSection) myElementStack.getFirst(); 
        sectList.setNumberingStyle(SiteNumberingStyle.parse(attrs.getValue(
                "numbering")));
    }
    
    /**
     * Tag di apertura di un elemento {@code <page>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementPage(Attributes attrs) throws SAXException {
        String value;
        File file;
        FilePath path;
        SitePage page;
        SiteSection sect;
        IOResources ioRes = new IOResources();
        CommonResources commonRes = new CommonResources();
        
        value = attrs.getValue("path");
        path = new FilePath(new File(mySourceDir.toFile(), value));            
        file = path.toRelative(mySourceDir);
        if (file == null) {
            throw new SAXParseException(ioRes.getFileNotInDirectoryException(
                    value, mySourceDir.toString()), getDocumentLocator());
        }
        
        if (!myPages.add(file.getPath())) {
            throw new SAXParseException(commonRes.getObjectDuplicateException(
                    file.getPath()), getDocumentLocator());
        }
        
        sect = (SiteSection) myElementStack.getFirst();
        page = new SitePage(myProject, sect, file);
        myElementStack.push(page);
    }
        
    /**
     * Tag di apertura di un elemento {@code <macro>}.
     * 
     * @param attrs Attributi.
     */        
    private void startElementMacro(Attributes attrs) throws SAXException {
        try {
            myCurrentMacro = new MacroWrapper(attrs.getValue("key"),
                    attrs.getValue("class"));
        } catch (Exception ex) {
            throw new SAXParseException(ApplicationException.toString(ex),
                    getDocumentLocator(), ex);            
        }
    }    
    
    /**
     * Tag di chiusura di un elemento {@code <macro>}.
     */        
    private void endElementMacro() throws SAXException {
        SiteElement el = myElementStack.getFirst();
        
        try {
            el.putMacro(myCurrentMacro);
        } catch (Exception ex) {
            throw new SAXParseException(ApplicationException.toString(ex),
                    getDocumentLocator(), ex);            
        }        
        
        myCurrentMacro = null;
    }
    
    /**
     * Tag di apertura di un elemento {@code <inline>}.
     * 
     * @param attrs Attributi.
     */        
    private void startElementInline(Attributes attrs) throws SAXException {
        try {
            myCurrentMacro.putPropertyValue(attrs.getValue("name"),
                    attrs.getValue("value"));            
        } catch (Exception ex) {
            throw new SAXParseException(ApplicationException.toString(ex),
                    getDocumentLocator(), ex);            
        }               
    }  
    
    /**
     * Tag di apertura di un elemento {@code <text>}.
     * 
     * @param attrs Attributi.
     */        
    private void startElementText(Attributes attrs) throws SAXException {
        myCurrentPropName = attrs.getValue("name");
        collectContent();
    }
    
    /**
     * Tag di chiusura di un elemento {@code <text>}.
     */        
    private void endElementText() throws SAXException {
        try {
            myCurrentMacro.putPropertyValue(myCurrentPropName,
                    getCollectedContent());
        } catch (Exception ex) {
            throw new SAXParseException(ApplicationException.toString(ex),
                    getDocumentLocator(), ex);            
        }
        
        myCurrentPropName = null;
    }  
    
    /**
     * Tag di apertura di un elemento {@code <file>}.
     * 
     * @param attrs Attributi.
     */        
    private void startElementFile(Attributes attrs) throws SAXException {
        String line, path, text;
        File file;
        Reader in = null;
        BufferedReader reader = null;
        StringWriter out = null;
        BufferedWriter writer = null;
        
        path = attrs.getValue("path");
        file = new File(path);
        if (!file.isAbsolute()) {
            file = new File(myProjectDir.toFile(), path);
        }

        text = "";
        try {
            in = new FileReader(file);
            reader = new BufferedReader(in);
            in = null;
            
            out = new StringWriter();
            writer = new BufferedWriter(out);
            
            line = reader.readLine();
            while (line != null) {
                writer.write(line);
                writer.newLine();                
                line = reader.readLine();
            }
            writer.flush();
            text = out.toString();
            out = null;
        } catch (IOException ex) {
            throw new SAXParseException(ApplicationException.toString(ex),
                    getDocumentLocator(), ex);             
        } finally {
            in = IOTools.close(in);
            reader = IOTools.close(reader);
            writer = IOTools.close(writer);
        }
        
        try {
            myCurrentMacro.putPropertyValue(attrs.getValue("name"), text);            
        } catch (Exception ex) {
            throw new SAXParseException(ApplicationException.toString(ex),
                    getDocumentLocator(), ex);            
        }            
    }    
}

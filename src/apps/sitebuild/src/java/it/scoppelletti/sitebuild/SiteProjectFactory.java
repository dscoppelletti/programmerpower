/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.xml.*;

/**
 * Classe di factory dei progetti dei siti.
 * 
 * @see   it.scoppelletti.sitebuild.SiteProject
 * @since 1.0.0
 */
public final class SiteProjectFactory  {
    private ErrorHandler myErrorHandler = null;
    
    /**
     * Costruttore. 
     */
    public SiteProjectFactory() {        
    }
     
    /**
     * Restituisce il gestore degli errori SAX.
     * 
     * @return Gestore. Pu&ograve; essere {@code null}.
     * @see    #setErrorHandler
     */
    public ErrorHandler getErrorHandler() {
        return myErrorHandler;
    }
    
    /**
     * Imposta il gestore degli errori SAX.
     * 
     * <P>Se non viene impostato un gestore degli errori (o viene impostato
     * {@code null}), viene utilizzato un gestore predefinito.</P>
     *  
     * @param handler Gestore. Pu&ograve; essere {@code null}.
     * @see           #getErrorHandler
     * @see           it.scoppelletti.programmerpower.xml.XmlDefaultHandler
     */
    public void setErrorHandler(ErrorHandler handler) {
        myErrorHandler = handler;
    }
    
    /**
     * Legge un progetto.
     * 
     * @param  file File.
     * @return      Oggetto.
     */
    public SiteProject loadProject(File file) {
        File dir;
        SAXParser saxParser;
        SiteProjectParser handler;
        
        if (file == null) {
            throw new ArgumentNullException("file");
        }
    
        dir = file.getParentFile();
        if (dir == null) {
            // Dal nome del file di progetto non e' determinabile il direttorio
            // del progetto:
            // Utilizzo il direttorio corrente.
            dir = new File(".");
        }             
        
        saxParser = XmlTools.newSAXParser();
        handler = new SiteProjectParser(dir);
        handler.setErrorHandler(myErrorHandler);
        try {
            saxParser.parse(file, handler);
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } catch (SAXException ex) {
            throw new XmlException(ex);            
        }        
                
        return handler.getProject();     
    }        
}

/*
 * Copyright (C) 2009-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import it.scoppelletti.programmerpower.*;

/**
 * Macro sostituita con un testo oppure con un altro a seconda che due operandi
 * di tipo stringa siano uguali oppure no.
 * 
 * @see   <A
 *        HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/macroequal.html"
 *        TARGET="_top">Macro <CODE>MacroEqual</CODE></A> 
 * @since 1.0.0
 */
public class MacroEqual extends AbstractSiteMacro {
    private String myOp1 = "";
    private String myOp2 = "";
    private String myTrueText = "";
    private String myFalseText = "";
    
    /**
     * Costruttore.
     */
    public MacroEqual() {        
    }
    
    /**
     * Restituisce il primo operando.
     * 
     * @return Valore.
     * @see    #setOperand1
     */
    public String getOperand1() {
        return myOp1;
    }
    
    /**
     * Imposta il primo operando.
     * 
     * @param value Valore.
     * @see         #getOperand1
     */
    public void setOperand1(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myOp1 = value;
    }    
    
    /**
     * Restituisce il secondo operando.
     * 
     * @return Valore.
     * @see    #setOperand2
     */
    public String getOperand2() {
        return myOp2;
    }
    
    /**
     * Imposta il secondo operando.
     * 
     * @param value Valore.
     * @see         #getOperand2
     */
    public void setOperand2(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myOp2 = value;
    }    
    
    /**
     * Restituisce il testo della macro nel caso di uguaglianza verificata tra i
     * due operandi.
     * 
     * @return Valore.
     * @see    #setTrueText
     * @see    #getOperand1
     * @see    #getOperand2
     */
    public String getTrueText() {
        return myTrueText;
    }
    
    /**
     * Imposta il testo della macro nel caso di uguaglianza verificata tra i
     * due operandi.
     * 
     * @param value Valore.
     * @see         #getTrueText
     * @see         #getOperand1
     * @see         #getOperand2 
     */
    public void setTrueText(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myTrueText = value;
    }    
    
    /**
     * Restituisce il testo della macro nel caso di uguaglianza non verificata
     * tra i due operandi.
     * 
     * @return Valore.
     * @see    #setFalseText
     * @see    #getOperand1
     * @see    #getOperand2 
     */
    public String getFalseText() {
        return myFalseText;
    }
    
    /**
     * Imposta il testo della macro nel caso di uguaglianza non verificata tra i
     * due operandi.
     * 
     * @param value Valore.
     * @see         #getFalseText
     * @see         #getOperand1
     * @see         #getOperand2 
     */
    public void setFalseText(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myFalseText = value;
    }    
    
    @Override
    protected Reader openReader() {
        String text;
        
        text = (myOp1.equals(myOp2)) ? myTrueText : myFalseText;
        return new StringReader(text);
    }    
}

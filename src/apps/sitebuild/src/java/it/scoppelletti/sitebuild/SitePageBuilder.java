/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import org.slf4j.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Compilatore di una pagina.
 */
final class SitePageBuilder {    
    private static final int BUFFER_SIZE = 128;
    private static final Logger myLogger = LoggerFactory.getLogger(
            SitePageBuilder.class);            
    private final SitePage myPage; 
    private final String myMacroStart;
    private final String myMacroEnd;    
    private final Reader myReader;
    private final Writer myWriter;    
    
    /**
     * Buffer di lettura.
     */
    private char[] myBuf = new char[SitePageBuilder.BUFFER_SIZE];
    
    /**
     * Dimensione del buffer di lettura.
     */
    private int myBufSize = SitePageBuilder.BUFFER_SIZE;
    
    /**
     * Numero di caratteri letti nel buffer {@code myBuf}.
     */
    private int myBufLen;
    
    /**
     * Indice di lettura del buffer {@code myBuf}.
     */
    private int myBufIdx;
    
    /**
     * Indice sul buffer {@code myBuf} del primo carattere da scrivere.
     */
    private int myWritingStart = 0;
    
    /**
     * Indice sul buffer {@code myBuf} dell&rsquo;ultimo carattere da scrivere.
     */
    private int myWritingEnd = -1;

    /**
     * Chiave della macro in corso di riconoscimento.
     */
    private StringBuilder myMacroKey = null;
    
    /**
     * Numero di caratteri consecutivi riconosciuti come facenti parte del tag
     * di inizio di una macro.
     */
    private int myMacroStartCount = 0;
        
    /**
     * Numero di caratteri consecutivi riconosciuti come facenti parte del tag
     * di fine di una macro.
     */
    private int myMacroEndCount = 0;
    
    /**
     * Costruttore.
     * 
     * @param page   Pagina.
     * @param reader Flusso di lettura.
     * @param writer Flusso di scrittura.
     */
    SitePageBuilder(SitePage page, Reader reader, Writer writer) {
        if (page == null) {
            throw new ArgumentNullException("page");
        }        
        if (reader == null) {
            throw new ArgumentNullException("reader");
        }
        if (writer == null) {
            throw new ArgumentNullException("writer");
        }
        
        myPage = page;
        myMacroStart = myPage.getProject().getMacroStart();
        myMacroEnd = myPage.getProject().getMacroEnd();
        myReader = reader;
        myWriter = writer;
    }
    
    /**
     * Esegue la compilazione.
     */
    void run() throws IOException {
        readBuffer();
        while (myBufLen > 0) {            
            while (myBufIdx < myBufLen) {
                visitChar();
            }
            
            readBuffer();
        }
        
        myBuf = null;
        myMacroKey = null;
    }
    
    /**
     * Legge sul buffer.
     */
    private void readBuffer() throws IOException {
        int offset, read;
        char[] newBuf;
        
        if (myMacroStartCount == 0 && myMacroKey == null) {
            // Scrivo il buffer            
            writeBuffer();
            offset = 0;
            if (myBufSize > SitePageBuilder.BUFFER_SIZE) {
                // Riporto la dimensione del buffer a quella minima
                myBufSize = SitePageBuilder.BUFFER_SIZE;
                myBuf = new char[myBufSize];
            }
        } else {
            // Ho esaurito il buffer prima di rilevare il tag finale di una
            // possibile macro
            if (myWritingStart == 0) {
                // Il possibile riferimento alla macro occupa gia' tutto il
                // buffer allocato:
                // Incremento la dimensione del buffer.
                offset = myBufSize;
                myBufSize += SitePageBuilder.BUFFER_SIZE;
                newBuf = new char[myBufSize];
                System.arraycopy(myBuf, 0, newBuf, 0, offset);
                myBuf = newBuf;
            } else {
                // Traslo il buffer in modo da mantenere i soli caratteri non
                // ancora scritti ma che dovro' scrivere nel caso il riferimento
                // alla macro non risulti valido
                offset = myBufLen - myWritingStart;
                System.arraycopy(myBuf, myWritingStart, myBuf, 0, offset);
            }
        } 
        
        read = myReader.read(myBuf, offset, myBufSize - offset);
        if (read <= 0 && offset > 0) {
            // Il flusso in input e' terminato ma ho ancora parte del buffer
            // da scrivere perche' poteva includere una possibile macro:
            // Scrivo il buffer perche' ovviamente ormai non rilevero' piu' il
            // tag di chiusura della macro.
            myWritingStart = 0;
            myWritingEnd = offset - 1;
            writeBuffer();
            myBufLen = -1;
            return;
        }
        
        myBufLen = offset + read;        
        myBufIdx = offset;
        myWritingStart = 0;
        myWritingEnd = myBufLen - 1;        
    }
    
    /**
     * Visita il carattere corrente del buffer.
     */
    private void visitChar() throws IOException {
        char c = myBuf[myBufIdx];
        
        if (myMacroKey == null) {
            // Rilevazione del tag iniziale di una possibile macro
            if (c == myMacroStart.charAt(myMacroStartCount)) { 
                myMacroStartCount++;
            } else {
                myMacroStartCount = 0;                    
            }
            if (myMacroStartCount == myMacroStart.length()) {
                // Ho riconosciuto il tag iniziale di una possibile macro:
                // Scrivo i caratteri precedenti.
                myWritingEnd = myBufIdx - myMacroStart.length();
                writeBuffer();
                myMacroKey = new StringBuilder();
                myMacroStartCount = 0;
                myMacroEndCount = 0;
            }
        } else {
            // Rilevazione del tag finale di una possibile macro
            if (c == myMacroEnd.charAt(myMacroEndCount)) { 
                myMacroEndCount++;
            } else {
                myMacroEndCount = 0;                    
            }
            if (myMacroEndCount == myMacroEnd.length()) {
                // Ho riconosciuto il tag finale della macro
                writeMacro();
                myMacroEndCount = 0;
                return;
            }                
            
            if (c == '\n' || c == '\r') {
                // Il riferimento ad una macro non puo' essere spezzato su piu'
                // righe
                abortMacro();
                return;
            }
            
            // Il carattere potrebbe far parte della chiave della macro
            myMacroKey.append(c);
        }      
        
        myBufIdx++;
    }
    
    /**
     * Scrive il buffer.
     */
    private void writeBuffer() throws IOException {
        if (myWritingEnd >= myWritingStart) {
            myWriter.write(myBuf, myWritingStart, myWritingEnd -
                    myWritingStart + 1);       
        }
        
        myWritingStart = myWritingEnd + 1;
        myWritingEnd = myBufLen - 1;
    }
    
    /**
     * Scrive la macro rilevata.
     */
    private void writeMacro() throws IOException {
        MacroWrapper macro;
        String key;
        Reader reader = null;
        SitePageBuilder macroBuilder;        
        
        // Nella chiave avevo accodato anche i primi caratteri del tag finale
        // della macro
        myMacroKey.setLength(myMacroKey.length() - myMacroEnd.length() + 1);
        
        if (myMacroKey.length() == 0) {
            key = "";
            macro = null;            
        } else {        
            key = myMacroKey.toString();            
            macro = myPage.getMacro(key);
        }        
        if (macro == null) {
            myLogger.warn("Macro {} not defined in page {}", key, myPage);
            abortMacro();
            return;
        }

        MacroStack.getInstance().push(myPage, key);
        try {
            reader = macro.openReader(myPage);
            macroBuilder = new SitePageBuilder(myPage, reader, myWriter);
            macroBuilder.run();
        } finally {
            MacroStack.getInstance().pop();
            reader = IOTools.close(reader);
        }
        
        // Riprendo a visitare il buffer dal carattere successivo al tag finale
        // della macro        
        myBufIdx++;
        myWritingStart = myBufIdx;
        myWritingEnd = myBufLen - 1;
        myMacroKey = null;
    }    
    
    /**
     * Annulla la rilevazione di una macro.
     */
    private void abortMacro() {
        myMacroKey = null;
        myMacroEndCount = 0;
        
        // La scrittura riprende dal carattere nel quale era stato rilevato il
        // tag iniziale della macro, ma la visita riprende dal carattere
        // successivo (altrimenti rileverei di nuovo il possibile tag iniziale
        // di una macro).        
        myBufIdx = myWritingStart + 1;        
    }
}

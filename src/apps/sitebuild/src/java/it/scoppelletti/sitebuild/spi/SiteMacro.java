/*
 * Copyright (C) 2009 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import it.scoppelletti.sitebuild.*;

/**
 * Interfaccia di una macro.
 * 
 * @see   it.scoppelletti.sitebuild.spi.AbstractSiteMacro
 * @since 1.0.0
 */
public interface SiteMacro {
   
    /**
     * Apre il flusso di lettura del testo della macro.
     * 
     * @param macroKey Chiave che indentifica la macro.
     * @param page     Pagina nella quale deve essere inserito il testo della
     *                 macro.
     * @return         Flusso di lettura.
     */
    Reader openReader(String macroKey, SitePage page);    
}

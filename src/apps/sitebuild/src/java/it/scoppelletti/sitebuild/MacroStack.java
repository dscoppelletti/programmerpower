/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Stack delle macro in corso di scrittura.
 */
final class MacroStack {
    private static final MacroStack myInstance = new MacroStack();
    private Deque<MacroStack.Entry> myStack;

    /**
     * Costruttore privato per classe singleton.
     */
    private MacroStack() {
        myStack = new ArrayDeque<MacroStack.Entry>();
    }
    
    /**
     * Restituisce l&rsquo;istanza dello stack.
     * 
     * @return Oggetto.
     */
    static MacroStack getInstance() {
        return myInstance;
    }

    /**
     * Inserisce nello stack una macro da espandere in una pagina.
     * 
     * @param page     Pagina.
     * @param macroKey Chiave della macro.
     */
    void push(SitePage page, String macroKey) throws IOException {        
        MacroStack.Entry entry;
        ProgramResources res = new ProgramResources();
       
        if (page == null) {
            throw new ArgumentNullException("page");
        }
        if (StringUtils.isBlank(macroKey)) {
            throw new ArgumentNullException("macroKey");
        }
        
        entry = new MacroStack.Entry(page, macroKey);

        if (myStack.contains(entry)) {
            throw new IOException(res.getMacroCircularReferenceException(
                    macroKey, page.toString()));
        }      
        
        myStack.push(entry);
    }

    /**
     * Rimuove dallo stack l&rsquo;ultima macro inserita.
     */
    void pop() {
        myStack.pop();
    }
    
    /**
     * Elemento dello stack.
     */
    private static final class Entry {
        private final File myPage;
        private final String myMacroKey;
        
        /**
         * Costruttore.
         * 
         * @param page     Pagina.
         * @param macroKey Chiave della macro. 
         */
        Entry(SitePage page, String macroKey) {
            myPage = new File(page.getProject().getSourceDirectory(),
                    page.getSourceFile().getPath());
            myMacroKey = macroKey;
        }
        
        /**
         * Restituisce il file sorgente della pagina.
         * 
         * @return File (percorso assoluto).
         */
        File getPage() {
            return myPage;
        }
        
        /**
         * Restituisce la chiave della macro.
         * 
         * @return Valore.
         */
        String getMacroKey() {
            return myMacroKey;
        }
        
        /**
         * Rappresenta l&rsquo;oggetto con una stringa.
         * 
         * @return Stringa.
         */
        @Override
        public String toString() {
            return String.format("(%1$s, %2$s)", myPage, myMacroKey);
        }  
        
        /**
         * Verifica l&rsquo;uguaglianza con un altro oggetto.
         * 
         * @param  obj Oggetto di confronto.
         * @return     Esito della verifica. 
         */
        @Override
        public boolean equals(Object obj) {
            MacroStack.Entry op;
            
            if (!(obj instanceof MacroStack.Entry)) {
                return false;
            }
            
            op = (MacroStack.Entry) obj;
            
            if (!myPage.equals(op.getPage()) ||
                    !myMacroKey.equals(op.getMacroKey())) {
                return false;
            }
            
            return true;
        }
        
        /**
         * Calcola il codice hash dell&rsquo;oggetto.
         * 
         * @return Valore.
         */
        @Override
        public int hashCode() {
            int value = 17;
            
            value = 37 * value + myPage.hashCode();
            value = 37 * value + myMacroKey.hashCode();
            
            return value;
        }            
    }
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.util.*;
import java.io.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.sitebuild.*;

/**
 * Macro di base per la composizione della struttura del numero di un elemento
 * all&rsquo;interno della gerarchia del sito.
 * 
 * @see   it.scoppelletti.sitebuild.SiteElement#getNumber
 * @see   it.scoppelletti.sitebuild.SiteElement#getNumberingStyle
 * @since 1.1.0
 */
public abstract class MacroElementNumber extends AbstractSiteMacro {
    private int myLevelStart = 0;
    private int myLevelMax = Integer.MAX_VALUE;
    private String mySeparator = ".";
    private boolean mySuppressSingleSections = false;
    
    /**
     * Costruttore.
     */
    protected MacroElementNumber() {
    }
    
    /**
     * Restituisce il livello nella struttura gerarchica del sito rispetto al
     * quale deve essere composta la struttura del numero dell&rsquo;elemento. 
     * 
     * @return Valore.
     * @see    #setLevelStart
     * @see    it.scoppelletti.sitebuild.SiteElement#getParentElement
     */
    public int getLevelStart() {
        return myLevelStart;
    }
    
    /**
     * Imposta il livello nella struttura gerarchica del sito rispetto al quale
     * deve essere composta la struttura del numero dell&rsquo;elemento.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code 0}
     * @see   #getLevelStart
     * @see   it.scoppelletti.sitebuild.SiteElement#getParentElement
     */
    public void setLevelStart(int value) {
        if (value < 0) {
            throw new ArgumentOutOfRangeException("value", value, "[0, ...]");
        }
        
        myLevelStart = value;
    }
    
    /**
     * Restituisce il livello massimo della struttura gerarchica del
     * numero dell&rsquo;elemento. 
     * 
     * @return Valore.
     * @see    #setLevelMax
     * @see    it.scoppelletti.sitebuild.SiteElement#getParentElement
     */
    public int getLevelMax() {
        return myLevelMax;
    }
    
    /**
     * Imposta il livello massimo della struttura del numero
     * dell&rsquo;elemento.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code Integer.MAX_VALUE}
     * @see   #getLevelMax
     * @see   it.scoppelletti.sitebuild.SiteElement#getParentElement
     */
    public void setLevelMax(int value) {
        if (value < 1) {
            throw new ArgumentOutOfRangeException("value", value, "[1, ...]");
        }
        
        myLevelMax = value;
    }
    
    /**
     * Restituisce il separatore da inserire tra i numeri degli elementi della
     * gerarchia che compongono il numero dell&rsquo;elemento.
     * 
     * @return Valore.
     * @see    #setSeparator
     */
    public String getSeparator() {
        return mySeparator;
    }
    
    /**
     * Imposta il separatore da inserire tra i numeri degli elementi della
     * gerarchia che compongono il numero dell&rsquo;elemento.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code "."}
     * @see                               #getSeparator
     */
    public void setSeparator(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        mySeparator = value;
    }
                
    /**
     * Indica se devono essere soppresse dalla struttura del numero le sezioni
     * incluse in una <I>famiglia di sezioni</I> allo stesso livello gerarchico
     * costituita da una sola sezione.
     * 
     * @return Indicatore.
     * @see    #setSuppressSingleSections
     */
    public boolean isSuppressSingleSections() {
        return mySuppressSingleSections;
    }
    
    /**
     * Imposta l&rsquo;indicatore di soppressione dalla struttura del numero
     * delle sezioni incluse in una famiglia di sezioni allo stesso livello
     * gerarchico costituita da una sola sezione.
     * 
     * @param                       value Indicatore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #isSuppressSingleSections
     */
    public void setSuppressSingleSections(boolean value) {
        mySuppressSingleSections = value;
    }
    
    protected abstract SiteElement getCurrentElement();
    
    @Override
    protected Reader openReader() {
        int level;
        StringBuilder buf = new StringBuilder();
        SiteElement nodeEl;
        SiteSectionList sectList;
        Deque<SiteElement> elStack = new ArrayDeque<SiteElement>();
        
        nodeEl = getCurrentElement();
        elStack.push(nodeEl);
        nodeEl = nodeEl.getParentElement();
        while (nodeEl != null) {
            if (nodeEl instanceof SiteProject) {
                break;
            }
            
            if (mySuppressSingleSections &&
                    nodeEl instanceof SiteSection) {
                sectList = (SiteSectionList) nodeEl.getParentElement();
                if (sectList.getSections().size() == 1) {
                    nodeEl = nodeEl.getParentElement();
                    continue;
                }
            }
            
            elStack.push(nodeEl);
            nodeEl = nodeEl.getParentElement();
        }        
        
        nodeEl = elStack.poll();
        level = 0;
        while (nodeEl != null && level < myLevelStart) {
            level++;
            nodeEl = elStack.poll();
        }
        
        level = 0;
        while (nodeEl != null && level < myLevelMax) {
            level++;
            if (buf.length() > 0) {
                buf.append(mySeparator);
            }
                        
            buf.append(nodeEl.getNumber());                        
            nodeEl = elStack.poll();
        }
        
        return new StringReader(buf.toString());
    }        
}

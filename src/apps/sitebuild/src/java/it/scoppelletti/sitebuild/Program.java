/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import java.util.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.spring.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
@ApplicationAbout.Annotation(
        copyright = "Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>",
        licenseResource = "it/scoppelletti/programmerpower/resources/asl2.txt",
        pageUrl = "http://www.scoppelletti.it/programmerpower/sitebuild")
public final class Program extends ConsoleApplicationRunner {
    private static final String RESOURCE_BASENAME =
        "it.scoppelletti.sitebuild.ProgramOptionSet";
    private static final String OPTION_PROJECT = "project";

    /**
     * Costruttore.
     */
    private Program() {
    }
    
    @Override
    protected void initOptions(List<CliOption> list) { 
        CliOption option;
               
        option = new CliOptionFile(Program.OPTION_PROJECT,
                Program.RESOURCE_BASENAME);
        option.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        list.add(option);
        
        super.initOptions(list);
    }    
    
    /**
     * File di progetto.
     * 
     * @return Opzione.
     * @since  1.1.1
     */
    public CliOptionFile getProjectFile() {
        return (CliOptionFile) getOption(Program.OPTION_PROJECT);
    }
    
    /**
     * Entry-point.
     * 
     * @param args Argomenti sulla linea di comando.
     * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/cli.html"
     *        TARGET="_top">Linea di comando</A>
     */       
    public static void main(String args[]) {
        Program appl = new Program();

        appl.run(args);
    }               
}

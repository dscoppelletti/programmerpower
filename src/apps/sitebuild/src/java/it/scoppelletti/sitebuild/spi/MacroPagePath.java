/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.text.*;
import java.util.*;
import java.io.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.sitebuild.*;

/**
 * Macro sostituita con un frammento di codice
 * <ACRONYM TITLE="HyperText Markup Language">HTML</ACRONYM> che pu&ograve;
 * riportare il riferimento alle pagine che costituiscono il <DFN>percorso</DFN>
 * (nell&rsquo;ambito della struttura gerarchica del sito) della pagina nella
 * quale deve essere inserito il testo della macro (tipicamente a scopo di
 * navigazione).
 * 
 * @see it.scoppelletti.sitebuild.SitePage#getParentPage
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/pagepath.html"
 *      TARGET="_top">Macro <CODE>MacroPagePath</CODE></A>
 * @since 1.0.0
 */
public class MacroPagePath extends AbstractSiteMacro {
    private String myMacroTitle;
    private String myNodeFmt = "";
    private String mySeparator = "";
    private String myEmpty = "";
    
    /**
     * Costruttore.
     */
    public MacroPagePath() {
    }
    
    /**
     * Restituisce la chiave che identifica la macro corrispondente al titolo
     * delle pagine.
     * 
     * @return Valore.
     * @see    #setMacroTitle
     */
    public String getMacroTitle() {
        return myMacroTitle;
    }
    
    /**
     * Imposta la chiave che identifica la macro corrispondente al titolo delle
     * pagine.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     * @see                                #getMacroTitle
     */
    public void setMacroTitle(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myMacroTitle = value;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito per ogni pagina del
     * percorso.
     * 
     * @return Formato.
     * @see    #setNodeFormat
     */
    public String getNodeFormat() {
        return myNodeFmt;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito per ogni pagina del
     * percorso.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getNodeFormat
     */
    public void setNodeFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        myNodeFmt = format;
    }
    
    /**
     * Restituisce il codice HTML utilizzato come separatore tra le pagine del
     * percorso.
     * 
     * @return Valore.
     * @see    #setSeparator
     */
    public String getSeparator() {
        return mySeparator;
    }
    
    /**
     * Imposta il codice HTML utilizzato come separatore tra le pagine del
     * percorso.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Stringa vuota {@code ""}.
     * @see                               #getSeparator
     */
    public void setSeparator(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        mySeparator = value;
    }
    
    /**
     * Restituisce il codice HTML sostituito alla macro nelle pagine per le
     * quali il percorso non &egrave; determinato.
     * 
     * @return Valore.
     * @see    #setEmptyPath
     */
    public String getEmptyPath() {
        return myEmpty;
    }
    
    /**
     * Imposta il codice HTML sostituito alla macro nelle pagine per le quali il
     * percorso non &egrave; determinato.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Stringa vuota {@code ""}.
     * @see                               #getEmptyPath
     */
    public void setEmptyPath(String value) {
        if (value == null) {
            throw new ArgumentNullException("value");
        }
        
        myEmpty = value;
    }
    
    @Override
    protected Reader openReader() {
        int level;
        MessageFormat nodeFmt;
        SitePage nodePage;
        Object[] args;
        StringBuilder buf = new StringBuilder();
        Deque<SitePage> pageStack = new ArrayDeque<SitePage>();
        
        nodePage = getPage().getParentPage();
        while (nodePage != null) {
            pageStack.push(nodePage);
            nodePage = nodePage.getParentPage();
        }
                
        if (pageStack.isEmpty()) {
            return new StringReader(myEmpty);
        }
        
        if (StringUtils.isBlank(myMacroTitle)) {
            throw new PropertyNotSetException(toString(), "macroTitle");
        }
        
        nodeFmt = new MessageFormat(myNodeFmt);
        args = new Object[3];
        
        level = 0;
        nodePage = pageStack.poll();
        while (nodePage != null) {
            if (level > 0) {
                buf.append(mySeparator);
            }
            
            args[0] = new Integer(level);
            args[1] = getPage().getBuildRootPath().concat(
                    nodePage.getBuildPath());
            args[2] = nodePage.getMacroAsString(myMacroTitle);
            buf.append(nodeFmt.format(args));
                        
            nodePage = pageStack.poll();
            level++;
        }
        
        return new StringReader(buf.toString());
    }
}

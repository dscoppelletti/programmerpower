/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.text.*;
import java.io.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.sitebuild.*;

/**
 * Macro sostituita con un frammento di codice
 * <ACRONYM TITLE="HyperText Markup Language">HTML</ACRONYM> che pu&ograve;
 * rappresentare il sommario delle pagine del sito sottostanti nella gerarchia
 * alla pagina nella quale deve essere inserito il testo della macro.
 * 
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/macrotoc.html"
 *      TARGET="_top">Macro <CODE>MacroTableOfContent</CODE></A>
 * @since 1.1.0
 */
public class MacroTableOfContents extends AbstractSiteMacro {
    private int myLevelMax = Integer.MAX_VALUE;
    private String myPageTitle;
    private String myPageNumber;
    private String myPageFmt = "";
    private String mySectionTitle;
    private String mySectionNumber;
    private String mySectionFmt = "";
    private boolean mySuppressSingleSections = false;
    
    /**
     * Costruttore.
     */
    public MacroTableOfContents() {
    }
    
    /**
     * Restituisce il livello massimo di annidamento degli elementi esposti nel
     * sommario.
     * 
     * @return Valore.
     * @see    #setLevelMax
     */
    public int getLevelMax() {
        return myLevelMax;
    }
    
    /**
     * Imposta il livello massimo di annidamento degli elementi esposti nel
     * sommario.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code Integer.MAX_VALUE}
     * @see                               #getLevelMax
     */
    public void setLevelMax(int value) {
        if (value <= 0) {
            throw new ArgumentOutOfRangeException("value", value, "[1, ...]");
        }
        
        myLevelMax = value;
    }
    
    /**
     * Restituisce la chiave che identifica la macro corrispondente al titolo
     * delle pagine.
     * 
     * @return Valore.
     * @see    #setMacroPageTitle
     */
    public String getMacroPageTitle() {
        return myPageTitle;
    }
    
    /**
     * Imposta la chiave che identifica la macro corrispondente al titolo delle
     * pagine.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     * @see                                #getMacroPageTitle
     */
    public void setMacroPageTitle(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myPageTitle = value;
    }
     
    /**
     * Restituisce la chiave che identifica la macro corrispondente al numero
     * delle pagine.
     * 
     * @return Valore.
     * @see    #setMacroPageNumber
     */
    public String getMacroPageNumber() {
        return myPageNumber;
    }
    
    /**
     * Imposta la chiave che identifica la macro corrispondente al numero delle
     * pagine.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Numero della pagina.
     * @see #getMacroPageNumber
     * @see it.scoppelletti.sitebuild.SiteElement#getNumber
     * @see it.scoppelletti.sitebuild.SiteElement#getNumberingStyle 
     */
    public void setMacroPageNumber(String value) {
        myPageNumber = value;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito per ogni pagina.
     * 
     * @return Formato.
     * @see    #setPageFormat
     */
    public String getPageFormat() {
        return myPageFmt;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito per ogni pagina.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getPageFormat
     */
    public void setPageFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        myPageFmt = format;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito per ogni sezione.
     * 
     * @return Formato.
     * @see    #setSectionFormat
     */
    public String getSectionFormat() {
        return mySectionFmt;
    }
    
    /**
     * Restituisce la chiave che identifica la macro corrispondente al titolo
     * delle sezioni.
     * 
     * @return Valore.
     * @see    #setMacroSectionTitle
     */
    public String getMacroSectionTitle() {
        return mySectionTitle;
    }
    
    /**
     * Imposta la chiave che identifica la macro corrispondente al titolo delle
     * sezioni.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     * @see                                #getMacroSectionTitle 
     */
    public void setMacroSectionTitle(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        mySectionTitle = value;
    }
    
    /**
     * Restituisce la chiave che identifica la macro corrispondente al numero
     * delle sezioni.
     * 
     * @return Valore.
     * @see    #setMacroSectionNumber
     */
    public String getMacroSectionNumber() {
        return mySectionNumber;
    }
    
    /**
     * Imposta la chiave che identifica la macro corrispondente al numero delle
     * sezioni.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Numero della sezione.
     * @see #getMacroSectionNumber
     * @see it.scoppelletti.sitebuild.SiteElement#getNumber
     * @see it.scoppelletti.sitebuild.SiteElement#getNumberingStyle
     */
    public void setMacroSectionNumber(String value) {
        mySectionNumber = value;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito per ogni pagina sezione.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getSectionFormat
     */
    public void setSectionFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        mySectionFmt = format;
    }
    
    /**
     * Indica se devono essere soppresse dal sommario le sezioni incluse in una
     * famiglia di sezioni allo stesso livello gerarchico costituita da una 
     * sola sezione.
     * 
     * @return Indicatore.
     * @see    #setSuppressSingleSections
     */
    public boolean isSuppressSingleSections() {
        return mySuppressSingleSections;
    }
    
    /**
     * Imposta l&rsquo;indicatore di soppressione dal sommario delle sezioni
     * incluse in una famiglia di sezioni allo stesso livello gerarchico
     * costituita da una sola sezione.
     * 
     * @param                       value Indicatore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #isSuppressSingleSections
     */
    public void setSuppressSingleSections(boolean value) {
        mySuppressSingleSections = value;
    }
    
    @Override
    protected Reader openReader() {
        MacroTableOfContents.Builder builder;
        
        if (StringUtils.isBlank(myPageTitle)) {
            throw new PropertyNotSetException(toString(), "macroPageTitle");
        }
        if (StringUtils.isBlank(mySectionTitle)) {
            throw new PropertyNotSetException(toString(), "macroSectionTitle");
        }
        
        builder = new MacroTableOfContents.Builder(this);
        builder.run();
        
        return new StringReader(builder.toString());
    }
        
    /**
     * Compilatore di una macro.
     */
    private static final class Builder implements Runnable {
        private final MacroTableOfContents myMacro;
        private final StringBuilder myBuf;
        private final MessageFormat myPageFmt;
        private final Object[] myPageArgs;
        private final MessageFormat mySectionFmt;
        private final Object[] mySectionArgs;
        
        /**
         * Costruttore.
         * 
         * @param macro Macro.
         */
        Builder(MacroTableOfContents macro) {
            myMacro = macro;
            myBuf = new StringBuilder();
            myPageFmt = new MessageFormat(myMacro.getPageFormat());
            myPageArgs = new Object[4];
            mySectionFmt = new MessageFormat(myMacro.getSectionFormat());
            mySectionArgs = new Object[3];
        }
        
        /**
         * Rappresenta l&rsquo;oggetto con una stringa.
         * 
         * @return Stringa.
         */
        @Override
        public String toString() {
            return myBuf.toString();
        }
        
        /**
         * Esegue l&rsquo;operazione.
         */
        public void run() {
            run(myMacro.getPage(), 1);                            
        }
        
        /**
         * Accoda una lista di sezioni.
         * 
         * @param sectionList Lista di sezioni.
         * @param level       Livello.
         */
        private void run(SiteSectionList sectionList, int level) {
            boolean sect;
            
            if (myMacro.isSuppressSingleSections() &&
                    sectionList.getSections().size() == 1) {                                                
                for (SitePage page :
                    sectionList.getSections().get(0).getPages()) {
                    appendPage(page, level);                                
                }
            } else {
                for (SiteSection section : sectionList.getSections()) {                
                    sect = true;                                
                    for (SitePage page : section.getPages()) {
                        if (sect) {
                            appendSection(page, level);
                            sect = false;
                        }
                        
                        if (level >= myMacro.getLevelMax()) {
                            break;
                        }
                        
                        appendPage(page, level + 1);
                    }                                
                }
            }                
        }
        
        /**
         * Accoda una sezione.
         * 
         * @param page  Prima pagina della sezione.
         * @param level Livello.
         */
        private void appendSection(SitePage page, int level) {
            String value;
            
            mySectionArgs[0] = new Integer(level);
            mySectionArgs[1] = page.getMacroAsString(
                    myMacro.getMacroSectionTitle());
            
            if (StringUtils.isBlank(myMacro.getMacroSectionNumber())) {
                value = page.getParentElement().getNumber();
                mySectionArgs[2] = StringUtils.isBlank(value) ? "" : value;
            } else {
                mySectionArgs[2] = page.getMacroAsString(
                        myMacro.getMacroSectionNumber());
            }
                                        
            myBuf.append(mySectionFmt.format(mySectionArgs));
        }
        
        /**
         * Accoda una pagina.
         * 
         * @param page  Pagina.
         * @param level Livello.
         */
        private void appendPage(SitePage page, int level) {
            myPageArgs[0] = new Integer(level);        
            myPageArgs[1] = page.getMacroAsString(
                    myMacro.getMacroPageTitle());
            
            if (StringUtils.isBlank(myMacro.getMacroPageNumber())) { 
                myPageArgs[2] = page.getNumber();
            } else {
                myPageArgs[2] = page.getMacroAsString(
                        myMacro.getMacroPageNumber());
            }
            
            myPageArgs[3] = myMacro.getPage().getBuildRootPath().concat(
                    page.getBuildPath());                  
            myBuf.append(myPageFmt.format(myPageArgs));
            
            if (level < myMacro.getLevelMax()) {
                run(page, level + 1);
            }
        }       
    }
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import it.scoppelletti.sitebuild.*;

/**
 * Macro sostituita con la rappresentazione del numero della pagina nella quale
 * deve essere inserito il testo della macro. 
 * 
 * @see   it.scoppelletti.sitebuild.SiteElement#getNumber
 * @see   it.scoppelletti.sitebuild.SiteElement#getNumberingStyle
 * @see   <A
 *        HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/pagenumber.html"
 *        TARGET="_top">Macro <CODE>MacroPageNumber</CODE></A>
 * @since 1.1.0
 */
public class MacroPageNumber extends MacroElementNumber {

    /**
     * Costruttore.
     */
    public MacroPageNumber() {
    }
        
    @Override
    protected SiteElement getCurrentElement() {
        return getPage();
    }
}

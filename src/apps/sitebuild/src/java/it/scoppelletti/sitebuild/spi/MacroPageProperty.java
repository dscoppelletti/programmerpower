/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.NotImplementedException;

/**
 * Macro sostituita con una delle propriet&agrave; della pagina nella quale deve
 * essere inserito il testo della macro. 
 *
 * @see it.scoppelletti.sitebuild.SitePage
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/pageprop.html"
 *      TARGET="_top">Macro <CODE>MacroPageProperty</CODE></A>
 * @since 1.0.0
 */
public class MacroPageProperty extends AbstractSiteMacro {
    
    /**
     * Nome della propriet&agrave; corrispondente al percorso del file compilato
     * della pagina relativo al direttorio di compilazione del progetto. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sitebuild.SitePage#getBuildPath
     * @see it.scoppelletti.sitebuild.SiteProject#getBuildDirectory
     */
    public static final String PROP_BUILDPATH = "buildPath";
    
    /**
     * Nome della propriet&agrave; corrispondente al percorso relativo del 
     * direttorio di compilazione del progetto rispetto alla pagina. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sitebuild.SitePage#getBuildRootPath
     * @see it.scoppelletti.sitebuild.SiteProject#getBuildDirectory
     */
    public static final String PROP_BUILDROOTPATH = "buildRootPath";
    
    /**
     * Nome della propriet&agrave; corrispondente al numero d&rsquo;ordine della
     * pagina. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see   it.scoppelletti.sitebuild.SiteElement#getNumber
     * @since 1.1.0
     */
    public static final String PROP_NUMBER = "number";
    
    private String myPropName;
    
    /**
     * Costruttore.
     */
    public MacroPageProperty() {
    }
    
    /**
     * Restituisce il nome della propriet&agrave; della pagina.
     * 
     * @return Valore.
     * @see    #setPropertyName
     */
    public String getPropertyName() {
        return myPropName;
    }
    
    /**
     * Imposta il nome della propriet&agrave; della pagina.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     * @see                                #getPropertyName
     */
    public void setPropertyName(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myPropName = value;
    }
    
    @Override
    protected Reader openReader() {
        String value;
        
        if (StringUtils.isBlank(myPropName)) {
            throw new PropertyNotSetException(toString(), "propertyName");
        }
        
        if (myPropName.equalsIgnoreCase(MacroPageProperty.PROP_BUILDPATH)) {
            value = getPage().getBuildPath();
        } else if (myPropName.equalsIgnoreCase(
                MacroPageProperty.PROP_BUILDROOTPATH)) {
            value = getPage().getBuildRootPath();
        } else if (myPropName.equalsIgnoreCase(
                MacroPageProperty.PROP_NUMBER)) {
            value = getPage().getNumber();            
        } else {
            throw new NotImplementedException(getPage().getClass().getName(),
                    myPropName);
        }
        
        return new StringReader(value);
    }
}

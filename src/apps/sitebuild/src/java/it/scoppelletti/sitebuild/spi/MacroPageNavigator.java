/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild.spi;

import java.io.*;
import java.text.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.sitebuild.*;

/**
 * Macro sostituita con un frammento di codice
 * <ACRONYM TITLE="HyperText Markup Language">HTML</ACRONYM> che pu&ograve;
 * riportare il riferimento alle pagine successiva e/o precedente rispetto alla
 * pagina nella quale deve essere inserito il testo della macro (tipicamente a
 * scopo di navigazione).
 *    
 * @see   it.scoppelletti.sitebuild.SitePage#getNextPage
 * @see   it.scoppelletti.sitebuild.SitePage#getPreviousPage
 * @see   <A
 *        HREF="${it.scoppelletti.token.referenceUrl}/sitebuild/navigator.html"
 *        TARGET="_top">Macro <CODE>MacroPageNavigator</CODE></A>
 * @since 1.0.0
 */
public class MacroPageNavigator extends AbstractSiteMacro {
    private String myMacroTitle;
    private String myNextPrevFmt = "";
    private String myNextFmt = "";
    private String myPrevFmt = "";
    private String myNoneFmt = "";
    
    /**
     * Costruttore.
     */
    public MacroPageNavigator() {        
    }
    
    /**
     * Restituisce la chiave che identifica la macro corrispondente al titolo
     * delle pagine.
     * 
     * @return Valore.
     * @see    #setMacroTitle
     */
    public String getMacroTitle() {
        return myMacroTitle;
    }
    
    /**
     * Imposta la chiave che identifica la macro corrispondente al titolo delle
     * pagine.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     * @see                                #getMacroTitle
     */
    public void setMacroTitle(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myMacroTitle = value;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito nelle pagine per le
     * quali sono determinate sia la pagina successiva che la pagina precedente.
     * 
     * @return Formato.
     * @see    #setNextPrevFormat
     */
    public String getNextPrevFormat() {
        return myNextPrevFmt;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito nelle pagine per le quali
     * sono determinate sia la pagina successiva che la pagina precedente.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getNextPrevFormat
     */
    public void setNextPrevFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        myNextPrevFmt = format;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito nelle pagine per le
     * quali &egrave; determinata la pagina successiva ma non la pagina
     * precedente.
     * 
     * @return Formato.
     * @see    #setNextFormat
     */
    public String getNextFormat() {
        return myNextFmt;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito nelle pagine per le quali
     * &egrave; determinata la pagina successiva ma non la pagina precedente.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getNextFormat
     */
    public void setNextFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        myNextFmt = format;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito nelle pagine per le
     * quali &egrave; determinata la pagina precedente ma non la pagina
     * successiva.
     * 
     * @return Formato.
     * @see    #setPrevFormat
     */
    public String getPrevFormat() {
        return myPrevFmt;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito nelle pagine per le quali
     * &egrave; determinata la pagina precedente ma non la pagina successiva.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getPrevFormat
     */
    public void setPrevFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        myPrevFmt = format;
    }
    
    /**
     * Restituisce il formato del codice HTML sostituito nelle pagine per le
     * quali non sono determinate n&eacute; la pagina successiva n&eacute; la
     * pagina precedente.
     * 
     * @return Formato.
     * @see    #setNoneFormat
     */
    public String getNoneFormat() {
        return myNoneFmt;
    }
    
    /**
     * Imposta il formato del codice HTML sostituito nelle pagine per le quali
     * non sono determinate n&eacute; la pagina successiva n&eacute; la pagina
     * precedente.
     * 
     * @param                       format Formato.
     * @it.scoppelletti.tag.default        Stringa vuota {@code ""}.
     * @see                                #getNoneFormat
     */
    public void setNoneFormat(String format) {
        if (format == null) {
            throw new ArgumentNullException("format");
        }
        
        myNoneFmt = format;
    }
    
    @Override
    protected Reader openReader() {
        String fmt;
        Object[] args;        
        SitePage nextPage, prevPage;
        MessageFormat formatter;
        
        if (StringUtils.isBlank(myMacroTitle)) {
            throw new PropertyNotSetException(toString(), "macroTitle");
        }
        
        args = new Object[] { "", "", "", "" };
        
        nextPage = getPage().getNextPage();
        if (nextPage != null) {
            args[0] = getPage().getBuildRootPath().concat(
                    nextPage.getBuildPath());
            args[1] = nextPage.getMacroAsString(myMacroTitle);
        }
        
        prevPage = getPage().getPreviousPage();
        if (prevPage != null) {
            args[2] = getPage().getBuildRootPath().concat(
                    prevPage.getBuildPath());
            args[3] = prevPage.getMacroAsString(myMacroTitle);            
        }
        
        if (nextPage != null && prevPage != null) {
            fmt = myNextPrevFmt;
        } else if (nextPage != null && prevPage == null) {
            fmt = myNextFmt;
        } else if (nextPage == null && prevPage != null) {
            fmt = myPrevFmt;
        } else /* if (nextPage == null && prevPage == null) */ {
            fmt = myNoneFmt;
        }
        
        formatter = new MessageFormat(fmt);
        
        return new StringReader(formatter.format(args));
    }
}

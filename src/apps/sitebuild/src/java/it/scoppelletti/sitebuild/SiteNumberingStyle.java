/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.NotImplementedException;

/**
 * Stile di numerazione di una lista di elementi di un sito.
 *
 * since 1.1.0
 */
public enum SiteNumberingStyle {
    
    /**
     * Numeri decimali.
     */
    DECIMAL("1"),
    
    /**
     * Lettere alfabetiche minuscole.
     */
    LOWERCASE_ALPHABETIC("a"),
    
    /**
     * Lettere alfabetiche maiuscole.
     */
    UPPERCASE_ALPHABETIC("A"),
    
    /**
     * Numerazione Romana minuscola. 
     */
    LOWERCASE_ROMAN("i"),
    
    /**
     * Numerazione Romana maiuscola.
     */
    UPPERCASE_ROMAN("I");
    
    private static final AlphabeticNumbering myLowerCaseAlphabetic =
            new AlphabeticNumbering("abcdefghijklmnopqrstuvwxyz");
    private static final AlphabeticNumbering myUpperCaseAlphabetic =
            new AlphabeticNumbering("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    private static final RomanNumbering myLowerCaseRoman =
            new RomanNumbering(true);
    private static final RomanNumbering myUpperCaseRoman =
            new RomanNumbering(false);
    
    private final String myCode;
    
    /**
     * Costruttore.
     * 
     * @param code Codice.
     */
    private SiteNumberingStyle(String code) {
        if (StringUtils.isBlank(code)) {
            throw new ArgumentNullException("code");
        }        
        
        myCode = code;
    }
    
    /**
     * Restituisce il codice.
     * 
     * @return Valore.
     */
    public String getCode() {
        return myCode;
    }
    
    /**
     * Converte un codice nel corrispondente stile di numerazione.
     * 
     * @param  code Codice.
     * @return      Stile.
     */
    public static SiteNumberingStyle parse(String code) {
        for (SiteNumberingStyle style : SiteNumberingStyle.values()) {
            if (style.getCode().equals(code)) {
                return style;
            }
        }
        
        throw new EnumConstantNotPresentException(SiteNumberingStyle.class,
                code);
    }
    
    /**
     * Converte un numero in stringa secondo lo stile di numerazione.
     * 
     * @param  n Numero da convertire.
     * @return   Stringa.
     */
    public String toString(int n) {
        String s;
        
        switch (this) {
        case DECIMAL:
            s = String.valueOf(n);
            break;
            
        case LOWERCASE_ALPHABETIC:
            s = myLowerCaseAlphabetic.toString(n);
            break;
            
        case UPPERCASE_ALPHABETIC:
            s = myUpperCaseAlphabetic.toString(n);
            break;
            
        case LOWERCASE_ROMAN:
            s = myLowerCaseRoman.toString(n);
            break;
            
        case UPPERCASE_ROMAN:
            s = myUpperCaseRoman.toString(n);
            break;
            
        default:
            throw new NotImplementedException(
                    SiteNumberingStyle.class.getName(), this.name());
        }
        
        return s;
    }
}

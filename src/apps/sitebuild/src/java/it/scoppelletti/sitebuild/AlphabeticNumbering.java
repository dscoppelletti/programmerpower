/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sitebuild;

import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.*;

/**
 * Numerazione alfabetica.
 */
final class AlphabeticNumbering {
    private final char[] myLetters;
    
    /**
     * Costruttore.
     * 
     * @param letters Lettere.
     */
    AlphabeticNumbering(String letters) {
        if (StringUtils.isBlank(letters)) {
            throw new ArgumentNullException("letters");
        }
        
        myLetters = letters.toCharArray();
    }
    
    /**
     * Converte un numero nella sua rappresentazione.
     * 
     * @param  n Numero da convertire.
     * @return   Rappresentazione.
     */
    String toString(int n) {
        int rem;
        int base = myLetters.length;
        
        StringBuilder buf = new StringBuilder();
        
        while (n > 0) {
            rem = (n - 1) % base;
            buf.append(myLetters[rem]);
            n = (n - 1 - rem) / base;
        }
        
        return buf.reverse().toString();
    }
}

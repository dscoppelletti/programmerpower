/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.keypairgen;

import it.scoppelletti.programmerpower.security.*;

import java.security.*;
import java.security.interfaces.*;
import java.util.*;
import it.scoppelletti.programmerpower.security.spi.*;

/**
 * Rappresentazione di una chiave pubblica per l&rsquo;algoritmo
 * <ACRONYM TITLE="Rivest, Shamir, & Adleman">RSA</ACRONYM> con una collezione
 * di parametri che consente di ricostruire la stessa chiave.
 * 
 * @see   it.scoppelletti.programmerpower.security.spi.RSAPublicKeyFactory
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top"><ACRONYM 
 *        TITLE="Public-Key Cryptography Standards">PKCS</ACRONYM> &#35;1: RSA
 *        Cryptography Standard</A>    
 * @since 1.0.0
 */
public final class RSAPublicKeyToPropertySetProvider implements
    KeyToPropertySetProvider {

    /**
     * Costruttore.
     */
    public RSAPublicKeyToPropertySetProvider() {        
    }
    
    public Properties toProperties(Key key) {
        RSAPublicKey rsaKey;
        Properties props;
        
        if (!(key instanceof RSAPublicKey)) {
            return null;
        }
        
        props = new Properties();
        rsaKey = (RSAPublicKey) key;
        props.setProperty(CryptoTools.PROP_KEYFACTORY,
                RSAPublicKeyFactory.class.getName());
        props.setProperty(RSAPublicKeyFactory.PROP_MODULUS, 
                rsaKey.getModulus().toString());
        props.setProperty(RSAPublicKeyFactory.PROP_PUBLICEXPONENT, 
                rsaKey.getPublicExponent().toString());
        
        return props;
    }
}

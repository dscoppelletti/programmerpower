/*
 * Copyright (C) 2010-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Key Pair Generator.
 * 
 * @it.scoppelletti.tag.module {@code it.scoppelletti.security.keypairgen}
 * @version 1.0.1
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/keypairgen/index.html"
 *      TARGET="_top">Key Pair Generator</A> 
 */
package it.scoppelletti.security.keypairgen;

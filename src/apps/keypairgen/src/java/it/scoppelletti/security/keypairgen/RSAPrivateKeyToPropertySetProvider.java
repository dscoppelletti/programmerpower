/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.keypairgen;

import it.scoppelletti.programmerpower.security.*;

import java.security.*;
import java.security.interfaces.*;
import java.util.*;
import it.scoppelletti.programmerpower.security.spi.*;

/**
 * Rappresentazione di una chiave privata per l&rsquo;algoritmo
 * <ACRONYM TITLE="Rivest, Shamir, & Adleman">RSA</ACRONYM> con una collezione
 * di parametri che consente di ricostruire la stessa chiave.
 * 
 * @see   it.scoppelletti.programmerpower.security.spi.RSAPrivateKeyFactory
 * @see   <A HREF="http://www.rsa.com/rsalabs" TARGET="_top"><ACRONYM
 *        TITLE="Public-Key Cryptography Standards">PKCS</ACRONYM> &#35;1: RSA
 *        Cryptography Standard</A>    
 * @since 1.0.0
 */
public final class RSAPrivateKeyToPropertySetProvider implements
    KeyToPropertySetProvider {

    /**
     * Costruttore.
     */
    public RSAPrivateKeyToPropertySetProvider() {        
    }
    
    public Properties toProperties(Key key) {
        RSAPrivateKey rsaKey;
        Properties props;
        
        if (!(key instanceof RSAPrivateKey)) {
            return null;
        }
        
        props = new Properties();
        rsaKey = (RSAPrivateKey) key;
        props.setProperty(CryptoTools.PROP_KEYFACTORY,
                RSAPrivateKeyFactory.class.getName());
        props.setProperty(RSAPrivateKeyFactory.PROP_MODULUS, 
                rsaKey.getModulus().toString());
        props.setProperty(RSAPrivateKeyFactory.PROP_PRIVATEEXPONENT, 
                rsaKey.getPrivateExponent().toString()); 
        
        return props;
    }
}

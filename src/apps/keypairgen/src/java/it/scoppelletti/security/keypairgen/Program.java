/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.keypairgen;

import java.util.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.spring.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
@ApplicationAbout.Annotation(
        copyright = "Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.",
        licenseResource = "it/scoppelletti/programmerpower/resources/asl2.txt",
        pageUrl = "http://www.scoppelletti.it/programmerpower/keypairgen")
public final class Program extends ConsoleApplicationRunner {
    private static final String RESOURCE_BASENAME =
        "it.scoppelletti.security.keypairgen.ProgramOptionSet";
    private static final String OPTION_CONFIG = "config";
    private static final String OPTION_PREFIX = "prefix";
    private static final String OPTION_PUBLIC = "public";
    private static final String OPTION_PRIVATE = "private";
    private static final String OPTION_OVERWRITE = "overwrite";
    private static final String OPTION_ENCODED = "encoded";
    
    /**
     * Costruttore.
     */
    private Program() {
    }

    @Override
    protected void initOptions(List<CliOption> list) { 
        CliOption option;
               
        option = new CliOptionFile(Program.OPTION_CONFIG,
                Program.RESOURCE_BASENAME);
        option.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        list.add(option);
        
        option = new CliOptionString(Program.OPTION_PREFIX,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionFile(Program.OPTION_PUBLIC,
                Program.RESOURCE_BASENAME);
        option.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        list.add(option);
        
        option = new CliOptionFile(Program.OPTION_PRIVATE,
                Program.RESOURCE_BASENAME);
        option.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        list.add(option);
        
        option = new CliOptionBoolean(Program.OPTION_OVERWRITE,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionBoolean(Program.OPTION_ENCODED,
                Program.RESOURCE_BASENAME);
        list.add(option);
                
        super.initOptions(list);
    }    
    
    /**
     * File delle propriet&agrave; di inizializzazione del generatore di coppie
     * di chiavi.
     * 
     * @return Opzione.
     * @since  1.0.1
     */
    public CliOptionFile getConfigFile() {
        return (CliOptionFile) getOption(Program.OPTION_CONFIG);
    }

    /**
     * File di output delle propriet&agrave; della chiave pubblica.
     * 
     * @return Opzione.
     * @since  1.0.1
     */
    public CliOptionFile getPublicFile() {
        return (CliOptionFile) getOption(Program.OPTION_PUBLIC);
    }

    /**
     * File di output delle propriet&agrave; della chiave privata.
     * 
     * @return Opzione.
     * @since  1.0.1
     */
    public CliOptionFile getPrivateFile() {
        return (CliOptionFile) getOption(Program.OPTION_PRIVATE);
    }

    /**
     * Entry-point.
     * 
     * @param args Argomenti sulla linea di comando.
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/keypairgen/cli.html"
     *      TARGET="_top">Linea di comando</A> 
     */    
    public static void main(String args[]) {
        Program appl = new Program();

        appl.run(args);
    }
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.keygen;

import it.scoppelletti.programmerpower.security.*;

import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.binary.*;
import it.scoppelletti.programmerpower.security.spi.*;

/**
 * Rappresentazione di una chiave per l&rsquo;algoritmo
 * <ACRONYM TITLE="Digital Encryption Standard">DES</ACRONYM> con una collezione
 * di parametri che consente di ricostruire la stessa chiave.
 * 
 * @see   it.scoppelletti.programmerpower.security.spi.DESKeyFactory
 * @see   <A HREF="http://csrc.nist.gov/publications/PubsFIPSArch.html"
 *        TARGET="_top"><ACRONYM
 *        TITLE="Federal Information Processing Standards">FIPS</ACRONYM> 46-3:
 *        Data Encryption Standard (DES)</A> 
 * @since 1.0.0
 */
public final class DESKeyToPropertySetProvider implements
        KeyToPropertySetProvider {

    /**
     * Costruttore.
     */
    public DESKeyToPropertySetProvider() {        
    }
    
    public Properties toProperties(Key key) {
        byte[] data;
        SecretKey desKey;        
        SecretKeyFactory keyFactory;
        DESKeySpec param;
        Properties props;
        
        if (!(key instanceof SecretKey)) {
            return null;
        }
             
        try {
            keyFactory = SecretKeyFactory.getInstance(DESKeyFactory.ALGORITHM);
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
        
        try {            
            desKey = keyFactory.translateKey((SecretKey) key);
        } catch (InvalidKeyException ex) {
            return null;
        }
        
        try {
            param = (DESKeySpec) keyFactory.getKeySpec(desKey,
                    DESKeySpec.class);
        } catch (InvalidKeySpecException ex) {
            return null;
        }
        
        props = new Properties();
        props.setProperty(CryptoTools.PROP_KEYFACTORY,
                DESKeyFactory.class.getName());
        data = param.getKey();
        props.setProperty(DESKeyFactory.PROP_KEY, Hex.encodeHexString(data));
        Arrays.fill(data, Byte.MIN_VALUE);
        
        return props;
    }
}

/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.keygen;

import java.io.*;
import java.security.*;
import java.util.*;
import javax.crypto.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Generazione di una chiave di crittografia simmetrica.
 * 
 * @since 1.0.0
 */
@Final
public class KeyGeneratorBean implements Runnable {
    private File myConfigFile;
    private String myPrefix;
    private File myOutputFile;
    private boolean myOverwrite;
    private boolean myEncoded;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
    
    /**
     * Costruttore.
     */
    public KeyGeneratorBean() {        
    }
        
    /**
     * Imposta il file di configurazione.
     * 
     * @param file File.
     */
    @Required
    public void setConfigFile(File file) {
        myConfigFile = file;
    }
    
    /**
     * Imposta il prefisso da applicare alle propriet&agrave; da interrogare.
     * 
     * @param prefix Valore.
     */
    public void setPrefix(String prefix) {
        myPrefix = prefix;
    }
    
    /**
     * Imposta il file di output.
     * 
     * @param                       file File.
     * @it.scoppelletti.tag.default      {@code stdout}
     */
    public void setOutputFile(File file) {
        myOutputFile = file;
    }
    
    /**
     * Imposta l&rsquo;indicatore di sovrascrittura del file di output anche se
     * esiste gi&agrave;
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     */
    public void setOverwrite(boolean value) {
        myOverwrite = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di formato di output codificato.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     */
    public void setEncoded(boolean value) {
        myEncoded = value;
    }
        
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {        
        Properties props;
        OutputStream out = null;
        Key key;
        KeyGenerator keyGen;
        
        if (myConfigFile == null) {
            throw new PropertyNotSetException(toString(), "configFile");
        }
        
        try {
            props = loadConfig();
            out = openOutput();
            if (out == null) {
                return;
            }
            
            keyGen = CryptoTools.getKeyGenerator(props, myPrefix);
            key = keyGen.generateKey();
       
            props = CryptoTools.toProperties(key, myEncoded);
            props.store(out, null);                   
        } catch (IOException ex) {
            throw new IOOperationException(ex);        
        } finally {
            if (out != null && myOutputFile != null) {
                IOTools.close(out);
                out = null;
            }            
        }        
    }
    
    /**
     * Legge le propriet&agrave; di configurazione.
     * 
     * @return Collezione.
     */
    private Properties loadConfig() throws IOException {
        InputStream in = null;
        Properties props = new Properties();
                
        try {
            in = new FileInputStream(myConfigFile);
            props.load(in);
        } finally {
            if (in != null) {
                in.close();
                in = null;
            }
        }
        
        return props;
    }
    
    /**
     * Apre il flusso di output.
     * 
     * @return Flusso.
     */
    private OutputStream openOutput() throws IOException {
        PrintStream out;
        IOResources ioRes = new IOResources();
        
        if (myOutputFile == null) {
            return System.out;
        }
        
        if (myOutputFile.exists()) {
            if (myOverwrite) {
                myUI.display(MessageType.WARNING, ioRes.getFileOverwriteMessage(
                        myOutputFile.toString()));
            } else {
                throw new FileAlreadyExistException(myOutputFile.toString());
            }
        }
        
        out = new PrintStream(myOutputFile);
        
        return out;
    }        
}

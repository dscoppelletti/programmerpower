/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.digest;

import java.util.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.security.*;
import it.scoppelletti.programmerpower.console.spring.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
@ApplicationAbout.Annotation(
        copyright = "Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.",
        licenseResource = "it/scoppelletti/programmerpower/resources/asl2.txt",
        pageUrl = "http://www.scoppelletti.it/programmerpower/digest")
public final class Program extends ConsoleApplicationRunner {

    /**
     * Costruttore.
     */
    private Program() {
    }
    
    @Override
    protected void initOptions(List<CliOption> list) { 
        list.add(new CliOptionPasswordFile());
        
        super.initOptions(list);
    }          
    
    /**
     * Entry-point.
     * 
     * @param args Argomenti sulla linea di comando.
     * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/digest/cli.html"
     *        TARGET="_top">Linea di comando</A>
     */    
    public static void main(String args[]) {
        Program appl = new Program();

        appl.run(args);
    }    
}

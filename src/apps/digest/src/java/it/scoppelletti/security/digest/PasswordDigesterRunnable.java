/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.security.digest;

import java.io.*;
import javax.security.auth.callback.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;

/**
 * Codifica una password.
 */
@Final 
public class PasswordDigesterRunnable implements Runnable {
    private CallbackHandler myCallbackHandler;
    
    @javax.annotation.Resource(name = PasswordDigester.BEAN_NAME)
    private PasswordDigester myPwdDigester;
    
    /**
     * Costruttore.
     */
    public PasswordDigesterRunnable() {        
    }

    /**
     * Imposta il gestore della comunicazione via callback tra i moduli di login
     * e un&rsquo;applicazione.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setCallbackHandler(CallbackHandler obj) {
        myCallbackHandler = obj;
    }
        
    /**
     * Esegue l&rsquo;operazione.
     */    
    public void run() {
        Callback[] cbArray;
        SecureString pwd = null;
        PasswordCallback pwdCallback = null;
        SecurityResources res = new SecurityResources();
        
        if (myCallbackHandler == null) {
            throw new PropertyNotSetException(toString(), "callbackHandler");
        }
        
        pwdCallback = new PasswordCallback(res.getPwdPrompt(), false);
        
        try {
            cbArray = new Callback[1];
            cbArray[0] = pwdCallback;
            myCallbackHandler.handle(cbArray);
            pwd = new SecureString(pwdCallback.getPassword());
            System.out.println(myPwdDigester.digestPassword(pwd));
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } catch (UnsupportedCallbackException ex) {
            throw SecurityTools.toSecurityException(ex);
        } finally {
            if (pwd != null) {
                pwd.clear();
                pwd = null;
            }
            if (pwdCallback != null) {
                pwdCallback.clearPassword();
                pwdCallback = null;
            }
        }        
    }
}

/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textdump;

import java.util.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.console.spring.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
@ApplicationAbout.Annotation(
        copyright = "Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.",
        licenseResource = "it/scoppelletti/programmerpower/resources/asl2.txt",
        pageUrl = "http://www.scoppelletti.it/programmerpower/textdump")
public final class Program extends ConsoleApplicationRunner {
    private static final String RESOURCE_BASENAME =
        "it.scoppelletti.textdump.ProgramOptionSet";
    private static final String OPTION_IN = "in";
    private static final String OPTION_ENCODING = "encoding";
    private static final String OPTION_OUT = "out";
    private static final String OPTION_OVERWRITE = "overwrite";

    /**
     * Costruttore.
     */
    private Program() {
    }

    @Override
    protected void initOptions(List<CliOption> list) { 
        CliOption option;
               
        option = new CliOptionFile(Program.OPTION_IN,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionCharset(Program.OPTION_ENCODING,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionFile(Program.OPTION_OUT,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        option = new CliOptionBoolean(Program.OPTION_OVERWRITE,
                Program.RESOURCE_BASENAME);
        list.add(option);
        
        super.initOptions(list);
    }    
    
    /**
     * File di input.
     * 
     * @return Opzione.
     * @since  1.0.2
     */
    public CliOptionFile getInputFile() {
        return (CliOptionFile) getOption(Program.OPTION_IN);
    }  
    
    /**
     * Codifica dei caratteri per la lettura del file di input.
     * 
     * @return Opzione.
     */
    private CliOptionCharset getEncoding() {
        return (CliOptionCharset) getOption(Program.OPTION_ENCODING);
    }  
    
    /**
     * File di output.
     * 
     * @return Opzione.
     * @since  1.0.2
     */
    public CliOptionFile getOutputFile() {
        return (CliOptionFile) getOption(Program.OPTION_OUT);
    }      
    
    /**
     * Sovrascrive il file di output anche se esiste gi&agrave;
     * 
     * @return Opzione.
     */
    private CliOptionBoolean getOverwrite() {
        return (CliOptionBoolean) getOption(Program.OPTION_OVERWRITE);
    }
    
    /**
     * Entry-point.
     * 
     * @param args Argomenti sulla linea di comando.
     * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/textdump/cli.html"
     *        TARGET="_top">Linea di comando</A> 
     */    
    public static void main(String args[]) {
        Program appl = new Program();

        appl.run(args);
    }
            
    /**
     * Controlli di validazione.
     */
    @Override
    public void checkOptions() {
        if (!getInputFile().isSpecified()) {
            getEncoding().setSpecifiedCheck(
                    CliOption.SpecifiedCheck.INCOMPATIBLE);
        }
        if (!getOutputFile().isSpecified()) {
            getOverwrite().setSpecifiedCheck(
                    CliOption.SpecifiedCheck.INCOMPATIBLE);            
        }
        
        super.checkOptions();
    }
}

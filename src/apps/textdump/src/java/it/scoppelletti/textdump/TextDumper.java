/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textdump;

import java.io.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.types.*;

/**
 * Implementazione dell&rsquo;applicazione.
 * 
 * @since 1.0.2
 */
@Final
public class TextDumper implements Runnable {
    private InputStreamReaderFactory myInputFactory;
    private PrintStreamFactory myOutputFactory;
    
    /**
     * Costruttore.
     */
    public TextDumper() {        
    }
    
    /**
     * Imposta l&rsquo;oggetto di factory del flusso di lettura.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setInputFactory(InputStreamReaderFactory obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myInputFactory = obj;
    }
    
    /**
     * Imposta l&rsquo;oggetto di factory del flusso di stampa.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setOutputFactory(PrintStreamFactory obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myOutputFactory = obj;        
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {
        int count;
        String line;
        int[] codePoints;
        BufferedReader reader = null;
        PrintStream out = null;
        
        if (myInputFactory == null) {
            throw new PropertyNotSetException(toString(), "inputFactory");
        }
        if (myOutputFactory == null) {
            throw new PropertyNotSetException(toString(), "outputFactory");
        }
        
        try {
            reader = myInputFactory.open();
            out = myOutputFactory.open();
            
            count = 0;
            line = reader.readLine();
            while (line != null) {
                count++;
                codePoints = StringTools.toCodePointArray(line);
                
                out.printf("C%1$010d:", count);
                for (int codePoint : codePoints) {
                    out.printf(" %1$-8c", codePoint);
                }
                out.println();
                
                out.printf("X%1$010d:", count);
                for (int codePoint : codePoints) {
                    out.printf(" 0x%1$06X", codePoint);
                }
                out.println();
                
                line = reader.readLine();
            }            
        } catch (IOException ex) {
            throw new IOOperationException(ex);        
        } finally {
            reader = IOTools.close(reader);
            out = IOTools.close(out);
        }        
    }
}

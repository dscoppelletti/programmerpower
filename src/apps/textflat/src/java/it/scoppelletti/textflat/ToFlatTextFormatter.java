/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textflat;

import java.io.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Implementazione dell&rsquo;applicazione.
 * 
 * @since 1.1.1
 */
@Final
public class ToFlatTextFormatter implements Runnable {
    private InputStreamReaderFactory myInputFactory;
    private OutputStreamWriterFactory myOutputFactory;
    
    /**
     * Costruttore.
     */
    public ToFlatTextFormatter() {        
    }
    
    /**
     * Imposta l&rsquo;oggetto di factory del flusso di lettura.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setInputFactory(InputStreamReaderFactory obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myInputFactory = obj;
    }
    
    /**
     * Imposta l&rsquo;oggetto di factory del flusso di stampa.
     * 
     * @param obj Oggetto.
     */
    @Required
    public void setOutputFactory(OutputStreamWriterFactory obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myOutputFactory = obj;        
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {
        String line;
        BufferedReader reader = null;
        BufferedWriter writer = null;
        
        if (myInputFactory == null) {
            throw new PropertyNotSetException(toString(), "inputFactory");
        }
        if (myOutputFactory == null) {
            throw new PropertyNotSetException(toString(), "outputFactory");
        }
        
        try {
            reader = myInputFactory.open();
            writer = myOutputFactory.open();
            
            line = reader.readLine();
            while (line != null) {
                writer.write(line);
                writer.newLine();
                line = reader.readLine();
            }            
        } catch (IOException ex) {
            throw new IOOperationException(ex);        
        } finally {
            reader = IOTools.close(reader);
            writer = IOTools.close(writer);
        }        
    }
}

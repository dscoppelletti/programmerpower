/*
 * Copyright (C) 2009-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textflat;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.types.*;
import it.scoppelletti.programmerpower.xml.*;

/**
 * Conversione dei caratteri speciali.
 * 
 * <P>La classe {@code SpecialCharacterConverter} configura i caratteri da
 * convertire in base ad un file
 * <ACRONYM TITLE="eXtensible Markup Language">XML</ACRONYM>
 * {@code it-scoppelletti-textflat-specialCharacterConverter.xml} che deve
 * essere rilevabile attraverso il class-path.</P>
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/textflat/specialcharacterconverter1.xsd}
 * @see   <A
 *        HREF="${it.scoppelletti.token.referenceUrl}/textflat/specialchar.html"
 *        TARGET="_top">Conversione dei caratteri speciali</A> 
 * @since 1.0.0
 */
public final class SpecialCharacterConverter {
    
    /**
     * Nome della risorsa di configurazione della conversione dei caratteri
     * speciali. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */
    public static final String CONFIG_FILE =
        "it-scoppelletti-textflat-specialCharacterConverter.xml";
    
    private final Map<Integer, String> myMap;
    
    /**
     * Costruttore.
     */
    public SpecialCharacterConverter() {
       myMap = loadMap(); 
    }

    /**
     * Restituisce la stringa corrispondente ad un carattere.
     * 
     * @param  codePoint Codice Unicode del carattere.
     * @return           Stringa corrispondente al carattere. Se per il
     *                   carattere {@code codePoint} non &egrave; configurata
     *                   una stringa corrispondente, restituisce una stringa
     *                   costituita dallo stesso carattere {@code codePoint}. 
     */
    public String convert(int codePoint) {
        String to;
        
        to = myMap.get(codePoint);
        if (to == null) {
            to = new String(new int[] { codePoint }, 0, 1);
        }
        
        return to;
    }
    
    /**
     * Converte tutti i caratteri speciali in una stringa 
     * 
     * @param  s Stringa originale.
     * @return   Stringa elaborata. Se la stringa {@code s} &egrave;
     *           {@code null}, restituisce {@code null}.
     */
    public String convert(String s) {
        int[] chars;
        String to;
        StringBuilder buf;
        
        if (s == null) {
            return null;            
        }
        
        chars = StringTools.toCodePointArray(s);
        buf = new StringBuilder(chars.length);
        
        for (int c : chars) {
            to = myMap.get(c);
            if (to == null) {
                buf.appendCodePoint(c);
            } else {
                buf.append(to);
            }
        }
        
        return buf.toString();
    }
    
    /**
     * Legge la configurazione.
     * 
     * @return Collezione.
     */
    private Map<Integer, String> loadMap() {
        InputStream in = null;        
        SAXParser saxParser;
        Map<Integer, String> map;
                
        map = new HashMap<Integer, String>();
        saxParser = XmlTools.newSAXParser();
        
        try {
            in = ReflectionTools.getResourceAsStream(
                    SpecialCharacterConverter.CONFIG_FILE);
            if (in == null) {
                throw new ResourceNotFoundException(
                        SpecialCharacterConverter.CONFIG_FILE);
            }
            
            saxParser.parse(in, new SpecialCharacterConverter.ParserHandler(
                    map));
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } catch (SAXException ex) {
            throw new XmlException(ex);            
        } finally {
            in = IOTools.close(in);
        }
        
        return map;
    }
    
    /**
     * Parser della configurazione.
     */
    private static final class ParserHandler extends XmlDefaultHandler {
        private final Map<Integer, String> myMap;
        
        /**
         * Costruttore.
         * 
         * @param map Collezione.
         */
        ParserHandler(Map<Integer, String> map) {
            myMap = map; 
            setEntityResolver(new SchemaEntityResolver(
                    "textflat/specialcharacterconverter", 1, 1));
        }
        
        /**
         * Tag di apertura di un elemento.
         * 
         * @param uri       Spazio dei nomi.
         * @param localName Nome locale.
         * @param qName     Nome qualificato.
         * @param attrs     Attributi.
         */
        @Override
        public void startElement(String uri, String localName, String qName,
                Attributes attrs) throws SAXException {
            int codePoint;
            String to;
            Exception ex;
            
            if (localName.equals("char")) {
                codePoint = Integer.parseInt(attrs.getValue("codePoint"), 16);
                to = attrs.getValue("to");                
                if (myMap.put(codePoint, to) != null) {
                    ex = new ObjectDuplicateException(
                            Integer.toHexString(codePoint).toUpperCase());
                    throw new SAXParseException(
                            ApplicationException.toString(ex),
                            getDocumentLocator(), ex);
                }
            }
        }                  
    }
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textflat;

import java.io.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.console.io.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Classe di factory del flusso di scrittura che implementa le
 * funzionalit&agrave; dell&rsquo;applicazione
 * <ACRONYM TITLE="To Flat Text">2FT</ACRONYM> Formatter.
 * 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/textflat/index.html"
 *        TARGET="_top">2FT Formatter</A> 
 * @since 1.1.1
 */
@Final
public class ToFlatTextWriterFactory extends ConsoleOutputStreamWriterFactory {
    private static final int DEF_LINELENMAX = 72;
    private boolean myWordWrap;
    private int myLineLenMax;
    private boolean myKeepNewLines;
    private boolean myConvertSpecialCharacters;
    private boolean myCollapseWhitespaces;
    
    /**
     * Costruttore.
     */
    public ToFlatTextWriterFactory() {        
        myLineLenMax = ToFlatTextWriterFactory.DEF_LINELENMAX;
    }
    
    /**
     * Imposta l&rsquo;indicatore di attivazione della funzione di word-wrap.
     * 
     * @param value Valore.
     * @see         #setLineLenMax
     * @see         #setKeepNewLines
     * @see         it.scoppelletti.programmerpower.io.WordWrapperWriter
     */
    public void setWordWrap(boolean value) {
        myWordWrap = value;
    }
    
    /**
     * Imposta la lunghezza massima delle linee per la funzione di word-wrap.
     *  
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code 72}
     * @see                               #setWordWrap
     */
    @Final
    public void setLineLenMax(int value) {
        if (value <= 0) {
            throw new ArgumentOutOfRangeException("value", value, "]0, ...[");
        }
        
        myLineLenMax = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di mantenimento le interruzioni di linea del
     * testo originale per la funzione di word-wrap.
     * 
     * @param value Indicatore.
     * @see   #setWordWrap
     * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/textflat/wordwrap.html#idKeepNewline"
     *        TARGET="_top">Gestione delle interruzioni di linea originali</A>       
     */
    public void setKeepNewLines(boolean value) {
        myKeepNewLines = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di compressione dei caratteri di spaziatura.
     * 
     * @param value Valore.
     * @see         it.scoppelletti.textflat.WhitespaceCollapserWriter
     */
    @Final
    public void setCollapseWhitespaces(boolean value) {
        myCollapseWhitespaces = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di conversione dei caratteri speciali.
     * 
     * @param value Valore.
     * @see         it.scoppelletti.textflat.SpecialCharacterConverterWriter
     */
    public void setConvertSpecialCharacters(boolean value) {
        myConvertSpecialCharacters = value;
    }
        
    @Override
    protected Writer activateFilters(Writer writer) throws IOException {
        if (myWordWrap) {
            writer = new WordWrapperWriter(writer, myLineLenMax,
                    myKeepNewLines);
        }
        if (myConvertSpecialCharacters) {
            writer = new SpecialCharacterConverterWriter(writer);
        }
        if (myCollapseWhitespaces) {
            writer = new WhitespaceCollapserWriter(writer);
        }
        
        return writer;
    }
}

/*
 * Copyright (C) 2009-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textflat;

import it.scoppelletti.programmerpower.console.*;

/**
 * Opzione per il word-wrap.
 */
final class CliOptionWordWrap extends CliOptionInteger {
    private static final int DEFAULT_LINELENMAX = 72;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */
    CliOptionWordWrap(String name, String resourceBaseName) {
        super(name, resourceBaseName);
        setValueMin(1);
    }    
    
    @Override
    protected final void onValueMissing() {
        setValue(CliOptionWordWrap.DEFAULT_LINELENMAX);
    }    
}

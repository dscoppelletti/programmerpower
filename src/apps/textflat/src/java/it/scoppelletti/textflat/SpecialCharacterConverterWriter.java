/*
 * Copyright (C) 2009-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textflat;

import java.io.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Flusso di filtro in scrittura per la conversione dei caratteri speciali.
 *  
 * @see   it.scoppelletti.textflat.SpecialCharacterConverter
 * @see   <A
 *        HREF="${it.scoppelletti.token.referenceUrl}/textflat/specialchar.html"
 *        TARGET="_top">Conversione dei caratteri speciali</A> 
 * @since 1.0.0
 */
public class SpecialCharacterConverterWriter extends AbstractFilterWriter {
    private SpecialCharacterConverter myConverter;    
    private BufferedWriter myWriter;    
    
    /**
     * Parola in corso di composizione. 
     */
    private StringBuilder myWord;
    
    /**
     * Costruttore.
     * 
     * @param writer Flusso di scrittura sottostante.
     */
    public SpecialCharacterConverterWriter(Writer writer) {
        super(writer);
        
        myConverter = new SpecialCharacterConverter();
        myWriter = new BufferedWriter(writer);        
        setWriter(myWriter);
        myWord = new StringBuilder();
    }

    /**
     * Chiude il flusso.
     */
    @Override
    public void close() throws IOException {
        super.close();
        myConverter = null;       
        myWriter = null;
        myWord = null;
    }
    
    /**
     * Scrive un'interruzione di linea.
     */
    public void newLine() throws IOException {
        checkClosed();
        flushWord();
        myWriter.newLine();
    }  
    
    /**
     * Scrive un carattere.
     * 
     * @param c Carattere.
     */
    @Override
    protected final void writeImpl(char c) throws IOException {
        if (Character.isWhitespace(c)) {
            flushWord();
            myWriter.append(c);
        } else {
            myWord.append(c);
        }
    }         

    /**
     * Scarica sul flusso sottostante gli eventuali caratteri scritti ma non
     * ancora scaricati.
     * 
     * <P>Nel caso si sia cominciato a scrivere una linea di testo non ancora
     * scaricata, tale linea di testo viene scaricata e di seguito viene
     * aggiunta un&rsquo;interruzione di linea.</P> 
     */
    @Override
    public void flush() throws IOException {
        // Se il flusso e' stato chiuso, e' stato anche gia' eseguito il flush e
        // dopo non e' piu' stato possibile scrivere altro e quindi non puo'
        // esserci una parola corrente da scaricare:
        // Non c'e' bisogno di controllare che il flusso sia ancora aperto prima
        // di verificare se c'e' una parola corrente da scaricare.
        if (myWord != null) {
            flushWord();
        }
        
        super.flush();
    }    
    
    /**
     * Scarica la parola corrente.
     */
    private void flushWord() throws IOException {
        if (myWord.length() > 0) {
            myWriter.write(myConverter.convert(myWord.toString()));
            myWord.setLength(0);
        }
    }
}

/*
 * Copyright (C) 2009-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.textflat;

import java.io.*;
import it.scoppelletti.programmerpower.io.*;

/**
 * Flusso di filtro in scrittura per la compressione dei caratteri di
 * spaziatura.
 * 
 * @see <A
 *      HREF="${it.scoppelletti.token.referenceUrl}/textflat/collapsespace.html"
 *      TARGET="_top">Compressione dei caratteri di spaziatura</A>
 * @since 1.0.0
 */
public class WhitespaceCollapserWriter extends AbstractFilterWriter {
    private BufferedWriter myWriter;
    
    /**
     * Numero di caratteri inseriti nella linea corrente.
     */
    private int myLineLen = 0;
    
    /**
     * Indica se un carattere {@code blank} &egrave; in attesa di essere
     * inserito.
     */
    private boolean myBlankPending = false;
        
    /**
     * Indica se l&rsquo;ultimo carattere accodato &egrave; un {@code CR}.
     */
    private boolean myLastCharCR = false;
    
    /**
     * Indica se l&rsquo;ultima linea accodata era una linea vuota.
     */
    private boolean myLastLineEmpty = true;
    
    /**
     * Indica se un&rsquo;interruzione di linea &egrave; in attesa di essere
     * inserita. 
     */
    private boolean myNewLinePending = false;
        
    /**
     * Costruttore.
     * 
     * @param writer Flusso di scrittura sottostante.
     */
    public WhitespaceCollapserWriter(Writer writer) {
        super(writer);
        
        myWriter = new BufferedWriter(writer);
        setWriter(myWriter);
    }

    /**
     * Chiude il flusso.
     */
    @Override
    public void close() throws IOException {
        super.close();
        myWriter = null;
    }

    /**
     * Scrive un'interruzione di linea.
     */
    public void newLine() throws IOException {
        checkClosed();          
        newLineImpl();
    }
    
    /**
     * Scrive un carattere.
     * 
     * @param c Carattere.
     */
    @Override
    protected final void writeImpl(char c) throws IOException {
        if (c == '\n') {
            if (myLastCharCR) {
                // Il NL e' immediatamente seguente ad un CR:
                // Lo ignoro.
                myLastCharCR = false;
            } else {            
                newLineImpl();
            }
            return;
        }
        
        if (c == '\r') {
            newLineImpl();                

            // Il CR potrebbe essere seguito da un NL
            myLastCharCR = true;
            return;
        }
        
        if (Character.isWhitespace(c)) {
            // Inseriro' la spaziatura solo se non sono all'inizio della linea
            if (myLineLen > 0) {
                myBlankPending = true;
            }
            return;
        }
        
        // Carattere non blank
        if (myNewLinePending) {
            doNewLine();
        }
        if (myBlankPending) {
            myWriter.append(' ');
            myBlankPending = false;
            myLineLen++;
        }
        
        myWriter.write(c);
        myLineLen++;
    }
   
    /**
     * Scrive un'interruzione di linea.
     */
    private void newLineImpl() throws IOException {
        if (myLineLen == 0) {
            // Inseriro' l'interruzione di linea solo se non ne ho appena
            // accodata un'altra
            if (!myLastLineEmpty) {
                myNewLinePending = true;
            }
        } else {            
            doNewLine();
        }
    }
    
    /**
     * Scrive un'interruzione di linea.
     */    
    private void doNewLine() throws IOException {
        myWriter.newLine();
        myLineLen = 0;        
        myBlankPending = false;
        myLastCharCR = false;
        myLastLineEmpty = false;
        myNewLinePending = false;        
    }
    
    /**
     * Scarica sul flusso sottostante gli eventuali caratteri scritti ma non
     * ancora scaricati.
     * 
     * <P>Nel caso si sia cominciato a scrivere una linea di testo non ancora
     * scaricata, tale linea di testo viene scaricata e di seguito viene
     * aggiunta un&rsquo;interruzione di linea.</P>
     */
    @Override
    public void flush() throws IOException {
        // Se il flusso e' stato chiuso, e' stato anche gia' eseguito il flush e
        // dopo non e' piu' stato possibile scrivere altro e quindi non puo'
        // esserci una linea corrente da scaricare:
        // Non c'e' bisogno di controllare che il flusso sia ancora aperto prima
        // di verificare se c'e' una linea corrente da scaricare.
        if (myLineLen > 0) {
            doNewLine();
        }
        
        myWriter.flush();
    }    
}

@echo off
rem # Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
rem # 
rem # Licensed under the Apache License, Version 2.0 (the "License");
rem # you may not use this file except in compliance with the License.
rem # You may obtain a copy of the License at
rem # 
rem #     http://www.apache.org/licenses/LICENSE-2.0
rem # 
rem # Unless required by applicable law or agreed to in writing, software
rem # distributed under the License is distributed on an "AS IS" BASIS,
rem # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
rem # See the License for the specific language governing permissions and
rem # limitations under the License.
setlocal
set _EXITCODE=0
if "%PROGRAMMERPOWER_HOME%" == "" set PROGRAMMERPOWER_HOME=%~dp0..
set _APPLNAME=%1
shift
set _APPLVER=%1
shift
if "%_APPLNAME%" == "" goto usage
if "%_APPLVER%" == "" goto usage
set _REPODIR=%PROGRAMMERPOWER_HOME%\modules
set _RUNTIME=%_REPODIR%\it\scoppelletti\runtime\1.1.1\it-scoppelletti-runtime.jar
if not exist "%_RUNTIME%" goto error1
set _ARGS=%1
if "%1"=="" goto launch
shift
:acceptArg
if "%1"=="" goto launch
set _ARGS=%_ARGS% %1
shift
goto acceptArg
:launch
java -jar "%_RUNTIME%" "%_REPODIR%" %_APPLNAME% %_APPLVER% %_ARGS%
set _EXITCODE=%ERRORLEVEL%
:finish
endlocal
exit /b %_EXITCODE%
:usage
echo "usage: %0 applname version [args]"
set _EXITCODE=2
goto finish
:error1
echo "%_RUNTIME% not exists."
set _EXITCODE=1
goto finish

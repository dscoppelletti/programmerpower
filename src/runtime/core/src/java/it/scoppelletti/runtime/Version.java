/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.util.regex.*;

/**
 * Versione.
 *
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/defs.html#idVersion"
 *        TARGET="_top">Versione</A>
 * @since 1.0.0
 */
public final class Version implements Comparable<Version>, Serializable {
    private static final long serialVersionUID = 1;   
    private static final Pattern myFormat = Pattern.compile(
            "([1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)");
    
    /**
     * @serial Numero principale.
     */
    private final int myMajor;
    
    /**
     * @serial Numero secondario.
     */
    private final int myMinor;
    
    /**
     * @serial Numero di revisione.
     */
    private final int myRevision;
    
    /**
     * Costruttore.
     * 
     * @param major    Numero principale.
     * @param minor    Numero secondario.
     * @param revision Numero di revisione.
     */
    public Version(int major, int minor, int revision) {
        if (major < 1) {
            throw new IllegalArgumentException("Argument major < 1.");
        }
        if (minor < 0) {
            throw new IllegalArgumentException("Argument minor < 0.");
        }
        if (revision < 0) {
            throw new IllegalArgumentException("Argument revision < 0.");
        }        
        
        myMajor = major;
        myMinor = minor;
        myRevision = revision;
    }
     
    /**
     * Costruttore.
     *  
     * @param version Versione.
     * 
     * <OL>
     * <LI>Le tre componenti devono essere tutte specificate.
     * <LI>Le tre componenti devono essere numeri interi non segnati e senza
     * cifre 0 non significative.
     * <LI>Il numero principale minimo &egrave; 1; il numero secondario ed il
     * numero di revisione possono anche essere 0.
     * </OL>   
     */
    public Version(String version) {
        String msg;
        Matcher matcher;
        
        if (version == null || version.isEmpty()) {
            throw new NullPointerException("Argument version is null.");
        }
                 
        matcher = myFormat.matcher(version);
        if (!matcher.matches()) {
            msg = String.format("Malformed version \"%1$s\".", version);
            throw new IllegalArgumentException(msg);
        }
        
        myMajor = Integer.parseInt(matcher.group(1));
        myMinor = Integer.parseInt(matcher.group(2));
        myRevision = Integer.parseInt(matcher.group(3));
    }

    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        return new Version(myMajor, myMinor, myRevision);
    }
        
    /**
     * Restituisce il numero principale.
     * 
     * @return Valore.
     */
    public int getMajor() {
        return myMajor;
    }
    
    /**
     * Restituisce il numero secondario.
     * 
     * @return Valore.
     */
    public int getMinor() {
        return myMinor;
    }
    
    /**
     * Restituisce il numero di revisione.
     * 
     * @return Valore.
     */
    public int getRevision() {
        return myRevision;
    }    

    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("%1$d.%2$d.%3$d", myMajor, myMinor, myRevision);
    }   
       
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        Version op;
        
        if (!(obj instanceof Version)) {
            return false;
        }
        
        op = (Version) obj;
        
        if (myMajor != op.getMajor() || myMinor != op.getMinor() ||
                myRevision != op.getRevision()) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myMajor;
        value = 37 * value + myMinor;
        value = 37 * value + myRevision;
        
        return value;
    }

    /**
     * Confronto con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Risultato del confronto.
     */
    public int compareTo(Version obj) {
        if (obj == null) {
            throw new NullPointerException("Argument obj is null.");
        }        

        if (myMajor < obj.getMajor()) {
            return -1;
        }
        if (myMajor > obj.getMajor()) {
            return 1;
        }
        
        if (myMinor < obj.getMinor()) {
            return -1;
        }
        if (myMinor > obj.getMinor()) {
            return 1;
        }
        
        if (myRevision < obj.getRevision()) {
            return -1;
        }
        if (myRevision > obj.getRevision()) {
            return 1;
        }
        
        return 0;
    }           
}


/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.net.*;
import java.util.*;
import org.xml.sax.*;

/**
 * Elemento di una libreria.
 */
final class LibraryEntry {
    private final List<String> myPath;
    private final List<String> mySourcePath;
    private final String mySourceRootPath;
    private final List<String> myDocURL;

    /**
     * Costruttore.
     */
    LibraryEntry() {
       myPath = Collections.<String>emptyList();
       mySourcePath = Collections.<String>emptyList();
       mySourceRootPath = null;
       myDocURL = Collections.<String>emptyList();
    }
    
    /**
     * Costruttore.
     * 
     * @param attrs Attributi.
     * @param base  Elemento di base.
     */
    LibraryEntry(Attributes attrs, LibraryEntry base) {
        if (base == null) {
            throw new NullPointerException("Argument base is null.");
        }
        if (attrs == null) {
            throw new NullPointerException("Argument attrs is null.");
        }
        
        myPath = combinePath(attrs.getValue("jar"), base.getPath());
        mySourcePath = combinePath(attrs.getValue("source"),
                base.getSourcePath());
        mySourceRootPath = combinePath(attrs.getValue("sourceRootPath"),
                base.getSourceRootPath());                        
        myDocURL = combinePath(attrs.getValue("docURL"), base.getDocURL());        
    }
    
    /**
     * Restituisce il percorso del modulo JAR (o del direttorio del codice
     * binario).
     * 
     * @return Collezione. Se il percorso del modulo JAR non &egrave; definito,
     *         restituisce una collezione vuota.
     */    
    List<String> getPath() {
        return myPath;
    }
    
    /**
     * Restituisce il percorso dell&rsquo;archivio del codice sorgente.
     * 
     * @return Collezione. Se il modulo JAR non &egrave; accompagnato dal codice
     *         sorgente, restituisce una collezione vuota.
     */    
    private List<String> getSourcePath() {
        return mySourcePath;
    }
    
    /**
     * Restituisce il percorso radice del codice sorgente all&rsquo;interno
     * dell&rsquo;archivio {@code getSourcePath}.
     * 
     * @return Percorso. Se il percorso radice del codice sorgente deve essere
     *         rilevato automaticamente, restituisce {@code null}.  
     */    
    private String getSourceRootPath() {
        return mySourceRootPath;
    }
    
    /**
     * Restituisce l&rsquo;URL della documentazione API.
     * 
     * @return Collezione. Se il modulo JAR non &egrave; accompagnato dalla
     *         documentazione API, una collezione vuota.
     */    
    private List<String> getDocURL() {
        return myDocURL;
    }   
    
    /**
     * Restituisce gli elementi del class-path.
     * 
     * @param  repo Repository dei moduli.
     * @return      Collezione.
     */
    List<LibraryClasspathEntry> listClasspathEntries(ModuleRepository repo) {
        List<LibraryClasspathEntry> classpath;
        
        classpath = new ArrayList<LibraryClasspathEntry>();
        classpath.add(toLibraryClasspathEntry(repo));
        
        return classpath;
    }
    
    /**
     * Converte l&rsquo;oggetto in un elemento di class-path.
     * 
     * @param  repo Repository dei moduli.
     * @return      Oggetto.
     */
    private LibraryClasspathEntry toLibraryClasspathEntry(
            ModuleRepository repo) {
        String sourceRootPath;
        File path, sourcePath;
        URL docURL;
        LibraryClasspathEntry entry;
        
        path = toFile(myPath, repo);
        sourcePath = toFile(mySourcePath, repo);
        sourceRootPath = mySourceRootPath;
        
        try {
            docURL = toURL(myDocURL);
        } catch (MalformedURLException ex) {
            throw new RuntimeEnvironmentException(ex);
        }
            
        entry = new DefaultClasspathEntry(path, sourcePath, sourceRootPath,
                docURL);
        
        return entry;
    }
        
    /**
     * Combinazione di un percorso.
     * 
     * @param value Valore.
     * @param base  Percorso di base.
     * @return      Percorso.
     */
    private String combinePath(String value, String base) {
        if (value == null) {
            return base;
        }
        
        if (value.isEmpty()) {
            return null;
        }
        
        return value;
    }  
    
    /**
     * Combinazione di un percorso.
     * 
     * @param value Valore.
     * @param base  Percorso di base.
     * @return      Collezione.
     */
    private List<String> combinePath(String value, List<String> base) {
        List<String> list;
        
        if (value == null) {
            return base;
        }
        
        if (value.isEmpty()) {
            return Collections.<String>emptyList();
        }
        
        list = new ArrayList<String>(base);
        list.add(value);
        
        return Collections.<String>unmodifiableList(list);
    }
    
    /**
     * Combina una lista di stringhe in un percorso.
     * 
     * @param  path Lista di stringhe.
     * @param  repo Repository dei moduli. 
     * @return      Percorso.
     */
    private File toFile(List<String> path, ModuleRepository repo) {
        File file = null;
        
        for (String name : path) {
            file = new File(file, name);
        }
        if (file == null) {
            return null;
        }
        
        if (!file.isAbsolute()) {
            file = new File(repo.getBaseDirectory(), file.getPath());
        }
        
        return file;
    }
    
    /**
     * Combina una lista di stringhe in un URL.
     * 
     * @param  path  Lista di stringhe.
     * @param  props Propriet&agrave da sostituire. 
     * @return       Percorso.
     */
    private URL toURL(List<String> path) throws MalformedURLException {
        URL url = null;
        
        for (String name : path) {
            url = new URL(url, name);
        }
        
        return url;
    }    
}

/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.jar.Attributes;
import org.xml.sax.*;

/**
 * Modulo.
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/runtime/module1.xsd}
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/runtime/modulelocal1.xsd}
 * @see   it.scoppelletti.runtime.ModuleRepository
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/repodir.html"
 *        TARGET="_top">Moduli e repository</A>
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/module.html"
 *        TARGET="_top">Metadati dei moduli</A>
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/local.html"
 *        TARGET="_top">Configurazione locale dei moduli</A> 
 * @since 1.0.0
 */
public final class Module {
            
    /**
     * Nome del file dei metadati di un modulo. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String METADATA_FILE = "module.xml";
    
    /**
     * Nome del file di configurazione locale di un modulo. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String LOCAL_FILE = "local.xml";
           
    private static int PROP_MAINCLASSNAME = 0x0001;        
    private final ModuleRepository myRepo;    
    private ModuleIdentity myId;
    private File myBaseDir;
    private ModuleTarget myTarget;
    private String myJarName = null;
    private String mySourceZipName = null;
    private String mySourceRootPath = null;
    private int myPropMask = 0x0000;
    private String myMainClassName;
    private List<ModuleIdentity> myDeps;
    private List<Module> myCompileDeps = null;
    private List<ModuleIdentity> myExts;
    private List<Module> myRuntimeDeps = null;
    private Map<String, String> myLocalProps;
    private Map<String, String> myRuntimeProps = null;
    private List<LibraryEntry> myLibrary;
    
    /**
     * Costruttore.
     *
     * <P>I moduli devono essere istanziati utilizzando un oggetto
     * {@code ModuleRepository}.</P>
     * 
     * @param repository Repository del modulo. 
     */
    Module(ModuleRepository repository) {
        if (repository == null) {
            throw new NullPointerException("Argument repository is null.");
        }

        myRepo = repository;                
        myDeps = new ArrayList<ModuleIdentity>(); 
        myExts = new ArrayList<ModuleIdentity>();
        myLocalProps = new HashMap<String, String>();
        myLibrary = new ArrayList<LibraryEntry>();
    }   
    
    /**
     * Termine dell&rsquo;inizializzazione.
     */
    void endInit() {
        myDeps = Collections.unmodifiableList(myDeps);
        myExts = Collections.unmodifiableList(myExts);
        myLocalProps = Collections.unmodifiableMap(myLocalProps);
        myLibrary = Collections.unmodifiableList(myLibrary);
    }
    
    /**
     * Restituisce l&rsquo;identificatore del modulo. 
     * 
     * @return Valore.
     */
    public ModuleIdentity getIdentity() {
        return myId;
    }
    
    /**
     * Imposta l&rsquo;identificatore del modulo.
     * 
     * @param id Valore.
     */
    void setIdentity(ModuleIdentity id) {
        if (id == null) {
            throw new NullPointerException("Argument id is null.");
        }
        
        myId = id;
    }
    
    /**
     * Restituisce il target.
     * 
     * @return Valore.
     * @since  1.1.0
     */
    public ModuleTarget getTarget() {
        return myTarget;
    }
    
    /**
     * Imposta il target.
     * 
     * @param value Valore.
     */
    void setTarget(ModuleTarget value) {
        if (value == null) {
            throw new NullPointerException("Argument value is null.");
        }
        
        myTarget = value;        
    }
    
    /**
     * Restituisce il repository del modulo.
     * 
     * @return Oggetto.
     */
    public ModuleRepository getRepository() {
        return myRepo;        
    }
    
    /**
     * Restituisce il direttorio di base del modulo.
     * 
     * @return Direttorio (percorso assoluto).
     */
    public File getBaseDirectory() {
        return myBaseDir;
    }
    
    /**
     * Imposta il direttorio di base del modulo.
     * 
     * @param baseDir Direttorio (percorso assoluto).
     */
    void setBaseDirectory(File baseDir) {
        if (baseDir == null) {
            throw new NullPointerException("Argument baseDir is null.");
        }        
        
        myBaseDir = baseDir;
    }
        
    /**
     * Restituisce il nome del modulo JAR.
     * 
     * <P>Il file si deve trovare nel direttorio di base del modulo.</P>
     * 
     * @return Valore. Se il modulo corrisponde ad una libreria composta da 
     *         uno o pi&ugrave; moduli JAR esterni al repository, restituisce
     *         {@code null}.
     * @see    #getBaseDirectory
     */
    public String getArchiveName() {
        return myJarName;
    }
    
    /**
     * Imposta il nome del modulo JAR.
     * 
     * @param value Valore.
     */
    void setArchiveName(String value) {
        if (value == null || value.isEmpty()) {
            throw new NullPointerException("Argument value is null");
        }
        
        myJarName = value;
    }
    
    /**
     * Restituisce il nome dell&rsquo;archivio del codice sorgente.
     * 
     * <P>Il file si deve trovare nel direttorio di base del modulo.</P>
     * 
     * @return Valore. Se il modulo JAR non &egrave; accompagnato dal codice
     *         sorgente, restituisce {@code null}.
     * @see    #getBaseDirectory
     */
    public String getSourceArchiveName() {
        return mySourceZipName;
    }
    
    /**
     * Imposta il nome dell&rsquo;archivio del codice sorgente.
     * 
     * @param value Valore.
     */
    void setSourceArchiveName(String value) {
        mySourceZipName = value;
    }
    
    /**
     * Restituisce il percorso radice del codice sorgente all&rsquo;interno
     * dell&rsquo;archivio {@code getSourceArchiveName}. 
     * 
     * @return Percorso. Se il percorso radice del codice sorgente deve essere
     *         rilevato automaticamente, restituisce {@code null}.         
     * @see    #getSourceArchiveName
     */
    public String getSourceRootPath() {
       return mySourceRootPath;
    }
    
    /**
     * Imposta il percorso radice del codice sorgente all&rsquo;interno
     * dell&rsquo;archivio {@code getSourceArchiveName}. 
     *  
     * @param value Valore.
     */
    void setSourceRootPath(String value) {
        mySourceRootPath = value;
    }
        
    /**
     * Restituisce il nome della classe che implementa l&rsquo;entry-point
     * {@code main} dell&rsquo;applicazione.
     * 
     * @return Valore. Se il modulo non &egrave; un&rsquo;applicazione
     *         eseguibile, restituisce {@code null}.
     */
    public String getMainClassName() {
        if ((myPropMask & Module.PROP_MAINCLASSNAME) != 
                Module.PROP_MAINCLASSNAME) {
            myMainClassName = initMainClassName();
            myPropMask |= Module.PROP_MAINCLASSNAME;
        }
        
        return myMainClassName;
    }
    
    /**
     * Inizializza il nome della classe che implementa l&rsquo;entry-point
     * {@code main} dell&rsquo;applicazione.
     * 
     * @return Valore.
     */
    private String initMainClassName() {
        String className;
        File file;
        URL url;
        JarURLConnection jar;
        Attributes attrs;
        
        if (myTarget != ModuleTarget.JAR) {
            return null;
        }
        
        file = new File(myBaseDir, getArchiveName());
        try {
            url = file.toURI().toURL();        
            url = new URL("jar", "", url.toString() + "!/");
            jar = (JarURLConnection) url.openConnection();
            attrs = jar.getMainAttributes();            
        } catch (IOException ex) {
            throw new RuntimeEnvironmentException(ex);
        }
                
        className = attrs.getValue(Attributes.Name.MAIN_CLASS);
        
        return className;
    }

    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("%1$s (ver. %2$s)", myId.getName(),
                myId.getVersion());
    }  
    
    /**
     * Restituisce gli elementi del class-path.
     * 
     * @return Collezione.
     * @since  1.1.0
     */    
    public List<LibraryClasspathEntry> listClasspathEntries() {
        File path, sourcePath;                
        LibraryClasspathEntry entry;
        List<LibraryClasspathEntry> classpath;
        
        classpath = new ArrayList<LibraryClasspathEntry>();
        
        if (myJarName != null) {
            path = new File(myBaseDir, myJarName);
            if (mySourceZipName != null) {
                sourcePath = new File(myBaseDir, mySourceZipName); 
            } else {
                sourcePath = null;
            }
            
            entry = new DefaultClasspathEntry(path, sourcePath,
                    mySourceRootPath, null);
            classpath.add(entry);
            return classpath;
        }

        for (LibraryEntry libEntry : myLibrary) {
            classpath.addAll(libEntry.listClasspathEntries(myRepo));
        }
        
        return classpath;
    }
    
    /**
     * Aggiunge un elemento di libreria.
     * 
     * @param entry Elemento.
     */
    void addLibraryEntry(LibraryEntry entry) {
        if (entry == null) {
            throw new NullPointerException("Argument entry is null.");
        }
        
        myLibrary.add(entry);
    }
        
    /**
     * Aggiunge la dipendenza da un modulo per la compilazione.
     * 
     * @param module Oggetto.
     */
    void addDependency(ModuleIdentity module) {
        if (module == null) {
            throw new NullPointerException("Argument module is null.");
        }
        
        myDeps.add(module);
    }
    
    /**
     * Restituisce le dipendenze per la compilazione del modulo.
     *
     * <P>Le dipendenze non includono il modulo stesso.</P>
     * 
     * @return Collezione.
     * @see    <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/deps.html#idCompile"
     *         TARGET="_top">Dipendenze per la compilazione di un modulo</A> 
     */    
    public List<Module> getCompileDependencies() {        
        if (myCompileDeps == null) { 
            myCompileDeps = Collections.unmodifiableList(
                    initCompileDependencies());
        }
        
        return myCompileDeps;
    }
    
    /**
     * Inizializza le dipendenze per la compilazione del modulo.
     * 
     * <P>Lo schema XML dei metadati garantisce l&rsquo;univocit&agrave; delle
     * dipendenze per nome del modulo, quindi lo stesso modulo non pu&ograve;
     * comparire pi&ugrave; volte nemmeno con versioni differenti.</P>
     *  
     * @return Collezione.
     */
    private List<Module> initCompileDependencies() {
        String msg;
        Module depModule;
        List<Module> moduleList;

        // Lo schema XML dei metadati garantisce l'univocita' delle dipendenze
        // per nome del modulo, quindi lo stesso modulo non puo' comparire piu'
        // volte nemmeno con versioni differenti.        
        moduleList = new ArrayList<Module>();
        for (ModuleIdentity depId : myDeps) {
            depModule = myRepo.loadModule(depId.getName(), depId.getVersion());
            if (depModule == null) {
                msg = String.format("Module (%1$s) not found.", depId);
                throw new RuntimeEnvironmentException(msg);
            }
            
            moduleList.add(depModule);
        }    
        
        return moduleList;        
    }

    /**
     * Restituisce gli elementi del class-path per la compilazione del modulo.
     * 
     * <P>Il class-path non include gli elementi corrispondenti al modulo
     * stesso.</P>
     * 
     * @return Collezione.
     * @see    #getCompileDependencies
     * @see    #listClasspathEntries 
     * @since  1.1.0
     */
    public List<LibraryClasspathEntry> listCompileClasspathEntries() {
        List<Module> moduleList;
        List<LibraryClasspathEntry> classpath;

        moduleList = getCompileDependencies();        
        classpath = new ArrayList<LibraryClasspathEntry>();
        
        for (Module module : moduleList) {
            classpath.addAll(module.listClasspathEntries());
        }

        return classpath;
    }
    
    /**
     * Aggiunge un modulo di estensione per l&rsquo;esecuzione.
     * 
     * @param module  Modulo.
     * @param locator Localizzatore degli eventi SAX. Pu&ograve; essere
     *                {@code null}.
     */
    void addExtension(ModuleIdentity module, Locator locator) throws
            SAXException {
        String msg;
        
        if (module == null) {
            throw new NullPointerException("Argument module is null.");
        }
        
        for (ModuleIdentity depId :  myDeps) {
            if (module.getName().equals(depId.getName())) {
                msg = String.format(
                        "Module %1$s inserted as both dependency and " +
                        "extension.", module.getName());
                throw new SAXParseException(msg, locator);
            }
        }
        
        myExts.add(module);
    }
    
    /**
     * Restituisce le dipendenze per l&rsquo;esecuzione del modulo.
     *
     * <P>Le dipendenze non includono il modulo stesso.</P>
     * 
     * @return Collezione.
     * @see    <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/deps.html#idRuntime"
     *         TARGET="_top">Dipendenze per l&rsquo;esecuzione di un modulo</A>
     */    
    public List<Module> getRuntimeDependencies() {        
        if (myRuntimeDeps == null) {
           myRuntimeDeps = Collections.unmodifiableList(
                   initRuntimeDependencies());
        }
        
        return myRuntimeDeps;
    }
    
    /**
     * Inizializza le dipendenze per l&rsquo;esecuzione del modulo.
     * 
     * @param  module Modulo.
     * @return        Collezione.       
     */
    private List<Module> initRuntimeDependencies() {
        Module depModule;
        List<Module> moduleList;
        List<ModuleIdentity> depList;
        Map<SymbolicName, ModuleVersionResolver> moduleMap;
        
        moduleList = new ArrayList<Module>();
        moduleMap = new HashMap<SymbolicName, ModuleVersionResolver>();
    
        // Dipendenze dirette
        depList = listDependencies();
        for (ModuleIdentity depId : depList) {
            depModule = loadRuntimeDependency(depId, moduleMap);
            moduleList.add(depModule);
        }    
        
        // Dipendenze indirette
        buildRuntimeDependency(moduleList, moduleMap);
        
        return moduleList;
    }
    
    /**
     * Costruisce la lista delle dipendenze dirette integrata con le estensioni
     * per l&rsquo;esecuzione.
     *  
     * <P>Lo schema XML dei metadati garantisce l&rsquo;univocit&agrave; delle
     * dipendenze per nome del modulo, quindi lo stesso modulo non pu&ograve;
     * comparire pi&ugrave; volte nemmeno con versioni differenti.</P>
     * 
     * <P>Lo schema XML della configurazione locale garantisce
     * l&rsquo;univocit&agrave; delle estensioni per nome del modulo, quindi lo
     * stesso modulo non pu&ograve; comparire pi&ugrave; volte nemmeno con
     * versioni differenti.<BR>
     * Il parser della configurazione locale controlla che un&rsquo;estensione
     * non compaia anche tra le dipendenze n&eacute; con la stessa versione
     * n&eacute; con versione differente.<P>
     *  
     * @return        Collezione.
     */
    private List<ModuleIdentity> listDependencies() {
        List<ModuleIdentity> list;
        
        list = new ArrayList<ModuleIdentity>(myDeps);              
        list.addAll(myExts);
        
        return list;
    }
    
    /**
     * Legge il modulo corrispondente ad una dipendenza che si intende inserire
     * nelle dipendenze per l&rsquo;esecuzione del modulo.
     *  
     * @param  dependencyId Identificatore del modulo da inserire nelle
     *                      dipendenze.
     * @param  moduleMap    Collezione per la risoluzione delle versioni dei
     *                      moduli.     
     * @return              Modulo. Se il modulo &egrave; gi&agrave; stato
     *                      inserito tra le dipendenze, restituisce
     *                      {@code null}.
     */
    private Module loadRuntimeDependency(ModuleIdentity dependencyId,
            Map<SymbolicName, ModuleVersionResolver> moduleMap) {
        String msg;
        Module module;
        ModuleVersionResolver moduleResolver;
        
        if (dependencyId.getName().equals(myId.getName())) {
            msg = String.format("Module %1$s depends on itself.",
                    myId.getName());
            throw new RuntimeEnvironmentException(msg);                
        }
            
        moduleResolver = moduleMap.get(dependencyId.getName());
        if (moduleResolver != null) {
           // Il modulo e' gia' inserito tra le dipendenze
           moduleResolver.checkVersion(dependencyId.getVersion());
           return null;
        }
        
        // Il modulo non e' ancora stato inserito nelle dipendenze
        moduleResolver = myRepo.newModuleVersionResolver(dependencyId.getName(),
                dependencyId.getVersion());
        moduleMap.put(moduleResolver.getName(), moduleResolver);
        
        module = myRepo.loadModule(moduleResolver);
        if (module == null) {
            msg = String.format("Module (%1$s) not found.", dependencyId);
            throw new RuntimeEnvironmentException(msg);
        }
        
        return module;
    }
    
    /**
     * Completa le dipendenze per l&rsquo;esecuzione del modulo con le
     * dipendenze indirette.
     * 
     * @param moduleList Collezione delle dipendenze.
     * @param moduleMap  Collezione per la risoluzione delle versioni dei
     *                   moduli.
     */
    private void buildRuntimeDependency(List<Module> moduleList,
            Map<SymbolicName, ModuleVersionResolver> moduleMap) {
        int i, startLevelIdx, nextLevelIdx;
        Module module, depModule;
        List<ModuleIdentity> depList;
        
        startLevelIdx = 0;
        nextLevelIdx = moduleList.size();
        while (startLevelIdx < nextLevelIdx) {
            for (i = startLevelIdx; i < nextLevelIdx; i++) {
                module = moduleList.get(i);
                depList = module.listDependencies();
                for (ModuleIdentity depId : depList) {
                    depModule = loadRuntimeDependency(depId, moduleMap);
                    if (depModule != null) {
                        moduleList.add(depModule);
                    }
                }            
            }
            
            startLevelIdx = nextLevelIdx;
            nextLevelIdx = moduleList.size();
        }
    }
    
    /**
     * Restituisce gli elementi del class-path per l&rsquo;esecuzione del
     * modulo.
     * 
     * <P>Il class-path include anche gli elementi al modulo stesso a meno che
     * il parametro {@code filter} escluda tale modulo.</P>
     * 
     * @param  filter         Filtro di selezione dei moduli da includere nel
     *                        class-path. Pu&ograve; essere {@code null} per
     *                        selezionare tutti i moduli.
     * @param  includeBaseDir Indica se il class-path deve includere anche i
     *                        direttori di base dei moduli (selezionati dal 
     *                        parametro {@code filter}).                                            
     * @return                Collezione.
     * @see                   #getRuntimeDependencies
     * @see                   #listClasspathEntries
     * @since                 1.1.0
     */
    public List<LibraryClasspathEntry> listRuntimeClasspathEntries(
            ModuleFilter filter, boolean includeBaseDir) {
        boolean includeThis;
        List<Module> moduleList, filteredList;
        List<LibraryClasspathEntry> classpath;
        
        includeThis = (filter == null || filter.accept(this));           
        moduleList = getRuntimeDependencies();
        
        if (filter != null) {
            filteredList = new ArrayList<Module>();
            for (Module module : moduleList) {
                if (filter.accept(module)) {
                    filteredList.add(module);
                }
            }
            moduleList = filteredList;
        }
                        
        classpath = new ArrayList<LibraryClasspathEntry>();

        // Moduli
        if (includeThis) {
            classpath.addAll(listClasspathEntries());
        }
        for (Module module : moduleList) {
            classpath.addAll(module.listClasspathEntries());
        }
        
        if (includeBaseDir) {
            // Direttori di base dei moduli        
            if (includeThis) {
                classpath.add(new DefaultClasspathEntry(myBaseDir, null, null,
                        null));                    
            }        
            for (Module module : moduleList) {
                classpath.add(new DefaultClasspathEntry(
                        module.getBaseDirectory(), null, null, null));                    
            }
        }
        
        return classpath;
    }
    
    /**
     * Aggiunge una propriet&agrave; impostata dalla configurazione locale.
     * 
     * @param key   Chiave.
     * @param value Valore.
     */
    void addLocalProperty(String key, String value) {
        myLocalProps.put(key, value);
    }
    
    /**
     * Restituisce le propriet&agrave; impostate dalla configurazione locale.
     * 
     * @return Collezione. 
     */
    private Map<String, String> getLocalProperties() {
        return myLocalProps;    
    }
    
    /**
     * Restituisce le propriet&agrave; di sistema da impostare per
     * l&rsquo;esecuzione del modulo.
     * 
     * @return Collezione.
     * @see    <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/deps.html#idProps"
     *         TARGET="_top">Propriet&agrave; di sistema per l&rsquo;esecuzione
     *         di un modulo</A> 
     */
    public Map<String, String> getRuntimeProperties() {
        if (myRuntimeProps == null) {
            myRuntimeProps = Collections.unmodifiableMap(
                    initRuntimeProperties());
        }
        
        return myRuntimeProps;
    }
    
    /**
     * Inizializza le propriet&agrave; di sistema da impostare per
     * l&rsquo;esecuzione del modulo.
     * 
     * @return Collezione.
     */
    private Map<String, String> initRuntimeProperties() {
        int i;
        Module module;
        List<Module> moduleList = getRuntimeDependencies();
        Map<String, String> props = new HashMap<String, String>();

        for (i = moduleList.size() - 1; i >= 0; i--) {
            module = moduleList.get(i);
            props.putAll(module.getLocalProperties());            
        }        
        props.putAll(myLocalProps);
        
        return props;
    }   
}

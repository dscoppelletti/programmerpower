/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;

/**
 * Identificatore di un modulo.
 * 
 * <P>Un modulo &egrave; identificato univocamente da una coppia costituita
 * dalle seguenti componenti:</P>
 * 
 * <OL>
 * <LI>Nome simbolico. Normalmente il nome di un modulo coincide con il nome del
 * pacchetto <I>principale</I> implementato dal modulo stesso.
 * <LI>Versione.
 * </OL>
 * 
 * @see   it.scoppelletti.runtime.Module
 * @see   it.scoppelletti.runtime.SymbolicName
 * @see   it.scoppelletti.runtime.Version
 * @since 1.0.0
 */
public final class ModuleIdentity implements Serializable {
    private static final long serialVersionUID = 1;
    
    /**
     * @serial Nome simbolico.
     */
    private final SymbolicName myName;
    
    /**
     * @serial Versione.
     */
    private final Version myVersion;
    
    /**
     * Costruttore.
     * 
     * @param name    Nome simbolico.
     * @param version Versione.
     */
    public ModuleIdentity(SymbolicName name, Version version) {
        if (name == null || name.isEmpty()) {
            throw new NullPointerException("Argument name is null.");
        }
        if (version == null) {
            throw new NullPointerException("Argument version is null.");
        }
        
        myName = name;
        myVersion = version;
    }
    
    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *  
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        return new ModuleIdentity(myName, myVersion);
    }
        
    /**
     * Restituisce il nome simbolico.
     * 
     * @return Valore.
     */
    public SymbolicName getName() {
        return myName;
    }
    
    /**
     * Restituisce la versione.
     * 
     * @return Valore.
     */
    public Version getVersion() {
        return myVersion;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("%1$s, %2$s", myName, myVersion);
    }   
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        ModuleIdentity op;
        
        if (!(obj instanceof ModuleIdentity)) {
            return false;
        }
        
        op = (ModuleIdentity) obj;
        
        if (!myName.equals(op.getName()) ||
                !myVersion.equals(op.getVersion())) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myName.hashCode();
        value = 37 * value + myVersion.hashCode();
        
        return value;
    }    
}

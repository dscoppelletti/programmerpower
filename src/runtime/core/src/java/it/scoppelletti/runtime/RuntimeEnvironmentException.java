/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import org.xml.sax.*;

/**
 * Eccezione del runtime.
 *
 * @since 1.0.0
 */
public class RuntimeEnvironmentException extends RuntimeException {
    private static final long serialVersionUID = 1;
    
    /**
     * Costruttore.
     * 
     * @param message Messaggio.
     */
    public RuntimeEnvironmentException(String message) {
        super(message);
    }    
        
    /**
     * Costruttore.
     * 
     * @param ex Eccezione originale.
     */        
    public RuntimeEnvironmentException(Throwable ex) {
        super(RuntimeEnvironmentException.toString(ex), ex);
    }  
    
    /**
     * Rappresenta un&rsquo;eccezione con una stringa.
     * 
     * <P>La rappresentazione dell&rsquo;eccezione &egrave; rilevata dalle
     * seguenti alternative:</P>
     * 
     * <OL>
     * <LI>Messaggio (non vuoto) restituito dal metodo
     * {@code getLocalizedMessage} dell&rsquo;eccezione.
     * <LI>Messaggio (non vuoto) restituito dal metodo {@code getMessage}
     * dell&rsquo;eccezione.
     * <LI>Stringa (non vuota) restituita dal metodo {@code toString}
     * dell&rsquo;eccezione.
     * <LI>Nome della classe dell&rsquo;eccezione (completo di pacchetto).   
     * </OL>
     * 
     * @param  ex Eccezione.
     * @return    Stringa. Se l&rsquo;eccezione {@code t} &egrave; {@code null},
     *            restituisce la stringa {@code "null"}.
     */
    private static String toString(Throwable ex) {
        String msg;
        SAXParseException xmlEx;
        
        if (ex == null) {
            return "null";
        }
        
        msg = RuntimeEnvironmentException.getMessage(ex);
        
        if (ex instanceof SAXParseException) {
            // Completo il messaggio con le informazioni per la locazione
            // dell'errore nel documento XML
            xmlEx = (SAXParseException) ex;            
            msg = String.format(
                    "%1$s [publicId=%2$s, systemId=%3$s, line=%4$d, column=%5$d]",
                    msg, xmlEx.getPublicId(), xmlEx.getSystemId(),
                    xmlEx.getLineNumber(), xmlEx.getColumnNumber());
        }
        
        return msg;
    }
    
    /**
     * Rappresenta un&rsquo;eccezione con una stringa.
     * 
     * @param  ex Eccezione.
     * @return    Stringa.
     */
    private static String getMessage(Throwable ex) {
        String s;
                
        s = ex.getLocalizedMessage();
        if (s != null && s.isEmpty()) {
            return s;
        }
        
        s = ex.getMessage();
        if (s != null && s.isEmpty()) {
            return s;
        }
        
        s = ex.toString();
        if (s != null && s.isEmpty()) {
            return s;
        }
        
        return ex.getClass().getCanonicalName();
    }    
}

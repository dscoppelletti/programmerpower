/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.util.*;
import org.xml.sax.*;

/**
 * Parser di un repository.
 */
final class ModuleRepositoryParser extends XmlDefaultHandler {
    private static final String VER1_ID =
        "http://www.scoppelletti.it/res/runtime/repository1.xsd";
    private static final String VER1_RES = "repository1.xsd";
    private static final String VER2_ID =
        "http://www.scoppelletti.it/res/runtime/repository2.xsd";
    private static final String VER2_RES = "repository2.xsd";    
    private final ModuleRepository myRepo;
    private SymbolicName myCurrentModule = null;    
    private Map<Version, Version> myCurrentVersionRedirs = null;
    
    /**
     * Costruttore.
     * 
     * @param factory      Oggetto di factory del repository dei moduli.
     * @param errorHandler Gestore degli errori SAX. Pu&ograve; essere
     *                     {@code null}. 
     */
    ModuleRepositoryParser(ModuleRepositoryFactory factory,
            ErrorHandler errorHandler) {
        super(errorHandler);
        
        if (factory == null) {
            throw new NullPointerException("Argument factory is null.");
        }        
        
        myRepo = new ModuleRepository(factory);
    }
    
    /**
     * Restituisce il repository.
     * 
     * @return Oggetto.
     */
    ModuleRepository getRepository() {
        return myRepo;
    }
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {        
        if (myRepo.isVersionRedirecting() && localName.equals("module")) {
            startElementModule(attrs);            
        } else if (myRepo.isVersionRedirecting() &&
                localName.equals("version")) {
            startElementVersion(attrs);
        }           
    }  
    
    /**
     * Tag di chiusura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws
            SAXException {        
        if (localName.equals("repository")) {
            endElementRepository();
        } else if (myRepo.isVersionRedirecting() &&
                localName.equals("module")) {
            endElementModule();
        }
    }  
    
    /**
     * Tag di chiusura dell&rsquo;elemento {@code <repository>}.     
     */
    private void endElementRepository() {
        myRepo.endInit();
    }
       
    /**
     * Tag di apertura di un elemento {@code <module>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementModule(Attributes attrs) {
        // Il controllo di univocita' del nome del modulo e' gia' implementato
        // dallo schema                
        myCurrentModule = new SymbolicName(attrs.getValue("name"));
        myCurrentVersionRedirs = new HashMap<Version, Version>();
    }
    
    /**
     * Tag di chiusura di un elemento {@code <module>}.     
     */
    private void endElementModule() {
        myRepo.putModuleVersionRedirections(myCurrentModule,
                myCurrentVersionRedirs);        
        myCurrentModule = null;
        myCurrentVersionRedirs = null;
    }

    /**
     * Tag di apertura di un elemento {@code <version>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementVersion(Attributes attrs) throws SAXException {
        String msg;
        Version fromVer, toVer;
        
        fromVer = new Version(attrs.getValue("from"));
        toVer = new Version(attrs.getValue("to"));
        
        if (myCurrentVersionRedirs.put(fromVer, toVer) != null) {
            // Dalla versione 2 dello schema, il controllo di univocita' delle
            // versioni redirezionate e' gia' implementato dallo schema; devo
            // comunque collezionare le redirezioni, quindi lascio anche il
            // controllo.
            msg = String.format(
                    "Duplicate version %2$s redirection for module %1$s.",
                    myCurrentModule, fromVer);
            throw new SAXParseException(msg, getDocumentLocator());
        }
    }
    
    /**
     * Risolve un&rsquo;entit&agrave;.
     *
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Flusso di lettura dell&rsquo;entit&agrave;.
     */
    public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException {
        String msg, name;
        InputStream stream;
        
        if (ModuleRepositoryParser.VER1_ID.equals(systemId)) {
            name = ModuleRepositoryParser.VER1_RES;
        } else if (ModuleRepositoryParser.VER2_ID.equals(systemId)) {
            name = ModuleRepositoryParser.VER2_RES;
        } else {
            msg = String.format("Bad schema (publicId=%1$s, systemId=%2$s).",
                    publicId, systemId);
            throw new SAXException(msg);
        }
        
        stream = getClass().getResourceAsStream(name);
        if (stream == null) {
            msg = String.format("Resource %1$s not found.", name);
            throw new SAXException(msg);
        }
        
        return new InputSource(stream);
    }    
}

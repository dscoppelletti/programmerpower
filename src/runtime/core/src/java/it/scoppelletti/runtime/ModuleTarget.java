package it.scoppelletti.runtime;

/**
 * Target di un modulo.
 * 
 * @since 1.1.0
 */
public enum ModuleTarget {
    
    /**
     * Modulo Java.
     */
    JAR,
    
    /**
     * Applicazione Web.
     */
    WAR,
    
    /**
     * Libreria.
     */
    LIB       
}

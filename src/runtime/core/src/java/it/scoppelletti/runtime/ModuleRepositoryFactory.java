/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

/**
 * Classe di factory dei repository di moduli.
 *
 * @see   it.scoppelletti.runtime.Module
 * @see   it.scoppelletti.runtime.ModuleRepository
 * @since 1.0.0
 */
public final class ModuleRepositoryFactory {
    private final SAXParser myParser;
    private File myBaseDir;    
    private ErrorHandler myErrorHandler;
    private boolean myVersionRedirecting;
    
    /**
     * Costruttore.
     */
    public ModuleRepositoryFactory() {
        myParser = XmlUtils.newSAXParser();
        myBaseDir = null;
        myErrorHandler = null;
        myVersionRedirecting = false;                               
    }
    
    /**
     * Restituisce il direttorio di base.
     * 
     * @return Direttorio.
     * @see    #setBaseDirectory
     * @see    it.scoppelletti.runtime.ModuleRepository#getBaseDirectory
     */
    public File getBaseDirectory() {
        return myBaseDir;
    }
    
    /**
     * Imposta il direttorio di base.
     *
     * @param                        dir Direttorio.
     * @it.scoppelletti.tag.required
     * @see                              #getBaseDirectory
     */
    public void setBaseDirectory(File dir) {
        myBaseDir = dir;
    }
    
    /**
     * Indica se la redirezione delle versioni &egrave; abilitata.
     * 
     * @return Indicatore.
     * @see    #setVersionRedirecting
     * @see    it.scoppelletti.runtime.ModuleRepository#isVersionRedirecting
     */
    public boolean isVersionRedirecting() {
        return myVersionRedirecting;
    }
    
    /**
     * Imposta l&rsquo;indicatore di redirezione delle versioni abilitata.
     * 
     * <P>Normalmente la redirezione delle versioni viene abilitata per
     * l&rsquo;ambiente di esecuzione delle applicazioni, mentre viene mantenuta
     * disabilitata nell&rsquo;ambiente di sviluppo.</P>
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #isVersionRedirecting
     */
    public void setVersionRedirecting(boolean value) {
        myVersionRedirecting = value;
    }
    
    /**
     * Restituisce il gestore degli errori SAX.
     * 
     * @return Gestore. Pu&ograve; essere {@code null}.
     * @see    #setErrorHandler
     */
    public ErrorHandler getErrorHandler() {
        return myErrorHandler;
    }
    
    /**
     * Imposta il gestore degli errori SAX.
     * 
     * <P>Se non viene impostato un gestore degli errori (o viene impostato
     * {@code null}), viene utilizzato un gestore predefinito che reinoltra
     * come eccezioni tutti gli errori (ed anche gli avvisi) rilevati dal
     * parser.</P>
     *  
     * @param handler Gestore. Pu&ograve; essere {@code null}.
     * @see           #getErrorHandler
     */
    public void setErrorHandler(ErrorHandler handler) {
        myErrorHandler = handler;
    }

    /**
     * Istanzia un repository di moduli utilizzando i parametri impostati.
     * 
     * @return Repository.
     * @see    #setBaseDirectory
     * @see    #setErrorHandler
     */
    public ModuleRepository newRepository() {
        File file;
        ModuleRepositoryParser handler;
        
        if (myBaseDir == null) {
            throw new NullPointerException("Reference myBaseDir is null.");
        }
 
        handler = new ModuleRepositoryParser(this, myErrorHandler);
        file = new File(handler.getRepository().getBaseDirectory(),
                ModuleRepository.METADATA_FILE);
        try {
            myParser.parse(file, handler);
        } catch (IOException ex) {
            throw new RuntimeEnvironmentException(ex);
        } catch (SAXException ex) {
            throw new RuntimeEnvironmentException(ex);
        }
        
        return handler.getRepository();
    }
}

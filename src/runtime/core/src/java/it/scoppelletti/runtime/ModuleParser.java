/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.util.*;
import org.xml.sax.*;

/**
 * Parser di un modulo.
 */
final class ModuleParser extends XmlDefaultHandler {
    private static final String VER1_ID =
        "http://www.scoppelletti.it/res/runtime/module1.xsd";
    private static final String VER1_RES = "module1.xsd"; 
    private static final String VER2_ID =
        "http://www.scoppelletti.it/res/runtime/module2.xsd";
    private static final String VER2_RES = "module2.xsd";    
    private final Module myModule;
    private int myModuleCount = 0;
    private Deque<LibraryEntry> myGroupStack = null; 
    
    /**
     * Costruttore.
     * 
     * @param repo         repository dei moduli.
     * @param errorHandler Gestore degli errori SAX. Pu&ograve; essere
     *                     {@code null}.
     */
    ModuleParser(ModuleRepository repository, ErrorHandler errorHandler) {
        super(errorHandler);
        
        if (repository == null) {
            throw new NullPointerException("Argument repository is null.");
        }        
        
        myModule = new Module(repository);        
    }
    
    /**
     * Restituisce il modulo.
     * 
     * @return Oggetto.
     */
    Module getModule() {
        return myModule;
    }
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {
        if (localName.equals("module")) {
            switch (myModuleCount) {
            case 0:
                myModuleCount++;
                startElementModule(attrs);
                break;
                
            case 1:
                myModuleCount++;
                startElementIdentity(attrs);
                break;
                
            default:
                startElementDependency(attrs);
            }          
        } else if (localName.equals("target")) {
            startElementTarget();
        } else if (localName.equals("classpath")) {
            if (myGroupStack == null) {
                startElementClasspath(attrs);
            } else {
                startElementLibraryClasspath(attrs);
            }
        } else if (localName.equals("library")) {
            startElementLibrary();
        } else if (localName.equals("group")) {
            startElementLibraryGroup(attrs);         
        }
    }  

    /**
     * Tag di chiusura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws
            SAXException {        
        if (localName.equals("target")) {
            endElementTarget();
        } else if (localName.equals("group")) {
            endElementLibraryGroup();
        }
    }  
    
    /**
     * Tag di apertura dell&rsquo;elemento {@code <module>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementModule(Attributes attrs) {
        int version;
        
        version = Integer.parseInt(attrs.getValue("version"));
        if (version < 2) { 
            myModule.setTarget(ModuleTarget.JAR);
        }
    }
    
    /**
     * Tag di apertura dell&rsquo;elemento {@code <module>/<module>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementIdentity(Attributes attrs) {
        ModuleIdentity id;
        
        id = new ModuleIdentity(new SymbolicName(attrs.getValue("name")),
                new Version(attrs.getValue("version")));
        myModule.setIdentity(id);
    }
       
    /**
     * Tag di apertura dell&rsquo;elemento {@code <target>}.
     */
    private void startElementTarget() {
        collectContent();
    }
    
    /**
     * Tag di chiusura dell&rsquo;elemento {@code <target>}.
     */
    private void endElementTarget() {
        String value;
        ModuleTarget target;
        
        value = getCollectedContent();
        target = Enum.valueOf(ModuleTarget.class, value.toUpperCase());
        myModule.setTarget(target);
    }
    
    /**
     * Tag di apertura dell&rsquo;elemento {@code <module>/<classpath>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementClasspath(Attributes attrs) throws SAXException {
        String name, msg;
        File file;
        
        if (myModule.getTarget() == ModuleTarget.LIB) {
            msg = String.format("Module [%1$s] is a library.", myModule);
            throw new SAXParseException(msg, getDocumentLocator());                 
        }
        
        name = attrs.getValue("jar");
        file = new File(name);
        if (file.getParentFile() != null) {
            msg = String.format(
                    "File name \"%1$s\" specifies parent directory.", name);
            throw new SAXParseException(msg, getDocumentLocator());
        }
        myModule.setArchiveName(name);
        
        name = attrs.getValue("source");
        if (name != null && !name.isEmpty()) {
            file = new File(name);
            if (file.getParentFile() != null) {
                msg = String.format(
                    "File name \"%1$s\" specifies parent directory.", name);
                throw new SAXParseException(msg, getDocumentLocator());
            }
        } else {
            name = null;
        }
        myModule.setSourceArchiveName(name);
        
        name = attrs.getValue("sourceRootPath");
        if (name != null && !name.isEmpty()) {
            if (myModule.getSourceArchiveName() == null) {
                throw new SAXParseException(
                        "sourceRootPath attribute without source attribute.",
                        getDocumentLocator());
            }
        } else {
            name = null;
        }
        myModule.setSourceRootPath(name);
    }
    
    /**
     * Tag di apertura di un elemento {@code <dependencies>/<module>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementDependency(Attributes attrs) throws SAXException {
        String msg;
        SymbolicName name;
        ModuleIdentity id;
        
        name = new SymbolicName(attrs.getValue("name"));
        if (name.equals(myModule.getIdentity().getName())) {
            msg = String.format("Module %1$s depends on itself.", name);
            throw new SAXParseException(msg, getDocumentLocator());            
        }
        
        id = new ModuleIdentity(name, new Version(attrs.getValue("version")));
        myModule.addDependency(id);
    }
    
    /**
     * Tag di apertura dell&rsquo;elemento {@code <library>}. 
     */
    private void startElementLibrary() throws SAXException {
        String msg;
        
        if (myModule.getTarget() != ModuleTarget.LIB) {
            msg = String.format("Module [%1$s] is not a library.", myModule);
            throw new SAXParseException(msg, getDocumentLocator());                 
        }
        
        myGroupStack = new ArrayDeque<LibraryEntry>();
        myGroupStack.push(new LibraryEntry());                    
    }
    
    /**
     * Tag di apertura di un elemento {@code <library>//<group>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementLibraryGroup(Attributes attrs) {
        LibraryEntry baseGroup;
        
        baseGroup = myGroupStack.peek();                
        myGroupStack.push(new LibraryEntry(attrs, baseGroup));                            
    }
        
    /**
     * Tag di chiusura di un elemento {@code <library>//<group>}. 
     */
    private void endElementLibraryGroup() {
        myGroupStack.pop();
    }
    
    /**
     * Tag di apertura dell&rsquo;elemento {@code <library>//<classpath>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementLibraryClasspath(Attributes attrs) throws
            SAXException {
        LibraryEntry baseGroup, entry;
        
        baseGroup = myGroupStack.peek();
        entry = new LibraryEntry(attrs, baseGroup);
        if (entry.getPath().isEmpty()) {
            throw new SAXParseException("Undefined path in library entry.",
                    getDocumentLocator());            
        }
            
        myModule.addLibraryEntry(entry);
    }
        
    /**
     * Risolve un&rsquo;entit&agrave;.
     *
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Flusso di lettura dell&rsquo;entit&agrave;.
     */
    public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException {
        String msg, name;
        InputStream stream;
        
        if (ModuleParser.VER2_ID.equals(systemId)) {
            name = ModuleParser.VER2_RES;
        } else if (ModuleParser.VER1_ID.equals(systemId)) {
            name = ModuleParser.VER1_RES;
        } else {
            msg = String.format("Bad schema (publicId=%1$s, systemId=%2$s).",
                    publicId, systemId);
            throw new SAXException(msg);
        }
        
        stream = getClass().getResourceAsStream(name);
        if (stream == null) {
            msg = String.format("Resource %1$s not found.", name);
            throw new SAXException(msg);
        }
        
        return new InputSource(stream);
    }    
}

/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * Nome simbolico.
 * 
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/defs.html#idSymbolicName"
 *        TARGET="_top">Nome simbolico</A>
 * @since 1.0.0
 */
public final class SymbolicName implements Serializable {
    private static final long serialVersionUID = 1;
    private static final char SEPARATOR = '.';
    private static final Pattern myFormat = Pattern.compile(
            "[\\p{Lower}_][\\p{Lower}\\d_]*(\\.[\\p{Lower}_][\\p{Lower}\\d_]*)*");
    
    /**
     * @serial Nome.
     */
    private final String myName;
    
    /**
     * Costruttore.
     *
     * @param name Nome.
     */
    public SymbolicName(String name) {
        String msg;
        Matcher matcher;
        
        if (name == null || name.isEmpty()) {
            myName = "";
            return;
        }
        
        matcher = myFormat.matcher(name);
        if (!matcher.matches()) {
            msg = String.format("Malformed symbolic name \"%1$s\".", name);
            throw new IllegalArgumentException(msg);
        }

        myName = name;         
    }

    /**
     * Sostituisce l&rsquo;istanza deserializzata.
     * 
     * <P>Il metodo {@code readResolve} &egrave; utilizzato per validare
     * l&rsquo;oggetto deserializzato.</P>
     *
     * @return Oggetto.
     */
    private Object readResolve() throws ObjectStreamException {
        return new SymbolicName(myName);
    }
    
    /**
     * Verifica se il nome &egrave; vuoto.
     * 
     * @return Esito della verifica.
     */
    public boolean isEmpty() {
        return myName.isEmpty();
    }
    
    /**
     * Scompone il nome in segmenti.
     * 
     * @return Collezione.
     */
    public List<String> listSegments() {
        int beginIndex, endIndex;
        String segment;
        List<String> list = new ArrayList<String>();
       
        beginIndex = 0;
        endIndex = myName.indexOf(SymbolicName.SEPARATOR);
        while (endIndex > 0) {
            segment = myName.substring(beginIndex, endIndex);
            list.add(segment);
            beginIndex = endIndex + 1;
            endIndex = myName.indexOf(SymbolicName.SEPARATOR, beginIndex);
        }
        segment = myName.substring(beginIndex);
        list.add(segment);
        
        return list;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return myName;
    }  
    
    /**
     * Costruisce un percorso del file system nel quale la sequenza dei nomi di
     * livello corrisponde alla sequenza dei segmenti del nome simbolico. 
     * 
     * <P>Ad esempio, per la piattaforma UNIX, al nome simbolico
     * {@code "it.scoppelletti.games.billiard"}, considerando il direttorio
     * parent {@code /opt/programmerpower}, corrisponde il percorso del file
     * system {@code /opt/programmerpower/it/scoppelletti/games/billiard}.</P>
     *  
     * @param parent Percorso del direttorio parent al quale viene accodato il
     *               percorso corrispondente al nome simbolico. Se {@code null},
     *               non inserisce nessun direttorio parent. 
     * @return       Percorso.
     */
    public File toFile(File parent) {
        File file = parent;
        
        for (String segment : listSegments()) {
            file = new File(file, segment);
        }
        
        return file;
    }
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    @Override
    public boolean equals(Object obj) {
        SymbolicName op;
        
        if (!(obj instanceof SymbolicName)) {
            return false;
        }
        
        op = (SymbolicName) obj;
        
        if (!myName.equals(op.toString())) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    @Override
    public int hashCode() {
        int value = 17;
        
        value = 37 * value + myName.hashCode();

        return value;
    }
}

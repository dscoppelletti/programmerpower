/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import javax.xml.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

/**
 * Funzioni di utilit&agrave;
 * <ACRONYM TITLE="eXtensible Markup Language">XML</ACRONYM>.
 */
final class XmlUtils {
    private static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    
    /**
     * Costruttore privato per classe statica.
     */
    private XmlUtils() {        
    }
    
    /**
     * Istanzia un parser SAX.
     * 
     * <P>Il metodo {@code newSAXParser} configura il parser SAX restituito
     * con le seguenti caratteristiche:</P>
     * 
     * <OL>
     * <LI>Supporto degli spazi dei nomi.
     * <LI>Validazione rispetto allo schema XML riferito dall&rsquo;attributo
     * {@code schemaLocation} (o {@code noNamespaceSchemaLocation}
     * dell&rsquo;elemento radice del documento. 
     * </OL>
     * 
     * @return Oggetto.
     */
    static SAXParser newSAXParser() {
        SAXParserFactory saxFactory;
        SAXParser saxParser;
        
        saxFactory = SAXParserFactory.newInstance();
        saxFactory.setNamespaceAware(true);
        saxFactory.setValidating(true);
        
        try {
            saxParser = saxFactory.newSAXParser();            
            saxParser.setProperty(XmlUtils.JAXP_SCHEMA_LANGUAGE,
                    XMLConstants.W3C_XML_SCHEMA_NS_URI);            
        } catch (ParserConfigurationException ex) {
            throw new RuntimeEnvironmentException(ex);
        } catch (SAXException ex) {
            throw new RuntimeEnvironmentException(ex);
        }        
                
        return saxParser;        
    }
}

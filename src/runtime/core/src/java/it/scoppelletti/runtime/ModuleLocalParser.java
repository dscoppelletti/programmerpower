/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import org.xml.sax.*;

/**
 * Parser di un modulo.
 */
final class ModuleLocalParser extends XmlDefaultHandler {
    private static final String VER1_ID =
        "http://www.scoppelletti.it/res/runtime/modulelocal1.xsd";
    private static final String VER1_RES = "modulelocal1.xsd"; 
    private final Module myModule;
    private int myModuleCount = 0;
    private String myCurrentProp = null;
    private StringBuilder myContentBuffer = null;
    
    /**
     * Costruttore.
     * 
     * @param module       Modulo.
     * @param errorHandler Gestore degli errori SAX. Pu&ograve; essere
     *                     {@code null}.
     */
    ModuleLocalParser(Module module, ErrorHandler errorHandler) {
        super(errorHandler);
        
        if (module == null) {
            throw new NullPointerException("Argument module is null.");
        }        
        
        myModule = module;
    }
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {
        if (localName.equals("module")) {
            switch (myModuleCount) {
            case 0:
                // Nessuna elaborazione sulla radice
                myModuleCount++;
                break;
                
            default:
                startElementExtension(attrs);
            }                                 
        } else if (localName.equals("entry")) {
            startElementProperty(attrs);
        }   
    }  
             
    /**
     * Tag di chiusura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws
            SAXException {        
        if (localName.equals("entry")) {
            endElementProperty();
        }
    }  
    
    /**
     * Tag di apertura di un elemento {@code <extensions>/<module>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementExtension(Attributes attrs) throws SAXException {
        String msg;
        SymbolicName name;
        ModuleIdentity id;
        
        name = new SymbolicName(attrs.getValue("name"));
        if (name.equals(myModule.getIdentity().getName())) {
            msg = String.format("Module %1$s depends on itself.", name);
            throw new SAXParseException(msg, getDocumentLocator());            
        }
        
        id = new ModuleIdentity(name, new Version(attrs.getValue("version")));
        myModule.addExtension(id, getDocumentLocator());
    }
    
    /**
     * Tag di apertura di un elemento {@code <properties>/<entry>}.
     * 
     * @param attrs Attributi.
     */
    private void startElementProperty(Attributes attrs) {
        myCurrentProp = attrs.getValue("key");
        myContentBuffer = new StringBuilder();
    }
    
    /**
     * Tag di chiusura di un elemento {@code <properties>/<entry>}.
     */
    private void endElementProperty() {
        myModule.addLocalProperty(myCurrentProp, myContentBuffer.toString());
        myCurrentProp = null;
        myContentBuffer = null;
    }
    
    /**
     * Caratteri nel contenuto di un elemento.
     * 
     * @param ch     Buffer del testo.
     * @param start  Indice del primo carattere del testo nel vettore
     *               {@code ch}.
     * @param length Numero di caratteri del testo.
     */
    @Override
    public void characters(char[] ch, int start, int length) throws
            SAXException {
        if (myContentBuffer != null) {
            myContentBuffer.append(ch, start, length);
        }
    }
    
    /**
     * Caratteri di spaziatura nel contenuto di un elemento.
     * 
     * <P>Alcuni parser possono utilizzare l&rsquo;evento
     * {@code ignorableWhitespace} anzich&acute; l&rsquo;evento 
     * {@code characters} per rilevare i caratteri di spaziatura presenti nel
     * testo nel contenuto degli elementi; per questo motivo, questa
     * implementazione del metodo {@code ignorableWhitespace} delega la
     * gestione dei caratteri di spaziatura allo stesso metodo
     * {@code characters}.</P>
     *  
     * @param ch     Buffer del testo.
     * @param start  Indice del primo carattere del testo nel vettore
     *               {@code ch}.
     * @param length Numero di caratteri del testo.
     */
    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws
            SAXException {
        characters(ch, start, length);        
    }
    
    /**
     * Risolve un&rsquo;entit&agrave;.
     *
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Flusso di lettura dell&rsquo;entit&agrave;.
     */
    public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException {
        String msg, name;
        InputStream stream;
        
        if (ModuleLocalParser.VER1_ID.equals(systemId)) {
            name = ModuleLocalParser.VER1_RES;
        } else {
            msg = String.format("Bad schema (publicId=%1$s, systemId=%2$s).",
                    publicId, systemId);
            throw new SAXException(msg);
        }
        
        stream = getClass().getResourceAsStream(name);
        if (stream == null) {
            msg = String.format("Resource %1$s not found.", name);
            throw new SAXException(msg);
        }
        
        return new InputSource(stream);
    }    
}

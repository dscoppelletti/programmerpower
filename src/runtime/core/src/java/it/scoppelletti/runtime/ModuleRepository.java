/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

/**
 * Repository dei moduli.
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/runtime/repository1.xsd}
 * @see it.scoppelletti.runtime.Module
 * @see it.scoppelletti.runtime.ModuleRepositoryFactory
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/repodir.html"
 *      TARGET="_top">Moduli e repository</A>
 * @see <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/repository.html"
 *      TARGET="_top">Metadati del repository</A>        
 * @since 1.0.0
 */
public final class ModuleRepository {
    
    /**
     * Nome del file dei metadati di un repository. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final String METADATA_FILE = "repository.xml";
           
    private final File myBaseDir;       
    private final boolean myVersionRedirecting;
    private final ErrorHandler myErrorHandler; 
    private final SAXParser myParser;
    private Map<SymbolicName, Map<Version, Version>> myVersionRedirs;
    
    /**
     * Costruttore.
     *  
     * <P>I repository devono essere istanziati utilizzando un oggetto
     * {@code ModuleRepositoryFactory}.</P>
     * 
     * @param factory Oggetto di factory.                              
     */
    ModuleRepository(ModuleRepositoryFactory factory) {
        if (factory == null) {
            throw new NullPointerException("Argument factory is null.");
        }        
            
        myBaseDir = factory.getBaseDirectory().getAbsoluteFile();
        myVersionRedirecting = factory.isVersionRedirecting();
        myErrorHandler = factory.getErrorHandler();
        myParser = XmlUtils.newSAXParser();
        myVersionRedirs = new HashMap<SymbolicName, Map<Version, Version>>();        
    }

    /**
     * Termine dell&rsquo;inizializzazione.
     */
    void endInit() {
        myVersionRedirs = Collections.unmodifiableMap(myVersionRedirs);
    }
    
    /**
     * Imposta le redirezioni delle versioni configurate per un modulo.
     * 
     * @param name                Nome del modulo.
     * @param versionRedirections Redirezioni delle versioni.
     */
    void putModuleVersionRedirections(SymbolicName name,
            Map<Version, Version> versionRedirections) {
        if (name == null || name.isEmpty()) {
            throw new NullPointerException("Argument name is null.");
        }
        if (versionRedirections == null) {
            throw new NullPointerException(
                    "Argument versionRedirections is null.");
        }
        
        myVersionRedirs.put(name,
                Collections.unmodifiableMap(versionRedirections));
    }
    
    /**
     * Restituisce il direttorio di base.
     * 
     * @return Direttorio (percorso assoluto).
     */
    public File getBaseDirectory() {
        return myBaseDir;
    }

    /**
     * Indica se la redirezione delle versioni &egrave; abilitata.
     * 
     * @return Indicatore.
     */
    public boolean isVersionRedirecting() {
        return myVersionRedirecting;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("ModuleRepository(%1$s)", myBaseDir.getPath());
    }  
    
    /**
     * Legge un modulo.
     *  
     * @param  name    Nome del modulo.
     * @param  version Versione del modulo. La versione del modulo
     *                 effettivamente letta pu&ograve; essere differente da
     *                 quella specificata per effetto del redirezionamento delle
     *                 versioni (se abilitato).
     * @return         Oggetto. Se il modulo non &egrave; presente nel
     *                 repository, restituisce {@code null}.         
     * @see            #isVersionRedirecting 
     */
    public Module loadModule(SymbolicName name, Version version) {
        ModuleVersionResolver moduleResolver;
        
        moduleResolver = newModuleVersionResolver(name, version);
        
        return loadModule(moduleResolver);        
    }
    
    /**
     * Legge un modulo.
     * 
     * <P>Questo sovraccarico del metodo {@code loadModule} pu&ograve; essere
     * utilizzato per la lettura di un modulo da una struttura personalizzata 
     * (es. il direttorio di sviluppo del progetto).</P> 
     * 
     * @param  baseDir Direttorio di base del modulo. 
     * @return         Oggetto. Se il modulo non esiste, restituisce
     *                 {@code null}.
     */
    public Module loadModule(File baseDir) {
        File metadataFile, localFile;
        Module module;
        ModuleParser metadataHandler;
        ModuleLocalParser localHandler;
        
        if (baseDir == null) {
            throw new NullPointerException("Argument baseDir is null.");
        }
        
        metadataFile = new File(baseDir, Module.METADATA_FILE);
        if (!metadataFile.exists()) {
            return null;
        }
        
        metadataHandler = new ModuleParser(this, myErrorHandler);
        try {
            myParser.parse(metadataFile, metadataHandler);
        } catch (IOException ex) {
            throw new RuntimeEnvironmentException(ex);
        } catch (SAXException ex) {
            throw new RuntimeEnvironmentException(ex);
        }
        
        module = metadataHandler.getModule();
        localFile = new File(baseDir, Module.LOCAL_FILE);
        if (localFile.exists()) {
            localHandler = new ModuleLocalParser(module, myErrorHandler);
            try {
                myParser.parse(localFile, localHandler);
            } catch (IOException ex) {
                throw new RuntimeEnvironmentException(ex);
            } catch (SAXException ex) {
                throw new RuntimeEnvironmentException(ex);
            }
        }
        
        module.setBaseDirectory(baseDir);
        module.endInit();
        
        return module;
    }

    /**
     * Istanzia il supporto per la risoluzione delle versioni di un modulo.
     * 
     * @param name    Nome del modulo.
     * @param version Versione richiesta del modulo.
     * @return        Oggetto.
     */
    ModuleVersionResolver newModuleVersionResolver(SymbolicName name,
            Version version) {
        ModuleVersionResolver moduleResolver;
        
        if (name == null || name.isEmpty()) {
            throw new NullPointerException("Argument name is null.");
        }
        if (version == null) {
            throw new NullPointerException("Argument version is null.");
        }
        
        moduleResolver = new ModuleVersionResolver(name, version,
                myVersionRedirs.get(name));
        
        return moduleResolver;
    }
    
    /**
     * Legge un modulo.
     * 
     * @param  moduleResolver Risoluzione delle versioni del modulo.
     * @return                Oggetto. Se il modulo non &egrave; presente nel
     *                        repository, restituisce {@code null}.                
     */
    Module loadModule(ModuleVersionResolver moduleResolver) {
        String msg;
        File baseDir;
        Module module;
        
        // Direttorio di base
        baseDir = moduleResolver.getName().toFile(myBaseDir);
        baseDir = new File(baseDir, moduleResolver.getVersion().toString());

        module = loadModule(baseDir);
        if (module == null) {
            return null;
        }

        if (!module.getIdentity().getName().equals(
                moduleResolver.getName()) ||
                !module.getIdentity().getVersion().equals(
                moduleResolver.getVersion())) {
            msg = String.format(
                    "Module [%1$s] mismatch name %2$s and version %3$s.",
                    module, moduleResolver.getName(),
                    moduleResolver.getVersion());
            throw new RuntimeEnvironmentException(msg);
        }
        
        return module;        
    } 
}


/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.net.*;

/**
 * Implementazione di default dell&rsquo;interfaccia
 * {@code LibraryClasspathEntry}.
 */
final class DefaultClasspathEntry implements LibraryClasspathEntry {
    private final File myPath;
    private final File mySourcePath;
    private final String mySourceRootPath;
    private final URL myDocURL;
    
    /**
     * Costruttore.
     * 
     * @param path           Percorso assoluto del modulo JAR (o del direttorio
     *                       del codice binario).
     * @param sourcePath     Percorso assoluto dell&rsquo;archivio del codice
     *                       sorgente.
     * @param sourceRootPath Percorso radice del codice sorgente
     *                       all&rsquo;interno dell&rsquo;archivio 
     *                       {@code sourcePath}.
     * @param docURL         URL della documentazione API.
     */
    DefaultClasspathEntry(File path, File sourcePath,
            String sourceRootPath, URL docURL) {
        myPath = path;
        mySourcePath = sourcePath;
        mySourceRootPath = sourceRootPath;
        myDocURL = docURL;
    }
    
    public File getPath() {
        return myPath;
    }
    
    public File getSourcePath() {
        return mySourcePath;
    }
    
    public String getSourceRootPath() {
        return mySourceRootPath;
    }
    
    public URL getDocURL() {
        return myDocURL;
    }
}

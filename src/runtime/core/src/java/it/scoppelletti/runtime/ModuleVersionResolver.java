/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.util.*;

/**
 * Risoluzione delle versioni di un modulo.
 * 
 * <P>Lo schema di validazione dei metadati di un modulo garantisce che le
 * dipendenze dirette di ogni modulo siano moduli distinte per nome: lo stesso
 * modulo non pu&ograve; comparire pi&ugrave; volte con versioni differenti.<BR>
 * La composizione delle dipendenze per l&rsquo;esecuzione di un modulo visita
 * ricorsivamente le dipendenze dirette dei moduli costruendo un albero delle
 * dipendenze nel quale &egrave; possibile che un modulo venga inserito
 * pi&ugrave; volte: in questo caso la versione deve sempre coincidere
 * (eventualmente anche attraverso le redirezioni di versione) con la versione
 * con la quale il modulo compare nel livello superiore dell&rsquo;albero delle
 * dipendenze (pi&ugrave; vicino alla radice); a parit&agrave; di livello
 * nell&rsquo;albero delle dipendenze viene considerato l&rsquo;ordine con il
 * quale le dipendenze sono elencate nei metadati dei moduli.</P>
 */
final class ModuleVersionResolver {            
    private final SymbolicName myName;
    private final Map<Version, Version> myVersionRedirs;
    
    /**
     * Versione richiesta del modulo (la prima che si cerca di caricare).
     */
    private final Version myVersion;
    
    /**
     * Prima versione del modulo effettivamente caricata. Potrebbe non
     * coincidere con la versione {@code myVersion} per effetto della
     * redirezione delle versioni.
     */
    private final Version myActualVersion;
    
    /**
     * Collezione delle versioni <I>caricate</I>.
     * 
     * <P>La collezione &egrave; alimentata con le seguenti versioni:</P>
     * 
     * <OL>
     * <LI>Versione richiesta {@code myVersion}.
     * <LI>Eventuale versione {@code myActualVersion} effettivamente caricata
     * diversa dalla versione {@code myVersion} per effetto della redirezione
     * delle versioni.
     * <LI>Versioni che si &egrave; verificato che corrispondono alla versione
     * {@code myActualVersion} per effetto della redirezione delle versioni.
     * </OL>
     * 
     * <P>Il <I>percorso di redirezione</I> da una versione all&rsquo;altra
     * pu&ograve; anche essere non diretto: nella collezione
     * {@code myVersionCluster} vengono inserite tutte le versioni
     * <I>visitate</I> lungo il percorso di redirezione.
     * 
     * <H4>Esempio</H4>
     * 
     * <OL>
     * <LI>Carico la versione {@code 1.0.0} che non &egrave; redirezionata.
     * &rarr; Inserisco la versione {@code 1.0.0} nella collezione
     * {@code myVersionCluster}.
     * <LI>Cerco di caricare la versione {@code 1.0.3} che &egrave;
     * redirezionata sulla versione {@code 1.0.2}, che a sua volta &egrave;
     * redirezionata sulla versione {@code 1.0.1}, che a sua volta &egrave;
     * redirezionata sulla versione {@code 1.0.0}. &rarr; Inserisco le versioni
     * {@code 1.0.1}, {@code 1.0.2} e {@code 1.0.3} nella collezione
     * {@code myVersionCluster}.
     * <LI>Cerco di caricare la versione {@code 1.0.2} che &egrave; gi&agrave;
     * inserita nella collezione {@code myVersionCluster}. &rarr; So gi&agrave;
     * che il percorso di redirezione della versione {@code 1.0.2} porta alla
     * versione {@code 1.0.0} senza ulteriori verifiche.
     * </OL>
     */
    private final Set<Version> myVersionCluster;
    
    /**
     * Collezione delle versioni che si &egrave; verificato che non
     * corrispondono alla versione {@code myActualVersion} nemmeno per effetto
     * della redirezione delle versioni.
     * 
     * <P>Anche nella collezione {@code myIncompatibleVersions} vengono inserite
     * tutte le versioni visitate lungo il percorso di redirezione.</P>
     * 
     * <H4>Esempio</H4>
     * 
     * <OL>
     * <LI>Carico la versione {@code 1.0.0} che non &egrave; redirezionata.
     * &rarr; Inserisco la versione {@code 1.0.0} nella collezione
     * {@code myVersionCluster}.
     * <LI>Cerco di caricare la versione {@code 2.0.2} che &egrave;
     * redirezionata sulla versione {@code 2.0.1}, che a sua volta &egrave;
     * redirezionata sulla versione {@code 2.0.0}, che a sua volta non &egrave;
     * redirezionata &rarr; La versione {@code 2.0.2} non &egrave;
     * <I>compatibile</I> con la versione {@code 1.0.0}. &rarr; Inserisco le
     * versioni {@code 2.0.0}, {@code 2.0.1} e {@code 2.0.2} nella collezione
     * {@code myIncompatibleVersions}.
     * <LI>Cerco di caricare la versione {@code 2.0.1} che &egrave; gi&agrave;
     * inserita nella collezione {@code myIncompatibleVersions}. &rarr; So
     * gi&agrave; che la versione {@code 2.0.1} non &egrave; compatibile con la
     * versione {@code 1.0.0} senza ulteriori verifiche.
     * </OL>
     */
    private final Set<Version> myIncompatibleVersions;
           
    /**
     * Costruttore.
     * 
     * @param name                Nome del modulo.
     * @param version             Versione richiesta del modulo.            
     * @param versionRedirections Redirezioni di versione configurate per il
     *                            modulo. Pu&ograve; essere {@code null}.  
     */
    ModuleVersionResolver(SymbolicName name, Version version,
            Map<Version, Version> versionRedirections) {
        Set<Version> versionSet;
        
        if (name == null || name.isEmpty()) {
            throw new NullPointerException("Argument name is null.");
        }
        if (version == null) {
            throw new NullPointerException("Argument version is null.");
        }
        
        myName = name;
        myVersionRedirs = versionRedirections;
        myVersion = version;            
        myVersionCluster = new HashSet<Version>();
        myIncompatibleVersions = new HashSet<Version>();
        
        versionSet = new HashSet<Version>();        
        myActualVersion = redirectVersion(myVersion, myVersion, versionSet);
        myVersionCluster.addAll(versionSet);       
    }
        
    /**
     * Restituisce il nome del modulo.
     * 
     * @return Valore.
     */
    SymbolicName getName() {
        return myName;
    }
    
    /**
     * Restituisce la versione del modulo.
     * 
     * @return Valore. Pu&ograve; essere differente dalla versione specificata
     *         nel costruttore per effetto del redirezionamento delle versioni.
     */
    Version getVersion() {
        return myActualVersion;
    }

    /**
     * Verifica se una versione corrisponde (eventualmente anche attraverso le
     * redirezioni di versione) alla versione gi&agrave; effettivamente
     * caricata.
     *  
     * @param version Versione da verificare.
     */
    void checkVersion(Version version) {
        String msg;
        Version toVer;
        Set<Version> versionSet;
        
        if (version == null) {
            throw new NullPointerException("Argument version is null.");
        }
        
        versionSet = new HashSet<Version>();        
        toVer = redirectVersion(version, version, versionSet);
        if (toVer == null) {
            return;
        }
        
        if (myVersionCluster.contains(toVer)) {
            myVersionCluster.addAll(versionSet);
        } else {
            myIncompatibleVersions.addAll(versionSet);
            msg = String.format(
                    "Attempt to load different versions (%2$s and %3$s) for same module %1$s.",
                    myName, myVersion, version);
            throw new RuntimeEnvironmentException(msg);            
        }
    }
    
    /**
     * Esegue il redirezionamento di una versione.
     * 
     * <P>Una versione pu&ograve; essere redirezionata su un&rsquo;altra
     * versione che a sua volta pu&ograve; essere ulteriormente redirezionata e
     * cos&igrave; via; per questo motivo il metodo {@code redirectVersion}
     * &egrave; ricorsivo: chi esegue il metodo {@code redirectVersion} deve
     * specificare la stessa versione nei parametri {@code loadingVersion} e
     * {@code version} mentre le chiamate ricorsive utilizzano il parametro
     * {@code version} per la versione in esame <I>conservando</I> invece la
     * versione originale nel parametro {@code loadingVersion}.</P>
     * 
     * @param  loadingVersion  Versione originale che si cerca di caricare.
     * @param  version         Versione da redirezionare.
     * @param  versionSet      Collezione di tutte le versioni visitate per il
     *                         redirezionamento ricorsivo: chi esegue il metodo
     *                         {@code redirectVersion} deve impostare una
     *                         collezione vuota; se una chiamata ricorsiva cerca
     *                         di inserire una versione gi&agrave; presente
     *                         nella collezione, significa che il percorso di
     *                         redirezionamento configura un ciclo infinito.
     * @return                 Versione sulla quale la versione {@code version}
     *                         &egrave; stata redirezionata. Se la versione
     *                         {@code version} corrisponde alla versione
     *                         gi&agrave; effettivamente caricata (eventualmente
     *                         anche attraverso le redirezioni di versione),
     *                         restituisce {@code null}.                                               
     */
    private Version redirectVersion(Version loadingVersion, Version version,
            Set<Version> versionSet) {
        String msg;
        Version toVer;
        
        if (myVersionCluster.contains(version)) {
            myVersionCluster.addAll(versionSet);
            return null;
        }
        if (myIncompatibleVersions.contains(version)) {
            myIncompatibleVersions.addAll(versionSet);
            msg = String.format(
                    "Attempt to load different versions (%2$s and %3$s) for same module %1$s.",
                    myName, myVersion, loadingVersion);
            throw new RuntimeEnvironmentException(msg);
        }
                
        if (!versionSet.add(version)) {
            msg = String.format(
                    "Infinite loop in redirection of module %1$s from version %2$s through version %3$s.",
                    myName, loadingVersion, version);
            throw new RuntimeEnvironmentException(msg);
        }
                       
        if (myVersionRedirs == null) {
            // Per il modulo non sono configurate redirezioni di versione
            return version;
        }
        
        toVer = myVersionRedirs.get(version);
        if (toVer == null) {
            // Versione non redirezionata
            return version;
        }
                               
        // Versione redirezionata
        return redirectVersion(loadingVersion, toVer, versionSet);
    }        
}

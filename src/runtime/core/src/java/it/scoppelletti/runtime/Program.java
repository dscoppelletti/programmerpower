/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.security.*;
import java.util.*;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
public final class Program {
    private static final String PROP_CLASSPATH = "java.class.path";
    private static final String PROP_USERLANGUAGE = "user.language";
    private static final String PROP_USERREGION = "user.region";
    private static final String PROP_USERCOUNTRY = "user.country";
    private static final String PROP_USERVARIANT = "user.variant";
    private static final String DEF_USERLANGUAGE = "en";
    
    /**
     * Codice di uscita di JVM per successo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final int EXIT_SUCCESS = 0;
        
    /**
     * Codice di uscita di JVM per errore. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final int EXIT_FAILURE = 1;
    
    /**
     * Costruttore.
     */
    private Program() {        
    }
    
    /**
     * Entry-point.     
     * 
     * @param args Argomenti sulla linea di comando.
     * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/cli.html"
     *        TARGET="_top">Linea di comando</A>
     */
    public static void main(String[] args) {
        int exitCode = Program.EXIT_SUCCESS;
        String errorMsg;
        Program program = new Program();
        
        try {
            errorMsg = program.run(args);
            if (errorMsg != null) {
                exitCode = Program.EXIT_FAILURE;
                program.writeAbout(System.err);
                program.writeUsage(System.err, errorMsg);
            }
        } catch (Throwable ex) {
            exitCode = Program.EXIT_FAILURE;
            program.writeAbout(System.err);
            ex.printStackTrace();
        }
        
        if (exitCode != 0) {
            System.exit(exitCode);
        }
    }    
    
    /**
     * Esegue l&rsquo;applicazione.
     *
     * @param args Argomenti sulla linea di comando.
     */
    private String run(String args[]) {
        // Questo metodo viene mantenuto con il minor numero possibile di
        // oggetti locali: tutti gli altri oggetti necessari
        // all'inizializzazione sono inseriti nel contesto del metodo
        // initApplication per favorirne il rilascio da parte del garbage
        // collector anche in ambiente di debug.
        Method mainMethod;
        String[] applArgs;
        
        if (args.length < 1) {
            return "Missing repository base directory.";
        }
        if (args.length < 2) {
            return "Missing module name.";
        }
        if (args.length < 3) {
            return "Missing module version.";
        }
        
        mainMethod = initApplication(args[0], args[1], args[2]);
        
        applArgs = new String[args.length - 3];
        System.arraycopy(args, 3, applArgs, 0, applArgs.length);
        
        try {
            mainMethod.invoke(null, new Object[] { applArgs });
        } catch (InvocationTargetException ex) {
            throw new RuntimeEnvironmentException(ex);
        } catch (IllegalAccessException ex) {
            // Questa eccezione non dovrebbe verificarsi perche' il controllo
            // di accessibilita' del linguaggio Java e' stato disabilitato
            // (metodo setAccessible).
            throw new RuntimeEnvironmentException(ex);
        }
        
        return null;
    }  
    
    /**
     * Inizializza l&rsquo;applicazione.
     * 
     * @param  repositoryDir Direttorio di base del repository dei moduli.
     * @param  name          Nome del modulo.
     * @param  version       Versione del modulo.
     * @return               Metodo di entry-point.
     */
    private Method initApplication(String repositoryDir, String name,
            String version) {
        String msg, className;
        SymbolicName applName;
        Version applVersion;
        ClassLoader loader;
        Module applModule;        
        ModuleRepository moduleRepo;
        
        applName = new SymbolicName(name);
        applVersion = new Version(version);        
        moduleRepo = initModuleRepository(repositoryDir);
        
        applModule = moduleRepo.loadModule(applName, applVersion);
        if (applModule == null) {
            msg = String.format("Module (%1$s, %2$s) not found.", applName,
                    applVersion);
            throw new RuntimeEnvironmentException(msg);            
        }
        
        className = applModule.getMainClassName();
        if (className == null || className.isEmpty()) {
            msg = String.format("Undefined main class for module %1$s.",
                    applModule);
            throw new RuntimeEnvironmentException(msg);
        }
  
        initSystemProps(applModule);
        loader = initClassLoader(applModule);
        
        return initMainMethod(className, loader);
    }
    
    /**
     * Inizializza il repository dei moduli.
     * 
     * @param  repositoryDir Direttorio di base del repository dei moduli.
     * @return               Oggetto.
     */
    private ModuleRepository initModuleRepository(String repositoryDir) {
        String msg;
        File dir;
        ModuleRepositoryFactory repoFactory;
        
        dir = new File(repositoryDir);
        if (!dir.isDirectory()) {
            msg = String.format("Directory %1$s not found.", dir);
            throw new RuntimeEnvironmentException(msg);
        }
        
        repoFactory = new ModuleRepositoryFactory();
        repoFactory.setBaseDirectory(dir);
        repoFactory.setVersionRedirecting(true);
        
        return repoFactory.newRepository();
    }
    
    /**
     * Imposta le propriet&agrave; di sistema.
     * 
     * @param module Modulo dell&rsquo;applicazione.
     */
    private void initSystemProps(Module module) {
        Map<String, String> props;
        Properties systemProps;
        
        props = module.getRuntimeProperties();
        
        // Il metodo getProperties della classe statica System verifica i
        // privilegi di accesso alle proprieta' di sistema; una volta ottenuta
        // la collezione pero', posso liberamente impostarle le proprieta' di
        // sistema.
        systemProps = AccessController.doPrivileged(
                new PrivilegedAction<Properties>() {
                    public Properties run() {
                        return System.getProperties();
                    }
                });
                
        systemProps.putAll(props);        
        initLocale(systemProps, props.keySet());
    }
    
    /**
     * Reimposta (se necessario) la localizzazione di default di JVM.
     *
     * <P>Il metodo {@code initLocale} &egrave; necessario perch&egrave; la
     * localizzazione di default di JVM viene determinata una-tantum
     * all&rsquo;inizializzazione di JVM stessa, quindi l&rsquo;eventuale
     * modifica delle propriet&agrave; di sistema che determinano questa
     * localizzazione da parte dei moduli caricati dal runtime non avrebbe alcun
     * effetto.</P> 
     * 
     * @param props         Propriet&agrave; di sistema.
     * @param modifiedProps Nomi delle propriet&agrave; modificate.
     */
    private void initLocale(Properties props, Set<String> modifiedProps) {
        int p;
        String region;
        final String lang, country, var;
        
        if (!modifiedProps.contains(Program.PROP_USERLANGUAGE) &&
                !modifiedProps.contains(Program.PROP_USERREGION) &&
                !modifiedProps.contains(Program.PROP_USERCOUNTRY) &&
                !modifiedProps.contains(Program.PROP_USERVARIANT)) {
            // Le proprieta' impostate non riguardano la localizzazione
            return;
        }
        
        lang = props.getProperty(Program.PROP_USERLANGUAGE,
                Program.DEF_USERLANGUAGE);
        region = props.getProperty(Program.PROP_USERREGION);
        if (region != null) {
            p = region.indexOf('_');
            if (p >= 0) {
                country = region.substring(0, p);
                var = region.substring(p + 1);
            } else {
                country = region;
                var = "";
            }
        } else {
            country = props.getProperty(Program.PROP_USERCOUNTRY, "");
            var = props.getProperty(Program.PROP_USERVARIANT, "");
        }
        
        AccessController.doPrivileged(
                new PrivilegedAction<Void>() {
                    public Void run() {
                        Locale.setDefault(new Locale(lang, country, var));                
                        return null;
                    }
                });                
    }
    
    /**
     * Inizializza il class-loader.
     * 
     * @param  applModule Modulo dell&rsquo;applicazione.
     * @return            Oggetto.
     */
    private ClassLoader initClassLoader(Module applModule) {
        int i;
        final StringBuilder buf;
        ClassLoader loader;
        final URL[] urls;
        List<File> classpath;        
        List<LibraryClasspathEntry> depList;
        
        classpath = new ArrayList<File>();
        depList = applModule.listRuntimeClasspathEntries(null, true);
        
        for (LibraryClasspathEntry entry : depList) {
            classpath.add(entry.getPath());
        }
        
        i = 0;
        urls = new URL[classpath.size()];
        buf = new StringBuilder();
        
        for (File file : classpath) {
            if (i > 0) {
                buf.append(File.pathSeparatorChar);
            }
            buf.append(file.getPath());
            
            try {
                urls[i] = file.toURI().toURL();
            } catch (MalformedURLException ex) {
                throw new RuntimeEnvironmentException(ex);
            }
            
            i++;            
        }
                                      
        loader = AccessController.doPrivileged(
                new PrivilegedAction<ClassLoader>() {
                    public ClassLoader run() {
                        ClassLoader loader;
        
                        loader = new URLClassLoader(urls);
                        System.setProperty(Program.PROP_CLASSPATH,
                                buf.toString());
                        Thread.currentThread().setContextClassLoader(loader);
                
                        return loader;
                    }
                });  
                
        return loader;
    }
        
    /**
     * Inizializza il metodo di entry-point dell&rsquo;applicazione.
     * 
     * @param  className Nome della classe che implementa l&rsquo;applicazione.
     * @param  loader    Caricatore delle classi.
     * @return           Oggetto.
     */
    private Method initMainMethod(String className, ClassLoader loader) {
        int mods;
        final Class<?> mainClass;
        Method method;
                
        try {
            mainClass = Class.forName(className, true, loader);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeEnvironmentException(ex);
        }
        
        try {            
            method = AccessController.doPrivileged(
                    new PrivilegedExceptionAction<Method>() {
                        public Method run() throws NoSuchMethodException {                
                            Method method;
                            String args[] = new String[0];
                    
                            method = mainClass.getMethod("main",
                                    new Class[] { args.getClass() });                    
                            method.setAccessible(true);
                    
                            return method;
                        }
                    });  
        } catch (PrivilegedActionException ex) {
            throw new RuntimeEnvironmentException(ex.getCause());
        }
        
        mods = method.getModifiers();
        try {
            if (!Modifier.isPublic(mods) || !Modifier.isStatic(mods) ||
                    method.getReturnType() != void.class) {
                throw new NoSuchMethodException("main");
            }
        } catch (NoSuchMethodException ex) {
            throw new RuntimeEnvironmentException(ex);
        }        
        
        return method;
    }
    
    /**
     * Emette le informazioni sull&rsquo;applicazione.
     * 
     * @param out Flusso di scrittura.
     */
    private void writeAbout(PrintStream out) {
        out.println("<about>");
        out.println("Programmer Power Runtime");
        out.println("1.1.1");
        out.println();
        out.println("Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>");
        out.println();
        out.println("Licensed under the Apache License, Version 2.0 (the \"License\");");
        out.println("you may not use this software except in compliance with the License.");
        out.println("You may obtain a copy of the License at");
        out.println();
        out.println("    http://www.apache.org/licenses/LICENSE-2.0");
        out.println();
        out.println("Unless required by applicable law or agreed to in writing, software");
        out.println("distributed under the License is distributed on an \"AS IS\" BASIS,");
        out.println("WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.");
        out.println("See the License for the specific language governing permissions and");
        out.println("limitations under the License.");
    }
    
    /**
     * Emette la descrizione della sintassi della linea di comando.
     * 
     * @param out Flusso di scrittura.
     * @param msg Messaggio di errore. Pu&ograve; essere {@code null}.
     */
    private void writeUsage(PrintStream out, String msg) {
        if (msg != null) {
            out.println("<error>");
            out.println(msg);
            out.println("</error>");
        }
        out.println("<usage>");
        out.print("<jvm> ");
        out.print(getClass().getName());
        out.print(" <repository base directory> <module name> ");
        out.println("<module version> [application arguments] ...");
        out.println("</usage>");
    }
}


/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 * Gestore di default degli eventi del parser
 * <ACRONYM TITLE="Simple API for XML">SAX</ACRONYM>.
 */
abstract class XmlDefaultHandler extends DefaultHandler {    
    private final ErrorHandler myErrorHandler;    
    private Locator myLocator = null;
    private StringBuilder myContentBuffer = null;
    
    /**
     * Costruttore.
     * 
     * @param errorHandler Gestore degli errori SAX.
     */
    XmlDefaultHandler(ErrorHandler errorHandler) {
        myErrorHandler = errorHandler;
    }
    
    /**
     * Restituisce il localizzatore degli eventi nel documento.
     * 
     * <P>Le classi derivate possono accedere al localizzatore degli eventi per
     * determinare appunto la posizione nel documento XML per la quale &egrave;
     * stato inoltrato l&rsquo;evento dal parser SAX; questo tipo di
     * informazione pu&ograve; essere utile, ad esempio, per integrare le
     * segnalazioni di errore.<BR>
     * Il localizzatore, se reso disponibile dalla specifica implementazione del
     * parser SAX, &egrave; accessibile dai gestore degli eventi relativi al
     * contenuto del documento XML dopo l&rsquo;evento {@code startDocument} e
     * prima dell&rsquo;evento {@code endDocument}.</P>
     * 
     * @return Oggetto. Pu&ograve; essere {@code null}.
     */
    Locator getDocumentLocator() {
        return myLocator;
    }
        
    /**
     * Imposta il localizzatore degli eventi nel documento.
     * 
     * @param                       locator Oggetto. Pu&ograve; essere
     *                                      {@code null}.
     * @it.scoppelletti.tag.default         Localizzatore eventualmente fornito
     *                                      dal parser.
     */
    @Override
    public void setDocumentLocator(Locator locator) {
        myLocator = locator;
    }
    
    /**
     * Errore fatale.
     *
     * @param ex Eccezione.
     */
    @Override
    public void fatalError(SAXParseException ex) throws SAXException {
        if (myErrorHandler != null) {
            myErrorHandler.fatalError(ex);
        } else {
            // SAX provvede comunque a inoltrare l'eccezione
        }
    }
    
    /**
     * Errore non fatale.
     *
     * @param ex Eccezione.
     */
    @Override
    public void error(SAXParseException ex) throws SAXException {
        if (myErrorHandler != null) {
            myErrorHandler.error(ex);
        } else {
            throw ex;
        }
    }
    
    /**
     * Avviso.
     *
     * @param ex Eccezione.
     */
    @Override
    public void warning(SAXParseException ex) throws SAXException {
        if (myErrorHandler != null) {
            myErrorHandler.warning(ex);
        } else {
            // Alzo il livello di gravit&agrave; da avviso ad errore
            throw ex;
        }
    }    
    
    /**
     * Caratteri nel contenuto di un elemento.
     * 
     * <P>Questa implementazione del metodo {@code characters} colleziona i
     * caratteri rilevati se tale raccolta &egrave; attiva.</P>
     * 
     * @param ch     Buffer del testo.
     * @param start  Indice del primo carattere del testo nel vettore
     *               {@code ch}.
     * @param length Numero di caratteri del testo.
     */
    @Override
    public void characters(char[] ch, int start, int length) throws
            SAXException {
        if (myContentBuffer != null) {
            myContentBuffer.append(ch, start, length);
        }
    }
    
    /**
     * Caratteri di spaziatura nel contenuto di un elemento.
     * 
     * <P>Alcuni parser possono utilizzare l&rsquo;evento
     * {@code ignorableWhitespace} anzich&eacute; l&rsquo;evento 
     * {@code characters} per rilevare i caratteri di spaziatura presenti nel
     * testo nel contenuto degli elementi; per questo motivo, questa
     * implementazione del metodo {@code ignorableWhitespace} delega la
     * gestione dei caratteri di spaziatura allo stesso metodo
     * {@code characters}.</P>
     *  
     * @param ch     Buffer del testo.
     * @param start  Indice del primo carattere del testo nel vettore
     *               {@code ch}.
     * @param length Numero di caratteri del testo.
     */
    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws
            SAXException {
        characters(ch, start, length);        
    }
    
    /**
     * Attiva la raccolta del testo nel contenuto degli elementi.
     * 
     * <P>Le classi derivate normalmente attivano la raccolta del testo nel
     * contenuto degli elementi che lo prevedono eseguendo il metodo
     * {@code collectContent} nel metodo {@code startElement} che rileva
     * l&rsquo;apertura degli elementi interessati.</P>
     *  
     * @throws java.lang.IllegalStateException
     *         La raccolta del testo &egrave; gi&agrave; stata attivata.
     */
    final void collectContent() {
        if (myContentBuffer != null) {
            throw new IllegalStateException(
                    "XML content text collection already active.");            
        }
        
        myContentBuffer = new StringBuilder();
    }
    
    /**
     * Restituisce il testo raccolto dal contenuto degli elementi e disattiva
     * la raccolta.
     * 
     * <P>Le classi derivate normalmente gestiscono il testo raccolto dal
     * contenuto degli elementi che lo prevedono eseguendo il metodo
     * {@code getCollectedContent} nel metodo {@code endElement} che rileva la
     * chiusura degli elementi interessati.</P>
     * 
     * @return Testo. Se la raccolta del testo non &egrave; stata attivata,
     *         restituisce {@code null}.
     */
    final String getCollectedContent() {
        String text;
        
        if (myContentBuffer == null) {
            return null;
        }
        
        text = myContentBuffer.toString();
        myContentBuffer = null;
        
        return text;
    }    
}

/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime;

import java.io.*;
import java.net.*;

/**
 * Elemento di un class-path che associa ad ogni modulo
 * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> l&rsquo;archivio del codice
 * sorgente e l&rsquo;<ACRONYIM TITLE="Uniform Resource Locator">URL</ACRONYM>
 * della documentazione delle
 * <ACRONYIM TITLE="Application Programming Interface">API</ACRONYM>.
 * 
 * @since 1.1.0
 */
public interface LibraryClasspathEntry {

    /**
     * Restituisce il percorso del modulo JAR (o del direttorio del codice
     * binario).
     * 
     * @return Percorso assoluto. 
     */
    File getPath();
    
    /**
     * Restituisce il percorso dell&rsquo;archivio del codice sorgente.
     * 
     * @return Percorso assoluto. Se il modulo JAR non &egrave; accompagnato dal
     *         codice sorgente, restituisce {@code null}.
     */
    File getSourcePath();
    
    /**
     * Restituisce il percorso radice del codice sorgente all&rsquo;interno
     * dell&rsquo;archivio {@code getSourcePath}.
     * 
     * @return Percorso. Se il percorso radice del codice sorgente deve essere
     *         rilevato automaticamente, restituisce {@code null}.  
     * @see    #getSourcePath
     */
    String getSourceRootPath();
    
    /**
     * Restituisce l&rsquo;URL della documentazione delle API.
     * 
     * @return URL. Se il modulo JAR non &egrave; accompagnato dalla
     *         documentazione API, restituisce {@code null}.
     */
    URL getDocURL();
}

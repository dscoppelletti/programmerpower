/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime.tomcat;

import org.apache.juli.logging.*;
import org.xml.sax.*;

/**
 * Gestore degli errori <ACRONYM TITLE="Simple API for XML">SAX</ACRONYM>.
 */
final class XmlErrorHandler implements ErrorHandler {
    private final Log myLogger;
    
    /**
     * Costruttore.
     * 
     * @param logger Emettitore degli eventi di log.
     */
    XmlErrorHandler(Log logger) {
        if (logger == null) {
            throw new NullPointerException("Argument logger is null.");            
        }

        myLogger = logger;
    }
    
    /**
     * Errore fatale.
     *
     * <P>Questa implementazione del metodo {@code fatalError} non esegue nulla
     * perch&eacute; SAX provvede comunque a inoltrare l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     */
    public void fatalError(SAXParseException ex) throws SAXException {
    }
    
    /**
     * Errore non fatale.
     *
     * <P>Questa implementazione del metodo {@code error} inoltra
     * l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     */
    public void error(SAXParseException ex) throws SAXException {
        throw ex;                
    }
    
    /**
     * Avviso.
     *
     * <P>Questa implementazione del metodo {@code warning} emette
     * l&rsquo;eccezione come segnalazione di log.</P>
     *
     * @param ex Eccezione.
     */
    public void warning(SAXParseException ex) throws SAXException {
        myLogger.warn(ex.getMessage(), ex);
    }      
}

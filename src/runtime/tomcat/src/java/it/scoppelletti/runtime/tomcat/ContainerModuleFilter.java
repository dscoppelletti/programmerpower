/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime.tomcat;

import java.io.*;
import java.util.*;
import it.scoppelletti.runtime.*;

/**
 * Filtro dei moduli per escludere quelli gi&agrave; installati nel Web
 * Container.
 */
final class ContainerModuleFilter implements ModuleFilter {
    private final Set<String> myExcludeNames;
    
    /**
     * Costruttore.
     */
    ContainerModuleFilter() {
        myExcludeNames = new HashSet<String>();
    }

    public boolean accept(Module module) {
        if (myExcludeNames.contains(
                module.getIdentity().getName().toString())) {
            return false;
        }
        
        return true;
    }       
    
    /**
     * Aggiunge un modulo da escludere.
     * 
     * @param name Nome del modulo.
     */
    void add(SymbolicName name) {
        if (name == null || name.isEmpty()) {
            throw new NullPointerException("Argument name is null.");
        }
        
        myExcludeNames.add(name.toString());
    }
    
    /**
     * Aggiunge i moduli da escludere elencati in un file.
     * 
     * @param  file File.
     */
    void add(File file) throws IOException {
        String line, name;
        FileReader in = null;
        BufferedReader reader = null;
         
        if (file == null) {
            throw new NullPointerException("Argument file is null.");
        }
        
        try {
            in = new FileReader(file);
            reader = new BufferedReader(in);
            in = null;
            
            line = reader.readLine();
            while (line != null) {
                name = line.trim().toLowerCase();                
                if (!name.isEmpty()) {
                    myExcludeNames.add(name);
                }
                
                line = reader.readLine();
            }
        } finally {
            if (in != null) {
                in.close();
                in = null;
            }
            if (reader != null) {
                reader.close();
                reader = null;
            }
        }       
    }
}

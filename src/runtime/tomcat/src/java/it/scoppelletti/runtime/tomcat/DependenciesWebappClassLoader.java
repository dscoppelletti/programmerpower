/*
 * Copyright (C) 2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime.tomcat;

import java.io.*;
import java.util.*;
import java.util.jar.*;
import javax.servlet.*;
import org.apache.catalina.*;
import org.apache.catalina.loader.*;
import org.apache.juli.logging.*;
import it.scoppelletti.runtime.*;

/**
 * Class-loader che risolve le dipendenze di un&rsquo;applicazione Web nel
 * repository dei moduli di Programmer Power. 
 * 
 * @see   it.scoppelletti.runtime.tomcat.DependenciesWebappLoader
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/tomcat/runtime.html"
 *        TARGET="_top">Integrazione di Apache Tomcat con il runtime di
 *        Programmer Power</A> 
 * @since 1.1.0
 */
public final class DependenciesWebappClassLoader extends WebappClassLoaderBase {
    
    /**
     * Attributo di applicazione 
     * {@code it.scoppelletti.runtime.tomcat.repositoryDir}: Direttorio di base
     * del repository dei moduli.
     * 
     * <P>Il direttorio pu&ograve; essere assoluto oppure relativo al direttorio
     * <CODE>$CATALINA_BASE/conf/&lt;engine&gt;/&lt;host&gt;</CODE>.</P>
     * 
     * @it.scoppelletti.tag.default Direttorio impostato nella propriet&agrave;
     * di sistema {@code it.scoppelletti.runtime.tomcat.repositoryDir}.
     * @see it.scoppelletti.runtime.ModuleRepositoryFactory#setBaseDirectory
     */
    public static final String ATTR_REPOSITORYDIR =
            "it.scoppelletti.runtime.tomcat.repositoryDir";
    
    /**
     * Attributo di applicazione 
     * {@code it.scoppelletti.runtime.tomcat.versionRedirecting}: Indicatore di
     * redirezione delle versioni abilitata.
     * 
     * @it.scoppelletti.tag.default {@code true}
     * @see it.scoppelletti.runtime.ModuleRepositoryFactory#setVersionRedirecting
     */
    public static final String ATTR_VERSIONREDIRECTING =
            "it.scoppelletti.runtime.tomcat.versionRedirecting";
        
    /**
     * Attributo di applicazione 
     * {@code it.scoppelletti.runtime.tomcat.installedFile}: File di elenco dei
     * nomi dei moduli gi&agrave; installati nell&rsquo;application server.
     * 
     * <P>Il file pu&ograve; essere assoluto oppure relativo al direttorio
     * <CODE>$CATALINA_BASE/conf/&lt;engine&gt;/&lt;host&gt;</CODE>.</P>
     * 
     * @it.scoppelletti.tag.default {@code it-scoppelletti-runtime-modules.txt}
     */
    public static final String ATTR_INSTALLEDFILE =
            "it.scoppelletti.runtime.tomcat.installedFile";
    
    /**
     * Attributo di applicazione
     * {@code it.scoppelletti.runtime.tomcat.moduleName}: Nome del modulo.
     * 
     * @see #ATTR_MODULENAME
     * @see it.scoppelletti.runtime.ModuleRepository#loadModule(SymbolicName, Version) 
     */
    public static final String ATTR_MODULENAME =
            "it.scoppelletti.runtime.tomcat.moduleName";
    
    /**
     * Attributo di applicazione
     * {@code it.scoppelletti.runtime.tomcat.moduleVersion}: Versione del modulo.
     * 
     * @see #ATTR_MODULENAME
     * @see it.scoppelletti.runtime.ModuleRepository#loadModule(SymbolicName, Version) 
     */
    public static final String ATTR_MODULEVERSION =
            "it.scoppelletti.runtime.tomcat.moduleVersion";
    
    /**
     * Attributo di applicazione
     * {@code it.scoppelletti.runtime.tomcat.moduleDir}: Direttorio di base del
     * modulo.
     * 
     * <P>Il direttorio pu&ograve; essere assoluto oppure relativo al direttorio
     * <CODE>$CATALINA_BASE/conf/&lt;engine&gt;/&lt;host&gt;</CODE>.</P>
     *  
     * @see it.scoppelletti.runtime.ModuleRepository#loadModule(File) 
     */
    public static final String ATTR_MODULEDIR =
            "it.scoppelletti.runtime.tomcat.moduleDir";
    
    /**
     * Propriet&agrave; di sistema
     * {@code it.scoppelletti.runtime.tomcat.repositoryDir}: Direttorio di base
     * del repository dei moduli.
     * 
     * @see #ATTR_REPOSITORYDIR
     */
    public static final String PROP_REPOSITORYDIR =
            "it.scoppelletti.runtime.tomcat.repositoryDir";
    
    /**
     * Nome di default del file di elenco dei nomi dei moduli gi&agrave;
     * installati nell&rsquo;application server. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #ATTR_INSTALLEDFILE
     */
    public static final String DEF_INSTALLEDFILE =
            "it-scoppelletti-runtime-modules.txt";
    
    private static final Log myLogger = LogFactory.getLog(
            DependenciesWebappClassLoader.class);
    
    // - Apache Tomcat 8.0.23
    // La classe base WebappClassLoaderBase mantiene lo stato del componente in
    // un campo privato e l'implementazione dei metodi dell'interfaccia
    // Lifecycle non prevede dei punti di estensione:
    // Implemento una versione prevalente dei metodi dell'interfaccia Lifecycle
    // che mantiene uno stato "prevalente" del componente.
    private volatile LifecycleState myState = LifecycleState.NEW;
    
    /**
     * Costruttore.
     */
    public DependenciesWebappClassLoader() {
    }
    
    /**
     * Costruttore.
     * 
     * @param parent Class-loader parent.
     */
    public DependenciesWebappClassLoader(ClassLoader parent) {
        super(parent);
    }
    
    /**
     * Restituisce lo stato del componente.
     * 
     * @return Valore.
     */
    @Override
    public LifecycleState getState() {
        return myState;
    }
    
    /**
     * Inizializza il componente.
     */
    @Override
    public void init() {
        super.init();
        myState = LifecycleState.INITIALIZED;
    }
    
    /**
     * Avvia il class-loader.
     */
    @Override
    public void start() throws LifecycleException {
        myState = LifecycleState.STARTING_PREP;
        super.start();
        myState = LifecycleState.STARTING;
        
        try {
            doStart();
        } catch (IOException|RuntimeException ex) {
            myLogger.error("Failed to start class-loader.", ex);
        }
        
        myState = LifecycleState.STARTED;
    }
    
    /**
     * Inizializza il class-loader.
     */
    private void doStart() throws IOException {
        File file;
        Context ctx;
        Module module;
        WebResourceRoot res;
        ServerInfo serverInfo;
        ServletContext servletCtx;
        ContainerModuleFilter filter;
        List<LibraryClasspathEntry> depList;
        
        res = getResources();
        if (res == null) {
            myLogger.warn("Property resources not set.");
            return;
        }
        
        ctx = resources.getContext();
        if (ctx == null) {
            myLogger.warn("Application context undefined.");
            return;
        }
        
        servletCtx = ctx.getServletContext();
        serverInfo = new ServerInfo(ctx);
        module = initModule(ctx, servletCtx, serverInfo);
        if (module == null) {
            myLogger.warn(String.format(
                    "No module found for application %1$s.", ctx.getName()));
            return;
        }
    
        if (!getDelegate()) {
            myLogger.warn("The class-loader will not follow the standard " +
                    "Java 2 delegation model.");
        }        
        
        filter = new ContainerModuleFilter();
        filter.add(module.getIdentity().getName());
        
        file = getInstalledFile(servletCtx, serverInfo);
        try {
            filter.add(file);
        } catch (IOException|RuntimeException ex) {
            myLogger.error(String.format("Failed to add filter-file %1$s.",
                    file), ex);            
        }                                    

        depList = module.listRuntimeClasspathEntries(filter, true);        
        for (LibraryClasspathEntry entry : depList) {
            file = entry.getPath();
 
            try {
                addURL(file.toURI().toURL());
            } catch (RuntimeException ex) {
                myLogger.error(String.format("Failed to add %1$s.", file), ex);                
            }            
        }       
        
        file = serverInfo.getHostConfigDir();
        if (file != null) {
            try {
                addURL(file.toURI().toURL());
            } catch (RuntimeException ex) {
                myLogger.error(String.format("Failed to add %1$s.", file), ex);                  
            }                
        }
        
        file = serverInfo.getConfigDir();
        if (file != null) {
            try {
                addURL(file.toURI().toURL());
            } catch (RuntimeException ex) {
                myLogger.error(String.format("Failed to add %1$s.", file), ex);             
            }                
        }                
    }
    
    /**
     * Termina il class-loader.
     */
    @Override
    public void stop() throws LifecycleException {
        myState = LifecycleState.STOPPING_PREP;
        super.stop();
        myState = LifecycleState.STOPPED;
    }
 
    /**
     * Rilascia le risorse del componente.
     */
    @Override
    public void destroy() {
        myState = LifecycleState.DESTROYING;
        super.destroy();
        myState = LifecycleState.DESTROYED;
    }
    
    /**
     * Restituisce una copia del class-loader priva degli eventuali
     * trasformatori di classi e classi trasformate.
     * 
     * @return Oggetto.
     */
    public ClassLoader copyWithoutTransformers() {
        DependenciesWebappClassLoader loader;
        
        loader = new DependenciesWebappClassLoader(getParent());
        copyStateWithoutTransformers(loader);
        loader.myState = LifecycleState.NEW;
        
        try {
            loader.start();
        } catch (LifecycleException ex) {
            throw new IllegalStateException(ex);
        }
        
        return loader;
    }
    
    /**
     * Verifica lo stato del class-loader.
     */
    @Override
    protected void checkStateForResourceLoading(String resource) throws
        IllegalStateException {
        String msg;
        
        if (!myState.isAvailable()) {
            msg = String.format("Illegal state %1$s.", myState);
            myLogger.error(msg);
            throw new IllegalStateException(msg);
        }
        
        super.checkStateForResourceLoading(resource);
    }    
    
    /**
     * Restituisce l&rsquo;oggetto per la sincronizzazione del caricamento delle
     * classi.
     * 
     * @return Oggetto.
     */
    @Override
    protected Object getClassLoadingLock(String className) {
        return this;
    }    
    
    /**
     * Inizializza il modulo.
     * 
     * @param  ctx        Contesto dell&rsquo;applicazione.
     * @param  servletCtx Contesto dell&rsquo;applicazione.
     * @param  serverInfo Informazioni sul server.
     * @return            Oggetto.
     */
    private Module initModule(Context ctx, ServletContext servletCtx,
            ServerInfo serverInfo) throws IOException {
        String value;
        Object obj;
        SymbolicName name;
        Version ver;        
        ModuleRepository repo;
        Manifest mf;
        Attributes attrs;
        
        repo = initModuleRepository(servletCtx, serverInfo);
        
        obj = servletCtx.getAttribute(
                DependenciesWebappClassLoader.ATTR_MODULENAME);
        name = (obj instanceof SymbolicName) ? (SymbolicName) obj : null;
        
        obj = servletCtx.getAttribute(
                DependenciesWebappClassLoader.ATTR_MODULEVERSION);
        ver = (obj instanceof Version) ? (Version) obj : null;
        
        if (name != null && !name.isEmpty() && ver != null) {
            if (myLogger.isTraceEnabled()) {
                myLogger.trace(String.format(
                    "Loading module %1$s, version %2$s", name, ver));
            }       
            
            return repo.loadModule(name, ver);
        }
        
        value = getAttribute(servletCtx,
                DependenciesWebappClassLoader.ATTR_MODULEDIR);
        if (value != null && !value.isEmpty()) {
            if (myLogger.isTraceEnabled()) {
                myLogger.trace(String.format(
                    "Loading module from base directory %1$s.", value));
            }
            
            return repo.loadModule(serverInfo.toFile(value));            
        }
            
        mf = loadManifest();
        if (mf == null) {
            if (myLogger.isWarnEnabled()) {
                myLogger.warn(String.format(
                    "MANIFEST.MF not found for application %1$s",
                    ctx.getName()));
            }                   
            
            return null;
        }
        
        attrs = mf.getMainAttributes();
        if (attrs == null) {
            return null;
        }
        
        value = attrs.getValue(Attributes.Name.IMPLEMENTATION_TITLE);
        if (value == null || value.isEmpty()) {
            return null;
        }        
        name = new SymbolicName(value);
        
        value = attrs.getValue(Attributes.Name.IMPLEMENTATION_VERSION);
        if (value == null || value.isEmpty()) {
            return null;
        }        
        ver = new Version(value);
        
        if (myLogger.isTraceEnabled()) {
            myLogger.trace(String.format(
                    "Loading module %1$s, version %2$s", name, ver));
        }
        
        return repo.loadModule(name, ver);
    }
    
    /**
     * Inizializza il repository dei moduli.
     * 
     * @param  ctx        Contesto dell&rsquo;applicazione.
     * @param  serverInfo Informazioni sul server.
     * @return            Oggetto.
     */
    private ModuleRepository initModuleRepository(ServletContext ctx,
            ServerInfo serverInfo) {
        boolean verRedirecting;
        File dir;
        Object obj;
        ModuleRepositoryFactory repoFactory;
        
        dir = getRepositoryDir(ctx, serverInfo);
        if (dir == null) {
            throw new NullPointerException("Missing attribute repositoryDir.");
        }
                
        obj = ctx.getAttribute(
                DependenciesWebappClassLoader.ATTR_VERSIONREDIRECTING);
        verRedirecting = (obj == null) ? true :
            Boolean.parseBoolean(obj.toString());
        
        repoFactory = new ModuleRepositoryFactory();
        repoFactory.setBaseDirectory(dir);
        repoFactory.setVersionRedirecting(verRedirecting);
        repoFactory.setErrorHandler(new XmlErrorHandler(myLogger));
        
        return repoFactory.newRepository();
    }
    
    /**
     * Restituisce il direttorio di base del repository.
     * 
     * @param  ctx        Contesto dell&rsquo;applicazione.
     * @param  serverInfo Informazioni sul server.
     * @return            Direttorio.
     */
    private File getRepositoryDir(ServletContext ctx, ServerInfo serverInfo) {
        String value;

        value = getAttribute(ctx,
                DependenciesWebappClassLoader.ATTR_REPOSITORYDIR);
        if (value != null && !value.isEmpty()) {
            return serverInfo.toFile(value);
        }
        
        value = JVMTools.getProperty(
                DependenciesWebappClassLoader.PROP_REPOSITORYDIR);
        if (value == null || value.isEmpty()) {
            return null;
        }
        
        return serverInfo.toFile(value);
    }
    
    /**
     * Legge il manifesto dell&rsquo;applicazione.
     * 
     * @return Oggetto.
     */
    private Manifest loadManifest() throws IOException {
        WebResource res;
        Manifest mf;
        InputStream in = null;
        
        res = getResources().getResource("/META-INF/MANIFEST.MF");
        if (res == null) {
            return null;
        }
        
        try {
            in = res.getInputStream();
            mf = new Manifest(in);
        } finally {
            if (in != null) {
                in.close();
                in = null;
            }
        }
        
        return mf;
    }     
    
    /**
     * Restituisce il file di elenco dei nomi dei moduli gi&agrave; installati
     * nell&rsquo;application server.
     *  
     * @param  ctx        Contesto dell&rsquo;applicazione. 
     * @param  serverInfo Informazioni sul server.
     * @return            Direttorio.
     */
    private File getInstalledFile(ServletContext ctx, ServerInfo serverInfo) {
        String value;
        
        value = getAttribute(ctx,
                DependenciesWebappClassLoader.ATTR_INSTALLEDFILE);
        if (value != null && !value.isEmpty()) {
            return serverInfo.toFile(value);
        }
        
        return serverInfo.toFile(
                DependenciesWebappClassLoader.DEF_INSTALLEDFILE);
    }    
    
    /**
     * Restituisce il valore di un attributo del contesto
     * dell&rsquo;applicazione.
     * 
     * @param  ctx  Contesto dell&rsquo;applicazione.
     * @param  name Nome dell&rsquo;attributo.
     * @return      Valore.
     */
    private String getAttribute(ServletContext ctx, String name) {
        Object obj;
        
        obj = ctx.getAttribute(name);
        return (obj == null) ? null : obj.toString();        
    }    
}

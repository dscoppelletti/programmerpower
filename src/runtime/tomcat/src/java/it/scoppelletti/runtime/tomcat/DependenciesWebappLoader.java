/*
 * Copyright (C) 2011-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime.tomcat;

import java.io.File;
import javax.servlet.ServletContext;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.loader.WebappLoader;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import it.scoppelletti.runtime.SymbolicName;
import it.scoppelletti.runtime.Version;

/**
 * Class-loader che risolve le dipendenze di un&rsquo;applicazione Web nel
 * repository dei moduli di Programmer Power. 
 * 
 * @see   it.scoppelletti.runtime.tomcat.DependenciesWebappClassLoader
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/tomcat/runtime.html"
 *        TARGET="_top">Integrazione di Apache Tomcat con il runtime di
 *        Programmer Power</A> 
 * @since 1.0.0
 */
public final class DependenciesWebappLoader extends WebappLoader {
    private static final Log myLogger = LogFactory.getLog(
                DependenciesWebappLoader.class);
    private String myRepoDir = null;
    private boolean myVerRedirecting = true;
    private String myInstalledFile = null;
    private SymbolicName myModuleName = null;    
    private Version myModuleVer = null;
    private String myModuleDir = null;    
    
    /**
     * Costruttore.
     */
    public DependenciesWebappLoader() {
        doInit();
    }
    
    /**
     * Costruttore.
     * 
     * @param parent Class-loader parent.
     */
    public DependenciesWebappLoader(ClassLoader parent) {
        super(parent);
        doInit();
    }
    
    /**
     * Inizializzazione.
     */
    private void doInit() {
        // - Apache Tomcat 8.0.23
        // Posso usare il metodo setLoaderClass per sovrascrivere il nome della
        // classe di default che implementa il class-loader perche' ho
        // verificato che questo accessore in scrittura non inoltra l'evento
        // PropertyChange come invece fanno altri.
        setLoaderClass(DependenciesWebappClassLoader.class.getName());
    }
    
    /**
     * Imposta il direttorio di base del repository.
     *
     * <P>Il direttorio pu&ograve; essere assoluto oppure relativo al direttorio
     * <CODE>$CATALINA_BASE/conf/&lt;engine&gt;/&lt;host&gt;</CODE>.</P>
     * 
     * @param                       dir Direttorio.
     * @it.scoppelletti.tag.default Direttorio impostato nella propriet&agrave;
     * di sistema {@code it.scoppelletti.runtime.tomcat.repositoryDir}.
     * @see it.scoppelletti.runtime.ModuleRepositoryFactory#setBaseDirectory
     */
    public void setRepositoryDir(String dir) {
        myRepoDir = dir;
    }

    /**
     * Imposta l&rsquo;indicatore di redirezione delle versioni abilitata.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code true}
     * @see it.scoppelletti.runtime.ModuleRepositoryFactory#setVersionRedirecting
     */    
    public void setVersionRedirecting(boolean value) {        
        myVerRedirecting = value;
    }
    
    /**
     * Imposta il file di elenco dei nomi dei moduli gi&agrave; installati
     * nell&rsquo;application server.
     * 
     * <P>Il file pu&ograve; essere assoluto oppure relativo al direttorio
     * <CODE>$CATALINA_BASE/conf/&lt;engine&gt;/&lt;host&gt;</CODE>.</P>
     *  
     * @param                       file File.
     * @it.scoppelletti.tag.default {@code it-scoppelletti-runtime-modules.txt}
     */
    public void setInstalledFile(String file) {
        myInstalledFile = file;
    }
     
    /**
     * Imposta il nome del modulo.
     * 
     * @param value Valore.
     * @see         #setModuleVersion
     * @see it.scoppelletti.runtime.ModuleRepository#loadModule(SymbolicName, Version) 
     */
    public void setModuleName(String value) {
        myModuleName = new SymbolicName(value);
    }
    
    /**
     * Imposta la versione del modulo.
     * 
     * @param value Valore.
     * @see         #setModuleName
     * @see it.scoppelletti.runtime.ModuleRepository#loadModule(SymbolicName, Version)
     */
    public void setModuleVersion(String value) {
        if (value == null || value.isEmpty()) {
            myModuleVer = null;
        } else {
            myModuleVer = new Version(value);
        }
    }

    /**
     * Imposta il direttorio di base del modulo.
     * 
     * <P>Il direttorio pu&ograve; essere assoluto oppure relativo al direttorio
     * <CODE>$CATALINA_BASE/conf/&lt;engine&gt;/&lt;host&gt;</CODE>.</P>
     *  
     * @param dir Direttorio.
     * @see       it.scoppelletti.runtime.ModuleRepository#loadModule(File)  
     */
    public void setModuleDir(String dir) {
        myModuleDir = dir;
    }
    
    /**
     * Avvia il class-loader.
     */
    @Override
    protected void startInternal() throws LifecycleException {
        doStart();
        super.startInternal(); 
    }   
    
    /**
     * Inizializza il class-loader.
     */
    private void doStart() {
        Context ctx;
        ServletContext servletCtx;
        
        ctx = getContext();
        if (ctx == null) {
            myLogger.warn("Property context not set.");
            return;
        }
        
        servletCtx = ctx.getServletContext();
        setAttribute(servletCtx,
                DependenciesWebappClassLoader.ATTR_REPOSITORYDIR, myRepoDir);
        setAttribute(servletCtx,
                DependenciesWebappClassLoader.ATTR_INSTALLEDFILE,
                myInstalledFile);        
        setAttribute(servletCtx, DependenciesWebappClassLoader.ATTR_MODULEDIR,
                myModuleDir);
        
        if (!myVerRedirecting) {
            servletCtx.setAttribute(
                    DependenciesWebappClassLoader.ATTR_VERSIONREDIRECTING,
                    Boolean.FALSE.toString());
        }
        if (myModuleName != null) {
            servletCtx.setAttribute(
                    DependenciesWebappClassLoader.ATTR_MODULENAME,
                    myModuleName);            
        }         
        if (myModuleVer != null) {
            servletCtx.setAttribute(
                    DependenciesWebappClassLoader.ATTR_MODULEVERSION,
                    myModuleVer);            
        }             
    }
    
    /**
     * Imposta il valore di un attributo del contesto
     * dell&rsquo;applicazione.
     * 
     * @param  ctx   Contesto dell&rsquo;applicazione.
     * @param  name  Nome dell&rsquo;attributo.
     * @param  value Valore.
     */
    private void setAttribute(ServletContext ctx, String name, String value) {
        if (value != null && !value.isEmpty()) {
            ctx.setAttribute(name, value);
        }
    }    
}

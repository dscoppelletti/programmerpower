/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.runtime.tomcat;

import java.io.*;
import org.apache.catalina.*;

/**
 * Informazioni sull&rsquo;<ACRONYM TITLE="Application Server">AS</ACRONYM>. 
 */
final class ServerInfo {
    private final File myConfigDir;
    private final File myHostConfigDir;
    
    /**
     * Costruttore.
     * 
     * @param container Server.
     */
    ServerInfo(Container container) {
        myConfigDir = initConfigDir();
        myHostConfigDir = initHostConfigDir(container);
    }
    
    /**
     * Restituisce il direttorio di configurazione del server.
     * 
     * @return Direttorio assoluto. Pu&ograve; essere {@code null}.
     */
    File getConfigDir() {
        return myConfigDir;
    }
    
    /**
     * Inizializza il direttorio di configurazione dell&rsquo;istanza.
     * 
     * @return Direttorio.
     */
    private File initConfigDir() {
        String path;
        File file;
        
        path = JVMTools.getProperty(Globals.CATALINA_BASE_PROP);
        if (path == null || path.isEmpty()) {
            return null;
        }
        
        file = new File(path, "conf");
        return file.getAbsoluteFile();        
    }
    
    /**
     * Restituisce il direttorio di configurazione dell&rsquo;host.
     * 
     * @return Direttorio assoluto. Pu&ograve; essere {@code null}.
     */
    File getHostConfigDir() {
        return myHostConfigDir;
    }
    
    /**
     * Inizializza il direttorio di configurazione dell&rsquo;host.
     * 
     * @param  container Server 
     * @return           Direttorio.
     */
    private File initHostConfigDir(Container container) {
        File dir;
        Container parent;
        Host host;
        
        if (container == null || myConfigDir == null) {
            return null;
        }
        
        parent = container.getParent();
        if (!(parent instanceof Host)) {
            return null;
        }        
        
        host = (Host) parent;
        
        dir = myConfigDir;
        parent = host.getParent();
        if (parent instanceof Engine) {
            dir = new File(dir, parent.getName());
        }
        
        return new File(dir, host.getName());       
    }
    
    /**
     * Converte una stringa in un percorso assoluto.
     * 
     * <P>Se la stringa corrisponde a un percorso relativo, questo &egrave;
     * considerato relativo al direttorio di configurazione dell&rsquo;host
     * oppure al direttorio di configurazione del server.</P>
     * 
     * @param  path Stringa.
     * @return      Percorso.
     */
    File toFile(String path) {                        
        File file;
        
        if (path == null || path.isEmpty()) {
            throw new NullPointerException("Argument path is null.");
        }
        
        file = new File(path);
        if (file.isAbsolute()) {
            return file;
        }
        
        if (myHostConfigDir != null) {
            file = new File(myHostConfigDir, path);
            if (file.exists()) {
                return file;
            }
        }
        
        if (myConfigDir != null) {
            file = new File(myConfigDir, path);
            if (file.exists()) {
                return file;
            }            
        }
        
        return file.getAbsoluteFile();
    }              
}

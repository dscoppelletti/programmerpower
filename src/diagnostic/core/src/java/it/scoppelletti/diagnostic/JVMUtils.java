/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic;

import java.security.*;

/**
 * Funzioni di utilit&agrave; di
 * <ACRONYM TITLE="Java Virtual Machine">JVM</ACRONYM>.
 */
final class JVMUtils {
    
    /**
     * Costruttore privato per classe statica.
     */
    private JVMUtils() {        
    }

    /**
     * Restituisce il valore di una propriet&agrave; di sistema.
     * 
     * <P>Il metodo {@code getSystemProperty}, a differenza del corrispondente
     * metodo della classe statica {@code System}, abilita i privilegi per
     * superare gli eventuali controlli di accesso impostati con un oggetto
     * {@code SecurityManager}.</P>
     * 
     * @param  key Nome della propriet&agrave;.
     * @return     Valore. Se la propriet&agrave; non &egrave; impostata,
     *             restituisce {@code null}.
     */
    static String getProperty(final String key) {
        String value;
        
        value = AccessController.doPrivileged(new PrivilegedAction<String>() {
            public String run() {
                return System.getProperty(key);
            }
        });
        
        return value;
    }    
}

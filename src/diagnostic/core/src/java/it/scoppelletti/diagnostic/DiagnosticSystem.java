/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic;

import java.util.*;

/**
 * Sistema di diagnostica.
 * 
 * @it.scoppelletti.tag.threadsafe
 * @since 1.0.0
 */
public final class DiagnosticSystem {
    
    /**
     * Nome della propriet&agrave; inserita nella struttura {@code MDC} del
     * sistema di logging che specifica il nome che identifica univocamente
     * l&rsquo;applicazione. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String MDC_APPLICATIONIDENTITY =
        "it.scoppelletti.diagnostic.ApplicationIdentity";
    
    /**
     * Nome della propriet&agrave; inserita nella struttura {@code MDC} del
     * sistema di logging che specifica l&rsquo;UUID dell&rsquo;istanza
     * dell&rsquo;applicazione. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String MDC_APPLICATIONINSTANCEID =
        "it.scoppelletti.diagnostic.ApplicationInstanceId";
       
    /**
     * Nome della propriet&agrave; inserita nella struttura {@code MDC} del
     * sistema di logging che specifica il nome del contesto del thread. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     */    
    public static final String MDC_THREADNAME =
        "it.scoppelletti.diagnostic.ThreadName";
    
    /**
     * Nome della propriet&agrave; inserita nella struttura {@code MDC} del
     * sistema di logging che specifica l&rsquo;UUID del contesto del thread. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     */    
    public static final String MDC_THREADID =
        "it.scoppelletti.diagnostic.ThreadId";
    
    /**
     * Nome della propriet&agrave; di sistema sulla quale pu&ograve; essere
     * impostato il nome della classe che implementa il rilascio delle risorse
     * del sistema di diagnostica attraverso l&rsquo;interfaccia 
     * {@code Runnable}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #shutdown
     */
    public static final String PROP_SPI_SHUTDOWN =
        "it.scoppelletti.diagnostic.DiagnosticSystem.shutdown";
    
    private static final DiagnosticSystem myInstance = new DiagnosticSystem();
    private static final ThreadDC myDC = new ThreadDC();
    private static final InheritableThreadDC myInheritableDC =
        new InheritableThreadDC();
    private Map<String, DiagnosticPlugin> myPlugins;

    /**
     * Costruttore privato per classe singleton.
     */
    private DiagnosticSystem() {
        myPlugins = new HashMap<String, DiagnosticPlugin>();
    }

    /**
     * Rilascia le risorse del sistema di diagnostica.
     * 
     * <P>&Egrave; importante che il metodo {@code shutdown} sia eseguito al
     * termine dell&rsquo;applicazione per garantire che vengano rilasciate
     * tutte le risorse del sistema di diagnostica; ad esempio, a seconda della
     * configurazione del sistema di logging, se l&rsquo;applicazione termina
     * senza eseguire il metodo {@code shutdown}, alcuni messaggi di log
     * potrebbero essere <I>persi</I>.<BR>
     * Poich&egrave; il rilascio delle risorse dipende dal sistema di logging
     * adottato, sulla propriet&agrave; di sistema
     * {@code it.scoppelletti.diagnostic.DiagnosticSystem.shutdown} pu&ograve;
     * essere impostato il nome della classe che implementa l&rsquo;operazione
     * attraverso l&rsquo;interfaccia {@code Runnable}.</P>
     */            
    public void shutdown() {
        String providerName;
        Class<?> providerClass;
        Runnable provider;
        
        providerName = JVMUtils.getProperty(
                DiagnosticSystem.PROP_SPI_SHUTDOWN);
        if (providerName == null || providerName.isEmpty()) {
            return;
        }
        
        try {
            providerClass = Class.forName(providerName);
            provider = (Runnable) providerClass.newInstance();
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
        
        provider.run();
    }
    
    /**
     * Restituisce l&rsquo;istanza del sistema di diagnostica.
     * 
     * @return Oggetto.
     */
    public static DiagnosticSystem getInstance() {
        return myInstance;
    }

    /**
     * Restituisce un plug-in.
     * 
     * @param  name Nome del plug-in.
     * @return      Oggetto. Se il plug-in non esiste, restituisce {@code null}
     *              e segnala sul flusso {@code stderr}.
     */
    public DiagnosticPlugin getPlugin(String name) {
        DiagnosticPlugin obj;

        synchronized (myPlugins) {
            obj = myPlugins.get(name);
        }

        if (obj == null) {
            System.err.printf("Plugin %1$s not found.%n", name);
        }

        return obj;
    }

    /**
     * Restituisce una copia non modificabile della collezione dei plug-in
     * registrati.
     * 
     * @return Collezione.
     */
    public Iterator<DiagnosticPlugin> getPlugins() {
        Collection<DiagnosticPlugin> c;

        synchronized (myPlugins) {
            c = Collections.unmodifiableCollection(myPlugins.values());
        }

        return c.iterator();
    }

    /**
     * Aggiunge un plug-in.
     * 
     * @param obj Oggetto.
     */
    public void addPlugin(DiagnosticPlugin obj) {
        if (obj == null) {
            throw new NullPointerException("Argument obj is null.");
        }

        synchronized (myPlugins) {
            if (myPlugins.put(obj.getName(), obj) != null) {
                System.err.printf("Plug-in %1$s replaces another plug-in " +
                        "with the same name.%n", obj.getName());
            }
        }
    }

    /**
     * Restituisce una copia non modificabile della collezione degli oggetti
     * aggiunti al contesto di diagnosi del thread corrente.
     * 
     * @return Collezione.
     */
    public Iterator<Object> getDiagnosticContext() {
        Collection<Object> c;

        c = new ArrayList<Object>(myDC.get());
        c.addAll(myInheritableDC.get());

        return Collections.unmodifiableCollection(c).iterator();
    }

    /**
     * Aggiunge un oggetto al contesto di diagnosi del thread corrente.
     * 
     * @param obj         Oggetto.
     * @param inheritable Indica se la registrazione deve essere ereditata dagli
     *                    eventuali thread figli (avviati dopo la
     *                    registrazione).
     */
    public void addDiagnosticContextObject(Object obj, boolean inheritable) {
        if (obj == null) {
            throw new NullPointerException("Argument obj is null.");
        }

        if (inheritable) {
            myInheritableDC.get().add(obj);
        } else {
            myDC.get().add(obj);
        }
    }

    /**
     * Rimuove un oggetto dal contesto di diagnosi del thread corrente.
     * 
     * @param obj Oggetto.
     */
    public void removeDiagnosticContextObject(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Argument obj is null.");
        }

        myDC.get().remove(obj);
        myInheritableDC.get().remove(obj);
    }

    /**
     * Rimuove tutti gli oggetti dal contesto di diagnosi del thread corrente.
     */
    public void removeDiagnosticContext() {
        myDC.remove();
        myInheritableDC.remove();
    }    
    
    /**
     * Contesto di diagnosi.
     */
    private static final class ThreadDC extends ThreadLocal<List<Object>> {

        /**
         * Costruttore.
         */
        ThreadDC() {
        }

        /**
         * Inizializza l&rsquo;oggetto.
         * 
         * @return Oggetto.
         */
        @Override
        protected List<Object> initialValue() {
            return new ArrayList<Object>();
        }
    }

    /**
     * Contesto di diagnosi.
     */
    private static final class InheritableThreadDC extends
            InheritableThreadLocal<List<Object>> {

        /**
         * Costruttore.
         */
        InheritableThreadDC() {
        }

        /**
         * Inizializza l&rsquo;oggetto.
         * 
         * @return Oggetto.
         */
        @Override
        protected List<Object> initialValue() {
            return new ArrayList<Object>();
        }
    }
}

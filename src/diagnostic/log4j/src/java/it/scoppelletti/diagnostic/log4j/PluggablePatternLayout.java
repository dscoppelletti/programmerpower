/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;

/**
 * Componente {@code Layout} con modello di formattazione configurabile.
 * 
 * <P>La classe {@code PluggablePatternLayout} estende le funzionalit&agrave;
 * della classe <ACRONYM TITLE="Logging for Java">log4j</ACRONYM>
 * {@code PatternLayout} con la possibilit&agrave; di implementare nuove
 * specifiche di conversione attraverso dei plug-in.<BR>
 * Per ogni possibile specifica di conversione sono interrogati i plug-in
 * registrati per rilevare il primo plug-in che implementa quella specifica di
 * conversione attraverso l&rsquo;interfaccia {@code PatternLayoutPlugin}; se
 * nessuno degli eventuali plug-in registrati implementa la specifica di
 * conversione in esame, l&rsquo;implementazione sar&agrave; quella standard
 * della libreria log4j.<BR>
 * L&rsquo;ordine di interrogazione dei plug-in non &egrave; determinabile,
 * quindi, per evitare comportamenti non definiti, sarebbe bene mantenere i
 * caratteri di conversione implementati dai plug-in univoci e distinti da
 * quelli standard implementati della libreria log4j.</P>      
 * 
 * @see   it.scoppelletti.diagnostic.log4j.PatternLayoutPlugin
 * @since 1.0.0
 */
public final class PluggablePatternLayout extends PatternLayout {

    /**
     * Costruttore.
     */
    public PluggablePatternLayout() {
    }

    /**
     * Istanzia il parser del modello di formattazione degli eventi di log.
     * 
     * @return Oggetto.
     */
    @Override
    protected PatternParser createPatternParser(String pattern) {
        return new PluggablePatternParser(pattern);
    }
}

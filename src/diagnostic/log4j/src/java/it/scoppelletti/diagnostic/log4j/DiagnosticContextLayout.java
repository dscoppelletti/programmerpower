/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import java.util.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import it.scoppelletti.diagnostic.*;

/**
 * Componente {@code Layout} che emette la rappresentazione come stringa degli
 * oggetti registrati nel contesto di diagnosi del thread corrente.
 *
 * <P>La rappresentazione come stringa delle classi di oggetto pu&ograve;
 * essere configurata con l&rsquo;utilizzo dei componenti
 * <ACRONYM TITLE="Logging for Java">log4j</ACRONYM>
 * {@code ObjectRenderer}.</P>
 * 
 * @see   it.scoppelletti.diagnostic.DiagnosticSystem#addDiagnosticContextObject
 * @since 1.0.0
 */
public final class DiagnosticContextLayout extends Layout {
    private String mySep;
    private String myTerminator;

    /**
     * Costruttore.
     */
    public DiagnosticContextLayout() {
        mySep = Layout.LINE_SEP;
        myTerminator = Layout.LINE_SEP;
    }

    /**
     * Attiva le opzioni impostate.
     */
    @Override
    public void activateOptions() {
    }

    /**
     * Restituisce il separatore intercalato tra gli oggetti emessi.
     * 
     * @return Valore.
     * @see    #setSeparator
     */
    public String getSeparator() {
        return mySep;
    }

    /**
     * Imposta il separatore intercalato tra gli oggetti emessi.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Separatore di linea
     * @see                               #getSeparator
     */
    public void setSeparator(String value) {
        mySep = value;
    }

    /**
     * Restituisce il terminatore dell&rsquo;elenco degli oggetti emessi.
     * 
     * @return Valore.
     * @see    #setTerminator
     */
    public String getTerminator() {
        return myTerminator;
    }

    /**
     * Imposta il terminatore dell&rsquo;elenco degli oggetti emessi.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Separatore di linea
     * @see                               #getTerminator
     */
    public void setTerminator(String value) {
        myTerminator = value;
    }

    /**
     * Indica se il {@code Layout} ignora l&rsquo;eventuale eccezione inclusa
     * negli eventi di log.
     * 
     * @return Indicatore.
     */
    @Override
    public boolean ignoresThrowable() {
        return true;
    }

    /**
     * Applica il layout ad un evento di log.
     * 
     * @param  event Evento.
     * @return       Stringa.     
     */
    @Override
    public String format(LoggingEvent event) {
        Object obj;
        String s;
        Iterator<Object> it;
        RendererSupport renderer;
        StringBuilder buf = new StringBuilder();

        it = DiagnosticSystem.getInstance().getDiagnosticContext();
        while (it.hasNext()) {
            obj = it.next();
            if (obj == null) {
                continue;
            }

            try {                
                renderer = (RendererSupport) LogManager.getLogger(
                        event.getLoggerName()).getLoggerRepository();
                s = renderer.getRendererMap().findAndRender(obj);
            } catch (Exception ex) {
                LogLog.error(
                        String.format("Failed to render object %1$s.", obj),
                        ex);
                s = obj.toString();
            }
            if (Strings.isNullOrEmpty(s)) {
                continue;
            }

            if (mySep != null && buf.length() > 0) {
                buf.append(mySep);
            }
            buf.append(s);
        }
        if (myTerminator != null && buf.length() > 0) {
            buf.append(myTerminator);
        }
        if (buf.length() == 0) {
            return null;
        }

        return buf.toString();
    }
}

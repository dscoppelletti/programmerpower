/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import java.io.*;
import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;

/**
 * Flusso di scrittura degli eventi di log su un file temporaneo.
 * 
 * @since 1.0.0
 */
public final class TempFileAppender extends WriterAppender {

    /**
     * Dimensione di default del buffer di scrittura. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #setBufferSize
     */
    public static final int DEF_BUFFERSIZE = 8 * 1024;

    /**
     * Suffisso di default per la composizione del nome del file. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #setSuffix
     */
    public static final String DEF_SUFFIX = ".tmp";

    private String myPrefix = null;
    private String mySuffix = TempFileAppender.DEF_SUFFIX;
    private String myDir = null;
    private boolean myBufferedIO = false;
    private int myBufferSize = TempFileAppender.DEF_BUFFERSIZE;

    /**
      * Costruttore.
      */
    public TempFileAppender() {
    }

    /**
     * Restituisce il prefisso per la composizione del nome del file.
     * 
     * @return Valore.
     * @see    #setPrefix
     */
    public String getPrefix() {
        return myPrefix;
    }

    /**
     * Imposta il prefisso per la composizione del nome del file.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required       Almeno 3 caratteri.
     * @see                                #getPrefix
     */
    public void setPrefix(String value) {
        myPrefix = (value != null) ? value.trim() : null;
    }

    /**
     * Restituisce il suffisso per la composizione del nome del file.
     * 
     * @return Valore.
     * @see    #setSuffix
     */
    public String getSuffix() {
        return mySuffix;
    }

    /**
     * Imposta il suffisso per la composizione del nome del file.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.default        {@code .tmp}
     * @see                                #getSuffix
     */
    public void setSuffix(String value) {
        mySuffix = (value != null) ? value.trim() : null;
    }

    /**
     * Restituisce il direttorio nel quale &egrave; inserito il file.
     * 
     * @return Valore.
     * @see    #setDir
     */
    public String getDir() {
        return myDir;
    }

    /**
     * Imposta il direttorio nel quale &egrave; inserito il file.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       Direttorio dei file temporanei del
     *                                    sistema.
     * @see                               #getDir 
     */
    public void setDir(String value) {
        myDir = (value != null) ? value.trim() : null;
    }

    /**
     * Restituisce l&rsquo;indicatore di scrittura attraverso buffer.
     * 
     * @return Valore.
     * @see    #setBufferedIO
     */
    public boolean getBufferedIO() {
        return myBufferedIO;
    }

    /**
     * Imposta l&rsquo;indicatore di scrittura attraverso buffer.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #getBufferedIO
     */
    public void setBufferedIO(boolean value) {
        myBufferedIO = value;
    }

    /**
     * Restituisce la dimensione del buffer di scrittura.
     * 
     * @return Valore.
     * @see    #setBufferSize
     */
    public int getBufferSize() {
        return myBufferSize;
    }

    /**
     * Imposta la dimensione del buffer di scrittura.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code 8 * 1024}.
     * @see                               #getBufferSize
     */
    public void setBufferSize(int value) {
        myBufferSize = value;
    }

    /**
     * Attiva le opzioni impostate.
     */
    @Override
    public void activateOptions() {
        File file;
        FileOutputStream out = null;
        Writer writer = null;
        Writer bufWriter = null;

        if (Strings.isNullOrEmpty(myPrefix)) {
            errorHandler.error("Property prefix is not set.");
            return;
        }

        if (myBufferedIO) {
            setImmediateFlush(false);
        }

        reset();

        try {
            file = File.createTempFile(myPrefix,
                    Strings.isNullOrEmpty(mySuffix) ?
                    TempFileAppender.DEF_SUFFIX : mySuffix,
                    Strings.isNullOrEmpty(myDir) ? null : new File(myDir));

            out = new FileOutputStream(file, false);
            writer = createWriter(out);
            out = null;

            if (myBufferedIO) {
                bufWriter = new BufferedWriter(writer,
                        (myBufferSize > 0) ? myBufferSize
                                : TempFileAppender.DEF_BUFFERSIZE);
            } else {
                bufWriter = writer;
            }
            writer = null;

            qw = new QuietWriter(bufWriter, errorHandler);
            bufWriter = null;
        } catch (IOException ex) {
            errorHandler.error("Appender initialization failed.", ex,
                    ErrorCode.FILE_OPEN_FAILURE);
            return;
        } finally {
            if (out != null) {
                IOUtils.close(out);
                out = null;
            }
            if (writer != null) {
                IOUtils.close(writer);
                writer = null;
            }
            if (bufWriter != null) {
                IOUtils.close(bufWriter);
                bufWriter = null;
            }
        }

        writeHeader();
    }

    /**
     * Rilascia le risorse.
     */
    @Override
    protected void reset() {
        if (qw != null) {
            IOUtils.close(qw);
            qw = null;
        }

        super.reset();
    }
}

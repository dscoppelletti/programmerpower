/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import java.util.*;
import org.apache.log4j.helpers.*;
import it.scoppelletti.diagnostic.*;

/**
 * Parser dei modelli di formattazione di un evento di log.
 */
final class PluggablePatternParser extends PatternParser {

    /**
     * Costruttore.
     * 
     * @param pattern Modello di formattazione.
     */
    PluggablePatternParser(String pattern) {
        super(pattern);
    }

    /**
     * Parser di un carattere di conversione.
     * 
     * @param c Carattere di conversione.
     */
    @Override
    protected void finalizeConverter(char c) {
        PatternConverter pc;

        pc = parseConverter(c);
        if (pc != null) {
            currentLiteral.setLength(0);
            addConverter(pc);
            return;
        }

        super.finalizeConverter(c);
    }

    /**
     * Contributo dei plug-in al parser di un carattere di conversione.
     * 
     * @param  c Carattere di conversione.
     * @return   Implementazione del carattere di conversione. Se il carattere
     *           di conversione non &egrave; riconosciuto da nessun plug-in,
     *           restituisce {@code null}.
     */
    private PatternConverter parseConverter(char c) {
        DiagnosticPlugin plugin;
        PatternConverter pc;
        PatternLayoutPlugin parser;
        PatternConversionSpecifier specifier;
        Iterator<DiagnosticPlugin> it;

        specifier = new PluggablePatternParser.ConversionSpecifier(c, this,
                formattingInfo);
        it = DiagnosticSystem.getInstance().getPlugins();
        while (it.hasNext()) {
            plugin = it.next();
            if (!(plugin instanceof PatternLayoutPlugin)) {
                continue;
            }

            parser = (PatternLayoutPlugin) plugin;
            pc = parser.parse(specifier);
            if (pc != null) {
                return pc;
            }
        }

        return null;
    }

    /**
     * Specificatore di conversione. 
     */
    private static final class ConversionSpecifier implements
            PatternConversionSpecifier {
        private final char myChar;
        private final PluggablePatternParser myParser;
        private final FormattingInfo myInfo;

        /**
         * Costruttore.
         * 
         * @param c      Carattere di conversione.
         * @param parser Parser dei modelli di formattazione di un evento di
         *               log.
         * @param info   Informazioni sui modificatori del carattere di 
         *               conversione in esame.
         */
        ConversionSpecifier(char c, PluggablePatternParser parser,
                FormattingInfo info) {
            myChar = c;
            myParser = parser;
            myInfo = info;
        }

        public char getCharacter() {
            return myChar;
        }

        public FormattingInfo getInfo() {
            return myInfo;
        }

        public String extractOption() {
            return myParser.extractOption();
        }

        public int extractPrecisionOption() {
            return myParser.extractPrecisionOption();
        }
    }
}

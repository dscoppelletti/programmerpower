/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import java.util.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import it.scoppelletti.diagnostic.*;

/**
 * Componente {@code Layout} composto da una sequenza di {@code Layout} definiti
 * attraverso dei plug-in {@code LayoutPlugin}.
 * 
 * @see   it.scoppelletti.diagnostic.log4j.LayoutPlugin
 * @since 1.0.0
 */
public final class CompositeLayout extends Layout {

    /**
     * Carattere separatore nell&rsquo;elenco dei nomi dei plug-in
     * {@code LayoutPlugin} che definisce la sequenza dei {@code Layout}. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see #setLayouts
     */
    public static final String SEPARATOR = ":";

    private String myPlugins;
    private List<Layout> myLayouts;
    private boolean myLineSeparator;

    /**
     * Costruttore.
     */
    public CompositeLayout() {
        myLayouts = new ArrayList<Layout>();
        myLineSeparator = false;
    }

    /**
     * Attiva le opzioni impostate.
     */
    @Override
    public void activateOptions() {
        String[] names;
        DiagnosticPlugin plugin;
        Layout layout;
        LayoutPlugin layoutPlugin;

        myLayouts.clear();

        if (Strings.isNullOrEmpty(myPlugins)) {
            names = null;
        } else {
            names = myPlugins.split(CompositeLayout.SEPARATOR);
        }
        if (names == null || names.length == 0) {
            LogLog.warn("No layout sequence set in CompositeLayouts.");
            return;
        }

        for (String name : names) {
            plugin = DiagnosticSystem.getInstance().getPlugin(name);
            if (plugin == null) {    
                continue;
            }

            if (!(plugin instanceof LayoutPlugin)) {
                LogLog.warn(String.format(
                        "Ignoring plug-in %1$s that is not a layout.", name));
                continue;
            }

            layoutPlugin = (LayoutPlugin) plugin;
            layout = layoutPlugin.getLayout();
            if (layout == null) {
                LogLog.warn(String.format(
                        "Ignoring plug-in %1$s that has not a layout.", name));
                continue;
            }

            myLayouts.add(layout);
        }
        if (myLayouts.size() == 0) {
            LogLog.warn("No layout sequence set in CompositeLayout.");
            return;
        }
    }

    /**
     * Restituisce la sequenza dei {@code Layout}.
     * 
     * @return Elenco dei nomi dei plug-in {@code LayoutPlugin} separati dal
     *         carattere {@code ":"}.
     * @see    #setLayouts
     */
    public String getLayouts() {
        return myPlugins;
    }

    /**
     * Imposta la sequenza dei {@code Layout}.
     * 
     * @param value Elenco dei nomi dei plug-in {@code LayoutPlugin} separati
     *              dal carattere {@code ":"}.
     * @see         #getLayouts
     */
    public void setLayouts(String value) {
        myPlugins = (value == null) ? null : value.trim();
    }

    /**
     * Restituisce l&rsquo;indicatore di intercalazione del separatore di linea
     * dopo ogni {@code Layout}.
     * 
     * @return Valore.
     * @see    #setLineSeparator
     */
    public boolean getLineSeparator() {
        return myLineSeparator;
    }

    /**
     * Imposta l&rsquo;indicatore di intercalazione del separatore di linea dopo
     * ogni layout.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see                               #getLineSeparator 
     */
    public void setLineSeparator(boolean value) {
        myLineSeparator = value;
    }

    /**
     * Indica se il {@code Layout} ignora l&rsquo;eventuale eccezione inclusa
     * negli eventi di log.
     * 
     * @return Indicatore.
     */
    @Override
    public boolean ignoresThrowable() {
        for (Layout layout : myLayouts) {
            if (!layout.ignoresThrowable()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Restituisce l&rsquo;intestazione del layout.
     * 
     * @return Stringa.
     */
    @Override
    public String getHeader() {
        String s;
        StringBuilder buf = new StringBuilder();

        for (Layout layout : myLayouts) {
            s = layout.getHeader();
            if (Strings.isNullOrEmpty(s)) {
                continue;
            }

            if (myLineSeparator && buf.length() > 0) {
                buf.append(Layout.LINE_SEP);
            }
            buf.append(s);
        }
        if (myLineSeparator && buf.length() > 0) {
            buf.append(Layout.LINE_SEP);
        }
        if (buf.length() == 0) {
            return null;
        }

        return buf.toString();
    }

    /**
     * Applica il layout ad un evento di log.
     * 
     * @param  event Evento.
     * @return       Stringa.     
     */
    @Override
    public String format(LoggingEvent event) {
        String s;
        StringBuilder buf = new StringBuilder();

        for (Layout layout : myLayouts) {
            s = layout.format(event);
            if (Strings.isNullOrEmpty(s)) {
                continue;
            }

            if (myLineSeparator && buf.length() > 0) {
                buf.append(Layout.LINE_SEP);
            }
            buf.append(s);
        }
        if (myLineSeparator && buf.length() > 0) {
            buf.append(Layout.LINE_SEP);
        }
        if (buf.length() == 0) {
            return null;
        }

        return buf.toString();
    }

    /**
     * Restituisce il piede del layout.
     * 
     * @return Stringa.
     */
    @Override
    public String getFooter() {
        String s;
        StringBuilder buf = new StringBuilder();

        for (Layout layout : myLayouts) {
            s = layout.getFooter();
            if (Strings.isNullOrEmpty(s)) {
                continue;
            }

            if (myLineSeparator && buf.length() > 0) {
                buf.append(Layout.LINE_SEP);
            }
            buf.append(s);
        }
        if (myLineSeparator && buf.length() > 0) {
            buf.append(Layout.LINE_SEP);
        }
        if (buf.length() == 0) {
            return null;
        }

        return buf.toString();
    }
}

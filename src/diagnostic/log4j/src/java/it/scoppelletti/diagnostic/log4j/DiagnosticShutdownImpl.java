/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import org.apache.log4j.*;

/**
 * Rilascio delle risorse della libreria
 * <ACRONYM TITLE="Logging for Java">log4j</ACRONYM>.
 * 
 * @see   it.scoppelletti.diagnostic.DiagnosticSystem#shutdown
 * @since 1.0.0
 */
public final class DiagnosticShutdownImpl implements Runnable {

    /**
     * Costruttore.
     */
    public DiagnosticShutdownImpl() {        
    }
    
    /**
     * Esegue l&rsquo;operazione.
     * 
     * <P>Questa implementazione del metodo {@code run} esegue il metodo
     * {@code shutdown} della classe statica {@code LogManager} della libreria
     * log4j. Eventuali emissioni di eventi di log successive
     * all&rsquo;esecuzione del metodo {@code shutdown} causano delle
     * segnalazioni sul flusso {@code stderr}.</P> 
     */
    public void run() {
        LogManager.shutdown();
    }    
}

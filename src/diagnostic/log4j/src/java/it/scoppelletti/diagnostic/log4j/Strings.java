/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

/** 
 * Funzioni di utilit&agrave; sulle stringhe.
 */
final class Strings {

    /**
     * Costruttore privato per classe statica.
     */
    private Strings() {
    }

    /**
     * Verifica se una stringa &egrave; {@code null} oppure una stringa vuota
     * {@code ""}.
     * 
     * @return Esito della verifica.
     */
    public static boolean isNullOrEmpty(String s) {
        return (s == null || s.isEmpty());
    }
}

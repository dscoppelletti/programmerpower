/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import java.util.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import it.scoppelletti.diagnostic.*;

/**
 * Plug-in dei componenti {@code PluggablePatternLayout} che implementa lo
 * specificatore di conversione per l&rsquo;emissione della rappresentazione
 * come stringa degli oggetti registrati nel contesto di diagnosi del thread
 * corrente.
 * 
 * <P>Supponendo che il {@linkplain #setConversionCharacter carattere di
 * conversione} impostato sia {@code D}, lo specificatore di conversione
 * {@code %D} &egrave; sostituito con l&rsquo;elenco degli oggetti registrati
 * nel contesto di diagnosi del thread corrente intercalati dal separatore di
 * linea.<BR>
 * Indicando un modificatore di formato, ad esempio con lo specificatore
 * <CODE>%D&#123;;&#125;</CODE>, tale stringa (nell&rsquo;esempio {@code ";"})
 * &egrave; utilizzata come separatore nell&rsquo;elenco degli oggetti.</P>
 * 
 * <P>La rappresentazione come stringa delle classi di oggetto pu&ograve;
 * essere configurata con l&rsquo;utilizzo dei componenti
 * <ACRONYM TITLE="Logging for Java">log4j</ACRONYM> {@code ObjectRenderer}.</P>
 * 
 * @see   it.scoppelletti.diagnostic.log4j.PluggablePatternLayout
 * @see   it.scoppelletti.diagnostic.DiagnosticSystem#addDiagnosticContextObject
 * @since 1.0.0
 */
public final class DiagnosticContextPatternPlugin extends AbstractPlugin
        implements PatternLayoutPlugin {

    /**
     * Carattere di conversione di default. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see #setConversionCharacter
     */
    public static final String DEF_CONVERSIONCHAR = "D";

    private String myChar;

    /**
     * Costruttore.
     */
    public DiagnosticContextPatternPlugin() {
        super(false);
        myChar = DiagnosticContextPatternPlugin.DEF_CONVERSIONCHAR;
    }

    /**
     * Attiva le opzioni impostate.
     */
    @Override
    protected void onActivateOptions() {
        if (Strings.isNullOrEmpty(myChar)) {
            LogLog.warn(String.format("Ignoring PatternLayoutPluing %1$s " +
                    "without conversion character.", getName()));
        }
    }

    /**
     * Restituisce il carattere di conversione gestito.
     * 
     * @return Valore.
     * @see    #setConversionCharacter
     */
    public String getConversionCharacter() {
        return myChar;
    }

    /**
     * Imposta il carattere di conversione gestito.
     * 
     * @param                        s Valore.
     * @it.scoppelletti.tag.required
     * @it.scoppelletti.tag.default    {@code D}
     * @see                            #getConversionCharacter
     */
    public void setConversionCharacter(String s) {
        s = (s == null) ? null : s.trim();
        if (Strings.isNullOrEmpty(s)) {
            myChar = null;
        } else {
            myChar = s.substring(0, 1);
        }
    }

    public PatternConverter parse(PatternConversionSpecifier c) {
        String sep;
        PatternConverter pc;
        
        if (c == null) {
            throw new NullPointerException("Argument c is null.");
        }
        if (Strings.isNullOrEmpty(myChar)) {
            return null;
        }

        if (c.getCharacter() != myChar.charAt(0)) {
            return null;
        }

        sep = c.extractOption();
        if (Strings.isNullOrEmpty(sep)) {
            sep = Layout.LINE_SEP;
        }

        pc = new DiagnosticContextPatternPlugin.Converter(sep);

        return pc;
    }

    /**
     * Convertitore.
     */
    private static final class Converter extends PatternConverter {
        private final String mySep;

        /**
         * Costruttore.
         * 
         * @param sep Separatore intercalato tra gli oggetti.
         */
        Converter(String sep) {
            mySep = sep;
        }

        /**
         * Applica la conversione ad un evento di log.
         * 
         * @param  event Evento.
         * @return       Stringa.
         */
        @Override
        protected String convert(LoggingEvent event) {
            Object obj;
            String s;
            Iterator<Object> it;
            RendererSupport renderer;
            StringBuilder buf = new StringBuilder();

            it = DiagnosticSystem.getInstance().getDiagnosticContext();
            while (it.hasNext()) {
                obj = it.next();
                if (obj == null) {
                    continue;
                }

                try {
                    renderer = (RendererSupport) LogManager.getLogger(
                            event.getLoggerName()).getLoggerRepository();
                    s = renderer.getRendererMap().findAndRender(obj);
                } catch (Exception ex) {
                    LogLog.error(String.format(
                            "Failed to render object %1$s.", obj), ex);
                    s = obj.toString();
                }
                if (Strings.isNullOrEmpty(s)) {
                    continue;
                }

                if (mySep != null && buf.length() > 0) {
                    buf.append(mySep);
                }
                buf.append(s);
            }

            return buf.toString();
        }
    }
}

/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import it.scoppelletti.diagnostic.*;

/**
 * Plug-in della libreria <ACRONYM TITLE="Logging for Java">log4j</ACRONYM>.
 * 
 * <P>I formati dei file di configurazione di log4j (sia file di
 * propriet&agrave; che documenti
 * <ACRONYM TITLE="eXtensible Markup Language">XML</ACRONYM>) non supportano la
 * configurazione di componenti JavaBeans generici differenti dalle classi di
 * componenti specifici dello stesso log4j ({@code Appender}, {@code Layout},
 * {@code Logger}, etc), e, anche per quest&rsquo;ultimi, le propriet&agrave;
 * dei componenti possono essere praticamente solo di un tipo primitivo a meno
 * che si tratti di un componente log4j previsto specificatamente dal formato
 * della configurazione.</P>
 * 
 * <P>Le classi derivate dalla classe {@code AbstractPlugin} rappresentano dei
 * componenti log4j {@code Appender} <I>fittizi</I>, ovvero dei componenti che
 * in realt&agrave; non emettono gli eventi di log, bens&igrave; rappresentano
 * dei plug-in registrati nel sistema di logging {@code LogSystem} e quindi
 * utilizzabili dai componenti log4j personalizzati.</P>
 * 
 * <BLOCKQUOTE CLASS="note">
 * La procedura di configurazione della libreria log4j inizializza i componenti
 * {@code Appender} solo se questi sono riferiti almeno da un componente
 * {@code Logger}. Per questo motivo &egrave; uso configurare un {@code Logger}
 * non utilizzato per emettere eventi di log, ma solo per riferire i componenti
 * {@code Appender} che costituiscono dei plug-in in modo da inizializzarli.
 * </BLOCKQUOTE>
 * 
 * <BLOCKQUOTE><PRE>
 * &lt;log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/"&gt;
 *     ...
 *     &lt;appender name="dc" class="it.scoppelletti.diagnostic.log4j.DiagnosticContextPatternPlugin" /&gt;
 *     ...
 *     &lt;logger name="plugins" additivity="false"&gt;
 *         &lt;level value="NONE" /&gt;
 *         ...
 *         &lt;appender-ref ref="dc" /&gt;
 *         ...
 *     &lt;/logger&gt;
 *     ...
 * &lt;/log4j:configuration&gt;     
 * </PRE></BLOCKQUOTE>
 * 
 * @see   it.scoppelletti.diagnostic.DiagnosticSystem
 * @since 1.0.0
 */
public abstract class AbstractPlugin implements DiagnosticPlugin, Appender,
        OptionHandler {
    private String myName;
    private boolean myRequiresLayout;
    private Layout myLayout;
    private ErrorHandler myErrorHandler;

    /**
     * Costruttore.
     * 
     * @param requiresLayout Indica se il plug-in richiede un componente
     *                       {@code Layout}.
     */
    protected AbstractPlugin(boolean requiresLayout) {
        myErrorHandler = new OnlyOnceErrorHandler();
        myRequiresLayout = requiresLayout;
        myLayout = null;
    }

    /**
     * Attiva le opzioni impostate.
     */
    public final void activateOptions() {
        if (Strings.isNullOrEmpty(myName)) {
            LogLog.warn("Ignoring anonymous plugin.");
            return;
        }
        if (myRequiresLayout && myLayout == null) {
            LogLog.warn(String.format(
                    "Required layout not set in plug-in %1$s.", myName));
        }

        try {
            onActivateOptions();
        } catch (Exception ex) {
            LogLog.error(String.format(
                    "Option activation for plug-in %1$s failed", myName), ex);
            return;
        }

        DiagnosticSystem.getInstance().addPlugin(this);
    }

    /**
     * Attiva le opzioni impostate.
     * 
     * <P>Le classi derivate possono implementare le proprie impostazioni
     * specifiche in una versione prevalente del metodo
     * {@code onActivateOptions}.<BR>
     * L&rsquo;implementazione di default del metodo {@code onActivateOptions}
     * non esegue nulla.</P>
     */
    protected void onActivateOptions() {
    }

    /**
     * Rilascia le risorse.
     */
    public void close() {
    }

    /**
     * Restituisce il nome del plug-in.
     * 
     * @return Valore. 
     * @see    #setName
     */
    public String getName() {
        return myName;
    }

    /**
     * Imposta il nome del plug-in.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     * @see                                #getName
     */
    public void setName(String value) {
        myName = value;
    }

    /**
     * Restituisce il gestore degli errori.
     * 
     * @return Oggetto.
     * @see    #setErrorHandler
     */
    public ErrorHandler getErrorHandler() {
        return myErrorHandler;
    }

    /**
     * Imposta il gestore degli errori.
     * 
     * @param                       obj Oggetto.
     * @it.scoppelletti.tag.default     Oggetto {@code OnlyOnceErrorHandler}.
     * @see                             #getErrorHandler
     */
    public void setErrorHandler(ErrorHandler obj) {
        if (obj == null) {
            LogLog.error("Argument obj is null.");
            return;
        }

        myErrorHandler = obj;
    }

    /**
     * Restituisce il primo filtro.
     * 
     * @return Oggetto.
     * @see    #addFilter
     * @see    #clearFilters
     */
    public final Filter getFilter() {
        return null;
    }

    /**
     * Aggiunge un filtro.
     * 
     * <P>I plug-in non utilizzano i filtri.</P>
     * 
     * @param obj Oggetto. 
     * @see       #getFilter
     * @see       #clearFilters
     */
    public final void addFilter(Filter obj) {
    }

    /**
     * Rimuove i filtri.
     * 
     * @see #getFilter
     * @see #addFilter 
     */
    public final void clearFilters() {
    }

    /**
     * Indica se il plug-in richiede un componente {@code Layout}.
     * 
     * @return Indicatore.
     */
    public final boolean requiresLayout() {
        return myRequiresLayout;
    }

    /**
     * Restituisce il componente {@code Layout}.
     * 
     * @return Oggetto.
     * @see    #requiresLayout
     * @see    #setLayout
     */
    public final Layout getLayout() {
        return myLayout;
    }

    /**
     * Imposta il componente {@code Layout}.
     * 
     * @param obj Oggetto.
     * @see       #getLayout
     * @see       #requiresLayout
     */
    public final void setLayout(Layout obj) {
        myLayout = obj;
    }

    /**
     * Emette un evento di log.
     *
     * <P>Questa implementazione del metodo {@code doAppend} non esegue
     * nulla.</P>
     * 
     * @param event Oggetto.
     */
    public final void doAppend(LoggingEvent event) {
    }
}

/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import org.apache.log4j.helpers.*;

/**
 * Specificatore di conversione nel modello di formattazione di un componente
 * {@code PluggablePatternLayout}.
 * 
 * @see   it.scoppelletti.diagnostic.log4j.PatternLayoutPlugin
 * @see   it.scoppelletti.diagnostic.log4j.PluggablePatternLayout 
 * @since 1.0.0
 */
public interface PatternConversionSpecifier {

    /**
     * Restituisce il carattere di conversione.
     * 
     * @return Valore.
     */
    char getCharacter();

    /**
     * Restituisce le informazioni sui modificatori del carattere di conversione
     * in esame.
     * 
     * @return Oggetto.
     */
    FormattingInfo getInfo();

    /**
     * Estrae l&rsquo;eventuale opzione del carattere di conversione in esame.
     * 
     * @return Valore.
     */
    public String extractOption();

    /**
     * Estrae l&rsquo;eventuale opzione di precisione del carattere di
     * conversione in esame.
     * 
     * @return Valore.
     */
    public int extractPrecisionOption();
}

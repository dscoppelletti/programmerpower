/*
 * Copyright (C) 2011 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.diagnostic.log4j;

import org.apache.log4j.helpers.*;
import it.scoppelletti.diagnostic.*;

/**
 * Plug-in utilizzato dai componenti {@code PluggablePatternLayout} per il
 * riconoscimento dei specificatori di conversione personalizzati.
 * 
 * @see   it.scoppelletti.diagnostic.log4j.PluggablePatternLayout
 * @since 1.0.0
 */
public interface PatternLayoutPlugin extends DiagnosticPlugin {

    /**
     * Esegue il parser di un specificatore di conversione.
     * 
     * @param  c Specificatore di conversione.
     * @return   Implementazione del specificatore di conversione. Se lo 
     *           specificatore di conversione non &egrave; riconosciuto dal
     *           plug-in, restituisce {@code null}.
     */
    PatternConverter parse(PatternConversionSpecifier c);
}

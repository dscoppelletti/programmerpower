/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Parametri per la lettura di un modulo.
 * 
 * @see   it.scoppelletti.sdk.buildlib.runtime.ModuleType
 * @since 1.0.0
 */
public abstract class ModuleLocation extends ProjectComponent {

    /**
     * Costruttore.
     */
    protected ModuleLocation() {        
    }

    /**
     * Verifica la validit&agrave; della combinazione di attributi e
     * sotto-elementi impostata sul componente.
     * 
     * @throws org.apache.tools.ant.BuildException Combinazione non valida. 
     */
    abstract void checkProps();
    
    /**
     * Legge il modulo.
     * 
     * @param  repo Repository.
     * @return      Modulo.
     */
    abstract Module loadData(ModuleRepository repo);
}

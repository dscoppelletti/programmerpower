/*
 * Copyright (C) 2010-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Componenti Ant per l&rsquo;integrazione con il runtime di Programmer Power.
 * 
 * @it.scoppelletti.tag.module {@code it.scoppelletti.sdk.buildlib}
 * @version                    1.2.0
 * @see <A HREF="{@docRoot}/it/scoppelletti/runtime/package-summary.html"><CODE>it.scoppelletti.runtime</CODE></A>
 */
package it.scoppelletti.sdk.buildlib.runtime.filters;

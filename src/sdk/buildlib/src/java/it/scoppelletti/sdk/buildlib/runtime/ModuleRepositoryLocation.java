/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Parametri per la lettura di un modulo installato in un repository.
 * 
 * @see   it.scoppelletti.sdk.buildlib.runtime.ModuleType
 * @see   it.scoppelletti.runtime.ModuleRepository#loadModule(SymbolicName, Version)
 * @since 1.0.0
 */
public final class ModuleRepositoryLocation extends ModuleLocation {
    private SymbolicName myName = null;
    private Version myVersion = null;
    
    /**
     * Costruttore.
     */
    public ModuleRepositoryLocation() {        
    }
   
    /**
     * Imposta il nome del modulo.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     */
    public void setName(String value) {        
        try {
            myName = new SymbolicName(value);
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }        
    }
       
    /**
     * Imposta la versione del modulo.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required                                                                         
     */
    public void setVersion(String value) {
        try {
            myVersion = new Version(value);
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }                
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("name=%1$s; version=%2$s", myName, myVersion);
    }
    
    @Override
    void checkProps() {
        if (myName == null) {
            throw new BuildException("Missing attributes name.", getLocation());            
        }
        if (myVersion == null) {
            throw new BuildException("Missing attribute version.",
                    getLocation());                        
        }        
    }
    
    @Override
    Module loadData(ModuleRepository repo) {
        String msg;
        Module module;
        
        if (repo == null) {
            throw new NullPointerException("Argument repo is null.");
        }
        
        module = repo.loadModule(myName, myVersion);
        if (module == null) {
            msg = String.format("Module (%1$s, %2$s) not found.", myName,
                    myVersion);
            throw new RuntimeEnvironmentException(msg);
        }
        
        return module;
    }    
}

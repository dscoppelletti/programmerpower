/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.scoppelletti.sdk.buildlib;

import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;

/**
 * Converte un componente {@code SDKClasspathType} in un class-path {@code Path}
 * per la ricerca del solo codice binario.
 * 
 * @see   it.scoppelletti.sdk.buildlib.SDKClasspathType
 * @since 1.1.0
 */
public final class SDKClasspathToClasspathTask extends Task implements
        Cloneable {
    private String myOutput = null; 
    private SDKClasspathType myClasspath = null;
    
    /**
     * Costruttore.
     */
    public SDKClasspathToClasspathTask() {        
    }    
    
    /**
     * Imposta il riferimento all&rsquo;oggetto {@code Path} sul quale acquisire
     * il class-path del codice binario.
     *
     * @param                        value Nome del riferimento.
     * @it.scoppelletti.tag.required
     */
    public void setClasspath(String value) {
       myOutput = value;
    }
    
    /**
     * Imposta il classpath.
     * 
     * @param obj Oggetto.
     */
    public void addClasspath(SDKClasspathType obj) {        
        if (myClasspath != null) {
            throw new BuildException("Cannot define more than one classpath.",
                    getLocation());
        }
        
        myClasspath = obj;        
    }   
        
    /**
     * Esegue il task.
     */
    @Override
    public void execute() throws BuildException {
        Path path;
        Path.PathElement el;
        
        if (myOutput == null) {
            throw new BuildException("Missing attribute classpath.",
                    getLocation());
        }
        if (myClasspath == null) {
            throw new BuildException("Missing element <sdkclasspath>.",
                    getLocation());
        }        
        
        path = new Path(getProject());
        for (SDKClasspathEntry entry : myClasspath.listEntries()) {
            if (entry.getPath() == null) {                
                log("Missing attribute path in element <classpathentry>.",
                        Project.MSG_WARN);
                continue;
            }          

            el = path.createPathElement();
            el.setLocation(entry.getPath());
        }
        
        getProject().addReference(myOutput, path);
    }    
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        SDKClasspathToClasspathTask target;
        
        target = (SDKClasspathToClasspathTask) super.clone();
        target.initClone();
        if (myClasspath != null) {            
            target.addClasspath((SDKClasspathType) myClasspath.clone());
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myClasspath = null;
    }    
}

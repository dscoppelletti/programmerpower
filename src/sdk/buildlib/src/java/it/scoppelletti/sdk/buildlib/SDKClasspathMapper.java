/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;
import org.apache.tools.ant.types.resources.*;
import org.apache.tools.ant.util.*;

/**
 * Mappatura per associare ad un insieme di moduli
 * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> i corrispondenti archivi del
 * codice sorgente, i percorsi radice del codice sorgente all&rsquo;interno di
 * questi e
 * l&rsquo;<ACRONYIM TITLE="Uniform Resource Locator">URL</ACRONYM> della 
 * documentazione delle
 * <ACRONYIM TITLE="Application Programming Interface">API</ACRONYM>.
 * 
 * @see   it.scoppelletti.sdk.buildlib.SDKClasspathType
 * @since 1.1.0
 */
public class SDKClasspathMapper extends ProjectComponent implements 
        SDKClasspath {
    private Union myResources = null;
    private SDKClasspathMapper.SourceMapper mySourceMapper = null;
    private SDKClasspathMapper.SourceRootMapper mySourceRootMapper = null;
    private SDKClasspathMapper.DocURLMapper myDocURLMapper = null;
    
    /**
     * Costruttore.
     */
    public SDKClasspathMapper() {
    }  
    
    /**
     * Aggiunge un insieme di moduli JAR (o direttori del codice binario).
     * 
     * @param set Collezione.
     */
    public void addFileset(FileSet set) {
        add(set);
    }
    
    /**
     * Aggiunge un insieme di moduli JAR (o direttori del codice binario).
     * 
     * @param set Collezione.
     */
    public void add(ResourceCollection set) {        
        if (set == null) {
            throw new NullPointerException("Argument set is null.");
        }

        if (myResources == null) {
            myResources = new Union();            
        }
        
        myResources.add(set);
    }
    
    /**
     * Inizializza la mappatura per determinare il percorso dell&rsquo;archivio
     * del codice sorgente associato a ciascun modulo JAR (o direttorio del
     * codice binario).<BR>
     * Se la mappatura non &egrave; impostata, ai moduli JAR non sar&agrave;
     * associato l&rsquo;archivio del codice sorgente.
     * 
     * @return Oggetto.
     */
    public SDKClasspathMapper.SourceMapper createSourceMapper() {        
        if (mySourceMapper != null) {
            throw new BuildException(
                    "Cannot define more than one source-mapper.",
                    getLocation());  
        }
        
        mySourceMapper = new SDKClasspathMapper.SourceMapper(getProject(),
                getLocation());
        
        return mySourceMapper;
    }
    
    /**
     * Inizializza la mappatura per determinare il percorso radice del codice
     * sorgente all&rsquo;interno dell&rsquo;apposito archivio associato a
     * ciascun modulo JAR (o direttorio del codice binario).<BR>
     * Se la mappatura non &egrave; impostata, i percorso radice del codice
     * sorgente all&rsquo;interno degli appositi archivi dovr&agrave; essere
     * rilevato automaticamente.
     * 
     * @return Oggetto.
     */
    public SDKClasspathMapper.SourceRootMapper createSourceRootMapper() {        
        if (mySourceRootMapper != null) {
            throw new BuildException(
                    "Cannot define more than one source-root-mapper.",
                    getLocation());                        
        }
        
        mySourceRootMapper = new SDKClasspathMapper.SourceRootMapper(
                getProject(), getLocation());
        
        return mySourceRootMapper;
    }
    
    /**
     * Inizializza la mappatura per determinare l&rsquo;URL della documentazione
     * delle API associata a ciascun modulo JAR (o direttorio del codice
     * binario).<BR>
     * Se la mappatura non &egrave; impostata, ai moduli JAR non sar&agrave;
     * associata la documentazione API. 
     * 
     * @return Oggetto.
     */
    public SDKClasspathMapper.DocURLMapper createDOCURLMapper() {        
        if (myDocURLMapper != null) {
            throw new BuildException(
                    "Cannot define more than one doc-url-mapper.",
                    getLocation());                        
        }
        
        myDocURLMapper = new SDKClasspathMapper.DocURLMapper(
                getProject(), getLocation());
        
        return myDocURLMapper;
    }
    
    public List<SDKClasspathEntry> listEntries() {
        Resource res;
        FileResource fileRes;
        FileNameMapper sourceMapper, sourceRootMapper, docURLMapper;
        SDKClasspathEntry entry;
        List<SDKClasspathEntry> list;
        
        @SuppressWarnings("rawtypes")
        Iterator it;

        sourceMapper = null;
        sourceRootMapper = null;
        docURLMapper = null;
        if (mySourceMapper != null) {
            sourceMapper = mySourceMapper.getMapper();
            if (sourceMapper == null) {
                throw new BuildException("Missing sub-element <mapper> in " +
                        "element <sourcemapper>.", getLocation());
            }
            
            if (mySourceRootMapper != null) {
                sourceRootMapper = mySourceRootMapper.getMapper();
                if (sourceRootMapper == null) {
                    throw new BuildException("Missing sub-element <mapper> " +
                            "in element <sourcerootmapper>.", getLocation());
                }                
            }
        } else if (mySourceRootMapper != null) {
            throw new BuildException("Missing element <sourcemapper>.",
                    getLocation());            
        }
        if (myDocURLMapper != null) {
            docURLMapper = myDocURLMapper.getMapper();
            if (docURLMapper == null) {
                throw new BuildException("Missing sub-element <mapper> in " +
                        "element <docurlmapper>.", getLocation());                
            }
        }
        
        list = new ArrayList<SDKClasspathEntry>();
        it = myResources.iterator();
        while (it.hasNext()) {
            res = (Resource) it.next();
            if (!res.isExists()) {
                continue;
            }
            
            if (!(res instanceof FileResource)) {
                throw new BuildException("Only filesystem base resources are " +
                        "supported by this component.", getLocation());
            }
            
            fileRes = (FileResource) res;
            
            try {
                entry = newEntry(fileRes.getFile(), sourceMapper,
                        sourceRootMapper, docURLMapper);
            } catch (Exception ex) {
                throw new BuildException(ex.getMessage(), ex, getLocation());
            }
            
            list.add(entry);            
        }
        
        return list;
    }
     
    /**
     * Instanzia un elemento del class-path.
     * 
     * @param  file             Modulo JAR (o direttorio del codice binario).
     * @param  sourceMapper     Mappatura per determinare il percorso
     *                          dell&rsquo;archivio del codice sorgente
     *                          associato al modulo {@file}.
     * @param  sourceRootMapper Mappatura per determinare il percorso radice del 
     *                          codice sorgente all&rsquo;interno dell&rsquo;
     *                          archivio determinato attraverso la mappatura
     *                          {@code sourceMapper}.
     * @param  docURLMapper     Mappatura per determinare l&rsuqo;URL della
     *                          documentazione API associata al modulo
     *                          {@code file}.
     * @return                  Oggetto.
     */
    private SDKClasspathEntry newEntry(File file, FileNameMapper sourceMapper,
            FileNameMapper sourceRootMapper, FileNameMapper docURLMapper) throws 
            MalformedURLException {
        String sourceRootPath;
        File sourceFile;
        URL docURL;
        SDKClasspathEntry entry;        
        String[] mappedNames;
        
        entry = new SDKClasspathEntry();
        entry.setPath(file);
        
        if (sourceMapper != null) {
            mappedNames = sourceMapper.mapFileName(file.getPath());
            if (mappedNames != null && mappedNames.length > 0) {
                sourceFile = new File(mappedNames[0]);
                entry.setSourcePath(sourceFile);
                
                if (sourceRootMapper != null) {
                    mappedNames = sourceRootMapper.mapFileName(file.getPath());
                    if (mappedNames != null && mappedNames.length > 0) {
                        sourceRootPath = mappedNames[0];
                        entry.setSourceRootPath(sourceRootPath);                        
                    }
                }
            }
        }
        if (docURLMapper != null) {
            mappedNames = docURLMapper.mapFileName(file.getPath());
            if (mappedNames != null && mappedNames.length > 0) {
                docURL = new URL(mappedNames[0]);
                entry.setDocURL(docURL);
            }
        }
        
        return entry;
    }
    
    /**
     * Mappatura per la determinazione del percorso dell&rsquo;archivio del
     * codice sorgente associato ad un modulo
     * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> (o direttorio del codice
     * binario).
     * 
     * @since 1.1.0
     */
    public static final class SourceMapper extends MapperElement {
        
        /**
         * Costruttore.
         * 
         * @param project  Progetto.
         * @param location Locazione. 
         */
        public SourceMapper(Project project, Location location) {
            super(project, location);
        }        
    }
    
    /**
     * Mappatura per la determinazione del percorso radice del codice sorgente 
     * all&rsquo;interno dell&rsquo;apposito archivio associato ad un modulo
     * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> (o direttorio del codice
     * binario).
     * 
     * @since 1.1.0
     */
    public static final class SourceRootMapper extends MapperElement {
        
        /**
         * Costruttore.
         * 
         * @param project  Progetto.
         * @param location Locazione. 
         */
        public SourceRootMapper(Project project, Location location) {
            super(project, location);
        }        
    }    
    
    /**
     * Mappatura per la determinazione
     * dell&rsquo;<ACRONYIM TITLE="Uniform Resource Locator">URL</ACRONYM> della 
     * documentazione delle
     * <ACRONYM TITLE="Application Programming Interface">API</ACRONYM>
     * associata ad un modulo
     * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> (o direttorio del codice
     * binario).
     * 
     * @since 1.1.0
     */
    public static final class DocURLMapper extends MapperElement {
        
        /**
         * Costruttore.
         * 
         * @param project  Progetto.
         * @param location Locazione. 
         */
        public DocURLMapper(Project project, Location location) {
            super(project, location);
        }        
    }        
}

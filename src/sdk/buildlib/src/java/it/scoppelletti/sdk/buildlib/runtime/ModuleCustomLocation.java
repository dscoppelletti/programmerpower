/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import java.io.*;
import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Parametri per la lettura di un modulo da una struttura personalizzata
 * (es&#46; il direttorio di sviluppo del progetto).
 * 
 * @see   it.scoppelletti.sdk.buildlib.runtime.ModuleType
 * @see   it.scoppelletti.runtime.ModuleRepository#loadModule(File)
 * @since 1.0.0
 */
public final class ModuleCustomLocation extends ModuleLocation {
    private File myBaseDir = null;
    
    /**
     * Costruttore.
     */
    public ModuleCustomLocation() {        
    }
   
    /**
     * Imposta il direttorio di base del modulo.
     * 
     * @param                        dir Direttorio.
     * @it.scoppelletti.tag.required                                    
     */
    public void setBaseDir(File dir) {
        myBaseDir = dir;
    }  
        
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format("basedir=%1$s", myBaseDir);
    }
    
    @Override
    void checkProps() {
        if (myBaseDir == null) {
            throw new BuildException("Missing attribute basedir.",
                    getLocation());                        
        }        
    }    
    
    @Override
    Module loadData(ModuleRepository repo) {
        String msg;
        Module module;
        
        if (repo == null) {
            throw new NullPointerException("Argument repo is null.");
        }
        
        module = repo.loadModule(myBaseDir);
        if (module == null) {
            msg = String.format("Module (%1$s) not found.", myBaseDir);
            throw new RuntimeEnvironmentException(msg);
        }
        
        return module;
    }
}

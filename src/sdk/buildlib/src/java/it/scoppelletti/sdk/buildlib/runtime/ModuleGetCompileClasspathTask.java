/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import java.util.*;
import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;
import it.scoppelletti.sdk.buildlib.*;

/**
 * Acquisisce il class-path corrispondente alle dipendenze per la compilazione
 * di un modulo.
 * 
 * @see   it.scoppelletti.sdk.buildlib.SDKClasspathType 
 * @see   it.scoppelletti.runtime.Module#listCompileClasspathEntries
 * @since 1.1.0 
 */
public final class ModuleGetCompileClasspathTask extends Task implements
        Cloneable {
    private String myClasspath = null;
    private ModuleType myModule = null;
    
    /**
     * Costruttore.
     */
    public ModuleGetCompileClasspathTask() {        
    }    

    /**
     * Imposta il riferimento all&rsquo;oggetto {@code SDKClasspathType} sul
     * quale acquisire le dipendenze per la compilazione del modulo.
     *
     * @param                        value Nome del riferimento.
     * @it.scoppelletti.tag.required
     */
    public void setClasspath(String value) {
       myClasspath = value;
    }

    /**
     * Imposta il modulo.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     */
    public void addModule(ModuleType obj) {        
        if (myModule != null) {
            throw new BuildException("Cannot define more than one module.",
                    getLocation());
        }
        
        myModule = obj;        
    }
        
    /**
     * Esegue il task.
     */
    @Override
    public void execute() throws BuildException {
        Module module;        
        SDKClasspathType classpath;
        SDKClasspathEntry entry;
        List<LibraryClasspathEntry> depList;
        
        if (myClasspath == null) {
            throw new BuildException("Missing attribute classpath.",
                    getLocation());            
        }        
        if (myModule == null) {
            throw new BuildException("Missing element <module>.",
                    getLocation());
        }
                
        try {
            module = myModule.getData();
            depList = module.listCompileClasspathEntries();
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }
                       
        classpath = new SDKClasspathType(getProject());
        classpath.setDescription(getDescription());
        classpath.setLocation(getLocation());
        
        for (LibraryClasspathEntry source : depList) {                       
            entry = new SDKClasspathEntry(source);
            classpath.addEntry(entry);                                   
        }
        
        getProject().addReference(myClasspath, classpath);
    }
        
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ModuleGetCompileClasspathTask target;
        
        target = (ModuleGetCompileClasspathTask) super.clone();
        target.initClone();
        if (myModule != null) {
            target.addModule((ModuleType) myModule.clone());
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myModule = null;
    }   
}

/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Rappresentazione di un elemento di un class-path {@code SDKClasspathType}.
 * 
 * @see   it.scoppelletti.sdk.buildlib.SDKClasspathType
 * @since 1.1.0
 */
public final class SDKClasspathEntry extends ProjectComponent implements 
        SDKClasspath, LibraryClasspathEntry {
    private File myPath = null;
    private File mySourcePath = null;
    private String mySourceRootPath = null;
    private URL myDocURL = null;
    
    /**
     * Costruttore.
     */
    public SDKClasspathEntry() {
    }  
    
    /**
     * Costruttore di copia.
     * 
     * @param source Elemento sorgente.
     */
    public SDKClasspathEntry(LibraryClasspathEntry source) {
        if (source == null) {
            throw new NullPointerException("Argument source is null.");
        }
        
        myPath = source.getPath();
        mySourcePath = source.getSourcePath();
        mySourceRootPath = source.getSourceRootPath();
        myDocURL = source.getDocURL();
    }  
    
    /**
     * Restituisce il percorso del modulo JAR (o del direttorio del codice
     * binario).
     * 
     * @return Percorso assoluto.
     * @see    #setPath 
     */
    public File getPath() {
        return  myPath;        
    }  
    
    /**
     * Imposta il percorso del modulo JAR (o del direttorio del codice binario).
     * 
     * @param                        path Percorso assoluto.
     * @see                               #getPath
     * @it.scoppelletti.tag.required
     */
    public void setPath(File path) {
        myPath = path;        
    }
    
    /**
     * Restituisce il percorso dell&rsquo;archivio del codice sorgente.
     * 
     * @return Percorso assoluto.
     * @see    #setSourcePath
     */
    public File getSourcePath() {
        return  mySourcePath;        
    }  
    
    /**
     * Imposta il percorso dell&rsquo;archivio del codice sorgente.
     * 
     * @param path Percorso assoluto. Pu&ograve; essere {@code null}, se il
     *             codice binario non &egrave; accompagnato dal codice sorgente.
     * @see        #getSourcePath
     */
    public void setSourcePath(File path) {
        mySourcePath = path;        
    }    
    
    /**
     * Restituisce il percorso radice del codice sorgente all&rsquo;interno
     * dell&rsquo;archivio {@code getSourcePath}.
     * 
     * @return Percorso.
     * @see    #setSourceRootPath
     * @see    #getSourcePath
     */
    public String getSourceRootPath() {
        return mySourceRootPath;
    }
    
    /**
     * Imposta il percorso radice del codice sorgente all&rsquo;interno
     * dell&rsquo;archivio {@code getSourcePath}.
     * 
     * @param path Percorso. Pu&ograve; essere {@code null}, se il percorso del
     *             codice sorgente deve essere rilevato automaticamente. 
     * @see        #getSourceRootPath
     * @see        #getSourcePath 
     */
    public void setSourceRootPath(String path) {
        mySourceRootPath = path;        
    }      
    
    /**
     * Restituisce l&rsquo;URL della documentazione API.
     * 
     * @return URL.
     * @see    #setDocURL
     */
    public URL getDocURL() {
        return myDocURL;
    }
    
    /**
     * Imposta l&rsquo;URL della documentazione API.
     *  
     * @param url URL. Pu&ograve; essere {@code null}, se il codice binario non
     *            &egrave; accompagnato dalla documentazione API.
     * @see       #getDocURL
     */
    public void setDocURL(URL url) {
        myDocURL = url;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        return String.format(
                "SDKClasspathEntry(path=%1$s,sourcePath=%2$s," +
                "sourceRootPath=%3$s,docURL=%4$s",
                myPath, mySourcePath, mySourceRootPath, myDocURL);
    }  
    
    public List<SDKClasspathEntry> listEntries() {
       return Collections.<SDKClasspathEntry>singletonList(this);
    }
}

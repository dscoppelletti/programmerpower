/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime.filters;

import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Filtri su un modulo che nega l&rsquo;esito della verifica del filtro
 * sottostante ({@code not}).
 * 
 * @since 1.1.0
 */
public final class ModuleNotFilterType extends ModuleFilterType {
    private ModuleFilterType myFilter = null;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleNotFilterType(Project project) {
        super(project);
    }
        
    @Override
    protected boolean isInline() {
        if (myFilter != null) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Imposta il filtro sottostante.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     */
    public void add(ModuleFilterType obj) {        
        checkAttributesAllowed();
        
        if (myFilter != null) {
            throw new BuildException("Cannot define more than one filter.",
                    getLocation());            
        }
        
        myFilter = obj;
    }
    
    @Override
    protected void doCheckProps() {
        if (myFilter == null) {
            throw new BuildException("Missing filter element.", getLocation());            
        }
        
        myFilter.checkProps();        
    }
    
    @Override
    protected boolean doAccept(Module module) {
        return !myFilter.accept(module);
    }
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ModuleNotFilterType target;
        
        target = (ModuleNotFilterType) super.clone();
        target.initClone();
        if (myFilter != null) {
            target.add((ModuleFilterType) myFilter.clone()); 
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myFilter = null;
    }    
}

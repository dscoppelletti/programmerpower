/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime.filters;

import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;
import it.scoppelletti.runtime.*;

/**
 * Filtro di selezione di un modulo.
 * 
 * @since 1.1.0
 */
public abstract class ModuleFilterType extends DataType implements 
        Cloneable, ModuleFilter {
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    protected ModuleFilterType(Project project) {
        setProject(project);
    }
    
    /**
     * Imposta il riferimento ad un altro componente.
     * 
     * @param id Riferimento.
     * @see      #isInline
     */
    @Override
    public void setRefid(Reference id) {
        if (isInline()) {
            throw tooManyAttributes();
        }
        
        super.setRefid(id);
    }        
    
    /**
     * Verifica se sono stati impostati attributi (diversi dal riferimento ad
     * un altro componente) o aggiunti sotto-elementi.
     * 
     * <P>Il metodo {@code setRefid} utilizza il metodo {@code isInline} per
     * verificare se sul componente sono gi&agrave; stati impostati altri
     * attributi o sotto-elementi e, in questo caso, impedire di impostare il
     * riferimento ad un altro componente.</P>
     *  
     * @return Esito della verifica.
     * @see    #setRefid
     */
    protected abstract boolean isInline();
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (isReference()) {
            return getCheckedRef().toString();
        }
        
        return super.toString();
    }  
     
    /**
     * Verifica la validit&agrave; della combinazione di attributi e
     * sotto-elementi impostata sul componente.
     * 
     * @throws org.apache.tools.ant.BuildException Combinazione non valida. 
     */
    public final void checkProps() {
        ModuleFilterType target;
        
        if (isReference()) {
            target = (ModuleFilterType) getCheckedRef();
            target.checkProps();
        } else {
            doCheckProps();
        }
    }
    
    /**
     * Verifica la validit&agrave; della combinazione di attributi e
     * sotto-elementi impostata sul componente.
     * 
     * @throws org.apache.tools.ant.BuildException Combinazione non valida. 
     */
    protected abstract void doCheckProps();
    
    public final boolean accept(Module module) {
        ModuleFilterType target;
        
        if (isReference()) {
            target = (ModuleFilterType) getCheckedRef();
            return target.accept(module);
        }
        
        return doAccept(module);        
    }
    
    /**
     * Verifica se un modulo deve essere selezionato.
     * 
     * @param  module Modulo.
     * @return        Esito della verifica.
     */    
    protected abstract boolean doAccept(Module module);
}

/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime.filters;

import java.util.*;
import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Combinazione di filtri su un modulo dei quali deve esserne verificato almeno
 * uno ({@code or}).
 * 
 * @since 1.1.0
 */
public final class ModuleOrFilterType extends ModuleFilterType {
    private List<ModuleFilterType> myFilters = null;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleOrFilterType(Project project) {
        super(project);
    }
        
    @Override
    protected boolean isInline() {
        if (myFilters != null) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Aggiunge un filtro.
     * 
     * <P>Il metodo
     * {@link it.scoppelletti.sdk.buildlib.runtime.filters.ModuleFilterType#accept accept}
     * potrebbe non valutare tutti i filtri perch&eacute; valuta ogni filtro
     * fino a quando ne rileva uno per il quale lo stesso metodo {@code accept}
     * restituisce {@code true}; l&rsquo;ordine di valutazione dei filtri non
     * &egrave; determinabile.</P>
     *  
     * @param                        obj Oggetto.  
     * @it.scoppelletti.tag.required
     */
    public void add(ModuleFilterType obj) {        
        if (obj == null) {
            throw new NullPointerException("Argument obj is null.");
        }        
        
        checkAttributesAllowed();
        if (myFilters == null) {
            myFilters = new ArrayList<ModuleFilterType>();            
        }
        myFilters.add(obj);
    }
    
    @Override
    protected void doCheckProps() {
        if (myFilters == null) {
            throw new BuildException("Missing filter elements.", getLocation());            
        }
        
        for (ModuleFilterType filter : myFilters) {
            filter.checkProps();
        }        
    }
    
    @Override
    protected boolean doAccept(Module module) {
        for (ModuleFilterType filter : myFilters) {
            if (filter.accept(module)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ModuleOrFilterType target;
        
        target = (ModuleOrFilterType) super.clone();
        target.initClone();
        if (myFilters != null) {
            for (ModuleFilterType filter : myFilters) {
                target.add((ModuleFilterType) filter.clone());
            }
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myFilters = null;
    }    
}

/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import java.util.*;

/**
 * Class-path che associa ad ogni modulo
 * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> l&rsquo;archivio del codice
 * sorgente e l&rsquo;<ACRONYIM TITLE="Uniform Resource Locator">URL</ACRONYM>
 * della documentazione delle
 * <ACRONYIM TITLE="Application Programming Interface">API</ACRONYM>.
 * 
 * @since 1.1.0
 */
public interface SDKClasspath {

    /**
     * Restituisce gli elementi del class-path.
     * 
     * @return Collezione.
     */
    List<SDKClasspathEntry> listEntries();
}

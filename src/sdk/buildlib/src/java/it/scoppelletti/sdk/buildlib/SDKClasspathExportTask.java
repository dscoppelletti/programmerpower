/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import java.io.*;
import javax.xml.*;
import javax.xml.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.apache.tools.ant.*;

/**
 * Esegue l&rsquo;export di un class-path {@code SDKClasspathType}.
 *  
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/sdk/ide/projects/classpath2.xsd}
 * @see   it.scoppelletti.sdk.buildlib.SDKClasspathType
 * @since 1.1.0
 */
public final class SDKClasspathExportTask extends Task implements Cloneable {
    private File myToFile = null;
    private SDKClasspathType myClasspath = null;
    
    /**
     * Costruttore.
     */
    public SDKClasspathExportTask() {        
    }    
   
    /**
     * Imposta il file di output.
     * 
     * @param                        file File.
     * @it.scoppelletti.tag.required
     */
    public void setToFile(File file) {
        myToFile = file;
    }  
    
    /**
     * Imposta il classpath.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     */
    public void addClasspath(SDKClasspathType obj) {        
        if (myClasspath != null) {
            throw new BuildException("Cannot define more than one classpath.",
                    getLocation());
        }
        
        myClasspath = obj;        
    }   
    
    /**
     * Esegue il task.
     */
    @Override
    public void execute() throws BuildException {
        if (myToFile == null) {
            throw new BuildException("Missing attribute tofile.",
                    getLocation());            
        }
        if (myClasspath == null) {
            throw new BuildException("Missing element <sdkclasspath>.",
                    getLocation());
        }        
        
        try {
            exportClasspath(myToFile);
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }
    }
     
    /**
     * Esegue l&rsquo;export.
     *  
     * @param file File di destinazione.
     */
    private void exportClasspath(File file) throws IOException,
            XMLStreamException, TransformerException,
            TransformerConfigurationException {  
        Writer out;
        XMLStreamWriter writer = null;
        XMLOutputFactory xmlFactory;
        
        xmlFactory = XMLOutputFactory.newInstance();

        out = new StringWriter();
        try {
            writer = xmlFactory.createXMLStreamWriter(out);

            writer.writeStartDocument("1.0");
            writer.writeStartElement("classpath");
            writer.writeDefaultNamespace(
                "http://www.scoppelletti.it/ns/classpath/2");
            writer.writeNamespace("xsi",
                XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);            
            writer.writeAttribute(
                XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI,
                "schemaLocation",
                "http://www.scoppelletti.it/ns/classpath/2 " +
                "http://www.scoppelletti.it/res/sdk/ide/projects/" +
                "classpath2.xsd");
            
            for (SDKClasspathEntry entry : myClasspath.listEntries()) {
                if (entry.getPath() == null) {
                    log("Missing attribute path in element <classpathentry>.",
                            Project.MSG_WARN);
                    continue;
                }
                
                writer.writeStartElement("entry");                
                writer.writeAttribute("path", entry.getPath().getPath());
                
                if (entry.getSourcePath() != null) {
                    writer.writeAttribute("sourcePath",
                            entry.getSourcePath().getPath());
                    
                    if (entry.getSourceRootPath() != null) { 
                        writer.writeAttribute("sourceRootPath",
                                entry.getSourceRootPath());
                    }                                
                }
                if (entry.getDocURL() != null) {
                    writer.writeAttribute("docURL",
                            entry.getDocURL().toString());
                }
                
                writer.writeEndElement();                
            }
            
            writer.writeEndElement();
            writer.writeEndDocument();
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
            out.close();
        }   
        
        formatXmlDocument(out.toString(), file);
    }
    
    /**
     * Formattazione di un documento XML.
     * 
     * @param buf  Buffer del documento.
     * @param file File di destinazione.
     */
    private void formatXmlDocument(String buf, File file) throws IOException,
        TransformerException, TransformerConfigurationException {
        Reader in = null;
        Writer out = null;
        Source source;
        Result result;        
        Transformer tn;
        TransformerFactory tnFactory;
        BuildException loggingEx;
        
        tnFactory = TransformerFactory.newInstance();
        
        try {
            tnFactory.setAttribute("indent-number", 4);
        } catch (IllegalArgumentException ex) {
            loggingEx = new BuildException(ex.getMessage(), ex, getLocation());
            getProject().log(ex.getMessage(), loggingEx, Project.MSG_WARN);            
        }
        
        tn = tnFactory.newTransformer();
        tn.setOutputProperty(OutputKeys.METHOD, "xml");
        tn.setOutputProperty(OutputKeys.INDENT, "yes");        
        
        try {
            in = new StringReader(buf);
            out = new FileWriter(file);

            source = new StreamSource(in);
            in = null;
            result = new StreamResult(out);
            out = null;

            tn.transform(source, result);
        } finally {
            if (in != null) {
                in.close();
                in = null;
            }
            if (out != null) {
                out.close();
                out = null;
            }
        }        
    }
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        SDKClasspathExportTask target;
        
        target = (SDKClasspathExportTask) super.clone();
        target.initClone();
        if (myClasspath != null) {            
            target.addClasspath((SDKClasspathType) myClasspath.clone());
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myClasspath = null;
    }          
}

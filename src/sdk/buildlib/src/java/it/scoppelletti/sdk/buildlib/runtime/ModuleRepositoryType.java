/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import java.io.*;
import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;
import it.scoppelletti.sdk.buildlib.*;

/**
 * Rappresentazione di un repository di moduli.
 * 
 * @see   it.scoppelletti.runtime.ModuleRepository
 * @see   it.scoppelletti.runtime.ModuleRepositoryFactory
 * @since 1.0.0
 */
public final class ModuleRepositoryType extends CachedDataType<ModuleRepository>
        implements Cloneable {
    private File myBaseDir = null;
    private boolean myVersionRedirecting = false;
    private boolean myAnyAttributeSet = false;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleRepositoryType(Project project) {
        super(project);
    }
 
    /**
     * Imposta il direttorio di base del repository.
     * 
     * @param                        dir Direttorio.
     * @it.scoppelletti.tag.required
     * @see it.scoppelletti.runtime.ModuleRepositoryFactory#setBaseDirectory
     */
    public void setBaseDir(File dir) {
        checkAttributesAllowed();
        myBaseDir = dir;
        disposeData();
    }
    
    /**
     * Imposta l&rsquo;indicatore di redirezione delle versioni abilitata.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code false}
     * @see it.scoppelletti.runtime.ModuleRepositoryFactory#setVersionRedirecting
     */    
    public void setVersionRedirecting(boolean value) {
        checkAttributesAllowed();        
        myVersionRedirecting = value;
        myAnyAttributeSet = true;
        disposeData();        
    }
        
    @Override
    protected boolean isInline() {
        if (myBaseDir != null || myAnyAttributeSet) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (isReference()) {
            return getCheckedRef().toString();
        }
        
        return String.format("ModuleRepository(%1$s)", myBaseDir);
    }   

    @Override
    protected void checkProps() throws BuildException {
        if (myBaseDir == null) {
            throw new BuildException("Missing attribute basedir.",
                    getLocation());
        }                
    }
    
    @Override
    protected ModuleRepository loadData() {
        ModuleRepositoryFactory repoFactory;
        
        repoFactory = new ModuleRepositoryFactory();
        repoFactory.setBaseDirectory(myBaseDir);            
        repoFactory.setVersionRedirecting(myVersionRedirecting);
        repoFactory.setErrorHandler(new XmlErrorHandler(this));
        
        return repoFactory.newRepository();                            
    }

    @Override
    protected void cloneTo(Object target) throws CloneNotSupportedException {        
    }          
}

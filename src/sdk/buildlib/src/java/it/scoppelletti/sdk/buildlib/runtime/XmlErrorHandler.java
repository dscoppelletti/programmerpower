/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import org.apache.tools.ant.*;
import org.xml.sax.*;

/**
 * Gestore degli errori <ACRONYM TITLE="Simple API for XML">SAX</ACRONYM>.
 */
final class XmlErrorHandler implements ErrorHandler {
    private final Project myProject;
    
    /**
     * Costruttore.
     * 
     * @param owner Componente di riferimento.
     */
    XmlErrorHandler(ProjectComponent owner) {
        if (owner == null) {
            throw new NullPointerException("Argument owner is null.");            
        }

        myProject = owner.getProject();
    }
    
    /**
     * Errore fatale.
     *
     * <P>Questa implementazione del metodo {@code fatalError} non esegue nulla
     * perch&eacute; SAX provvede comunque a inoltrare l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     */
    public void fatalError(SAXParseException ex) throws SAXException {
    }
    
    /**
     * Errore non fatale.
     *
     * <P>Questa implementazione del metodo {@code error} inoltra
     * l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     */
    public void error(SAXParseException ex) throws SAXException {
        throw new BuildException(ex.getMessage(), ex, new Location(
                ex.getSystemId(), ex.getLineNumber(), ex.getColumnNumber()));                
    }
    
    /**
     * Avviso.
     *
     * <P>Questa implementazione del metodo {@code warning} emette
     * l&rsquo;eccezione come segnalazione di log.</P>
     *
     * @param ex Eccezione.
     */
    public void warning(SAXParseException ex) throws SAXException {
        BuildException loggingEx;
        
        loggingEx = new BuildException(ex.getMessage(), ex, new Location(
                ex.getSystemId(), ex.getLineNumber(), ex.getColumnNumber()));
        myProject.log(ex.getMessage(), loggingEx, Project.MSG_WARN);
    }      
}

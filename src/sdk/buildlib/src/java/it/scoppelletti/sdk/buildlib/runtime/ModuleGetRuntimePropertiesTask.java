/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import java.util.*;
import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;
import it.scoppelletti.runtime.*;

/**
 * Acquisisce le propriet&agrave; di sistema da impostare per l&rsquo;esecuzione
 * di un modulo.
 * 
 * @see   it.scoppelletti.runtime.Module#getRuntimeProperties
 * @since 1.0.0 
 */
public final class ModuleGetRuntimePropertiesTask extends Task implements
        Cloneable {
    private String myPropSet = null;
    private ModuleType myModule = null;
    
    /**
     * Costruttore.
     */
    public ModuleGetRuntimePropertiesTask() {        
    }    
    
    /**
     * Imposta il riferimento all&rsquo;oggetto {@code PropertySet} sul quale
     * acquisire le propriet&agrave; per l&rsquo;esecuzione del modulo.
     *
     * @param                        value Nome del riferimento.
     * @it.scoppelletti.tag.required
     */
    public void setPropertySet(String value) {
       myPropSet = value;
    }
    
    /**
     * Imposta il modulo.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     */
    public void addModule(ModuleType obj) {        
        if (myModule != null) {
            throw new BuildException("Cannot define more than one module.",
                    getLocation());
        }
        
        myModule = obj;        
    }

    /**
     * Esegue il task.
     */
    @Override
    public void execute() {
        Module module;
        PropertySet propSet;        
        Map<String, String> props;
        
        if (myPropSet == null) {
            throw new BuildException("Missing attribute propertyset.",
                    getLocation());            
        }        
        if (myModule == null) {
            throw new BuildException("Missing element <module>.",
                    getLocation());
        }
        
        try {
            module = myModule.getData();
            props = module.getRuntimeProperties();                       
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }
        
        propSet = new ModuleGetRuntimePropertiesTask.FixedPropertySet(props);
        propSet.setProject(getProject());
        propSet.setDescription(getDescription());
        propSet.setLocation(getLocation());               
        
        getProject().addReference(myPropSet, propSet);
    }
        
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ModuleGetRuntimePropertiesTask target;
        
        target = (ModuleGetRuntimePropertiesTask) super.clone();
        target.initClone();
        if (myModule != null) {
            target.addModule((ModuleType) myModule.clone());
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myModule = null;
    }    
    
    /**
     * Collezione di propriet&agrave; costante.
     */
    private static final class FixedPropertySet extends PropertySet {
        private final Properties myProps;
        
        /**
         * Costruttore.
         * 
         * @param props Collezione.
         */
        FixedPropertySet(Map<String, String> props) {
            myProps = new Properties();            
            myProps.putAll(props);
            setDynamic(false);
        }
        
        @Override
        public Properties getProperties() {
            return myProps;
        }
    }
}

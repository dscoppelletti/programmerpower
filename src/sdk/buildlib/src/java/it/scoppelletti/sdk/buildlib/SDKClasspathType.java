/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import java.util.*;
import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;

/**
 * Rappresentazione di un class-path che associa ad ogni modulo
 * <ACRONYM TITLE="Java ARchive">JAR</ACRONYM> l&rsquo;archivio del codice
 * sorgente e 
 * l&rsquo;<ACRONYIM TITLE="Uniform Resource Locator">URL</ACRONYM> della 
 * documentazione delle
 * <ACRONYM TITLE="Application Programming Interface">API</ACRONYM>.
 * 
 * @since 1.1.0
 */
public final class SDKClasspathType extends DataType implements Cloneable,
        SDKClasspath {
    private boolean myIsInline = false;    
    private List<SDKClasspath> myClasspath = null;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public SDKClasspathType(Project project) {
        setProject(project);
    }  
    
    /**
     * Imposta il riferimento ad un altro componente.
     * 
     * @param id Riferimento.
     */
    @Override
    public void setRefid(Reference id) {
        if (myIsInline) {
            throw tooManyAttributes();
        }
        
        super.setRefid(id);
    }
            
    /**
     * Aggiunge un elemento del class-path.
     * 
     * @param entry Elemento.
     */
    public void addEntry(SDKClasspathEntry entry) {        
        if (entry == null) {
            throw new NullPointerException("Argument entry is null.");
        }
        
        checkChildrenAllowed();
        add(entry);
    }
    
    /**
     * Aggiunge gli elementi di un altro class-path.
     * 
     * @param classpath Class-path.
     */
    public void addClasspath(SDKClasspathType classpath) {        
        if (classpath == null) {
            throw new NullPointerException("Argument classpath is null.");
        }
        
        checkChildrenAllowed();
        add(classpath);
    }
    
    /**
     * Aggiunge una mappatura per associare ai moduli JAR i corrispondenti
     * archivi del codice sorgente, i percorsi radice del codice sorgente
     * all&rsquo;interno di questi e l&rsquo;URL della documentazione delle API.
     * 
     * @param mapper Mappatura.
     */
    public void addMapper(SDKClasspathMapper mapper) {        
        if (mapper == null) {
            throw new NullPointerException("Argument mapper is null.");
        }
        
        checkChildrenAllowed();
        add(mapper);
    }
    
    /**
     * Aggiunge un class-path.
     * 
     * @param classpath Class-path.
     */    
    private void add(SDKClasspath classpath) {
        if (myClasspath == null) {
            myClasspath = new ArrayList<SDKClasspath>();
            myIsInline = true;
        }
        
        myClasspath.add(classpath);        
    }
    
    public List<SDKClasspathEntry> listEntries() {
        SDKClasspathType target;
        List<SDKClasspathEntry> list;
        
        if (isReference()) {
            target = (SDKClasspathType) getCheckedRef();
            return target.listEntries();            
        }
        
        list = new ArrayList<SDKClasspathEntry>();
        if (myClasspath == null) {
            return list;
        }
        
        for (SDKClasspath classpath : myClasspath) {
            list.addAll(classpath.listEntries());
        }
        
        return list;
    }
        
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (isReference()) {
            return getCheckedRef().toString();
        }
        
        return super.toString();
    }    
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        SDKClasspathType target;
        
        try {
            target = (SDKClasspathType) super.clone();
            target.initClone();
            if (myClasspath != null) {
                for (SDKClasspath classpath : myClasspath) {
                    add(classpath);
                }
            }            
        } catch (CloneNotSupportedException ex) {
            throw new BuildException(ex.getMessage(), ex);
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myClasspath = null;
    }       
}

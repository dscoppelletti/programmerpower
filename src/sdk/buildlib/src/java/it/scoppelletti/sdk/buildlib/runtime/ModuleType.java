/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;
import it.scoppelletti.sdk.buildlib.*;

/**
 * Rappresentazione di un modulo.
 * 
 * @see   it.scoppelletti.runtime.Module
 * @since 1.0.0
 */
public final class ModuleType extends CachedDataType<Module> implements
        Cloneable {
    private ModuleLocation myLocation = null;
    private ModuleRepositoryType myRepo = null;    
 
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleType(Project project) {
        super(project);
    }
    
    /**
     * Imposta i parametri per la lettura del modulo.
     * 
     * @param                        obj Elemento.
     * @it.scoppelletti.tag.required     Se non &egrave; presente
     *                                   l&rsquo;elemento
     *                                   {@code <customlocation>}.
     */
    public void addRepositoryLocation(ModuleRepositoryLocation obj) {
        addLocation(obj);
    }
    
    /**
     * Imposta i parametri per la lettura del modulo.
     * 
     * @param                        obj Elemento.
     * @it.scoppelletti.tag.required     Se non &egrave; presente
     *                                   l&rsquo;elemento
     *                                   {@code <repositorylocation>}. 
     */
    public void addCustomLocation(ModuleCustomLocation obj) {
        addLocation(obj);
    }
    
    /**
     * Imposta i parametri per la lettura del modulo.
     * 
     * @param obj Elemento.
     */    
    private void addLocation(ModuleLocation obj) {
       checkChildrenAllowed();
       
       if (myLocation != null) {
           throw new BuildException("Cannot define more than one location.",
                   getLocation());           
       }
       
       myLocation = obj;
       disposeData();
    }
    
    /**
     * Imposta il repository dei moduli.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     * @see it.scoppelletti.runtime.ModuleRepository
     */
    public void addRepository(ModuleRepositoryType obj) {
        checkChildrenAllowed();
        
        if (myRepo != null) {
            throw new BuildException("Cannot define more than one repository.",
                    getLocation());
        }
        
        myRepo = obj;
        disposeData();
    }
    
    /**
     * Restituisce il repository dei moduli.
     * 
     * @return Oggetto.
     */
    protected ModuleRepositoryType getRepository() {
        ModuleType target;
        
        if (isReference()) {                        
            target = (ModuleType) getCheckedRef();
            return target.getRepository();
        }        
        
        return myRepo;
    }
    
    @Override
    protected boolean isInline() {
        if (myLocation != null || myRepo != null) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    @Override
    public String toString() {
        if (isReference()) {
            return getCheckedRef().toString();
        }
        
        return String.format("Module(%1$s)", myLocation); 
    }
    
    @Override
    protected void checkProps() throws BuildException {
        if (myLocation == null) {
            throw new BuildException(
                    "Missing elements <repositorylocation> or <customlocation>.",
                    getLocation());            
        }
        myLocation.checkProps();
        
        if (myRepo == null) {
            throw new BuildException("Missing element <repository>.",
                    getLocation());
        }               
    }
    
    @Override
    protected Module loadData() {
        return myLocation.loadData(myRepo.getData());                                     
    }
    
    @Override
    protected void initClone() {
        super.initClone();
        myLocation = null;
        myRepo = null;
    }
    
    @Override
    protected void cloneTo(Object target) throws CloneNotSupportedException {
        ModuleType obj = (ModuleType) target;
    
        obj.addLocation(myLocation);
        if (myRepo != null) {
            obj.addRepository((ModuleRepositoryType) myRepo.clone());
        }        
    }        
}

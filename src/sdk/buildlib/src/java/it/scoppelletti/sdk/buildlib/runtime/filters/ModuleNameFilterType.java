/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime.filters;

import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Filtro sul nome di un modulo.
 *
 * @see   it.scoppelletti.runtime.Module#getIdentity
 * @since 1.1.0
 */
public final class ModuleNameFilterType extends ModuleFilterType {
    private SymbolicName myName = null;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleNameFilterType(Project project) {
        super(project);
    }
    
    /**
     * Imposta il nome del modulo.
     * 
     * @param                        value Valore.
     * @it.scoppelletti.tag.required
     */
    public void setName(String value) {
        checkAttributesAllowed();
        
        try {
            myName = new SymbolicName(value);
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }        
    }
    
    @Override
    protected boolean isInline() {
        if (myName != null) {
            return true;
        }
        
        return false;
    }
    
    @Override
    protected void doCheckProps() {
        if (myName == null) {
            throw new BuildException("Missing attributes name.", getLocation());            
        }
    }
    
    @Override
    protected boolean doAccept(Module module) {
        return module.getIdentity().getName().equals(myName);
    }
}

/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;

/**
 * Classe base per i componenti che rappresentano un tipo di dato.
 * 
 * <P>La stessa istanza di un componente pu&ograve; essere riferita in task
 * differenti (attraverso gli attributi {@code id} e {@code refid}), ma la
 * rilettura del dato sottostante all&rsquo;esecuzione di tutti i task
 * pu&ograve; degradare le prestazioni del sistema (es. se devono essere lette
 * strutture dati di una certa dimensione sul disco).<BR> 
 * Il metodo {@link #getData} della classe {@code CachedDataType} legge il dato
 * sottostante eseguendo una-tantum il metodo {@code #loadData}.</P> 
 * 
 * @param <T> Tipo del dato sottostante.
 * @since     1.0.0
 */
public abstract class CachedDataType<T> extends DataType implements Cloneable {
    private T myData = null;
    
    /**
     * Risorsa privata per la sincronizzazione.
     * 
     * <P>Il campo {@code mySyncRoot} non &egrave; {@code final} perch&eacute;
     * devo poterlo essere reimpostare nel metodo {@code initClone}.</P>
     */
    private Object mySyncRoot;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    protected CachedDataType(Project project) {
        setProject(project);
        mySyncRoot = new Object();
    }    
    
    /**
     * Imposta il riferimento ad un altro componente.
     * 
     * @param id Riferimento.
     * @see      #isInline
     */
    @Override
    public final void setRefid(Reference id) {
        if (isInline()) {
            throw tooManyAttributes();
        }
        
        super.setRefid(id);
        disposeData();
    }
    
    /**
     * Verifica se sono stati impostati attributi (diversi dal riferimento ad
     * un altro componente) o aggiunti sotto-elementi.
     * 
     * <P>Il metodo {@code setRefid} utilizza il metodo {@code isInline} per
     * verificare se sul componente sono gi&agrave; stati impostati altri
     * attributi o sotto-elementi e, in questo caso, impedire di impostare il
     * riferimento ad un altro componente.</P>
     *  
     * @return Esito della verifica.
     * @see    #setRefid
     */
    protected abstract boolean isInline();
    
    /**
     * Verifica la validit&agrave; della combinazione di attributi e
     * sotto-elementi impostata sul componente.
     * 
     * @throws org.apache.tools.ant.BuildException Combinazione non valida.
     */
    protected abstract void checkProps() throws BuildException;
    
    /**
     * Restituisce il dato sottostante.
     * 
     * <P>Se il dato sottostante non &egrave; ancora stato letto, viene letto
     * eseguendo il metodo {@link #loadData}.<BR>
     * Il metodo {@code getData} della stessa istanza del componente potrebbe
     * essere eseguito in concorrenza da thread differenti, ad esempio
     * combinando la possibilit&agrave; di riferire lo stesso componente in task
     * differenti (attraverso gli attributi {@code id} e {@code refid}) e
     * l&rsquo;utilizzo del task {@code Parallel}; per questo motivo, il metodo
     * {@code loadData} viene eseguito sincronizzando i thread concorrenti. Si
     * specifica che gli altri metodi della classe {@code CachedDataType} non
     * sono invece sincronizzati, n&eacute; possono esserlo i metodi
     * implementati dalle classi derivate (es. i metodi accessori di
     * lettura/scrittura) perch&eacute; la classe {@code CachedDataType}
     * utilizza una <I>risorsa privata per la sincronizzazione</I>; la
     * sincronizzazione degli altri metodi non &egrave; generalmente necessaria
     * se il componente viene utilizzato in uno script Ant, ma potrebbe esserlo
     * in contesti di programmazione.</P>   
     *  
     * @return Oggetto.
     * @it.scoppelletti.tag.threadsafe
     * @see    #setRefid
     */
    @SuppressWarnings("unchecked")
    public final T getData() {
        CachedDataType<T> target;
        
        if (isReference()) {                        
            target = (CachedDataType<T>) getCheckedRef();
            return target.getData();
        }
        
        checkProps();
        
        synchronized (mySyncRoot) {
            if (myData == null) {
                myData = loadData();
            }
            
            return myData;
        }                
    }

    /**
     * Legge il dato sottostante.
     * 
     * @return Oggetto.
     * @see    #getData
     */
    protected abstract T loadData();
           
    /**
     * Rilascia l&rsquo;oggetto sottostante.      
     * 
     * <P>Le classi derivate devono eseguire il metodo {@code disposeData} in
     * ogni metodo che invalida il dato sottostante eventualmente gi&agrave;
     * letto (es. i metodi accessori di scrittura) per fare in modo che la
     * successiva esecuzione del metodo {@code getData} legga il nuovo dato
     * sottostante.</P>
     * 
     * @see #getData
     */
    protected final void disposeData() {
        myData = null;
    }    
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    @SuppressWarnings("unchecked")
    public final Object clone() {
        CachedDataType<T> target;
        
        try {
            target = (CachedDataType<T>) super.clone();
            target.initClone();
            cloneTo(target);
        } catch (CloneNotSupportedException ex) {
            throw new BuildException(ex.getMessage(), ex);
        }
        
        return target;
    }
    
    /**
     * Inizializzazione di un clone.
     */
    protected void initClone() {
        // Ne' il dato sottostante ne' la risorsa per la sincronizzazione
        // privata devono essere condivisi tra l'instanza originale e quella
        // clonata
        mySyncRoot = new Object();
        disposeData();
    }

    /**
     * Clona i campi dell&rsquo;istanza in un clone.
     * 
     * @param target Clone.
     */
    protected abstract void cloneTo(Object target) throws
        CloneNotSupportedException;
}

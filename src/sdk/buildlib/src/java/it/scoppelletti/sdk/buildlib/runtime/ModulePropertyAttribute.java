/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import org.apache.tools.ant.types.*;

/**
 * Propriet&agrave; di un modulo.
 * 
 * @see   it.scoppelletti.sdk.buildlib.runtime.ModuleGetPropertyTask
 * @see   it.scoppelletti.runtime.Module
 * @since 1.0.0
 */
public final class ModulePropertyAttribute extends EnumeratedAttribute {

    /**
     * Nome del modulo. Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.runtime.Module#getIdentity
     */
    public static final String NAME = "name";
        
    /**
     * Versione del modulo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.runtime.Module#getIdentity
     */
    public static final String VERSION = "version";
    
    /**
     * Target del modulo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see   it.scoppelletti.runtime.Module#getTarget
     * @since 1.1.0
     */
    public static final String TARGET = "target";
    
    /**
     * Direttorio di base del modulo. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.runtime.Module#getBaseDirectory
     */
    public static final String BASEDIR = "basedir";
    
    /**
     * Nome del modulo JAR. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.runtime.Module#getArchiveName
     */
    public static final String JARNAME = "jarname";
    
    /**
     * Nome dell&rsquo;archivio del codice sorgente. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     *
     * @see it.scoppelletti.runtime.Module#getSourceArchiveName
     */
    public static final String SOURCEZIPNAME = "sourcezipname";
    
    /**
     * Percorso radice del codice sorgente all&rsquo;interno dell&rsquo;archivio
     * {@code SOURCEZIPNAME}. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     *
     * @see it.scoppelletti.runtime.Module#getSourceRootPath
     */
    public static final String SOURCEROOTPATH = "sourcerootpath";
    
    /**
     * Nome della classe che implementa l&rsquo;entry-point {@code main}
     * dell&rsquo;applicazione. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     *
     * @see it.scoppelletti.runtime.Module#getMainClassName
     */
    public static final String MAINCLASS = "mainclass";
    
    /**
     * Costruttore.
     */
    public ModulePropertyAttribute() {            
    }
    
    /**
     * Restituisce i possibili valori dell&rsquo;enumerato.
     * 
     * @return Vettore.
     */
    @Override
    public String[] getValues() {
        return new String[] { ModulePropertyAttribute.NAME,
                ModulePropertyAttribute.VERSION,
                ModulePropertyAttribute.TARGET,
                ModulePropertyAttribute.BASEDIR, 
                ModulePropertyAttribute.JARNAME,
                ModulePropertyAttribute.SOURCEZIPNAME,
                ModulePropertyAttribute.SOURCEROOTPATH,
                ModulePropertyAttribute.MAINCLASS};
    }    
}

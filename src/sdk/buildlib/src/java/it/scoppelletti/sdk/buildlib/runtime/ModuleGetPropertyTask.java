/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime;

import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Acquisisce il valore di una propriet&agrave; di un modulo.
 * 
 * @see   it.scoppelletti.runtime.Module
 * @since 1.0.0
 */
public final class ModuleGetPropertyTask extends Task implements Cloneable {       
    private ModuleType myModule = null;
    private String myProperty = null;
    private String myOutputProperty = null;
    private boolean mySetOnEmpty = true;
    
    /**
     * Costruttore.
     */
    public ModuleGetPropertyTask() {        
    }
    
    /**
     * Imposta il nome della propriet&agrave; del modulo da interrogare.
     * 
     * @param                        value Nome della propriet&agrave;.
     * @it.scoppelletti.tag.required
     */
    public void setProperty(ModulePropertyAttribute value) {
       if (value != null) {
           myProperty = value.getValue();
       } else {
           myProperty = null;
       }              
    }
    
    /**
     * Imposta il nome della propriet&agrave; da impostare con il valore della
     * propriet&agrave; del modulo da interrogare.
     * 
     * @param                        value Nome della propriet&agrave;.
     * @it.scoppelletti.tag.required
     */
    public void setOutputProperty(String value) {
       myOutputProperty = value;
    }
    
    /**
     * Imposta l&rsquo;indicatore di propriet&agrave; {@code outputproperty} da
     * impostare anche se la propriet&agrave; del modulo da interrogare non
     * &egrave; valorizzata.
     *  
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@code true}
     * @see                               #setProperty
     * @see                               #setOutputProperty
     */
    public void setSetOnEmpty(boolean value) {
        mySetOnEmpty = false;
    }
    
    /**
     * Imposta il modulo.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     */
    public void addModule(ModuleType obj) {
        if (myModule != null) {
            throw new BuildException("Cannot define more than one module.",
                    getLocation());
        }
        
        myModule = obj;        
    }

    /**
     * Esegue il task.
     */
    @Override
    public void execute() {
        String msg, value;
        Module module;
        
        if (myProperty == null) {
            throw new BuildException("Missing attribute property.",
                    getLocation());            
        }
        if (myOutputProperty == null || myOutputProperty.isEmpty()) {
            throw new BuildException("Missing attribute outputproperty.",
                    getLocation());            
        }        
        if (myModule == null) {
            throw new BuildException("Missing element <module>.",
                    getLocation());
        }
        
        try {
            module = myModule.getData();
            
            value = null;
            if (myProperty.equals(ModulePropertyAttribute.NAME)) {
                value = module.getIdentity().getName().toString();
            } else if (myProperty.equals(ModulePropertyAttribute.VERSION)) {
                value = module.getIdentity().getVersion().toString();
            } else if (myProperty.equals(ModulePropertyAttribute.TARGET)) {
                value = module.getTarget().toString().toLowerCase();                
            } else if (myProperty.equals(ModulePropertyAttribute.BASEDIR)) {
                value = module.getBaseDirectory().getPath();                    
            } else if (myProperty.equals(ModulePropertyAttribute.JARNAME)) {
                value = module.getArchiveName();                
            } else if (myProperty.equals(
                    ModulePropertyAttribute.SOURCEZIPNAME)) {
                value = module.getSourceArchiveName();
            } else if (myProperty.equals(
                    ModulePropertyAttribute.SOURCEROOTPATH)) {
                value = module.getSourceRootPath();
            } else if (myProperty.equals(ModulePropertyAttribute.MAINCLASS)) {
                value = module.getMainClassName();                
            } else {
                msg = String.format("Unsupported property \"%1$s\".",
                        myProperty);
                throw new UnsupportedOperationException(msg);
            }
        
            if (value == null && mySetOnEmpty) {
                value = "";
            }
            if (value != null) {
                getProject().setProperty(myOutputProperty, value);
            }
        } catch (Exception ex) {
            throw new BuildException(ex.getMessage(), ex, getLocation());
        }
    }
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ModuleGetPropertyTask target;
        
        target = (ModuleGetPropertyTask) super.clone();
        target.initClone();
        if (myModule != null) {
            target.addModule((ModuleType) myModule.clone());
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myModule = null;
    }        
}

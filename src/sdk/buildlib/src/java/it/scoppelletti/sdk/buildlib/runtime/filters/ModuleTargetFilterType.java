/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime.filters;

import org.apache.tools.ant.*;
import it.scoppelletti.sdk.buildlib.runtime.*;
import it.scoppelletti.runtime.*;

/**
 * Filtro sul target di un modulo
 *
 * @see   it.scoppelletti.runtime.Module#getTarget
 * @since 1.1.0
 */
public final class ModuleTargetFilterType extends ModuleFilterType {
    private String myTarget = null;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleTargetFilterType(Project project) {
        super(project);
    }
    
    /**
     * Imposta il nome target del modulo.
     * 
     * @param                        value Target.
     * @it.scoppelletti.tag.required
     */
    public void setTarget(ModuleTargetAttribute value) {
       if (value != null) {
           myTarget = value.getValue();
       } else {
           myTarget = null;
       }              
    }
        
    @Override
    protected boolean isInline() {
        if (myTarget != null) {
            return true;
        }
        
        return false;
    }
    
    @Override
    protected void doCheckProps() {
        if (myTarget == null) {
            throw new BuildException("Missing attributes target.",
                    getLocation());            
        }
    }
    
    @Override
    protected boolean doAccept(Module module) {
        return myTarget.equalsIgnoreCase(module.getTarget().name());
    }
}

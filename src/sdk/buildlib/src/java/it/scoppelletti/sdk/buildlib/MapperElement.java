/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib;

import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;
import org.apache.tools.ant.util.*;

/**
 * Elemento <I>wrapper</I> utilizzabile per inserire in un componente 
 * pi&ugrave; sotto-elementi {@code Mapper} ma corrispondenti a componenti
 * distinti.
 *  
 * @since 1.1.0
 */
public abstract class MapperElement extends ProjectComponent {
    private Mapper myMapper = null;
    
    /**
     * Costruttore.
     * 
     * @param project  Progetto.
     * @param location Locazione.
     */
    MapperElement(Project project, Location location) {
        setProject(project);
        setLocation(location);
    }
    
    /**
     * Inizializza una mappatura.
     * 
     * @return Oggetto.
     */
    public final Mapper createMapper() {               
        if (myMapper != null) {
            throw new BuildException("Cannot define more than one mapper.",
                    getLocation());            
        }
        
        myMapper = new Mapper(getProject());
        
        return myMapper;
    }

    /**
     * Aggiunge una mappatura.
     * 
     * @param map Oggetto.
     */
    public final void add(FileNameMapper map) {                
        if (map == null) {
            throw new NullPointerException("Argument map is null.");
        }
        
        createMapper().add(map);        
    }    
    
    /**
     * Restituisce l&rsquo;implementazione della mappatura.
     * 
     * @return Oggetto.
     */
    public final FileNameMapper getMapper() {
        if (myMapper == null) {
            return null;
        }
        
        return myMapper.getImplementation();
    }
}

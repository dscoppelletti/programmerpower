/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.buildlib.runtime.filters;

import java.util.*;
import org.apache.tools.ant.*;
import it.scoppelletti.runtime.*;

/**
 * Combinazione di filtri su un modulo che devono essere tutti verificati
 * ({@code and}).
 * 
 * @since 1.1.0
 */
public final class ModuleAndFilterType extends ModuleFilterType {
    private List<ModuleFilterType> myFilters = null;
    
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */
    public ModuleAndFilterType(Project project) {
        super(project);
    }
        
    @Override
    protected boolean isInline() {
        if (myFilters != null) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Aggiunge un filtro.
     * 
     * @param                        obj Oggetto.
     * @it.scoppelletti.tag.required
     */
    public void add(ModuleFilterType obj) {        
        if (obj == null) {
            throw new NullPointerException("Argument obj is null.");
        }        
        
        checkAttributesAllowed();
        if (myFilters == null) {
            myFilters = new ArrayList<ModuleFilterType>();            
        }
        myFilters.add(obj);
    }
    
    @Override
    protected void doCheckProps() {
        if (myFilters == null) {
            throw new BuildException("Missing filter elements.", getLocation());            
        }
        
        for (ModuleFilterType filter : myFilters) {
            filter.checkProps();
        }        
    }
    
    @Override
    protected boolean doAccept(Module module) {
        for (ModuleFilterType filter : myFilters) {
            if (!filter.accept(module)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Clona l&rsquo;oggetto.
     * 
     * @return Oggetto.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ModuleAndFilterType target;
        
        target = (ModuleAndFilterType) super.clone();
        target.initClone();
        if (myFilters != null) {
            for (ModuleFilterType filter : myFilters) {
                target.add((ModuleFilterType) filter.clone());
            }
        }
        
        return target;
    }    
    
    /**
     * Inizializzazione di un clone.
     */
    private void initClone() {
        myFilters = null;
    }    
}

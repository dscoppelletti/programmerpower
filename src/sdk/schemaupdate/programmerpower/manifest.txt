Specification-Title: Database Schema Updater
Specification-Version: ${module.version}
Specification-Vendor: Dario Scoppelletti
Implementation-Title: ${module.name}
Implementation-Version: ${module.version}
Implementation-Vendor: Dario Scoppelletti
Sealed: true
Main-Class: it.scoppelletti.sdk.schemaupdate.Program

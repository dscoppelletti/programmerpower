/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate.security;

import it.scoppelletti.programmerpower.resources.*;

/**
 * Risorse dell&rsquo;applicazione.
 */
final class SecurityResources extends ResourceWrapper {

    /**
     * Costruttore.
     */
    SecurityResources() {        
    }
    
    /**
     * Messaggio per utente creato.
     * 
     * @param  name Nome.
     * @param  pwd  Passoword.
     * @return      Testo.
     */
    String getUserSavedMessage(String name, String pwd) {
        return format("UserSavedMessage", name, pwd);
    }
        
    /**
     * Eccezione per codice di localizzazione non impostato.
     * 
     * @param  role Ruolo.
     * @return      Testo.
     */
    String getEmptyLocaleException(String role) {
        return format("EmptyLocaleException", role);
    }
    
    /**
     * Eccezione per descrizione non impostata.
     * 
     * @param role   Ruolo.
     * @param locale Localizzazione.
     * @return       Testo.
     */
    String getEmptyDescriptionException(String role, String locale) {
        return format("EmptyDescriptionException", role, locale);
    }    

    /**
     * Messaggio per ruolo creato.
     * 
     * @param  code Codice.
     * @return      Testo.
     */
    String getRoleSavedMessage(String code) {
        return format("RoleSavedMessage", code);
    }
    
    /**
     * Messaggio per ruolo cancellato.
     * 
     * @param  code Codice.
     * @return      Testo.
     */
    String getRoleDeletedMessage(String code) {
        return format("RoleDeletedMessage", code);
    }
    
    /**
     * Messaggio per ruoli pre-esistenti non importati.
     * 
     * @return Testo.
     */
    String getNotImportingRolesMessage() {
        return getString("NotImportingRolesMessage");
    }
    
    /**
     * Eccezione per nessun ruolo definito.
     * 
     * @return Testo.
     */
    String getNoRoleDefinedException() {
        return getString("NoRoleDefinedException");
    }

    /**
     * Eccezione per ruolo gi&agrave; esistente.
     * 
     * @param  role Ruolo.
     * @return      Testo.
     */
    String getRoleAlreadyExistException(String role) {
        return format("RoleAlreadyExistException", role);
    }  
    
    /**
     * Eccezione per ruolo non esistente.
     * 
     * @param  role Codice
     * @return      Testo.
     */
    String getRoleNotFoundException(String role) {
        return format("RoleNotFoundException", role);
    }
    
    /**
     * Messaggio di assegnazione di un ruolo.
     * 
     * @param  user Utente.
     * @param  role Ruolo.
     * @return      Testo.
     */
    String getGrantedRoleMessage(String user, String role) {
        return format("GrantedRoleMessage", user, role);
    }    
    
    /**
     * Messaggio di revoca di un ruolo.
     * 
     * @param  user Utente.
     * @param  role Ruolo.
     * @return      Testo.
     */    
    String getRevokedRoleMessage(String user, String role) {
        return format("RevokedRoleMessage", user, role);
    }
    
    /**
     * Eccezione per nome di file non supportato.
     * 
     * @param  name Nome del file.
     * @return      Testo.
     */
    String getFileNameNotSupportedException(String name) {
        return format("FileNameNotSupportedException", name);
    }
    
    /**
     * Eccezione per localizzazione non supportata.
     * 
     * @param  locale Localizzazione.
     * @return        Testo.
     */
    String getLocaleNotSupportedException(String locale) {
        return format("LocaleNotSupportedException", locale);
    }       
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate.security;

import java.util.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Crea un ruolo.
 * 
 * @see   it.scoppelletti.programmerpower.security.Role
 * @since 1.0.0
 */
@Final
public class RoleCreator implements Runnable {   
    private String myCode;
    private String myDesc;
    private Map<String, String> myDescMap;
    
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private RoleManager myRoleMgr;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
        
    /**
     * Crea un ruolo.
     */
    public RoleCreator() {       
    }
    
    /**
     * Restituisce il codice del ruolo.
     * 
     * @return Valore.
     */
    String getCode() {
        return myCode;
    }
    
    /**
     * Imposta il codice del ruolo.
     * 
     * @param value Valore.
     */
    @Required
    public void setCode(String value) {
        if (StringUtils.isBlank(value)) {
            throw new ArgumentNullException("value");
        }
        
        myCode = value;
    }
        
    /**
     * Imposta la descrizione del ruolo.
     * 
     * @param                       value Valore.
     * @it.scoppelletti.tag.default       {@link #setCode}
     */
    public void setDescription(String value) {        
        myDesc = value;
    }
    
    /**
     * Imposta le descrizioni localizzate.
     * 
     * @param obj Collezione.
     */
    public void setLocalizedDescriptions(Map<String, String> obj) {
        if (obj == null) {
            throw new ArgumentNullException("obj");
        }
        
        myDescMap = new HashMap<String, String>(obj);
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {        
        Role role;
        SecurityResources res = new SecurityResources();
        
        if (StringUtils.isBlank(myCode)) {
            throw new PropertyNotSetException(toString(), "code");
        }
    
        role = myRoleMgr.loadRole(myCode);
        if (role != null) {
            myUI.display(MessageType.WARNING, res.getRoleAlreadyExistException(
                    myCode));
            return;            
        }
        
        role = initRole();
        myRoleMgr.saveRole(role);
        myUI.display(MessageType.INFORMATION,
                res.getRoleSavedMessage(role.getCode()));        
    } 
    
    /**
     * Inizializza un ruolo.
     * 
     * @return Oggetto.
     */
    Role initRole() {
        String locale, desc;
        Role role;
        SecurityResources res = new SecurityResources();

        role = myRoleMgr.newRole(myCode);
        role.setDescription(StringUtils.isBlank(myDesc) ? myCode : myDesc);
        role.setSystem(true);
        
        if (myDescMap != null) {
            for (Map.Entry<String, String> descEntry : myDescMap.entrySet()) {
                locale = descEntry.getKey();
                if (StringUtils.isBlank(locale)) {
                    myUI.display(MessageType.ERROR,
                            res.getEmptyLocaleException(myCode));
                    continue;
                }            
                
                desc = descEntry.getValue();
                if (StringUtils.isBlank(desc)) {
                    myUI.display(MessageType.ERROR,
                            res.getEmptyDescriptionException(myCode, locale));                
                    continue;                
                }                           
                
                role.getLocalizedDescriptions().put(locale, desc);
            }            
        }        
        
        return role;
    }
}

/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate;

import liquibase.exception.*;
import liquibase.database.*;
import liquibase.database.jvm.*;
import liquibase.statement.*;
import liquibase.change.*;
import org.apache.commons.lang3.*;
import org.springframework.context.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Modifica LiquiBase implementata da un bean Spring attraverso
 * l&rsquo;interfaccia {@code JdbcWorker}.
 * 
 * @since 2.1.0
 */
@Final
public class JdbcChangeBeanWrapper extends AbstractChange {

    /**
     * Tag della modifica. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String TAG_NAME = "jdbcChangeBean";
    
    private String myBeanName;
    
    @ChangeProperty(includeInSerialization = false)
    private ApplicationContext myApplCtx;
    
    @ChangeProperty(includeInSerialization = false)
    private JdbcWorker myBean;
    
    /**
     * Costruttore.
     */
    public JdbcChangeBeanWrapper() {
        super(JdbcChangeBeanWrapper.TAG_NAME, "JDBC Change Bean",
                ChangeMetaData.PRIORITY_DEFAULT);        
    }

    /**
     * Inizializzazione.
     */
    @Override
    public void init() throws SetupException {
        super.init();
        
        try {
            myApplCtx = (ApplicationContext) getChangeLogParameters().getValue(
                 JPALiquibase.PARAM_APPLICATIONCONTEXT);
        } catch (Exception ex) {
            throw new SetupException(ex.getMessage(), ex);
        }
        if (myApplCtx == null) {
            throw new SetupException(String.format("Parameter %1$s not set.",
                    JPALiquibase.PARAM_APPLICATIONCONTEXT));
        }        
    }
    
    /**
     * Imposta il nome del bean {@code JdbcWorker} che implementa la modifica.
     * 
     * @param value Valore.
     * @see         #generateStatements
     */
    public void setBeanName(String value) {
        myBeanName = value;
    }
    
    /**
     * Restituisce il messaggio di conferma esposto dopo l&rsquo;esecuzione 
     * della modifica.
     * 
     * @return Testo.
     */
    public String getConfirmationMessage() {
        return String.format("Bean %1$s executed.", myBeanName);
    }

    /**
     * Verifica se la modifica supporta l&rsquo;annullamento.
     * 
     * @param  database Database.
     * @return          {@code false}
     */
    @Override
    public boolean supportsRollback(Database database) {
        return false;
    }

    /**
     * Genera le istruzioni SQL per l&rsquo;esecuzione della modifica.
     * 
     * <P>Questa versione prevalente del metodo {@code generateStatements}
     * esegue il metodo {@code run} dell&rsquo;interfaccia {@code JdbcWorker}
     * del del bean che implementa la modifica.</P>
     * 
     * @param  database Database.
     * @return          Vettore vuoto.
     * @see             #setBeanName
     */    
    public SqlStatement[] generateStatements(Database database) {        
        try {
            myBean.run((JdbcConnection) database.getConnection());
        } catch (Exception ex) {
            throw new UnexpectedLiquibaseException(ex);
        }
        
        return new SqlStatement[0];
    }
    
    /**
     * Genera le istruzioni SQL per l&rsquo;annullamento della modifica.
     * 
     * @param  database Database.
     * @return          Vettore vuoto.
     */
    @Override
    public SqlStatement[] generateRollbackStatements(Database database) throws
            UnsupportedChangeException, RollbackImpossibleException {
        return new SqlStatement[0];
    }
    
    /**
     * Validazione preventiva all&rsquo;applicazione della modifica.
     * 
     * @param  database Database.
     * @return          Collezione degli eventuali errori rilevati.
     */
    @Override
    public ValidationErrors validate(Database database) {
        ValidationErrors errs = new ValidationErrors();
        
        if (StringUtils.isBlank(myBeanName)) {
            errs.checkRequiredField("beanName", null);
            return errs;
        }
                
        try {
            myBean = myApplCtx.getBean(myBeanName, JdbcWorker.class);
        } catch (Exception ex) {
            errs.addError(ex.getMessage());
        }
        
        return errs;
    }
    
    /**
     * Restituisce gli avvisi emessi dall&rsquo;applicazione della modifica.
     * 
     * @param  database Database.
     * @return          Collezione vuota.
     */
    @Override
    public Warnings warn(Database database) {
        return new Warnings();
    }
}

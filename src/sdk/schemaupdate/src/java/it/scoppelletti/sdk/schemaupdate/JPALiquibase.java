/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate;

import java.sql.*;
import java.util.*;
import javax.persistence.*;
import liquibase.*;
import liquibase.change.*;
import liquibase.database.*;
import liquibase.database.jvm.*;
import liquibase.exception.*;
import liquibase.resource.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.*;
import org.springframework.core.io.*;
import it.scoppelletti.programmerpower.data.*;
import it.scoppelletti.programmerpower.reflect.*;

/**
 * Integrazione LiquiBase con
 * <ACRONYM TITLE="Java Persistence API">JPA</ACRONYM>.
 * 
 * @since 1.0.0
 */
@Final
public class JPALiquibase implements Runnable, InitializingBean {

    /**
     * Parametro riservato
     * {@code it.scoppelletti.sdk.schemaupdate.applicationContext}: contesto
     * dell&rsquo;applicazione Spring.
     */
    public static final String PARAM_APPLICATIONCONTEXT =
            "it.scoppelletti.sdk.schemaupdate.applicationContext";
    
    @PersistenceUnit(unitName = DataTools.PERSISTENCE_UNIT)        
    private EntityManagerFactory myEntityMgrFactory;
    
    @Autowired
    private ApplicationContext myApplCtx;
    
    @Autowired
    private ResourceLoader myResLoader;
    
    private String myChangeLog;
    private String myDefaultSchema;
    private String myContexts;
    private boolean myDropFirst;
    private Map<String, Object> myParams;    
        
    /**
     * Costruttore.
     */
    public JPALiquibase() {        
    }

    /**
     * Inizializzazione.
     */
    @Reserved
    public void afterPropertiesSet() throws Exception {
        ChangeFactory changeFactory = ChangeFactory.getInstance();
        
        changeFactory.register(CustomChangeBeanWrapper.class);        
        changeFactory.register(JdbcChangeBeanWrapper.class);
    }
    
    /**
     * Imposta il riferimento alla risorsa Spring che corrisponde ai file delle
     * modifiche da applicare al database.
     * 
     * @param value Valore. 
     */
    public void setChangeLog(String value) {
        myChangeLog = value;
    }
    
    /**
     * Imposta lo schema di default per gli oggetti gestiti del database e le
     * tabelle di controllo di LiquiBase.
     * 
     * @param value Valore.
     */
    public void setDefaultSchema(String value) {
        myDefaultSchema = value;
    }
    
    /**
     * Imposta l&rsquo;elenco dei contesti per i quali applicare le modifiche
     * al database.
     * 
     * @param list Elenco.
     */
    public void setContexts(String list) {
        myContexts = list;
    }
    
    /**
     * Imposta l&rsquo;indicatore di cancellazione degli oggetti del database
     * prima di applicare le modifiche alla sorgente dati.
     * 
     * @param value Valore.
     */
    public void setDropFirst(boolean value) {
        myDropFirst = value;
    }
    
    /**
     * Imposta i parametri di LiquiBase.
     * 
     * @param map Collezione.
     */
    public void setParameters(Map<String, Object> map) {
        myParams = map;
    }

    /**
     * Esegue l&rsquo;aggiornamento del database.
     */
    public void run() {
        EntityManager entityMgr = null;
        org.hibernate.Session session;
        
        // Devo aprire e chiudere esplicitamente la connessione al database
        // perche', se applicassi l'annotazione @Transactional, tutti i passi
        // dell'aggiornamento sarebbero eseguiti nella stessa transazione,
        // anziche' ognuno nella propria transazione.        
        try {
            entityMgr = myEntityMgrFactory.createEntityManager();
            session = (org.hibernate.Session) entityMgr.getDelegate(); 
            session.doWork(new JPALiquibase.LiquibaseWork());
        } finally {
            if (entityMgr != null) {
                entityMgr.close();
                entityMgr = null;
            }
        }        
    }
        
    /**
     * Crea un&rsquo;istanza di LiquiBase.
     * 
     * @param  cn Connessione.
     * @return    Oggetto.
     */
    private Liquibase createLiquibase(Connection cn) throws LiquibaseException {
        Liquibase liquibase;
        ResourceAccessor fileOpener;
        Database db;
        DatabaseFactory factory = DatabaseFactory.getInstance();

        fileOpener = new SpringResourceAccessor(myChangeLog, myResLoader);
        
        db = factory.findCorrectDatabaseImplementation(
                new JdbcConnection(cn));
        if (!StringUtils.isBlank(myDefaultSchema)) {
            db.setDefaultSchemaName(myDefaultSchema);
        }
        
        liquibase = new Liquibase(myChangeLog, fileOpener, db);
        if (myParams != null) {
            for (Map.Entry<String, Object> param : myParams.entrySet()) {
                liquibase.setChangeLogParameter(param.getKey(),
                        param.getValue());
            }
        }
        liquibase.setChangeLogParameter(JPALiquibase.PARAM_APPLICATIONCONTEXT,
                myApplCtx);
        
        if (myDropFirst) {
            liquibase.dropAll();
        }
        
        return liquibase;
    }
        
    /**
     * Implementazione del task LiquiBase.
     */
    private final class LiquibaseWork implements org.hibernate.jdbc.Work {

        /**
         * Costruttore.
         */
        LiquibaseWork() {            
        }
        
        /**
         * Esegue l&rsquo;operazione.
         * 
         * @param cn Connessione.
         */
        public void execute(Connection cn) throws SQLException {
            Liquibase liquibase;
            
            try {
                liquibase = createLiquibase(cn);
                liquibase.update(myContexts);
            } catch (Exception ex) {
                throw new SQLException(ex.getMessage(), ex);
            }            
        }        
    }  
}

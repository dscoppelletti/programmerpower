/*
 * Copyright (C) 2011-2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate.security;

import java.util.*;
import org.apache.commons.collections4.*;
import org.springframework.transaction.annotation.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Inizializza l&rsquo;anagrafica degli utenti.
 * 
 * @see   it.scoppelletti.programmerpower.security.User
 * @since 1.0.0
 */
@Final
@Transactional
public class UserInitializer implements Runnable {
    
    @javax.annotation.Resource(name = UserManager.BEAN_NAME)
    private UserManager myUserMgr;
    
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private RoleManager myRoleMgr;
    
    @javax.annotation.Resource(name = PasswordDigester.BEAN_NAME)
    private PasswordDigester myPwdDigester;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
    
    /**
     * Costruttore.
     */
    public UserInitializer() {        
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {
        List<User> list;
                
        list = myUserMgr.listUsers();
        if (!CollectionUtils.isEmpty(list)) {
            return;
        }
        
        SecureString pwd = null;
        User user;
        Role adminRole;
        SecurityResources res = new SecurityResources();
        
        adminRole = myRoleMgr.loadRole(RoleManager.ROLE_ADMIN);
        if (adminRole == null) {
            myUI.display(MessageType.ERROR, res.getRoleNotFoundException(
                    RoleManager.ROLE_ADMIN));
            return;
        }
        
        try {
            pwd = new SecureString("changeit");
            
            user = myUserMgr.newUser("admin");
            user.setPassword(myPwdDigester.digestPassword(pwd));
            user.setName("Administrator");
            user.setEnabled(true);
            user.getGrantedRoles().add(adminRole);
            myUserMgr.saveUser(user);
            myUI.display(MessageType.INFORMATION,
                    res.getUserSavedMessage("admin", "changeit"));
            
            user = myUserMgr.newUser("rememberme");
            user.setPassword(myPwdDigester.digestPassword(pwd));
            user.setName("RememberMe Service");
            user.setEnabled(false);
            myUserMgr.saveUser(user);
            myUI.display(MessageType.INFORMATION, res.getUserSavedMessage(
                    "rememberme", "changeit"));                        
        } finally {
            if (pwd != null) {
                pwd.clear();
                pwd = null;
            }
        }        
    }         
}

/*
 * Copyright (C) 2012-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import it.scoppelletti.programmerpower.console.*;
import it.scoppelletti.programmerpower.resources.*;

/**
 * Opzione per specificare i contesti.
 */
final class CliOptionContextList extends CliOptionStringList {
    private static final String CONTEXT_MIN = "it-scoppelletti-programmerpower";    
    private static final String VALUE_SEP = ",";
    private final ResourceBundle myResources;
    
    /**
     * Costruttore.
     *
     * @param name             Nome dell&rsquo;opzione.
     * @param resourceBaseName Base del nome del contenitore di risorse per la
     *                         localizzazione.
     */   
    CliOptionContextList(String name, String resourceBaseName) {
        super(name, resourceBaseName);
        
        myResources = ResourceBundle.getBundle(
                getClass().getCanonicalName(), ResourceTools.getLocale());             
    }
    
    /**
     * Rappresenta il valore dell&rsquo;opzione con una stringa.
     * 
     * @return Valore.
     */    
    @Override
    public String getStringValue() {
        StringBuilder buf = new StringBuilder();
        Set<String> contexts = new HashSet<String>();
        
        for (String value : getList()) {
            if (!contexts.add(value)) {
                continue;
            }
            
            if (buf.length() > 0) {
                buf.append(CliOptionContextList.VALUE_SEP);
            }
            
            buf.append(value);
        }
        
        return buf.toString();
    }
    
    @Override
    public void check() {
        ProgramResources res = new ProgramResources();
        
        super.check();
                
        if (!isSpecified()) {
            return;
        }
        
        if (!getList().contains(CliOptionContextList.CONTEXT_MIN)) {
            getList().add(CliOptionContextList.CONTEXT_MIN);
        }        
        
        for (String context : getList()) {
            if (!myResources.containsKey(context)) {
                throw new CommandLineArgumentException(
                        res.getContextNotFoundException(context));                
            }
        }
    }    
    
    @Override
    public void writeUsage(Writer writer) {
        String context, desc;
        boolean first;
        Enumeration<String> keys;
        PrintWriter printer = null;
        
        super.writeUsage(writer);
        
        try {
            printer = new PrintWriter(writer);
            
            first = true;
            keys = myResources.getKeys();
            while (keys.hasMoreElements()) {
                context = keys.nextElement();
                
                try {
                    desc = myResources.getString(context);
                } catch(MissingResourceException ex) {
                    desc = null;
                }
                
                if (first) {
                    first = false;
                    printer.print("[");                    
                } else {
                    printer.println();
                }
                printer.print(". ");
                printer.print(context);
                if (!StringUtils.isBlank(desc)) {
                    printer.print(": ");
                    printer.print(desc);
                }
            }                       
            if (!first) {
                printer.println("]");
            }
        } finally {
            if (printer != null) {
                printer.flush();
                printer = null;
            }
        }
    }      
}

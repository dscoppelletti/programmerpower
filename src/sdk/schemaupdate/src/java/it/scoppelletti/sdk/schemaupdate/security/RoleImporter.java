/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate.security;

import java.io.*;
import java.util.*;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.*;
import org.springframework.transaction.annotation.*;
import it.scoppelletti.programmerpower.*;
import it.scoppelletti.programmerpower.io.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.programmerpower.ui.*;

/**
 * Import dei ruoli dall&rsquo;anagrafica implementata attraverso file di
 * propriet&agrave;.
 * 
 * @since 2.1.0
 */
@Final
@Transactional
public class RoleImporter implements Runnable, InitializingBean {

    /**
     * Propriet&agrave; di sistema 
     * {@code it.scoppelletti.programmerpower.io.SharedDataDirectory.dir}:
     * Percorso assoluto del direttorio dei dati condivisi tra tutte le
     * applicazioni.
     */
    public static final String PROP_SHAREDDATADIR =
            "it.scoppelletti.programmerpower.io.SharedDataDirectory.dir";
    
    /**
     * Base del nome del contenitore di risorse per la localizzazione dei ruoli.
     * Il valore della costante &egrave; <CODE>{@value}</CODE>.
     */    
    public static final String BASENAME = "roles";
    
    /**
     * Estensione dei file di propriet&agrave;. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String PROPERTIES_EXT = ".properties";
    
    /**
     * Separatore tra il nome di base di un file ed il codice della
     * localizzazione. Il valore della costante &egrave; <CODE>{@value}</CODE>. 
     */
    public static final String LOCALE_SEP = "_";

    private File myBaseDir;        
    private List<RoleCreator> mySystemRoles;
         
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private RoleManager myRoleMgr;
    
    @javax.annotation.Resource(name = UserInterfaceProvider.BEAN_NAME)
    private UserInterfaceProvider myUI;
        
    /**
     * Costruttore.
     */
    public RoleImporter() {        
    }
    
    /**
     * Imposta i ruoli di sistema.
     * 
     * @param obj Collezione.
     */
    public void setSystemRoles(List<RoleCreator> obj) {
        mySystemRoles = obj;
    }
    
    /**
     * Inizializzazione.
     */
    @Reserved
    public void afterPropertiesSet() throws Exception {
        myBaseDir = initBaseDir();              
    }
    
    /**
     * Inizializza il direttorio dal quale importare i ruoli definiti.
     * 
     * @return Valore.
     */
    private File initBaseDir() {
        String value;
        File dir;
        Exception ex;
        SecurityResources res = new SecurityResources();
        
        value = JVMTools.getProperty(RoleImporter.PROP_SHAREDDATADIR);
        if (StringUtils.isBlank(value)) {
            ex = new ApplicationException(res.getNotImportingRolesMessage(),
                    new PropertyNotSetException(System.class.getName(),
                    RoleImporter.PROP_SHAREDDATADIR));
            myUI.display(MessageType.WARNING, ex);
            return null;
        }        
        
        dir = new File(value);
        if (!dir.isAbsolute()) {
            throw new PathNotAbsoluteException(value);
        }
        
        dir = new File(dir, DefaultRoleManager.class.getCanonicalName());
        return dir;         
    }
    
    /**
     * Esegue l&rsquo;operazione.
     */
    public void run() {
        Map<String, Role> roleMap = new HashMap<>();
        
        if (myBaseDir != null) {
            loadRoles(roleMap);
        }
        
        if (mySystemRoles != null) {
            fixSystemRoles(roleMap);
        }
        
        if (myBaseDir != null) {
            loadLocalizedDescriptions(roleMap.values());
        }
        
        saveRoles(roleMap.values());
    }
    
    /**
     * Legge i ruoli definiti.
     * 
     * @param roleMap Collezione dei ruoli.
     */
    private void loadRoles(Map<String, Role> roleMap) {
        String code, desc;
        File file;
        Properties props;
        Role role;
        SecurityResources res = new SecurityResources();
        
        file = new File(myBaseDir, RoleImporter.BASENAME.concat(
                RoleImporter.PROPERTIES_EXT));
        props = loadProperties(file);
        if (props.isEmpty()) {
            myUI.display(MessageType.INFORMATION,
                    res.getNoRoleDefinedException());
            return;
        }
        
        for (Object key : props.keySet()) {
            code = (String) key;
            desc = props.getProperty(code);
            if (StringUtils.isBlank(desc)) {
                desc = code;
            }
            
            role = myRoleMgr.newRole(code);
            role.setDescription(desc);
            roleMap.put(role.getCode(), role);
        }        
    }
    
    /**
     * Assicura l&rsquo;esistenza dei ruoli di sistema previsti.
     * 
     * @param roleMap Collezione dei ruoli.
     */
    private void fixSystemRoles(Map<String, Role> roleMap) {
        String code;
        Role role;
        SecurityResources res = new SecurityResources();
                
        for (RoleCreator roleCreator : mySystemRoles) {
            code = roleCreator.getCode();
            if (StringUtils.isBlank(code)) {
                throw new PropertyNotSetException(roleCreator.toString(),
                        "code");
            }
            
            role = roleMap.get(code); 
            if (role == null) {
                myUI.display(MessageType.INFORMATION,
                        res.getRoleNotFoundException(code));
                role = roleCreator.initRole();
                roleMap.put(role.getCode(), role);
            } else {
                myUI.display(MessageType.INFORMATION,
                        res.getRoleAlreadyExistException(code));
                role.setSystem(true);
            }                                                                               
        }               
    }
    
    /**
     * Legge le descrizioni localizzate.
     * 
     * @param roles Collezione dei ruoli.
     */
    private void loadLocalizedDescriptions(Collection<Role> roles) {
        int lenMin;
        String locale, mainName, name, prefix;
        SecurityResources res = new SecurityResources();
        
        mainName = RoleImporter.BASENAME.concat(
                RoleImporter.PROPERTIES_EXT);
        prefix = RoleImporter.BASENAME.concat(
                RoleImporter.LOCALE_SEP);
        lenMin = prefix.length() + RoleImporter.PROPERTIES_EXT.length() + 1;
        
        for (File file : myBaseDir.listFiles()) {
            name = file.getName();
            if (name.equals(mainName)) {
                // Main file
                continue;
            }            
             
            if (name.length() < lenMin || !name.startsWith(prefix) ||
                    !name.endsWith(RoleImporter.PROPERTIES_EXT)) {
                myUI.display(MessageType.WARNING,
                        res.getFileNameNotSupportedException(name));
                continue;            
            }            
            
            locale = name.substring(prefix.length(),
                    name.length() - RoleImporter.PROPERTIES_EXT.length());
            loadLocalizedDescriptions(roles, locale, file);
        }        
    }
        
    /**
     * Legge le descrizioni per una localizzazione.
     * 
     * @param roles  Collezione dei ruoli.
     * @param locale Localizzazione.
     * @param file   File delle descrizioni localizzate. 
     */
    private void loadLocalizedDescriptions(Collection<Role> roles,
            String locale, File file) {
        int variant;
        String desc;
        Properties props;
        SecurityResources res = new SecurityResources();
        
        variant = locale.lastIndexOf(RoleImporter.LOCALE_SEP);
        if (variant >= 0 && variant !=
                locale.indexOf(RoleImporter.LOCALE_SEP)) {
            myUI.display(MessageType.WARNING,
                    res.getLocaleNotSupportedException(locale));
            locale = locale.substring(0, variant);
            if (locale.endsWith(RoleImporter.LOCALE_SEP)) {
                locale.substring(0, locale.length() -
                        RoleImporter.LOCALE_SEP.length());
            }
        }       
        
        if (getLocale(locale) == null) {
            myUI.display(MessageType.ERROR,
                    res.getLocaleNotSupportedException(locale));
            return;
        }
        
        props = loadProperties(file);
        for (Role role : roles) {
            desc = props.getProperty(role.getCode());
            if (StringUtils.isBlank(desc)) {
                continue;
            }
            
            role.getLocalizedDescriptions().put(locale, desc);
        }
    }
        
    /**
     * Crea i ruoli.
     * 
     * @param roles Collezione dei ruoli.
     */
    private void saveRoles(Collection<Role> roles) {
        SecurityResources res = new SecurityResources();
        
        for (Role role : roles) {
            myRoleMgr.saveRole(role);
            myUI.display(MessageType.INFORMATION,
                    res.getRoleSavedMessage(role.getCode()));            
        }                
    }
    
    /**
     * Legge un file di propriet&agrave;.
     * 
     * @param  file File.
     * @return      Collezione.
     */
    private Properties loadProperties(File file) {
        Properties props;
        InputStream in = null;
        
        props = new Properties();
        if (!file.exists()) {
            return props;
        }
        
        try {
            in = new FileInputStream(file);
            props.load(in);
        } catch (IOException ex) {
            throw new IOOperationException(ex);
        } finally {
            in = IOTools.close(in);
        }
        
        return props;
    }        
    
    /**
     * Restituisce la localizzazione corrispondente ad un codice.
     * 
     * <P>La classe {@code Locale} di JDK pubblica i sovraccarichi dei
     * costruttori per le varie combinazioni dei codici (lingua, paese,
     * variante), ma non la possibilit&agrave; di ottenere la localizzazione
     * corrispondente ad un codice nel formato del parametro {@code name}.</P>
     *  
     * @param  name Codice nel formato
     *              <VAR>language</VAR><CODE>_</CODE><VAR>country</VAR> (ad
     *              esempio, {@code it_IT}) o <VAR>language</VAR> (ad esempio,
     *              {@code it}).
     * @return      Oggetto. Se non esiste alcuna localizzazione corrispondente
     *              al codice {@code name} tra quelle disponibili in JVM,
     *              restituisce {@code null}.
     */
    private Locale getLocale(String name) {        
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.toString().equals(name)) {
                return locale;
            }
        }
        
        return null;
    }    
}

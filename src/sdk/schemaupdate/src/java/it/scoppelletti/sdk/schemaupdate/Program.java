/*
 * Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate;

import java.util.List;
import it.scoppelletti.programmerpower.console.CliOption;
import it.scoppelletti.programmerpower.console.CliOptionBoolean;
import it.scoppelletti.programmerpower.console.CliOptionConfig;
import it.scoppelletti.programmerpower.console.CliStandardOptionSet;
import it.scoppelletti.programmerpower.console.security.CliOptionPasswordFile;
import it.scoppelletti.programmerpower.console.security.CliOptionUserName;
import it.scoppelletti.programmerpower.console.spring.ConsoleApplicationRunner;
import it.scoppelletti.programmerpower.data.DataTools;
import it.scoppelletti.programmerpower.resources.ApplicationAbout;

/**
 * Applicazione.
 * 
 * @since 1.0.0
 */
@ApplicationAbout.Annotation(
        copyright = "Copyright (C) 2012-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.",
        licenseResource = "it/scoppelletti/programmerpower/resources/asl2.txt",
        pageUrl = "http://www.scoppelletti.it/programmerpower/schemaupdate")
public final class Program extends ConsoleApplicationRunner {        
    private static final String OPTION_CONTEXT = "context";
    private static final String OPTION_DROPFIRST = "dropfirst";
    private static final String RESOURCE_BASENAME =
            "it.scoppelletti.sdk.schemaupdate.ProgramOptionSet";       
    
    /**
     * Costruttore.
     */
    private Program() {
    }
    
    @Override
    protected void initOptions(List<CliOption> list) {
        CliOption option;
        
        option = new CliOptionConfig(CliStandardOptionSet.OPTION_DATACONFIG,
                CliStandardOptionSet.RESOURCE_BASENAME,
                DataTools.CONFIG_FACTORY);
        option.setSpecifiedCheck(CliOption.SpecifiedCheck.REQUIRED);
        list.add(option);
        
        list.add(new CliOptionUserName());
        list.add(new CliOptionPasswordFile());
        
        option = new CliOptionContextList(Program.OPTION_CONTEXT,
                Program.RESOURCE_BASENAME);
        list.add(option);        
        
        option = new CliOptionBoolean(Program.OPTION_DROPFIRST,
                Program.RESOURCE_BASENAME);
        list.add(option);
         
        super.initOptions(list);
    }    
    
    /**
     * Entry-point.
     * 
     * @param args Argomenti sulla linea di comando.
     * @see <A HREF="${it.scoppelletti.token.referenceUrl}/schemaupdate/cli.htm"
     *      TARGET="_top">Linea di comando</A> 
     */    
    public static void main(String args[]) {
        Program appl = new Program();

        appl.run(args);
    }
    
    @Override
    protected String[] getProfiles() {
        return new  String[] { DataTools.PROFILE };
    }    
}

/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate.security;

import java.security.Principal;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import it.scoppelletti.programmerpower.config.ConfigContext;
import it.scoppelletti.programmerpower.reflect.Final;
import it.scoppelletti.programmerpower.security.PasswordDigester;
import it.scoppelletti.programmerpower.security.RoleManager;
import it.scoppelletti.programmerpower.security.SecureString;
import it.scoppelletti.programmerpower.security.SecurityResources;
import it.scoppelletti.programmerpower.security.login.AbstractUserLoginModule;
import it.scoppelletti.programmerpower.security.login.DefaultAuthorityPrincipal;
import it.scoppelletti.programmerpower.types.StringTools;
import it.scoppelletti.programmerpower.types.ValueTools;

/**
 * Modulo di login.
 * 
 * @since 2.1.0
 */
@Final
public class LoginModule extends AbstractUserLoginModule {
    
    /**
     * Propriet&agrave; {@code pwd}: Password per l&rsquo;autenticazione di un
     * utente.
     */
    public static final String PROP_PASSWORD = "pwd"; 
    
    /**
     * Oggetto di factory del contesto di configurazione delle credenziali di
     * autenticazione.
     */
    public static final ConfigContext.Factory LOGINMODULE_CONFIG_FACTORY =
            new ConfigContext.Factory(LoginModule.class,
            ConfigContext.MODE_SHARED);
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            LoginModule.class);
 
    @javax.annotation.Resource(name = PasswordDigester.BEAN_NAME)
    private PasswordDigester myPwdDigester;
    
    /**
     * Costruttore.
     */
    public LoginModule() {                      
    }
            
    protected Principal authenticate(String name, SecureString pwd) {
        String pwdCode, userPwd;
        ConfigContext configCtx;
        Properties props;
        SecurityResources res = new SecurityResources();
                 
        if (StringUtils.isBlank(name)) {
            myLogger.warn("Argument name not set.");
            return null;
        }               
        if (ValueTools.isNullOrEmpty(pwd)) {
            myLogger.warn("Argument pwd not set.");
            return null;
        }
        
        name = StringUtils.lowerCase(name);
        
        try {
            configCtx =
                    LoginModule.LOGINMODULE_CONFIG_FACTORY.newConfigContext();
            props = configCtx.getProperties(name);
            userPwd = (props == null) ? null : 
                props.getProperty(LoginModule.PROP_PASSWORD);
            if (StringUtils.isBlank(userPwd)) {
                throw new UsernameNotFoundException(
                        res.getUserNotFoundException(name));                
            }
            
            pwdCode = myPwdDigester.digestPassword(pwd);
            if (!StringTools.equals(userPwd, pwdCode, false)) {
                return null;
            }                           
        } catch (Exception ex) {
            myLogger.error("Method authenticate failed.", ex);
            return null;            
        }
        
        return new DefaultAuthorityPrincipal(RoleManager.AUTHORITY_ADMIN);
    }
}

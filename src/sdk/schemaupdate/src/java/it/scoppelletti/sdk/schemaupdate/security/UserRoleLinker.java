/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate.security;

import java.sql.*;
import liquibase.database.jvm.*;
import liquibase.exception.*;
import org.springframework.transaction.annotation.*;
import it.scoppelletti.programmerpower.reflect.*;
import it.scoppelletti.programmerpower.security.*;
import it.scoppelletti.sdk.schemaupdate.*;

/**
 * Modifica del collegamento (utente, ruolo) in modo che utilizzi come chiave
 * del ruolo l&rsquo;identificatore e non il codice utente.
 * 
 * @see   it.scoppelletti.programmerpower.security.Role
 * @see   it.scoppelletti.programmerpower.security.User
 * @since 2.1.0
 */
@Final
@Transactional
public class UserRoleLinker implements JdbcWorker {
    
    @javax.annotation.Resource(name = RoleManager.BEAN_NAME)
    private RoleManager myRoleMgr;
    
    /**
     * Costruttore.
     */
    public UserRoleLinker() {        
    }
    
    public void run(JdbcConnection cn) throws DatabaseException, SQLException {
        String code;
        Role role;
        PreparedStatement stUser = null;        
        ResultSet rsUser = null;
        SecurityResources res = new SecurityResources();
        
        try {
            stUser = cn.prepareStatement(
                    "SELECT user_id, role, role_id FROM sec_user_role",
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE,
                    ResultSet.CLOSE_CURSORS_AT_COMMIT);
            
            rsUser = stUser.executeQuery();                       
            while (rsUser.next()) {
                code = rsUser.getString("role");
                
                role = myRoleMgr.loadRole(code);
                if (role == null) {
                    throw new SQLException(res.getRoleNotFoundException(code));
                }
                
                rsUser.updateInt("role_id", role.getId());
                rsUser.updateRow();
            }           
        } finally {
            if (rsUser != null) {
                rsUser.close();
                rsUser = null;
            }
            if (stUser != null) {
                stUser.close();
                stUser = null;
            }           
        }
    }
}


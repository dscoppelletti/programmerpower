/*
 * Copyright (C) 2012 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.schemaupdate;

import java.io.*;
import java.net.*;
import java.util.*;
import liquibase.resource.*;
import org.springframework.core.io.*;

/**
 * Implementazione dell&rsquo;accesso alle risorse Spring.
 */
final class SpringResourceAccessor implements ResourceAccessor {
    private final String myParentFile;
    private final ResourceLoader myResLoader; 
    
    /**
     * Costruttore.
     */
    SpringResourceAccessor(String parentFile, ResourceLoader loader) {
        myParentFile = parentFile;
        myResLoader = loader;
    }
    
    /**
     * Restituisce il flusso di lettura di una risorsa.
     * 
     * @param  file Nome della risorsa.
     * @return      Flusso di lettura.
     */
    public InputStream getResourceAsStream(String file) throws IOException {
        Resource res;
        
        try {
            res = getResource(file);
            return res.getInputStream();
        } catch(Exception ex) {
            // NOP
        }
        
        return null;
    }

    /**
     * Restituisce le risorse corrispondenti ad un pacchetto.
     * 
     * @param  packageName Nome del pacchetto.
     * @return             Collezione.
     */
    public Enumeration<URL> getResources(String packageName)
            throws IOException {
        List<URL> list = new ArrayList<URL>(1);
        
        list.add(getResource(packageName).getURL());
        
        return Collections.enumeration(list);
    }
    
    /**
     * Restituisce una risorsa.
     * 
     * @param  file Nome della risorsa.
     * @return      Risorsa.
     */
    private Resource getResource(String file) {
        if (isClasspathResource(myParentFile) &&
                !isClasspathResource(file)) {
            file = ResourceLoader.CLASSPATH_URL_PREFIX.concat(file);
        }
        
        return myResLoader.getResource(file);
    }
            
    /**
     * Verifica se il percorso di una risorsa &egrave; relativo al
     * class-path.
     * 
     * @param  path Percorso.
     * @return      Esito della verifica.
     */
    private boolean isClasspathResource(String path) {
        return path.startsWith(ResourceLoader.CLASSPATH_URL_PREFIX);
    }
    
    /**
     * Restituisce il class-loader.
     * 
     * @return Oggetto.
     */
    public ClassLoader toClassLoader() {
        return myResLoader.getClassLoader();
    }        
}

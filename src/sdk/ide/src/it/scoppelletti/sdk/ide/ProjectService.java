/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide;

import java.util.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import it.scoppelletti.sdk.ide.projects.*;

/**
 * Servizio di gestione dei progetti.
 */
final class ProjectService {
    private static final ProjectService myInstance = new ProjectService(); 
    private final ProjectService.ResourceChangeListener myListener;
    
    /**
     * Costruttore.
     */
    private ProjectService() {
        myListener = new ProjectService.ResourceChangeListener();
    }
    
    /**
     * Restituisce l&rsquo;istanza del servizio.
     * 
     * @return Oggetto.
     */
    static ProjectService getInstance() {
        return myInstance;        
    }
        
    /**
     * Avvia il servizio.
     */
    void start() {
        JavaCore.addPreProcessingResourceChangedListener(myListener, 
                IResourceChangeEvent.POST_CHANGE);                       
    }
    
    /**
     * Interrompe il servizio.
     */
    void shutdown() {        
        JavaCore.removePreProcessingResourceChangedListener(myListener);                                
    }
            
    /**
     * Rilevatore delle modifiche delle risorse.
     * 
     * <P>Questo componente esegue il ricalcolo del class-path corrispondente
     * alle dipendenze dei progetti in risposta alla modifica delle risorse
     * opportune.</P>
     * 
     * <P>In caso di import nel workspace di un progetto esistente, il calcolo
     * del class-path risulta eseguito due volte: probabilmente viene sia
     * rilevato l&rsquo;inserimento nel workspace del file dei metadati che
     * eseguito il componente {@code DependenciesContainerInitializer} (apertura
     * del progetto).<BR>
     * Probabilmente succede la stessa cosa anche con il plug-in
     * <ACRONYM TITLE="Plug-in Development Environment">PDE</ACRONYM>, ma con il
     * Programmer Power
     * <ACRONYM TITLE="Integrated Development Environment">IDE</ACRONYM> si nota
     * perch&eacute; i componenti scrivono sulla console.</P>
     */
    private static final class ResourceChangeListener implements
            IResourceChangeListener, IResourceDeltaVisitor {
        private List<IProject> myProjectList;
        
        /**
         * Costruttore. 
         */
        ResourceChangeListener() {
            myProjectList = null;
        }
                
        /**
         * Gestisce la modifica delle risorse.
         * 
         * @param event Evento.
         */
        public void resourceChanged(IResourceChangeEvent event) {
            myProjectList = new ArrayList<IProject>();
            
            try { 
                switch (event.getType()) {
                case IResourceChangeEvent.POST_CHANGE:
                    resourceChanged(event.getDelta());
                    break;
                }
                
                for (IProject project : myProjectList) {
                    setDependenciesContainer(project);
                }
            } finally {
                myProjectList = null;
            }                        
        }

        /**
         * Accoda un progetto da elaborare.
         * 
         * @param project Progetto.
         */
        private void addProject(IProject project) {
            if (!myProjectList.contains(project)) {
                myProjectList.add(project);
            }
        }

        /**
         * Calcola il class-path per un progetto corrispondente alle dipendenze
         * del progetto stesso.
         * 
         * @param project Progetto.
         */
        private void setDependenciesContainer(IProject project) {
            IJavaProject javaProject;
            
            if (!project.isOpen()) {
                return;
            }
            
            javaProject = JavaCore.create(project);
            
            try {
                DependenciesContainer.setClasspath(javaProject, null);
            } catch (CoreException ex) {
                StatusTools.logStatus(ex);
            }
        }
        
        /**
         * Gestisce la modifica delle risorse.
         * 
         * @param delta Evento.
         */
        private void resourceChanged(IResourceDelta delta) {
            if (delta == null) {
                return;
            }
            
            try {
                delta.accept(this);
            } catch (CoreException ex) {
                StatusTools.logStatus(ex);                
            }                        
        }
        
        /**
         * Gestisce la modifica di una risorsa.
         * 
         * @param  delta Evento.
         * @return       Indica se continuare nella visita delle risorse figlie.
         */
        public boolean visit(IResourceDelta delta) throws CoreException {
            boolean ret = false;
            IResource res;
            
            res = delta.getResource();
            if (res.isDerived()) {
                return false;
            }
            
            switch (res.getType()) {
                case IResource.ROOT:
                    ret = true;
                    break;
                    
                case IResource.PROJECT:
                    ret = visitProject(delta, (IProject) res);
                    break;
                    
                case IResource.FOLDER:
                    ret = visitFolder((IFolder) res);
                    break;
                    
                case IResource.FILE:
                    visitFile(delta, (IFile) res);
                    break;
            }
            
            return ret;
        }   
        
        /**
         * Gestisce la modifica di un progetto.
         * 
         * @param delta   Evento.
         * @param project Progetto.
         * @return        Indica se continuare nella visita delle risorse
         *                figlie.
         */
        private boolean visitProject(IResourceDelta delta, IProject project)
                throws CoreException {
            int kind = delta.getKind();
            
            if (kind == IResourceDelta.REMOVED) {                
                return false;
            }

            if (!project.isOpen()) {
                return false;
            }
            
            if (!project.hasNature(JavaCore.NATURE_ID)) {
                return false;
            }         
            
            if (kind == IResourceDelta.ADDED ||
                    (delta.getFlags() & IResourceDelta.OPEN) != 0) {
                // Il class-path viene gia' calcolato dal componente
                // DependenciesContainerInitializer
                return false;                
            }            
            
            return true;
        }
        
        /**
         * Gestisce la modifica di una cartella.
         * 
         * @param  folder Cartella.
         * @return        Indica se continuare nella visita delle risorse
         *                figlie.
         */
        private boolean visitFolder(IFolder folder) {
            IPath metadataPath, modulePath, targetPath;
            IProject project;
            ProjectDirectory projectDir;
           
            project = folder.getProject();
            if (project == null) {
                return false;
            }
            
            projectDir = new ProjectDirectory(project);        
            metadataPath =
                    projectDir.getMetadataFolder().getProjectRelativePath();
            modulePath = projectDir.getModuleFolder().getProjectRelativePath();
            targetPath = folder.getProjectRelativePath();
            
            if (targetPath.equals(metadataPath) ||
                    targetPath.equals(modulePath)) {
                // Il class-path dipende dai metadati
                return true;
            }   
            
            return false;
        }
        
        /**
         * Gestisce la modifica di un file.
         * 
         * @param delta Evento.
         * @param file  File.
         */
        private void visitFile(IResourceDelta delta, IFile file) {
            IPath buildPath, metadataPath, targetPath;
            IProject project;            
            ProjectDirectory projectDir;
            
            if (!isContentChanged(delta)) {                
                return;
            }

            project = file.getProject();
            if (project == null) {
                return;
            }
            
            projectDir = new ProjectDirectory(project);
            
            // Le dipendenze dalle quali calcolare il class-path sono impostate
            // sul file dei metadati del modulo
            metadataPath =
                projectDir.getModuleMetadataFile().getProjectRelativePath();
            
            // Alcuni progetti possono personalizzare il calcolo del class-path
            // sullo script di build
            buildPath = projectDir.getBuildFile().getProjectRelativePath();
            
            targetPath = file.getProjectRelativePath();            
            if (targetPath.equals(metadataPath) ||
                    targetPath.equals(buildPath)) {             
                addProject(project);
            }                                                
        }
        
        /**
         * Verifica se la modifica di una risorsa riguarda il suo contenuto.
         * 
         * @param  delta Evento.
         * @return Esito della verifica.
         */
        private boolean isContentChanged(IResourceDelta delta) {
            int kind = delta.getKind();
            
            if (kind == IResourceDelta.ADDED ||
                    kind == IResourceDelta.REMOVED ||
                    (kind == IResourceDelta.CHANGED &&
                    (delta.getFlags() & IResourceDelta.CONTENT) != 0)) { 
                return true;
            }

            return false;
        }       
    }
}

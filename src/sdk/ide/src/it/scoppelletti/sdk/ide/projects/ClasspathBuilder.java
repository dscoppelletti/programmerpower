/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.projects;

import java.io.*;
import java.util.*;
import javax.xml.*;
import javax.xml.parsers.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import it.scoppelletti.sdk.ide.*;
import it.scoppelletti.sdk.ide.build.*;
import it.scoppelletti.sdk.ide.ui.*;

/**
 * Composizione di un class-path.
 * 
 * @param <E> Interfaccia degli elementi del class-path. Per il class-path di
 *            compilazione, deve essere l&rsquo;interfaccia
 *            {@code IClasspathEntry}; per il class-path di esecuzione, deve
 *            essere l&rsquo;interfaccia {@code IRuntimeClasspathEntry}.            
 */
abstract class ClasspathBuilder<E> extends DefaultHandler {    
    private static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    private static final String VER1_ID =
        "http://www.scoppelletti.it/res/sdk/ide/projects/classpath1.xsd";    
    private static final String VER1_RES = "classpath1.xsd";
    private static final String VER2_ID =
        "http://www.scoppelletti.it/res/sdk/ide/projects/classpath2.xsd";    
    private static final String VER2_RES = "classpath2.xsd";    
    private final IJavaProject myProject;
    private final String myTarget;
    private final String myClasspathProp;
    private final SAXParser mySAXParser;
    private List<E> myList = null;
    
    /**
     * Costruttore.
     * 
     * @param project           Progetto.
     * @param target            Nome del target Ant che esegue l&rsquo;export
     *                          del class-path.
     * @param classpathProperty Nome della propriet&agrave; Ant che specifica il
     *                          nome assoluto del file sul quale il target
     *                          {@code target} deve eseguire l&rsquo;export del
     *                          class-path.
     */
    ClasspathBuilder(IJavaProject project, String target,
            String classpathProperty) throws CoreException {
        if (project == null) {
            throw new NullPointerException("Argument project is null.");
        }
        if (target == null) {
            throw new NullPointerException("Argument target is null.");
        }
        if (classpathProperty == null) {
            throw new NullPointerException(
                    "Argument classpathProperty is null.");
        }
        
        myProject = project;
        myTarget = target;
        myClasspathProp = classpathProperty;
        mySAXParser = initSAXParser();
    }
    
    /**
     * Inizializza il parser XML.
     * 
     * @return Oggetto.
     */    
    private SAXParser initSAXParser() throws CoreException {
        SAXParserFactory saxFactory;
        SAXParser saxParser;
        
        saxFactory = SAXParserFactory.newInstance();
        saxFactory.setNamespaceAware(true);
        saxFactory.setValidating(true);
        
        try {
            saxParser = saxFactory.newSAXParser();            
            saxParser.setProperty(ClasspathBuilder.JAXP_SCHEMA_LANGUAGE,
                    XMLConstants.W3C_XML_SCHEMA_NS_URI);            
        } catch (ParserConfigurationException ex) {
            throw new CoreException(StatusTools.toStatus(ex));
        } catch (SAXException ex) {
            throw new CoreException(StatusTools.toStatus(ex));
        }        
                
        return saxParser;
    }
       
    /**
     * Restituisce il progetto.
     * 
     * @return Oggetto.
     */
    final IJavaProject getProject() {
        return myProject;
    }

    /**
     * Restituisce la descrizione del class-path.
     * 
     * @return Valore.
     */
    abstract String getClasspathDescription();
     
    /**
     * Costruisce il class-path.
     * 
     * @param  monitor Gestore dell&rsquo;avanzamento.
     * @return         Collezione.
     */
    List<E> run(IProgressMonitor monitor) throws CoreException {
        String msg;
        File buildFile, classpathFile;        
        IPath path;
        IFile file;
        IProject project;
        List<E> classpath;
        ProjectDirectory projectDir;            
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, 2);
        
        try {
            project = myProject.getProject();
            projectDir = new ProjectDirectory(project);
        
            file = projectDir.getBuildFile();
            path = file.getLocation();
            buildFile = path.toFile();
        
            // Sembra che durante l'apertura del progetto, lo script di build
            // potrebbe esistere ma non risultare ancora inserito nel workspace:
            // Controllo l'esistenza dello script di build direttamente sul file
            // system.
            if (!buildFile.exists()) {
                msg = String.format("File %1$s not found.",
                        buildFile.getPath());
                StatusTools.logStatus(IStatus.WARNING, msg);
                return new ArrayList<E>(); 
            }
 
            try {
                classpathFile = File.createTempFile("classpath", ".xml");
            } catch (IOException ex) {
                throw new CoreException(StatusTools.toStatus(ex));
            }
                    
            export(buildFile, classpathFile, progress.newChild(1));
            progress.worked(1);
            
            classpath = load(classpathFile);
            progress.worked(1);
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }

        return classpath;
    }
    
    /**
     * Esegue l&rsquo;export del class-path.
     * 
     * @param buildFile     File dello script.
     * @param classpathFile File del class-path.
     * @param monitor       Gestore dell&rsquo;avanzamento.
     */
    private void export(File buildFile, File classpathFile,
            IProgressMonitor monitor) throws
            CoreException {
        BuildRunner runner = new BuildRunner();
        
        runner.setScriptFile(buildFile);
        runner.getTargets().add(myTarget);             
        runner.getUserProperties().put(myClasspathProp,
                classpathFile.getAbsolutePath());
        
        runner.run(monitor);        
    }
    
    /**
     * Legge il file del class-path.
     * 
     * @param  classpathFile File del class-path.
     * @return               Collezione.
     */    
    private List<E> load(File classpathFile) throws CoreException {
        Exception pendingEx = null;
        List<E> list;
        ClasspathBuilder.ClasspathWriter classpathWriter;
        
        myList = new ArrayList<E>();
        try {
            mySAXParser.parse(classpathFile, this);
            // Reinoltro l'eccezione solo dopo aver emesso il file sulla console            
        } catch (IOException ex) {            
            pendingEx = ex;
        } catch (SAXException ex) {
            pendingEx = ex;
        } finally {
            list = myList;
            myList = null;
        }
        
        classpathWriter = new ClasspathBuilder.ClasspathWriter(
                getClasspathDescription(), classpathFile, pendingEx);
        classpathWriter.schedule();
        
        if (pendingEx != null) {
            throw new CoreException(StatusTools.toStatus(pendingEx));
        }
        
        return list;
    }
    
    /**
     * Tag di apertura di un elemento.
     * 
     * @param uri       Spazio dei nomi.
     * @param localName Nome locale.
     * @param qName     Nome qualificato.
     * @param attrs     Attributi.
     */
    @Override
    public final void startElement(String uri, String localName, String qName,
            Attributes attrs) throws SAXException {
        String docLoc, path, source, sourceRoot;
        IPath sourcePath, sourceRootPath;
        E entry;
        
        if (!localName.equals("entry")) {
            return;
        }
        
        path = attrs.getValue("path");
        
        source = attrs.getValue("sourcePath");
        if (source == null || source.isEmpty()) {
            sourcePath = null;
            sourceRoot = null;
        } else {    
            sourcePath = new Path(source);
            sourceRoot = attrs.getValue("sourceRootPath");        
        }
        
        if (sourceRoot == null || sourceRoot.isEmpty()) {
            sourceRootPath = null;
        } else {
            sourceRootPath = new Path(sourceRoot);            
        }
        
        docLoc = attrs.getValue("docURL");

        entry = newClasspathEntry(new Path(path), sourcePath, sourceRootPath,
                docLoc);
        myList.add(entry);        
    }

    /**
     * Istanzia un elemento del class-path.
     * 
     * @param path           Percorso assoluto del modulo JAR.
     * @param sourcePath     Percorso assoluto dell&rsquo;archivio (o del
     *                       direttorio) del codice sorgente. Pu&ograve; essere
     *                       {@code null}, se il modulo JAR non &egrave;
     *                       accompagnato dal codice sorgente.
     * @param sourceRootPath Percorso radice del codice sorgente
     *                       all&rsquo;interno dell&rsquo;archivio
     *                       {@code sourcePath}. Pu&ograve; essere {@code null},
     *                       se il percorso del codice sorgente deve essere
     *                       rilevato automaticamente.
     * @param docURL         Locazione della documentazione API del modulo.
     *                       Pu&ograve; essere {@code null}, se il modulo JAR
     *                       non &egrave; accompagnato dalla documentazione
     *                       API.                   
     * @return               Oggetto.
     */
    abstract E newClasspathEntry(IPath path, IPath sourcePath,
            IPath sourceRootPath, String docURL); 
    
    /**
     * Risolve un&rsquo;entit&agrave;.
     *
     * @param  publicId Identificatore pubblico.
     * @param  systemId Identificatore di sistema.
     * @return          Flusso di lettura dell&rsquo;entit&agrave;.
     */
    @Override
    public final InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException {
        String msg, name;
        InputStream stream;
        
        if (ClasspathBuilder.VER2_ID.equals(systemId)) {
            name = ClasspathBuilder.VER2_RES;
        } else if (ClasspathBuilder.VER1_ID.equals(systemId)) {
            name = ClasspathBuilder.VER1_RES;
        } else {
            msg = String.format("Bad schema (publicId=%1$s, systemId=%2$s).",
                    publicId, systemId);
            throw new SAXException(msg);
        }
        
        stream = getClass().getResourceAsStream(name);
        if (stream == null) {
            msg = String.format("Resource %1$s not found.", name);
            throw new SAXException(msg);
        }
        
        return new InputSource(stream);
    }
    
    /**
     * Errore fatale.
     *
     * <P>Questa implementazione del metodo {@code fatalError} non esegue nulla
     * perch&eacute; SAX provvede comunque a inoltrare l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     */
    @Override
    public final void fatalError(SAXParseException ex) throws SAXException {
    }
    
    /**
     * Errore non fatale.
     *
     * <P>Questa implementazione del metodo {@code error} inoltra
     * l&rsquo;eccezione.</P>
     *
     * @param ex Eccezione.
     */
    @Override
    public final void error(SAXParseException ex) throws SAXException {
        throw ex;
    }
    
    /**
     * Avviso.
     *
     * <P>Questa implementazione del metodo {@code warning} emette
     * l&rsquo;eccezione come segnalazione su log.</P>
     *
     * @param ex Eccezione.
     */
    @Override
    public final void warning(SAXParseException ex) throws SAXException {
        StatusTools.logStatus(IStatus.WARNING, ex);
    }
    
    /**
     * Scrittura del class-path.
     */
    private static final class ClasspathWriter extends DisplayJob {
        private final String myClasspathDesc;
        private final File myClasspathFile;
        private final Exception myException;
        
        /**
         * Costruttore.
         * 
         * @param classpathDescription Descrizione del class-path.
         * @param classpathFile        File del class-path.
         * @param ex                   Eccezione inoltrata dalla lettura del
         *                             class-path. Pu&ograve; essere
         *                             {@code null}.
         */
        ClasspathWriter(String classpathDescription, File classpathFile,
                Exception ex) {
            myClasspathDesc = classpathDescription;
            myClasspathFile = classpathFile;
            myException = ex;
        }
        
        @Override
        protected void run() {
            String msg;
            IDEConsole console = IDEConsole.getInstance();
            
            if (myException != null) {
                console.write(IStatus.ERROR, "");
                msg = String.format("Exception in building %1$s.",
                        myClasspathDesc);
                console.write(IStatus.ERROR, msg);                
                console.write(IStatus.ERROR, myException.getMessage());
            } else {
                console.write(IStatus.INFO, "");
                console.write(IStatus.INFO, myClasspathDesc);
            }            

            try {
                console.write(myClasspathFile);
            } catch (IOException ex) {
                StatusTools.logStatus(ex);
            }
            
            console.write(IStatus.OK, "");
        }
    }    
}

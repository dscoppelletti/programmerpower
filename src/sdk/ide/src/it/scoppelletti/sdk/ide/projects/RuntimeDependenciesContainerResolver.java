/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.projects;

import java.util.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.debug.core.*;
import org.eclipse.jdt.core.*;
import org.eclipse.jdt.launching.*;

/**
 * Risoluzione delle dipendenze di un progetto per la sua esecuzione.
 *    
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/sdk/ide/projects/classpath2.xsd} 
 * @see   #TARGET 
 * @see   it.scoppelletti.sdk.ide.projects.DependenciesContainer
 * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getSourceResourcesFolder
 * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getModuleFolder
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/deps.html#idRuntime"
 *        TARGET="_top">Dipendenze per l&rsquo;esecuzione di un modulo</A>  
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Programmer-Power-Dependencies#idRuntime"
 *        TARGET="_top">Class-path per l&rsquo;esecuzione di un progetto</A>    
 * @since 1.0.0 
 */
public final class RuntimeDependenciesContainerResolver implements
        IRuntimeClasspathEntryResolver2 {

    /**
     * Nome del target Ant che esegue l&rsquo;export del class-path. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     */
    public static final String TARGET = "export-classpath-runtime";
   
    /**
     * Nome della propriet&agrave; Ant che specifica il nome assoluto del file
     * sul quale il target {@code export-classpath-runtime} deve eseguire
     * l&rsquo;export del class-path. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile 
     */
    public static final String PROP_FILEOUT = "it.scoppelletti.sdk.file.out";
    
    /**
     * Costruttore.
     */
    public RuntimeDependenciesContainerResolver() {        
    }
    
    /**
     * Risolve un elemento del class-path per l&rsquo;esecuzione di un progetto.
     * 
     * @param  entry   Elemento del class-path. 
     * @param  project Progetto.
     * @return         Vettore.
     */
    public IRuntimeClasspathEntry[] resolveRuntimeClasspathEntry(            
            IRuntimeClasspathEntry entry, IJavaProject project)
            throws CoreException {        
        List<IRuntimeClasspathEntry> list;
        IRuntimeClasspathEntry[] classpath;
        
        list = buildClasspath(project);        
        classpath = (IRuntimeClasspathEntry[]) list.toArray(
                new IRuntimeClasspathEntry[list.size()]);
        
        return classpath;        
    }

    /**
     * Risolve un elemento del class-path per una configurazione di avvio.
     * 
     * @param  entry         Elemento del class-path.
     * @param  configuration Configurazione di avvio.
     * @return               Vettore. 
     */
    public IRuntimeClasspathEntry[] resolveRuntimeClasspathEntry(
            IRuntimeClasspathEntry entry, ILaunchConfiguration configuration)
            throws CoreException {
        IJavaProject project;
        
        project = JavaRuntime.getJavaProject(configuration);
        if (project == null) {
            return new IRuntimeClasspathEntry[0];
        }
        
        return resolveRuntimeClasspathEntry(entry, project);
    }
       
    /**
     * Costruisce il class-path.
     * 
     * @param  javaProject Progetto.
     * @return             Class-path.
     */
    private List<IRuntimeClasspathEntry> buildClasspath(
            IJavaProject javaProject) throws CoreException {
        IFolder folder;
        IPath path;
        IProject project;
        IRuntimeClasspathEntry entry;
        ProjectDirectory projectDir;
        RuntimeDependenciesContainerResolver.Builder builder;
        List<IRuntimeClasspathEntry> classpath;
        
        project = javaProject.getProject();               
        projectDir = new ProjectDirectory(project);
        classpath = new ArrayList<IRuntimeClasspathEntry>();
        
        folder = projectDir.getSourceResourcesFolder();
        path = folder.getLocation();        
        entry = JavaRuntime.newArchiveRuntimeClasspathEntry(path);
        entry.setClasspathProperty(IRuntimeClasspathEntry.USER_CLASSES);
        classpath.add(entry);
                
        folder = projectDir.getModuleFolder();
        path = folder.getLocation();        
        entry = JavaRuntime.newArchiveRuntimeClasspathEntry(path);
        entry.setClasspathProperty(IRuntimeClasspathEntry.USER_CLASSES);
        classpath.add(entry);
        
        builder = new RuntimeDependenciesContainerResolver.Builder(javaProject);
        classpath.addAll(builder.run(null));
        
        return classpath; 
    }
    
    /**
     * Restituisce l&rsquo;installazione di JVM associata ad un elemento di un
     * class-path.
     * 
     * @param  entry Elemento.
     * @return       Installazione di JVM ({@code null}).
     */
    public IVMInstall resolveVMInstall(IClasspathEntry entry) throws
            CoreException {
        return null;
    }
    
    /**
     * Indica se un elemento di un class-path si riferisce ad una installazione
     * di JVM.
     * 
     * @param  entry Elemento.
     * @return       Indicatore ({@code false}).
     */
    public boolean isVMInstallReference(IClasspathEntry entry) {
        return false;
    }
    
    /**
     * Composizione del class-path di esecuzione corrispondente alle dipendenze
     * di un progetto.
     */
    private static final class Builder extends
            ClasspathBuilder<IRuntimeClasspathEntry> {

        /**
         * Costruttore.
         * 
         * @param project Progetto.
         */
        Builder(IJavaProject project) throws CoreException {
            super(project, RuntimeDependenciesContainerResolver.TARGET,
                    RuntimeDependenciesContainerResolver.PROP_FILEOUT);
        }
        
        @Override
        String getClasspathDescription() {
            return String.format(
                    "Run-time class-path container \"%1$s\" for project \"%2$s\"",
                    DependenciesContainer.NAME, getProject().getElementName());        
        }
        
        @Override
        IRuntimeClasspathEntry newClasspathEntry(IPath path, IPath sourcePath,
                IPath sourceRootPath, String docURL) {
            IRuntimeClasspathEntry entry;
            
            entry = JavaRuntime.newArchiveRuntimeClasspathEntry(path);
            entry.setClasspathProperty(IRuntimeClasspathEntry.USER_CLASSES);                
            entry.setSourceAttachmentPath(sourcePath);        
            entry.setSourceAttachmentRootPath(sourceRootPath);
            
            return entry;
        }                
    }        
}

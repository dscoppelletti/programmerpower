/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.jface.text.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.console.*;

/**
 * Visualizzatore della console.
 */
final class IDEConsoleViewer extends TextConsoleViewer implements
        IDocumentListener {

    /**
     * Costruttore.
     * 
     * @param parent  Controllo parent.
     * @param console Console.
     */
    IDEConsoleViewer(Composite parent, TextConsole console) {
        super(parent, console);
 
        DisplayJob job;
        
        job = new IDEConsoleViewer.SetReadonlyJob(this);
        job.schedule();
    }
    
    /**
     * Imposta il documento.
     * 
     * @param document Documento.
     */
    @Override
    public void setDocument(IDocument document) {
        IDocument prevDoc = getDocument();
        
        super.setDocument(document);
        
        if (prevDoc != null) {
            prevDoc.removeDocumentListener(this);
        } 
        if (document != null) {
            document.addDocumentListener(this);
        }        
    }
    
    /**
     * Richiesta di modifica del documento.
     * 
     * @param event Evento.
     */
    public void documentAboutToBeChanged(DocumentEvent event) {        
    }
    
    /**
     * Modifica del documento.
     * 
     * @param event Evento.
     */
    public void documentChanged(DocumentEvent event) {
        revealEndOfDocument();
    }
        
    /**
     * Impostazione della console a sola lettura.
     */
    private static final class SetReadonlyJob extends DisplayJob {
        private final ITextViewer myViewer;
        
        /**
         * Costruttore.
         * 
         * @param viewer Visualizzatore.
         */
        SetReadonlyJob(ITextViewer viewer) {
            myViewer = viewer;
        }
        
        @Override
        protected void run() {
            StyledText text;
            
            text = myViewer.getTextWidget();
            if (text != null && !text.isDisposed()) {
                text.setEditable(false);
            }
        }
    }        
}

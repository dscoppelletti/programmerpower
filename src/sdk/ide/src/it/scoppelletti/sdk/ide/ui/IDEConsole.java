/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.io.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.dialogs.*;
import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.console.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Console di Programmer Power
 * <ACRONYM TITLE="Integrated Development Environment">IDE</ACRONYM>.
 *
 * <P>Bisognerebbe evitare di eseguire scritture massive nella console che
 * bloccherebbero il thread della <ACRONYM TITLE="User Interface">UI</ACRONYM>:
 * in questi casi &egrave; possibile utilizzare una classe derivata dalla classe
 * {@code DisplayJob}.</P>
 *   
 * @see   it.scoppelletti.sdk.ide.ui.DisplayJob
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Console-di-Programmer-Power-IDE"
 *        TARGET="_top">Console di Programmer Power IDE</A>
 * @since 1.0.0
 */
public final class IDEConsole {
    private static final String PREF_SHOWCONSOLE =
        "it.scoppelletti.sdk.ide.ui.IDEConsole.showconsole";
    private static IDEConsole myInstance = new IDEConsole();
    private final Object mySyncRoot = new Object();
    private IDEConsoleImpl myConsole = null;
    
    /**
     * Costruttore.
     */
    private IDEConsole() {
    }
    
    /**
     * Restituisce l&rsquo;istanza della console.
     * 
     * @return Oggetto.
     */
    public static IDEConsole getInstance() {
        return myInstance;
    }
               
    /**
     * Apre la console.
     */
    public void openConsole() {
        boolean found;
        IDialogSettings settings;
        IDEPlugin plugin = IDEPlugin.getInstance();        
        ConsolePlugin consolePlugin = ConsolePlugin.getDefault();
        IConsoleManager consoleMgr = consolePlugin.getConsoleManager();
        
        synchronized (mySyncRoot) {
            if (myConsole == null) {
                myConsole = new IDEConsoleImpl();   
            }
            
            found = false;            
            for (IConsole console : consoleMgr.getConsoles()) {
                if (console == myConsole) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                // La console non e' ancora stata inserita nel ConsoleManager:
                // La inserisco ed attivo il gestore del ciclo di vita.
                consoleMgr.addConsoles(new IConsole[] { myConsole } );
                consoleMgr.addConsoleListener(new IDEConsole.LifeCycle(this));
            }
            
            consoleMgr.showConsoleView(myConsole);            
        }         
        
        settings = plugin.getDialogSettings();
        settings.put(IDEConsole.PREF_SHOWCONSOLE, true);
    }
    
    /**
     * Chiude la console (se &egrave; stata rimossa).
     * 
     * @param  consoles Elenco delle console rimosse.
     * @return          Indica se la console &egrave; stata chiusa.
     */
    private boolean closeConsole(IConsole[] consoles) {        
        synchronized (mySyncRoot) {
            if (myConsole == null) {
                // Questo metodo non dovrebbe essere stato eseguito perche' il
                // gestore del ciclo di vita non dovrebbe essere collegato se la
                // console e' stata chiusa:
                // Faccio finta di averla chiusa adesso la console in modo da
                // scollegare il gestore del ciclo di vita.
                return true;
            }
            
            for (IConsole console : consoles) {
                if (console == myConsole) {
                    // Questo metodo e' eseguito alla rimozione della console
                    // attraverso il gestore del ciclo di vita, quindi la
                    // console e' gia' stata rilasciata:
                    // Mi limito ad azzerarne il riferimento.
                    myConsole = null;                   
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Attiva il servizio della console.
     */
    public static void startService() {
        IDialogSettings settings;
        IDEPlugin plugin = IDEPlugin.getInstance();
        
        settings = plugin.getDialogSettings();
        if (settings.getBoolean(IDEConsole.PREF_SHOWCONSOLE)) {
            myInstance.openConsole();
        }
    }
    
    /**
     * Scrive un messaggio.
     * 
     * @param severity Livello di attenzione.
     * @param message  Messaggio.
     */
    public void write(int severity, String message) {
        int color = -1;
        boolean autoOpen = false;
        IDEConsoleStream writer = null;
        Display display = Display.getDefault();
        
        synchronized (mySyncRoot) {                                    
            if ((severity & IStatus.ERROR) == IStatus.ERROR) {
                color = SWT.COLOR_RED;
                autoOpen = true;
            } else if ((severity & IStatus.WARNING) == IStatus.WARNING) {
                color = SWT.COLOR_DARK_YELLOW;
                autoOpen = true;
            } else if ((severity & IStatus.CANCEL) == IStatus.CANCEL) {
                color = SWT.COLOR_DARK_YELLOW;
            } else if ((severity & IStatus.INFO) == IStatus.INFO) {
                color = SWT.COLOR_BLUE;
            }
            
            if (autoOpen) {
                openConsole();
            }
            
            if (myConsole == null) {
                // La console e' chiusa:
                // Evito la scrittura (perdendo quindi i messaggi).
                return;
            }
            
            writer = myConsole.newConsoleStream();
            if (color > 0) {
                writer.setColor(display.getSystemColor(color));
            }
            
            try {
                writer.writeln(message);
            } catch (IOException ex) {
                StatusTools.displayStatus(ex);
            } finally {    
            	writer = IOTools.close(writer);
            }
        }            
    }
    
    /**
     * Scrive un file.
     * 
     * @param file File.
     */    
    public void write(File file) throws IOException {
        String line;
        Reader in = null;
        BufferedReader reader = null;
        IDEConsoleStream writer = null;
        
        synchronized (mySyncRoot) {
            if (myConsole == null) {
                // La console e' chiusa:
                // Evito la scrittura (perdendo quindi i messaggi).
                return;
            }
                        
            writer = myConsole.newConsoleStream();
            try {
                in = new FileReader(file);
                reader = new BufferedReader(in);
                in = null;
                
                line = reader.readLine();
                while (line != null) {
                    writer.writeln(line);
                    line = reader.readLine();
                }
            } finally {
            	reader = IOTools.close(reader);
            	in = IOTools.close(in);
            	writer = IOTools.close(writer);
            }
        }
    }
    
    /**
     * Gestore del ciclo di vita delle console.
     */
    private static final class LifeCycle implements IConsoleListener {
        private final IDEConsole myConsole;
        
        /**
         * Costruttore.
         * 
         * @param console Console.
         */
        LifeCycle(IDEConsole console) {
            myConsole = console;
        }
        
        /**
         * Inserimento di alcune console.
         * 
         * @param consoles Elenco delle console inserite.
         */
        public void consolesAdded(IConsole[] consoles) {
        }

        /**
         * Rimozione di alcune console.
         * 
         * @param consoles Elenco delle console rimosse.
         */
        public void consolesRemoved(IConsole[] consoles) {            
            IConsoleManager consoleMgr;
            IDialogSettings settings;
            IDEPlugin plugin;
            ConsolePlugin consolePlugin;
            
            if (myConsole.closeConsole(consoles)) {
                // Ho chiuso la console:
                // Rimuovo il gestore del ciclo di vita.
                consolePlugin = ConsolePlugin.getDefault();
                consoleMgr = consolePlugin.getConsoleManager();
                consoleMgr.removeConsoleListener(this);
                
                plugin = IDEPlugin.getInstance();
                if (plugin != null) {
                    // Eclipse 3.5.2: sembra che questo metodo possa essere
                    // eseguito anche dopo aver terminato il plug-in.
                    settings = plugin.getDialogSettings();
                    settings.put(IDEConsole.PREF_SHOWCONSOLE, false);
                }
            }
        }        
    }    
}

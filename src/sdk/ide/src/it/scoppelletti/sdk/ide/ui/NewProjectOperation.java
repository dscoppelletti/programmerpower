/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.io.*;
import java.lang.reflect.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import org.eclipse.jdt.launching.*;
import org.eclipse.jface.operation.*;
import it.scoppelletti.sdk.ide.*;
import it.scoppelletti.sdk.ide.projects.*;

/**
 * Operazione di creazione di un nuovo progetto.
 */
final class NewProjectOperation extends NewProjectOperationBase {
    private static final String MODULE_TEMPLATE = "template_module.txt";   
    private static final String BUILD_TEMPLATE = "template_build.txt";         
    private static final String LOCAL_TEMPLATE = "template_local.txt";
    private static final String CMD_TEMPLATE = "template_cmd.txt";
    private static final String BAT_TEMPLATE = "template_bat.txt";
    private static final String BAT_EXT = ".bat";
    
    /**
     * Costruttore.
     * 
     * @param params Parametri.
     */
    private NewProjectOperation(INewProjectParameters params) {
        super(params);
    }           

    /**
     * Istanzia l&rsquo;operazione di creazione di un nuovo progetto.
     * 
     * @param  params Parametri.
     * @return        Operazione.
     */
    static IRunnableWithProgress newOperation(INewProjectParameters params) {
        if (params == null) {
            throw new NullPointerException("Argument params is null.");
        }
        
        if (INewProjectParameters.TARGET_WAR.equals(params.getTarget())) {
            return new NewWebProjectOperation(params);
        }
        
        return new NewProjectOperation(params);
    }
    
    /**
     * Esegue l&rsquo;operazione.
     * 
     * @param monitor Gestore dell&rsquo;avanzamento.
     */
    @Override
    protected void execute(IProgressMonitor monitor) throws CoreException,
            InvocationTargetException, InterruptedException {
        IProject project;
        IJavaProject javaProject;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, "Creating...", 5);
        
        try {         
            project = createProject(progress.newChild(1));
            progress.worked(1);
        
            createFolders(project, progress.newChild(1));                        
            progress.worked(1);
            
            createFiles(project, progress.newChild(1));
            progress.worked(1);
            
            javaProject = createJavaProject(project, progress.newChild(1));
            progress.worked(1);                   
            
            setClasspath(javaProject, progress.newChild(1));
            progress.worked(1);                        
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }
    }
        
    /**
     * Crea la struttura dei direttori.
     * 
     * @param project Progetto.
     * @param monitor Gestore dell&rsquo;avanzamento.
     */
    private void createFolders(IProject project, IProgressMonitor monitor)
            throws CoreException {
        String value;
        ProjectDirectory projectDir;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, "Creating the directory tree...",
                6);
        
        try {
            projectDir = new ProjectDirectory(project);
            
            createFolder(projectDir.getSourceJavaFolder(),
                    progress.newChild(1));
            progress.worked(1);
            
            value = getParams().getMainClass();
            if (value != null && !value.isEmpty()) {            
                createFolder(projectDir.getSourceBinFolder(),
                        progress.newChild(1));
            }
            progress.worked(1);
            
            createFolder(projectDir.getSourceEtcFolder(), progress.newChild(1));
            progress.worked(1);

            createFolder(projectDir.getSourceResourcesFolder(),
                    progress.newChild(1));                
            progress.worked(1);
            
            createFolder(projectDir.getModuleFolder(), progress.newChild(1));
            progress.worked(1);
            
            createFolder(projectDir.getBuildClassesFolder(),
                    progress.newChild(1));
            progress.worked(1);
        } finally {
            if (monitor != null) {
                monitor.done();
            }                
        }               
    }    
    
    /**
     * Crea i file.
     * 
     * @param project Progetto.
     * @param monitor Gestore dell&rsquo;avanzamento.
     */
    private void createFiles(IProject project, IProgressMonitor monitor) throws
            CoreException {
        String contents, value;
        IFile file;
        IFolder folder;        
        ProjectDirectory projectDir;
        SubMonitor progress;

        progress = SubMonitor.convert(monitor, "Creating the files...", 12);
        
        try {
            projectDir = new ProjectDirectory(project);

            contents = buildFilteredContents(
                    NewProjectOperation.MODULE_TEMPLATE, progress.newChild(1));
            progress.worked(1);

            createFile(projectDir.getModuleMetadataFile(), contents,
                    progress.newChild(1));
            progress.worked(1);

            contents = buildManifest(progress.newChild(1));
            progress.worked(1);

            createFile(projectDir.getManifestFile(), contents,
                    progress.newChild(1));
            progress.worked(1);

            contents = buildFilteredContents(NewProjectOperation.BUILD_TEMPLATE,
                    progress.newChild(1));
            progress.worked(1);

            createFile(projectDir.getBuildFile(), contents,
                    progress.newChild(1));
            progress.worked(1);

            value = getParams().getMainClass();
            if (value != null && !value.isEmpty()) {
                folder = projectDir.getSourceBinFolder();
                
                contents = buildFilteredContents(
                        NewProjectOperation.CMD_TEMPLATE, progress.newChild(1));
                progress.worked(1);            
                                
                file = folder.getFile(getParams().getBaseName());
                createFile(file, contents, progress.newChild(1));
                progress.worked(1);              
                
                contents = buildFilteredContents(
                        NewProjectOperation.BAT_TEMPLATE, progress.newChild(1));
                progress.worked(1);              
                
                value = getParams().getBaseName().concat(
                        NewProjectOperation.BAT_EXT);
                file = folder.getFile(value);                
                createFile(file, contents, progress.newChild(1));
                progress.worked(1);                     
            } else {
                progress.worked(4);
            }
            
            contents = buildFilteredContents(NewProjectOperation.LOCAL_TEMPLATE,
                    progress.newChild(1));
            progress.worked(1);

            createFile(projectDir.getEtcLocalFile(), contents,
                    progress.newChild(1));
            progress.worked(1);                            
        } catch (IOException ex) {
            throw new CoreException(StatusTools.toStatus(ex));
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }        
    }    
    
    /**
     * Imposta il class-path.
     * 
     * @param project Progetto.
     * @param monitor Gestore dell&rsquo;avanzamento.
     */
    private void setClasspath(IJavaProject project, IProgressMonitor monitor)
            throws CoreException, JavaModelException {
        IPath path;
        IFolder outputFolder, sourceFolder;
        IClasspathEntry[] classpath = new IClasspathEntry[3];
        ProjectDirectory projectDir;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, "Setting class-path...", 2);
        try {
            projectDir = new ProjectDirectory(project.getProject());

            outputFolder = projectDir.getBuildClassesFolder();
            project.setOutputLocation(outputFolder.getFullPath(),
                    progress.newChild(1));
            progress.worked(1);
                   
            sourceFolder = projectDir.getSourceJavaFolder();
            classpath[0] = JavaCore.newSourceEntry(sourceFolder.getFullPath());

            path = JavaRuntime.newDefaultJREContainerPath();
            classpath[1] = JavaCore.newContainerEntry(path);

            classpath[2] = JavaCore.newContainerEntry(
                    DependenciesContainer.PATH);
       
            project.setRawClasspath(classpath, progress.newChild(1));
            progress.worked(1);
        } finally {
            if (monitor != null) {
                monitor.done();
            }            
        }
    }    
}

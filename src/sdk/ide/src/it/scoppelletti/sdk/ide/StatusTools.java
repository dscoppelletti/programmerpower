/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide;

import org.eclipse.core.runtime.*;
import org.eclipse.jface.dialogs.*;
import org.eclipse.swt.widgets.*;
import org.xml.sax.*;

/**
 * Funzioni di utilit&agrave; per la gestione del risultato delle operazioni.
 *
 * @since 1.2.0
 */
public final class StatusTools {

    /**
     * Costruttore privato per classe statica.
     */
    private StatusTools() {
    }

    /**
     * Converte un messaggio in un risultato.
     * 
     * @param severity Livello di attenzione. 
     * @param message  Messaggio. 
     * @return         Oggetto.
     */
    public static IStatus toStatus(int severity, String message) {
        return new Status(severity, IDEPlugin.ID, message);
    }
    
    /**
     * Converte un&rsquo;eccezione in un risultato.
     * 
     * @param ex Eccezione. 
     * @return   Oggetto.
     */
    public static IStatus toStatus(Throwable ex) {
        return StatusTools.toStatus(IStatus.ERROR, ex);
    }    

    /**
     * Converte un&rsquo;eccezione in un risultato.
     * 
     * @param severity Livello di attenzione. Normalmente le eccezioni
     *                 rappresentano degli errori ({@code IStatus.ERROR}); il
     *                 livello di attenzione pu&ograve; tuttavia anche essere
     *                 abbassato ad esempio ad avviso ({@code IStatus.WARNING}).
     * @param ex       Eccezione. 
     * @return         Oggetto.
     */
    private static IStatus toStatus(int severity, Throwable ex) {
        String msg;
        IStatus status;
        CoreException coreEx;
        SAXParseException xmlEx;
        
        if (ex instanceof CoreException) {
            coreEx = (CoreException) ex;
            status = coreEx.getStatus();            
        } else if (ex instanceof SAXParseException) {
            xmlEx = (SAXParseException) ex;            
            msg = String.format(
                    "%1$s [line=%2$d, column=%3$d, publicId=%4$s, systemId=%5$s]",
                    xmlEx.getMessage(), xmlEx.getLineNumber(),
                    xmlEx.getColumnNumber(), xmlEx.getPublicId(),
                    xmlEx.getSystemId());                
            status = new Status(severity, IDEPlugin.ID, msg, ex);
        } else {                
            status = new Status(IStatus.ERROR, IDEPlugin.ID, ex.getMessage(),
                    ex);
        }
        
        return status;
    }
        
    /**
     * Emette una segnalazione su log.
     * 
     * @param status Risultato.
     */
    public static void logStatus(IStatus status) {
        IDEPlugin plugin = IDEPlugin.getInstance();
        
        plugin.getLog().log(status);        
    }
        
    /**
     * Emette una segnalazione su log.
     * 
     * @param severity Livello di attenzione. 
     * @param message  Messaggio.
     */
    public static void logStatus(int severity, String message) {
        StatusTools.logStatus(StatusTools.toStatus(severity, message));
    }

    /**
     * Emette un&rsquo;eccezione su log.
     * 
     * @param ex Eccezione.
     */
    public static void logStatus(Throwable ex) {
        StatusTools.logStatus(StatusTools.toStatus(ex));                
    }
    
    /**
     * Emette un&rsquo;eccezione su log.
     * 
     * @param severity Livello di attenzione. Normalmente le eccezioni
     *                 rappresentano degli errori ({@code IStatus.ERROR}); 
     *                 un&rsquo;eccezione pu&ograve; tuttavia anche essere
     *                 emessa su log abbassando il livello di attenzione ad
     *                 esempio ad avviso ({@code IStatus.WARNING}). 
     * @param ex       Eccezione.
     */
    public static void logStatus(int severity, Throwable ex) {
        StatusTools.logStatus(StatusTools.toStatus(severity, ex));                
    }

    /**
     * Visualizza una segnalazione.
     * 
     * @param ex Eccezione.
     */ 
    private static void displayStatus(IStatus status) {
        Display display;
        IDEPlugin plugin = IDEPlugin.getInstance();
        
        StatusTools.logStatus(status);
        
        display = plugin.getCurrentDisplay();       
        display.asyncExec(new StatusTools.ErrorDisplayer(status));        
    }  
    
    /**
     * Visualizza una segnalazione.
     * 
     * @param severity Livello di attenzione. 
     * @param message  Messaggio.
     * @since          1.1.0
     */
    public static void displayStatus(int severity, String message) {
        StatusTools.displayStatus(StatusTools.toStatus(severity, message));
    }
    
    /**
     * Visualizza un&rsquo;eccezione.
     * 
     * @param ex Eccezione.
     */ 
    public static void displayStatus(Throwable ex) {
        StatusTools.displayStatus(StatusTools.toStatus(ex));
    }      
    
    /**
     * Visualizzatore di un risultato.
     */
    private static final class ErrorDisplayer implements Runnable {
        private final IStatus myStatus;
        
        /**
         * Costruttore.
         * 
         * @param status Risultato.
         */
        ErrorDisplayer(IStatus status) {
            myStatus = status;
        }
        
        /**
         * Esegue l&rsquo;operazione.
         */
        public void run() {
            ErrorDialog.openError(null, null, null, myStatus);
        }
    }
}

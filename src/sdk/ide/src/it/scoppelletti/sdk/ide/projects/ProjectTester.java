/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.projects;

import org.eclipse.core.expressions.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Test sulle propriet&agrave; di un progetto.
 * 
 * @since 1.2.0
 */
public final class ProjectTester extends PropertyTester {

    /**
     * Propriet&agrave; {@code hasJavaNature}.
     */
    public static final String PROP_HASJAVANATURE = "hasJavaNature";
    
    /**
     * Costruttore.
     */
    public ProjectTester() {        
    }
    
    /**
     * Esegue un test.
     * 
     * @param receiver      Oggetto sul quale eseguire il test.
     * @param property      Propriet&agrave; dell&rsquo;oggetto da verificare.
     * @param args          Parametri aggiuntivi del test.
     * @param expectedValue Valore atteso della propriet&agrave;. 
     */
    public boolean test(Object receiver, String property, Object[] args,
            Object expectedValue) {
        boolean b;
        IProject project = (IProject) receiver;
        
        if (ProjectTester.PROP_HASJAVANATURE.equals(property)) {
            try {
                b = project.hasNature(JavaCore.NATURE_ID);
                if (expectedValue == null) {
                    return b;
                }
                
                return (b == ((Boolean) expectedValue).booleanValue());
            } catch (CoreException ex) {
                StatusTools.logStatus(ex);
                return false;
            }
        }
        
        return false;
    }
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.build;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.swt.widgets.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Direttorio di output di un processo di build.
 * 
 * <P>Se un processo di build scrive in un direttorio appartenente ad un
 * progetto, pu&ograve; istanziare preventivamente un oggetto
 * {@code BuildOutputDirectory} per garantire le seguenti condizioni:</P>
 * 
 * <OL>
 * <LI>Il direttorio esiste.
 * <LI>Tutti i file del direttorio (e ricorsivamente dei sotto-direttori) sono
 * scrivibili.
 * <LI>Nessuno dei file del direttorio (e ricorsivamente dei sotto-direttori)
 * &egrave; soggetto al controllo di revisione.
 * </OL>
 * 
 * @since 1.0.0
 */
public final class BuildOutputDirectory {   
    private final IFolder myFolder;
    
    /**
     * Costruttore.
     * 
     * @param folder Direttorio.
     */    
    public BuildOutputDirectory(IFolder folder) throws CoreException {
        if (folder == null) {
            throw new NullPointerException("Argument folder is null.");
        }

        myFolder = folder;
        initFolder();                
    }    
    
    /**
     * Inizializza il direttorio.
     */
    private void initFolder() throws CoreException {
        createFolder(myFolder);
        myFolder.accept(new BuildOutputDirectory.FolderVisitor());        
    }
    
    /**
     * Esegue il refresh del direttorio.
     * 
     * <P>I processi di build eseguono normalmente un refresh del direttorio di
     * output al termine dell&rsquo;elaborazione.</P> 
     */
    public void refresh() throws CoreException {
        Display display;
     
        // Reinizializzo il direttorio perche' potrebbe anche essere stato 
        // cancellato e inoltre potrebbero essere state create delle risorse a
        // sola lettura o soggette al controllo di revisione.         
        initFolder();        
     
        display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
             
        display.asyncExec(new BuildOutputDirectory.RefreshJob(myFolder));
    }
    
    /**
     * Crea un direttorio (se non esiste gi&agrave;).
     * 
     * <P>Se il direttorio &egrave; un sotto-direttorio di un direttorio non
     * esistente, crea anche il direttorio parent ricorsivamente.</P>
     * 
     * @param folder Direttorio.
     */
    private void createFolder(IFolder folder) throws CoreException {
        IContainer parent;
        
        if (folder.exists()) {
            return;
        }
        
        parent = folder.getParent();
        if (parent instanceof IFolder) {
            createFolder((IFolder) parent);
        }
        
        folder.create(true, true, null);
    }        
    
    /**
     * Visita del direttorio di lavoro per assicurarsi che tutte le risorse
     * siano scrivibili e non siano sotto il controllo di revisione.
     */
    private static final class FolderVisitor implements IResourceVisitor {
     
        /**
         * Costruttore.
         */
        FolderVisitor() {            
        }
     
        /**
         * Elaborazione di una risorsa.
         * 
         * @param  resource Risorsa.
         * @return          Indica se continuare nella visita delle risorse
         *                  figlie. 
         */
        public boolean visit(IResource resource) throws CoreException {
            resource.setDerived(true, null);
            setReadOnly(resource, false);
         
            return (resource.getType() != IResource.FILE);
        }
 
        /**
         * Imposta l&rsquo;indicatore di sola lettura per una risorsa.
         * 
         * @param resource Risorsa.
         * @param readOnly Valore.
         */
        private void setReadOnly(IResource resource, boolean readOnly) {
            ResourceAttributes attrs;
         
            attrs = resource.getResourceAttributes();
            if (attrs == null) {
                // Gli attributi non sono supportati per la piattaforma o per il
                // tipo di risorsa
                return;
            }
         
            attrs.setReadOnly(readOnly);
            try {
                resource.setResourceAttributes(attrs);
            } catch (CoreException ex) {
                // L'attributo potrebbe non essere supportato dal file system
            }
        }        
    }
    
    /**
     * Refresh di una risorsa.
     * 
     * <P>La struttura delle risorse non &egrave; modificabile durante la
     * notifica di un evento {@code IResourceChangeEvent.POST_CHANGE} (es.
     * attraverso un oggetto {@code IResourceChangeListener}: in questo caso
     * l&rsquo;esecuzione del metodo {@code IResource.refreshLocal} inoltra
     * l&rsquo;eccezione &ldquo;The resource tree is locked for
     * modifications.&rdquo;.</P>
     */
    private static final class RefreshJob implements Runnable {
        private final IResource myResource;
     
        /**
         * Costruttore.
         * 
         * @param resource Risorsa.
         */
        RefreshJob(IResource resource) {
            myResource = resource;
        }
     
        /**
         * Implementa l&rsquo;operazione.
         */
        public void run() {
            try {
                myResource.refreshLocal(IResource.DEPTH_INFINITE, null);
            } catch (CoreException ex) {
                StatusTools.logStatus(ex.getStatus());
            }
        }        
    }      
}

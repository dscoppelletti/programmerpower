/*
 * Copyright (C) 2008-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.jface.resource.*;
import org.eclipse.ui.console.*;
import org.eclipse.ui.part.*;

/**
 * Implementazione della console.
 */
final class IDEConsoleImpl extends IOConsole {
    private static final String NAME = "Programmer Power IDE";
    private static final String TYPE = "it.scoppelletti.sdk.ide.ui.IDEConsole";
    private static final String IMAGE_RES = "programmerpower16x16.gif";     
    private final IDEConsolePartitioner myPartitioner;
    
    /**
     * Costruttore.
     */
    IDEConsoleImpl() {
        super(IDEConsoleImpl.NAME, IDEConsoleImpl.TYPE,
                ImageDescriptor.createFromFile(IDEConsoleImpl.class,
                IDEConsoleImpl.IMAGE_RES), true);
        
        myPartitioner = new IDEConsolePartitioner(this);
        myPartitioner.connect(getDocument());
    }
    
    /**
     * Rilascia la console.
     */
    @Override
    public void dispose() {
        super.dispose();        
        myPartitioner.close();
        myPartitioner.disconnect();
    }
    
    /**
     * Restituisce il flusso di lettura dalla tastiera.
     *  
     * @return Flusso di lettura.
     * @throws java.lang.UnsupportedOperationException La console
     *         {@code IDEConsole} non supporta l&rsquo;input da tastiera.
     */
    @Override
    public IOConsoleInputStream getInputStream() {
        throw new UnsupportedOperationException(
                "IDEConsoleImpl.getInputStream");
    }

    /**
     * Crea una nuova pagina.
     * 
     * @return Pagina. 
     */
    @Override
    public IPageBookViewPage createPage(IConsoleView view) {
        return new IDEConsolePage(this, view);
    }
    
    /**
     * Crea un nuovo flusso di scrittura.
     * 
     * @return Flusso di scrittura.
     * @throws java.lang.UnsupportedOperationException Il flusso 
     *         {@code IOConsoleOutputStream} &egrave; dichiarato dalla
     *         documentazione come non derivabile (pur non essendo
     *         {@code final}, quindi la console {@code IDEConsole} deve
     *         utilizzare un flusso implementato ad hoc. 
     */
    @Override
    public IOConsoleOutputStream newOutputStream() {
        throw new UnsupportedOperationException(
                "IDEConsoleImpl.newOutputStream");
    }

    /**
     * Crea un nuovo flusso di scrittura.
     * 
     * @return Flusso di scrittura.
     */    
    IDEConsoleStream newConsoleStream() {
        return new IDEConsoleStream(this, myPartitioner);
    }    
}

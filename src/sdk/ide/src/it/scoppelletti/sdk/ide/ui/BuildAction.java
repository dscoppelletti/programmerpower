/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.lang.reflect.*;
import java.util.*;
import org.eclipse.core.commands.*;
import org.eclipse.core.commands.common.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.operation.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.ui.handlers.*;
import it.scoppelletti.sdk.ide.*;
import it.scoppelletti.sdk.ide.build.*;
import it.scoppelletti.sdk.ide.projects.*;

/**
 * Esecuzione di una delle funzioni di Programmer Power 
 * <ACRONYM TITLE="Integrated Development Environment">IDE</ACRONYM> per il
 * progetto selezionato.
 *
 * <P>Lo script Ant che implementa le funzioni pu&ograve; scrivere nel
 * direttorio di build del progetto: dopo l&rsquo;esecuzione dello script (e
 * indipendentemente dal suo esito), viene automaticamente eseguito un refresh
 * del direttorio di build.</P>
 *  
 * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
 * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFolder
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti"
 *        TARGET="_top">Distribuzione dei progetti</A>
 * @since 1.0.0
 */
public final class BuildAction extends AbstractHandler implements 
        IExecutableExtension {    
    private static final String PARAM_TARGET = "target";
    
    /**
     * Nome del target Ant che genera i file di distribuzione. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getDistributionFolder
     * @see <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti#idBuild"
     *      TARGET="_top">Comando <CODE>Build</CODE></A>
     */    
    public static final String TARGET_BUILD = "build";
    
    /**
     * Nome del target Ant che installa i file di distribuzione. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getDistributionFolder
     * @see <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti#idInstall"
     *      TARGET="_top">Comando <CODE>Install</CODE></A> 
     */    
    public static final String TARGET_INSTALL = "install";
    
    /**
     * Nome del target Ant che esegue il deploy. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     * 
     * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti#idDeploy"
     *        TARGET="_top">Comando <CODE>Deploy</CODE></A>
     * @since 1.1.0   
     */    
    public static final String TARGET_DEPLOY = "deploy";
        
    /**
     * Nome del target Ant che cancella i file di distribuzione. Il valore della
     * costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getDistributionFolder
     * @see <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti#idClean"
     *      TARGET="_top">Comando <CODE>Clean</CODE></A> 
     */    
    public static final String TARGET_CLEAN = "clean";
    
    private String myTarget = null;
    
    /**
     * Costruttore.
     */
    public BuildAction() {        
    }
    
    /**
     * Esegue l&rsquo;azione.
     * 
     * @param  event Evento.
     * @return       {@code null}.
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        Object obj;
        IProject project;
        IAdaptable adapt;
        ISelection selection;
        IStructuredSelection sel;
        List<IProject> projectList;
        IRunnableWithProgress op;
        IDEPlugin plugin = IDEPlugin.getInstance();
               
        @SuppressWarnings("rawtypes")
        Iterator it;
        
        if (myTarget == null) {
            return null;
        }
        
        selection = HandlerUtil.getCurrentSelection(event);
        if (!(selection instanceof IStructuredSelection)) {
            return null;
        }
        
        sel = (IStructuredSelection) selection;
        it = sel.iterator();
        projectList = new ArrayList<IProject>();
        while (it.hasNext()) {
            obj = it.next();
            if (obj instanceof IProject) {
                project = (IProject) obj;
            } else if (obj instanceof IAdaptable) {
                adapt = (IAdaptable) obj;
                project = (IProject) adapt.getAdapter(IProject.class);
            } else {
                project = null;
            }
            if (project != null) {
                projectList.add(project);
            }
        }

        if (projectList.isEmpty()) {
            return null;
        }

        try {
            op = new BuildAction.Executor(event, myTarget, projectList);
            plugin.runOperation(op);
        } catch (InvocationTargetException ex) {
            StatusTools.displayStatus(ex.getTargetException());
        }
        
        return null;
    }
    
    /**
     * Inizializza le informazioni di configurazione.
     * 
     * @param config       Elemento della configurazione.
     * @param propertyName Nome dell&rsquo;attributo di configurazione.
     * @param data         Dati di configurazione.
     */ 
    @SuppressWarnings("rawtypes")
    public void setInitializationData(IConfigurationElement config,
            String propertyName, Object data) throws CoreException {               
        Hashtable dataMap;
        
        if (!(data instanceof Hashtable)) {
            return;
        }
        
        dataMap = (Hashtable) data;
        myTarget = (String) dataMap.get(BuildAction.PARAM_TARGET);
    }  
    
    /**
     * Operazione.
     */
    private static final class Executor implements IRunnableWithProgress {
        private final String myText;
        private final String myTarget;
        private final List<IProject> myProjectList;
                
        /**
         * Costruttore.
         * 
         * @param event       Evento.
         * @param target      Target Ant.
         * @param projectList Collezione dei progetti.
         */
        Executor(ExecutionEvent event, String target,
                List<IProject> projectList) {
            String s;
            
            try {
                s = event.getCommand().getName();
            } catch (NotDefinedException ex) {
                s = target;
            }
            
            myText = s;
            myTarget = target;
            myProjectList = projectList;
        }
        
        /**
         * Esegue l&rsquo;operazione.
         * 
         * @param monitor Gestore dell&rsquo;avanzamento.
         */
        public void run(IProgressMonitor monitor) throws
                InvocationTargetException, InterruptedException {
            String msg;
            SubMonitor progress;
            
            msg = String.format("%1$s...", myText);
            progress = SubMonitor.convert(monitor, msg, myProjectList.size());
            
            try {
                for (IProject project : myProjectList) {
                    try {
                        build(project, progress.newChild(1));
                    } catch (CoreException ex) {
                        throw new InvocationTargetException(ex);
                    }
                    
                    progress.worked(1);
                }
            } finally {
                if (monitor != null) {
                    monitor.done();
                }                
            }
        }
    
        /**
         * Esegue il target Ant.
         * 
         * @param project Progetto.
         * @param monitor Gestore dell&rsquo;avanzamento.
         */
        private void build(IProject project, IProgressMonitor monitor) throws
                CoreException {
            String msg;
            IPath path;
            IFile buildFile;        
            ProjectDirectory projectDir;
            BuildOutputDirectory buildDir;
            BuildRunner runner;
            SubMonitor progress;
            
            msg = String.format("%1$s for project %2$s...", myText,
                    project.getName());
            progress = SubMonitor.convert(monitor, "", 2);
            
            try {
                projectDir = new ProjectDirectory(project);
                
                buildFile = projectDir.getBuildFile();
                if (!buildFile.exists()) {
                    path = buildFile.getLocation();
                    msg = String.format("File %1$s not found.", path);   
                    throw new CoreException(StatusTools.toStatus(IStatus.ERROR,
                            msg));                
                }
                
                runner = new BuildRunner();
                path = buildFile.getLocation();
                runner.setScriptFile(path.toFile());                             
                runner.getTargets().add(myTarget);
                
                buildDir = new BuildOutputDirectory(
                        projectDir.getBuildFolder());
                
                try {
                    runner.run(progress.newChild(1));
                } finally {
                    progress.worked(1);
                    buildDir.refresh();
                    progress.worked(1);
                }            
            } finally {
                if (monitor != null) {
                    monitor.done();
                }
            }
        }
    }    
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.lang.reflect.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.operation.*;
import org.eclipse.jface.resource.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.wizard.*;
import org.eclipse.ui.*;
import org.eclipse.ui.wizards.newresource.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Wizard per la creazione di un nuovo progetto.
 *
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Wizard-per-la-creazione-di-un-nuovo-progetto"
 *        TARGET="_top">Wizard per la creazione di un nuovo progetto</A>
 * @since 1.0.0
 */
public final class NewProjectWizard extends Wizard implements INewWizard,
    IExecutableExtension {
    // Alcuni degli attributi dell'estensione
    // it.scoppelletti.sdk.ide.ui.NewProjectWizard coincidono con quelli
    // delle estensioni org.eclipse.jdt.ui.wizards.JavaProjectWizard di JDT e
    // org.eclipse.jst.servlet.ui.project.facet.WebProjectWizard,
    // org.eclipse.jst.ejb.ui.project.facet.EjbProjectWizard, 
    // org.eclipse.jpt.ui.wizard.newJpaProject, 
    // org.eclipse.jst.j2ee.ui.project.facet.UtilityProjectWizard di WST.
    // L'estensione ha il riferimento all'icona programmerpower16x16.gif.
    
    private static final String IMAGE_RES = "programmerpower32x32.png"; 
    
    private IConfigurationElement myConfig;
    private NewProjectPage myProjectPage;
    
    /**
     * Costruttore.
     */
    public NewProjectWizard() {
        ImageDescriptor image;
        
        image = ImageDescriptor.createFromFile(getClass(),
                NewProjectWizard.IMAGE_RES);
        setDefaultPageImageDescriptor(image);
        setDialogSettings(IDEPlugin.getInstance().getDialogSettings());
        setWindowTitle("New Programmer Power Project"); 
        setNeedsProgressMonitor(true);        
    }
    
    /**
     * Inizializza il wizard.
     * 
     * @param workbench Workbench.
     * @param selection Selezione.
     */
    public void init(IWorkbench workbench, IStructuredSelection selection) { 
    }  
    
    /**
     * Inizializza le informazioni di configurazione.
     * 
     * @param config       Elemento della configurazione.
     * @param propertyName Nome dell&rsquo;attributo di configurazione.
     * @param data         Dati di configurazione.
     */
    public void setInitializationData(IConfigurationElement config,
            String propertyName, Object data) throws CoreException {
        myConfig = config;
    }  
    
    /**
     * Inizializza le pagine del wizard.
     */
    @Override
    public void addPages() {
        myProjectPage = new NewProjectPage();
        addPage(myProjectPage);
    }
    
    /**
     * Conferma del dialogo.
     * 
     * @return Esito dell&rsquo;operazione.
     */    
    @Override
    public boolean performFinish() {
        IRunnableWithProgress operation;
        IDialogSettings settings = getDialogSettings();
        
        try {
            if (settings != null) {
                myProjectPage.saveSettings(settings);
            }
            
            BasicNewProjectResourceWizard.updatePerspective(myConfig);
            
            operation = NewProjectOperation.newOperation(myProjectPage);
            getContainer().run(false, false, operation);
            
            return true;
        } catch (InvocationTargetException ex) {
            StatusTools.displayStatus(ex.getTargetException());
        } catch (InterruptedException ex) { 
            StatusTools.logStatus(ex);
        }
        
        return false;
    }
}

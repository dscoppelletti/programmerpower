/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.io.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.ui.console.*;

/**
 * Flusso di scrittura sulla console. 
 */
final class IDEConsoleStream extends OutputStream {
    private static String CR = "\r";
    private static String NL = "\n";
    private IDEConsoleImpl myConsole;
    private IDEConsolePartitioner myPartitioner;
    private boolean myClosed = false;
    private boolean myCRPending = false;
    private Color myColor = null;
    private Object mySyncRoot = new Object();
    
    /**
     * Costruttore.
     * 
     * @param console     Console.
     * @param partitioner Partizionatore.
     */
    IDEConsoleStream(IDEConsoleImpl console,
            IDEConsolePartitioner partitioner) {
        myConsole = console;
        myPartitioner = partitioner;
    }
   
    /**
     * Chiude il flusso.
     */
    @Override
    public void close() throws IOException {
        synchronized (mySyncRoot) {
            if (myClosed) {
                return;
            }
        
            if (myCRPending) {
                myCRPending = false;
                onWrite(IDEConsoleStream.CR);
            }
            
            myClosed = true;
            myPartitioner = null;
        }
    }
    
    /**
     * Restituisce il colore per la scrittura sul flusso.
     * 
     * @return Colore. Pu&ograve; essere {@code null}.
     */
    Color getColor() {
        return myColor;
    }
    
    /**
     * Imposta il colore per la scrittura sul flusso.
     * 
     * @param color Colore. Pu&ograve; essere {@code null}.
     */
    void setColor(Color color) {
        Color colorOld = myColor;
        
        if (myColor == null || !myColor.equals(color)) {
            myColor = color;
            myConsole.firePropertyChange(this, IConsoleConstants.P_STREAM_COLOR,
                    colorOld, myColor);
        }
    }
    
    /**
     * Scrive una stringa seguita da un&rsquo;interruzione di linea.
     * 
     * @param s Stringa.
     */
    public void writeln(String s) throws IOException {
        if (s == null) {
            throw new NullPointerException("Argument s is null.");
        }
        
        write(s.concat(IDEConsoleStream.NL));
    }
    
    /**
     * Scrive un byte.
     * 
     * @param b Valore.
     */
    @Override
    public void write(int b) throws IOException {
        write(new byte[] { (byte) b }, 0, 1);
    }
    
    /**
     * Scrive un vettore di byte.
     * 
     * @param b   Vettore.
     * @param off Indice del primo byte da scrivere sul vettore {@code b}.
     * @param len Numero di byte da scrivere.
     */
    @Override
    public void write(byte b[], int off, int len) throws IOException {
        write(new String(b, off, len));
    }
    
    /**
     * Scarica sul flusso sottostante gli eventuali caratteri scritti ma non
     * ancora scaricati.
     * 
     * <P>Questa versione del metodo {@code flush} non esegue nulla.</P>
     */    
    @Override
    public void flush() throws IOException {        
    }
           
    /**
     * Scrive una stringa.
     * 
     * @param s Stringa.
     */
    private void write(String s) throws IOException {
        synchronized (mySyncRoot) {
            if (myCRPending) {
                s = IDEConsoleStream.CR.concat(s);
                myCRPending = false;            
            }
            
            if (s.endsWith(IDEConsoleStream.CR)) {
                // La stringa termina con un carattere CR:
                // Rimando la scrittura del carattere alla prossima scrittura.
                myCRPending = true;
                s = s.substring(0, s.length() - 1);                
            }
            
            onWrite(s);
        }
    }
    
    /**
     * Scrive una stringa.
     * 
     * @param s Stringa.
     */
    private void onWrite(String s) throws IOException {
        ConsolePlugin consolePlugin = ConsolePlugin.getDefault();
        IConsoleManager consoleMgr = consolePlugin.getConsoleManager();
        
        myPartitioner.append(this, s);
        consoleMgr.warnOfContentChange(myConsole);     
    }
}

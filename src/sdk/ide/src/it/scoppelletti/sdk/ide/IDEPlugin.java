/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide;

import java.lang.reflect.*;
import org.eclipse.jface.dialogs.*;
import org.eclipse.jface.operation.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.plugin.*;
import org.osgi.framework.*;
import it.scoppelletti.sdk.ide.ui.*;

/**
 * Programmer Power
 * <ACRONYM TITLE="Integrated Development Environment">IDE</ACRONYM>.
 *
 * @since 1.0.0
 */
public final class IDEPlugin extends AbstractUIPlugin {
    
    /**
     * Identificatore del plug-in. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String ID = "it.scoppelletti.sdk.ide";
         
    private static IDEPlugin myInstance;
    
    /**
     * Costruttore.
     */
    public IDEPlugin() {
    }

    /**
     * Restituisce l&rsquo;istanza del plug-in.
     * 
     * @return Oggetto.
     */
    public static IDEPlugin getInstance() {
        return myInstance;
    }
    
    /**
     * Avvia il plug-in.
     */
    @Override
    public void start(BundleContext context) throws Exception {
        ProjectService projectService = ProjectService.getInstance();
        
        super.start(context);
        
        myInstance = this;        
        projectService.start();        
        IDEConsole.startService();
    }

    /**
     * Interrompe il plug-in.
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        ProjectService projectService = ProjectService.getInstance();
                
        projectService.shutdown();
        
        super.stop(context);
        myInstance = null;
    }    

    /**
     * Restituisce la <ACRONYM TITLE="User Interface">UI</ACRONYM> standard.
     * 
     * @return Oggetto.
     */
    public Display getCurrentDisplay() {
        Display display;
        
        display = Display.getCurrent();
        if (display == null) {
            // Non e' associata una UI al thread corrente:
            // Restituisco la UI di default.
            display = Display.getDefault();
        }
        
        return display;
    }

    /**
     * Esegue un&rsquo;operazione con notifica dell&rsquo;avanzamento.
     * 
     * @param op Operazione.
     * @since    1.1.0
     */
    public void runOperation(IRunnableWithProgress op) throws
            InvocationTargetException {
        ProgressMonitorDialog dlg;
        
        if (op == null) {
            throw new NullPointerException("op");                        
        }
        
        dlg = new ProgressMonitorDialog(getCurrentDisplay().getActiveShell());
        try {
            dlg.run(false, false, op);
        } catch (InterruptedException ex) {
            StatusTools.logStatus(ex);
        }
    }   
}

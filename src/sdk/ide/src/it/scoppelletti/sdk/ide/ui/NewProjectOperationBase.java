/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.io.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import org.eclipse.ui.actions.*;
import org.eclipse.wst.common.project.facet.core.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Operazione di base per la creazione di un nuovo progetto.
 */
abstract class NewProjectOperationBase extends WorkspaceModifyOperation {                               
    private final INewProjectParameters myParams;
    
    /**
     * Costruttore.
     * 
     * @param params Parametri.
     */
    NewProjectOperationBase(INewProjectParameters params) {
        if (params == null) {
            throw new NullPointerException("Argument params is null.");
        }
        
        myParams = params;
    }
    
    /**
     * Restituisce i parametri impostati dal wizard.
     * 
     * @return Oggetto.
     */
    final INewProjectParameters getParams() {
        return myParams;
    }
       
    /**
     * Crea il progetto.
     * 
     * @param  monitor Gestore dell&rsquo;avanzamento.
     * @return         Progetto. 
     */
    final IProject createProject(IProgressMonitor monitor) throws
            CoreException { 
        IPath location; 
        IProject project;
        IProjectDescription desc;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, "Creating the project...", 2);
        try {
            project = myParams.getProjectHandle();
            if (!project.exists()) {
                location = myParams.getLocationPath();
                if (Platform.getLocation().equals(location)) {
                    project.create(progress.newChild(1));
                } else {
                    desc = project.getWorkspace().newProjectDescription(
                            project.getName());
                    desc.setLocation(location);
                    project.create(desc, progress.newChild(1));
                }
                progress.worked(1);
                
                project.open(progress.newChild(1));
                progress.worked(1);
            }               
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }
                                           
        return project;
    }
    
    /**
     * Crea un progetto Java.
     *  
     * @param  project Progetto.
     * @param  monitor Gestore dell&rsquo;avanzamento.
     * @return         Interfaccia per la gestione del progetto Java.
     */
    final IJavaProject createJavaProject(IProject project,
            IProgressMonitor monitor) throws CoreException {
        IJavaProject javaProject;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, "Creating the java project...",
                2);
        
        try {
            addProjectNature(project, JavaCore.NATURE_ID, progress.newChild(1));
            progress.worked(1);
            
            javaProject = JavaCore.create(project);
            progress.worked(1);
        } finally {
            if (monitor != null) {
                monitor.done();
            }            
        }
        
        return javaProject;
    }
    
    /**
     * Imposta su un progetto la gestione dei facet.
     * 
     * @param  project Progetto.
     * @param  monitor Gestore dell&rsquo;avanzamento.
     * @return         Interfaccia per la gestione dei facet del progetto.
     */
    final IFacetedProject createFacetedProject(IProject project,
            IProgressMonitor monitor) throws CoreException {
        // Impostare sul progetto la natura
        // org.eclipse.wst.common.project.facet.core.nature non e' sufficiente
        // perche' deve essere gestita anche un'apposita struttura di file di
        // configurazione del progetto. 
        return ProjectFacetsManager.create(project, true, monitor);       
    }
                
    /**
     * Imposta una natura su un progetto (se non &egrave; gi&agrave; impostata).
     * 
     * @param project  Progetto.
     * @param natureId Identificatore della natura.
     * @param monitor  Gestore dell&rsquo;avanzamento.
     */
    final void addProjectNature(IProject project, String natureId,
            IProgressMonitor monitor) throws CoreException {
        String[] prevNatures, newNatures;       
        IProjectDescription desc;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, 2);
        
        try {
            if (project.hasNature(natureId)) {
                return;
            }
            
            desc = project.getDescription();
            prevNatures = desc.getNatureIds();
            newNatures = new String[prevNatures.length + 1];
            System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
            newNatures[prevNatures.length] = natureId;
            progress.worked(1);
            
            desc.setNatureIds(newNatures);
            project.setDescription(desc, progress.newChild(1));
            progress.worked(1);
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }        
    }
        
    /**
     * Compone il contenuto di uno dei file basati su un template con variabili
     * da sostituire.
     * 
     * @param  templateName Nome della risorsa del template.
     * @param  monitor      Gestore dell&rsquo;avanzamento.
     * @return              Testo.
     */
    final String buildFilteredContents(String templateName,
            IProgressMonitor monitor) throws IOException {
        String line, msg;
        InputStream in = null;
        BufferedReader reader = null;
        StringWriter out = null;
        PrintWriter writer = null;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, 1);
        
        try {
            in = getClass().getResourceAsStream(templateName);
            if (in == null) {
                msg = String.format("Resource \"%1$s\" not found.",
                        templateName);
                throw new FileNotFoundException(msg);
            }
            reader = new BufferedReader(new InputStreamReader(in));
            in = null;
            
            out = new StringWriter();
            writer = new PrintWriter(out);
            
            line = reader.readLine();
            while (line != null) { 
                writer.println(filterText(line));
                line = reader.readLine();
            }
            progress.worked(1);
        } finally {
        	in = IOTools.close(in);
        	reader = IOTools.close(reader);
        	writer = IOTools.close(writer);
            if (monitor != null) {
                monitor.done();
            }            
        }
        
        return out.toString();
    }
 
    /**
     * Sostituisce le variabili in un testo.
     * 
     * @param  text Testo da elaborare.
     * @return      Testo elaborato.
     */
    private String filterText(String text) {
        String buf, value;
 
        buf = text.replace("$projectName$", myParams.getProjectName());
        buf = buf.replace("$version$", myParams.getVersion());
        buf = buf.replace("$baseName$", myParams.getBaseName());       
        buf = buf.replace("$title$", myParams.getSpecificationTitle());
        buf = buf.replace("$projectTarget$", myParams.getTarget());
        value = myParams.getBaseName();
        if (value != null) {
            value = value.concat(myParams.getArchiveExtension());
        }
        buf = buf.replace("$jarName$", value);
        
        return buf;
    }
    
    /**
     * Compone il template del file di manifesto.
     * 
     * @param  monitor Gestore dell&rsquo;avanzamento.
     * @return         Testo.
     */
    final String buildManifest(IProgressMonitor monitor) throws IOException {
        String value;
        StringWriter out = null;
        PrintWriter writer = null;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, 1);
        
        try {
            out = new StringWriter();
            writer = new PrintWriter(out);
            
            value = myParams.getSpecificationTitle();
            if (value != null && !value.isEmpty()) {
                writer.print("Specification-Title: ");
                writer.println(value);                
            } else {
                writer.println("Specification-Title: ${module.name}");                
            }
            
            writer.println("Specification-Version: ${module.version}");
            
            value = myParams.getVendor();
            if (value != null && !value.isEmpty()) {
                writer.print("Specification-Vendor: ");
                writer.println(value);                   
            }
            
            writer.println("Implementation-Title: ${module.name}");            
            writer.println("Implementation-Version: ${module.version}");            
            
            value = myParams.getVendor();
            if (value != null && !value.isEmpty()) {
                writer.print("Implementation-Vendor: ");
                writer.println(value);                   
            }
                        
            writer.println("Sealed: true");
            
            value = myParams.getMainClass();
            if (value != null && !value.isEmpty()) {
                writer.print("Main-Class: ");
                writer.println(value);                
            }
            
            progress.worked(1);
        } finally {
        	writer = IOTools.close(writer);         
            if (monitor != null) {
                monitor.done();
            }                        
        }
        
        return out.toString();        
    }
    
    /**
     * Crea un file.
     * 
     * @param file     File.
     * @param contents Contenuto.
     * @param monitor  Gestore dell&rsquo;avanzamento.
     */
    final void createFile(IFile file, String contents,
            IProgressMonitor monitor) throws CoreException {
        InputStream in;
        
        in = new ByteArrayInputStream(contents.getBytes());
        if (file.exists()) {
            file.setContents(in, false, true, monitor);
        } else {
            file.create(in, false, monitor);
        }
                       
        // Secondo la documentazione dei metodi IFile.setContents e
        // IFile.create, il flusso viene chiuso sia in caso di successo che in
        // caso di errore, ma io ho rilevato degli esempi di utilizzo che
        // provvedono comunque a chiudere il flusso.
        IOTools.close(in);
    }
    
    /**
     * Crea un direttorio (se non esiste gi&agrave;).
     * 
     * <P>Se il direttorio &egrave; un sotto-direttorio di un direttorio non
     * esistente, crea anche il direttorio parent ricorsivamente.</P>
     * 
     * @param folder  Direttorio.
     * @param monitor Gestore dell&rsquo;avanzamento.
     */
    final void createFolder(IFolder folder, IProgressMonitor monitor) throws
            CoreException {
        IContainer parent;
        
        if (folder.exists()) {
            return;
        }
        
        parent = folder.getParent();
        if (parent instanceof IFolder) {
            createFolder((IFolder) parent, monitor);
        }
        
        folder.create(true, true, monitor);
    }        
}

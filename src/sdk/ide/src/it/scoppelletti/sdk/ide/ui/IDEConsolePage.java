/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.jface.action.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.console.*;
import org.eclipse.ui.console.actions.*;

/**
 * Pagina della console.
 */
final class IDEConsolePage extends TextConsolePage {
    private CloseConsoleAction myCloseAction;
    
    /**
     * Costruttore.
     * 
     * @param console Console.
     * @param view    Vista.
     */
    IDEConsolePage(TextConsole console, IConsoleView view) {
        super(console, view);
    }

    
    /**
     * Crea il visualizzatore.
     * 
     * @param  parent Controllo parent.
     * @return        Visualizzatore.
     */
    @Override
    protected TextConsoleViewer createViewer(Composite parent) {
        return new IDEConsoleViewer(parent, (TextConsole) getConsole());
    }
    
    /**
     * Crea i comandi.
     */
    @Override
    protected void createActions() {
        super.createActions();
        myCloseAction = new CloseConsoleAction(getConsole());
    }
    
    /**
     * Configura la barra degli strumenti.
     */
    @Override
    protected void configureToolBar(IToolBarManager mgr) {
        super.configureToolBar(mgr);
        mgr.appendToGroup(IConsoleConstants.LAUNCH_GROUP, myCloseAction);
    }       
}

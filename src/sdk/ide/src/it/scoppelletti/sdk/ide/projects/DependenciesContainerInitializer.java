/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.projects;

import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Inizializzatore del class-path corrispondente alle dipendenze di un progetto.
 * 
 * <P>Il class-path &egrave; costruito dallo script Ant che implementa le
 * funzioni dell&rsquo;<ACRONYM
 * TITLE="Integrated Development Environment">IDE</ACRONYM>.</P>
 * 
 * @it.scoppelletti.tag.schema {@code http://www.scoppelletti.it/res/sdk/ide/projects/classpath2.xsd} 
 * @see   #TARGET
 * @see   it.scoppelletti.sdk.ide.projects.DependenciesContainer
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/deps.html#idCompile"
 *        TARGET="_top">Dipendenze per la compilazione di un modulo</A> 
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Programmer-Power-Dependencies#idCompile"
 *        TARGET="_top">Class-path per la compilazione di un progetto</A> 
 * @since 1.0.0 
 */
public final class DependenciesContainerInitializer extends
        ClasspathContainerInitializer {

    /**
     * Nome del target Ant che esegue l&rsquo;export del class-path. Il valore
     * della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     */
    public static final String TARGET = "export-classpath-compile";

    /**
     * Nome della propriet&agrave; Ant che specifica il nome assoluto del file
     * sul quale il target {@code export-classpath-compile} deve eseguire
     * l&rsquo;export del class-path. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile 
     */
    public static final String PROP_FILEOUT = "it.scoppelletti.sdk.file.out";
    
    /**
     * Costruttore.
     */
    public DependenciesContainerInitializer() {       
    }
        
    /**
     * Restituisce la descrizione del class-path.
     * 
     * @param  containerPath Identificatore del class-path. 
     * @param  project       Progetto.
     * @return               Valore.
     */
    @Override
    public String getDescription(IPath containerPath, IJavaProject project) {
        return DependenciesContainer.NAME;
    }    
    
    /**
     * Restituisce l&rsquo;oggetto che identifica il class-path ai fini di
     * confronto.
     * 
     * @param  containerPath Identificatore del class-path. 
     * @param  project       Progetto.
     * @return               Oggetto. 
     */
    @Override
    public Object getComparisonID(IPath containerPath, IJavaProject project) {
        String path;
        
        if (containerPath == null || project == null) {
            return null;
        }
        
        path = containerPath.segment(0);
        path.concat("/");
        path.concat(project.getPath().segment(0));        
        
        return path;
    }   
    
    /**
     * Inizializza il class-path.
     * 
     * @param containerPath Identificatore del class-path. 
     * @param project       Progetto.
     */
    @Override
    public void initialize(IPath containerPath, IJavaProject project) throws
        CoreException {
        try {
            DependenciesContainer.setClasspath(project, null);
        } catch (CoreException ex) {
            StatusTools.logStatus(ex);
        }            
    }      
}

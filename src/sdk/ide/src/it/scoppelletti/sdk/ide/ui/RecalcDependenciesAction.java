/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.lang.reflect.*;
import java.util.*;
import org.eclipse.core.commands.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import org.eclipse.jface.operation.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.ui.handlers.*;
import it.scoppelletti.sdk.ide.*;
import it.scoppelletti.sdk.ide.projects.*;

/**
 * Ricalcolo del class-path corrispondente alle dipendenze del progetto
 * selezionato.
 * 
 * @see   it.scoppelletti.sdk.ide.projects.DependenciesContainer
 * @see   it.scoppelletti.sdk.ide.projects.DependenciesContainerInitializer
 * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getModuleMetadataFile
 * @see   it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Programmer-Power-Dependencies#idCompile"
 *        TARGET="_top">Class-path per la compilazione di un progetto</A>
 * @since 1.0.0
 */ 
public final class RecalcDependenciesAction extends AbstractHandler {
    
    /**
     * Costruttore.
     */
    public RecalcDependenciesAction() {        
    }
    
    /**
     * Esegue l&rsquo;azione.
     * 
     * @param  event Evento.
     * @return       {@code null}.
     */    
    public Object execute(ExecutionEvent event) throws ExecutionException {
        Object obj;
        IProject project;
        IAdaptable adapt;        
        ISelection selection;
        IStructuredSelection sel;        
        List<IProject> projectList;        
        IRunnableWithProgress op;
        IDEPlugin plugin = IDEPlugin.getInstance();
                       
        @SuppressWarnings("rawtypes")
        Iterator it;
        
        selection = HandlerUtil.getCurrentSelection(event);
        if (!(selection instanceof IStructuredSelection)) {
            return null;
        }
        
        sel = (IStructuredSelection) selection;
        it = sel.iterator();
        projectList = new ArrayList<IProject>();
        while (it.hasNext()) {
            obj = it.next();
            if (obj instanceof IProject) {
                project = (IProject) obj;
            } else if (obj instanceof IAdaptable) {
                adapt = (IAdaptable) obj;
                project = (IProject) adapt.getAdapter(IProject.class);
            } else {
                project = null;
            }
            if (project != null) {
                projectList.add(project);
            }
        }

        if (projectList.isEmpty()) {
            return null;
        }
        
        try {
            op = new RecalcDependenciesAction.Executor(projectList);
            plugin.runOperation(op);
        } catch (InvocationTargetException ex) {
            StatusTools.displayStatus(ex.getTargetException());
        }
        
        return null;
    }
        
    /**
     * Operazione.
     */
    private static final class Executor implements IRunnableWithProgress {
        private final List<IProject> myProjectList;
        
        /**
         * Costruttore.
         * 
         * @param projectList Collezione dei progetti.
         */
        Executor(List<IProject> projectList) {
            myProjectList = projectList;
        }
        
        /**
         * Esegue l&rsquo;operazione.
         * 
         * @param monitor Gestore dell&rsquo;avanzamento.
         */
        public void run(IProgressMonitor monitor) throws
                InvocationTargetException, InterruptedException {
            String msg;
            SubMonitor progress;
            
            msg = String.format("Recalculating class-path \"%1$s\"...",
                    DependenciesContainer.NAME);            
            progress = SubMonitor.convert(monitor, msg, myProjectList.size());
            
            try {
                for (IProject project : myProjectList) {
                    try {
                        setClasspath(project, progress.newChild(1));
                    } catch (CoreException ex) {
                        throw new InvocationTargetException(ex);
                    }
                    
                    progress.worked(1);
                }
            } finally {
                if (monitor != null) {
                    monitor.done();
                }                
            }
        }
        
        /**
         * Imposta per un progetto il class-path corrispondente alle dipendenze
         * del progetto stesso.
         * 
         * @param project Progetto.
         * @param monitor Gestore dell&rsquo;avanzamento.
         */
        private void setClasspath(IProject project, IProgressMonitor monitor)
                throws CoreException {        
            IJavaProject javaProject;
            
            javaProject = JavaCore.create(project);            
            DependenciesContainer.setClasspath(javaProject, monitor);
        }        
    }    
}

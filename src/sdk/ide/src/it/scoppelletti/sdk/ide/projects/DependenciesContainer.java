/*
 * Copyright (C) 2008-2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.projects;

import java.util.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;

/**
 * Class-path corrispondente alle dipendenze di un progetto.
 *
 * @see   <A HREF="${it.scoppelletti.token.referenceUrl}/runtime/deps.html"
 *        TARGET="_top">Risoluzione delle dipendenze</A>
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Programmer-Power-Dependencies"
 *        TARGET="_top"><CODE>Programmer Power Dependencies</CODE></A>
 * @since 1.0.0
 */
public final class DependenciesContainer implements IClasspathContainer {

    /**
     * Identificatore del class-path.
     */
    public static final IPath PATH = new Path(
            "it.scoppelletti.sdk.ide.DependenciesContainer");
    
    /**
     * Nome del class-path. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final String NAME = "Programmer Power Dependencies";
     
    private final IClasspathEntry[] myClasspath;
         
    /**
     * Costruttore.
     * 
     * @param classpath Elementi del class-path.
     */ 
    private DependenciesContainer(List<IClasspathEntry> classpath) {
        if (classpath == null) {
            throw new NullPointerException("Argument classpath is null.");
        }
        
        myClasspath = (IClasspathEntry[]) classpath.toArray(
                new IClasspathEntry[classpath.size()]);
    }
    
    /**
     * Restituisce l&rsquo;identificatore del class-path.
     * 
     * @return Percorso.
     * @see    #PATH
     */
    public IPath getPath() {
        return DependenciesContainer.PATH;
    }

    /**
     * Restituisce la descrizione del class-path.
     * 
     * @return Valore.
     * @see    #NAME
     */
    public String getDescription() {
        return DependenciesContainer.NAME;        
    }
    
    /**
     * Restituisce il tipo del class-path.
     * 
     * @return Valore ({@code IClasspathContainer.K_APPLICATION}).
     */
    public int getKind() {
        return IClasspathContainer.K_APPLICATION;
    }
    
    /**
     * Restituisce gli elementi del class-path.
     * 
     * @return Class-path.
     */
    public IClasspathEntry[] getClasspathEntries() {        
        return myClasspath;
    }      
    
    /**
     * Imposta per un progetto il class-path corrispondente alle dipendenze del
     * progetto stesso.
     * 
     * @param project Progetto.
     * @param monitor Gestore dell&rsquo;avanzamento.
     * @since         1.1.0
     */
    public static void setClasspath(IJavaProject project, 
            IProgressMonitor monitor) throws CoreException {
        String msg;
        DependenciesContainer.Builder builder;
        IClasspathContainer container;
        SubMonitor progress;
        
        if (project == null) {
            throw new NullPointerException("Argument project is null.");
        }
        
        msg = String.format(
                "Calculating class-path \"%1$s\" for project \"%2$s\"...",
                DependenciesContainer.NAME, project.getElementName());
        progress = SubMonitor.convert(monitor, msg, 2);
        try {
            builder = new DependenciesContainer.Builder(project);
            container = new DependenciesContainer(builder.run(
                    progress.newChild(1)));
            progress.worked(1);
            
            JavaCore.setClasspathContainer(container.getPath(),
                    new IJavaProject[] { project },
                    new IClasspathContainer[] { container },
                    progress.newChild(1));
            progress.worked(1);
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }
    }         
    
    /**
     * Composizione del class-path di compilazione corrispondente alle
     * dipendenze di un progetto.
     */
    private static final class Builder extends
            ClasspathBuilder<IClasspathEntry> {

        /**
         * Costruttore.
         * 
         * @param project Progetto.
         */
        Builder(IJavaProject project) throws CoreException {
            super(project, DependenciesContainerInitializer.TARGET,
                    DependenciesContainerInitializer.PROP_FILEOUT);
        }
        
        @Override
        String getClasspathDescription() {
            return String.format(
                    "Class-path container \"%1$s\" for project \"%2$s\"",
                    DependenciesContainer.NAME, getProject().getElementName());        
        }    
        
        @Override
        IClasspathEntry newClasspathEntry(IPath path, IPath sourcePath,
                IPath sourceRootPath, String docURL) {
            IClasspathEntry entry;
            IClasspathAttribute[] attrs;
            
            if (docURL == null || docURL.isEmpty()) {
                attrs = new IClasspathAttribute[0];
            } else {
                attrs = new IClasspathAttribute[1];
                attrs[0] = JavaCore.newClasspathAttribute(
                        IClasspathAttribute.JAVADOC_LOCATION_ATTRIBUTE_NAME,
                        docURL);
            }
            
            // Eclipse 3.3.0: La documentazione del metodo
            // JavaCore.newLibraryEntry dice che il parametro sourcePath puo'
            // anche essere un direttorio, ma sono riuscito a far funzionare
            // solo gli archivi JAR e ZIP.
            // Non ho verificato una simile limitazione anche per il class-path
            // di esecuzione.
            // Ho dovuto inserire la limitazione come una specifica del runtime. 
            entry = JavaCore.newLibraryEntry(path, sourcePath, sourceRootPath,
                    new IAccessRule[0], attrs, false);
          
            return entry;
        }
    }            
}

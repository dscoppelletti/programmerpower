/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide;

import java.io.*;

/**
 * Funzioni di utilit&agrave; <ACRONYM TITLE="input/output">I/O</ACRONYM>.
 * 
 * @since 1.2.0
 */
public final class IOTools {

    /**
     * Costruttore privato per classe statica.
     */
    private IOTools() {        
    }
    
    /**
     * Chiude un flusso.
     * 
     * <P>Il metodo {@code close} chiude un flusso ignorando l&rsquo;eventuale
     * eccezione {@code IOException} inoltrata dall&rsquo;operazione (tale
     * eccezione viene emessa come segnalazione di log).<BR>
     * Il valore di ritorno {@code null} pu&ograve; essere utilizzato per
     * azzerare la stessa variabile {@code stream} del flusso in modo tale
     * variabile funga anche da indicatore di flusso aperto
     * ({@code stream != null}) o chiuso ({@code stream == null}).</P>
     * 
     * @param  stream Flusso. Se &egrave; {@code null}, si assume che il flusso
     *                sia gi&agrave; stato chiuso (o non sia mai stato aperto).
     * @return        {@code null}.
     */
    public static <T extends Closeable> T close(T stream) {
    	if (stream != null) {
    		try {
    			stream.close();
    		} catch (IOException ex) {
    			StatusTools.logStatus(ex);
    		}
    	}
    	
    	return null;
    }      
}

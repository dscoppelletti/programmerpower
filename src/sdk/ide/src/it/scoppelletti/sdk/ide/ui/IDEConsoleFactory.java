/*
 * Copyright (C) 2008-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.ui.console.*;

/**
 * Gestore dell&rsquo;apertura della console
 * {@link it.scoppelletti.sdk.ide.ui.IDEConsole}.
 *
 * @since 1.0.0
 */
public final class IDEConsoleFactory implements IConsoleFactory {
    // L'estensione it.scoppelletti.sdk.ide.ui.IDEConsoleFactory ha il
    // riferimento all'icona programmerpower16x16.gif.
    
    /**
     * Costruttore.
     */
    public IDEConsoleFactory() {        
    }
    
    /**
     * Apertura della console.
     */
    public void openConsole() {
        IDEConsole console = IDEConsole.getInstance();
        
        console.openConsole();
    }       
}

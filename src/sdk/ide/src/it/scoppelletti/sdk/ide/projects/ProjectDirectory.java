/*
 * Copyright (C) 2008-2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.projects;

import org.eclipse.core.resources.*;

/**
 * Direttorio di progetto.
 *
 * @see   <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Direttorio-di-progetto"
 *        TARGET="_top">Direttorio di progetto</A>
 * @since 1.0.0
 */
public final class ProjectDirectory {    
    private final IProject myProject;
        
    /**
     * Costruttore.
     * 
     * @param project Progetto.
     */    
    public ProjectDirectory(IProject project) {
        if (project == null) {
            throw new NullPointerException("Argument project is null.");
        }
            
        myProject = project;        
    }

    /**
     * Restituisce il direttorio dei file sorgente. 
     * 
     * @return Direttorio (<CODE>&lt;direttorio di progetto&gt;/src</CODE>). 
     */
    private IFolder getSourceFolder() {
        IFolder folder;
        
        folder = myProject.getFolder("src");

        return folder;
    }
    
    /**
     * Restituisce il direttorio dei file di codice sorgente Java.
     * 
     * @return Direttorio
     *         (<CODE>&lt;direttorio di progetto&gt;/src/java</CODE>). 
     */
    public IFolder getSourceJavaFolder() {
        IFolder folder;
       
        folder = getSourceFolder();
        folder = folder.getFolder("java");
        
        return folder;
    }    
    
    /**
     * Restituisce il direttorio sorgente dei file di risorsa.
     * 
     * @return Direttorio
     *         (<CODE>&lt;direttorio di progetto&gt;/src/resources</CODE>). 
     */
    public IFolder getSourceResourcesFolder() {
        IFolder folder;
       
        folder = getSourceFolder();
        folder = folder.getFolder("resources");
        
        return folder;
    }      
     
    /**
     * Restituisce il direttorio sorgente dei contenuti Web.
     * 
     * @return Direttorio
     *         (<CODE>&lt;direttorio di progetto&gt;/src/WebContent</CODE>).
     * @since  1.1.0 
     */
    public IFolder getSourceWebContentFolder() {
        IFolder folder;
       
        folder = getSourceFolder();
        folder = folder.getFolder("WebContent");
        
        return folder;
    }  
    
    /**
     * Restituisce il direttorio sorgente dei metadati Web.
     *  
     * @return Direttorio
     * (<CODE>&lt;direttorio di progetto&gt;/src/WebContent/WEB-INF</CODE>).
     * @since  1.1.0
     */
    public IFolder getSourceWebMetadataFolder() {
        IFolder folder;
        
        folder = getSourceWebContentFolder();
        folder = folder.getFolder("WEB-INF");
        
        return folder;
    }
       
    /**
     * Restituisce il direttorio sorgente dei file eseguibili.
     * 
     * @return Direttorio (<CODE>&lt;direttorio di progetto&gt;/src/bin</CODE>).
     * @see    <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti#idBin"
     *         TARGET="_top">File eseguibili</A> 
     */
    public IFolder getSourceBinFolder() {
        IFolder folder;
       
        folder = getSourceFolder();
        folder = folder.getFolder("bin");
        
        return folder;
    }  

    /**
     * Restituisce il direttorio sorgente dei file di configurazione.
     * 
     * @return Direttorio (<CODE>&lt;direttorio di progetto&gt;/src/etc</CODE>). 
     * @see    <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti#idEtc"
     *         TARGET="_top">File di configurazione</A>
     */
    public IFolder getSourceEtcFolder() {
        IFolder folder;
       
        folder = getSourceFolder();
        folder = folder.getFolder("etc");
        
        return folder;
    }  
    
    /**
     * Restituisce il direttorio dei metadati.
     * 
     * @return Direttorio
     *         (<CODE>&lt;direttorio di progetto&gt;/programmerpower</CODE>).
     */
    public IFolder getMetadataFolder() {
        IFolder folder;
        
        folder = myProject.getFolder("programmerpower");
        
        return folder;
    }
    
    /**
     * Restituisce il direttorio di base del modulo.
     * 
     * @return Direttorio
     * (<CODE>&lt;direttorio di progetto&gt;/programmerpower/module</CODE>).
     */
    public IFolder getModuleFolder() {
        IFolder folder;
        
        folder = getMetadataFolder();
        folder = folder.getFolder("module");
        
        return folder;
    }
    
    /**
     * Restituisce il file dei metadati del modulo.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/programmerpower/module/module.xml</CODE>).
     */    
    public IFile getModuleMetadataFile() {
        IFile file;
        IFolder folder;
        
        folder = getModuleFolder();
        file = folder.getFile("module.xml");
        
        return file;        
    }
        
    /**
     * Restituisce il template del file di manifesto.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/programmepower/manifest.txt</CODE>).
     */
    public IFile getManifestFile() {
        IFile file;
        IFolder folder;
        
        folder = getMetadataFolder();
        file = folder.getFile("manifest.txt");
        
        return file;
    }
         
    /**
     * Restituisce lo script Ant che implementa le funzioni dell&rsquo;IDE.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/programmepower/build.xml</CODE>).
     * @see    it.scoppelletti.sdk.ide.build.BuildRunner
     * @see    <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Script-di-build"
     *         TARGET="_top">Script di build</A>
     */
    public IFile getBuildFile() {
        IFile file;
        IFolder folder;
        
        folder = getMetadataFolder();
        file = folder.getFile("build.xml");
        
        return file;
    }
    
    /**
     * Restituisce il file sorgente della configurazione locale del modulo.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/programmerpower/src/etc/local.xml</CODE>).
     */
    public IFile getEtcLocalFile() {
        IFile file;
        IFolder folder;
        
        folder = getSourceEtcFolder();
        file = folder.getFile("local.xml");
        
        return file;
    }
    
    /**
     * Restituisce il file sorgente del descrittore di deployment per 
     * un&rsquo;applicazione Web.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/src/resources/WEB-INF/web.xml</CODE>).
     * @since  1.1.0
     */    
    public IFile getWebMetadataFile() {
        IFile file;
        IFolder folder;
        
        folder = getSourceWebMetadataFolder();
        file = folder.getFile("web.xml");
        
        return file;        
    }
         
    /**
     * Restituisce il file sorgente della configurazione di deployment per 
     * un&rsquo;applicazione Web.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/src/etc/context.xml</CODE>).
     * @since  1.1.0
     */    
    public IFile getDeployFile() {
        IFile file;
        IFolder folder;
        
        folder = getSourceEtcFolder();
        file = folder.getFile("context.xml");
        
        return file;        
    }
    
    /**
     * Restituisce il file sorgente della configurazione MVC per 
     * un&rsquo;applicazione Web.
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/src/resources/struts.xml</CODE>).
     * @since  1.1.0
     */    
    public IFile getMvcConfigFile() {
        IFile file;
        IFolder folder;
        
        folder = getSourceResourcesFolder();
        file = folder.getFile("struts.xml");
        
        return file;        
    }
     
    /**
     * Restituisce il file sorgente della configurazione del contesto
     * dell&rsquo;applicazione. 
     *  
     * @return File
     * (<CODE>&lt;direttorio di progetto&gt;/src/WebContext/WEB-INF/it-scoppelletti-applicationContext.xml</CODE>).
     * @since  1.1.0
     */    
    public IFile getWebContextFile() {
        IFile file;
        IFolder folder;
        
        folder = getSourceWebMetadataFolder();
        file = folder.getFile("it-scoppelletti-applicationContext.xml");
        
        return file;        
    }
    
    /**
     * Restituisce il direttorio di build del progetto.
     * 
     * @return Direttorio (<CODE>&lt;direttorio di progetto&gt;/build</CODE>). 
     */
    public IFolder getBuildFolder() {
        IFolder folder;
        
        folder = myProject.getFolder("build");

        return folder;
    }  
        
    /**
     * Restituisce il direttorio dei file di bytecode Java.
     * 
     * @return Direttorio
     *         (<CODE>&lt;direttorio di progetto&gt;/build/classes</CODE>). 
     */
    public IFolder getBuildClassesFolder() {
        IFolder folder;
       
        folder = getBuildFolder();
        folder = folder.getFolder("classes");
        
        return folder;
    }
    
    /**
     * Restituisce il direttorio dei file di bytecode Java per le applicazioni
     * Web.
     * 
     * @return Direttorio
     * (<CODE>&lt;direttorio di progetto&gt;/build/WEB-INF/classes</CODE>). 
     */
    public IFolder getBuildWebClassesFolder() {
        IFolder folder;
       
        folder = getBuildFolder();
        folder = folder.getFolder("WEB-INF");
        folder = folder.getFolder("classes");
        
        return folder;
    }
    
    /**
     * Restituisce il direttorio dei file di distribuzione.
     * 
     * @return Direttorio
     *         (<CODE>&lt;direttorio di progetto&gt;/build/dist</CODE>). 
     * @see    <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Distribuzione-dei-progetti"
     *         TARGET="_top">Distribuzione dei progetti</A>
     */    
    public IFolder getDistributionFolder() {
        IFolder folder;
        
        folder = getBuildFolder();
        folder = folder.getFolder("dist");
        
        return folder;        
    }
}

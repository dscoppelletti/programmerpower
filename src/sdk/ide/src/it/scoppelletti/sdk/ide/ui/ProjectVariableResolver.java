/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.io.*;
import java.util.*;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.core.variables.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.*;
import it.scoppelletti.sdk.ide.*;
import it.scoppelletti.sdk.ide.build.*;
import it.scoppelletti.sdk.ide.projects.*;

/**
 * Risoluzione delle variabili dinamiche dipendenti dal progetto.
 * 
 * @since 1.0.0
 */
public final class ProjectVariableResolver implements IDynamicVariableResolver {

    /**
     * Nome della variabile da sostituire con la definizione delle
     * propriet&agrave; per l&rsquo;esecuzione del progetto selezionato. Il
     * valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * @see <A HREF="${it.scoppelletti.token.wikiUrl}/Programmer-Power-IDE/Programmer-Power-runtime-properties"
     *      TARGET="_top">Variabile <CODE>Programmer Power run-time
     *      properties</CODE></A>
     */
    public static final String VAR_RUNTIMEPROPS = 
            "it.scoppelletti.sdk.props.runtime";
    
    /**
     * Nome del target Ant che esegue l&rsquo;export delle propriet&agrave; per
     * l&rsquo;esecuzione del progetto. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile
     */
    public static final String TARGET_RUNTIMEPROPS = "export-props-runtime";

    /**
     * Nome della propriet&agrave; Ant che specifica il nome assoluto del file
     * sul quale il target {@code export-props-runtime} deve eseguire
     * l&rsquo;export delle propriet&agrave;. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     * 
     * @see it.scoppelletti.sdk.ide.projects.ProjectDirectory#getBuildFile 
     */
    public static final String PROP_FILEOUT = "it.scoppelletti.sdk.file.out";
    
    /**
     * Costruttore.
     */
    public ProjectVariableResolver() {        
    }
    
    /**
     * Risolve una variabile dinamica.
     * 
     * @param  variable Variabile.
     * @param  argument Argomento. Pu&ograve; essere {@code null}.
     * @return          Valore. Pu&ograve; essere {@code null}.
     */
    public String resolveValue(IDynamicVariable variable, String argument)
            throws CoreException {
        String msg;
        File propFile;
        IResource res;
        IProject project;
        
        res = getSelectedResource();
        if (res == null || !res.exists()) {
            throw new CoreException(StatusTools.toStatus(IStatus.ERROR,
                    "No project selected."));                
        }
        
        project = res.getProject();
        if (project == null || !project.exists()) {
            throw new CoreException(StatusTools.toStatus(IStatus.ERROR,
                    "No project selected."));                            
        }
        
        // Verifico che si tratti dell'unica variabile supportata
        if (!ProjectVariableResolver.VAR_RUNTIMEPROPS.equals(
                variable.getName())) {
            msg = String.format("Unsupported variabile \"%1$s\".", 
                    variable.getName());
            throw new CoreException(StatusTools.toStatus(IStatus.ERROR, msg));
        }
                
        try {
            propFile = File.createTempFile("runtime", ".properties");
        } catch (IOException ex) {
            throw new CoreException(StatusTools.toStatus(ex));
        }            

        export(project, propFile);
        
        return buildValue(project, propFile);       
    }
    
    /**
     * Restituisce la risorsa selezionata.
     * 
     * @return Risorsa.
     */
    private IResource getSelectedResource() {
        Display display;
        ProjectVariableResolver.SelectedResourceGetter getter;
        IDEPlugin plugin = IDEPlugin.getInstance();
                
        display = plugin.getCurrentDisplay();
        if (display.getThread().equals(Thread.currentThread())) {
            // Il thread corrente e' lo stesso della UI:
            // Posso determinare direttamente la risorsa selezionata.
            return getSelectedResourceImpl();
        }
        
        // Devo usare il thread della UI per determinare la risorsa selezionata
        getter = new ProjectVariableResolver.SelectedResourceGetter(this);
        display.syncExec(getter);
        
        return getter.getResource();
    }
    
    /**
     * Restituisce la risorsa selezionata.
     * 
     * @return Risorsa.
     */    
    private IResource getSelectedResourceImpl() {
        Object obj;
        IResource res;
        IAdaptable adapt;
        IWorkbench workbench;
        IWorkbenchWindow window;
        IWorkbenchPage page;
        IWorkbenchPart part;
        IEditorPart editor;        
        IWorkbenchPartSite site;
        ISelectionProvider selProvider;
        ISelection sel;
        IStructuredSelection structSel;
        IDEPlugin plugin = IDEPlugin.getInstance();
        
        workbench = plugin.getWorkbench();
        window = workbench.getActiveWorkbenchWindow();
        if (window == null) {
            return null;
        }

        page = window.getActivePage();
        if (page == null) {
            return null;
        }
        
        part = page.getActivePart();
        if (part == null) {
            return null;
        }
        
        if (part instanceof IEditorPart) {
            editor = (IEditorPart) part;
            res = (IResource) editor.getEditorInput().getAdapter(
                    IResource.class);
            return res;
        }
        
        site = part.getSite();
        if (site == null) {
            return null;
        }
 
        selProvider = site.getSelectionProvider();
        if (selProvider == null) {
            return null;
        }
        
        sel = selProvider.getSelection();
        if (!(sel instanceof IStructuredSelection)) {
            return null;
        }
        
        structSel = (IStructuredSelection) sel;
        if (structSel.isEmpty()) {
            return null;
        }
            
        obj = structSel.getFirstElement();
        if (!(obj instanceof IAdaptable)) {
            return null;
        }
        
        adapt = (IAdaptable) obj;
        res = (IResource) adapt.getAdapter(IResource.class);

        return res;
    }
    
    /**
     * Esegue l&rsquo;export delle propriet&agrave;.
     * 
     * @param project Progetto.
     * @param file    File delle propriet&agrave;
     */
    private void export(IProject project, File propFile) throws CoreException {
        String msg;
        IPath path;
        IFile buildFile;
        ProjectDirectory projectDir;
        BuildRunner runner;
        
        projectDir = new ProjectDirectory(project);
        
        buildFile = projectDir.getBuildFile();
        if (!buildFile.exists()) {
            path = buildFile.getLocation();
            msg = String.format("File %1$s not found.", path);   
            throw new CoreException(StatusTools.toStatus(IStatus.ERROR, msg));                
        }
        
        runner = new BuildRunner();
        path = buildFile.getLocation();
        runner.setScriptFile(path.toFile());                             
        runner.getTargets().add(ProjectVariableResolver.TARGET_RUNTIMEPROPS);
        runner.getUserProperties().put(ProjectVariableResolver.PROP_FILEOUT,
                propFile.getAbsolutePath());
                
        runner.run();
    }
    
    /**
     * Costruisce il valore della variabile.
     * 
     * @param  project  Progetto.
     * @param  propFile File delle propriet&agrave;
     * @return          Valore.
     */
    private String buildValue(IProject project, File propFile) throws
            CoreException {
        boolean quoting;
        String value;
        StringBuilder buf;
        InputStream in = null;
        Properties props = new Properties();
        Exception pendingEx = null;
        ProjectVariableResolver.PropertiesWriter propWriter;
        
        try {
            in = new FileInputStream(propFile);
            props.load(in);
            // Reinoltro l'eccezione solo dopo aver emesso il file sulla console                        
        } catch(IOException ex) {
            pendingEx = ex;
        } finally {           
        	in = IOTools.close(in);
        }
        
        propWriter = new ProjectVariableResolver.PropertiesWriter(project, propFile,
                pendingEx);
        propWriter.schedule();
        
        if (pendingEx != null) {
            throw new CoreException(StatusTools.toStatus(pendingEx));
        }        
        
        // Il comando JVM non prevede un'opzione per leggere da file le
        // proprieta' di sistema da impostare ne' un response file:
        // Sono costretto ad usare un'opzione -D per ogni proprieta' sperando
        // che non siano troppe e di riuscire ad evitare problemi con gli
        // eventuali caratteri speciali.
        buf = new StringBuilder();
        for (String key : props.stringPropertyNames()) {
            value = props.getProperty(key);
            value = value.replace("\"", "\\\"");
            
            quoting = false;
            for (char ch : value.toCharArray()) {
                if (Character.isWhitespace(ch)) {
                    quoting = true;
                    break;
                }
            }
            
            if (buf.length() > 0) {
                buf.append(' ');
            }            
            buf.append("-D");
            buf.append(key);
            buf.append('=');
            if (quoting) {
                buf.append('\"');
            }
            buf.append(value);
            if (quoting) {
                buf.append('\"');
            }            
        }
        
        return buf.toString();
    }
    
    /**
     * Determinazione della risorsa selezionata.
     */
    private static final class SelectedResourceGetter implements Runnable {
        private ProjectVariableResolver myResolver;
        private IResource myResource = null;
        
        /**
         * Costruttore.
         * 
         * @param Risolutore.
         */
        SelectedResourceGetter(ProjectVariableResolver resolver) {
            myResolver = resolver;
        }
 
        /**
         * Restituisce la risorsa.
         * 
         * @return Oggetto.
         */
        IResource getResource() {
            return myResource;
        }
        
        /**
         * Esegue l&rsquo;operazione.
         */
        public void run() {
           myResource = myResolver.getSelectedResourceImpl(); 
        }
    }
    
    /**
     * Scrittura delle propriet&agrave;.
     */
    private static final class PropertiesWriter extends DisplayJob {
        private final IProject myProject;
        private final File myPropFile;
        private final Exception myException;
        
        /**
         * Costruttore.
         * 
         * @param project  Progetto.
         * @param propFile File dele propriet&agrave;.
         * @param ex       Eccezione inoltrata dalla lettura delle
         *                 propriet&agrave;. Pu&ograve; essere {@code null}.
         */
        PropertiesWriter(IProject project, File propFile, Exception ex) {
            myProject = project;
            myPropFile = propFile;
            myException = ex;
        }
        
        @Override
        protected void run() {
            String msg;
            IDEConsole console = IDEConsole.getInstance();
             
            if (myException != null) {
                // Mi assicuro di poter consultare il file sulla console 
                console.openConsole();
                console.write(IStatus.ERROR, "");
                msg = String.format(
                        "Exception in building run-time properties for project \"%1$s\".",
                        myProject.getName());
                console.write(IStatus.ERROR, msg);
                console.write(IStatus.ERROR, myException.getMessage());
            } else {
                console.write(IStatus.INFO, "");
                msg = String.format("Run-time properties for project \"%1$s\".",
                        myProject.getName());                
                console.write(IStatus.INFO, msg);
            }            

            try {
                console.write(myPropFile);
            } catch (IOException ex) {
                StatusTools.logStatus(ex);
            }
            
            console.write(IStatus.OK, "");
        }
    }    
}

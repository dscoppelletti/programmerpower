/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.build;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.eclipse.ant.core.*;
import org.eclipse.core.runtime.*;
import it.scoppelletti.sdk.ide.*;
import it.scoppelletti.sdk.ide.ui.*;

/**
 * Esecuzione di uno script Ant.
 * 
 * <P>La classe {@code BuildRunner} esegue uno script Ant in un&rsquo;istanza di
 * <ACRONYM TITLE="Java Virtual Machine">JVM</ACRONYM> separata da quella che
 * esegue Eclipse ed &egrave; quindi soggetta alle limitazioni descritte nei
 * paragrafi successivi per questo caso.</P>
 *  
 * <P>Il direttorio di installazione di Ant {@code ant.home} &egrave; assunto
 * uguale a quello impostato nella sezione {@code Ant|Runtime|Classpath} del
 * dialogo {@code Preferences}.<BR>
 * Il class-path per l&rsquo;esecuzione degli script include gli eventuali
 * elementi {@code Global Entries} impostati nella sezione
 * {@code Ant|Runtime|Classpath} del dialogo {@code Preferences}; gli altri
 * elementi del class-path impostati nella stessa sezione di configurazione sono
 * invece ignorati; il class-path pu&ograve; essere ulteriormente integrato
 * direttamente dallo script eseguito.</P>
 * 
 * <P>La classe {@code BuildRunner} recepisce le propriet&agrave; impostate
 * nella sezione {@code Ant|Runtime|Properties} del dialogo {@code Preferences};
 * queste propriet&agrave; possono quindi essere utilizzate per il passaggio di
 * parametri dall&rsquo;istanza del workbench di Eclipse agli script Ant da
 * eseguire.<BR>
 * I componenti Ant personalizzati impostati nelle sezioni 
 * {@code Ant|Runtime|Tasks} e {@code Ant|Runtime|Types} del dialogo
 * {@code Preferences} sono ignorati; lo script Ant pu&ograve; integrare
 * direttamente gli eventuali componenti personalizzati utilizzati.</P> 
 * 
 * <H4>1. Integrazione tra Eclipse e Ant</H4>
 * 
 * <P>L&rsquo;esecuzione di un script Ant nella stessa istanza di JVM che esegue
 * Eclipse presenta alcune limitazioni illustrate nei paragrafi seguenti.</P> 
 *
 * <H5>1.1. Caricamento delle classi</H5>
 * 
 * <P>Il meccanismo di caricamento delle classi di Ant consente di modificare il
 * class-path a run-time, ma questo &egrave; incompatibile con il modello dei
 * plug-in di Eclipse che prevede invece l&rsquo;immutabilit&agrave; del
 * class-path e delle dipendenze tra i plug-in a run-time.<BR>
 * Per ovviare a questo problema, &egrave; necessario che gli eventuali
 * componenti Ant personalizzati siano implementati da un modulo
 * <ACRONYM TITLE="Java Archive">JAR</ACRONYM> separato da qualsiasi altro
 * modulo JAR che implementa un plug-in di Eclipse.<BR>
 * Questa soluzione rappresenta un buon isolamento tra il codice dei plug-in di
 * Eclipse ed il codice delle estensioni Ant evitando dipendenze incrociate; in
 * questo caso per&ograve;, la distribuzione del plug-in non si limita ad un
 * singolo file JAR da copiare nel direttorio dei plug-in di Eclipse.</P> 
 * 
 * <H5>1.2. Librerie native</H5>
 * 
 * <P>Ogni volta che Eclipse esegue uno script Ant, viene istanziato un nuovo
 * class-loader che potrebbe caricare una libreria nativa utilizzata per la
 * prima volta da uno dei task; poich&eacute; le librerie native possono essere
 * caricate da un solo class-loader nella stessa istanza di JVM, se una
 * successiva esecuzione di uno script tenta di caricare la stessa libreria
 * nativa mentre il class-loader precedente non &egrave; ancora stato rilasciato
 * dal garbage collector, viene inoltrata un&rsquo;eccezione e
 * l&rsquo;esecuzione dello script fallisce.<BR>
 * Per ovviare a questo problema, &egrave; necessario che le librerie native
 * utilizzate dai task Ant siano caricate preventivamente da una classe del
 * plug-in di Eclipse.<BR>
 * Questa soluzione rende il codice dei plug-in di Eclipse dipendente
 * dall&rsquo;implementazione dei componenti Ant utilizzati.</P>
 * 
 * <H5>1.3. Utilizzo di una JVM separata</H5>
 * 
 * <P>Le limitazioni esposte sono aggirabili anche eseguendo gli script Ant in
 * un&rsquo;istanza di JVM separata da quella che esegue Eclipse.<BR>
 * Questa soluzione ostacola la completa integrazione e comunicazione tra
 * Eclipse e gli script Ant; ad esempio, i task Ant non possono utilizzare le
 * <ACRONYM TITLE="Application Programming Interface">API</ACRONYM> della
 * piattaforma Eclipse per interagire con l&rsquo;istanza del workbench.</P>
 * 
 * <H4>2. Le API di Eclipse</H4>
 * 
 * <P>La piattaforma Eclipse include gi&agrave; alcune API di integrazione con
 * Ant che per&ograve; presentano alcune limitazioni illustrate nei paragrafi
 * seguenti.</P>
 * 
 * <H5>2.1. Classe {@code AntRunner}</H5>
 * 
 * <P>La classe {@code AntRunner} esegue uno script Ant nella stessa istanza di
 * JVM che esegue Eclipse ed &egrave; quindi soggetta alle limitazioni descritte
 * in precedenza per questo caso.<BR>
 * L&rsquo;implementazione della classe {@code AntRunner} richiede inoltre che
 * la stessa istanza di JVM esegua un solo script Ant per volta; questo limita
 * la possibilit&agrave; di utilizzare la classe {@code AntRunner} nei
 * componenti Eclipse che potrebbero essere utilizzati in concorrenza da thread
 * paralleli.</P>
 * 
 * <H5>2.2. Configurazione di avvio di Ant</H5>
 * 
 * <P>Il dialogo {@code External Tools} del workbench di Eclipse consente di
 * configurare l&rsquo;esecuzione di uno script Ant: &egrave; possibile
 * richiedere l&rsquo;esecuzione sia nella stessa istanza di JVM che esegue
 * Eclipse sia in un&rsquo;istanza separata e sono previsti svariati altri
 * parametri. L&rsquo;output degli script avviati in questo modo &egrave;
 * dirottato su un&rsquo;apposita console che implementa anche le
 * funzionalit&agrave; di collegamento ipertestuale con lo script eseguito.<BR>  
 * Le classi che implementano la configurazione di avvio di Ant non sono
 * purtroppo delle API pubbliche (pacchetti {@code *internal*}) e quindi non
 * dovrebbero essere utilizzate nei plug-in prodotti da terze parti.</P>
 * 
 * <H5>2.3. Configurazione di avvio di applicazioni Java</H5>
 * 
 * <P>Le API della piattaforma Eclipse e di
 * <ACRONYM TITLE="Java Development Tooling">JDT</ACRONYM> pubblicano i
 * componenti per l&rsquo;avvio di applicazioni Java in un&rsquo;istanza di JVM
 * separata da quella che esegue Eclipse (pacchetti
 * {@code org.eclipse.debug.core}, {@code org.eclipse.debug.ui},
 * {@code org.eclipse.jdt.launching}).<BR>
 * Questi componenti potrebbero ad esempio essere utilizzati per eseguire degli
 * script Ant tenendo presente le limitazioni dovute all&rsquo;utilizzo di una
 * JVM separata descritte in precedenza.<BR> 
 * Le applicazioni sarebbero comunque avviate in modalit&agrave; asincrona e
 * quindi sarebbe difficoltoso utilizzare degli script Ant come parte
 * personalizzabile di un processo costituito da passi sequenziali sotto il
 * controllo dell&rsquo;istanza di Eclipse.</P>
 * 
 * @see   <A HREF="${it.scoppelletti.token.antUrl}" TARGET="_top">Apache Ant</A>
 * @since 1.0.0
 */
public final class BuildRunner {
    
    /**
     * Timeout per l&rsquo;esecuzione dello script (espresso in millisecondi).
     * Il valore della costante &egrave; <CODE>{@value}</CODE>.
     * 
     * <P>La sezione {@code Ant|Runtime} del dialogo {@code Preferences}
     * consente di configurare il valore del timeout nella casella
     * {@code Separate JRE timeout (ms)}; la classe {@code AntCorePreferences} 
     * della piattaforma Eclipse purtroppo non espone pubblicamente questo
     * valore di configurazione, quindi la classe {@code BuildRunner} utilizza
     * come timeout il valore della costante {@code TIMEOUT}.</P> 
     */
    public static final long TIMEOUT = 20000;
    
    private static final String MAIN_CLASS =
            "org.apache.tools.ant.launch.Launcher";
    
    private File myScriptFile = null;
    private List<String> myTargets;
    private Properties myUserProps;
    
    /**
     * Costruttore.
     */
    public BuildRunner() {
        myTargets = new ArrayList<String>();
        myUserProps = new Properties();
    }
    
    /**
     * Restituisce il file dello script.
     * 
     * @return File.
     * @see    #setScriptFile
     */
    public File getScriptFile() {
        return myScriptFile;
    }
    
    /**
     * Imposta il file dello script.
     * 
     * <P>Il direttorio di lavoro &egrave; assunto uguale al direttorio in cui
     * &egrave; inserito lo script; il direttorio di base per l&rsquo;esecuzione
     * dello script &egrave; comunque quello specificato dall&rsquo;attributo
     * {@code basedir} dell&rsquo;elemento radice dello script.</P>
     *
     * @param                        file File.
     * @it.scoppelletti.tag.required 
     * @see                               #getScriptFile
     */
    public void setScriptFile(File file) {
        myScriptFile = file;
    }

    /**
     * Restituisce la lista dei target da eseguire.
     * 
     * <P>Se non viene impostato nessun target da eseguire sulla collezione
     * {@code getTargets}, viene eseguito il target di default specificato
     * dall&rsquo;attributo {@code default} dell&rsquo;elemento radice dello
     * script.</P>
     * 
     * @return Collezione.
     */
    public List<String> getTargets() {
        return myTargets;
    }
    
    /**
     * Restituisce le propriet&agrave; utente.
     *
     * <P>Lo script eseguito recepisce sia le propriet&agrave; impostate sulla
     * sezione {@code Ant|Runtime|Properties} del dialogo {@code Preferences}
     * che le propriet&agrave; impostate sulla collezione
     * {@code getUserProperties}; in caso di conflitti, queste ultime hanno la
     * priorit&agrave;.<BR>
     * La classe {@code AntRunner} della piattaforma Eclipse invece recepisce
     * le propriet&agrave; impostate sulla sezione
     * {@code Ant|Runtime|Properties} del dialogo {@code Preferences} solo se
     * non viene utilizzato il metodo {@code addUserProperties} per impostare
     * delle propriet&agrave; da programma.</P>
     *  
     * @return Collezione.
     */
    public Properties getUserProperties() {
        return myUserProps;
    }
    
    /**
     * Esegue lo script.
     * 
     * @see #run(IProgressMonitor)
     */
    public void run() throws CoreException {        
        run(null);
    }
    
    /**
     * Esegue lo script.
     * 
     * <P>L&rsquo;esecuzione dello script &egrave; sincrona in modo da poter
     * essere facilmente inserita in un processo costituito da passi sequenziali
     * sotto il controllo dell&rsquo;istanza di Eclipse.</P> 
     *  
     * <P>Se la console {@code IDEConsole} &egrave; aperta, il metodo
     * {@code run} invia l&rsquo;output dello script eseguito su tale console.
     * Se l&rsquo;esecuzione dello script fallisce, la console viene
     * automaticamente aperta per assicurare la possibilit&agrave; di esaminare
     * l&rsquo;output dello script.</P>
     *  
     * @param monitor Gestore dell&rsquo;avanzamento.
     * @see           it.scoppelletti.sdk.ide.ui.IDEConsole
     * @since         1.1.0
     */
    public void run(IProgressMonitor monitor) throws CoreException {        
        File outputFile;
        BuildException pendingEx = null;
        Task task;                
        BuildRunner.OutputWriter outputWriter;
        SubMonitor progress;
        
        progress = SubMonitor.convert(monitor, "Running Ant...", 2);
        try {
            try {
                outputFile = File.createTempFile("build", ".log");
            } catch (IOException ex) {
                throw new CoreException(StatusTools.toStatus(ex));
            }   
         
            try {
                task = buildTask(outputFile);
            } catch (BuildException ex) {
                throw new CoreException(StatusTools.toStatus(ex));
            } catch (IOException ex) {
                throw new CoreException(StatusTools.toStatus(ex));
            }   
            progress.worked(1);
            
            try {
                task.execute();
            } catch (BuildException ex) {
                // Reinoltro l'eccezione solo dopo aver emesso l'output sulla
                // console
                pendingEx = ex;    
            }
            progress.worked(1);
            
            outputWriter = new BuildRunner.OutputWriter(myScriptFile,
                    outputFile, pendingEx);
            outputWriter.schedule();
            
            if (pendingEx != null) {
                throw toCoreException(pendingEx);
            }            
        } finally {
            if (monitor != null) {
                monitor.done();
            }
        }        
    }
    
    /**
     * Costruisce il task Ant.
     * 
     * @param  outputFile File di output.
     * @return            Oggetto.
     */
    private Task buildTask(File outputFile) throws CoreException, IOException {
        String name;
        File workingDir;
        org.apache.tools.ant.Project project;
        org.apache.tools.ant.Target projectTarget;
        org.apache.tools.ant.taskdefs.Java task;
        org.apache.tools.ant.types.Commandline.Argument arg;
        
        name = getClass().getSimpleName();
        
        project = new org.apache.tools.ant.Project();
        project.init();
        project.setName(name);
        
        projectTarget = new org.apache.tools.ant.Target();
        projectTarget.setName(name);
        projectTarget.setProject(project);
        
        task = new org.apache.tools.ant.taskdefs.Java();
        task.setTaskName(name);
        task.setTaskType("java");
        task.setProject(project);
        task.setOwningTarget(projectTarget);
        
        // Entry-point
        task.setClassname(BuildRunner.MAIN_CLASS);
        
        // Direttorio di lavoro
        workingDir = myScriptFile.getParentFile();
        if (workingDir != null) {
            task.setDir(workingDir);
        }
        
        // Avvio una nuova JVM
        task.setFork(true);
        
        // Attendo il termine della JVM
        task.setSpawn(false);
        
        // Timeout
        task.setTimeout(BuildRunner.TIMEOUT);

        // Non propago le variabili di ambiente correnti
        task.setNewenvironment(false);
        
        // Non replico le proprieta' di sistema della JVM corrente.
        // Impostando l'attributo, ho verificato che, ad esempio, la 
        // proprieta' user.dir coincide con il direttorio di lavoro corrente
        // (normalmente il direttorio home dell'utente) anziche' con l'attributo
        // dir impostato.
        task.setCloneVm(false);
                
        // Se il codice di uscita della JVM non e' 0, inoltro un eccezione.
        task.setFailonerror(true);

        // File di output
        task.setOutput(outputFile);
        task.setAppend(false);
        
        // Script da eseguire
        arg = task.createArg();
        arg.setValue("-buildfile");        
        arg = task.createArg();
        arg.setFile(myScriptFile.getAbsoluteFile());
        
        // Disabilito l'input
        arg = task.createArg();
        arg.setValue("-noinput");
        
        // Proprieta'
        arg = task.createArg();
        arg.setValue("-propertyfile");        
        arg = task.createArg();
        arg.setFile(createPropertyFile());        
        
        // Target da eseguire
        for (String target : myTargets) {
            arg = task.createArg();
            arg.setValue(target);
        }
        
        setClasspath(task.createClasspath());
        
        return task;
    }

    /**
     * Imposta il class-path.
     * 
     * @param classpath Class-path.
     */
    private void setClasspath(org.apache.tools.ant.types.Path classpath) throws
            CoreException {
        String antHome;
        File file;
        URL url;
        org.apache.tools.ant.types.Path.PathElement pathEntry;
        IAntClasspathEntry[] entries;
        AntCorePlugin antPlugin = AntCorePlugin.getPlugin();
        AntCorePreferences prefs = antPlugin.getPreferences();
        
        antHome = prefs.getAntHome();
        if (antHome == null) {
            throw new CoreException(StatusTools.toStatus(IStatus.ERROR,
                    "Undefined Ant home directory."));
        }
        
        file = new File(antHome);
        file = new File(file, "lib");
        file = new File(file, "ant-launcher.jar");        
        
        pathEntry = classpath.createPathElement();
        pathEntry.setLocation(file);
        
        entries = prefs.getAdditionalClasspathEntries();
        if (entries == null) {
            return;
        }
        
        for (IAntClasspathEntry entry : entries) {
            if (entry.isEclipseRuntimeRequired()) {
                continue;
            }
            
            url = entry.getEntryURL();
            if (url == null) {
                continue;
            }
            
            pathEntry = classpath.createPathElement();
            pathEntry.setPath(url.getPath());            
        }
    }
    
    /**
     * Crea il file delle propriet&agrave;
     * 
     * <P>L&rsquo;impostazione delle propriet&agrave; avviene attraverso un file
     * temporaneo (opzione {@code -properyfile} per evitare i problemi che
     * l&rsquo;opzione {@code -D} potrebbe avere con i caratteri speciali.</P>
     *   
     * @return File.
     */
    private File createPropertyFile() throws IOException {
        File propFile;
        OutputStream out = null;
        Properties props;
        
        propFile = File.createTempFile("build", ".properties");
        
        props = getDefaultProperties();
        props.putAll(myUserProps);
        
        try {
            out = new FileOutputStream(propFile);
            props.store(out, getClass().getName());
        } finally {
        	out = IOTools.close(out);
        }
        
        return propFile;
    }
        
    /**
     * Restituisce le propriet&agrave; configurate.
     * 
     * @return Collezione.
     */
    private Properties getDefaultProperties() {
        Properties props;
        List<Property> list;
        AntCorePlugin antPlugin = AntCorePlugin.getPlugin();
        AntCorePreferences prefs = antPlugin.getPreferences();
        
        props = new Properties();
        list = (List<Property>) prefs.getProperties();
        for (Property prop : list) {
            props.put(prop.getName(), prop.getValue());
        }
        
        return props;
    }    
    
    /**
     * Converte l&rsquo;eccezione inoltrata dall&rsquo;esecuzione dello script
     * in un&rsquo;eccezione Eclipse.
     * 
     * @param  ex Eccezione Ant.
     * @return    Eccezione Eclipse.
     */
    private CoreException toCoreException(BuildException ex) {
        StringBuilder buf;
        IStatus status;
        
        buf = new StringBuilder("BuildRunner failed: ");
        buf.append(BuildRunner.MAIN_CLASS);
        buf.append(" -buildfile ");
        buf.append(myScriptFile.getAbsolutePath());
        
        for (String target : myTargets) {
            buf.append(' ');
            buf.append(target);
        }        
        
        status = new Status(IStatus.ERROR, IDEPlugin.ID, buf.toString(), ex);
        
        return new CoreException(status);
    }
    
    /**
     * Scrittura dell&rsquo;output dello script.
     */
    private static final class OutputWriter extends DisplayJob {
        private final File myScriptFile;
        private final File myOutputFile;
        private final BuildException myException;
        
        /**
         * Costruttore.
         * 
         * @param scriptFile File dello script.
         * @param outputFile File di output.
         * @param ex         Eccezione inoltrata dall&rsquo;esecuzione dello
         *                   script. Pu&ograve; essere {@code null}.
         */
        OutputWriter(File scriptFile, File outputFile, BuildException ex) {
            myScriptFile = scriptFile;
            myOutputFile = outputFile;
            myException = ex;
        }
        
        @Override
        protected void run() {
            String msg;
            IDEConsole console = IDEConsole.getInstance();
            
            if (myException != null) {
                console.write(IStatus.ERROR, "");
                msg = String.format("Script %1$s failed.",
                        myScriptFile.getAbsolutePath());
                console.write(IStatus.ERROR, msg);
                console.write(IStatus.ERROR, myException.getMessage());
            } else {
                console.write(IStatus.OK, "");
            }

            try {                
                console.write(myOutputFile);
            } catch (IOException ex) {
                StatusTools.logStatus(ex);
            }
            
            console.write(IStatus.OK, "");
        }
    }
}

/*
 * Copyright (C) 2008-2015 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.jdt.core.*;
import org.eclipse.jdt.ui.wizards.*;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.*;
import org.eclipse.jface.wizard.*;
import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;
import it.scoppelletti.sdk.ide.projects.*;

/**
 * Dialogo per l&rsquo;inserimento del class-path corrispondente alle dipendenze
 * di un progetto.
 *
 * @see   it.scoppelletti.sdk.ide.projects.DependenciesContainer
 * @since 1.0.0
 */
public final class DependenciesContainerPage extends WizardPage implements
    IClasspathContainerPage {
    private static final String ID =
        "it.scoppelletti.sdk.ide.ui.DependenciesContainerPage";         
    private static final String IMAGE_RES = "programmerpower32x32.png";

    private IClasspathEntry myEntry = null;
    
    /**
     * Costruttore.
     */
    public DependenciesContainerPage() {
        super(DependenciesContainerPage.ID);
        
        ImageDescriptor image;
        
        setTitle(DependenciesContainer.NAME);
        image = ImageDescriptor.createFromFile(getClass(),
                DependenciesContainerPage.IMAGE_RES);        
        this.setImageDescriptor(image);
    }
    
    /**
     * Crea il controllo.
     * 
     * @param parent Controllo parent.
     */
    public void createControl(Composite parent) {
        // Devo impostare almeno un controllo con il metodo setControl,
        // altrimenti non funziona correttamente il wizard "Build Path|Add
        // Libraries" del progetto.
        // Gli elementi che risolvono il container vengono comunque esposti
        // nell'explorer del progetto.
        Label label;
        
        label = new Label(parent, SWT.NULL);
        label.setText(DependenciesContainer.NAME);
        setControl(label);
        Dialog.applyDialogFont(label);
    }

    /**
     * Conferma del dialogo.
     * 
     * @return Esito dell&rsquo;operazione.
     */
    public boolean finish() {
        return true;
    }
    
    /**
     * Restituisce il class-path selezionato.
     * 
     * @return Oggetto.
     */
    public IClasspathEntry getSelection() {
        return myEntry;
    }

    /**
     * Imposta il class-path selezionato.
     * 
     * @param containerEntry Oggetto.
     */
    public void setSelection(IClasspathEntry containerEntry) {
        myEntry = containerEntry;
        if (myEntry == null) {
            myEntry = initEntry();
        }
    }    
    
    /**
     * Inizializza il class-path.
     * 
     * @return Oggetto.
     */
    private IClasspathEntry initEntry() {
        return JavaCore.newContainerEntry(DependenciesContainer.PATH);
    }    
}

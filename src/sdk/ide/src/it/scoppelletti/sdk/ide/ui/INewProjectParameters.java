/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;

/**
 * Parametri per la creazione di un nuovo progetto.
 */
interface INewProjectParameters {

    /**
     * Tipo di progetto Java. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    static final String TARGET_JAR = "jar";
    
    /**
     * Tipo di progetto Dynamic Web Application. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    static final String TARGET_WAR = "war";
        
    /**
     * Restituisce il progetto.
     * 
     * @return Oggetto.
     */
    IProject getProjectHandle();
    
    /**
     * Restituisce il nome del progetto.
     * 
     * @return Valore.
     */
    String getProjectName();
    
    /**
     * Restituisce la versione.
     * 
     * @return Valore.
     */
    String getVersion();
    
    /**
     * Restituisce il titolo.
     * 
     * @return Valore.
     */
    String getSpecificationTitle();
    
    /**
     * Restituisce il produttore.
     * 
     * @return Valore.
     */
    String getVendor();
    
    /**
     * Restituisce il nome di base del progetto.
     * 
     * @return Valore.
     */
    String getBaseName();
    
    /**
     * Restituisce l&rsquo;estensione del nome dell&rsquo;archivio di
     * distribuzione.
     * 
     * @return Valore.
     */
    String getArchiveExtension();
    
    /**
     * Restituisce il tipo di progetto.
     * 
     * @return Valore.
     */
    String getTarget();
    
    /**
     * Restituisce il nome della classe che implementa l&rsquo;entry-point
     * {@code main}.
     * 
     * @return Valore.
     */
    String getMainClass();

    /**
     * Restituisce il percorso del direttorio di progetto.
     * 
     * @return Percorso.
     */
    IPath getLocationPath();    
}

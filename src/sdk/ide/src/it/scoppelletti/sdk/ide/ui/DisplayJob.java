/*
 * Copyright (C) 2008 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import org.eclipse.swt.widgets.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Schedulatore di un&rsquo;operazione nel thread della
 * <ACRONYM TITLE="User Interface">UI</ACRONYM>.
 * 
 * <P>Bisognerebbe evitare di eseguire operazioni massive nel thread della
 * UI che bloccherebbero il normale ciclo degli eventi della UI stessa: in
 * questi casi &egrave; possibile utilizzare una classe derivata dalla classe
 * {@code DisplayJob} che implementa l&rsquo;operazione da eseguire nel metodo
 * {@code run}.</P>
 * 
 * @since 1.0.0
 */
public abstract class DisplayJob {
    
    /**
     * Costruttore.
     */
    protected DisplayJob() {        
    }
    
    /**
     * Schedula l'operazione.
     */
    public final void schedule() {
        Display display;        
        IDEPlugin plugin = IDEPlugin.getInstance();
        
        display = plugin.getCurrentDisplay();       
        display.asyncExec(new DisplayJob.Executor(this));        
    }       
    
    /**
     * Operazione.
     */
    protected abstract void run();
    
    /**
     * Operazione.
     */
    private static final class Executor implements Runnable {
        private final DisplayJob myJob;
        
        /**
         * Costruttore
         * 
         * @param job Operazione.
         */
        Executor(DisplayJob job) {
           myJob = job; 
        }
        
        /**
         * Esegue l'operazione.
         */
        public void run() {
            myJob.run();
        }
    }
}

/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.io.*;
import java.util.*;
import java.util.List;
import org.eclipse.core.runtime.*;
import org.eclipse.core.runtime.jobs.*;
import org.eclipse.jface.text.*;
import org.eclipse.swt.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.console.*;
import org.eclipse.ui.progress.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Partizionatore della console. 
 */
final class IDEConsolePartitioner implements IConsoleDocumentPartitioner,
    IDocumentPartitionerExtension {
    private static final int UPDATE_DELAY = 50;
    private static final int LEN_WARNING = 1000;
    private static final int LEN_CRITICAL = 10000;    
    private final IDEConsoleImpl myConsole;
    private final List<IDEConsolePartitioner.Partition> myPartitions;
    private final List<IDEConsolePartitioner.PendingPartition>
        myPendingPartitions;        
    private int myPendingLen = 0;
    private int myUpdatingOffset = 0;
    private IDocument myDoc;        
    private IDEConsolePartitioner.UpdateJob myUpdateJob;        
    private IDEConsolePartitioner.Partition myLastPartition = null;
    private IDEConsolePartitioner.PendingPartition myCloseConsolePartition =
        null;    
    private List<IDEConsolePartitioner.PendingPartition> myUpdatingPartitions =
        null;            
    
    /**
     * Costruttore.
     * 
     * @param console Console
     */
    IDEConsolePartitioner(IDEConsoleImpl console) {
        myConsole = console;
        myPartitions = new ArrayList<IDEConsolePartitioner.Partition>();
        myPendingPartitions =
            new ArrayList<IDEConsolePartitioner.PendingPartition>();
    }

    /**
     * Restituisce i tipi di partizione supportati.
     * 
     * @return Vettore.
     */
    public String[] getLegalContentTypes() {
        return new String[] { IDEConsolePartitioner.Partition.PARTITION_TYPE };
    }
         
    /**
     * Collega un documento.
     */
    public void connect(IDocument document) {
        synchronized (myPartitions) {
            myDoc = document;
            myDoc.setDocumentPartitioner(this);
            myUpdateJob = new IDEConsolePartitioner.UpdateJob(this);
            myUpdateJob.setRule(myConsole.getSchedulingRule());
            myPartitions.clear();
            myPendingPartitions.clear();                  
        }
    }
    
    /**
     * Scollega il documento.
     */
    public void disconnect() {
        synchronized (myPartitions) {    
            myDoc = null;
            myPartitions.clear();
        }
    }

    /**
     * Chiude il flusso di scrittura.
     * 
     * @param out Flusso di scrittura.
     */
    void close() {
        synchronized (myPartitions) {
            // Uso una partizione particolare per segnalare al processo di
            // aggiornamento che la console deve essere chiusa
            myCloseConsolePartition =
                new IDEConsolePartitioner.PendingPartition(this, null, null);            
            myPendingPartitions.add(myCloseConsolePartition);
        }
        myUpdateJob.schedule();
    }
    
    /**
     * Aggiunge una stringa alla console.
     * 
     * @param out Flusso di scrittura.
     * @param s   Stringa.
     */
    void append(IDEConsoleStream out, String s) throws IOException {
        int n;
        PendingPartition lastPartition;
        
        synchronized (myPartitions) {
            if (myDoc == null) {
                throw new IOException("Document is closed.");
            }

            n = myPendingPartitions.size();
            if (n > 0) {
                lastPartition = myPendingPartitions.get(n - 1);
            } else {
                lastPartition = null;
            }
   
            if (lastPartition != null && lastPartition.getStream() == out) {
                lastPartition.append(s);
            } else {
                myPendingPartitions.add(new PendingPartition(this, out, s));
                if (myPendingLen > IDEConsolePartitioner.LEN_WARNING) {
                    // La dimensione totale delle partizioni in attesa di
                    // scrittura ha superato la soglia di attenzione:
                    // Meglio schedulare l'aggiornamento senza ritardo.
                    myUpdateJob.schedule();
                } else {
                    myUpdateJob.schedule(IDEConsolePartitioner.UPDATE_DELAY);
                }
            }

            n = myPendingLen;
        }
        
        if (n > IDEConsolePartitioner.LEN_CRITICAL) {
            // La dimensione totale delle partizioni in attesa di scrittura
            // ha superato la soglia di critica 
            if (Display.getCurrent() == null) {
                // Blocco il thread fino a che non sia avvenuto l'aggiornamento
                try {
                    myPartitions.wait();
                } catch (InterruptedException ex) {
                    StatusTools.logStatus(ex);
                }
            } else {
                // Non posso bloccare il thread della UI:
                // Aggiorno subito.
                update();
            }
        }
    }
    
    /**
     * Incrementa la dimensione del testo in attesa di essere aggiunto al
     * documento.
     * 
     * @param delta Incremento.
     */
    private void incrementPendingSize(int delta) {
        myPendingLen += delta;
    }
    
    /**
     * Restituisce la partizione che include una porzione del documento.
     * 
     * @param offset Posizione della porzione del documento ricercata.
     */
    public ITypedRegion getPartition(int offset) {
        int startIdx, endIdx;
        
        synchronized (myPartitions) {
            for (ITypedRegion partition : myPartitions) {
                startIdx = partition.getOffset();
                endIdx = startIdx + partition.getLength();
                if (offset >= startIdx && offset < endIdx) {
                    return partition;
                } 
            }
        }
        
        return null;
    }

    /**
     * Restituisce il tipo di partizione che che include una porzione del
     * documento.
     * 
     * @param offset Posizione della porzione del documento interessata.
     */
    public String getContentType(int offset) {
        ITypedRegion partition;
        
        partition = getPartition(offset);
        if (partition == null) {
            return null;            
        }
        
        return partition.getType();
    }
    
    /**
     * Verifica se una porzione del documento &egrave; a sola lettura.
     * 
     * @param  offset Posizione della porzione del documento interessata.
     * @return        Esito della verifica. Restituisce sempre {@code true}
     *                perch&eacute; la console &egrave; a sola lettura.
     */
    public boolean isReadOnly(int offset) {
        return true;
    }
    
    /**
     * Calcola il partizionamento di una porzione del documento.
     * 
     * @param  offset Posizione iniziale della porzione del documento
     *                interessata.
     * @param  len    Dimensione della porzione del documento interessata.
     * @return        Vettore delle partizioni.
     */
    public ITypedRegion[] computePartitioning(int offset, int len) {
        int idx, endIdx, leftIdx, rightIdx, midIdx;
        IDEConsolePartitioner.Partition partition;
        List<IDEConsolePartitioner.Partition> list;
        
        synchronized (myPartitions) {
            if (myPartitions.size() == 1) {
                return new IDEConsolePartitioner.Partition[]
                       { myPartitions.get(0) };
            }
        
            endIdx = offset + len;
            leftIdx = 0;
            rightIdx = myPartitions.size() - 1;
            while (leftIdx < rightIdx) {                
                midIdx = (leftIdx + rightIdx) / 2;                        
                partition = myPartitions.get(midIdx);
                if (endIdx < partition.getOffset()) {
                    if (leftIdx == midIdx) {
                        rightIdx = leftIdx;
                    } else {
                        rightIdx = midIdx - 1;
                    }
                } else if (offset >
                    partition.getOffset() + partition.getLength() - 1) {
                    if (rightIdx == midIdx) {
                        leftIdx = rightIdx;
                    } else {
                        leftIdx = midIdx + 1;
                    }
                } else {
                    leftIdx = midIdx;
                    rightIdx = midIdx;
                }
            }
                        
            idx = leftIdx - 1;
            for (idx = leftIdx - 1; idx >= 0; idx--) {
                partition = myPartitions.get(idx);
                if (partition.getOffset() + partition.getLength() <= offset) {
                    break;
                }     
            }
            
            list = new ArrayList<IDEConsolePartitioner.Partition>();
            for (idx = idx + 1; idx < myPartitions.size(); idx++) {
                partition = myPartitions.get(idx);
                if (partition.getOffset() >= endIdx) {
                    break;
                }
            
                list.add(partition);
            }
        }
               
        return (ITypedRegion[]) list.toArray(
                new IDEConsolePartitioner.Partition[list.size()]);
    }
    
    /**
     * Calcola gli stili di visualizzazione per una porzione del documento.
     * 
     * @param  offset Posizione iniziale della porzione del documento
     *                interessata.
     * @param  len    Dimensione della porzione del documento interessata.
     * @return        Vettore degli stili. 
     */
    public StyleRange[] getStyleRanges(int offset, int len) {
        int i, n, rangeOffset, rangeLen;
        StyleRange[] styles;
        IDEConsolePartitioner.Partition[] partitions;
        
        synchronized (myPartitions) {
            if (myDoc == null) {
                return new StyleRange[0];
            }
        }
        
        partitions = (IDEConsolePartitioner.Partition[])
            computePartitioning(offset, len);
        n = partitions.length;
        styles = new StyleRange[n];
        for (i = 0; i < n; i++) {
            rangeOffset = partitions[i].getOffset();
            if (offset > rangeOffset) {
                rangeOffset = offset;
            }
            rangeLen = partitions[i].getLength();
            styles[i] = partitions[i].getStyleRange(rangeOffset, rangeLen);
        }            
        
        return styles;
    }

    /**
     * Richiesta di modifica del documento.
     * 
     * @param event Evento.
     */
    public void documentAboutToBeChanged(DocumentEvent event) {        
    }

    /**
     * Modifica del documento.
     * 
     * @param  event Evento.
     * @return       Regione del documento modificata.
     */
    public IRegion documentChanged2(DocumentEvent event) {
        synchronized (myPartitions) {
            if (myDoc == null) {
                return null;
            }
        
            if (myDoc.getLength() == 0) {
                myLastPartition = null;
                return new Region(0, 0);
            }
        
            if (myUpdatingPartitions != null) {
                documentChangedUpdating();
            }
        }
        
        return new Region(event.getOffset(), event.getText().length());
    }
    
    /**
     * Modifica del documento durante l&rsquo;aggiornamento.
     */
    private void documentChangedUpdating() {
        int len;
        IDEConsolePartitioner.Partition partition;
                
        if (myUpdatingPartitions == null) {
            return;
        }
        
        for (PendingPartition pendingPartition : myUpdatingPartitions) {
            if (pendingPartition == myCloseConsolePartition) {
                continue;
            }
            
            len = pendingPartition.getText().length();
            if (myLastPartition != null && myLastPartition.getStream() ==
                    pendingPartition.getStream()) {
                myLastPartition.setLength(myLastPartition.getLength() + len);                            
            } else {
                partition = new IDEConsolePartitioner.Partition(
                        pendingPartition.getStream(), len);
                partition.setOffset(myUpdatingOffset);                            
                myPartitions.add(partition);
                myLastPartition = partition;
            }
            myUpdatingOffset += len;
        }        
    }
    
    /**
     * Modifica del documento.
     * 
     * @param  event Evento.
     * @return       Indicatore di documento modificato.
     */
    public boolean documentChanged(DocumentEvent event) {
        return (documentChanged2(event) != null);
    }

    /**
     * Aggiornamento della console.
     */
    private void update() {
        boolean consoleClosed;
        StringBuilder buf;
        List<IDEConsolePartitioner.PendingPartition> pendingPartitions;
        
        pendingPartitions =
            new ArrayList<IDEConsolePartitioner.PendingPartition>();
        synchronized (myPartitions) {
            pendingPartitions.addAll(myPendingPartitions);
            myPendingPartitions.clear();
            myPendingLen = 0;
            myPartitions.notifyAll();
            
            consoleClosed = false;
            buf = new StringBuilder();
            for (IDEConsolePartitioner.PendingPartition pendingPartition :
                pendingPartitions) {
                if (pendingPartition != myCloseConsolePartition) {
                    buf.append(pendingPartition.getText());
                } else {
                    consoleClosed = true;
                }
            }           
            
            if (myDoc != null) {
                myUpdatingPartitions = pendingPartitions;
                myUpdatingOffset = myDoc.getLength();
                
                try {
                    myDoc.replace(myUpdatingOffset, 0, buf.toString());
                } catch (BadLocationException ex) {
                    StatusTools.logStatus(ex);
                } finally {                
                    myUpdatingPartitions = null;
                }
            }
            
            if (consoleClosed) {
                myConsole.partitionerFinished();
            }
        }
    }
    
    /**
     * Verifica la necessit&agrave; dell&aggiornamento della console.
     * 
     * @return Esito della verifica.
     */
    private boolean shouldUpdate() {
        synchronized (myPartitions) {
            if (myDoc != null && myPendingPartitions.size() > 0) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Partizione.
     */
    private static final class Partition implements ITypedRegion {
        private static final String ID = 
            "it.scoppelletti.sdk.ide.ui.IDEConsole.Partition";
        
        /**
         * Tipo di partizione.
         */
        static final String PARTITION_TYPE =
            ConsolePlugin.getUniqueIdentifier().concat(
            IDEConsolePartitioner.Partition.ID);
        
        private int myOffset;
        private int myLen;
        private IDEConsoleStream myOut;
        
        /**
         * Costruttore.
         * 
         * @param out  Flusso di scrittura.
         * @param len  Dimensione della partizione.
         */
        Partition(IDEConsoleStream out, int len) {
            myOut = out;
            myLen = len;
        }
        
        /**
         * Restituisce il flusso di scrittura.
         * 
         * @return Flusso di scrittura.
         */
        IDEConsoleStream getStream() {
            return myOut;
        }
        
        /**
         * Restituisce il tipo di partizione.
         * 
         * @return Valore.
         */
        public String getType() {
            return IDEConsolePartitioner.Partition.PARTITION_TYPE;
        }  
        
        /**
         * Restituisce la posizione della partizione nel documento.
         * 
         * @return Valore.
         */
        public int getOffset() {
            return myOffset;
        }
        
        /**
         * Imposta la posizione della partizione nel documento.
         * 
         * @param offset Valore.
         */
        void setOffset(int offset) {
            myOffset = offset;
        }
        
        /**
         * Restituisce la dimensione della partizione.
         * 
         * @return Valore.
         */
        public int getLength() {
            return myLen;
        }
        
        /**
         * Imposta la dimensione della partizione.
         * 
         * @param len Valore.
         */
        void setLength(int len) {
            myLen = len;
        }                
        
        /**
         * Restituisce lo stile di visualizzazione per una porzione della
         * partizione.
         * 
         * @param  offset Posizione iniziale della porzione della partizione
         *                interessata.
         * @param  len    Dimensione della porzione della porzione della partizione
         *                interessata. 
         */
        StyleRange getStyleRange(int offset, int len) {
            return new StyleRange(offset, len, myOut.getColor(), null,
                    SWT.NORMAL);
        }    
    }
    
    /**
     * Partizione in attesa dell&rsquo;aggiornamento del documento.
     */
    private static final class PendingPartition {
        private final IDEConsolePartitioner myPartitioner;
        private final IDEConsoleStream myOut;
        private StringBuilder myBuf = new StringBuilder();
      
        /**
         * Costruttore.
         * 
         * @param partitioner Partizionatore.
         * @param out         Flusso di scrittura.
         * @param s           Testo iniziale.
         */
        PendingPartition(IDEConsolePartitioner partitioner,
                IDEConsoleStream out, String s) {
            myPartitioner = partitioner;
            myOut = out;
            if (s != null) {
                append(s);
            }
        }
        
        /**
         * Restituisce il flusso di scrittura.
         * 
         * @return Flusso di scrittura.
         */
        IDEConsoleStream getStream() {
            return myOut;
        }
        
        /**
         * Restituisce il testo.
         * 
         * @return Testo.
         */
        String getText() {
            return myBuf.toString();
        }
        
        /**
         * Aggiunge una stringa alla partizione.
         * 
         * @param s Testo.
         */
        void append(String s) {
            myBuf.append(s);
            myPartitioner.incrementPendingSize(s.length());
        }
    }
    
    /**
     * Aggiornamento della console.
     * 
     * @param partitioner Partizionatore.
     */
    private static final class UpdateJob extends UIJob {
        private final IDEConsolePartitioner myPartitioner;
        
        /**
         * Costruttore.
         */
        UpdateJob(IDEConsolePartitioner partitioner) {
            super("Update job");
            
            myPartitioner = partitioner;
            setSystem(true);
            setPriority(Job.INTERACTIVE);
        }

        /**
         * Esegue il job.
         */
        @Override
        public IStatus runInUIThread(IProgressMonitor monitor) {
            myPartitioner.update();
            return Status.OK_STATUS;
        }        
            
        /**
         * Verifica se il job deve essere eseguito.
         * 
         * @return Esito della verifica.
         */
        @Override
        public boolean shouldRun() {
            return myPartitioner.shouldUpdate();
        }
    }       
}

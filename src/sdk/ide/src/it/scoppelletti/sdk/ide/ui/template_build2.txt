<?xml version="1.0" encoding="UTF-8"?>
<project name="$projectName$" default="build" basedir="..">
	<property name="project.dir" value="${basedir}" />
    <property name="system.dir" value="${it.scoppelletti.programmerpower.home}" />
	<property name="tomcat.properties" value="${it.scoppelletti.sdk.tomcat.properties}" />	        
    <property name="build.impl" value="${system.dir}/etc/build-impl.xml" />
        
    <import file="${build.impl}" />
</project>

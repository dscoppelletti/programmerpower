/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sdk.ide.ui;

import java.util.*;
import java.util.List;
import java.util.regex.*;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.dialogs.*;
import it.scoppelletti.sdk.ide.*;

/**
 * Pagina del wizard per la creazione di un nuovo progetto.
 */
final class NewProjectPage extends WizardNewProjectCreationPage
        implements INewProjectParameters {
    
    private static final String DEF_VENDOR =
        "it.scoppelletti.sdk.ide.ui.NewProjectPage.vendor";
    private static final String PROP_USERNAME = "user.name";
    private static final char PROJECTNAME_SEP = '.';    
    private static final char BASENAME_SEP = '-';
    private static final String JAR_EXT = ".jar";
    private static final String WAR_EXT = ".war";
    private static final Pattern mySymbolicNameFormat = Pattern.compile(
            "[\\p{Lower}_][\\p{Lower}\\d_]*(\\.[\\p{Lower}_][\\p{Lower}\\d_]*)*");
    private static final Pattern myVersionFormat = Pattern.compile(
            "([1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)");    
    private Text myVersionText = null;
    private Text mySpecificationTitleText = null;
    private Text myVendorText = null;
    private Combo myTargetCombo = null;
    private List<String> myTargetList = null;
    private Text myMainClassText = null;
    
    /**
     * Costruttore.
     */
    NewProjectPage() {
        super("project");
        setTitle("Programmer Power Project");
        setDescription("Create a new Programmer Power project.");        
    }

    /**
     * Restituisce il nome del progetto.
     * 
     * @return Valore.
     */
    @Override
    public String getProjectName() {
        String name;
        
        name = super.getProjectName();
        if (name == null) {
            return "";
        }
        
        return name.toLowerCase();
    }
    
    /**
     * Restituisce la versione.
     * 
     * @return Valore.
     */
    public String getVersion() {
        if (myVersionText == null) {
            return "";
        }
        
        return myVersionText.getText().trim();
    }
    
    /**
     * Restituisce il titolo.
     * 
     * @return Valore.
     */
    public String getSpecificationTitle() {
        if (mySpecificationTitleText == null) {
            return "";
        }
        
        return mySpecificationTitleText.getText().trim();
    }
    
    /**
     * Restituisce il produttore.
     * 
     * @return Valore.
     */
    public String getVendor() {
        if (myVendorText == null) {
            return "";
        }
        
        return myVendorText.getText().trim();
    }
    
    public String getBaseName() {
        String baseName, projectName;
        
        projectName = getProjectName();
        baseName = projectName.replace(
                NewProjectPage.PROJECTNAME_SEP,
                NewProjectPage.BASENAME_SEP);
        
        return baseName;        
    }
    
    public String getArchiveExtension() {
        String target = getTarget();
        
        if (INewProjectParameters.TARGET_WAR.equals(target)) {
            return NewProjectPage.WAR_EXT;
        }
        
        return NewProjectPage.JAR_EXT;
    }
    
    public String getTarget() {
        int selectedIdx;
        String target;

        if (myTargetCombo == null || myTargetList == null) {
            return null;
        }
        
        target = null;
        selectedIdx = myTargetCombo.getSelectionIndex();
        if (selectedIdx >= 0 && selectedIdx < myTargetList.size()) {
            target = myTargetList.get(selectedIdx);            
        }
        
        return target;
    }
    
    public String getMainClass() {
        if (myMainClassText == null) {
            return "";
        }
        
        return myMainClassText.getText().trim();
    }
    
    /**
     * Registra le impostazioni.
     * 
     * @param settings Impostazioni.
     */
    void saveSettings(IDialogSettings settings) {
        settings.put(NewProjectPage.DEF_VENDOR, getVendor());
    }
    
    /**
     * Crea i controlli.
     * 
     * @param parent Controllo parent.
     */
    @Override
    public void createControl(Composite parent) {
        Composite control;
        
        super.createControl(parent);
        
        control = (Composite) getControl();        
        control.setLayout(new GridLayout());    
        create(control);
        
        Dialog.applyDialogFont(control);
        setControl(control);
    }
    
    /**
     * Crea i controlli.
     * 
     * @param container Contenitore.
     */
    private void create(Composite container) {
        String value;
        Composite panel;
        IDialogSettings settings = getDialogSettings();
        
        panel = new Composite(container, SWT.NONE);
        panel.setLayout(new GridLayout(2, false));
        panel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        
        createLabel(panel, "Version:");
        myVersionText = createText(panel);
        myVersionText.setText("1.0.0");
        
        createLabel(panel, "Title:");
        mySpecificationTitleText = createText(panel);
        
        createLabel(panel, "Vendor:");
        myVendorText = createText(panel);
        
        value = null;
        if (settings != null) {
            value = settings.get(NewProjectPage.DEF_VENDOR);
        }
        if (value == null || value.isEmpty()) {
            try {
                value = System.getProperty(NewProjectPage.PROP_USERNAME);
            } catch (Exception ex) {
                StatusTools.logStatus(ex);
            }
        }
        
        myVendorText.setText(value);
         
        createLabel(panel, "Target:");
        myTargetCombo = createCombo(panel);
        myTargetList = loadTargetCombo(myTargetCombo);
                
        createLabel(panel, "Main-Class:");
        myMainClassText = createText(panel);
        
        myVersionText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                setPageComplete(validatePage());
            }            
        });
        
        myTargetCombo.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                setPageComplete(validatePage());
            }

            public void widgetDefaultSelected(SelectionEvent arg0) {
                setPageComplete(validatePage());
            }                        
        });
    }
    
    /**
     * Verifica la validit&agrave; delle impostazioni sui controlli della
     * pagina.
     * 
     * @return Esito della verifica.
     */
    @Override
    protected boolean validatePage() {
        String msg, projectName, target, version;
        Matcher matcher;
        
        if (!super.validatePage()) {
            return false;
        }
             
        projectName = getProjectName(); 
        matcher = mySymbolicNameFormat.matcher(projectName);
        if (!matcher.matches()) {
            msg = String.format("Malformed project name \"%1$s\"", projectName);
            setErrorMessage(msg);
            return false;
        }
        
        version = getVersion();
        if (version.isEmpty()) {
            setErrorMessage("Version must be specified");
            return false;
        }
        
        matcher = myVersionFormat.matcher(version);
        if (!matcher.matches()) {
            msg = String.format("Malformed version \"%1$s\"", version);
            setErrorMessage(msg);            
            return false;
        }
       
        target = getTarget();
        if (target == null || target.isEmpty()) {
            setErrorMessage("Any target must be selected.");
            return false;
        }
        
        if (INewProjectParameters.TARGET_WAR.equals(target)) {
            myMainClassText.setText("");
            myMainClassText.setEnabled(false);
        } else {
            myMainClassText.setEnabled(true);
        }
        
        setErrorMessage(null);
        setMessage(null);        
        return true;
    }
            
    /**
     * Crea un&rsquo;etichetta.
     * 
     * @param container Contenitore.
     * @param text      Testo.
     */
    private void createLabel(Composite container, String text) {
        Label label;
        GridData gridData;
        
        label = new Label(container, SWT.NONE);
        label.setText(text);
        gridData = new GridData();
        label.setLayoutData(gridData);        
    }    
    
    /**
     * Crea una casella di testo.
     * 
     * @param  container Contenitore. 
     * @return           Controllo.
     */
    private Text createText(Composite container) {
        Text text;
        GridData gridData;
        
        text = new Text(container, SWT.BORDER | SWT.SINGLE);
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.widthHint = 300;
        text.setLayoutData(gridData);        
                 
        return text;
    }            
    
    /**
     * Crea una casella combinata.
     * 
     * @param  container Contenitore. 
     * @return           Controllo.
     */
    private Combo createCombo(Composite container) {
        Combo combo;
        GridData gridData;
        
        combo = new Combo(container, SWT.READ_ONLY);
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.widthHint = 300;
        combo.setLayoutData(gridData);        
                         
        return combo;
    }
    
    /**
     * Costruisce la lista dei target selezionabili.
     * 
     * @param  combo Casella combinata.
     * @return       Lista delle chiavi dei target corrispondenti agli elementi
     *               inseriti nella casella combinata.
     */
    private List<String> loadTargetCombo(Combo combo) {
        List<String> targetList = new ArrayList<String>();
        
        targetList.add(INewProjectParameters.TARGET_JAR);
        combo.add("Java module");
        
        targetList.add(INewProjectParameters.TARGET_WAR);
        combo.add("Dynamic web application");

        combo.select(0);
        
        return targetList;
    }
}

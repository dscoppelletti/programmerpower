## 1. pom.xml

1. Download and install [Apache Maven](http://maven.apache.org).<BR><BR>
2. Run the following command from
`$PROGRAMMERPOWER_HOME/etc/cas-server`:<BR><BR>
`mvn`<BR><BR>
... in order to get the [CAS server](http://www.apereo.org/cas)
software.<BR><BR>
3. Follow the instructions at
[Installing CAS server](http://www.scoppelletti.it/programmerpower/tomcat/cas).

## 2. Glossary

`$PROGRAMMERPOWER_HOME` = Directory where you installed Programmer Power
(e.g. `/var/programmerpower`).


<?xml version="1.0" encoding="UTF-8"?>
<project name="Global Build" default="usage" basedir="."
    xmlns:sdk="http://www.scoppelletti.it/ns/sdk"
    xmlns:tc="http://www.scoppelletti.it/ns/tomcat">

    <target name="usage">
        <echo><![CDATA[ant install
Installs all modules in the Programmer Power repository.

ant clean
Cleans all modules.

ant deploy-libs -Dcatalina.home=<e.g. /var/tomcat>
Installs some libraries as extension libraries in Apache Tomcat.
NOTE: Tomcat server can not be running and
you must have write permissions in Tomcat installation directory.

ant deploy-log4j -Dcatalina.home=<e.g. /var/tomcat>
Installs Apache Tomcat Extras libraries for integrate Tomcat with Apache log4j.
NOTE: Tomcat server can not be running and
you must have write permissions in Tomcat installation directory.

ant deploy-cas -Dcatalina.home=<e.g. /var/tomcat>
Deploys CAS Server in Apache Tomcat.
NOTE: Tomcat server can not be running and
you must have write permissions in Tomcat installation directory.

ant deploy-apps -Dtomcat.url=<e.g. http://localhost:8080/manager/text> -Dtomcat.user=<i.e. ant> -Dtomcat.pwd=<i.e. changeit>
ant deploy-apps -Dtomcat.properties=<e.g. /home/joe/private/tomcat.properties>
Deploys all Web applications in Apache Tomcat.
NOTE: Tomcat server has to be running.
]]></echo>
    </target>

    <target name="-init" description="Init.">
        <!-- Direttorio di sistema -->
        <property name="system.dir" value="${basedir}/.." />
        <fail message="System directory ${system.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${system.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
		<!-- Repository dei moduli -->
		<property name="module.repo.dir" value="${system.dir}/modules" />
        <fail message="Directory ${module.repo.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${module.repo.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
    </target>

    <target name="-init-build" description="Init to build."
        depends="-init">
		<!-- Direttorio dei sorgenti -->
		<property name="src.dir" value="${system.dir}/src" />
        <fail message="Directory ${src.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${src.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
		<!-- Task personalizzati -->
        <macrodef name="ant"
            description="Delegate target to specified project."
            uri="http://www.scoppelletti.it/ns/sdk">
	        <attribute name="projectdir" description="Project directory." />
	        <attribute name="target" description="Target name." />
            <sequential>
                <ant dir="@{projectdir}" antfile="programmerpower/build.xml"
                    target="@{target}"
                    inheritAll="false" inheritRefs="false">
                    <propertyset>
                        <propertyref name="system.dir" />
                    </propertyset>
                </ant>
            </sequential>
        </macrodef>
        <macrodef name="antall"
            description="Delegate target to all projects."
            uri="http://www.scoppelletti.it/ns/sdk">
	        <attribute name="target" description="Target name." />
            <sequential>
                <!-- Runtime -->
                <sdk:ant projectdir="${src.dir}/runtime/core"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/sdk/buildlib"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/runtime/tomcat"
                    target="@{target}" />
                <!-- Libraries -->
                <sdk:ant projectdir="${src.dir}/diagnostic/core"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/diagnostic/log4j"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower/core"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower/xml"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower/config"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower/data"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower/security"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower/console"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower-web/core"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower-web/mvc"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower-web/rest"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/programmerpower-web/wui"
                    target="@{target}" />
                <!-- Applications -->
                <sdk:ant projectdir="${src.dir}/apps/digest"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/sdk/schemaupdate"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/apps/textdump"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/apps/textflat"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/apps/sitebuild"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/apps/fileshuffle"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/apps/keygen"
                    target="@{target}" />
                <sdk:ant projectdir="${src.dir}/apps/keypairgen"
                    target="@{target}" />
                <!-- Web applications -->
                <sdk:ant projectdir="${src.dir}/webapps/wui"
                    target="@{target}" />
            </sequential>
        </macrodef>
    </target>

    <target name="-install-thirdparty"
        description="Install third-party components."
        depends="-init-build">
        <sdk:ant projectdir="${src.dir}/thirdparty/antlr"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/aopalliance"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/ant"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/beanutils"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/codec"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/collections4"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/digester"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/fileupload"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/io"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/lang3"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/logging"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/commons/ognl"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/hc/httpclient"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/hc/httpcore"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/log4j"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/struts2"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/tiles"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apache/tomcat"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/apereo/cas/client"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/cglib"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/dom4j"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/freemarker"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/javax/annotation"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/javax/el"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/javax/jpa"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/javax/jsp"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/javax/jta"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/javax/servlet"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/jboss/hibernate"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/jboss/javassist"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/json"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/liquibase"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/mchange/c3p0"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/mysql/connectorj"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/ow2/asm"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/qos/slf4j"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/qos/slf4j-log4j"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/restlet/jee/restlet"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/restlet/jee/restlet-servlet"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/springio/framework/core"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/springio/framework/data"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/springio/framework/web"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/springio/security/core"
            target="install" />
        <sdk:ant projectdir="${src.dir}/thirdparty/springio/security/web"
            target="install" />
    </target>

    <target name="install" description="Install."
        depends="-init-build,-install-thirdparty">
        <sdk:antall target="install"/>
    </target>

    <target name="clean" description="Clean."
        depends="-init-build">
        <sdk:ant projectdir="${src.dir}/runtime/core"
            target="clean" />
        <sdk:ant projectdir="${src.dir}/sdk/buildlib"
            target="clean" />
        <sdk:ant projectdir="${src.dir}/runtime/core"
            target="install" />
        <sdk:ant projectdir="${src.dir}/sdk/buildlib"
            target="install" />
        <sdk:antall target="clean"/>
    </target>

    <target name="-init-deploy" description="Init to deploy."
        depends="-init">
        <property name="thirdparty.lib.dir"
            value="${system.dir}/thirdparty/lib" />
        <fail message="Directory ${thirdparty.lib.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${thirdparty.lib.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
    </target>

    <target name="-init-deploy-libs" description="Init to deploy libraries."
        depends="-init-deploy">
        <fail unless="catalina.home"
        	message="Property &quot;catalina.home&quot; not set." />
        <fail message="Directory ${catalina.home} not exists.">
        	<condition>
        		<not>
        			<available file="${catalina.home}" type="dir" />
        		</not>
        	</condition>
        </fail>
        <property name="catalina.conf.dir" value="${catalina.home}/conf" />
        <fail message="Directory ${catalina.conf.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${catalina.conf.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
        <property name="catalina.lib.dir" value="${catalina.home}/lib/ext" />
        <mkdir dir="${catalina.lib.dir}" />
    </target>

    <target name="deploy-libs" description="Deploy libraries."
        depends="-init-deploy-libs">
        <copy todir="${catalina.lib.dir}" overwrite="false" flatten="true">
            <fileset dir="${thirdparty.lib.dir}">
                <include name="log4j.jar" />
                <include name="mysql-connector-java.jar" />
            </fileset>
            <fileset dir="${module.repo.dir}">
<include name="it/scoppelletti/runtime/1.1.1/it-scoppelletti-runtime.jar" />
<include name="it/scoppelletti/runtime/tomcat/1.2.0/it-scoppelletti-runtime-tomcat.jar" />
<include name="it/scoppelletti/diagnostic/1.0.0/it-scoppelletti-diagnostic.jar" />
<include name="it/scoppelletti/diagnostic/log4j/1.0.0/it-scoppelletti-diagnostic-log4j.jar" />
            </fileset>
        </copy>
        <copy todir="${catalina.conf.dir}" overwrite="false" flatten="true">
            <fileset dir="${module.repo.dir}">
<include name="it/scoppelletti/runtime/tomcat/1.2.0/it-scoppelletti-runtime-modules.txt" />
            </fileset>
        </copy>
        <copy file="${thirdparty.lib.dir}/log4j-tomcat.xml"
              tofile="${catalina.conf.dir}/log4j.xml"
              overwrite="false" /> 
        <echo><![CDATA[Before restart Apache Tomcat, you have to edit the
${catalina.conf.dir}/catalina.properties file and:
1) Append ",$${catalina.home}/lib/ext/*.jar" to the common.loader property.
2) Set the log4j.configuration property to "file://${catalina.conf.dir}/log4j.xml".
3) Set the org.restlet.engine.loggerFacadeClass property to 
"org.restlet.ext.slf4j.Slf4jLoggerFacade".
]]></echo>			
    </target>

    <target name="-init-deploy-log4j" description="Init to deploy log4j."
        depends="-init-deploy-libs">
        <property name="catalina.bin.dir" value="${catalina.home}/bin" />
        <fail message="Directory ${catalina.bin.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${catalina.bin.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
    </target>

    <target name="deploy-log4j" description="Deploy log4j."
        depends="-init-deploy-log4j">
        <copy file="${thirdparty.lib.dir}/tomcat-extras-juli.jar"
              tofile="${catalina.bin.dir}/tomcat-juli.jar"
              overwrite="true" /> 
        <copy file="${thirdparty.lib.dir}/tomcat-extras-juli-adapters.jar"
              tofile="${catalina.lib.dir}/tomcat-juli-adapters.jar"
              overwrite="false" /> 
        <delete dir="${catalina.conf.dir}">
            <include name="logging.properties" />
        </delete>
    </target>

    <target name="-init-deploy-cas" description="Init to deploy CAS."
        depends="-init-deploy">
        <property name="thirdparty.cas.dir"
            value="${system.dir}/thirdparty/cas-server" />
        <fail message="Directory ${thirdparty.cas.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${thirdparty.cas.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
        <fail unless="catalina.home"
        	message="Property &quot;catalina.home&quot; not set." />
        <fail message="Directory ${catalina.home} not exists.">
        	<condition>
        		<not>
        			<available file="${catalina.home}" type="dir" />
        		</not>
        	</condition>
        </fail>
        <property name="catalina.webapps.dir"
            value="${catalina.home}/webapps" />
        <fail message="Directory ${catalina.webapps.dir} not exists.">
        	<condition>
        		<not>
        			<available file="${catalina.webapps.dir}" type="dir" />
        		</not>
        	</condition>
        </fail>
        <property name="catalina.cas.dir"
            value="${catalina.webapps.dir}/cas" />
        <mkdir dir="${catalina.cas.dir}" />
    </target>

    <target name="deploy-cas" description="Deploy CAS server."
        depends="-init-deploy-cas">
        <unwar src="${thirdparty.cas.dir}/cas-server-webapp.war"
            dest="${catalina.cas.dir}" overwrite="true"
            failOnEmptyArchive="true" />
        <delete dir="${catalina.cas.dir}/WEB-INF">
            <include name="classes/log4j.xml" />
            <include name="lib/log4j-1.2.17.jar" />
            <include name="spring-configuration/log4jConfiguration.xml" />
        </delete>
        <copy todir="${catalina.cas.dir}/META-INF" overwrite="false"
            flatten="true">
            <fileset dir="${thirdparty.cas.dir}">
                <include name="context.xml" />
            </fileset>
        </copy>
        <copy todir="${catalina.cas.dir}/WEB-INF/lib" overwrite="false"
            flatten="true">
            <fileset dir="${thirdparty.cas.dir}">
                <include name="cas-server-support-jdbc.jar" />
                <include name="cas-server-integration-restlet.jar" />
                <include name="org.restlet.jar" />
                <include name="org.restlet.ext.servlet.jar" />
                <include name="org.restlet.ext.slf4j.jar" />
                <include name="org.restlet.ext.spring.jar" />
            </fileset>
        </copy>				
    </target>

    <target name="-init-tomcat-properties" description="Load Tomcat properties.">
        <condition property="tomcat.properties.enabled">
            <and>
                <isset property="tomcat.properties" />
                <available file="${tomcat.properties}" type="file" />
            </and>
        </condition>
    </target>

    <target name="-tomcat-properties" description="Load Tomcat properties."
        if="tomcat.properties.enabled">
        <property file="${tomcat.properties}" />
    </target>

    <target name="-init-deploy-apps" description="Init to deploy applications."
depends="-init-deploy,-init-tomcat-properties,-tomcat-properties">
        <fail unless="tomcat.user"
            message="Property &quot;tomcat.user&quot; not set." />
        <fail unless="tomcat.pwd"
            message="Property &quot;tomcat.pwd&quot; not set." />
        <fail unless="tomcat.url"
            message="Property &quot;tomcat.url&quot; not set." />
        <typedef resource="org/apache/catalina/ant/catalina.tasks"
            uri="http://www.scoppelletti.it/ns/tomcat">
            <classpath>
                <fileset dir="${thirdparty.lib.dir}">
					<include name="commons-logging.jar" />
                    <include name="tomcat-catalina-ant.jar" />
                    <include name="tomcat-coyote.jar" />
					<include name="tomcat-jasper.jar" />
                    <include name="tomcat-jsp-api.jar" />
					<include name="tomcat-juli.jar" />
                    <include name="tomcat-servlet-api.jar" />
					<include name="tomcat-util.jar" />
                </fileset>
            </classpath>
        </typedef>
        <macrodef name="deploy-app"
            description="Deploy web application."
            uri="http://www.scoppelletti.it/ns/sdk">
            <attribute name="contextName" description="Context name." />
            <attribute name="moduleDir" description="Module directory." />
            <sequential>
                <tc:deploy url="${tomcat.url}"
                    username="${tomcat.user}" password="${tomcat.pwd}"
                    path="/@{contextName}" update="true"
                    localWar="@{moduleDir}/@{contextName}.war"
                    config="@{moduleDir}/context.xml" />
            </sequential>
        </macrodef>
    </target>

    <target name="deploy-apps" description="Deploy applications."
        depends="-init-deploy-apps">
        <sdk:deploy-app contextName="it-scoppelletti-wui"
            moduleDir="${module.repo.dir}/it/scoppelletti/wui/2.0.0" />
    </target>
</project>


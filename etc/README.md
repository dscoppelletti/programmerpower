## 1. Third parties' software

See the `README.md` files in each sub-directories in order to get the
needed third parties' software.

## 2. build.xml

Ant script for batch installing of all Programmer Power modules.
Run `ant usage` for more.

You have to download and install [Apache Ant](http://ant.apache.org) in
order to use this and other scripts.

## 3. build-impl.xml

Core Ant script used by the `build.xml` script of each Programmer Power
module.<BR>
**This script is for internal use only**.

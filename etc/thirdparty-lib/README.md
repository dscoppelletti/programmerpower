## 1. pom.xml

1. Download and install [Apache Maven](http://maven.apache.org).<BR><BR>
2. Run the following command from
`$PROGRAMMERPOWER_HOME/etc/thirdparty-lib`:<BR><BR>
`mvn`<BR><BR>
... in order to get the needed third parties' libraries.

## 2. log4j-tomcat.xml

Sample configuration file of [Apache log4j](http://logging.apache.org/log4j/1.2)
for [Apache Tomcat](http://tomcat.apache.org).

## 3. Glossary

`$PROGRAMMERPOWER_HOME` = Directory where you installed Programmer Power
(e.g. `/var/programmerpower`).

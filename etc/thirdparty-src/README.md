## 1. pom.xml

1. Download and install [Apache Maven](http://maven.apache.org).<BR><BR>
2. Run the following command from
`$PROGRAMMERPOWER_HOME/etc/thirdparty-src`:<BR><BR>
`mvn`<BR><BR>
... in order to get the source code of the third parties' libraries.

## 2. Glossary

`$PROGRAMMERPOWER_HOME` = Directory where you installed Programmer Power
(e.g. `/var/programmerpower`).

